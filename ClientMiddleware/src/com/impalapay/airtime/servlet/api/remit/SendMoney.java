package com.impalapay.airtime.servlet.api.remit;

import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.mno.servlet.api.remit.AsyncTransactionDispatcher;
import com.impalapay.mno.servlet.api.remit.TransactionDispatcher;
import com.impalapay.airtel.accountmgmt.session.SessionConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.mno.beans.accountmgmt.balance.ClientAccountBalanceByCountry;
import com.impalapay.mno.beans.accountmgmt.balance.MasterAccountBalance;
import com.impalapay.airtel.beans.forex.ForexEngine;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.geolocation.CountryMsisdn;
import com.impalapay.airtel.beans.sessionlog.SessionLog;
import com.impalapay.airtel.beans.thirdreference.ThirdPartyReference;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.beans.transaction.TransactiontransferType;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.mno.persistence.accountmgmt.balance.AccountBalanceDAO;
import com.impalapay.mno.persistence.prefix.PrefixDAO;
import com.impalapay.airtel.persistence.geolocation.CountryMsisdnDAO;
import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;
import com.impalapay.airtel.persistence.transaction.TransactionDAO;
import com.impalapay.airtel.persistence.transaction.TransactionTypeDAO;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.util.SecurityUtil;
import com.impalapay.airtel.util.MapUtil;
import com.impalapay.airtel.util.CurrencyConvertUtil;
import com.impalapay.airtel.util.PhonenumberSplitUtil;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
import com.impalapay.beans.network.Network;
import com.impalapay.beans.prefix.PrefixSplit;
import com.impalapay.beans.route.RouteDefine;
import com.impalapay.persistence.routing.RouteDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Allows for sending through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class SendMoney extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1073286884431640677L;

	private PostWithIgnoreSSL postMinusThread;

	private PhonenumberSplitUtil phonenumbersplit;

	private TransactionDAO transactionDAO;

	private TransactionTypeDAO transactiontypeDAO;

	private Cache accountsCache, countryCache, transactionStatusCache, forexCache;
	private Cache networkCache, prefixCache;

	private SessionLogDAO sessionlogDAO;

	private AccountBalanceDAO accountbalanceDAO;

	private CountryMsisdnDAO countryMsisdnDAO;

	private RouteDAO routeDAO;

	private PrefixDAO prefixDAO;

	private HashMap<String, String> countryHash = new HashMap<>();

	private HashMap<String, String> countryCode = new HashMap<>();

	private HashMap<String, String> countryUuid = new HashMap<>();

	private HashMap<String, String> countryIp = new HashMap<>();

	private HashMap<String, String> countryUsername = new HashMap<>();

	private HashMap<String, String> countryPassword = new HashMap<>();

	private HashMap<String, String> transactionStatusHash = new HashMap<>();

	private HashMap<String, String> statusDescriptionHash = new HashMap<>();

	private HashMap<String, Double> forexmarketratemap = new HashMap<>();

	private HashMap<String, Double> forexspreadratemap = new HashMap<>();

	private HashMap<String, String> networkCode = new HashMap<>();

	private HashMap<String, Integer> mobilesplitlenght = new HashMap<>();

	private HashMap<String, String> prefixnumbernetworkHashmap = new HashMap<>();

	private HashMap<String, String> networkRemitUrlmap = new HashMap<>();

	private HashMap<String, String> networkBridgeRemitUrlmap = new HashMap<>();

	private HashMap<String, String> networkUsernamemap = new HashMap<>();

	private HashMap<String, String> networkPasswordmap = new HashMap<>();

	private HashMap<String, String> networkPartnernamemap = new HashMap<>();

	private HashMap<String, String> networkcountrymap = new HashMap<>();

	private HashMap<String, String> networknamemap = new HashMap<>();

	private HashMap<String, String> countryMsisdnmap = new HashMap<>();

	private HashMap<String, String> routeaccountnetworkmap = new HashMap<>();

	private HashMap<String, Boolean> routenetworkuuidmap = new HashMap<>();

	private HashMap<String, Double> routeoperatingbalancemap = new HashMap<>();

	private HashMap<String, String> routenetworkaccountuuidmap = new HashMap<>();

	private HashMap<String, String> routedeterminemap = new HashMap<>();

	private Map<String, Double> balancemap = new HashMap<>();

	private String CLIENT_URL = "";

	private String phoneresults = "";

	private String networkroute = "", networkrouteuuid = "";

	private int prefixlength = 0;

	private List<ClientAccountBalanceByCountry> clientBalances;

	private List<PrefixSplit> prefixes;

	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		transactionDAO = TransactionDAO.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);

		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);

		forexCache = mgr.getCache(CacheVariables.CACHE_FOREX_BY_UUID);

		networkCache = mgr.getCache(CacheVariables.CACHE_NETWORK_BY_UUID);

		prefixCache = mgr.getCache(CacheVariables.CACHE_PREFIX_BY_UUID);

		sessionlogDAO = SessionLogDAO.getInstance();

		accountbalanceDAO = AccountBalanceDAO.getInstance();

		countryMsisdnDAO = CountryMsisdnDAO.getInstance();

		routeDAO = RouteDAO.getInstance();

		transactiontypeDAO = TransactionTypeDAO.getInstance();

		prefixDAO = PrefixDAO.getInstance();

		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		OutputStream out = response.getOutputStream();
		// responseobject
		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(sendMoney(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return JSon response
	 * @throws IOException
	 */
	private String sendMoney(HttpServletRequest request) throws IOException {
		Account account = null;

		// String impalaexchange = "";

		double impalaexchangecalculate = 0, baseexchange = 0, convertedamountToWallet = 0, amount = 0,
				imtmasterbalance = 0, countryamount = 0;

		int finalconvertedamount = 0;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonObject vendorfields = null, root2 = null, creditrequest = null;

		// These represent parameters received over the network
		String username = "", sessionid = "", sourcecountrycode = "", sendername = "", recipientmobile = "",
				recipientcurrencycode = "", recipientcountrycode = "", referencenumber = "", clienttime = "",
				sendertoken = "", sendcurrency = "", countrycodetodb = "";

		// represents hashmaps values
		String apiusername = "", remiturlss = "", bridgeurl = "", apipassword = "", networktodatabase="";

		// route extract values
		String responseobject = "", switchresponse = "", statusdescription = "", statusuuid = "", success = "",
				inprogress = "", accountuuid = "", receiveruuid = "", unifiedstatusdescription = "";

		// prerequisites
		String amountstring = "", transactioinid = "", transactionforexhistoryuuid = "";

		String originatecurrency = "", terminatecurrency = "", currencypair = "",wallettype="";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), " ");

		// ###########################################################
		// instantiate the JSon
		// ##########################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("api_username").getAsString();

			sessionid = root.getAsJsonObject().get("session_id").getAsString();

			sourcecountrycode = root.getAsJsonObject().get("source_country_code").getAsString();

			sendername = root.getAsJsonObject().get("sendername").getAsString();

			recipientmobile = root.getAsJsonObject().get("recipient_mobile").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("recipient_currency_code").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("recipient_country_code").getAsString();

			referencenumber = root.getAsJsonObject().get("reference_number").getAsString();

			sendertoken = root.getAsJsonObject().get("sendertoken").getAsString();

			amount = root.getAsJsonObject().get("amount").getAsDouble();

			clienttime = root.getAsJsonObject().get("client_datetime").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// ####################################################################
		// check for the presence of all required parameters
		// ####################################################################

		if (StringUtils.isBlank(username) || StringUtils.isBlank(sessionid) || StringUtils.isBlank(sourcecountrycode)
				|| StringUtils.isBlank(sendername) || StringUtils.isBlank(recipientmobile)
				|| StringUtils.isBlank(recipientcurrencycode) || StringUtils.isBlank(recipientcountrycode)
				|| StringUtils.isBlank(referencenumber) || StringUtils.isBlank(sendertoken)
				|| StringUtils.isBlank(clienttime) || amount <= 0) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALIDEMPTY_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

			vendorfields = root.getAsJsonObject().get("vendor_uniquefields").getAsJsonObject();

		}

		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		if (account == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		originatecurrency = account.getAccounttype();

		// check if route has originate currency
		if (root2.has("Originate_currency")) {

			sendcurrency = root.getAsJsonObject().get("Originate_currency").getAsString();
			if (StringUtils.isBlank(sendcurrency)) {
				originatecurrency = account.getAccounttype();
			} else {
				originatecurrency = sendcurrency;
			}
		}

		// test for invalid sessionid
		SessionLog sessionlog = sessionlogDAO.getValidSessionLog(account);

		// ################################################################
		// Guard against all invalid sessionid error
		// ################################################################
		if (sessionlog == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		String session = sessionlog.getSessionUuid();

		if (!StringUtils.equals(SecurityUtil.getMD5Hash(sessionid), session)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		List keys;
		
		
		wallettype = SessionConstants.AIRTIME_ENDPONT;


		// fetch from cache
		Country country;
		keys = countryCache.getKeys();
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryHash.put(country.getCountrycode(), country.getCurrencycode());
		}

		// country and country uuid
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryCode.put(country.getCountrycode(), country.getUuid());
		}
		// country uuid and country code
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryUuid.put(country.getUuid(), country.getCountrycode());
		}

		// country and country ip
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryIp.put(country.getCountrycode(), country.getCountryremitip());
		}

		// country and username
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryUsername.put(country.getCountrycode(), country.getUsername());
		}

		// country and country password
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryPassword.put(country.getCountrycode(), country.getPassword());
		}

		// Countrycode and mobile splitlegth
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			mobilesplitlenght.put(country.getCountrycode(), country.getMobilesplitlength());
		}

		// **************Network Cache****************//

		Network network;
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkCode.put(network.getCountryUuid(), network.getPartnername());
		}

		// network and remiturl
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkRemitUrlmap.put(network.getUuid(), network.getRemitip());
		}
		// network and username
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkUsernamemap.put(network.getUuid(), network.getUsername());
		}

		// network and username
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networknamemap.put(network.getUuid(), network.getNetworkname());
		}

		// network and password
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkPasswordmap.put(network.getUuid(), network.getPassword());
		}

		// network and password
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkPartnernamemap.put(network.getUuid(), network.getPartnername());
		}

		// network and bridgeremiturl
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkBridgeRemitUrlmap.put(network.getUuid(), network.getBridgeremitip());
		}

		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkcountrymap.put(network.getUuid(), network.getCountryUuid());
		}

		// *************Prefix Cache****************//
		PrefixSplit prefixsplit;

		// prefixnumber and networkuuid
		keys = prefixCache.getKeys();
		for (Object key : keys) {
			element = prefixCache.get(key);
			prefixsplit = (PrefixSplit) element.getObjectValue();
			prefixnumbernetworkHashmap.put(prefixsplit.getPrefix(), prefixsplit.getNetworkUuid());
		}
		// forex with curency pairs
		ForexEngine forexengine;
		keys = forexCache.getKeys();
		for (Object key : keys) {
			element = forexCache.get(key);
			forexengine = (ForexEngine) element.getObjectValue();
			forexmarketratemap.put(forexengine.getCurrencypair(), forexengine.getMarketrate());

		}

		keys = forexCache.getKeys();
		for (Object key : keys) {
			element = forexCache.get(key);
			forexengine = (ForexEngine) element.getObjectValue();
			forexspreadratemap.put(forexengine.getCurrencypair(), forexengine.getSpreadrate());

		}

		// ======================================================
		// Populate with the mapping of Transaction Statuses.
		// The key is a UUID of the status
		// ======================================================

		TransactionStatus status;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status.getStatus(), status.getUuid());
		}

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status = (TransactionStatus) element.getObjectValue();
			statusDescriptionHash.put(status.getStatus(), status.getDescription());
		}

		// ################################################################
		// Empty the below current Hashmaps(helps in updating deleted items)
		// ################################################################
		routeaccountnetworkmap.clear();
		routenetworkuuidmap.clear();
		routeoperatingbalancemap.clear();
		routenetworkaccountuuidmap.clear();
		routedeterminemap.clear();

		// ################################################################
		// Fetch route set up details
		// #################################################################
		List<RouteDefine> routedefine = routeDAO.getAllRoute(account);

		for (RouteDefine routenetworkuuid : routedefine) {
			routeaccountnetworkmap.put(routenetworkuuid.getNetworkUuid(), routenetworkuuid.getUuid());
			routenetworkuuidmap.put(routenetworkuuid.getUuid(), routenetworkuuid.isSupportforex());
			routeoperatingbalancemap.put(routenetworkuuid.getUuid(), routenetworkuuid.getMinimumbalance());
			routenetworkaccountuuidmap.put(routenetworkuuid.getNetworkUuid(), routenetworkuuid.getAccountUuid());
		}
		// ##################################################################
		// checks for the provide currencyCode(invalid)
		// ##################################################################
		if (!countryHash.containsValue(recipientcurrencycode)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_CURRENCYCODE);

			return g.toJson(expected);
		}

		// checks for the provided countryCode(invalid)
		if (!countryHash.containsKey(recipientcountrycode)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_COUNTRYCODE);

			return g.toJson(expected);
		}

		// =========================================================
		// determines if the provided recipient currencyCode doesn't
		// correspond to the countryCode
		// =========================================================

		if (!StringUtils.equalsIgnoreCase(countryHash.get(recipientcountrycode), recipientcurrencycode)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_CURRENCY_COUNTRYMISMATCH);

			return g.toJson(expected);
		}

		// retrieve from countrycode hashmap uuid representing the provided
		// country
		countrycodetodb = countryCode.get(recipientcountrycode);

		// select prefix.
		// checks for the provided receiver country code is included in the
		// prefix hash(invalid)
		if (!mobilesplitlenght.containsKey(recipientcountrycode)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_COUNTRYCODE);

			return g.toJson(expected);
		}

		// select the split length
		prefixlength = mobilesplitlenght.get(recipientcountrycode);
		// split the received phone number
		phonenumbersplit = new PhonenumberSplitUtil();
		phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, prefixlength);

		// check if the phonenumber matches the ones listed on hashmap.
		if (!prefixnumbernetworkHashmap.containsKey(phoneresults)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_NO_MSISDN_NETWORK_MATCH + phoneresults);

			return g.toJson(expected);
		}
		// ##################################################################
		// choose the networks that have the defined prefix
		// ##################################################################
		//Suggestion to add endpoint type e.g mobile money or airtime
		//prefixes = prefixDAO.getPrefixSplits(phoneresults, countrycodetodb);
		prefixes = prefixDAO.getPrefixSplits(phoneresults, countrycodetodb, wallettype);


		// =========================================================
		// Loop through the reurned prefix list and create a Map for network
		// and account
		// =========================================================
		for (PrefixSplit splitprefix : prefixes) {

			if (StringUtils.equalsIgnoreCase(routenetworkaccountuuidmap.get(splitprefix.getNetworkUuid()),
					account.getUuid())) {

				routedeterminemap.put(splitprefix.getNetworkUuid(),
						routenetworkaccountuuidmap.get(splitprefix.getNetworkUuid()));
			}

		}

		// ##################################################################
		// check and see if the created route hashmap has
		// ##################################################################
		if (routedeterminemap.size() > 1) {

			// check the active route
			// choose randomly
			// choose a random hashmap value.
			Random generator = new Random();
			Object[] keyss = routedeterminemap.keySet().toArray();
			// Object randomValue = values[generator.nextInt(values.length)];

			networkroute = (String) keyss[generator.nextInt(keyss.length)];
			// or return an error duplicate routes

			// return "duplicates found "+routedeterminemap +" the current
			// hashmap "+routenetworkaccountuuidmap+" the choosen network
			// is"+networkroute;
		} else {

			// fetch key based on value

			networkroute = (String) MapUtil.getKeyFromValue(routedeterminemap, account.getUuid());

		}

		// networkroute = routedeterminemap.get(account.getUuid());

		networkrouteuuid = routeaccountnetworkmap.get(networkroute);

		if (StringUtils.isEmpty(networkrouteuuid)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_ROUTEDEFINE_ERROR);
			System.out.println(networkroute);
			return g.toJson(expected);
		}

		// check the url authentication details(username and password exist)
		if (!networkRemitUrlmap.containsKey(networkroute) || !networkUsernamemap.containsKey(networkroute)
				|| !networkPasswordmap.containsKey(networkroute)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_RECEIVER_ENDPOINT_AUTHENTICATE_ERROR);

			return g.toJson(expected);
		}

		if (!StringUtils.equalsIgnoreCase(countryUuid.get(networkcountrymap.get(networkroute)), recipientcountrycode)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_COUNTRY_NO_NETWORK_ERROR
					+ countryUuid.get(networkcountrymap.get(networkroute)));

			return g.toJson(expected);
		}

		// check if route allows for forex conversion.
		boolean forexstatus = routenetworkuuidmap.get(networkrouteuuid);
		// fetch the operating balance
		double minimumoperatingbalance = routeoperatingbalancemap.get(networkrouteuuid);

		// =============================================================================
		// Test to see if the provided reference number has previously been
		// used.
		// if reference number is a duplicate return duplicate reference number
		// response.
		// =============================================================================

		List referencetest = transactionDAO.getTransactionstatus(referencenumber);

		int size = referencetest.size();

		if (size != 0) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_DUPLICATE_REFERENCE);

			return g.toJson(expected);

		}

		// ###############################################################
		// fetch the countrymsisdn by account then place them in a hashmap
		// ################################################################

		List<CountryMsisdn> countrysourcenumber = countryMsisdnDAO.getCountryMsisdn(account);

		for (CountryMsisdn countrynumber : countrysourcenumber)
			countryMsisdnmap.put(countrynumber.getNetworkUuid(), countrynumber.getMsisdn());

		String sourcemsisdn = countryMsisdnmap.get(networkroute);

		if (StringUtils.isEmpty(sourcemsisdn)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_REMIT_NUMBER);

			return g.toJson(expected);
		}

		// select username password and route credentilas
		apiusername = networkUsernamemap.get(networkroute);

		remiturlss = networkRemitUrlmap.get(networkroute);

		bridgeurl = networkBridgeRemitUrlmap.get(networkroute);

		apipassword = networkPasswordmap.get(networkroute);
		
		networktodatabase = networkroute;

		if (StringUtils.isEmpty(apipassword) || StringUtils.isEmpty(apiusername)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_COUNTRYAUTH_ERROR);

			return g.toJson(expected);
		}

		// check if route suuports forex equates to true
		String booleanforex = "true";

		// originatecurrency = accounttype;
		terminatecurrency = recipientcurrencycode;
		currencypair = originatecurrency + "/" + terminatecurrency;

		if (!forexspreadratemap.containsKey(currencypair)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_FOREX_ERROR);
			return g.toJson(expected);
		}
		// ############################################################
		// Check master balance.
		// MasterAccountBalance masterbalance =
		// accountbalanceDAO.getMasterAccountBalance(account);
		// ###########################################################
		MasterAccountBalance masterbalance = accountbalanceDAO.getMasterAccountBalance(account, originatecurrency);

		try {
			imtmasterbalance = masterbalance.getBalance();
			// #######################################################
			// Introduce a check for Master Balance to prevent one from
			// sending what he/She doesnt have
			// #######################################################

			if (imtmasterbalance <= amount) {
				expected.put("command_status", APIConstants.COMMANDSTATUS_LESS_MASTERBALANCE);
				return g.toJson(expected);
			}

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_NO_MASTERBALANCE);
			return g.toJson(expected);

		}

		if (booleanforex.equalsIgnoreCase(String.valueOf(forexstatus))) {
			// ###########################################################
			// fetch forex Module
			// ###########################################################
			// perform forex validation checks

			double impalausdrate = forexspreadratemap.get(currencypair);

			double baseusdrate = forexmarketratemap.get(currencypair);

			impalaexchangecalculate = impalausdrate;

			baseexchange = baseusdrate;

			convertedamountToWallet = CurrencyConvertUtil.multiplyForex(amount, impalaexchangecalculate);

		} else {
			// means no forex conversion is involved
			convertedamountToWallet = amount;
		}

		// ##################################################
		// for a float based system the below is needed
		// ##################################################

		// fetch the list containing balance by country with the respective
		// balances
		try {

			clientBalances = accountbalanceDAO.getClientBalanceByCountry(account);

		} catch (Exception e) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_NO_BALANCE_COUNTRIES);
			return g.toJson(expected);
		}

		// convert the resultant list to a hashmap.
		balancemap = new LinkedHashMap<>();

		for (ClientAccountBalanceByCountry balance : clientBalances)
			balancemap.put(countryUuid.get(balance.getCountryUuid()), balance.getBalance());
		try {

			countryamount = balancemap.get(recipientcountrycode);

		} catch (Exception e) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_NO_BALANCE);
			return g.toJson(expected);

		}

		// confirm to make sure that the balance does not move below a
		// stipulated threshhold per route
		if (countryamount <= minimumoperatingbalance) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_LOW_BALANCEBYCOUNTRY);
			return g.toJson(expected);

		}

		if (countryamount <= convertedamountToWallet) {
			expected.put("command_status", APIConstants.STATUS_CODE_INSUFFICIENT_BALANCE);
			return g.toJson(expected);
		}

		// final converted amount(to be sent to the wallet).
		// finalconvertedamount =
		// CurrencyConvertUtil.round2(convertedamountToWallet, 0);

		finalconvertedamount = CurrencyConvertUtil.doubleToInteger(convertedamountToWallet);

		double remainderamount = CurrencyConvertUtil.subtractDeficit(convertedamountToWallet, finalconvertedamount);

		Country countrys = new Country();
		countrys.setUuid(countrycodetodb);

		// convert amount from double to string
		amountstring = String.valueOf(finalconvertedamount);

		// generate UUID then save transaction and sending to comviva wallet.
		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		// generate UUID then save transaction and sending to
		// transactionhistory.
		transactionforexhistoryuuid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		// #############################################################################################
		// construct a Mega-Json Object to route-transactions to internal
		// Servlet routing transactions.
		// #############################################################################################
		creditrequest = new JsonObject();

		// construct array to addd user information.
		// JsonObject senderdataset = new JsonObject();
		creditrequest.addProperty("username", apiusername);
		creditrequest.addProperty("password", apipassword);
		creditrequest.addProperty("sendingIMT", username);
		creditrequest.addProperty("transaction_id", transactioinid);
		creditrequest.addProperty("sourcecountrycode", sourcecountrycode);
		creditrequest.addProperty("recipientcurrencycode", recipientcurrencycode);
		creditrequest.addProperty("recipientcountrycode", recipientcountrycode);
		creditrequest.addProperty("source_msisdn", sourcemsisdn);
		creditrequest.addProperty("beneficiary_msisdn", recipientmobile);
		creditrequest.addProperty("Sender_Name", sendername);
		creditrequest.addProperty("amount", amountstring);
		creditrequest.addProperty("url", remiturlss);
		// creditrequest.addProperty("bridgeurl",bridgeurl);

		if (vendorfields != null) {
			creditrequest.add("vendor_uniquefields", vendorfields);
		}

		String jsonData = g.toJson(creditrequest);

		if (StringUtils.isNotEmpty(remiturlss)) {

			// assign the remit url from properties.config
			// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");
			CLIENT_URL = bridgeurl;

			postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonData);
			
			logger.error(".....................................................");
			logger.error("AIRTIME TRANSFER REQUEST TO BRIDGE ");
			logger.error(".....................................................");
			logger.error("AIRTIME TRANSFER REQUEST TO BRIDGE URI " + CLIENT_URL + "\n");
			logger.error("AIRTIME TRANSFER REQUEST TO BRIDGE REQUEST OBJECT " + jsonData + "\n");

			try {
				// capture the switch respoinse.
				responseobject = postMinusThread.doPost();
				// pass the returned json string
				roots = new JsonParser().parse(responseobject);

				// exctract a specific json element from the object(status_code)
				switchresponse = roots.getAsJsonObject().get("status_code").getAsString();

				// exctract a specific json element from the object(status_code)
				statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

				receiveruuid = roots.getAsJsonObject().get("am_referenceid").getAsString();
			} catch (Exception e) {
				logger.equals("Airtime Transaction id " + transactioinid
						+ " was complete failure investigate immediately(might be timeout issue)");
				// switchresponse = "00032";
				switchresponse = "S001";
				statusdescription = "CREDIT_INPROGRESS";

			}

			if (!transactionStatusHash.containsKey(switchresponse)) {
				switchresponse = "00032";
				statusdescription = "UNKNOWN_ERROR";
			}

			// set the status UUID
			statusuuid = transactionStatusHash.get(switchresponse);

			success = "S000";

			inprogress = "S001";

			// the account UUID
			accountuuid = account.getUuid();

			unifiedstatusdescription = statusDescriptionHash.get(switchresponse);

			Transaction saved = new Transaction();

			TransactiontransferType transfertypetrans = new TransactiontransferType();

			// server time
			Date now = new Date();

			saved.setUuid(transactioinid);
			saved.setAccountUuid(accountuuid);
			saved.setSourceCountrycode(sourcecountrycode);
			saved.setSenderName(sendername);
			saved.setRecipientMobile(recipientmobile);
			saved.setAmount(finalconvertedamount);
			saved.setCurrencyCode(recipientcurrencycode);
			saved.setRecipientCountryUuid(countrycodetodb);
			saved.setSenderToken(sendertoken);
			saved.setClientTime(clienttime);
			saved.setServerTime(now);
			saved.setTransactionStatusUuid(statusuuid);
			saved.setReferenceNumber(referencenumber);
			saved.setReceivertransactionUuid(receiveruuid);
			saved.setNetworkuuid(networktodatabase);

			// the extra details for mobilepayment transactions.
			transfertypetrans.setUuid(transactionforexhistoryuuid);
			transfertypetrans.setTransactionUuid(transactioinid);
			transfertypetrans.setTransactiontypeUuid(SessionConstants.AIRTIME_TRANSFER);// billpaymenttransfer
			transfertypetrans.setSenderfirstname(sendername);
			transfertypetrans.setSenderlastname(sendername);
			transfertypetrans.setRecipientmobile(recipientmobile);
			transfertypetrans.setSourcecountrycode(sourcecountrycode);
			transfertypetrans.setRecipientaccount(recipientmobile);
			transfertypetrans.setServertime(now);

			// testing to see if adding of transaction is successful(it's
			// failing to return true)
			if (!transactionDAO.addTransaction(saved) || !transactiontypeDAO.addTransactionType(transfertypetrans)) {
				expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL);
				String jsonResult = g.toJson(expected);
				return jsonResult;
			}

			// response when the transaction is a success to deduct balance.
			TransactionForexrate transactionratehistory = new TransactionForexrate();

			transactionratehistory.setUuid(transactionforexhistoryuuid);
			transactionratehistory.setTransactionUuid(transactioinid);
			transactionratehistory.setAccount(accountuuid);
			transactionratehistory.setRecipientcountry(countrycodetodb);
			transactionratehistory.setLocalamount(amount);
			transactionratehistory.setAccounttype(originatecurrency);
			transactionratehistory.setConvertedamount(finalconvertedamount);
			transactionratehistory.setImpalarate(impalaexchangecalculate);
			transactionratehistory.setBaserate(baseexchange);
			transactionratehistory.setReceivermsisdn(recipientmobile);
			transactionratehistory.setSurplus(remainderamount);
			transactionratehistory.setServerTime(now);

			if (switchresponse.equalsIgnoreCase(success)) {

				new TransactionDispatcher(saved, transactionratehistory).start();
				expected.put("api_username", username);
				expected.put("transaction_id", transactioinid);
				expected.put("command_status", APIConstants.COMMANDSTATUS_OK);
				expected.put("status_code", switchresponse);
				expected.put("remit_status", unifiedstatusdescription);
				String jsonResult = g.toJson(expected);

				return jsonResult;

			}

			if (switchresponse.equalsIgnoreCase(inprogress)) {

				ThirdPartyReference thirdreference = new ThirdPartyReference();
				thirdreference.setTransactionuuid(transactioinid);
				thirdreference.setReferencenumber(receiveruuid);
				thirdreference.setServerTime(now);
				thirdreference.setUuid(transactioinid);
				// *************************************************************
				// Save the Transaction ForexRate
				// *************************************************************
				new AsyncTransactionDispatcher(transactionratehistory).start();
				expected.put("api_username", username);
				expected.put("transaction_id", transactioinid);
				expected.put("command_status", APIConstants.COMMANDSTATUS_OK);
				expected.put("status_code", switchresponse);
				expected.put("remit_status", unifiedstatusdescription);
				String jsonResult = g.toJson(expected);
				return jsonResult;

			}

			// response to be returned if transaction fails
			expected.put("api_username", username);
			expected.put("transaction_id", transactioinid);
			expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL);
			expected.put("status_code", switchresponse);
			expected.put("remit_status", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}

		expected.put("api_username", username);
		expected.put("command_status", APIConstants.COMMANDSTATUS_UNOPERATIONAL_COUNTRY);
		String jsonResult = g.toJson(expected);
		return jsonResult;
		
		

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.collection.accountmgmt.admin.pagination.collectiondefine;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import com.impalapay.beans.route.RouteDefine;
import com.impalapay.collection.beans.network.CollectionDefine;

public class TestCollectionDefinePaginator {

	final String DB_NAME = "airteldblive";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "airtellive";
	final String DB_PASSWD = "ThejKoyb34";
	final int DB_PORT = 5432;

	/**
	 * Test method for getting firstpage
	 */
	@Ignore
	@Test
	public void testGetFirstPage() {
		CollectionDefinePaginator topupPaginator = new CollectionDefinePaginator(DB_NAME, DB_HOST, DB_USERNAME,
				DB_PASSWD, DB_PORT);

		CollectionDefinePage firstPage = topupPaginator.getFirstPage();
		List<CollectionDefine> topupList = firstPage.getContents();
		assertEquals(topupList.size(), CollectionDefinePaginator.PAGESIZE);

		for (CollectionDefine s : topupList) {
			System.out.println(s);
		}
	}

	/**
	 * Test method for getting lastpage
	 */
	@Ignore
	@Test
	public void testGetLastPage() {
		CollectionDefinePaginator topupPaginator = new CollectionDefinePaginator(DB_NAME, DB_HOST, DB_USERNAME,
				DB_PASSWD, DB_PORT);

		CollectionDefinePage firstPage = topupPaginator.getLastPage();
		List<CollectionDefine> topupList = firstPage.getContents();

		for (CollectionDefine s : topupList) {
			System.out.println(s);
		}
	}

	// @Ignore
	@Test
	public void testGetNextPage() {
		CollectionDefinePaginator paginator = new CollectionDefinePaginator(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD,
				DB_PORT);
		int currentPageNum = 1;

		CollectionDefinePage page = new CollectionDefinePage(currentPageNum, 1, CollectionDefinePaginator.PAGESIZE,
				new ArrayList<CollectionDefine>());
		CollectionDefinePage nextPage = paginator.getNextPage(page);
		List<CollectionDefine> topupList = nextPage.getContents();

		assertEquals(topupList.size(), CollectionDefinePaginator.PAGESIZE);

		for (CollectionDefine s : topupList) {
			System.out.println(s);
		}
	}

}

package com.impalapay.collection.accountmgmt.admin.pagination.transaction;

import java.util.List;

import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.collection.accountmgmt.pagination.transaction.CollectionPage;
import com.impalapay.collection.accountmgmt.pagination.transaction.CollectionPaginating;
import com.impalapay.collection.beans.incoming.ProcessedCollection;
import com.impalapay.collection.persistence.processedtransactions.ProcessedTransactionDAOImpl;

public class SearchOriginateUuidPaginator implements CollectionPaginating {

	public static final int PAGESIZE = 15; // The number of Transactions to display per page
	private CountUtils countUtils;
	private ProcessedTransactionDAOImpl transactionDAO;
	private String originateuuid;

	/**
	 * Disable the default constructor
	 */
	public SearchOriginateUuidPaginator() {
	}

	/**
	 *
	 * @param username
	 */
	public SearchOriginateUuidPaginator(String originateuuid) {

		this.originateuuid = originateuuid;
		countUtils = CountUtils.getInstance();
		transactionDAO = ProcessedTransactionDAOImpl.getInstance();

	}

	/**
	 *
	 * @param username
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public SearchOriginateUuidPaginator(String originateuuid, String dbName, String dbHost, String dbUsername,
			String dbPasswd, int dbPort) {
		// this.username = username;
		this.originateuuid = originateuuid;

		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);

		transactionDAO = new ProcessedTransactionDAOImpl(dbName, dbHost, dbUsername, dbPasswd, dbPort);

	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getFirstPage()
	 */
	@Override
	public CollectionPage getFirstPage() {
		CollectionPage result = new CollectionPage();
		List<ProcessedCollection> transactionList;

		transactionList = transactionDAO.getAllCollectionByOriginateUuid(originateuuid, 0, PAGESIZE);

		result = new CollectionPage(1, getTotalPage(), PAGESIZE, transactionList);

		return result;
	}

	/**
	 *
	 * @return TransactionPage
	 */
	@Override
	public CollectionPage getLastPage() {
		int transactionCount, startIndex;
		int totalPage = getTotalPage();

		CollectionPage result = new CollectionPage();
		List<ProcessedCollection> transactionList;

		startIndex = (totalPage - 1) * PAGESIZE;

		transactionCount = countUtils.getAllCollectionByOriginateUuidCount(originateuuid);

		transactionList = transactionDAO.getAllCollectionByOriginateUuid(originateuuid, startIndex, transactionCount);

		result = new CollectionPage(totalPage, totalPage, PAGESIZE, transactionList);

		return result;
	}

	/**
	 *
	 * @return TransactionPage
	 */
	@Override
	public CollectionPage getNextPage(CollectionPage currentPage) {
		int totalPage = getTotalPage();

		CollectionPage result = new CollectionPage();
		List<ProcessedCollection> transactionList;

		transactionList = transactionDAO.getAllCollectionByOriginateUuid(originateuuid, currentPage.getPageNum() * PAGESIZE,
				(currentPage.getPageNum() * PAGESIZE) + PAGESIZE);

		result = new CollectionPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, transactionList);

		return result;
	}

	/**
	 *
	 * @return TransactionPage
	 */
	@Override
	public CollectionPage getPrevPage(CollectionPage currentPage) {
		int totalPage = getTotalPage();

		CollectionPage result = new CollectionPage();
		List<ProcessedCollection> transactionList;

		transactionList = transactionDAO.getAllCollectionByOriginateUuid(originateuuid,
				(currentPage.getPageNum() - 2) * PAGESIZE, (currentPage.getPageNum() - 1) * PAGESIZE);

		result = new CollectionPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, transactionList);

		return result;
	}

	/**
	 *
	 * @return int
	 */
	public int getTotalPage() {

		int totalSize = 0;

		totalSize = countUtils.getAllCollectionByOriginateUuidCount(originateuuid);

		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

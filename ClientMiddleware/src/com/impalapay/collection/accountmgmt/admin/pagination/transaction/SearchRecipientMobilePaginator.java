package com.impalapay.collection.accountmgmt.admin.pagination.transaction;

import java.util.List;

import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.collection.accountmgmt.pagination.transaction.CollectionPage;
import com.impalapay.collection.accountmgmt.pagination.transaction.CollectionPaginating;
import com.impalapay.collection.beans.incoming.ProcessedCollection;
import com.impalapay.collection.persistence.processedtransactions.ProcessedTransactionDAOImpl;

public class SearchRecipientMobilePaginator implements CollectionPaginating {

	public static final int PAGESIZE = 15; // The number of Transactions to display per page
	private CountUtils countUtils;
	private ProcessedTransactionDAOImpl transactionDAO;
	private String phone;

	/**
	 * Disable the default constructor
	 */
	public SearchRecipientMobilePaginator() {
	}

	/**
	 *
	 * @param username
	 */
	public SearchRecipientMobilePaginator(String phone) {

		this.phone = phone;
		countUtils = CountUtils.getInstance();
		transactionDAO = ProcessedTransactionDAOImpl.getInstance();

	}

	/**
	 *
	 * @param username
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public SearchRecipientMobilePaginator(String phone, String dbName, String dbHost, String dbUsername,
			String dbPasswd, int dbPort) {
		// this.username = username;
		this.phone = phone;

		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);

		transactionDAO = new ProcessedTransactionDAOImpl(dbName, dbHost, dbUsername, dbPasswd, dbPort);

	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getFirstPage()
	 */
	@Override
	public CollectionPage getFirstPage() {
		CollectionPage result = new CollectionPage();
		List<ProcessedCollection> transactionList;

		transactionList = transactionDAO.getAllCollectionByReceiverPhone(phone, 0, PAGESIZE);

		result = new CollectionPage(1, getTotalPage(), PAGESIZE, transactionList);

		return result;
	}

	/**
	 *
	 * @return TransactionPage
	 */
	@Override
	public CollectionPage getLastPage() {
		int transactionCount, startIndex;
		int totalPage = getTotalPage();

		CollectionPage result = new CollectionPage();
		List<ProcessedCollection> transactionList;

		startIndex = (totalPage - 1) * PAGESIZE;

		transactionCount = countUtils.getAllCollectionByDebitedAccountCount(phone);

		transactionList = transactionDAO.getAllCollectionByReceiverPhone(phone, startIndex, transactionCount);

		result = new CollectionPage(totalPage, totalPage, PAGESIZE, transactionList);

		return result;
	}

	/**
	 *
	 * @return TransactionPage
	 */
	@Override
	public CollectionPage getNextPage(CollectionPage currentPage) {
		int totalPage = getTotalPage();

		CollectionPage result = new CollectionPage();
		List<ProcessedCollection> transactionList;

		transactionList = transactionDAO.getAllCollectionByReceiverPhone(phone, currentPage.getPageNum() * PAGESIZE,
				(currentPage.getPageNum() * PAGESIZE) + PAGESIZE);

		result = new CollectionPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, transactionList);

		return result;
	}

	/**
	 *
	 * @return TransactionPage
	 */
	@Override
	public CollectionPage getPrevPage(CollectionPage currentPage) {
		int totalPage = getTotalPage();

		CollectionPage result = new CollectionPage();
		List<ProcessedCollection> transactionList;

		transactionList = transactionDAO.getAllCollectionByReceiverPhone(phone,
				(currentPage.getPageNum() - 2) * PAGESIZE, (currentPage.getPageNum() - 1) * PAGESIZE);

		result = new CollectionPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, transactionList);

		return result;
	}

	/**
	 *
	 * @return int
	 */
	public int getTotalPage() {

		int totalSize = 0;

		totalSize = countUtils.getAllCollectionByDebitedAccountCount(phone);

		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

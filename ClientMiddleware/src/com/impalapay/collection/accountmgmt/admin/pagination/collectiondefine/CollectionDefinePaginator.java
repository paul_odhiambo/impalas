/**
 * Copyright 2015 Tawi Commercial Services Ltd
 * 
 * Licensed under the Open Software License, Version 3.0  you may
 * not use this file except in compliance with the License. You may obtain a copy
 * of the License at:
 * http://opensource.org/licenses/OSL-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an AS IS BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 * 
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package com.impalapay.collection.accountmgmt.admin.pagination.collectiondefine;

import java.util.List;
import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.collection.beans.network.CollectionDefine;
import com.impalapay.collection.persistence.network.CollectionDefineDAOImpl;

/**
 * Paginate an Inbox HTML view.
 * <p>
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 */
public class CollectionDefinePaginator {

	/**
	 *
	 */
	// public static final int PAGESIZE = 15; // The number of Incoming SMS to
	// display per page
	public static final int PAGESIZE = 13;
	private final CollectionDefineDAOImpl collectiondefineDAO;
	private final CountUtils countUtils;

	/**
	 *
	 * @param accountuuid
	 */
	public CollectionDefinePaginator() {

		countUtils = CountUtils.getInstance();
		collectiondefineDAO = CollectionDefineDAOImpl.getInstance();

	}

	/**
	 *
	 * @param email
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public CollectionDefinePaginator(String dbName, String dbHost, String dbUsername, String dbPasswd, int dbPort) {

		// initialize the DAOs
		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);
		collectiondefineDAO = new CollectionDefineDAOImpl(dbName, dbHost, dbUsername, dbPasswd, dbPort);
	}

	/**
	 *
	 * @return
	 */
	public CollectionDefinePage getFirstPage() {

		CollectionDefinePage page = new CollectionDefinePage();

		List<CollectionDefine> accountList = collectiondefineDAO.getAllCollectionRoute(0, PAGESIZE);

		page = new CollectionDefinePage(1, getTotalPage(), PAGESIZE, accountList);
		// result = new IncomingSMSPage (1, getTotalPage(), PAGESIZE, smsList);

		return page;
	}

	/**
	 * Provides the last page of the accounts report
	 *
	 * @return accounts page
	 */
	public CollectionDefinePage getLastPage() {
		CollectionDefinePage page = new CollectionDefinePage();

		List<CollectionDefine> accountList = null;
		int accountCount, startIndex;
		int totalPage = getTotalPage();

		startIndex = (totalPage - 1) * PAGESIZE;
		accountCount = countUtils.getAllCollectionDefineCount();

		accountList = collectiondefineDAO.getAllCollectionRoute(startIndex, accountCount);

		page = new CollectionDefinePage(totalPage, totalPage, PAGESIZE, accountList);

		return page;
	}

	/**
	 * Moves you forward to the page of the Accounts that comes after the current
	 * page
	 *
	 * @param currentPage
	 * @return an Account page
	 */
	public CollectionDefinePage getNextPage(final CollectionDefinePage currentPage) {
		int totalPage = getTotalPage();

		CollectionDefinePage page = new CollectionDefinePage();

		List<CollectionDefine> accountList = collectiondefineDAO.getAllCollectionRoute(
				currentPage.getPageNum() * PAGESIZE, ((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		page = new CollectionDefinePage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, accountList);

		return page;
	}

	/**
	 * Moves you backward to the page of the Accounts that comes before the current
	 * page
	 *
	 * @param currentPage
	 * @return an Account page
	 */
	public CollectionDefinePage getPrevPage(final CollectionDefinePage currentPage) {
		int totalPage = getTotalPage();

		CollectionDefinePage page = new CollectionDefinePage();

		List<CollectionDefine> accountList = collectiondefineDAO.getAllCollectionRoute(
				(currentPage.getPageNum() - 2) * PAGESIZE, ((currentPage.getPageNum() - 1) * PAGESIZE));

		page = new CollectionDefinePage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, accountList);

		return page;
	}

	/**
	 * Calculates the total number of pages that would be printed for the Account
	 * sessions that belong to the logged-in account
	 *
	 * @return an integer
	 */
	public int getTotalPage() {
		int totalSize = 0;

		// get the number of all sessions belonging to this email
		totalSize = countUtils.getAllCollectionDefineCount();

		// TODO: divide by the page size and add one to take care of remainders
		// and what else?
		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

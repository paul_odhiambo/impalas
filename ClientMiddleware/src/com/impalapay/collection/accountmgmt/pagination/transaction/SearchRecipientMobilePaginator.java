package com.impalapay.collection.accountmgmt.pagination.transaction;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.persistence.accountmgmt.AccountDAO;
import com.impalapay.airtel.persistence.util.CountUtils;
import com.impalapay.collection.beans.incoming.ProcessedCollection;
import com.impalapay.collection.persistence.processedtransactions.ProcessedTransactionDAOImpl;

public class SearchRecipientMobilePaginator implements CollectionPaginating {

	public static final int PAGESIZE = 15; // The number of Transactions to display per page
	private Account account;
	private CountUtils countUtils;
	private AccountDAO accountDAO;
	private ProcessedTransactionDAOImpl processedcollectionDAO;
	private String username;
	private String phone;

	/**
	 * Disable the default constructor
	 */
	public SearchRecipientMobilePaginator() {
	}

	/**
	 *
	 * @param username
	 */
	public SearchRecipientMobilePaginator(String username, String phone) {

		this.username = username;
		this.phone = phone;

		countUtils = CountUtils.getInstance();

		accountDAO = AccountDAO.getInstance();

		account = accountDAO.getAccountName(username);

		processedcollectionDAO = ProcessedTransactionDAOImpl.getInstance();

	}

	/**
	 *
	 * @param username
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public SearchRecipientMobilePaginator(String username, String phone, String dbName, String dbHost,
			String dbUsername, String dbPasswd, int dbPort) {
		this.username = username;
		this.phone = phone;

		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);

		processedcollectionDAO = new ProcessedTransactionDAOImpl(dbName, dbHost, dbUsername, dbPasswd, dbPort);

		accountDAO = new AccountDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);

		account = accountDAO.getAccountName(username);

	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getFirstPage()
	 */
	@Override
	public CollectionPage getFirstPage() {
		CollectionPage result = new CollectionPage();
		List<ProcessedCollection> transactionList;

		transactionList = processedcollectionDAO.getTransactionByDebitorAccount(account, phone, 0, PAGESIZE);

		result = new CollectionPage(1, getTotalPage(), PAGESIZE, transactionList);

		return result;
	}

	/**
	 *
	 * @return TransactionPage
	 */
	@Override
	public CollectionPage getLastPage() {
		int transactionCount, startIndex;
		int totalPage = getTotalPage();

		CollectionPage result = new CollectionPage();
		List<ProcessedCollection> transactionList;

		startIndex = (totalPage - 1) * PAGESIZE;

		transactionCount = countUtils.getCollectionByDebitAccountCount(account, phone);

		transactionList = processedcollectionDAO.getTransactionByDebitorAccount(account, phone, startIndex, transactionCount);

		result = new CollectionPage(totalPage, totalPage, PAGESIZE, transactionList);

		return result;
	}

	/**
	 *
	 * @return TransactionPage
	 */
	@Override
	public CollectionPage getNextPage(CollectionPage currentPage) {
		int totalPage = getTotalPage();

		CollectionPage result = new CollectionPage();
		List<ProcessedCollection> transactionList;

		transactionList = processedcollectionDAO.getTransactionByDebitorAccount(account, phone,
				currentPage.getPageNum() * PAGESIZE, (currentPage.getPageNum() * PAGESIZE) + PAGESIZE);

		result = new CollectionPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, transactionList);

		return result;
	}

	/**
	 *
	 * @return TransactionPage
	 */
	@Override
	public CollectionPage getPrevPage(CollectionPage currentPage) {
		int totalPage = getTotalPage();

		CollectionPage result = new CollectionPage();
		List<ProcessedCollection> transactionList;

		transactionList = processedcollectionDAO.getTransactionByDebitorAccount(account, phone,
				(currentPage.getPageNum() - 2) * PAGESIZE, (currentPage.getPageNum() - 1) * PAGESIZE);

		result = new CollectionPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, transactionList);

		return result;
	}

	/**
	 *
	 * @return int
	 */
	public int getTotalPage() {

		int totalSize = 0;

		totalSize = countUtils.getCollectionByDebitAccountCount(account, phone);

		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

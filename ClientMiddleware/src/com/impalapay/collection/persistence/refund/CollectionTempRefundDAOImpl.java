package com.impalapay.collection.persistence.refund;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.collection.beans.refund.CollectionRefund;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

public class CollectionTempRefundDAOImpl extends GenericDAO implements CollectionTempRefundDAO {

	public static CollectionTempRefundDAOImpl collectionbalanceDAOImpl;

	private Logger logger;

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return {@link TransactionDAO}
	 */
	public static CollectionTempRefundDAOImpl getInstance() {

		if (collectionbalanceDAOImpl == null) {
			collectionbalanceDAOImpl = new CollectionTempRefundDAOImpl();
		}

		return collectionbalanceDAOImpl;
	}

	/**
	 * 
	 */
	public CollectionTempRefundDAOImpl() {
		super();

		logger = Logger.getLogger(this.getClass());
	}

	/**
	 * 
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public CollectionTempRefundDAOImpl(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

		logger = Logger.getLogger(this.getClass());
	}

	@Override
	public boolean putCollectionTempRefund(CollectionRefund collectionrefund) {
		boolean success = true;


		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn
					.prepareStatement("INSERT INTO temp_collectionrefund(uuid,accountuuid,"
							+ "refundreason,currencyuuid,amount,comission,comissioncurrencyuuid,"
							+ "adminextrainformation,transactionstatusuuid,authorisedmaker,deductamount,referencenumber,"
							+ "dateadded) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, collectionrefund.getUuid());
			pstmt.setString(2, collectionrefund.getAccountuuid());
			pstmt.setString(3, collectionrefund.getRefundreason());
			pstmt.setString(4, collectionrefund.getCurrencyUuid());
			pstmt.setDouble(5, collectionrefund.getAmount());
			pstmt.setDouble(6, collectionrefund.getComission());
			pstmt.setString(7, collectionrefund.getComissioncurrencyUuid());
			pstmt.setString(8, collectionrefund.getAdminextrainformation());
			pstmt.setString(9, collectionrefund.getTransactionStatusUuid());
			pstmt.setString(10, collectionrefund.getAuthorisedmaker());
			pstmt.setDouble(11, collectionrefund.getDeductamount());
			pstmt.setString(12, collectionrefund.getReferencenumber());
			//pstmt.setString(13, collectionrefund.getReceivertransactionuuid());
			pstmt.setTimestamp(13, new Timestamp(collectionrefund.getDateadded().getTime()));

			pstmt.execute();
				

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + collectionrefund);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<CollectionRefund> getAllCollectionTempRefund(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
				List<CollectionRefund> list = new ArrayList<>();

				Connection conn = null;
				PreparedStatement pstmt = null;
				ResultSet rset = null;

				try {
					conn = dbCredentials.getConnection();
					pstmt = conn
							.prepareStatement("SELECT * FROM temp_collectionrefund ORDER BY accountuuid DESC LIMIT ? OFFSET ?;");
					pstmt.setInt(1, toIndex - fromIndex);
					pstmt.setInt(2, fromIndex);

					rset = pstmt.executeQuery();

					list = beanProcessor.toBeanList(rset, CollectionRefund.class);

				} catch (SQLException e) {
					logger.error("SQLException exception while getting all temp_collectionrefund from index " + fromIndex
							+ " to index " + toIndex);
					logger.error(ExceptionUtils.getStackTrace(e));

				} finally {
					if (rset != null) {
						try {
							rset.close();
						} catch (SQLException e) {
						}
					}

					if (pstmt != null) {
						try {
							pstmt.close();
						} catch (SQLException e) {
						}
					}

					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
						}
					}
				}

				return list;
	}

	@Override
	public List<CollectionRefund> getAllCollectionTempRefund(Account account, int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		List<CollectionRefund> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM temp_collectionrefund WHERE accountuuid=? ORDER BY accountuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setString(1, account.getUuid());
			pstmt.setInt(2, toIndex - fromIndex);
			pstmt.setInt(3, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CollectionRefund.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all temp_collectionrefund from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public CollectionRefund getCollectionTempRefund(String uuid) {
		// TODO Auto-generated method stub
		CollectionRefund s = null;

				Connection conn = null;
				PreparedStatement pstmt = null;
				ResultSet rset = null;

				try {
					conn = dbCredentials.getConnection();
					pstmt = conn.prepareStatement("SELECT * FROM temp_collectionrefund WHERE uuid = ?;");
					pstmt.setString(1, uuid);

					rset = pstmt.executeQuery();

					if (rset.next()) {
						s = beanProcessor.toBean(rset, CollectionRefund.class);
						// s.setId(rset.getInt("id"));
					}

				} catch (SQLException e) {
					logger.error("SQLException while getting temp_collectionrefund with uuid '" + uuid + "'");
					logger.error(ExceptionUtils.getStackTrace(e));

				} finally {
					if (rset != null) {
						try {
							rset.close();
						} catch (SQLException e) {
						}
					}

					if (pstmt != null) {
						try {
							pstmt.close();
						} catch (SQLException e) {
						}
					}

					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
						}
					}
				}

				return s;
	}

	@Override
	public boolean deleteCollectionTempRefund(String uuid) {
		boolean success = true;
		Date date;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("DELETE FROM temp_collectionrefund WHERE uuid=?;");

			pstmt.setString(1, uuid);

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while Deleting " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<CollectionRefund> getCollectionTempRefundByStatusUuid(TransactionStatus transactionStatus, int limit) {
		List<CollectionRefund> list = new LinkedList<>();
		CollectionRefund transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM temp_collectionrefund WHERE transactionstatusuuid=? ORDER BY RANDOM() ASC LIMIT ?;");
			pstmt.setString(1, transactionStatus.getUuid());
			pstmt.setInt(2, limit);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transaction = beanProcessor.toBean(rset, CollectionRefund.class);

				list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error(
					"SQL exception while fetching temp_collectionrefund with transactionstatus" + transactionStatus);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	
}

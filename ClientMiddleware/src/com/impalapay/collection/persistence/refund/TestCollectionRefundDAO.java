package com.impalapay.collection.persistence.refund;

import com.impalapay.collection.beans.balance.CollectionBalanceHistory;
import com.impalapay.collection.beans.refund.CollectionRefund;

import java.util.Date;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

/**
 * Tests the {@link AccountBalanceDAO}
 * <p>
 * Copyright (c) ImpalaPay LTD., Sep 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class TestCollectionRefundDAO {

	final String DB_NAME = "remittancedb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "remittance";
	final String DB_PASSWD = "gertedNen2";
	final int DB_PORT = 5432;

	// Account holders' Uuids
	public static final String DEMO = "9756f889-811a-4a94-b13d-1c66c7655a7f";

	final String CLIENTBALANCE_UUID = "61a86ead-98a4-4bc6-b00f-3028e61abc69";
	final String CLIENTBALANCE_ACCOUNT_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String Country_UUID2 = "5db5fa02790e4ee0a8d7a538b4df820a";

	final int CLIENTBALANCE_AMOUNT = 48_079_021;

	final int ALL_CLIENTS_BALANCE_COUNT = 150;
	final double AMOUNT = 10000;
	final int AMOUNT2 = 46271257;

	private CollectionRefundDAOImpl storage = new CollectionRefundDAOImpl(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD,
			DB_PORT);

	// @Ignore
	@Test
	public void testCollectionBalance() {

		CollectionRefund newbalance = new CollectionRefund();

		newbalance.setUuid(CLIENTBALANCE_UUID);
		newbalance.setAccountuuid(DEMO);
		newbalance.setReferencenumber("512145454527845");
		newbalance.setRefundreason("GOODS NOT RECEIVED");
		newbalance.setCurrencyUuid("d4a676822f4546a0bee789e83070f788");
		newbalance.setAmount(100);
		newbalance.setDateadded(new Date());

		
		assertTrue(storage.putCollectionRefund(newbalance));
	}

}

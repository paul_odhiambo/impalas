package com.impalapay.collection.persistence.refund;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.collection.beans.incoming.CollectionUpdateStatus;

public class CollectionUpdateStatusDAOImpl extends GenericDAO implements CollectionUpdateStatusDAO {

	public static CollectionUpdateStatusDAOImpl transactionforexDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	public static CollectionUpdateStatusDAOImpl getinstance() {
		if (transactionforexDAO == null) {
			transactionforexDAO = new CollectionUpdateStatusDAOImpl();
		}

		return transactionforexDAO;

	}

	public CollectionUpdateStatusDAOImpl() {
		super();
	}

	/**
	 * 
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public CollectionUpdateStatusDAOImpl(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);
	}

	@Override
	public List<CollectionUpdateStatus> getAllCollectionUpdateStatus() {
		// TODO Auto-generated method stub
		List<CollectionUpdateStatus> list = new LinkedList<>();

		CollectionUpdateStatus transactionupdatestatus = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collectionupdatestatus;");

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transactionupdatestatus = beanProcessor.toBean(rset, CollectionUpdateStatus.class);
				// transaction.setId(rset.getInt("id"));
				list.add(transactionupdatestatus);

			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collectionupdatestatus for update");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<CollectionUpdateStatus> getAllCollectionUpdateStatus(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		List<CollectionUpdateStatus> list = new LinkedList<>();

		// Transaction transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM collectionupdatestatus ORDER BY  servertime DESC LIMIT ? OFFSET ? ;");

			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				list = beanProcessor.toBeanList(rset, CollectionUpdateStatus.class);
				
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting collectionupdatestatus with uuid  from " + fromIndex
					+ " to " + toIndex);
			logger.error(e.toString());

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public boolean addCollectionUpdateStatus(CollectionUpdateStatus transactionupdatestatus) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO collectionupdatestatus (uuid,originatetransactionuuid,debitcurrency, originatecurrency ,originateamount, "
							+ "amount,currentstatus, updatestatus,creditaccountuuid,servertime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, transactionupdatestatus.getUuid());
			pstmt.setString(2, transactionupdatestatus.getOriginatetransactionuuid());
			pstmt.setString(3, transactionupdatestatus.getDebitcurrency());
			pstmt.setString(4, transactionupdatestatus.getOriginatecurrency());
			pstmt.setDouble(5, transactionupdatestatus.getOriginateamount());
			pstmt.setDouble(6, transactionupdatestatus.getAmount());
			pstmt.setString(7, transactionupdatestatus.getCurrentstatus());
			pstmt.setString(8, transactionupdatestatus.getUpdatestatus());
			pstmt.setString(9, transactionupdatestatus.getCreditaccountuuid());
			pstmt.setTimestamp(10, new Timestamp(transactionupdatestatus.getServertime().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + transactionupdatestatus);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;

	}

	@Override
	public boolean deleteCollectionUpdateStatus(String transactionid) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("DELETE FROM collectionupdatestatus WHERE originatetransactionuuid=?;");

			pstmt.setString(1, transactionid);

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while deleting " + transactionid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}
    //in this case the receiver uuid reprents the processed transaction uuid
	@Override
	public CollectionUpdateStatus getCollectionUpdateStatusUuid(String transactionuuid) {
		CollectionUpdateStatus transactionforexrate = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collectionupdatestatus WHERE originatetransactionuuid=?;");
			pstmt.setString(1, transactionuuid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transactionforexrate = beanProcessor.toBean(rset, CollectionUpdateStatus.class);

			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching collectionupdatestatus with originatetransactionuuid" + transactionuuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return transactionforexrate;
	}

	@Override
	public boolean addCollectionUpdateStatusHistory(CollectionUpdateStatus transactionupdatetable) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO collectionupdatestatushistory (uuid,originatetransactionuuid,debitcurrency, originatecurrency ,originateamount, "
							+ "amount, currentstatus, updatestatus,creditaccountuuid, servertime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, transactionupdatetable.getUuid());
			pstmt.setString(2, transactionupdatetable.getOriginatetransactionuuid());
			pstmt.setString(3, transactionupdatetable.getDebitcurrency());
			pstmt.setString(4, transactionupdatetable.getOriginatecurrency());
			pstmt.setDouble(5, transactionupdatetable.getOriginateamount());
			pstmt.setDouble(6, transactionupdatetable.getAmount());
			pstmt.setString(7, transactionupdatetable.getCurrentstatus());
			pstmt.setString(8, transactionupdatetable.getUpdatestatus());
			pstmt.setString(9, transactionupdatetable.getCreditaccountuuid());
			pstmt.setTimestamp(10, new Timestamp(transactionupdatetable.getServertime().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + transactionupdatetable);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;

	}

	@Override
	public List<CollectionUpdateStatus> getAllCollectionUpdateStatusHistory(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		List<CollectionUpdateStatus> list = new LinkedList<>();

		// Transaction transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM collectionupdatestatushistory ORDER BY  servertime DESC LIMIT ? OFFSET ? ;");

			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				list = beanProcessor.toBeanList(rset, CollectionUpdateStatus.class);
				// transaction = b.toBean(rset, Transaction.class);
				// transaction.setId(rset.getInt("sessionid"));

				// list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting transactionupdatestatus with uuid  from " + fromIndex
					+ " to " + toIndex);
			logger.error(e.toString());

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public CollectionUpdateStatus getCollectionUpdateStatusUuidHistory(String transactionuuid) {
		CollectionUpdateStatus transactionforexrate = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collectionupdatestatushistory WHERE originatetransactionuuid=?;");
			pstmt.setString(1, transactionuuid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transactionforexrate = beanProcessor.toBean(rset, CollectionUpdateStatus.class);

			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching collectionupdatestatushistory with originatetransactionuuid"
					+ transactionuuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return transactionforexrate;
	}

	@Override
	public boolean adddeleteprocessedUpdateStatusHistory(CollectionUpdateStatus transactionupdatetable) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO deleteprocessedupdatestatushistory (uuid,originatetransactionuuid,debitcurrency, originatecurrency ,originateamount, "
							+ "amount, currentstatus, updatestatus,creditaccountuuid, servertime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, transactionupdatetable.getUuid());
			pstmt.setString(2, transactionupdatetable.getOriginatetransactionuuid());
			pstmt.setString(3, transactionupdatetable.getDebitcurrency());
			pstmt.setString(4, transactionupdatetable.getOriginatecurrency());
			pstmt.setDouble(5, transactionupdatetable.getOriginateamount());
			pstmt.setDouble(6, transactionupdatetable.getAmount());
			pstmt.setString(7, transactionupdatetable.getCurrentstatus());
			pstmt.setString(8, transactionupdatetable.getUpdatestatus());
			pstmt.setString(9, transactionupdatetable.getCreditaccountuuid());
			pstmt.setTimestamp(10, new Timestamp(transactionupdatetable.getServertime().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + transactionupdatetable);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

}

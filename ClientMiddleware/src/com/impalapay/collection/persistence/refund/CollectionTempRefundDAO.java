package com.impalapay.collection.persistence.refund;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.collection.beans.refund.CollectionRefund;

/**
 * Abstraction for persistence of balance in an the master account.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public interface CollectionTempRefundDAO {

	/**
	 * 
	 * @param collectionbalance
	 * @return
	 */
	//public boolean deductCollectionBalance(CollectionBalanceHistory collectionbalance);
	
	/**
	 * 
	 * @param uuid
	 * @return
	 */
	boolean deleteCollectionTempRefund(String uuid);

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public CollectionRefund getCollectionTempRefund(String uuid);

	/**
	 * 
	 * @param collectionbalance
	 * @return
	 */
	boolean putCollectionTempRefund(CollectionRefund collectinrefund);
	
	/***
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CollectionRefund> getAllCollectionTempRefund(int fromIndex, int toIndex);

	/**
	 * 
	 * @param account
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CollectionRefund> getAllCollectionTempRefund(Account account, int fromIndex, int toIndex);
	
	/**
	 * 
	 * @param transactionStatus
	 * @param limit
	 * @return
	 */
	public List<CollectionRefund> getCollectionTempRefundByStatusUuid(TransactionStatus transactionStatus, int limit);



}

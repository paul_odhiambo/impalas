package com.impalapay.collection.persistence.refund;

import com.impalapay.collection.beans.balance.CollectionBalanceHistory;
import com.impalapay.collection.beans.incoming.CollectionUpdateStatus;
import com.impalapay.collection.beans.refund.CollectionRefund;

import java.util.Date;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

/**
 * Tests the {@link AccountBalanceDAO}
 * <p>
 * Copyright (c) ImpalaPay LTD., Sep 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class TestCollectionUpdateStatusDAO {

	final String DB_NAME = "remittancedb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "switchremittance";
	final String DB_PASSWD = "RhycsOyFrin4";
	final int DB_PORT = 5432;

	// Account holders' Uuids
	public static final String DEMO = "4935a169cf604d7697293f84dc1e313f";

	final String CLIENTBALANCE_UUID = "61a86ead-98a4-4bc6-b00f-3028e61abc69";
	final String CLIENTBALANCE_ACCOUNT_UUID = "4935a169cf604d7697293f84dc1e313f";
	final String Country_UUID2 = "5db5fa02790e4ee0a8d7a538b4df820a";

	final int CLIENTBALANCE_AMOUNT = 48_079_021;

	final int ALL_CLIENTS_BALANCE_COUNT = 150;
	final double AMOUNT = 10000;
	final int AMOUNT2 = 46271257;

	private CollectionUpdateStatusDAOImpl storage = new CollectionUpdateStatusDAOImpl(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD,
			DB_PORT);

	// @Ignore
	@Test
	public void testCollectionBalance() {

		CollectionUpdateStatus newbalance = new CollectionUpdateStatus();

		newbalance.setUuid(CLIENTBALANCE_UUID);
		newbalance.setOriginatetransactionuuid("786865757575757");
		newbalance.setDebitcurrency("d4a676822f4546a0bee789e83070f788");
		newbalance.setOriginatecurrency("fd3248086f2d4e7896b97503719ce91e");
		newbalance.setAmount(AMOUNT);
		newbalance.setOriginateamount(AMOUNT);
		newbalance.setCurrentstatus("6f017761-5ed2-47b6-b585-f525bfbd3664");
		newbalance.setUpdatestatus("b73a8c43-9758-48fb-a6d5-92816d357cab");
		newbalance.setCreditaccountuuid(DEMO);
		newbalance.setServertime(new Date());
		
		
		

		
		assertTrue(storage.addCollectionUpdateStatus(newbalance));
	}

}

package com.impalapay.collection.persistence.refund;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.collection.beans.refund.CollectionRefund;

/**
 * Abstraction for persistence of balance in an the master account.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public interface CollectionRefundDAO {

	/**
	 * 
	 * @param collectionbalance
	 * @return
	 */
	//public boolean deductCollectionBalance(CollectionBalanceHistory collectionbalance);
	
	/**
	 * 
	 * @param uuid
	 * @return
	 */
	boolean deleteCollectionRefund(String uuid);

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public CollectionRefund getCollectionRefund(String uuid);

	/**
	 * 
	 * @param collectionbalance
	 * @return
	 */
	boolean putCollectionRefund(CollectionRefund collectinrefund);
	
	/***
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CollectionRefund> getAllCollectionRefund(int fromIndex, int toIndex);

	/**
	 * 
	 * @param account
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CollectionRefund> getAllCollectionRefund(Account account, int fromIndex, int toIndex);


}

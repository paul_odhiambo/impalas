package com.impalapay.collection.persistence.refund;

import java.util.List;

import com.impalapay.collection.beans.incoming.CollectionUpdateStatus;


public interface CollectionUpdateStatusDAO {

	/**
	 * Gets all transaction requests
	 *
	 * @return List<{@link TransactionForexrate}> all transaction requests
	 */
	public List<CollectionUpdateStatus> getAllCollectionUpdateStatus();

	/**
	 * Gets all transactionForexs requests between the specified fromIndex,
	 * inclusive, and toIndex, exclusive.
	 *
	 * @param fromIndex
	 * @param toIndex
	 * 
	 * @return List<{@link TransactionForexrate}> all transaction requests
	 */
	public List<CollectionUpdateStatus> getAllCollectionUpdateStatus(int fromIndex, int toIndex);

	/**
	 *
	 * @param transaction
	 * @return transaction
	 */
	public boolean addCollectionUpdateStatus(CollectionUpdateStatus transactionupdatetable);

	/**
	 * 
	 * @param transactionupdatetable
	 * @return
	 */
	public boolean addCollectionUpdateStatusHistory(CollectionUpdateStatus transactionupdatetable);
	
	/**
	 * 
	 * @param transactionupdatetable
	 * @return
	 */
	public boolean adddeleteprocessedUpdateStatusHistory(CollectionUpdateStatus transactionupdatetable);


	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CollectionUpdateStatus> getAllCollectionUpdateStatusHistory(int fromIndex, int toIndex);

	/**
	 * Returns a view of the portion of an Account's username Transaction activity
	 * between the specified fromIndex, inclusive, and toIndex, exclusive.
	 *
	 * @param account
	 * @param fromIndex
	 * @param toIndex
	 * @return List<{@link Transaction}> all transaction requests
	 * 
	 *         public List<Transaction> getTransactions(String username, int
	 *         fromIndex, int toIndex);
	 **/
	public boolean deleteCollectionUpdateStatus(String transactionid);

	/**
	 * 
	 * @param transactionuuid
	 * @return
	 */
	public CollectionUpdateStatus getCollectionUpdateStatusUuid(String transactionuuid);

	/**
	 * 
	 * @param transactionuuid
	 * @return
	 */
	public CollectionUpdateStatus getCollectionUpdateStatusUuidHistory(String transactionuuid);

}

package com.impalapay.collection.persistence.refund;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.collection.beans.refund.CollectionRefundHistory;

/**
 * Abstraction for persistence of balance in an the master account.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public interface CollectionRefundHistoryDAO {

	/**
	 * 
	 * @param collectionbalance
	 * @return
	 */
	//public boolean deductCollectionBalance(CollectionBalanceHistory collectionbalance);
	
	/**
	 * 
	 * @param uuid
	 * @return
	 */
	boolean deleteCollectionRefundHistory(String uuid);

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public CollectionRefundHistory getCollectionRefundHistory(String uuid);
	
	
	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public CollectionRefundHistory getCollectionRefundHistoryByreference(String reference);

	/**
	 * 
	 * @param collectionbalance
	 * @return
	 */
	boolean putCollectionRefundHistory(CollectionRefundHistory collectinrefund);
	
	/***
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CollectionRefundHistory> getAllCollectionRefundHistory(int fromIndex, int toIndex);

	/**
	 * 
	 * @param account
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CollectionRefundHistory> getAllCollectionRefundHistory(Account account, int fromIndex, int toIndex);
	
	/**
	 * 
	 * @param transactionStatus
	 * @param limit
	 * @return
	 */
	public List<CollectionRefundHistory> getCollectionRefundHistoryByStatusUuid(TransactionStatus transactionStatus, int limit);



}

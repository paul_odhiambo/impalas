package com.impalapay.collection.persistence.balance;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.collection.beans.balance.CollectionBalanceHistory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

public class CollectionBalanceDAOImpl extends GenericDAO implements CollectionBalanceDAO {

	public static CollectionBalanceDAOImpl collectionbalanceDAOImpl;

	private Logger logger;

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return {@link TransactionDAO}
	 */
	public static CollectionBalanceDAOImpl getInstance() {

		if (collectionbalanceDAOImpl == null) {
			collectionbalanceDAOImpl = new CollectionBalanceDAOImpl();
		}

		return collectionbalanceDAOImpl;
	}

	/**
	 * 
	 */
	public CollectionBalanceDAOImpl() {
		super();

		logger = Logger.getLogger(this.getClass());
	}

	/**
	 * 
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public CollectionBalanceDAOImpl(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

		logger = Logger.getLogger(this.getClass());
	}

	@Override
	public boolean putCollectionBalance(CollectionBalanceHistory collectionbalance) {
		boolean success = true;

		int countryBalanceId = 0;
		double countryBalance = 0, finalBalance;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null, pstmt3 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			conn.setAutoCommit(false);

			// Credit
			pstmt = conn.prepareStatement("SELECT * FROM collectionbalance WHERE accountUuid = ? AND countryuuid=?;");

			pstmt.setString(1, collectionbalance.getAccountuuid());
			pstmt.setString(2, collectionbalance.getCountryuuid());
			rset = pstmt.executeQuery();

			if (rset.next()) {
				countryBalanceId = rset.getInt("balanceid");
				countryBalance = rset.getDouble("balance");
			}

			// if master balance already exists, credit the balance
			if (countryBalanceId > 0) {
				pstmt2 = conn.prepareStatement("UPDATE collectionbalance SET balance=? WHERE balanceid=?;");
				pstmt2.setDouble(1, countryBalance + collectionbalance.getAmount());
				pstmt2.setInt(2, countryBalanceId);
				pstmt2.executeUpdate();
				finalBalance = countryBalance + collectionbalance.getAmount();

			} else {
				pstmt2 = conn.prepareStatement(
						"INSERT INTO collectionbalance(uuid,countryuuid, accountuuid, balance) " + "VALUES(?,?,?,?);");

				pstmt2.setString(1, UUID.randomUUID().toString());
				pstmt2.setString(2, collectionbalance.getCountryuuid());
				pstmt2.setString(3, collectionbalance.getAccountuuid());
				pstmt2.setDouble(4, collectionbalance.getAmount());
				pstmt2.execute();
				finalBalance = collectionbalance.getAmount();
			}

			// Lastly store on the other Table.

			pstmt3 = conn.prepareStatement(
					"INSERT INTO collectionbalancehistory (uuid, accountuuid, countryuuid, transactionuuid, "
							+ "amount,balance,topuptime) VALUES (?, ?, ?, ?, ?, ?, ?);");

			pstmt3.setString(1, collectionbalance.getUuid());
			pstmt3.setString(2, collectionbalance.getAccountuuid());
			pstmt3.setString(3, collectionbalance.getCountryuuid());
			pstmt3.setString(4, collectionbalance.getTransactionuuid());
			pstmt3.setDouble(5, collectionbalance.getAmount());
			pstmt3.setDouble(6, finalBalance);
			pstmt3.setTimestamp(7, new Timestamp(collectionbalance.getTopupTime().getTime()));

			pstmt3.execute();

			conn.commit();

		} catch (SQLException e) {
			logger.error("SQLException exception while inserting: " + collectionbalance);
			logger.error(ExceptionUtils.getStackTrace(e));

			try {
				conn.rollback();
			} catch (SQLException ex) {

				System.out.print("WOWEEEEEEEEEEEEEEE HAPA KUMEHARIBIKA");
			}
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt3 != null) {
				try {
					pstmt3.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<CollectionBalanceHistory> getAllClientCollectionBalance(Account account, int fromIndex, int toIndex) {
		List<CollectionBalanceHistory> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM collectionbalance WHERE accountUuid=? ORDER BY accountuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setString(1, account.getUuid());
			pstmt.setInt(2, toIndex - fromIndex);
			pstmt.setInt(3, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CollectionBalanceHistory.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collectionbalance from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<CollectionBalanceHistory> getAllClientCollectionBalanceHistory(Account account, int fromIndex,
			int toIndex) {
		// TODO Auto-generated method stub
		List<CollectionBalanceHistory> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM collectionbalancehistory WHERE accountUuid=? ORDER BY accountuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setString(1, account.getUuid());
			pstmt.setInt(2, toIndex - fromIndex);
			pstmt.setInt(3, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CollectionBalanceHistory.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collectionbalancehistory from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public CollectionBalanceHistory getCollectionBalance(String uuid) {
		CollectionBalanceHistory s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collectionbalance WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, CollectionBalanceHistory.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting collectionbalance with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public boolean deductCollectionBalance(CollectionBalanceHistory collectionbalance) {
		// TODO Auto-generated method stub
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("UPDATE collectionbalance "
					+ "SET balance = (SELECT balance FROM collectionbalance WHERE accountuuid=? AND countryuuid=?) "
					+ "- ? "
					+ "WHERE uuid = (SELECT uuid FROM collectionbalance WHERE accountuuid=? AND countryuuid=?);");

			pstmt.setString(1, collectionbalance.getAccountuuid());
			pstmt.setString(2, collectionbalance.getCountryuuid());
			pstmt.setDouble(3, collectionbalance.getAmount());
			pstmt.setString(4, collectionbalance.getAccountuuid());
			pstmt.setString(5, collectionbalance.getCountryuuid());
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQLException exception while deducting balance");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<CollectionBalanceHistory> getAllClientCollectionBalance(int fromIndex, int toIndex) {
		List<CollectionBalanceHistory> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn
					.prepareStatement("SELECT * FROM collectionbalance ORDER BY accountuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CollectionBalanceHistory.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collectionbalance from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<CollectionBalanceHistory> getAllClientCollectionBalanceHistory(int fromindex, int toindex) {
		// TODO Auto-generated method stub
		List<CollectionBalanceHistory> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM collectionbalancehistory ORDER BY accountuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toindex - fromindex);
			pstmt.setInt(2, fromindex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CollectionBalanceHistory.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collectionbalancehistory from index " + fromindex
					+ " to index " + toindex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}
}

package com.impalapay.collection.persistence.forwardcollection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.collection.beans.incoming.ForwardCollection;

public class ForwardCollectionDAOImpl extends GenericDAO implements ForwardCollectionDAO {

	private static ForwardCollectionDAOImpl processedDAOImpl;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return
	 */
	public static ForwardCollectionDAOImpl getInstance() {
		if (processedDAOImpl == null) {
			processedDAOImpl = new ForwardCollectionDAOImpl();
		}

		return processedDAOImpl;
	}

	protected ForwardCollectionDAOImpl() {
		super();
	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public ForwardCollectionDAOImpl(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

	}

	@Override
	public ForwardCollection getForwardColletion(String uuid) {
		ForwardCollection s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forwardcollection WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, ForwardCollection.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting forwardcollection with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public boolean putForwardColletion(ForwardCollection tempincomingtransaction) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO forwardcollection(uuid,creditaccountuuid,networkname,transactiontype,debitorname,"
							+ "debitcurrency,amount,accountreference,debitedaccount,originatetransactionuuid,forwarduri,"
							+ "collectiondate) VALUES (? ,? ,? ,? ,? ,?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, tempincomingtransaction.getUuid());
			pstmt.setString(2, tempincomingtransaction.getCreditaccountuuid());
			pstmt.setString(3, tempincomingtransaction.getNetworkname());
			pstmt.setString(4, tempincomingtransaction.getTransactiontype());
			pstmt.setString(5, tempincomingtransaction.getDebitorname());
			pstmt.setString(6, tempincomingtransaction.getDebitcurrency());
			pstmt.setDouble(7, tempincomingtransaction.getAmount());
			pstmt.setString(8, tempincomingtransaction.getAccountreference());
			pstmt.setString(9, tempincomingtransaction.getDebitedaccount());
			pstmt.setString(10, tempincomingtransaction.getOriginatetransactionuuid());
			pstmt.setString(11, tempincomingtransaction.getForwarduri());
			pstmt.setTimestamp(12, new Timestamp(tempincomingtransaction.getCollectiondate().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + tempincomingtransaction);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

			System.out.println(ExceptionUtils.getStackTrace(e));

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public boolean updateForwardColletion(String uuid, ForwardCollection tempincomingtransaction) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forwardcollection WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {

				// System.out.println("executing second stage");
				pstmt2 = conn.prepareStatement(
						"UPDATE forwardcollection SET collectionnetworkuuid=?,networksubaccountuuid=?,"
								+ "creditaccountuuid=?,amount=?,sendername=?,currency=?,originateaccount=?,originatetransactionuuid=?,"
								+ "accountreference=?,vendorunique=?,transactionstatusuuid=?,commission=?,presettlement=?,serverTime=?"
								+ "WHERE uuid=?;");

				// pstmt2.setString(1, tempincomingtransaction.getCollectionnetworkuuid());
				// pstmt2.setString(2, tempincomingtransaction.getNetworksubaccountuuid());
				pstmt2.setString(3, tempincomingtransaction.getCreditaccountuuid());
				pstmt2.setDouble(4, tempincomingtransaction.getAmount());
				// pstmt2.setString(5, tempincomingtransaction.getSendername());
				// pstmt2.setString(6, tempincomingtransaction.getCurrency());
				// pstmt2.setString(7, tempincomingtransaction.getOriginateaccount());
				pstmt2.setString(8, tempincomingtransaction.getOriginatetransactionuuid());
				pstmt2.setString(9, tempincomingtransaction.getAccountreference());
				// pstmt2.setString(10, tempincomingtransaction.getVendorunique());
				// pstmt2.setString(11, tempincomingtransaction.getTransactionstatusuuid());
				// pstmt2.setDouble(12, tempincomingtransaction.getCommission());
				// pstmt2.setBoolean(13, tempincomingtransaction.isPresettlement());
				// pstmt2.setTimestamp(14, new
				// Timestamp(tempincomingtransaction.getServertime().getTime()));

				pstmt2.executeUpdate();

			} else {
				success = putForwardColletion(tempincomingtransaction);
			}

		} catch (SQLException e) {
			logger.error("SQLException when trying to update forwardcollection with uuid '" + uuid + "' with "
					+ tempincomingtransaction + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<ForwardCollection> getAllForwardColletion() {
		List<ForwardCollection> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forwardcollection ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, ForwardCollection.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all forwardcollection.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<ForwardCollection> getAllForwardColletion(int fromIndex, int toIndex) {
		List<ForwardCollection> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn
					.prepareStatement("SELECT * FROM forwardcollection ORDER BY collectiondate DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, ForwardCollection.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all forwardcollection from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public ForwardCollection getForwardColletionstatus(String transactionstatusuuid) {
		ForwardCollection s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forwardcollection WHERE processingstatus=? ;");
			pstmt.setString(1, transactionstatusuuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, ForwardCollection.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting forwardcollection with processingstatusuuid '"
					+ transactionstatusuuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	/**
	 * 
	 */
	@Override
	public List<ForwardCollection> getAllForwardColletion(String sendertransactionid) {
		List<ForwardCollection> list = new LinkedList<>();
		ForwardCollection transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forwardcollection WHERE originatetransactionuuid=?;");
			pstmt.setString(1, sendertransactionid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transaction = beanProcessor.toBean(rset, ForwardCollection.class);

				list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching forwardcollection with originatetransactionuuid"
					+ sendertransactionid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<ForwardCollection> getTransactionReference(String originatetransactionuuid) {
		List<ForwardCollection> list = new LinkedList<>();
		ForwardCollection transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forwardcollection WHERE originatetransactionuuid=?;");
			pstmt.setString(1, originatetransactionuuid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transaction = beanProcessor.toBean(rset, ForwardCollection.class);

				list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error(
					"SQL exception while fetching forwardcollection with referenceNumber" + originatetransactionuuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<ForwardCollection> getForwardColletionByStatusUuid(int limit) {
		List<ForwardCollection> list = new LinkedList<>();
		ForwardCollection transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forwardcollection ORDER BY RANDOM() ASC LIMIT ?;");
			pstmt.setInt(1, limit);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transaction = beanProcessor.toBean(rset, ForwardCollection.class);

				list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching forwardcollection with processingstatus");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public boolean putForwardColletionHistory(ForwardCollection tempincomingtransaction) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO forwardcollectionhistory(uuid,creditaccountuuid,networkname,transactiontype,debitorname,"
							+ "debitcurrency,amount,accountreference,debitedaccount,originatetransactionuuid,forwarduri,processingstatus,endpointstatusdescription,"
							+ "collectiondate) VALUES (? ,? ,? ,? ,? ,?, ?, ?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, tempincomingtransaction.getUuid());
			pstmt.setString(2, tempincomingtransaction.getCreditaccountuuid());
			pstmt.setString(3, tempincomingtransaction.getNetworkname());
			pstmt.setString(4, tempincomingtransaction.getTransactiontype());
			pstmt.setString(5, tempincomingtransaction.getDebitorname());
			pstmt.setString(6, tempincomingtransaction.getDebitcurrency());
			pstmt.setDouble(7, tempincomingtransaction.getAmount());
			pstmt.setString(8, tempincomingtransaction.getAccountreference());
			pstmt.setString(9, tempincomingtransaction.getDebitedaccount());
			pstmt.setString(10, tempincomingtransaction.getOriginatetransactionuuid());
			pstmt.setString(11, tempincomingtransaction.getForwarduri());
			pstmt.setString(12, tempincomingtransaction.getProcessingstatus());
			pstmt.setString(13, tempincomingtransaction.getEndpointstatusdescription());
			pstmt.setTimestamp(14, new Timestamp(tempincomingtransaction.getCollectiondate().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + tempincomingtransaction);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

			System.out.println(ExceptionUtils.getStackTrace(e));

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public boolean deleteForwardCollection(String temptransactionuuidid) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("DELETE FROM forwardcollection WHERE uuid=?;");

			pstmt.setString(1, temptransactionuuidid);

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while deleting " + temptransactionuuidid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

}

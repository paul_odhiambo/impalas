package com.impalapay.collection.persistence.accountmgmt.inprogressbalance;

import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressMasterBalanceHold;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressMasterBalanceHoldHist;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.airtel.persistence.systemlog.SystemLogDAO;
import com.impalapay.collection.beans.balance.CollectionBalanceHistory;
import com.impalapay.collection.persistence.balance.CollectionBalanceDAOImpl;
import com.impalapay.mno.beans.accountmgmt.balance.AccountPurchaseByCountry;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

public class InProgressBalanceDAO extends GenericDAO implements InProgressBalance {

	public static InProgressBalanceDAO inprogressbalanceDAO;

	private Logger logger;
	private CollectionBalanceDAOImpl collectionbalanceDAO;
	private SystemLogDAO systemlogDAO;

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return {@link TransactionDAO}
	 */
	public static InProgressBalanceDAO getInstance() {

		if (inprogressbalanceDAO == null) {
			inprogressbalanceDAO = new InProgressBalanceDAO();
		}

		return inprogressbalanceDAO;
	}

	/**
	 * 
	 */
	public InProgressBalanceDAO() {
		super();
		collectionbalanceDAO = CollectionBalanceDAOImpl.getInstance();
		systemlogDAO = SystemLogDAO.getInstance();
		logger = Logger.getLogger(this.getClass());
	}

	/**
	 * 
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public InProgressBalanceDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);
		collectionbalanceDAO = new CollectionBalanceDAOImpl(dbName, dbHost, dbUsername, dbPassword, dbPort);
		logger = Logger.getLogger(this.getClass());
	}

	@Override
	public boolean deleteMasterBalanceOnSuccess(String transactionid) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean putBalanceOnHoldAccount(InProgressMasterBalanceHoldHist masteraccount) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null, pstmt3 = null;
		ResultSet rset = null, rset2 = null;

		try {
			conn = dbCredentials.getConnection();
			conn.setAutoCommit(false);

			// inser into master balance hold history table
			pstmt = conn.prepareStatement(
					"INSERT INTO withdrawalbalanceHoldhistory(uuid,accountuuid,amount,currency,transactionuuid,refundedback,processed,topuptime) "
							+ "VALUES(?,?,?,?,?,?,?,?);");

			pstmt.setString(1, masteraccount.getUuid());
			pstmt.setString(2, masteraccount.getAccountUuid());
			pstmt.setDouble(3, masteraccount.getAmount());
			pstmt.setString(4, masteraccount.getCurrency());
			pstmt.setString(5, masteraccount.getTransactionuuid());
			pstmt.setBoolean(6, masteraccount.isRefundedback());
			pstmt.setBoolean(7, masteraccount.isProcessed());
			pstmt.setTimestamp(8, new Timestamp(masteraccount.getTopuptime().getTime()));
			pstmt.execute();

			// Credit the master float balance(clientbalance)
			pstmt2 = conn.prepareStatement("SELECT * FROM withdrawalbalanceHoldhistory WHERE transactionuuid = ?;");

			pstmt2.setString(1, masteraccount.getTransactionuuid());
			rset = pstmt2.executeQuery();

			if (rset.next()) {

				// insert into client balance holding account.
				pstmt3 = conn.prepareStatement(
						"INSERT INTO withdrawalbalanceHold(uuid, accountuuid,currency,transactionuuid,amount) "
								+ "VALUES(?,?,?,?,?);");

				pstmt3.setString(1, UUID.randomUUID().toString());
				pstmt3.setString(2, masteraccount.getAccountUuid());
				pstmt3.setString(3, masteraccount.getCurrency());
				pstmt3.setString(4, masteraccount.getTransactionuuid());
				pstmt3.setDouble(5, masteraccount.getAmount());
				pstmt3.execute();

			}

			conn.commit();

		} catch (SQLException e) {
			logger.error("SQLException exception while inserting: " + masteraccount);
			logger.error(ExceptionUtils.getStackTrace(e));

			System.out.println(e.getStackTrace());

			try {
				conn.rollback();
			} catch (SQLException ex) {
			}
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (rset2 != null) {
				try {
					rset2.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt3 != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public boolean removeBalanceHoldSuccess(String transactionuuid) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null, pstmt3 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			conn.setAutoCommit(false);

			pstmt = conn.prepareStatement("SELECT * FROM withdrawalbalanceHoldhistory WHERE transactionuuid=?;");
			pstmt.setString(1, transactionuuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				// update the status to success/processed
				pstmt2 = conn.prepareStatement(
						"UPDATE withdrawalbalanceHoldhistory SET refundedback=?,processed=? WHERE transactionuuid = ?;");
				pstmt2.setBoolean(1, false);
				pstmt2.setBoolean(2, true);
				pstmt2.setString(3, transactionuuid);
				pstmt2.executeUpdate();
			}

			// Delete the purchase
			pstmt3 = conn.prepareStatement("DELETE FROM withdrawalbalanceHold WHERE transactionuuid=?;");
			pstmt3.setString(1, transactionuuid);
			pstmt3.executeUpdate();

			conn.commit();

		} catch (SQLException e) {
			logger.error(
					"SQLException exception while executing multiple update for master balance with transactionuuid: "
							+ transactionuuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;
			try {
				conn.rollback();
			} catch (SQLException ex) {
			}

		} finally {
			try {
				if (rset != null) {
					rset.close();
				}

				if (pstmt != null) {
					pstmt.close();
				}
				if (pstmt2 != null) {
					pstmt2.close();
				}

				if (pstmt3 != null) {
					pstmt3.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (SQLException e) {
			}
		}

		return success;

	}

	@Override
	public boolean removeBalanceHoldFail(String transactionuuid) {
		boolean success = true;
		double masteramount = 0, bycountryamount = 0;
		String accountuuid = "", mastercurrency = "", countryuuid = "";

		Connection conn = null;
		CollectionBalanceHistory p;
		SystemLog systemlogmaster, systemlogbycountry;
		AccountPurchaseByCountry bycountry;
		PreparedStatement pstmt = null, pstmt2 = null, pstmt3 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			conn.setAutoCommit(false);

			pstmt = conn.prepareStatement("SELECT * FROM withdrawalbalanceHoldhistory WHERE transactionuuid=?;");
			pstmt.setString(1, transactionuuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {

				// assign appropriate values
				accountuuid = rset.getString("accountuuid");
				masteramount = rset.getDouble("amount");
				mastercurrency = rset.getString("currency");
				// update the status to success/processed
				pstmt2 = conn.prepareStatement(
						"UPDATE withdrawalbalanceHoldhistory SET refundedback=?,processed=? WHERE transactionuuid = ?;");
				pstmt2.setBoolean(1, true);
				pstmt2.setBoolean(2, true);
				pstmt2.setString(3, transactionuuid);
				pstmt2.executeUpdate();
			}

			// Prepare object for Master Float.
			p = new CollectionBalanceHistory();
			p.setUuid(UUID.randomUUID().toString());
			p.setAccountuuid(accountuuid);
			p.setAmount(masteramount);
			p.setCountryuuid(mastercurrency);
			p.setTransactionuuid(transactionuuid);

			if (collectionbalanceDAO.putCollectionBalance(p)) {

				// update the systemlogs for master float.
				systemlogmaster = new SystemLog();
				systemlogbycountry = new SystemLog();
				systemlogmaster.setUsername("automatic-systemreversal");
				systemlogmaster.setUuid(StringUtils.remove(UUID.randomUUID().toString(), '-'));
				systemlogmaster
						.setAction("automatic-systemreversal" + " added and approved new withdrawal refund of currency "
								+ mastercurrency + " amount= " + masteramount + " for account " + accountuuid+"with collectionbalancehistory UUID "+p.getUuid());

				// update system logs for float by country
				systemlogbycountry.setUsername("automatic-systemreversal");
				systemlogbycountry.setUuid(StringUtils.remove(UUID.randomUUID().toString(), '-'));
				systemlogbycountry.setAction("automatic-systemreversal" + "added and approved new balance of "
						+ bycountryamount + " on country " + countryuuid + " for account " + accountuuid);
				systemlogDAO.putsystemlog(systemlogmaster);
				systemlogDAO.putsystemlog(systemlogbycountry);

				// Delete the tables
				pstmt3 = conn.prepareStatement("DELETE FROM withdrawalbalanceHold WHERE transactionuuid=?;");
				pstmt3.setString(1, transactionuuid);
				pstmt3.executeUpdate();

				conn.commit();
			}

		} catch (SQLException e) {
			logger.error(
					"SQLException exception while executing multiple update for withdrawal balance with transactionuuid: "
							+ transactionuuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;
			try {
				conn.rollback();
			} catch (SQLException ex) {
			}

		} finally {
			try {
				if (rset != null) {
					rset.close();
				}
				if (pstmt != null) {
					pstmt.close();
				}
				if (pstmt2 != null) {
					pstmt2.close();
				}
				if (pstmt3 != null) {
					pstmt3.close();
				}

				if (conn != null) {
					conn.close();
				}

			} catch (SQLException e) {
			}
		}

		return success;
	}

	@Override
	public List<InProgressMasterBalanceHold> getAllMasterBalanceHold(int fromIndex, int toIndex) {
		List<InProgressMasterBalanceHold> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM withdrawalbalanceHold ORDER BY accountuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, InProgressMasterBalanceHold.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all withdrawalbalanceHold from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<InProgressMasterBalanceHoldHist> getAllMasterBalanceHoldHist(int fromIndex, int toIndex) {
		List<InProgressMasterBalanceHoldHist> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM withdrawalbalanceHoldhistory ORDER BY topuptime DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, InProgressMasterBalanceHoldHist.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all withdrawalbalanceHoldhistory from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

}

package com.impalapay.collection.persistence.accountmgmt.inprogressbalance;

import java.util.List;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressBalancebyCountryHoldHist;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressMasterBalanceHold;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressMasterBalanceHoldHist;

/**
 * Abstraction for persistence of balance in an the master account.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public interface InProgressBalance {

	/**
	 * We will start with Master Balanace.
	 */

	boolean deleteMasterBalanceOnSuccess(String transactionid);

	boolean putBalanceOnHoldAccount(InProgressMasterBalanceHoldHist masteraccount);

	boolean removeBalanceHoldSuccess(String transactionuuid);

	boolean removeBalanceHoldFail(String transactionuuid);

	// Main float on hold
	public List<InProgressMasterBalanceHold> getAllMasterBalanceHold(int fromIndex, int toIndex);

	public List<InProgressMasterBalanceHoldHist> getAllMasterBalanceHoldHist(int fromIndex, int toIndex);

}

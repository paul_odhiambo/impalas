package com.impalapay.collection.persistence.networksubaccount;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.collection.beans.network.CollectionNetworkSubaccount;

public class CollectionNetworkSubaccountDAOImpl extends GenericDAO implements CollectionNetworkSubaccountDAO {

	private static CollectionNetworkSubaccountDAOImpl collectionNetworkSubaccountDAOImpl;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return
	 */
	public static CollectionNetworkSubaccountDAOImpl getInstance() {
		if (collectionNetworkSubaccountDAOImpl == null) {
			collectionNetworkSubaccountDAOImpl = new CollectionNetworkSubaccountDAOImpl();
		}

		return collectionNetworkSubaccountDAOImpl;
	}

	protected CollectionNetworkSubaccountDAOImpl() {
		super();
	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public CollectionNetworkSubaccountDAOImpl(String dbName, String dbHost, String dbUsername, String dbPassword,
			int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

	}

	/**
	 * 
	 */
	@Override
	public CollectionNetworkSubaccount getCollectionNetworkSubAcct(String uuid) {
		CollectionNetworkSubaccount s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collection_networksubaccount WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, CollectionNetworkSubaccount.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting collection_networksubaccount with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public boolean putNetworkSubaccount(CollectionNetworkSubaccount networksubaccount) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn
					.prepareStatement("INSERT INTO collection_networksubaccount(uuid,networkuuid,sharedconfiguration,"
							+ "collectionnumber,currency,referencesplitlength,"
							+ "dateadded) VALUES (?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, networksubaccount.getUuid());
			pstmt.setString(2, networksubaccount.getNetworkuuid());
			pstmt.setBoolean(3, networksubaccount.isSharedconfiguration());
			pstmt.setString(4, networksubaccount.getCollectionnumber());
			pstmt.setString(5, networksubaccount.getCurrency());
			pstmt.setInt(6, networksubaccount.getReferencesplitlength());
			pstmt.setTimestamp(7, new Timestamp(networksubaccount.getDateadded().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + networksubaccount);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;

	}

	@Override
	public boolean updateNetworkSubaccount(String uuid, CollectionNetworkSubaccount networksubaccount) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collection_networksubaccount WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {

				// System.out.println("executing second stage");
				pstmt2 = conn.prepareStatement(
						"UPDATE collection_networksubaccount SET networkuuid=?,sharedconfiguration=?,collectionnumber=?,currency=?,referencesplitlength=?,dateadded=?"
								+ "WHERE uuid=?;");

				pstmt2.setString(1, networksubaccount.getNetworkuuid());
				pstmt2.setBoolean(2, networksubaccount.isSharedconfiguration());
				pstmt2.setString(3, networksubaccount.getCollectionnumber());
				pstmt2.setString(4, networksubaccount.getCurrency());
				pstmt2.setInt(5, networksubaccount.getReferencesplitlength());
				pstmt2.setTimestamp(6, new Timestamp(networksubaccount.getDateadded().getTime()));
				pstmt2.setString(7, networksubaccount.getUuid());

				pstmt2.executeUpdate();

			} else {
				success = putNetworkSubaccount(networksubaccount);
			}

		} catch (SQLException e) {
			logger.error("SQLException when trying to update collection_networksubaccount with uuid '" + uuid
					+ "' with " + networksubaccount + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<CollectionNetworkSubaccount> getAllNetworkSubaccount() {
		List<CollectionNetworkSubaccount> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collection_networksubaccount ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CollectionNetworkSubaccount.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all collection_networksubaccount.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<CollectionNetworkSubaccount> getAllNetworkSubaccount(int fromIndex, int toIndex) {
		List<CollectionNetworkSubaccount> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM collection_networksubaccount ORDER BY dateadded DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CollectionNetworkSubaccount.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collection_networksubaccount from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public CollectionNetworkSubaccount getNetworkSubaccount(CollectionNetworkSubaccount name) {
		CollectionNetworkSubaccount s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collection_networksubaccount WHERE collectionnumber = ?;");
			pstmt.setString(1, name.getCollectionnumber());

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, CollectionNetworkSubaccount.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting collection_networksubaccount with uuid '" + name.getUuid() + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public CollectionNetworkSubaccount getCollectionNetworkSubAcct(String collecnetworkuuid, String collectionnumber) {
		CollectionNetworkSubaccount s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM collection_networksubaccount WHERE networkuuid = ? AND collectionnumber=? ;");
			pstmt.setString(1, collecnetworkuuid);
			pstmt.setString(2, collectionnumber);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, CollectionNetworkSubaccount.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting collection_networksubaccount with networkuuid '"
					+ collecnetworkuuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public boolean deleteNetworkSubaccount(String uuid) {
		boolean success = true;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("DELETE FROM collection_networksubaccount WHERE uuid=?;");

			pstmt.setString(1, uuid);

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while Deleting " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}
}

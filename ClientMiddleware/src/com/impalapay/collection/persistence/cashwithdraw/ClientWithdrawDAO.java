package com.impalapay.collection.persistence.cashwithdraw;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.collection.beans.balance.CashWithdrawal;

/**
 * Abstraction for persistence of balance in an the master account.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public interface ClientWithdrawDAO {

	boolean deleteClientWithdraw(String uuid);

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public CashWithdrawal getClientWithdraw(String uuid);

	/**
	 * 
	 * @param collectionbalance
	 * @return
	 */
	boolean putClientWithdraw(CashWithdrawal collectionbalance);

	/***
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CashWithdrawal> getAllClientClientWithdraw(int fromIndex, int toIndex);

	/**
	 * 
	 * @param account
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CashWithdrawal> getAllClientClientWithdraw(Account account, int fromIndex, int toIndex);

}

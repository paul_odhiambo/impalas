package com.impalapay.collection.persistence.cashwithdraw;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.collection.beans.balance.CashWithdrawal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

public class CheckerWithdrawHistoryDAOImpl extends GenericDAO implements CheckerWithdrawHistoryDAO {

	public static CheckerWithdrawHistoryDAOImpl clientwithdrawDAOImpl;

	private Logger logger;
	private String inprogresstransaction = "5c9b8b0b-a035-4a07-bfd8-eccd4f039d53";

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return {@link TransactionDAO}
	 */
	public static CheckerWithdrawHistoryDAOImpl getInstance() {

		if (clientwithdrawDAOImpl == null) {
			clientwithdrawDAOImpl = new CheckerWithdrawHistoryDAOImpl();
		}

		return clientwithdrawDAOImpl;
	}

	/**
	 * 
	 */
	public CheckerWithdrawHistoryDAOImpl() {
		super();

		logger = Logger.getLogger(this.getClass());
	}

	/**
	 * 
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public CheckerWithdrawHistoryDAOImpl(String dbName, String dbHost, String dbUsername, String dbPassword,
			int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

		logger = Logger.getLogger(this.getClass());
	}

	@Override
	public boolean putCheckerWithdrawHistory(CashWithdrawal collectionbalance) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO withdrawal_history(uuid,accountUuid,currencyUuid,tocurrencyUuid,amount,extrainformation,systemexchangerate,"
							+ "comission,comissioncurrencyuuid,adminextrainformation,transactionstatusuuid,bankwithdrawexchangerate,authorisedchecker,authorisedmaker,transfercharges,transferchargecurrencyuuid,receivableamount,transactionDate) VALUES (?,?,?,?,?, ?, ?,?,?,?,?,?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, collectionbalance.getUuid());
			pstmt.setString(2, collectionbalance.getAccountuuid());
			pstmt.setString(3, collectionbalance.getCurrencyUuid());
			pstmt.setString(4, collectionbalance.getTocurrencyUuid());
			pstmt.setDouble(5, collectionbalance.getAmount());
			pstmt.setString(6, collectionbalance.getExtrainformation());
			pstmt.setDouble(7, collectionbalance.getSystemexchangerate());
			pstmt.setDouble(8, collectionbalance.getComission());
			pstmt.setString(9, collectionbalance.getComissioncurrencyUuid());
			pstmt.setString(10, collectionbalance.getExtrainformation());
			pstmt.setString(11, collectionbalance.getTransactionStatusUuid());
			pstmt.setDouble(12, collectionbalance.getBankwithdrawexchangerate());
			pstmt.setString(13, collectionbalance.getAuthorisedchecker());
			pstmt.setString(14, collectionbalance.getAuthorisedmaker());
			pstmt.setDouble(15, collectionbalance.getTransfercharges());
			pstmt.setString(16, collectionbalance.getTransferchargecurrencyUuid());
			pstmt.setDouble(17, collectionbalance.getReceivableamount());
			pstmt.setTimestamp(18, new Timestamp(collectionbalance.getTransactionDate().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + collectionbalance);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

			System.out.println(ExceptionUtils.getStackTrace(e));

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<CashWithdrawal> getAllCheckerWithdrawHistory(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		List<CashWithdrawal> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn
					.prepareStatement("SELECT * FROM withdrawal_history ORDER BY accountuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CashWithdrawal.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all withdrawal_history from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<CashWithdrawal> getAllInprogressWithdrawHistory(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		List<CashWithdrawal> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM withdrawal_history WHERE transactionstatusuuid=? ORDER BY accountuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setString(1, inprogresstransaction);
			pstmt.setInt(2, toIndex - fromIndex);
			pstmt.setInt(3, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CashWithdrawal.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all withdrawal_history from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public boolean updateTransactionStatus(String transactionUuid, TransactionStatus transactionstatus) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM withdrawal_history WHERE uuid=?;");
			pstmt.setString(1, transactionUuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				pstmt2 = conn.prepareStatement("UPDATE withdrawal_history SET transactionstatusuuid=? WHERE uuid=?;");

				pstmt2.setString(1, transactionstatus.getUuid());
				pstmt2.setString(2, transactionUuid);

				pstmt2.execute();

			} else {

				// addTransaction(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQLException when trying to update withdrawal_history with uuid '" + transactionUuid
					+ "' with " + transactionstatus + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public CashWithdrawal getWithdrawalHistoryTransaction(String uuid) {
		// TODO Auto-generated method stub
		CashWithdrawal s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM withdrawal_history WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, CashWithdrawal.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting withdrawal_history with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public List<CashWithdrawal> getAllCheckerWithdrawHistory(Account account, int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		List<CashWithdrawal> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM withdrawal_history WHERE accountuuid=? ORDER BY accountuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setString(1, account.getUuid());
			pstmt.setInt(2, toIndex - fromIndex);
			pstmt.setInt(3, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CashWithdrawal.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all withdrawal_history from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

}

package com.impalapay.collection.persistence.network;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.beans.route.RouteDefine;
import com.impalapay.collection.beans.network.CollectionDefine;

public interface CollectionDefineDAO {

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public CollectionDefine getCollectionRoute(String uuid);

	/**
	 * 
	 * @param networksubaccountuuid
	 * @param routeprefix
	 * @return
	 */
	public CollectionDefine getCollectionRouteUuid(String networksubaccountuuid, String routeprefix);

	/**
	 * 
	 * @param networksubaccountuuid
	 * @return
	 */
	public CollectionDefine getCollectionRouteUuid(String networksubaccountuuid);

	/**
	 * 
	 * @param collectiondefine
	 * @return
	 */
	public boolean PutCollectionDefine(CollectionDefine collectiondefine);

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	boolean deleteCollectionDefine(String uuid);

	/**
	 * 
	 * @param uuid
	 * @param collectiondefine
	 * @return
	 */
	public boolean UpdateCollectionDefine(String uuid, CollectionDefine collectiondefine);

	/**
	 * 
	 * @return
	 */
	public List<CollectionDefine> getAllCollectionRoute();

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CollectionDefine> getAllCollectionRoute(int fromIndex, int toIndex);

	/**
	 * 
	 * @param uuid
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CollectionDefine> getAllCollectionRoute(String uuid, int fromIndex, int toIndex);

	/**
	 * 
	 * @param account
	 * @return
	 */
	public List<CollectionDefine> getAllCollectionRoute(Account account);

}

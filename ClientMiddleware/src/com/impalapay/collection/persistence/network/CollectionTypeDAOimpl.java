package com.impalapay.collection.persistence.network;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.collection.beans.network.CollectionType;

public class CollectionTypeDAOimpl extends GenericDAO implements CollectionTypeDAO {

	private static CollectionTypeDAOimpl collectiontypeDAOImpl;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return CountryMsisdnDAO
	 */
	public static CollectionTypeDAOimpl getInstance() {
		if (collectiontypeDAOImpl == null) {
			collectiontypeDAOImpl = new CollectionTypeDAOimpl();
		}

		return collectiontypeDAOImpl;
	}

	protected CollectionTypeDAOimpl() {
		super();
	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public CollectionTypeDAOimpl(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

	}

	@Override
	public boolean PutCollectionType(CollectionType collectiontype) {
		// TODO Auto-generated method stub
		boolean success = true;
		Date date;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("INSERT INTO collectionchannel(uuid,type) VALUES (?, ?);");

			pstmt.setString(1, collectiontype.getUuid());
			pstmt.setString(2, collectiontype.getType());

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + collectiontype);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<CollectionType> getAllCollectionType() {
		List<CollectionType> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collectionchannel ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CollectionType.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all collectionchannel.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

}

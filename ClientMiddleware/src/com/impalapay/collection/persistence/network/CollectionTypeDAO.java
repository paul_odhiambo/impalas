package com.impalapay.collection.persistence.network;

import java.util.List;

import com.impalapay.collection.beans.network.CollectionType;

public interface CollectionTypeDAO {

	/**
	 * 
	 * @param collectiontype
	 * @return
	 */
	public boolean PutCollectionType(CollectionType collectiontype);

	/**
	 * 
	 * @return
	 */
	public List<CollectionType> getAllCollectionType();

}

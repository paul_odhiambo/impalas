package com.impalapay.collection.persistence.network;

import java.util.List;

import com.impalapay.collection.beans.network.CollectionNetwork;

public interface CollectionNetworkDAO {
	/**
	 * Retrieve the Network corresponding to the uuid.
	 * 
	 * @param uuid
	 * @return Network
	 */

	public CollectionNetwork getNetwork(String uuid);

	/**
	 * @param CollectionNetwork
	 * @return whether the action was successful or not
	 */
	public boolean putNetwork(CollectionNetwork network);

	/**
	 * 
	 * @param uuid
	 * @param CollectionNetwork
	 * @return
	 */
	boolean updateNetwork(String uuid, CollectionNetwork network);

	/**
	 * 
	 * @return
	 */
	public List<CollectionNetwork> getAllNetwork();

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CollectionNetwork> getAllNetwork(int fromIndex, int toIndex);

	/**
	 * 
	 * @param name
	 * @return
	 */
	public CollectionNetwork getNetwork(CollectionNetwork name);

	public List<CollectionNetwork> getAllNetwork(String uuid, int fromIndex, int toIndex);

}

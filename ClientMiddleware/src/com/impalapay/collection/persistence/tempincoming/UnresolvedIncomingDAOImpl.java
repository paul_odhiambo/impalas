package com.impalapay.collection.persistence.tempincoming;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.collection.beans.incoming.TempCollection;

public class UnresolvedIncomingDAOImpl extends GenericDAO implements UnresolvedIncomingDAO {

	private static UnresolvedIncomingDAOImpl tempIncomingDAOImpl;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return
	 */
	public static UnresolvedIncomingDAOImpl getInstance() {
		if (tempIncomingDAOImpl == null) {
			tempIncomingDAOImpl = new UnresolvedIncomingDAOImpl();
		}

		return tempIncomingDAOImpl;
	}

	protected UnresolvedIncomingDAOImpl() {
		super();
	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public UnresolvedIncomingDAOImpl(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

	}

	@Override
	public TempCollection getTempIncomingTrans(String uuid) {
		TempCollection s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM temp_unresolvedtransaction WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, TempCollection.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting temp_unresolvedtransaction with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public boolean putTempIncomingTrans(TempCollection tempincomingtransaction) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO temp_unresolvedtransaction(uuid,networkuuid,creditaccountuuid,transactiontype,debitorname,"
							+ "debitcurrency,debitcountry,amount,accountreference,transactionstatusuuid,debitedaccount,originatetransactionuuid,receivertransactionuuid,"
							+ "vendorunique,originateamount,originatecurrency,processingstatus,endpointstatusdescription,collectiondefineuuid,serverTime) VALUES (?, ?, ?, ?, ?, ?, ?, ? ,? ,? ,? ,? ,?, ?, ?, ?, ?, ?, ?, ?);");
			pstmt.setString(1, tempincomingtransaction.getUuid());
			pstmt.setString(2, tempincomingtransaction.getNetworkuuid());
			pstmt.setString(3, tempincomingtransaction.getCreditaccountuuid());
			pstmt.setString(4, tempincomingtransaction.getTransactiontype());
			pstmt.setString(5, tempincomingtransaction.getDebitorname());
			pstmt.setString(6, tempincomingtransaction.getDebitcurrency());
			pstmt.setString(7, tempincomingtransaction.getDebitcountry());
			pstmt.setDouble(8, tempincomingtransaction.getAmount());
			pstmt.setString(9, tempincomingtransaction.getAccountreference());
			pstmt.setString(10, tempincomingtransaction.getTransactionstatusuuid());
			pstmt.setString(11, tempincomingtransaction.getDebitedaccount());
			pstmt.setString(12, tempincomingtransaction.getOriginatetransactionuuid());
			pstmt.setString(13, tempincomingtransaction.getReceivertransactionuuid());
			pstmt.setString(14, tempincomingtransaction.getVendorunique());
			pstmt.setDouble(15, tempincomingtransaction.getOriginateamount());
			pstmt.setString(16, tempincomingtransaction.getOriginatecurrency());
			pstmt.setString(17, tempincomingtransaction.getProcessingstatus());
			pstmt.setString(18, tempincomingtransaction.getEndpointstatusdescription());
			pstmt.setString(19, tempincomingtransaction.getCollectiondefineuuid());
			pstmt.setTimestamp(20, new Timestamp(tempincomingtransaction.getServertime().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + tempincomingtransaction);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

			System.out.println(ExceptionUtils.getStackTrace(e));

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public boolean updateTempIncomingTrans(String uuid, TempCollection tempincomingtransaction) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM temp_unresolvedtransaction WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {

				// System.out.println("executing second stage");
				pstmt2 = conn.prepareStatement(
						"UPDATE temp_unresolvedtransaction SET collectionnetworkuuid=?,networksubaccountuuid=?,"
								+ "creditaccountuuid=?,amount=?,sendername=?,currency=?,originateaccount=?,originatetransactionuuid=?,"
								+ "accountreference=?,vendorunique=?,transactionstatusuuid=?,commission=?,presettlement=?,serverTime=?"
								+ "WHERE uuid=?;");

				// pstmt2.setString(1, tempincomingtransaction.getCollectionnetworkuuid());
				// pstmt2.setString(2, tempincomingtransaction.getNetworksubaccountuuid());
				pstmt2.setString(3, tempincomingtransaction.getCreditaccountuuid());
				pstmt2.setDouble(4, tempincomingtransaction.getAmount());
				// pstmt2.setString(5, tempincomingtransaction.getSendername());
				// pstmt2.setString(6, tempincomingtransaction.getCurrency());
				// pstmt2.setString(7, tempincomingtransaction.getOriginateaccount());
				pstmt2.setString(8, tempincomingtransaction.getOriginatetransactionuuid());
				pstmt2.setString(9, tempincomingtransaction.getAccountreference());
				pstmt2.setString(10, tempincomingtransaction.getVendorunique());
				pstmt2.setString(11, tempincomingtransaction.getTransactionstatusuuid());
				// pstmt2.setDouble(12, tempincomingtransaction.getCommission());
				// pstmt2.setBoolean(13, tempincomingtransaction.isPresettlement());
				pstmt2.setTimestamp(14, new Timestamp(tempincomingtransaction.getServertime().getTime()));

				pstmt2.executeUpdate();

			} else {
				success = putTempIncomingTrans(tempincomingtransaction);
			}

		} catch (SQLException e) {
			logger.error("SQLException when trying to update temp_unresolvedtransaction with uuid '" + uuid + "' with "
					+ tempincomingtransaction + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<TempCollection> getAllTempIncomingTrans() {
		List<TempCollection> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM temp_unresolvedtransaction ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, TempCollection.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all temp_unresolvedtransaction.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<TempCollection> getAllTempIncomingTrans(int fromIndex, int toIndex) {
		List<TempCollection> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM temp_unresolvedtransaction ORDER BY serverTime DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, TempCollection.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all temp_unresolvedtransaction from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public TempCollection getTempIncomingTranstatus(String transactionstatusuuid) {
		TempCollection s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM temp_unresolvedtransaction WHERE transactionstatusuuid=? ;");
			pstmt.setString(1, transactionstatusuuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, TempCollection.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting temp_unresolvedtransaction with transactionstatusuuid '"
					+ transactionstatusuuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	/**
	 * 
	 */
	@Override
	public List<TempCollection> getAllTempIncomingTrans(String sendertransactionid) {
		List<TempCollection> list = new LinkedList<>();
		TempCollection transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM temp_unresolvedtransaction WHERE originatetransactionuuid=?;");
			pstmt.setString(1, sendertransactionid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transaction = beanProcessor.toBean(rset, TempCollection.class);

				list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching temp_unresolvedtransaction with originatetransactionuuid"
					+ sendertransactionid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<TempCollection> getTransactionReference(String originatetransactionuuid) {
		List<TempCollection> list = new LinkedList<>();
		TempCollection transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM temp_unresolvedtransaction WHERE originatetransactionuuid=?;");
			pstmt.setString(1, originatetransactionuuid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transaction = beanProcessor.toBean(rset, TempCollection.class);

				list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching transaction with referenceNumber" + originatetransactionuuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<TempCollection> getTempIncomingTranstatusByStatusUuid(TransactionStatus transactionStatus, int limit) {
		List<TempCollection> list = new LinkedList<>();
		TempCollection transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					// "SELECT * FROM temp_incomingtransaction WHERE transactionstatusuuid=? ORDER
					// BY RANDOM() ASC LIMIT ?;");
					"SELECT * FROM temp_unresolvedtransaction WHERE transactionstatusuuid=? AND receivertransactionuuid !='' ORDER BY servertime ASC LIMIT ?;");
			pstmt.setString(1, transactionStatus.getUuid());
			pstmt.setInt(2, limit);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transaction = beanProcessor.toBean(rset, TempCollection.class);

				list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error(
					"SQL exception while fetching temp_unresolvedtransaction with transactionstatus" + transactionStatus);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<TempCollection> getTempIncomingTranstatusByStatusUuidAutoSettlement(TransactionStatus transactionStatus,
			int limit) {
		List<TempCollection> list = new LinkedList<>();
		TempCollection transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM temp_unresolvedtransaction WHERE processingstatus=? AND transactionstatusuuid !=? ORDER BY RANDOM() ASC LIMIT ?;");
			pstmt.setString(1, transactionStatus.getUuid());
			pstmt.setString(2, transactionStatus.getUuid());
			pstmt.setInt(3, limit);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transaction = beanProcessor.toBean(rset, TempCollection.class);

				list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error(
					"SQL exception while fetching temp_unresolvedtransaction with transactionstatus" + transactionStatus);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	/**
	 * 
	 * @param temptransactionuuidid
	 * @return
	 */
	@Override
	public boolean deleteTempTransaction(String temptransactionuuidid) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("DELETE FROM temp_unresolvedtransaction WHERE uuid=?;");

			pstmt.setString(1, temptransactionuuidid);

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while deleting " + temptransactionuuidid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public boolean updateTempIncomingTransactionStatus(String transactionUuid, TransactionStatus transactionstatus) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM temp_unresolvedtransaction WHERE uuid=?;");
			pstmt.setString(1, transactionUuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				pstmt2 = conn.prepareStatement(
						"UPDATE temp_unresolvedtransaction SET transactionstatusuuid=?,endpointstatusdescription=? WHERE uuid=?;");

				pstmt2.setString(1, transactionstatus.getUuid());
				pstmt2.setString(2, transactionstatus.getDescription());
				pstmt2.setString(3, transactionUuid);

				pstmt2.execute();

			} else {
				success = false;

				// addTransaction(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQLException when trying to update temp_unresolvedtransaction with uuid '" + transactionUuid
					+ "' with " + transactionstatus + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public TempCollection getTransactionstatus(String referencenumber, Account account) {
		TempCollection transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM temp_unresolvedtransaction WHERE originatetransactionuuid=? AND creditaccountuuid=?;");
			pstmt.setString(1, referencenumber);
			pstmt.setString(2, account.getUuid());

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transaction = beanProcessor.toBean(rset, TempCollection.class);

				// list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching transaction with referenceNumber" + referencenumber);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return transaction;
	}

	@Override
	public TempCollection getTransactionstatus(String receivertransactionuuid, TransactionStatus transactionstatus) {
		TempCollection transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM temp_unresolvedtransaction WHERE receivertransactionuuid=? AND transactionstatusuuid=?;");
			pstmt.setString(1, receivertransactionuuid);
			pstmt.setString(2, transactionstatus.getUuid());

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transaction = beanProcessor.toBean(rset, TempCollection.class);

				// list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching temp_unresolvedtransaction with receivertransactionuuid"
					+ receivertransactionuuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return transaction;
	}

}

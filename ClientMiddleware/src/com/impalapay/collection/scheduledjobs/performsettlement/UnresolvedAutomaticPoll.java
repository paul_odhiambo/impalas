package com.impalapay.collection.scheduledjobs.performsettlement;

import java.util.HashMap;
import java.util.List;
import com.google.gson.Gson;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.collection.beans.incoming.TempCollection;
import com.impalapay.collection.persistence.tempincoming.TempIncomingDAOImpl;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This Quartz job is used to check for Session Ids in the SessionLog database
 * table for expiry. Session Ids are expired after a fixed duration of time.
 * <p>
 * Copyright (c) Tawi Commercial Services Ltd., Sep 23, 2014
 * 
 * @author <a href="mailto:michael@tawi.mobi">Michael Wakahe</a>
 * 
 */

public class UnresolvedAutomaticPoll implements Job {

	private PostWithIgnoreSSL postMinusThread;

	private TempIncomingDAOImpl transactionDAO;

	private String TRANSACTIONSTATUS_UUID = "5c9b8b0b-a035-4a07-bfd8-eccd4f039d53";

	private String CLIENT_URL = "";
	private String responseobject = "";

	private HashMap<String, String> expected = new HashMap<>();

	private Logger logger;
	//numnber of minutes the transaction should have been in the system(3DAYS)
	final int SESSIONID_MINUTES_ALIVE = 4320;


	/**
	 * Empty constructor for job initialization
	 * <p>
	 * Quartz requires a public empty constructor so that the scheduler can
	 * instantiate the class whenever it needs.
	 */
	public UnresolvedAutomaticPoll() {

		transactionDAO = TempIncomingDAOImpl.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * <p>
	 * Called by the <code>{@link org.quartz.Scheduler}</code> when a
	 * <code>{@link org.quartz.Trigger}</code> fires that is associated with the
	 * <code>Job</code>.
	 * </p>
	 * 
	 * @throws JobExecutionException
	 *             if there is an exception while executing the job.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Gson g = new Gson();

		DateTime dateTime = new DateTime().minusMinutes(SESSIONID_MINUTES_ALIVE);

		// categorise the transaction as invalid
		// temporaryDAO.expireTempTransaction(dateTime.toGregorianCalendar().getTime());
		// System.out.println("This will definately Rock");

		// delete all the invalidated transactions from the database.
		// temporaryDAO.deleteTempTransactionByTime();

		TransactionStatus ts = new TransactionStatus();
		ts.setUuid(TRANSACTIONSTATUS_UUID);
		//List<TempCollection> transactionByStatus = transactionDAO.getTempIncomingTranstatusByStatusUuid(ts, 4);
		List<TempCollection> transactionByStatus = transactionDAO.getUnresolvedTransaction(dateTime.toGregorianCalendar().getTime(), 50);

		for (TempCollection t : transactionByStatus) {

			logger.error(".....................................................");
			logger.error(" START AUTOMATIC EXPULSION OF UNRESOLVED FOR TRANSACTION WITH UUID : " + t.getUuid() + "\n");
			logger.error(".....................................................");

			expected.put("transaction_id", t.getUuid());
			expected.put("networkuuid", t.getNetworkuuid());

			String jsonforxData = g.toJson(expected);

			CLIENT_URL = PropertiesConfig.getConfigValue("UNRESOLVEDAUTOPOLL_URL");
			try {
				postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonforxData);

				// capture the switch respoinse.
				responseobject = postMinusThread.doPost();

				System.out.println(jsonforxData + " response " + responseobject + " ");
			} catch (Exception e) {
				logger.error(".....................................................");
				logger.error(" ERROR OCCURED DURING AUTOMATIC AUTOMATIC EXPULSION OF UNRESOLVED OF TRANSACTION WITH UUID : " + t.getUuid()
						+ "\n");
				logger.error(responseobject);
				logger.error(".....................................................");
			}

			logger.error(".....................................................");
			logger.error(" RESPONSE FOR AUTOMATIC AUTOMATIC EXPULSION OF UNRESOLVED FOR TRANSACTION WITH UUID : " + t.getUuid() + "\n");
			logger.error(responseobject);
			logger.error(".....................................................");
		}

	}
}

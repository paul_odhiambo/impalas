package com.impalapay.collection.servlet.api.bridge.mpesa;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class TestRegisterURI {

	public static void main(String args[]) {

		JsonObject urirequest = new JsonObject();

		urirequest.addProperty("ShortCode", "1");
		urirequest.addProperty("ResponseType", "");
		urirequest.addProperty("ConfirmationURL", "http://staging.impalapay.net:8333/info.php");
		urirequest.addProperty("ValidationURL", "http://staging.impalapay.net:8333/info.php");

		Gson g = new Gson();

		String jsonData = g.toJson(urirequest);

		System.out.println(jsonData);

		String CLIENT_URL3 = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl";

		SafaricomPostThreadUniversal veve = new SafaricomPostThreadUniversal(CLIENT_URL3, jsonData);

		System.out.println(veve.doPostSecure());

	}

}

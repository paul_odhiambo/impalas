package com.impalapay.collection.servlet.api.bridge.mpesa;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.codec.binary.Base64;

public class HttpURLConnectionExample {

	private final String USER_AGENT = "Mozilla/5.0";

	public static void main(String[] args) throws Exception {

		HttpURLConnectionExample http = new HttpURLConnectionExample();

		System.out.println("Testing 1 - Send Http GET request");
		http.doGet("https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials");

		// System.out.println("\nTesting 2 - Send Http POST request");
		// http.sendPost();

	}

	public String doGet(String httpsUrl) {
		URL url;
		String response = "";
		String app_key = "Gy8GgxKuQKzGSSoHKAhgScQZswIZZpGk";
		String app_secret = "a2TXsUs7lzE8ol0z";
		String appKeySecret = app_key + ":" + app_secret;

		System.out.println("Original String is " + appKeySecret);

		// encode data on your side using BASE64
		byte[] bytesEncoded = Base64.encodeBase64(appKeySecret.getBytes());
		String authEncoded = new String(bytesEncoded);

		// HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		try {
			// Create a context that doesn't check certificates.
			SSLContext sslContext = SSLContext.getInstance("TLS");
			TrustManager[] trustMgr = getTrustManager();

			sslContext.init(null, // key manager
					trustMgr, // trust manager
					new SecureRandom()); // random number generator
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

			url = new URL(httpsUrl);
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			// con.setRequestProperty("Content-Type", "application/json;charset=utf-8");
			con.setRequestProperty("Authorization", "Basic " + authEncoded);
			con.setRequestMethod("GET");
			// con.setDoOutput(false);
			// ######################################################################
			// Send data to the output
			// ######################################################################
			// sendData(con, params);

			// ######################################################################
			// Dump all the content
			// #######################################################################
			response = getContent(con);

		} catch (MalformedURLException e) {
			System.err.println("MalformedURLException");
			e.printStackTrace();

		} catch (IOException e) {
			System.err.println("IOException");
			e.printStackTrace();

		} catch (NoSuchAlgorithmException e) {
			System.err.println("NoSuchAlgorithmException");
			e.printStackTrace();

		} catch (KeyManagementException e) {
			System.err.println("KeyManagementException");
			e.printStackTrace();
		}

		System.out.println(response);

		return response;
	}

	// ##################################################################
	/**
	 * Send data to the url
	 * 
	 * @param con
	 */
	// #################################################################
	private void sendData(HttpsURLConnection con, String args) {
		if (con != null) {

			try {
				// send data to output
				OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());

				writer.write(args);
				writer.flush();
				writer.close();

			} catch (IOException e) {
				System.err.println("IOException");
				e.printStackTrace();
			}
		}
	}

	// ###############################################################
	/**
	 * @param con
	 * @throws IOException
	 */
	// ###############################################################
	private String getContent(HttpsURLConnection con) throws IOException {
		StringBuffer buff = new StringBuffer("");

		if (con != null) {

			try {

				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

				String input;

				while ((input = br.readLine()) != null) {
					buff.append(input + "\n");
				}
				br.close();

			} catch (IOException e) {
				// e.printStackTrace();
				// return buff.toString().trim();
				int code = con.getResponseCode();

				if (code == 400) {
					/**
					 * InputStream is = null; is = con.getErrorStream();
					 * 
					 * // Create an InputStream in order to extract the response // object
					 * 
					 * String result = convertStreamToString(is);
					 **/
					String result = "invalid results";

					buff.append(result + "\n");

				} else {
					e.printStackTrace();
				}

			}
		} // end 'if(con != null)'

		return buff.toString().trim();
	}

	// ################################################################
	/**
	 * @return {@link TrustManager}
	 */
	// ################################################################
	private TrustManager[] getTrustManager() {

		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };

		return certs;
	}

	private static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
}
package com.impalapay.collection.servlet.api.bridge.mpesa;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import javax.crypto.*;

public class SecurityPassword {

	public static String getEncrypted(String data, String Key) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		PublicKey publicKey = KeyFactory.getInstance("RSA")
				.generatePublic(new X509EncodedKeySpec(Base64.getDecoder().decode(Key.getBytes())));
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		byte[] encryptedbytes = cipher.doFinal(data.getBytes());
		return new String(Base64.getEncoder().encode(encryptedbytes));
	}

	public static String encryptInitiatorPassword(String securityCertificate, String password) {
		String encryptedPassword = "YOUR_INITIATOR_PASSWORD";
		try {
			// Security.addProvider(new
			// org.bouncycastle.jce.provider.BouncyCastleProvider());
			byte[] input = password.getBytes();

			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
			FileInputStream fin = new FileInputStream(new File(securityCertificate));
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			X509Certificate certificate = (X509Certificate) cf.generateCertificate(fin);
			PublicKey pk = certificate.getPublicKey();
			cipher.init(Cipher.ENCRYPT_MODE, pk);

			byte[] cipherText = cipher.doFinal(input);

			// Convert the resulting encrypted byte array into a string using base64
			// encoding
			encryptedPassword = new String(Base64.getEncoder().encode(cipherText));

		} catch (Exception e) {
			// TODO: handle exception
		}

		return encryptedPassword;
	}

}

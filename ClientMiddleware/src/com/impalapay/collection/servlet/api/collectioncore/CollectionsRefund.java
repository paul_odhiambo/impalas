package com.impalapay.collection.servlet.api.collectioncore;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.collection.beans.incoming.ProcessedCollection;
import com.impalapay.collection.beans.incoming.TempCollection;
import com.impalapay.collection.beans.refund.CollectionRefund;
import com.impalapay.collection.beans.refund.CollectionRefundHistory;
import com.impalapay.collection.persistence.processedtransactions.ProcessedTransactionDAOImpl;
import com.impalapay.collection.persistence.refund.CollectionRefundDAOImpl;
import com.impalapay.collection.persistence.refund.CollectionRefundHistoryDAOImpl;
import com.impalapay.collection.persistence.tempincoming.TempIncomingDAOImpl;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.clientipaddress.ClientIP;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.transaction.TransactionStatusDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class CollectionsRefund extends HttpServlet {

	private Cache accountsCache, clientIpCache;

	private TempIncomingDAOImpl tempincollectiontransactionDAO;
	
	private CollectionRefundDAOImpl collectionrefundDAO;
	
	private CollectionRefundHistoryDAOImpl collectionrefundhistoryDAO;

	private TransactionStatusDAO transactionstatusDAO;

	private HashMap<String, String> clientipHash = new HashMap<>();

	private HashMap<String, String> clientipaccountHash = new HashMap<>();

	private ProcessedTransactionDAOImpl processedtransactionDAO;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);
		clientIpCache = mgr.getCache(CacheVariables.CACHE_IPADDRESS_BY_UUID);

		tempincollectiontransactionDAO = TempIncomingDAOImpl.getInstance();
		
		collectionrefundDAO = CollectionRefundDAOImpl.getInstance();
		
		collectionrefundhistoryDAO = CollectionRefundHistoryDAOImpl.getInstance();

		processedtransactionDAO = ProcessedTransactionDAOImpl.getInstance();
		transactionstatusDAO = TransactionStatusDAO.getInstance();
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;
		CollectionRefund collectionrefund=null;

		// joined json string
		String join = "";
		JsonElement root = null;

		// These represent parameters received over the network
		String username = "", referencenumber = "", jsonResult = "",refundreason="",collectionrefundid="";
		
		String transactionstatusuuid =APIConstants.UUID_TOPUP_SUCESS;

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("api_username").getAsString();
			referencenumber = root.getAsJsonObject().get("reference_number").getAsString();
			refundreason = root.getAsJsonObject().get("refund_reason").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(referencenumber)) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		if (account == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// ip address module
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}

		List keys;

		// fetch from cache
		ClientIP clientIP;
		keys = clientIpCache.getKeys();
		for (Object key : keys) {
			element = clientIpCache.get(key);
			clientIP = (ClientIP) element.getObjectValue();
			clientipHash.put(clientIP.getUuid(), clientIP.getIpAddress());
		}

		keys = clientIpCache.getKeys();
		for (Object key : keys) {
			element = clientIpCache.get(key);
			clientIP = (ClientIP) element.getObjectValue();
			clientipaccountHash.put(clientIP.getIpAddress(), clientIP.getAccountUuid());
		}

		// compare remote address with the one stored in propertiesconfig
		if (!clientipHash.containsValue(ip)) {
			expected.put("your_ip", ip);
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_IPADDRESS);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		// ####################################################################
		// Check if the Provided Ip address matches with account used.
		// ####################################################################

		if (!StringUtils.equalsIgnoreCase(clientipaccountHash.get(ip), account.getUuid())) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_IPADDRESS_MISMATCH);

			return g.toJson(expected);
		}

		// At this point we check to see if there is no transaction with the
		// given reference number.

		// First step is to check if the transaction is still in Temp incoming
		// Transactions.
		TempCollection transaction = tempincollectiontransactionDAO.getTransactionstatus(referencenumber, account);

		if (transaction != null) {

			// Transaction still being processed wait for finals status

			expected.put("transaction_id", transaction.getUuid());
			expected.put("transaction_amount", String.valueOf(transaction.getAmount()));
			expected.put("sender_name", transaction.getDebitorname());
			expected.put("api_username", username);
			expected.put("transaction_status", "00029");
			expected.put("command_status", "TRANSACTION_INPROGRESS_REFUND_NOT_ALLOWED");
			jsonResult = g.toJson(expected);

			return jsonResult;

		}
		
		
		//CHECK IF TRANSACTION WAS ALREADY SUBMITTED FOR REFUND
		//temp_collectionrefund
		
		//CHECK IF TRANSACTION WAS ALREDAY REFUNDED
		//collectionrefundhistory
		List<ProcessedCollection> processtransaction1 = processedtransactionDAO.getTransactionstatus(referencenumber,
				account);

		if (processtransaction1.size() == 0) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_REFERENCENUMBER);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		List<ProcessedCollection> processtransaction = processedtransactionDAO.getTransactionstatus(referencenumber,transactionstatusuuid,
				account);

		if (processtransaction.size() == 0) {
			expected.put("command_status", "ONLY_SUCCESSFULLTRANSANSACTION_CANBEREVERSED");
			jsonResult = g.toJson(expected);

			return jsonResult;

		}
		
		
		CollectionRefundHistory collectionrefundhistory = collectionrefundhistoryDAO.getCollectionRefundHistoryByreference(referencenumber); 
		
		if(collectionrefundhistory!=null) {
			
			expected.put("command_status", "REVERSAL_PREVIOUSLY_ATTEMPTED_PLEASE_CONTACT_ADMIN");
			jsonResult = g.toJson(expected);

			return jsonResult;

			
		}

		ProcessedCollection returnprocesstransactions = processedtransactionDAO.getTransactionstatus1(referencenumber,
				account);

		
		collectionrefundid = StringUtils.remove(UUID.randomUUID().toString(), '-');
		
		collectionrefund = new CollectionRefund();
		

		collectionrefund.setUuid(collectionrefundid);
		collectionrefund.setAccountuuid(account.getUuid());
		collectionrefund.setReferencenumber(referencenumber);
		collectionrefund.setRefundreason(refundreason);
		collectionrefund.setCurrencyUuid(returnprocesstransactions.getDebitcurrency());
		collectionrefund.setAmount(returnprocesstransactions.getAmount());
		collectionrefund.setDateadded(new Date());
		
		if (!collectionrefundDAO.putCollectionRefund(collectionrefund)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL);
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		expected.put("transaction_id", returnprocesstransactions.getUuid());
		expected.put("api_username", username);
		expected.put("status_code", "S001");
		expected.put("remit_status", "REVERSAL_INPROGRESS");


		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.collection.servlet.api.collectioncore;

import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.accountmgmt.session.SessionConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.forex.ForexEngine;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.sessionlog.SessionLog;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.util.SecurityUtil;
import com.impalapay.airtel.util.CurrencyConvertUtil;
import com.impalapay.airtel.util.net.PostThreadUniversalUpdated;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
import com.impalapay.collection.beans.incoming.TempCollection;
import com.impalapay.collection.beans.network.CollectionDefine;
import com.impalapay.collection.beans.network.CollectionNetwork;
import com.impalapay.collection.persistence.network.CollectionDefineDAOImpl;
import com.impalapay.collection.persistence.processedtransactions.ProcessedTransactionDAOImpl;
import com.impalapay.collection.persistence.tempincoming.TempIncomingDAOImpl;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Allows for sending through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class DirectDebit extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1073286884431640677L;

	//private PostWithIgnoreSSL postMinusThread;
	private PostThreadUniversalUpdated postMinusThread;

	private TempIncomingDAOImpl tempincollectiontransactionDAO;

	private String TRANSACTIONSTATUS_INPROGRESSUUID = "5c9b8b0b-a035-4a07-bfd8-eccd4f039d53";

	private Cache accountsCache, countryCache, transactionStatusCache, forexCache;

	private Cache collectionnetworkCache;

	private SessionLogDAO sessionlogDAO;

	private CollectionDefineDAOImpl collectiondefineDAO;

	private HashMap<String, String> countryHash = new HashMap<>();

	private HashMap<String, String> countryCode = new HashMap<>();

	private HashMap<String, String> countryUuid = new HashMap<>();

	private HashMap<String, String> transactionStatusHash = new HashMap<>();

	private HashMap<String, String> statusDescriptionHash = new HashMap<>();

	private HashMap<String, Double> forexmarketratemap = new HashMap<>();

	private HashMap<String, Double> forexspreadratemap = new HashMap<>();

	private HashMap<String, String> collectionnetworkDebitUrlmap = new HashMap<>();

	private HashMap<String, String> collectionnetworkBridgeDebitUrlmap = new HashMap<>();

	private HashMap<String, String> collectionnetworkUsernamemap = new HashMap<>();

	private HashMap<String, String> collectionnetworkPasswordmap = new HashMap<>();

	private HashMap<String, String> collectionnetworkcountrymap = new HashMap<>();

	private HashMap<String, String> routenetworkuuiddefineuuidmap = new HashMap<>();

	private HashMap<String, Boolean> routenefixedcommissionmap = new HashMap<>();

	private HashMap<String, Double> routecommissionmap = new HashMap<>();

	private HashMap<String, String> routenetworksccountuuidmap = new HashMap<>();

	private HashMap<String, Boolean> routenesupportforexmap = new HashMap<>();

	private HashMap<String, String> collectionnetworknamemap = new HashMap<>();

	private HashMap<String, Boolean> collectionnetworksupportdebitmap = new HashMap<>();

	private ProcessedTransactionDAOImpl processedtransactionDAO;

	private String CLIENT_URL = "";

	private String networkroute = "";

	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		tempincollectiontransactionDAO = TempIncomingDAOImpl.getInstance();

		processedtransactionDAO = ProcessedTransactionDAOImpl.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);

		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);

		forexCache = mgr.getCache(CacheVariables.CACHE_FOREX_BY_UUID);

		collectionnetworkCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_NETWORK);

		sessionlogDAO = SessionLogDAO.getInstance();

		collectiondefineDAO = CollectionDefineDAOImpl.getInstance();

		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		OutputStream out = response.getOutputStream();
		// responseobject
		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(sendMoney(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return JSon response
	 * @throws IOException
	 */
	private String sendMoney(HttpServletRequest request) throws IOException {
		Account account = null;

		// String impalaexchange = "";

		double impalaexchangecalculate = 0, baseexchange = 0, convertedamountToWallet = 0, amount = 0,
				impalausdrate = 0, baseusdrate = 0;

		int finalconvertedamount = 0;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonObject vendorfields = null, root2 = null, creditrequest = null, root3 = null, responsetoreceiver = null;

		// These represent parameters received over the network
		String username = "", sessionid = "", collectionname = "", sendername = "", debitaccount = "",
				debitcurrencycode = "", referencenumber = "", clienttime = "", sendcurrency = "", routedefineuuid = "",
				debitcountrycode = "", jsonResult = "";

		// represents hashmaps values
		String apiusername = "", remiturlss = "", bridgeurl = "", apipassword = "", currentnetworkuuid = "",
				currentcollectiondefineuuid = "", currentaccountuuid = "", booleanforex = "true";

		boolean isdirectdebit = false, comissiontype = false, forexstatus = false;

		// route extract values
		String responseobject = "", switchresponse = "", statusdescription = "", statusuuid = "", success = "",
				inprogress = "", accountuuid = "", receiveruuid = "", unifiedstatusdescription = "", debitmobile = "";

		// prerequisites
		String amountstring = "", transactioinid = "", transactionforexhistoryuuid = "";

		String originatecurrency = "", terminatecurrency = "", currencypair = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), " ");

		logger.error(".....................................................");
		logger.error("DIRECT DEBIT REQUEST FROM CLIENT :" + join);
		logger.error(".....................................................");

		// ###########################################################
		// instantiate the JSon
		// ##########################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("api_username").getAsString();

			sessionid = root.getAsJsonObject().get("session_id").getAsString();

			sendername = root.getAsJsonObject().get("debitor_name").getAsString();

			collectionname = root.getAsJsonObject().get("route_name").getAsString();

			debitaccount = root.getAsJsonObject().get("debitor_account").getAsString();

			debitmobile = root.getAsJsonObject().get("debitor_mobile").getAsString();

			debitcurrencycode = root.getAsJsonObject().get("debitor_currency_code").getAsString();

			debitcountrycode = root.getAsJsonObject().get("debitor_country_code").getAsString();

			referencenumber = root.getAsJsonObject().get("reference_number").getAsString();

			amount = root.getAsJsonObject().get("amount").getAsDouble();

			clienttime = root.getAsJsonObject().get("client_datetime").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// ####################################################################
		// check for the presence of all required parameters
		// ####################################################################

		if (StringUtils.isBlank(username) || StringUtils.isBlank(sessionid) || StringUtils.isBlank(collectionname)
				|| StringUtils.isBlank(sendername) || StringUtils.isBlank(debitaccount)
				|| StringUtils.isBlank(debitcurrencycode) || StringUtils.isBlank(referencenumber)
				|| StringUtils.isBlank(clienttime) || StringUtils.isBlank(debitmobile)
				|| StringUtils.isBlank(debitcountrycode) || amount <= 0) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALIDEMPTY_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

			vendorfields = root.getAsJsonObject().get("vendor_uniquefields").getAsJsonObject();

		}

		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		if (account == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		originatecurrency = account.getAccounttype();

		// check if route has originate currency
		if (root2.has("Originate_currency")) {

			sendcurrency = root.getAsJsonObject().get("Originate_currency").getAsString();
			if (StringUtils.isBlank(sendcurrency)) {
				originatecurrency = account.getAccounttype();
			} else {
				originatecurrency = sendcurrency;
			}
		}

		// test for invalid sessionid
		SessionLog sessionlog = sessionlogDAO.getValidSessionLog(account);

		// ################################################################
		// Guard against all invalid sessionid error
		// ################################################################
		if (sessionlog == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		String session = sessionlog.getSessionUuid();

		if (!StringUtils.equals(SecurityUtil.getMD5Hash(sessionid), session)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		List keys;

		// fetch from cache
		Country country;
		keys = countryCache.getKeys();
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryHash.put(country.getCountrycode(), country.getCurrencycode());
		}

		// country and country uuid
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryCode.put(country.getCountrycode(), country.getUuid());
		}
		// country uuid and country code
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryUuid.put(country.getUuid(), country.getCountrycode());
		}

		// ************* forex with curency pairs****************//
		ForexEngine forexengine;
		keys = forexCache.getKeys();
		for (Object key : keys) {
			element = forexCache.get(key);
			forexengine = (ForexEngine) element.getObjectValue();
			forexmarketratemap.put(forexengine.getCurrencypair(), forexengine.getMarketrate());

		}

		keys = forexCache.getKeys();
		for (Object key : keys) {
			element = forexCache.get(key);
			forexengine = (ForexEngine) element.getObjectValue();
			forexspreadratemap.put(forexengine.getCurrencypair(), forexengine.getSpreadrate());

		}

		// **************Collection Network Cache****************//

		CollectionNetwork collectionnetwork;

		// collection name and collection network uuid

		keys = collectionnetworkCache.getKeys();
		for (Object key : keys) {
			element = collectionnetworkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			collectionnetworknamemap.put(collectionnetwork.getNetworkname(), collectionnetwork.getUuid());
		}

		// collectionnetwork name supportdebit
		keys = collectionnetworkCache.getKeys();
		for (Object key : keys) {
			element = collectionnetworkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			collectionnetworksupportdebitmap.put(collectionnetwork.getNetworkname(),
					collectionnetwork.isSupportdebit());
		}

		// collectionnetwork debit url
		keys = collectionnetworkCache.getKeys();
		for (Object key : keys) {
			element = collectionnetworkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			collectionnetworkDebitUrlmap.put(collectionnetwork.getUuid(), collectionnetwork.getDebiturl());
		}

		// collectionnetwork and username

		keys = collectionnetworkCache.getKeys();
		for (Object key : keys) {
			element = collectionnetworkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			collectionnetworkUsernamemap.put(collectionnetwork.getUuid(), collectionnetwork.getUsername());
		}

		// collectionnetwork and password
		keys = collectionnetworkCache.getKeys();
		for (Object key : keys) {
			element = collectionnetworkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			collectionnetworkPasswordmap.put(collectionnetwork.getUuid(), collectionnetwork.getPassword());
		}

		// collectionnetwork and bridgedebiturl
		keys = collectionnetworkCache.getKeys();
		for (Object key : keys) {
			element = collectionnetworkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			collectionnetworkBridgeDebitUrlmap.put(collectionnetwork.getUuid(), collectionnetwork.getBridgedebiturl());
		}

		// collectionnetwork and countryuuid

		keys = collectionnetworkCache.getKeys();
		for (Object key : keys) {
			element = collectionnetworkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			collectionnetworkcountrymap.put(collectionnetwork.getUuid(), collectionnetwork.getCountryuuid());
		}

		// ======================================================
		// Populate with the mapping of Transaction Statuses.
		// The key is a UUID of the status
		// ======================================================

		TransactionStatus status;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status.getStatus(), status.getUuid());
		}

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status = (TransactionStatus) element.getObjectValue();
			statusDescriptionHash.put(status.getStatus(), status.getDescription());
		}

		// ################################################################
		// Empty the below current Hashmaps(helps in updating deleted items)
		// ################################################################
		routenetworkuuiddefineuuidmap.clear();
		routenefixedcommissionmap.clear();
		routecommissionmap.clear();
		routenetworksccountuuidmap.clear();
		routenesupportforexmap.clear();

		// ##################################################################
		// checks for the provide currencyCode(invalid)
		// ##################################################################
		if (!countryHash.containsValue(debitcurrencycode)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_CURRENCYCODE);

			return g.toJson(expected);
		}

		// ##################################################################
		// checks for the provided countryCode(invalid)
		// ##################################################################
		if (!countryHash.containsKey(debitcountrycode)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_COUNTRYCODE);

			return g.toJson(expected);
		}

		// =========================================================
		// determines if the provided recipient currencyCode doesn't
		// correspond to the countryCode
		// =========================================================

		if (!StringUtils.equalsIgnoreCase(countryHash.get(debitcountrycode), debitcurrencycode)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_CURRENCY_COUNTRYMISMATCH);

			return g.toJson(expected);
		}

		// ##################################################################
		// checks and validate if the collection name provided is valid
		// ##################################################################

		if (!collectionnetworknamemap.containsKey(collectionname)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_COLLECTIONAME);

			return g.toJson(expected);
		}

		// ##################################################################
		// checks if the route support direct debit
		// ##################################################################
		isdirectdebit = collectionnetworksupportdebitmap.get(collectionname);

		if (!isdirectdebit) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_DIRECTDEBIT_NOTSUPPORTED);

			return g.toJson(expected);

		}

		// ################################################################
		// Fetch route set up details
		// #################################################################
		List<CollectionDefine> routedefine = collectiondefineDAO.getAllCollectionRoute(account);

		for (CollectionDefine routenetworkuuid : routedefine) {
			routenetworkuuiddefineuuidmap.put(routenetworkuuid.getNetworkuuid(), routenetworkuuid.getUuid());
			routenefixedcommissionmap.put(routenetworkuuid.getUuid(), routenetworkuuid.isFixedcommission());
			routecommissionmap.put(routenetworkuuid.getUuid(), routenetworkuuid.getCommission());
			routenetworksccountuuidmap.put(routenetworkuuid.getUuid(), routenetworkuuid.getAccountuuid());
			routenesupportforexmap.put(routenetworkuuid.getUuid(), routenetworkuuid.isSupportforex());
		}
		// ##################################################################
		// checks for the provide currencyCode(invalid)
		// ##################################################################
		if (!countryHash.containsValue(debitcurrencycode)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_CURRENCYCODE);

			return g.toJson(expected);
		}

		currentnetworkuuid = collectionnetworknamemap.get(collectionname);

		currentcollectiondefineuuid = routenetworkuuiddefineuuidmap.get(currentnetworkuuid);

		currentaccountuuid = routenetworksccountuuidmap.get(currentcollectiondefineuuid);

		if (StringUtils.isEmpty(currentaccountuuid) || StringUtils.isEmpty(currentcollectiondefineuuid)
				|| StringUtils.isEmpty(currentnetworkuuid)) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_ROUTEDEFINE_ERROR);

			return g.toJson(expected);
		}

		if (!currentaccountuuid.equalsIgnoreCase(account.getUuid())) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_ROUTEDEFINE_ERROR);

			return g.toJson(expected);
		}

		networkroute = currentnetworkuuid;

		routedefineuuid = currentcollectiondefineuuid;

		// check the url authentication details(username and password exist)
		if (!collectionnetworkDebitUrlmap.containsKey(networkroute)
				|| !collectionnetworkUsernamemap.containsKey(networkroute)
				|| !collectionnetworkPasswordmap.containsKey(networkroute)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_RECEIVER_ENDPOINT_AUTHENTICATE_ERROR);

			return g.toJson(expected);
		}

		// check if route allows fixed commission.
		comissiontype = routenefixedcommissionmap.get(routedefineuuid);

		forexstatus = routenesupportforexmap.get(routedefineuuid);

		// put if empty commission not yet defined to avoid errors

		// =============================================================================
		// Test to see if the provided reference number has previously been
		// used.
		// if reference number is a duplicate return duplicate reference number
		// response.
		// =============================================================================

		List referencetest = tempincollectiontransactionDAO.getTransactionReference(referencenumber);

		int size = referencetest.size();

		if (size != 0) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_DUPLICATE_REFERENCE);

			return g.toJson(expected);

		}

		// Check the same transaction uuid in Processed transactions Table.
		List referencetest1 = processedtransactionDAO.getAllProcessedTrans(referencenumber);

		int size1 = referencetest1.size();

		if (size1 != 0) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_DUPLICATE_REFERENCE + "_LEVEL2");
			return g.toJson(expected);

		}

		// select username password and route credentilas
		apiusername = collectionnetworkUsernamemap.get(networkroute);

		remiturlss = collectionnetworkDebitUrlmap.get(networkroute);

		bridgeurl = collectionnetworkBridgeDebitUrlmap.get(networkroute);

		apipassword = collectionnetworkPasswordmap.get(networkroute);

		if (StringUtils.isEmpty(apipassword) || StringUtils.isEmpty(apiusername)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_COUNTRYAUTH_ERROR);

			return g.toJson(expected);
		}

		// check if route suuports forex equates to true

		// originatecurrency = accounttype;
		terminatecurrency = debitcurrencycode;
		currencypair = originatecurrency + "/" + terminatecurrency;

		if (!forexspreadratemap.containsKey(currencypair)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_FOREX_ERROR);
			return g.toJson(expected);
		}

		if (booleanforex.equalsIgnoreCase(String.valueOf(forexstatus))) {
			// ###########################################################
			// fetch forex Module
			// ###########################################################
			// perform forex validation checks

			impalausdrate = forexspreadratemap.get(currencypair);

			baseusdrate = forexmarketratemap.get(currencypair);

			impalaexchangecalculate = impalausdrate;

			baseexchange = baseusdrate;

			convertedamountToWallet = CurrencyConvertUtil.multiplyForex(amount, impalaexchangecalculate);

		} else {
			// means no forex conversion is involved
			convertedamountToWallet = amount;
		}

		finalconvertedamount = CurrencyConvertUtil.doubleToInteger(convertedamountToWallet);

		double remainderamount = CurrencyConvertUtil.subtractDeficit(convertedamountToWallet, finalconvertedamount);

		// generate UUID then save transaction and sending to comviva wallet.
		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		// generate UUID then save transaction and sending to
		// transactionhistory.
		transactionforexhistoryuuid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		// #############################################################################################
		// construct a Mega-Json Object to route-transactions to internal
		// Servlet routing transactions.
		// #############################################################################################
		creditrequest = new JsonObject();

		creditrequest.addProperty("username", apiusername);
		creditrequest.addProperty("password", apipassword);
		creditrequest.addProperty("sendingIMT", username);
		creditrequest.addProperty("transaction_id", transactioinid);
		creditrequest.addProperty("debitcountrycode", debitcountrycode);
		creditrequest.addProperty("debitcurrencycode", debitcurrencycode);
		creditrequest.addProperty("amount", finalconvertedamount);
		creditrequest.addProperty("debitname", sendername);
		creditrequest.addProperty("debitaccount", debitaccount);
		creditrequest.addProperty("debitreferencenumber", referencenumber);
		creditrequest.addProperty("networkname", collectionname);
		creditrequest.addProperty("url", remiturlss);

		if (vendorfields != null) {
			creditrequest.add("vendor_uniquefields", vendorfields);
		}

		String jsonData = g.toJson(creditrequest);

		if (StringUtils.isNotEmpty(remiturlss)) {

			CLIENT_URL = bridgeurl;

			//postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonData);
			postMinusThread = new PostThreadUniversalUpdated(CLIENT_URL, jsonData);

			logger.error(".....................................................");
			logger.error("DIRECT DEBIT REQUEST TO BRIDGE ");
			logger.error(".....................................................");
			logger.error("DIRECT DEBIT BRIDGE REQUEST BRIDGE URI " + CLIENT_URL + "\n");
			logger.error("DIRECT DEBIT BRIDGE REQUEST OBJECT " + jsonData + "\n");

			try {
				// capture the switch respoinse.
				//responseobject = postMinusThread.doPost();
				responseobject = postMinusThread.runurl();
				// pass the returned json string
				roots = new JsonParser().parse(responseobject);

				// exctract a specific json element from the object(status_code)
				switchresponse = roots.getAsJsonObject().get("status_code").getAsString();

				// exctract a specific json element from the object(status_code)
				statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

				receiveruuid = roots.getAsJsonObject().get("am_referenceid").getAsString();

				root3 = roots.getAsJsonObject();

				logger.error(".....................................................");
				logger.error("DIRECT DEBIT REQUEST RESPONSE FROM BRIDGE ");
				logger.error(".....................................................");
				logger.error("RESPONSE " + root3.toString() + "\n");
			} catch (Exception e) {
				logger.error("Transaction id " + transactioinid + " was complete failure investigate immediately");
				switchresponse = "00032";
				statusdescription = APIConstants.COMMANDSTATUS_INTERNAL_ERROR;
			}

			if (!transactionStatusHash.containsKey(switchresponse)) {
				switchresponse = "00032";
				statusdescription = "UNKNOWN_ERROR";
			}

			// set the status UUID
			statusuuid = transactionStatusHash.get(switchresponse);

			success = "S000";

			inprogress = "S001";

			// the account UUID
			accountuuid = account.getUuid();

			unifiedstatusdescription = statusDescriptionHash.get(switchresponse);

			TempCollection incomingdebittransfer = new TempCollection();

			// server time
			Date now = new Date();

			incomingdebittransfer.setUuid(transactioinid);
			incomingdebittransfer.setNetworkuuid(currentnetworkuuid);
			incomingdebittransfer.setCreditaccountuuid(account.getUuid());
			incomingdebittransfer.setTransactiontype(SessionConstants.MOBILE_DEBIT);
			incomingdebittransfer.setDebitorname(sendername);
			incomingdebittransfer.setCollectiondefineuuid(currentcollectiondefineuuid);
			incomingdebittransfer.setDebitcurrency(countryCode.get(debitcountrycode));
			incomingdebittransfer.setDebitcountry(countryCode.get(debitcountrycode));
			incomingdebittransfer.setAmount(finalconvertedamount);
			incomingdebittransfer.setAccountreference("DIRECTDEBIT");
			incomingdebittransfer.setTransactionstatusuuid(statusuuid);
			incomingdebittransfer.setOriginatetransactionuuid(referencenumber);
			incomingdebittransfer.setReceivertransactionuuid(receiveruuid);
			incomingdebittransfer.setDebitedaccount(debitaccount);
			incomingdebittransfer.setOriginateamount(amount);
			incomingdebittransfer.setOriginatecurrency(originatecurrency);
			incomingdebittransfer.setProcessingstatus(TRANSACTIONSTATUS_INPROGRESSUUID);
			incomingdebittransfer.setEndpointstatusdescription(statusdescription);
			incomingdebittransfer.setServertime(now);

			if (!tempincollectiontransactionDAO.putTempIncomingTrans(incomingdebittransfer)) {
				expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL + " unable to add to database");
				jsonResult = g.toJson(expected);

				return jsonResult;
			}

			if (switchresponse.equalsIgnoreCase(success) || switchresponse.equalsIgnoreCase(inprogress)) {

				receiveruuid = roots.getAsJsonObject().get("am_referenceid").getAsString();

				if (root3.has("vendor_uniquefields")) {

					vendorfields = root3.getAsJsonObject().get("vendor_uniquefields").getAsJsonObject();

				}

				// new TransactionDispatcher(saved, transactionratehistory).start();
				responsetoreceiver = new JsonObject();
				responsetoreceiver.addProperty("api_username", username);
				responsetoreceiver.addProperty("transaction_id", transactioinid);
				responsetoreceiver.addProperty("command_status", APIConstants.COMMANDSTATUS_OK);
				responsetoreceiver.addProperty("status_code", switchresponse);
				responsetoreceiver.addProperty("remit_status", unifiedstatusdescription);

				if (vendorfields != null) {
					responsetoreceiver.add("vendor_uniquefields", vendorfields);

				}
				jsonResult = g.toJson(responsetoreceiver);
				return jsonResult;

			}

			// response to be returned if transaction fails
			expected.put("api_username", username);
			expected.put("transaction_id", transactioinid);
			expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL);
			expected.put("status_code", switchresponse);
			expected.put("remit_status", statusdescription);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		expected.put("api_username", username);
		expected.put("command_status", APIConstants.COMMANDSTATUS_UNOPERATIONAL_COUNTRY);
		jsonResult = g.toJson(expected);
		return jsonResult;

		// return "so far Success eugene "+jsonData;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

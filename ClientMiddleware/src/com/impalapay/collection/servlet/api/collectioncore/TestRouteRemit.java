package com.impalapay.collection.servlet.api.collectioncore;

import com.impalapay.airtel.servlet.api.APIConstants;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Tests the {@link AccountCheck}
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class TestRouteRemit {

	final String CGI_URL = "https://staging.impalapay.net/directdebit";

	/**
	 * Test method for
	 * {@link com.impalapay.airtel.servlet.api.status.QueryStatus#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 * .
	 */
	@Test
	public void testDoPostHttpServletRequestHttpServletResponse() {

		// ##########################################################
		// Test by calling the URL without all required parameters
		// ##########################################################
		Map<String, String> expected = new HashMap<>();
		expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);

		JsonObject creditrequest = new JsonObject();

		creditrequest.addProperty("api_username", "demo");
		creditrequest.addProperty("session_id", "1547a9e6fcb943118ee1cf148b3e4d77");
		creditrequest.addProperty("debitor_name", "nayan jain");
		creditrequest.addProperty("route_name", "MPESADIRECT");
		creditrequest.addProperty("debitor_account", "256715290374");
		creditrequest.addProperty("debitor_mobile", "256715290374");
		creditrequest.addProperty("debitor_currency_code", "KES");
		creditrequest.addProperty("debitor_country_code", "KE");
		creditrequest.addProperty("reference_number", "1535719478863");
		creditrequest.addProperty("amount", "10.0");
		creditrequest.addProperty("client_datetime", "2018-08-31T18:14:39+06:14");
		creditrequest.addProperty("Originate_currency", "KES");

		Gson g = new Gson();// GsonBuilder().setPrettyPrinting().serializeNulls().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		String jsonData = g.toJson(creditrequest);
		String jsonResult = g.toJson(expected);

		// System.out.println(jsonData);
		// assertEquals(getResponse(CGI_URL, jsonData), jsonResult);

		System.out.println("Initiating Comet request from code the URI is " + CGI_URL + "\n");

		System.out.println("This is My request from code  " + jsonData + "\n");

		System.out.println("The response from SwitchData" + getResponse(CGI_URL, jsonData));

		// ##########################################################
		// Test for an unknown username
		// ##########################################################
		Map<String, String> expected2 = new HashMap<>();
		expected2.put("command_status", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);

		JsonObject creditrequest2 = new JsonObject();

		creditrequest2.addProperty("username", "veve");
		creditrequest2.addProperty("password", "veve");
		creditrequest2.addProperty("sendingIMT", "veve");
		creditrequest2.addProperty("transaction_id", "veve");
		creditrequest2.addProperty("sourcecountrycode", "veve");
		creditrequest2.addProperty("recipientcurrencycode", "veve");
		creditrequest2.addProperty("recipientcountrycode", "veve");
		creditrequest2.addProperty("source_msisdn", "veve");
		creditrequest2.addProperty("beneficiary_msisdn", "veve");
		creditrequest2.addProperty("Sender_Name", "veve");
		creditrequest2.addProperty("amount", "veve");
		creditrequest2.addProperty("url", "veve");
		String jsonData2 = g.toJson(creditrequest2);
		String jsonResult2 = g.toJson(expected2);

		// System.out.println(jsonData);
		// assertEquals(getResponse(CGI_URL, jsonData2), jsonResult2);

		// ##########################################################
		// Test using correct parameters(without vendor unique fields)
		// ##########################################################
		// Map<String, String> expected4 = new HashMap<>();
		// expected3.put("command_status",
		// APIConstants.COMMANDSTATUS_INVALID_SESSIONID);

		JsonObject creditrequest3 = new JsonObject();

		creditrequest3.addProperty("username", "");
		creditrequest3.addProperty("password", "");
		creditrequest3.addProperty("sendingIMT", "");
		creditrequest3.addProperty("transaction_id", "");
		creditrequest3.addProperty("sourcecountrycode", "");
		creditrequest3.addProperty("recipientcurrencycode", "");
		creditrequest3.addProperty("recipientcountrycode", "");
		creditrequest3.addProperty("source_msisdn", "");
		creditrequest3.addProperty("beneficiary_msisdn", "");
		creditrequest3.addProperty("Sender_Name", "");
		creditrequest2.addProperty("amount", "");
		creditrequest2.addProperty("url", "");

		// String jsonData4 = g.toJson(creditrequest2);
		// String jsonResult4 = g.toJson(expected4);

		// System.out.println(jsonData);
		// assertEquals(getResponse(CGI_URL, jsonData4), jsonResult4);
		// System.out.println(getResponse(CGI_URL, jsonData4));

	}

	/**
	 * @param httpsUrl
	 * @param args
	 */
	private String getResponse(String httpsUrl, String args) {
		URL url;
		String response = "";

		try {
			// Create a context that doesn't check certificates.
			SSLContext sslContext = SSLContext.getInstance("TLS");
			TrustManager[] trustMgr = getTrustManager();

			sslContext.init(null, // key manager
					trustMgr, // trust manager
					new SecureRandom()); // random number generator
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

			url = new URL(httpsUrl);
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			con.setRequestMethod("POST");
			con.setDoOutput(true);

			// Guard against "bad hostname" errors during handshake.
			con.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost")) {
						return true;
					} else {
						return false;
					}
				}
			});

			// Send data to the output
			sendData(con, args);

			// Dump all cert info
			// printHttpsCert(con);

			// Dump all the content
			response = getContent(con);

		} catch (MalformedURLException e) {
			System.err.println("MalformedURLException");
			e.printStackTrace();

		} catch (IOException e) {
			System.err.println("IOException");
			e.printStackTrace();

		} catch (NoSuchAlgorithmException e) {
			System.err.println("NoSuchAlgorithmException");
			e.printStackTrace();

		} catch (KeyManagementException e) {
			System.err.println("KeyManagementException");
			e.printStackTrace();
		}

		return response;
	}

	/**
	 * Send data to the url
	 * 
	 * @param con
	 */
	private void sendData(HttpsURLConnection con, String args) {
		if (con != null) {

			try {
				// send data to output
				OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());

				writer.write(args);
				writer.flush();
				writer.close();

			} catch (IOException e) {
				System.err.println("IOException");
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param con
	 */
	private String getContent(HttpsURLConnection con) {
		StringBuffer buff = new StringBuffer("");

		if (con != null) {

			try {

				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

				String input;

				while ((input = br.readLine()) != null) {
					buff.append(input + "\n");
				}
				br.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		} // end 'if(con != null)'

		return buff.toString().trim();
	}

	/**
	 * @return {@link TrustManager}
	 */
	private TrustManager[] getTrustManager() {

		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };

		return certs;
	}

	/**
	 * @param con
	 */
	private void printHttpsCert(HttpsURLConnection con) {
		if (con != null) {

			try {
				System.out.println("Response Code : " + con.getResponseCode());
				System.out.println("Cipher Suite : " + con.getCipherSuite());
				System.out.println("\n");

				Certificate[] certs = con.getServerCertificates();

				for (Certificate cert : certs) {
					System.out.println("Cert Type : " + cert.getType());
					System.out.println("Cert Hash Code : " + cert.hashCode());
					System.out.println("Cert Public Key Algorithm : " + cert.getPublicKey().getAlgorithm());
					System.out.println("Cert Public Key Format : " + cert.getPublicKey().getFormat());
					System.out.println("\n");
				}

			} catch (SSLPeerUnverifiedException e) {
				System.err.println("SSLPeerUnverifiedException");
				e.printStackTrace();

			} catch (IOException e) {
				System.err.println("IOException");
				e.printStackTrace();
			}

		} // end 'if(con != null)'
	}
}
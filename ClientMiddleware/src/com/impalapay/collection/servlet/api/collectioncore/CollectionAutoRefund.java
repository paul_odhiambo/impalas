package com.impalapay.collection.servlet.api.collectioncore;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressMasterBalanceHoldHist;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostThreadUniversalEquity;
import com.impalapay.airtel.util.net.PostWithIgnoreSSAutoFwrd;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
import com.impalapay.collection.beans.balance.CollectionBalanceHistory;
import com.impalapay.collection.beans.incoming.ProcessedCollection;
import com.impalapay.collection.beans.network.CollectionNetwork;
import com.impalapay.collection.beans.refund.CollectionRefund;
import com.impalapay.collection.beans.refund.CollectionRefundHistory;
import com.impalapay.collection.persistence.accountmgmt.inprogressbalance.InProgressBalanceDAO;
import com.impalapay.collection.persistence.balance.CollectionBalanceDAOImpl;
import com.impalapay.collection.persistence.cashwithdraw.CheckerWithdrawHistoryDAOImpl;
import com.impalapay.collection.persistence.processedtransactions.ProcessedTransactionDAOImpl;
import com.impalapay.collection.persistence.refund.CollectionRefundHistoryDAOImpl;
import com.impalapay.collection.persistence.refund.CollectionTempRefundDAOImpl;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class CollectionAutoRefund extends HttpServlet {

	private Cache networkCache, transactionStatusCache, accountsCache, countryCache;

	private PostWithIgnoreSSL postMinusThread;

	private CollectionTempRefundDAOImpl tempcollectionrefundDAO;
	
	private CollectionRefundHistoryDAOImpl collectionrefundhistoryDAO;
	
	private ProcessedTransactionDAOImpl processedtransDAO;
	
	private CollectionBalanceDAOImpl collectionbalanceDAO;
	
	private CheckerWithdrawHistoryDAOImpl checkerwithdrawalhistoryDAO;

	private InProgressBalanceDAO inprogressbalanceholdDAO;

	private HashMap<String, String> countryCode = new HashMap<>();

	private HashMap<String, String> currencyCode = new HashMap<>();

	private HashMap<String, String> transactionStatusHash = new HashMap<>();

	private HashMap<String, String> transactionStatusuuidHash = new HashMap<>();

	private HashMap<String, String> networkReverseUrlmap = new HashMap<>();

	private HashMap<String, String> networkBridgeReverseUrlmap = new HashMap<>();

	private HashMap<String, String> networksupportreversemap = new HashMap<>();

	private HashMap<String, String> networkUsernamemap = new HashMap<>();

	private HashMap<String, String> networkPasswordmap = new HashMap<>();

	private HashMap<String, String> accountUsername = new HashMap<>();

	private HashMap<String, String> accountPassword = new HashMap<>();

	private String CLIENT_URL = "";
 
	private ProcessedCollection processedcollection;
	private CollectionRefundHistory refundhistory;
	private CollectionBalanceHistory deductbalance;
	private InProgressMasterBalanceHoldHist putcollectionbalanceonhold;
	private TransactionStatus statusoftransaction;
	

	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		tempcollectionrefundDAO = CollectionTempRefundDAOImpl.getInstance();
		collectionrefundhistoryDAO = CollectionRefundHistoryDAOImpl .getInstance();
		processedtransDAO = ProcessedTransactionDAOImpl.getInstance();
		collectionbalanceDAO = CollectionBalanceDAOImpl.getInstance();
		inprogressbalanceholdDAO = InProgressBalanceDAO.getInstance();
		checkerwithdrawalhistoryDAO = CheckerWithdrawHistoryDAOImpl.getInstance();

		networkCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_NETWORK);
		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_UUID);
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {

		// joined json string
		Account accounts,creditedacount;
		String join = "";
		JsonElement root = null, roots = null,root3=null;
		JsonObject refundrequest = null;

		// These represent parameters received over the network
		String transactionid = "", networkid = "", switchresponse = "", statusdescription = "";

		String receiverlisteningurl = "", jsonResult = "", responseobject = "", statusuuid = "", username = "",
				password = "", results2 = "",networkroute="",transactioinid2="",jsonData="",receiveruuid="";
		
		String apiusername="",remiturlss="",bridgeurl="",apipassword="",rejectedtransactionuuid = "6f017761-5ed2-47b6-b585-f525bfbd3664",successfullreversaluuid="b73a8c43-9758-48fb-a6d5-92816d357cab";

		//
		String Successfulltransaction = "S000", fail = "00029",successfullreversal="R000";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ####################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			transactionid = root.getAsJsonObject().get("transaction_id").getAsString();

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(transactionid)) {

			expected.put("status_code", "00032");
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		Element element;

		List keys;

		// **************Network Cache****************//

		CollectionNetwork collectionnetwork;

		// collection name and collection network uuid

		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkReverseUrlmap.put(collectionnetwork.getUuid(), collectionnetwork.getReversalurl());
		}

		// network and username
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkUsernamemap.put(collectionnetwork.getUuid(), collectionnetwork.getUsername());
		}

		// network and password
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkPasswordmap.put(collectionnetwork.getUuid(), collectionnetwork.getPassword());
		}

		// network and bridgeremiturl
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkBridgeReverseUrlmap.put(collectionnetwork.getUuid(), collectionnetwork.getBridgereversalurl());
		}

		// network and support querying
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networksupportreversemap.put(collectionnetwork.getUuid(),
					String.valueOf(collectionnetwork.isSupportreversal()));
		}

		// ======================================================
		// Populate with the mapping of Transaction Statuses.
		// The key is a UUID of the status
		// ======================================================

		TransactionStatus status1;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status1.getStatus(), status1.getUuid());
		}

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusuuidHash.put(status1.getUuid(), status1.getStatus());
		}

		// **************Accounts Cache****************//
		
		keys = accountsCache.getKeys();

		for (Object key : keys) {
			element = accountsCache.get(key);
			accounts = (Account) element.getObjectValue();
			accountUsername.put(accounts.getUuid(), accounts.getUsername());
		}

		for (Object key : keys) {
			element = accountsCache.get(key);
			accounts = (Account) element.getObjectValue();
			accountPassword.put(accounts.getUuid(), accounts.getApiPasswd());
		}

		// fetch from cache
		Country country;
		keys = countryCache.getKeys();
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			currencyCode.put(country.getUuid(), country.getCurrencycode());
		}

		// country and country uuid
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryCode.put(country.getUuid(), country.getCountrycode());
		}

		CollectionRefund transactions = tempcollectionrefundDAO.getCollectionTempRefund(transactionid);
		// String transactionref = transactions.getReferenceNumber();
		if ((transactions == null)) {
			expected.put("status_code", "00032");
			expected.put("command_status", "We have a problem fetching the above transaction");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}
		
		//Now fetch this transaction from processd transaction table
		creditedacount = new Account();
		creditedacount.setUuid(transactions.getAccountuuid());
		
		processedcollection = processedtransDAO.getTransactionstatus1(transactions.getReferencenumber(),creditedacount);
		
		if(processedcollection==null) {
			expected.put("status_code", "00032");
			expected.put("command_status", "Problem fetching transaction from processed collection table");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}
		
		networkroute = processedcollection.getNetworkuuid();

		// select username password and route credentilas
		apiusername = networkUsernamemap.get(networkroute);

		remiturlss = networkReverseUrlmap.get(networkroute);

		bridgeurl = networkBridgeReverseUrlmap.get(networkroute);

		apipassword = networkPasswordmap.get(networkroute);
		
		username = accountUsername.get(transactions.getAccountuuid());
		
		
		System.out.println("THE REVERSE URL "+remiturlss+" THE BRIDGE URL "+bridgeurl+"THE NETWORK ROUTE "+networkroute);

		if (StringUtils.isEmpty(apipassword) || StringUtils.isEmpty(apiusername) ||StringUtils.isEmpty(username) ) {
			//response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			expected.put("command_status", APIConstants.COMMANDSTATUS_COUNTRYAUTH_ERROR);

			return g.toJson(expected);
			
		}
		
		
		
		processedcollection.getOriginatetransactionuuid();
		
		networkroute = processedcollection.getNetworkuuid();
		
		transactioinid2 = StringUtils.remove(UUID.randomUUID().toString(), '-');
		
		//SEND BALANCE TO TEMPORARY HOLD TABLE
		//DEDUCT BALANCE
		//SEND REQUEST TO EXTERNAL SYSTEM
		deductbalance = new CollectionBalanceHistory();
		putcollectionbalanceonhold = new InProgressMasterBalanceHoldHist();
		
		
		putcollectionbalanceonhold.setUuid(transactions.getUuid());
		putcollectionbalanceonhold.setAccountUuid(transactions.getAccountuuid());
		putcollectionbalanceonhold.setAmount(transactions.getAmount());
		putcollectionbalanceonhold.setCurrency(transactions.getCurrencyUuid());
		putcollectionbalanceonhold.setTransactionuuid(transactions.getUuid());
		putcollectionbalanceonhold.setRefundedback(false);
		putcollectionbalanceonhold.setProcessed(false);
		putcollectionbalanceonhold.setTopuptime(new Date());

		deductbalance.setAccountuuid(transactions.getAccountuuid());
		deductbalance.setCountryuuid(transactions.getCurrencyUuid());
		deductbalance.setAmount(transactions.getAmount());
		
		//Addd transaction to tempary refunds Table history
		
		//advisable check if person has enough balance in collection Account first

		if(!collectionbalanceDAO.deductCollectionBalance(deductbalance)){
			
			System.out.println("WE HAVE A BIG PROBLEM HAPA");
			return "UNABLE TO DEDUCT AND SEND TO BALANCE ON HOLD";
			
		}
		
		
		if(!inprogressbalanceholdDAO.putBalanceOnHoldAccount(putcollectionbalanceonhold)) {
			
			//REFUND BACK THE BALANCE THAT WAS DEDUCTED ON STEP 1 /consider the process as FAILED
			
			return "UNABLE TO DEDUCT AND SEND TO BALANCE ON HOLD2";
		}
		
		
		refundrequest = new JsonObject();

		refundrequest.addProperty("username", apiusername);
		refundrequest.addProperty("password", apipassword);
		refundrequest.addProperty("sendingIMT", username);
		refundrequest.addProperty("referencenumber", transactions.getReferencenumber());
		refundrequest.addProperty("receivertransactionid", processedcollection.getReceivertransactionuuid());
		refundrequest.addProperty("refundurl", remiturlss);
		
		jsonData = g.toJson(refundrequest);
		
		

		if (StringUtils.isNotEmpty(remiturlss)) {

			CLIENT_URL = bridgeurl;
			
			postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonData);
			
			logger.error(".....................................................");
			logger.error("VISA/MASTERCARD REFUND TO BRIDGE ");
			logger.error(".....................................................");
			logger.error("VISA/MASTERCARD BRIDGE REQUEST BRIDGE URI " + CLIENT_URL + "\n");
			logger.error("VISA/MASTERCARD BRIDGE REQUEST OBJECT " + jsonData + "\n");
			
			try {
				// capture the switch respoinse.
				//responseobject = postMinusThread.doPost();
				responseobject = postMinusThread.doPost();
				// pass the returned json string
				roots = new JsonParser().parse(responseobject);

				// exctract a specific json element from the object(status_code)
				switchresponse = roots.getAsJsonObject().get("status_code").getAsString();

				// exctract a specific json element from the object(status_code)
				statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

				receiveruuid = roots.getAsJsonObject().get("am_referenceid").getAsString();

				root3 = roots.getAsJsonObject();

				
			} catch (Exception e) {
				logger.error("Referencenumber Refund id " + transactions.getReferencenumber() + " was complete failure investigate immediately");
				switchresponse = "00032";
				statusdescription = APIConstants.COMMANDSTATUS_INTERNAL_ERROR;
			}
			
			
			logger.error(".....................................................");
			logger.error("VISA/MASTERCARD REFUND RESPONSE FROM BRIDGE ");
			logger.error(".....................................................");
			logger.error("RESPONSE " + root3.toString() + "\n");

			if (!transactionStatusHash.containsKey(switchresponse)) {
				switchresponse = "00032";
				statusdescription = "UNKNOWN_ERROR";
			}

			// set the status UUID
			statusuuid = transactionStatusHash.get(switchresponse);
			
			refundhistory = new CollectionRefundHistory();

			refundhistory.setUuid(transactions.getUuid());
			refundhistory.setAccountuuid(transactions.getAccountuuid());
			refundhistory.setRefundreason(transactions.getRefundreason());
			refundhistory.setCurrencyUuid(transactions.getCurrencyUuid());
			refundhistory.setAmount(transactions.getAmount());
			
			
			refundhistory.setComission(transactions.getComission());
			refundhistory.setComissioncurrencyUuid(transactions.getComissioncurrencyUuid());
			refundhistory.setAdminextrainformation(transactions.getAdminextrainformation());
			refundhistory.setTransactionStatusUuid(statusuuid);
			refundhistory.setAuthorisedmaker(transactions.getAuthorisedmaker());
			refundhistory.setDeductamount(transactions.getDeductamount()); //consider adding with commission
			
			refundhistory.setReferencenumber(transactions.getReferencenumber());
			refundhistory.setTransactionrefunduuid(receiveruuid);
			
			refundhistory.setDateadded(new Date());
			
			
			
			if (switchresponse.equalsIgnoreCase(Successfulltransaction)) {
				//TransactionStatus reversalsuccess = new TransactionStatus();
				//reversalsuccess.setUuid(transactionStatusHash.get(successfullreversal));
				//MEANS TRANSACTION IS SUCCESSFULL
				statusoftransaction = new TransactionStatus();
				statusoftransaction.setUuid(successfullreversaluuid);
				
				System.out.println("TUKO HAPA JAMENI  WAKENYA");
				
				
				
				//DELETE TRANSACTION FROM TEMP TABLE
				tempcollectionrefundDAO.deleteCollectionTempRefund(transactionid);
				
				//UPDATE TEMP BALANCE ON HOLD
				inprogressbalanceholdDAO.removeBalanceHoldSuccess(transactions.getUuid());
				//SAVE TRANSACTION IN TEMP HISTORY
				collectionrefundhistoryDAO.putCollectionRefundHistory(refundhistory);
				
				//UPDATE TRANSACTION STATUS IN PROCESSED TABLE
				processedtransDAO.updateTransactionStatus(processedcollection.getUuid(), statusoftransaction);
			}else {
				
				System.out.println("TUKO HAPA JAMENI  UGANDANS");
				statusoftransaction = new TransactionStatus();
				statusoftransaction.setUuid(rejectedtransactionuuid);
				//UPDATE THE WITHDRAWAL TABLE WITH STATUS OF WITHDRAWAL
				checkerwithdrawalhistoryDAO.updateTransactionStatus(transactions.getUuid(), statusoftransaction);
				
				inprogressbalanceholdDAO.removeBalanceHoldFail(transactions.getUuid());
				
				//Means the Transaction has failed to refund
				//SAVE TRANSACTION IN TEMP HISTORY
				collectionrefundhistoryDAO.putCollectionRefundHistory(refundhistory);
				//DELETE TRANSACTION FROM TEMP TABLE
				tempcollectionrefundDAO.deleteCollectionTempRefund(transactionid);
				
			}
			
			expected.put("status_code", switchresponse);
			expected.put("status_description",statusdescription);
			
			jsonResult = g.toJson(expected);

			return jsonResult;
			
		}
		
		//REMIT URL IS EMPTY RETURN RESPONSE 
		expected.put("command_status", "FAILED");
		expected.put("remit_status","REFUND_URL_IS_EMPTY");
		jsonResult = g.toJson(expected);

		return jsonResult;
		

		

	}// end of inprogress status

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

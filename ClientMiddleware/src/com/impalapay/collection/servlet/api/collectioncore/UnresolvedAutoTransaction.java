package com.impalapay.collection.servlet.api.collectioncore;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.collection.beans.incoming.TempCollection;
import com.impalapay.collection.persistence.tempincoming.TempIncomingDAOImpl;
import com.impalapay.collection.persistence.tempincoming.UnresolvedIncomingDAOImpl;

import net.sf.ehcache.CacheManager;


import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class UnresolvedAutoTransaction extends HttpServlet {



	private TempIncomingDAOImpl transactionDAO;

	private UnresolvedIncomingDAOImpl unresolvedtransactionDAO;

	
	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		transactionDAO = TempIncomingDAOImpl.getInstance();
		unresolvedtransactionDAO = UnresolvedIncomingDAOImpl.getInstance();

		
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {

		// joined json string
		String join = "";
		JsonElement root = null;
	

		

		// These represent parameters received over the network
		String transactionid = "", networkid = "", jsonResult = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ####################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			transactionid = root.getAsJsonObject().get("transaction_id").getAsString();

			networkid = root.getAsJsonObject().get("networkuuid").getAsString();

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(transactionid) || StringUtils.isBlank(networkid)) {

			expected.put("status_code", "00032");
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		TempCollection transactions = transactionDAO.getTempIncomingTrans(transactionid);
		// String transactionref = transactions.getReferenceNumber();
		if ((transactions == null)) {
			expected.put("status_code", "00032");
			expected.put("command_status", "We have a problem at Unresolved Query under fetching transactions");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		
		

		TempCollection unresolvedtransfer = new TempCollection();

		// server time
		Date now = new Date();

		unresolvedtransfer.setUuid(transactions.getUuid());
		unresolvedtransfer.setNetworkuuid(transactions.getNetworkuuid());
		unresolvedtransfer.setCreditaccountuuid(transactions.getCreditaccountuuid());
		unresolvedtransfer.setTransactiontype(transactions.getTransactiontype());
		unresolvedtransfer.setDebitorname(transactions.getDebitorname());
		unresolvedtransfer.setCollectiondefineuuid(transactions.getCollectiondefineuuid());
		unresolvedtransfer.setDebitcurrency(transactions.getDebitcurrency());
		unresolvedtransfer.setDebitcountry(transactions.getDebitcountry());
		unresolvedtransfer.setAmount(transactions.getAmount());
		unresolvedtransfer.setAccountreference(transactions.getAccountreference());
		unresolvedtransfer.setTransactionstatusuuid(transactions.getTransactionstatusuuid());
		unresolvedtransfer.setOriginatetransactionuuid(transactions.getOriginatetransactionuuid());
		unresolvedtransfer.setReceivertransactionuuid(transactions.getReceivertransactionuuid());
		unresolvedtransfer.setDebitedaccount(transactions.getDebitedaccount());
		unresolvedtransfer.setOriginateamount(transactions.getOriginateamount());
		unresolvedtransfer.setOriginatecurrency(transactions.getOriginatecurrency());
		unresolvedtransfer.setProcessingstatus(transactions.getProcessingstatus());
		unresolvedtransfer.setEndpointstatusdescription(transactions.getEndpointstatusdescription());
		unresolvedtransfer.setServertime(now);



			if (!unresolvedtransactionDAO.putTempIncomingTrans(unresolvedtransfer)) {

				expected.put("status_code", "00032");
				expected.put("command_status",
						APIConstants.COMMANDSTATUS_FAIL + " unable to add to unresolved transactiondatabase");
				jsonResult = g.toJson(expected);

				return jsonResult;
			}

	       if (!transactionDAO.deleteTempTransaction(transactionid)) {
				// Delete from temp transaction if it refuses do marke the transaction under
				// dispute.)
				expected.put("status_code", "00032");
				expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL
						+ "Unable to Add on Collection Balance Table OR Delete from Temp transaction Table");
				jsonResult = g.toJson(expected);
			} else {

				expected.put("status_code", "S000");
				expected.put("command_status", APIConstants.COMMANDSTATUS_OK);
				jsonResult = g.toJson(expected);
			}

			return jsonResult;

	}
	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

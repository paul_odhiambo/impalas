package com.impalapay.collection.servlet.api.collectioncore;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.sessionlog.SessionLog;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.util.SecurityUtil;
import com.impalapay.collection.beans.network.CollectionNetwork;
import com.impalapay.collection.beans.network.CollectionType;

public class QueryCollectionNetwork extends HttpServlet {
	private Cache accountsCache, collectionnetworkCache, countryCache, collectiontypeCache;

	private SessionLogDAO sessionlogDAO;

	private HashMap<String, String> countryHash = new HashMap<>();

	private HashMap<String, String> collectiontypeHash = new HashMap<>();

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

		collectionnetworkCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_NETWORK);

		collectiontypeCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_CHANNEL);

		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);

		sessionlogDAO = SessionLogDAO.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(checkForex(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkForex(HttpServletRequest request) throws IOException {
		Account account = null;
		CollectionNetwork collectionnetwork = null;

		// These represent parameters received over the network
		String username = "", sessionid = "", jsonResult = "", jsonData = "";
		String join = "";
		JsonElement root = null;
		JsonObject collectionelements = null, mainrateobject = null, billpaymentresult = null;
		JsonArray mainarray;

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ##################################################################
		// instantiate the JSon
		// ##################################################################

		Gson g = new GsonBuilder().setPrettyPrinting().serializeNulls()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		LinkedHashMap<String, String> expected = new LinkedHashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("api_username").getAsString();

			sessionid = root.getAsJsonObject().get("session_id").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(sessionid)) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		if (account == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// test for invalid sessionid
		SessionLog sessionlog = sessionlogDAO.getValidSessionLog(account);

		// ################################################################
		// Guard against all invalid sessionid error
		// ################################################################

		if (sessionlog == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		String session = sessionlog.getSessionUuid();
		if (!StringUtils.equals(SecurityUtil.getMD5Hash(sessionid), session)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		List keys;
		Country country;
		keys = countryCache.getKeys();
		// place country uuid and country code in a hashmap
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryHash.put(country.getUuid(), country.getName());
		}

		// collectiontypeHash
		CollectionType collectiontype;
		keys = collectiontypeCache.getKeys();
		for (Object key : keys) {
			element = collectiontypeCache.get(key);
			collectiontype = (CollectionType) element.getObjectValue();
			collectiontypeHash.put(collectiontype.getUuid(), collectiontype.getType());
		}

		mainarray = new JsonArray();
		keys = collectionnetworkCache.getKeys();
		for (Object key : keys) {
			element = collectionnetworkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();

			collectionelements = new JsonObject();

			collectionelements.addProperty("CollectionName", collectionnetwork.getNetworkname());
			collectionelements.addProperty("CollectionType",collectiontypeHash.get(collectionnetwork.getCollectiontchannel()));
			collectionelements.addProperty("SupportDirectDebit", collectionnetwork.isSupportdebit());
			collectionelements.addProperty("Country", countryHash.get(collectionnetwork.getCountryuuid()));
			collectionelements.addProperty("Instructions", collectionnetwork.getInstruction());

			mainarray.add(collectionelements);

		}

		billpaymentresult = new JsonObject();
		billpaymentresult.addProperty("api_username", username);
		billpaymentresult.addProperty("command_status", APIConstants.COMMANDSTATUS_OK);

		billpaymentresult.add("Collection_Networks", mainarray);

		jsonResult = g.toJson(billpaymentresult);

		System.out.println(collectionnetworkCache.toString());

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

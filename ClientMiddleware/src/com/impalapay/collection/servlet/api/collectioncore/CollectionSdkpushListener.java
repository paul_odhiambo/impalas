package com.impalapay.collection.servlet.api.collectioncore;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.collection.beans.incoming.TempCollection;
import com.impalapay.collection.beans.network.CollectionNetwork;
import com.impalapay.collection.persistence.tempincoming.TempIncomingDAOImpl;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class CollectionSdkpushListener extends HttpServlet {

	private PostWithIgnoreSSL postMinusThread;
	
	private Logger logger;


	private Cache networkCache, transactionStatusCache;

	private TempIncomingDAOImpl transactionDAO;

	private HashMap<String, String> transactionStatusHash = new HashMap<>();

	private HashMap<String, String> transactionStatusuuidHash = new HashMap<>();

	private HashMap<String, String> networkQueryUrlmap = new HashMap<>();

	private HashMap<String, String> networkBridgeQueryUrlmap = new HashMap<>();

	private HashMap<String, String> networksupportquerymap = new HashMap<>();

	private HashMap<String, String> networkUsernamemap = new HashMap<>();

	private HashMap<String, String> networkPasswordmap = new HashMap<>();

	private String CLIENT_URL = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		transactionDAO = TempIncomingDAOImpl.getInstance();
		logger = Logger.getLogger(this.getClass());

		networkCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_NETWORK);
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		String responseobject = "";

		// joined json string
		String join = "";
		JsonElement root = null;
		
		TransactionStatus statusoftransaction =null,statusoftransactiondb =null;
		
		TempCollection transactions =null;

		// These represent parameters received over the network
		String transactionid = "", networkid = "", switchresponse = "", statusdescription = "",
				receiverstatusdesciption = "",receivertransactionid = "",receiverstatuscode = "",receiverstatusdecription = "";

		//
		String statusuuid="",statusuuiddb="",creditinprogresscode = "S001",jsonResult="",fail = "00029";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");
		
		
		logger.error(".....................................................");
		logger.error(" SDK PUSH LISTENERCORE BRIDGE INCOMING REQUEST :" + join);
		logger.error(".....................................................");

		// ####################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			receivertransactionid = root.getAsJsonObject().get("am_referenceid").getAsString();

			receiverstatuscode = root.getAsJsonObject().get("status_code").getAsString();

			receiverstatusdesciption = root.getAsJsonObject().get("status_description").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(receivertransactionid) || StringUtils.isBlank(receiverstatuscode)|| StringUtils.isBlank(receiverstatusdesciption)) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		Element element;

		List keys;

		// **************Network Cache****************//

		CollectionNetwork collectionnetwork;

		// collection name and collection network uuid

		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkQueryUrlmap.put(collectionnetwork.getUuid(), collectionnetwork.getQueryingurl());
		}

		// network and username
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkUsernamemap.put(collectionnetwork.getUuid(), collectionnetwork.getUsername());
		}

		// network and password
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkPasswordmap.put(collectionnetwork.getUuid(), collectionnetwork.getPassword());
		}

		// network and bridgeremiturl
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkBridgeQueryUrlmap.put(collectionnetwork.getUuid(), collectionnetwork.getBridgequeryurl());
		}

		// network and support querying
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networksupportquerymap.put(collectionnetwork.getUuid(),
					String.valueOf(collectionnetwork.isSupportquerying()));
		}

		// ======================================================
		// Populate with the mapping of Transaction Statuses.
		// The key is a UUID of the status
		// ======================================================

		TransactionStatus status1;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status1.getStatus(), status1.getUuid());
		}

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusuuidHash.put(status1.getUuid(), status1.getStatus());
		}

		
		//WES TART HERE CHECK IF TRANSACTION STATUS IS STILL INPROGRESS
		statusoftransaction = new TransactionStatus();
		statusuuid = transactionStatusHash.get(creditinprogresscode);
		statusoftransaction.setUuid(statusuuid);
		

		transactions = transactionDAO.getTransactionstatus(receivertransactionid,statusoftransaction);
		
		System.out.print("WHAT WE ARE FETCHING FROM DB UUID "+receivertransactionid+" status of transaction "+statusoftransaction+"\n");

		// String transactionref = transactions.getReferenceNumber();
		if ((transactions == null)) {
			expected.put("command_status", "We have a problem at sdk listener under fetching transactions");
			expected.put("status_code", fail);
			expected.put("status_description", statusdescription);
		     jsonResult = g.toJson(expected);
			return jsonResult;
		}

		

		// System.out.println(switchresponse);

		if (!transactionStatusHash.containsKey(receiverstatuscode)) {
			receiverstatuscode = "00032";
			statusdescription = "UNKNOWN_ERROR";
		} // Transaction forex

		// set the status UUID
		statusuuiddb = transactionStatusHash.get(receiverstatuscode);

		String success = "S000";

		
		transactionid = transactions.getUuid();
		if (receiverstatuscode.equalsIgnoreCase(success) || receiverstatuscode.equalsIgnoreCase(fail)) {

			statusoftransactiondb = new TransactionStatus();
			statusoftransactiondb.setUuid(statusuuiddb);
			statusoftransactiondb.setDescription(statusdescription);
			System.out.println("THE TRANSACTION ID "+transactionid+" THE STATUSUUID "+statusuuiddb);
			transactionDAO.updateTempIncomingTransactionStatus(transactionid, statusoftransactiondb);

			expected.put("status_code", success);
			expected.put("status_description", statusdescription);

			jsonResult = g.toJson(expected);
		}else {

		expected.put("status_code", fail);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);
		}
		
		logger.error(".....................................................");
		logger.error("COLLECTION SDK PUSH LISTENER REQUEST RESPONSE FROM BRIDGE ");
		logger.error(".....................................................");
		logger.error("RESPONSE " + jsonResult + "\n");


		return jsonResult;

	}// end of inprogress status

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

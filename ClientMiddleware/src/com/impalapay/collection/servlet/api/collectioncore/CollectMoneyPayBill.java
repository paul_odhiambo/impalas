package com.impalapay.collection.servlet.api.collectioncore;

import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.accountmgmt.session.SessionConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.util.PhonenumberSplitUtil;
import com.impalapay.collection.beans.incoming.ForwardCollection;
import com.impalapay.collection.beans.incoming.TempCollection;
import com.impalapay.collection.beans.network.CollectionDefine;
import com.impalapay.collection.beans.network.CollectionNetwork;
import com.impalapay.collection.beans.network.CollectionNetworkSubaccount;
import com.impalapay.collection.persistence.forwardcollection.ForwardCollectionDAOImpl;
import com.impalapay.collection.persistence.network.CollectionDefineDAOImpl;
import com.impalapay.collection.persistence.networksubaccount.CollectionNetworkSubaccountDAOImpl;
import com.impalapay.collection.persistence.processedtransactions.ProcessedTransactionDAOImpl;
import com.impalapay.collection.persistence.tempincoming.TempIncomingDAOImpl;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Allows for sending through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class CollectMoneyPayBill extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1073286884431640677L;

	private Cache collectionnetworkCache, collectionetworksubaccountCache, collectiondefineCache,
			transactionStatusCache, countryCache;

	private HashMap<String, String> collectionnetworknamemap = new HashMap<>();

	private HashMap<String, String> collectionnetworkaccountnumbermap = new HashMap<>();

	private HashMap<String, Integer> referencesplitlenghtmap = new HashMap<>();

	private HashMap<String, Boolean> sharedcollectionnetworkaccountmap = new HashMap<>();

	private HashMap<String, String> sharedcollectionnetworkaccountcurrencymap = new HashMap<>();

	private HashMap<String, String> collectiondefinereferencenetworkmap = new HashMap<>();

	private HashMap<String, String> collectiondefineuuidurimap = new HashMap<>();

	private HashMap<String, String> collectiondefinesubaccountreferencenetworkmap = new HashMap<>();

	private HashMap<String, String> transactionStatusHash = new HashMap<>();

	private HashMap<String, String> countryHash = new HashMap<>();

	private HashMap<String, String> countryCode = new HashMap<>();

	private HashMap<String, String> countryUuid = new HashMap<>();

	private CollectionNetworkSubaccountDAOImpl collectionetworkaccountDAO;

	private CollectionDefineDAOImpl collectiondefineDAO;

	private String TRANSACTIONSTATUS_INPROGRESSUUID = "5c9b8b0b-a035-4a07-bfd8-eccd4f039d53";

	private TempIncomingDAOImpl tempincollectiontransactionDAO;

	private ProcessedTransactionDAOImpl processedtransactionDAO;

	private ForwardCollectionDAOImpl forwardcolectionDAO;

	private PhonenumberSplitUtil referencenumbersplit;

	private String referencesplitresults = "";

	private int referencesplitlegnth = 0;

	private TempCollection incomingdebittransfer;

	private ForwardCollection forwardcollection;

	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		collectionnetworkCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_NETWORK);
		collectionetworksubaccountCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_NETWORK_SUBACCOUNT);
		collectiondefineCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_DEFINE);
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
		processedtransactionDAO = ProcessedTransactionDAOImpl.getInstance();
		collectionetworkaccountDAO = CollectionNetworkSubaccountDAOImpl.getInstance();
		collectiondefineDAO = CollectionDefineDAOImpl.getInstance();
		tempincollectiontransactionDAO = TempIncomingDAOImpl.getInstance();
		forwardcolectionDAO = ForwardCollectionDAOImpl.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		OutputStream out = response.getOutputStream();
		// responseobject
		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(sendMoney(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return JSon response
	 * @throws IOException
	 */
	private String sendMoney(HttpServletRequest request) throws IOException {
		Account account = null;

		// String impalaexchange = "";

		double amount = 0, switchcommission = 0, commission = 0;

		boolean fixedcommission, sharedcolllection;

		String iscollectionshared = "true";

		List keys;

		// joined json string
		String join = "";
		JsonElement root = null;
		JsonObject vendorfields = null, root2 = null;
		int size = 0;

		// These represent parameters received over the network
		String collectionnumber = "", sendername = "", sourceaccount = "", referencenumber = "", clienttime = "",
				debitcurrencycode = "";

		String networksubaccountuuid = "", collectionnetworkuuid = "", transactioinid = "", extrainformation = "",
				statusuuid = "", switchresponse = "";

		// prerequisites
		String sourcetransactioinid = "", jsonResult = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), " ");

		logger.error(".....................................................");
		logger.error("COLLECT MONEY PAYBILL LISTENER REQUEST :" + join);
		logger.error(".....................................................");

		// ###########################################################
		// instantiate the JSon
		// ##########################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			collectionnumber = root.getAsJsonObject().get("collection_number").getAsString();

			referencenumber = root.getAsJsonObject().get("reference_number").getAsString();

			sourceaccount = root.getAsJsonObject().get("debitor_account").getAsString();

			debitcurrencycode = root.getAsJsonObject().get("debitor_currency_code").getAsString();

			sourcetransactioinid = root.getAsJsonObject().get("debitor_transaction_id").getAsString();

			amount = root.getAsJsonObject().get("amount").getAsDouble();

			sendername = root.getAsJsonObject().get("debitor_name").getAsString();

			clienttime = root.getAsJsonObject().get("collection_datetime").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);

			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// ####################################################################
		// check for the presence of all required parameters
		// ####################################################################
		if (StringUtils.isBlank(referencenumber) || StringUtils.isBlank(sourceaccount)
				|| StringUtils.isBlank(sourcetransactioinid) || StringUtils.isBlank(sendername)
				|| StringUtils.isBlank(clienttime) || amount <= 0 || StringUtils.isEmpty(debitcurrencycode)) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALIDEMPTY_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

			vendorfields = root.getAsJsonObject().get("vendor_uniquefields").getAsJsonObject();

		}

		// Retrieve the account details then check against username and
		// sessionid
		Element element;

		// **************Country Cache****************//
		// fetch from cache
		Country country;
		keys = countryCache.getKeys();
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryHash.put(country.getCountrycode(), country.getCurrencycode());
		}

		// country uuid and country code
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryUuid.put(country.getCurrencycode(), country.getUuid());
		}

		// **************Network Cache****************//

		CollectionNetwork collectionnetwork;

		// collection name and collection network uuid

		keys = collectionnetworkCache.getKeys();
		for (Object key : keys) {
			element = collectionnetworkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			collectionnetworknamemap.put(collectionnetwork.getUuid(), collectionnetwork.getNetworkname());
		}

		// **************CollectionNetwork Paybill Cache****************//

		CollectionNetworkSubaccount collectionnetworksubaccount;
		keys = collectionetworksubaccountCache.getKeys();
		for (Object key : keys) {
			element = collectionetworksubaccountCache.get(key);
			collectionnetworksubaccount = (CollectionNetworkSubaccount) element.getObjectValue();
			collectionnetworkaccountnumbermap.put(collectionnetworksubaccount.getCollectionnumber(),
					collectionnetworksubaccount.getNetworkuuid());
		}

		keys = collectionetworksubaccountCache.getKeys();
		for (Object key : keys) {
			element = collectionetworksubaccountCache.get(key);
			collectionnetworksubaccount = (CollectionNetworkSubaccount) element.getObjectValue();
			referencesplitlenghtmap.put(collectionnetworksubaccount.getUuid(),
					collectionnetworksubaccount.getReferencesplitlength());
		}

		keys = collectionetworksubaccountCache.getKeys();
		for (Object key : keys) {
			element = collectionetworksubaccountCache.get(key);
			collectionnetworksubaccount = (CollectionNetworkSubaccount) element.getObjectValue();
			sharedcollectionnetworkaccountmap.put(collectionnetworksubaccount.getUuid(),
					collectionnetworksubaccount.isSharedconfiguration());
		}

		keys = collectionetworksubaccountCache.getKeys();
		for (Object key : keys) {
			element = collectionetworksubaccountCache.get(key);
			collectionnetworksubaccount = (CollectionNetworkSubaccount) element.getObjectValue();
			sharedcollectionnetworkaccountcurrencymap.put(collectionnetworksubaccount.getUuid(),
					collectionnetworksubaccount.getCurrency());
		}

		CollectionDefine collectiondefine;
		keys = collectiondefineCache.getKeys();
		for (Object key : keys) {
			element = collectiondefineCache.get(key);
			collectiondefine = (CollectionDefine) element.getObjectValue();
			// collectiondefinereferencenetworkmap.put(collectiondefine.getCollectionnetworksubaccountuuid(),collectiondefine.getReferenceprefix());
			collectiondefinereferencenetworkmap.put(collectiondefine.getUuid(), collectiondefine.getReferenceprefix());

		}

		keys = collectiondefineCache.getKeys();
		for (Object key : keys) {
			element = collectiondefineCache.get(key);
			collectiondefine = (CollectionDefine) element.getObjectValue();
			// collectiondefinereferencenetworkmap.put(collectiondefine.getCollectionnetworksubaccountuuid(),collectiondefine.getReferenceprefix());
			collectiondefineuuidurimap.put(collectiondefine.getUuid(), collectiondefine.getListeninguri());

		}

		keys = collectiondefineCache.getKeys();
		for (Object key : keys) {
			element = collectiondefineCache.get(key);
			collectiondefine = (CollectionDefine) element.getObjectValue();
			collectiondefinesubaccountreferencenetworkmap.put(collectiondefine.getCollectionnetworksubaccountuuid(),
					collectiondefine.getUuid());

		}

		// ======================================================
		// Populate with the mapping of Transaction Statuses.
		// The key is a UUID of the status
		// ======================================================

		TransactionStatus status;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status.getStatus(), status.getUuid());
		}

		// ##################################################################
		// checks for the provide currencyCode(invalid)
		// ##################################################################
		if (!countryHash.containsValue(debitcurrencycode)) {
			expected.put("status_code", "00032");
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_CURRENCYCODE);

			return g.toJson(expected);
		}

		if (!collectionnetworkaccountnumbermap.containsKey(collectionnumber)) {

			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALID_COLLECTIONAME);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		List referencetest2 = tempincollectiontransactionDAO.getAllTempIncomingTrans(sourcetransactioinid);

		int size2 = referencetest2.size();

		if (size2 != 0) {

			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_DUPLICATE_REFERENCE);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		// Check the same transaction uuid in Processed transactions Table.
		List referencetest1 = processedtransactionDAO.getAllProcessedTrans(sourcetransactioinid);

		int size1 = referencetest1.size();

		if (size1 != 0) {

			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_DUPLICATE_REFERENCE + "_LEVEL2");
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		// if the first step is successful get the collection network uuid to be
		// used to check in collection network account

		// Get the collectionnetwworkuuid that matches the collection number
		collectionnetworkuuid = collectionnetworkaccountnumbermap.get(collectionnumber);

		// Second step please select the collection network sub account uuid by
		// using collectionnetworkuuid and collectionnumber
		collectionnetworksubaccount = collectionetworkaccountDAO.getCollectionNetworkSubAcct(collectionnetworkuuid,
				collectionnumber);

		networksubaccountuuid = collectionnetworksubaccount.getUuid();

		// check if value contains a value or is null
		// also please check if the networksubaccount belongs in collectiondefinetable

		if (networksubaccountuuid == null
				|| !collectiondefinesubaccountreferencenetworkmap.containsKey(networksubaccountuuid)) {

			// returns null so it means the route is not predefined save to
			// dispute collections and return error
			expected.put("status_code", "00032");
			expected.put("status_description", "DISPUTE_COLLECTION");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check if the network is pre-shared

		iscollectionshared = "true";

		sharedcolllection = sharedcollectionnetworkaccountmap.get(networksubaccountuuid);

		if (iscollectionshared.equalsIgnoreCase(String.valueOf(sharedcolllection))) {

			// if preshared take the referenece split leghth split the reference
			// number to get the split prefix
			referencesplitlegnth = referencesplitlenghtmap.get(networksubaccountuuid);

			// split the received phone number
			referencenumbersplit = new PhonenumberSplitUtil();
			referencesplitresults = referencenumbersplit.PhonenumberSplitUtil(referencenumber, referencesplitlegnth);

			// check if the prefix matches any listed on hashmap.
			if (!collectiondefinereferencenetworkmap.containsValue(referencesplitresults)) {

				expected.put("status_code", "00032");
				expected.put("status_description", "NO_MATCH_FOR_REFERENCESPLIT");
				jsonResult = g.toJson(expected);

				return jsonResult;
			}

			// fetch from collectiondefine where the collectionnetworkuuid and
			// reference number equates the above
			collectiondefine = collectiondefineDAO.getCollectionRouteUuid(networksubaccountuuid, referencesplitresults);

		} else {

			// not shared
			collectiondefine = collectiondefineDAO.getCollectionRouteUuid(networksubaccountuuid);
		}

		if (collectiondefine == null) {

			// check if the collection define is empty if so save to disputed
			// transactions
			// return response

			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_ROUTEDEFINE_ERROR);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		fixedcommission = collectiondefine.isFixedcommission();
		commission = collectiondefine.getCommission();

		try {

			if (fixedcommission) {
				switchcommission = commission;
			} else {
				switchcommission = (commission * amount) / 100;
			}

		} catch (Exception e) {

			expected.put("status_code", "00032");
			expected.put("status_description", "ERROR_PROCESSING_COMMISSION");
			jsonResult = g.toJson(expected);

		}

		// calculate commision
		// check if the source_transaction_id is not duplicate

		switchresponse = "S000";
		statusuuid = transactionStatusHash.get(switchresponse);

		// change the vendor field object into string
		if (vendorfields != null) {
			extrainformation = String.valueOf(vendorfields);
		}

		// save in temporary transaction table.
		// generate UUID then save transaction and sending to comviva wallet.
		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		incomingdebittransfer = new TempCollection();
		forwardcollection = new ForwardCollection();

		// server time
		Date now = new Date();

		incomingdebittransfer.setUuid(transactioinid);
		incomingdebittransfer.setNetworkuuid(collectionnetworkuuid);
		incomingdebittransfer.setCreditaccountuuid(collectiondefine.getAccountuuid());
		incomingdebittransfer.setTransactiontype(SessionConstants.PAYBILL_DEBIT);
		incomingdebittransfer.setDebitorname(sendername);
		incomingdebittransfer.setCollectiondefineuuid(collectiondefine.getUuid());
		incomingdebittransfer.setDebitcurrency(countryUuid.get(debitcurrencycode));
		incomingdebittransfer.setDebitcountry(countryUuid.get(debitcurrencycode));
		incomingdebittransfer.setAmount(amount);
		incomingdebittransfer.setAccountreference(referencenumber);
		incomingdebittransfer.setVendorunique(extrainformation);
		incomingdebittransfer.setTransactionstatusuuid(statusuuid);
		incomingdebittransfer.setOriginatetransactionuuid(sourcetransactioinid);
		incomingdebittransfer.setDebitedaccount(sourceaccount);
		incomingdebittransfer.setOriginateamount(amount);
		incomingdebittransfer.setOriginatecurrency(debitcurrencycode);
		incomingdebittransfer.setProcessingstatus(TRANSACTIONSTATUS_INPROGRESSUUID);
		incomingdebittransfer.setServertime(now);

		forwardcollection.setUuid(transactioinid);
		forwardcollection.setCreditaccountuuid(collectiondefine.getAccountuuid());
		forwardcollection.setNetworkname(collectionnetworknamemap.get(collectionnetworkuuid));
		forwardcollection.setTransactiontype(SessionConstants.PAYBILL_DEBIT);
		forwardcollection.setDebitorname(sendername);
		forwardcollection.setDebitcurrency(countryUuid.get(debitcurrencycode));
		forwardcollection.setAmount(amount);
		forwardcollection.setAccountreference(referencenumber);
		forwardcollection.setDebitedaccount(sourceaccount);
		forwardcollection.setOriginatetransactionuuid(sourcetransactioinid);
		forwardcollection.setForwarduri(collectiondefine.getListeninguri());
		forwardcollection.setCollectiondate(now);

		// TransactiontransferType transfertypetrans = new TransactiontransferType();

		if (!tempincollectiontransactionDAO.putTempIncomingTrans(incomingdebittransfer)
				|| !forwardcolectionDAO.putForwardColletion(forwardcollection)) {
			// if (!forwardcolectionDAO.putForwardColletion(forwardcollection)) {

			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_FAIL + " unable to add to database");
			jsonResult = g.toJson(expected);
		} else {
			expected.put("status_code", "S000");
			expected.put("status_description", "SUCCESS");
			jsonResult = g.toJson(expected);
		}

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.collection.servlet.api.simulation;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.simulation.ComvivaSimulation;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;
import com.impalapay.airtel.persistence.simulation.ComvivaSimulationDAO;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class SimulatePayBillListener extends HttpServlet {

	private PostWithIgnoreSSL postMinusThread;
	private String CLIENT_URL = "";
	private Cache accountsCache;

	private SessionLogDAO sessionlogDAO;

	private ComvivaSimulationDAO comvivasimulationDAO;

	private HashMap<String, String> countryHash = new HashMap<>();

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

		sessionlogDAO = SessionLogDAO.getInstance();

		comvivasimulationDAO = ComvivaSimulationDAO.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkBalance(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkBalance(HttpServletRequest request) throws IOException {
		Account account = null;
		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonObject root2 = null, creditrequest = null, responsetoreceiver = null;

		// These represent parameters received over the network
		String transactiontype = "", transactioinid = "", thirdptransactioinid = "", transactiontime = "",
				amountstring = "", shortcode = "", referencenumber = "", invoicenumber = "", accountbalance = "",
				sourcemsisdn = "", firstname = "", middlename = "", lastname = "", remiturlss = "", responseobject = "",
				statuscode = "", statusdescription = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {

			root = new JsonParser().parse(join);
			// parse the JSon string
			transactiontype = root.getAsJsonObject().get("TransType").getAsString();

			transactioinid = root.getAsJsonObject().get("TransID").getAsString();

			transactiontime = root.getAsJsonObject().get("TransTime").getAsString();

			amountstring = root.getAsJsonObject().get("TransAmount").getAsString();

			shortcode = root.getAsJsonObject().get("BusinessShortCode").getAsString();

			referencenumber = root.getAsJsonObject().get("BillRefNumber").getAsString();

			invoicenumber = root.getAsJsonObject().get("InvoiceNumber").getAsString();

			accountbalance = root.getAsJsonObject().get("OrgAccountBalance").getAsString();

			thirdptransactioinid = root.getAsJsonObject().get("ThirdPartyTransID").getAsString();

			sourcemsisdn = root.getAsJsonObject().get("MSISDN").getAsString();

			firstname = root.getAsJsonObject().get("FirstName").getAsString();

			middlename = root.getAsJsonObject().get("MiddleName").getAsString();

			lastname = root.getAsJsonObject().get("LastName").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(transactioinid) || StringUtils.isBlank(transactiontime)
				|| StringUtils.isBlank(amountstring) || StringUtils.isBlank(shortcode)
				|| StringUtils.isBlank(referencenumber) || StringUtils.isBlank(accountbalance)
				|| StringUtils.isBlank(sourcemsisdn) || StringUtils.isBlank(firstname)
				|| StringUtils.isBlank(lastname)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALIDEMPTY_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		creditrequest = new JsonObject();

		creditrequest.addProperty("collection_number", shortcode);
		creditrequest.addProperty("reference_number", referencenumber);
		creditrequest.addProperty("debitor_account", sourcemsisdn);
		creditrequest.addProperty("debitor_currency_code", "KES");
		creditrequest.addProperty("debitor_transaction_id", transactioinid);
		creditrequest.addProperty("amount", amountstring);
		creditrequest.addProperty("debitor_name", amountstring);
		creditrequest.addProperty("collection_datetime", transactiontime);

		// assign the remit url from properties.config
		CLIENT_URL = PropertiesConfig.getConfigValue("COLLECTION_BRIDGEURL");

		String jsonData = g.toJson(creditrequest);

		postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonData);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {

			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			statuscode = roots.getAsJsonObject().get("status_code").getAsString();

			// exctract a specific json element from the object(status_code)
			statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			referencenumber = "investigateTransaction";
			statuscode = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
		}

		// unknown username
		/**
		 * if (account == null) { expected.put("command_status",APIConstants.
		 * COMMANDSTATUS_UNKNOWN_USERNAME); String jsonResult = g.toJson(expected);
		 * 
		 * return jsonResult; }
		 **/

		expected.put("status_code", statuscode);
		expected.put("am_referenceid", "test");
		expected.put("status_description", statusdescription);
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

}

package com.impalapay.collection.servlet.api.simulation;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.simulation.ComvivaSimulation;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;
import com.impalapay.airtel.persistence.simulation.ComvivaSimulationDAO;
import com.impalapay.airtel.servlet.api.APIConstants;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class SimulateDirectDebit extends HttpServlet {

	private Cache accountsCache;

	private SessionLogDAO sessionlogDAO;

	private ComvivaSimulationDAO comvivasimulationDAO;

	private HashMap<String, String> countryHash = new HashMap<>();

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

		sessionlogDAO = SessionLogDAO.getInstance();

		comvivasimulationDAO = ComvivaSimulationDAO.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkBalance(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkBalance(HttpServletRequest request) throws IOException {
		Account account = null;
		// joined json string
		String join = "";
		JsonElement root = null;

		// These represent parameters received over the network
		String username = "", sessionid = "", phonenumber = "", transactionid = "", beneficiarymsisdn = "", amount = "",
				sendingimt = "", sendername = "", sourcecurrency = "", sourcecountry = "", url = "", networkname = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {

			root = new JsonParser().parse(join);
			// parse the JSon string
			username = root.getAsJsonObject().get("username").getAsString();
			sessionid = root.getAsJsonObject().get("password").getAsString();
			sendingimt = root.getAsJsonObject().get("sendingIMT").getAsString();
			transactionid = root.getAsJsonObject().get("transaction_id").getAsString();
			sourcecountry = root.getAsJsonObject().get("debitcountrycode").getAsString();
			sourcecurrency = root.getAsJsonObject().get("debitcurrencycode").getAsString();
			amount = root.getAsJsonObject().get("amount").getAsString();
			sendername = root.getAsJsonObject().get("debitname").getAsString();
			phonenumber = root.getAsJsonObject().get("debitaccount").getAsString();
			sendername = root.getAsJsonObject().get("debitreferencenumber").getAsString();
			networkname = root.getAsJsonObject().get("networkname").getAsString();

			url = root.getAsJsonObject().get("url").getAsString();
		} catch (Exception e) {

			expected.put("status_code", "00032");
			expected.put("am_referenceid", "test");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(sessionid) || StringUtils.isBlank(phonenumber)
				|| StringUtils.isBlank(transactionid) || StringUtils.isBlank(amount)) {
			expected.put("status_code", "00032");
			expected.put("am_referenceid", "test");
			expected.put("status_description", "empty parameters");
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		/**
		 * if (account == null) { expected.put("command_status",APIConstants.
		 * COMMANDSTATUS_UNKNOWN_USERNAME); String jsonResult = g.toJson(expected);
		 * 
		 * return jsonResult; }
		 **/

		// fetch the specific response based on phone number.
		ComvivaSimulation testingkit = comvivasimulationDAO.getErrorphone(phonenumber);

		if (testingkit != null) {

			String statuscode = testingkit.getErrorcode();
			String errorDescription = testingkit.getErrorname();

			expected.put("am_referenceid", username);
			expected.put("am_timestamp", statuscode);
			expected.put("status_code", statuscode);
			expected.put("status_description", errorDescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("status_code", "00029");
		expected.put("am_referenceid", "test");
		expected.put("status_description", "USE_PROVIDED_MSISDN");
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

}

package com.impalapay.collection.servlet.api.simulation;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.google.gson.Gson;
import com.impalapay.airtel.persistence.transaction.TransactionDAO;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;

import org.apache.commons.lang3.StringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This Quartz job is used to check for Session Ids in the SessionLog database
 * table for expiry. Session Ids are expired after a fixed duration of time.
 * <p>
 * Copyright (c) Tawi Commercial Services Ltd., Sep 23, 2014
 * 
 * @author <a href="mailto:michael@tawi.mobi">Michael Wakahe</a>
 * 
 */

public class SimulatePayBillPoll implements Job {

	private PostWithIgnoreSSL postMinusThread;

	private TransactionDAO transactionDAO;

	private String TRANSACTIONSTATUS_UUID = "5c9b8b0b-a035-4a07-bfd8-eccd4f039d53";

	private String CLIENT_URL = "";
	private String responseobject = "";

	private HashMap<String, String> expected = new HashMap<>();

	/**
	 * Empty constructor for job initialization
	 * <p>
	 * Quartz requires a public empty constructor so that the scheduler can
	 * instantiate the class whenever it needs.
	 */
	public SimulatePayBillPoll() {

		transactionDAO = TransactionDAO.getInstance();
	}

	/**
	 * <p>
	 * Called by the <code>{@link org.quartz.Scheduler}</code> when a
	 * <code>{@link org.quartz.Trigger}</code> fires that is associated with the
	 * <code>Job</code>.
	 * </p>
	 * 
	 * @throws JobExecutionException
	 *             if there is an exception while executing the job.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Gson g = new Gson();

		// DateTime dateTime = new
		// DateTime().minusMinutes(SESSIONID_MINUTES_ALIVE);

		// categorise the transaction as invalid
		// temporaryDAO.expireTempTransaction(dateTime.toGregorianCalendar().getTime());
		// System.out.println("This will definately Rock");

		// delete all the invalidated transactions from the database.
		// temporaryDAO.deleteTempTransactionByTime();

		// TransactionStatus ts = new TransactionStatus();
		// ts.setUuid(TRANSACTIONSTATUS_UUID);
		// List<Transaction> transactionByStatus =
		// transactionDAO.getTransactionByStatusUuid(ts, 4);

		// for (Transaction t : transactionByStatus) {
		String transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		expected.put("TransactionType", "BILLPAYMENT");
		expected.put("TransID", transactioinid);
		expected.put("TransTime", new Date().toLocaleString());
		expected.put("TransAmount", "200");

		expected.put("BusinessShortCode", "8080");
		expected.put("BillRefNumber", "FSI22064576778");
		expected.put("InvoiceNumber", transactioinid);
		expected.put("OrgAccountBalance", "200000");

		expected.put("ThirdPartyTransID", transactioinid);
		expected.put("MSISDN", "254715290374");
		expected.put("FirstName", "Eugene");
		expected.put("MiddleName", "Chimita");
		expected.put("LastName", "Chimita");

		String jsonforxData = g.toJson(expected);

		CLIENT_URL = PropertiesConfig.getConfigValue("SIMULATEDEBITPAYBILL_URI");
		try {
			postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonforxData);

			// capture the switch respoinse.
			responseobject = postMinusThread.doPost();

			System.out.println(jsonforxData + " response " + responseobject + " ");
		} catch (Exception e) {
			System.out.println(jsonforxData + " automatic query error" + responseobject);
		}

		// Perform post request.
		// }

	}
}

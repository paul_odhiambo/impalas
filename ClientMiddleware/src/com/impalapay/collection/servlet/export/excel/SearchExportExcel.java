package com.impalapay.collection.servlet.export.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.persistence.util.DbFileUtils;
import com.impalapay.airtel.util.export.ZipUtil;
import com.impalapay.collection.util.export.transactions.SearchReportExportUtil;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * Allows the client to export a list of Transaction activity to a Microsoft
 * Excel sheet.
 * <p>
 * For a list of HTTP header fields, see
 * <a href="http://en.wikipedia.org/wiki/List_of_HTTP_header_fields"> http://en.
 * wikipedia.org/wiki/List_of_HTTP_header_fields} </a>
 * <p>
 * For a list of Microsoft Office MIME types, see
 * <a href="http://bit.ly/aZQzzH">http://bit.ly/aZQzzH</a>
 * <p>
 * Copyright (c) ImpalaPAY Ltd., Jan 31, 2014
 *
 * 
 * @author <a href="mailto:michael@impalapay.com">Michael Wakahe</a>
 * @version %I%, %G%
 */
public class SearchExportExcel extends HttpServlet {

	private final String SPREADSHEET_NAME = "CollectionExport.xlsx";
	private static final long serialVersionUID = 3897947782599L;

	private Cache accountsCache, countrysCache, transactionStatusCache;

	// This is a mapping between the UUIDs of countries and their names
	private HashMap<String, String> countryHash;

	// This is a mapping between the UUIDs of TransactionStatuses and their
	// status in English
	private HashMap<String, String> transactionStatusHash;

	private DbFileUtils dbFileUtils;
	private String accountuuid, countryuuid, networkuuid,statusuuid;
	private String addDay, addMonth, addYear, addDay2, addMonth2, addYear2;
	private Calendar f, t;
	private SimpleDateFormat format;
	private String parsefromDate = null, parsetoDate = null;
	private Date fromDate, toDate;

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);
		countrysCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
		f = Calendar.getInstance();
		t = Calendar.getInstance();

		countryHash = new HashMap<>();
		transactionStatusHash = new HashMap<>();

		List keys = countrysCache.getKeys();
		Element element;
		Country country;

		for (Object key : keys) {
			element = countrysCache.get(key);
			country = (Country) element.getObjectValue();
			countryHash.put(country.getUuid(), country.getName());
		}

		TransactionStatus transactionStatus;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			transactionStatus = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(transactionStatus.getUuid(), transactionStatus.getDescription());
		}

		dbFileUtils = DbFileUtils.getInstance();
	}

	/**
	 * Returns a zipped MS Excel file of the data specified for exporting.
	 *
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ServletOutputStream out = response.getOutputStream();
		response.setContentType("application/zip");
		response.setHeader("Cache-Control", "cache, must-revalidate");
		response.setHeader("Pragma", "public");

		HttpSession session = request.getSession(false);
		Account account;
		String fileName;

		fileName = new StringBuffer(SPREADSHEET_NAME).toString();

		// String exportExcelOption = request.getParameter("exportExcel");

		// String sessionUsername = (String)
		// session.getAttribute(SessionConstants.ACCOUNT_SIGN_IN_KEY);

		networkuuid = StringUtils.trimToEmpty(request.getParameter("networkUuid"));
		countryuuid = StringUtils.trimToEmpty(request.getParameter("countryUuid"));
		accountuuid = StringUtils.trimToEmpty(request.getParameter("accountUuid"));
		statusuuid = StringUtils.trimToEmpty(request.getParameter("transactionstatus"));
		addDay = StringUtils.trimToEmpty(request.getParameter("addDay"));
		addMonth = StringUtils.trimToEmpty(request.getParameter("addMonth"));
		addYear = StringUtils.trimToEmpty(request.getParameter("addYear"));
		addDay2 = StringUtils.trimToEmpty(request.getParameter("addDay2"));
		addMonth2 = StringUtils.trimToEmpty(request.getParameter("addMonth2"));
		addYear2 = StringUtils.trimToEmpty(request.getParameter("addYear2"));
		/**
		 * Element element = accountsCache.get(sessionUsername); account = (Account)
		 * element.getObjectValue();
		 * 
		 * fileName = new StringBuffer(account.getFirstName()).append(" ")
		 * .append(StringUtils.trimToEmpty(account.getLastName())).append("
		 * ").append(SPREADSHEET_NAME).toString();
		 **/

		response.setHeader("Content-Disposition",
				"attachment; filename=\"" + StringUtils.replace(fileName, ".xlsx", ".zip") + "\"");

		File excelFile = new File(FileUtils.getTempDirectoryPath() + File.separator + fileName);
		File csvFile = new File(StringUtils.replace(excelFile.getCanonicalPath(), ".xlsx", ".csv"));
		File zippedFile = new File(StringUtils.replace(excelFile.getCanonicalPath(), ".xlsx", ".zip"));

		// These are to determine whether or not we have created a CSV & Excel
		// file on disk
		boolean successCSVFile = true, successExcelFile = true;

		if (StringUtils.isNotBlank(networkuuid) || StringUtils.isNotBlank(countryuuid)
				|| StringUtils.isNotBlank(accountuuid) || StringUtils.isNotBlank(addDay)
				|| StringUtils.isNotBlank(addMonth) || StringUtils.isNotBlank(addYear)
				|| StringUtils.isNotBlank(addDay2) || StringUtils.isNotBlank(addMonth2)
				|| StringUtils.isNotBlank(addYear2) || StringUtils.isNotBlank(statusuuid)) {

			format = new SimpleDateFormat("yyyy-MM-dd");
			// Create the Dates
			f.set(NumberUtils.toInt(addYear), NumberUtils.toInt(addMonth) - 1, NumberUtils.toInt(addDay));
			t.set(NumberUtils.toInt(addYear2), NumberUtils.toInt(addMonth2) - 1, NumberUtils.toInt(addDay2));

			fromDate = f.getTime();
			toDate = t.getTime();

			parsefromDate = format.format(fromDate);
			parsetoDate = format.format(toDate);

			System.out.println("toDate " + toDate + "Year " + addDay2 + " MOnth " + addDay2 + " Date " + addDay2);

			successCSVFile = dbFileUtils.sqlResultToCSV(
					getExportTransactionsSqlQuery(accountuuid, countryuuid, networkuuid, parsefromDate, parsetoDate),
					csvFile.toString(), '|');

			if (successCSVFile) {
				successExcelFile = SearchReportExportUtil.createExcelExport(csvFile.toString(), "|",
						excelFile.toString());
			}

		} else { // export search results

			// return a string
		}

		if (successExcelFile) { // If we successfully created the MS Excel File
								// on disk
			// Zip the Excel file
			List<File> filesToZip = new ArrayList<>();
			filesToZip.add(excelFile);
			ZipUtil.compressFiles(filesToZip, zippedFile.toString());

			// Push the file to the request
			FileInputStream input = FileUtils.openInputStream(zippedFile);
			IOUtils.copy(input, out);
		}

		out.close();

		FileUtils.deleteQuietly(excelFile);
		FileUtils.deleteQuietly(csvFile);
		FileUtils.deleteQuietly(zippedFile);
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * Gets the String that will be used to export all the topup activity of an
	 * account holder.
	 * <p>
	 * Note that it is tied to the design of the database.
	 * 
	 * @param account
	 * @return the SQL query to be used
	 * 
	 * 
	 */

	private String getExportTransactionsSqlQuery(String imtaccount, String countryuuid, String networkuuid,
			String fromdate, String todate) {

		StringBuffer query = new StringBuffer(
				//"SELECT processedtransaction.servertime,processedtransaction.commission,processedtransaction.endpointstatusdescription,processedtransaction.originatecurrency, ")
				"SELECT processedtransaction.servertime,processedtransaction.commission,processedtransaction.originatecurrency, ")
						.append("processedtransaction.originateamount,processedtransaction.receivertransactionuuid,processedtransaction.originatetransactionuuid,processedtransaction.debitedaccount,transactionStatus.description,processedtransaction.accountreference,processedtransaction.amount,")
						//.append("country.currencycode,processedtransaction.collectiondefineuuid,processedtransaction.debitorname,processedtransaction.transactiontype,account.username,collection_network.networkname,processedtransaction.uuid ")
						.append("country.currencycode,processedtransaction.debitorname,processedtransaction.transactiontype,account.username,collection_network.networkname,processedtransaction.uuid ")
						.append("FROM processedtransaction ")
						//.append("INNER JOIN transactiontype ON processedtransaction.transactiontype=transactiontype.uuid ")
						.append("INNER JOIN country ON processedtransaction.debitcurrency=country.uuid ")
						.append("INNER JOIN collection_network ON processedtransaction.networkuuid=collection_network.uuid ")
						.append("INNER JOIN account ON processedtransaction.creditaccountuuid=account.uuid ")
						.append("INNER JOIN transactionStatus ON processedtransaction.transactionstatusUuid=transactionStatus.uuid ")
						.append("WHERE creditaccountuuid = '").append(imtaccount).append("' AND debitcountry = '")
						.append(countryuuid).append("' AND networkuuid = '").append(networkuuid).append("' AND transactionstatusuuid = '").append(statusuuid)
						.append("' AND processedtransaction.servertime >= '").append(fromdate)
						.append("' AND processedtransaction.servertime <= '").append(todate).append("';");
				

		System.out.println(query.toString());

		return query.toString();

	}

}

/*
 ** Local Variables: mode: java c-basic-offset: 2 tab-width: 2 indent-tabs-mode:
 * nil End:
 **
 ** ex: set softtabstop=2 tabstop=2 expandtab:
 **
 */

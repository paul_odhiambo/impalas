package com.impalapay.collection.servlet.admin.adjuststatus;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressMasterBalanceHoldHist;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.accountmgmt.ManageAccountDAO;
import com.impalapay.airtel.persistence.systemlog.SystemLogDAO;
import com.impalapay.airtel.persistence.transaction.TransactionStatusDAO;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.collection.beans.balance.CashWithdrawal;
import com.impalapay.collection.beans.balance.CollectionBalanceHistory;
import com.impalapay.collection.beans.incoming.CollectionUpdateStatus;
import com.impalapay.collection.beans.incoming.ProcessedCollection;
import com.impalapay.collection.beans.incoming.TempCollection;
import com.impalapay.collection.beans.refund.CollectionRefund;
import com.impalapay.collection.beans.refund.CollectionRefundHistory;
import com.impalapay.collection.persistence.accountmgmt.inprogressbalance.InProgressBalanceDAO;
import com.impalapay.collection.persistence.balance.CollectionBalanceDAOImpl;
import com.impalapay.collection.persistence.cashwithdraw.CheckerWithdrawHistoryDAOImpl;
import com.impalapay.collection.persistence.processedtransactions.ProcessedTransactionDAOImpl;
import com.impalapay.collection.persistence.refund.CollectionRefundDAOImpl;
import com.impalapay.collection.persistence.refund.CollectionRefundHistoryDAOImpl;
import com.impalapay.collection.persistence.refund.CollectionUpdateStatusDAOImpl;
import com.impalapay.collection.persistence.tempincoming.TempIncomingDAOImpl;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add/update forex.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Nov 24, 2014
 *
 * @author <a href="mailto:mike@impalapay.com">Michael Wakahe</a>
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 *
 */
public class ApproveChangeCollectionStatus extends HttpServlet {

	final String ERROR_NO_CHECKERCHNAGESTATUSUUID = "No checker change transactionstatus UUID";
	final String ERROR_NO_USERNAME = "No username provided";
	final String ERROR_NOTEFECTED = "This change is currently not allowed for the selcted status update";
	final String ERROR_FAIL = "Operation failed";
	final String ERROR_UNABLE_UPDATE = "unable to update Change .";
	final String ERROR_NORECORDS = "No records found on the updatetransaction table";
	final String ERROR_NOT_ALLOWED = "You dont have access rights to perform this Action,Please Contact System Administrator";
	final String SUCCESS = "You have successfully approved the transaction status change request";
	private String statusuuid, currentstatus, username, updatestatus, jsonResult;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;
	private Map<String, String> expected = new HashMap<>();
	private HashMap<String, String> transactionStatusHash = new HashMap<>();
	private HashMap<String, String> transactionStatusuuidHash = new HashMap<>();
	private HashMap<String, String> currencyCode = new HashMap<>();
	private Map<String, Double> balancemap = new HashMap<>();
	private SystemLogDAO systemlogDAO;
	private ManageAccountDAO managementaccountDAO;
	private CollectionUpdateStatus checkercollectionupdatestatus, collectionupdatehistory;
	private CollectionUpdateStatusDAOImpl collectionUpdateStatusDAO;
	private ProcessedTransactionDAOImpl processedtransactionDAO;
	private TempIncomingDAOImpl tempincollectiontransactionDAO;
	private TransactionStatus updatecollectionstatusobject;
	private TransactionStatusDAO transactionstatusDAO;
	private CollectionBalanceDAOImpl collectionbalanceDAO;
	private InProgressMasterBalanceHoldHist putcollectionbalanceonhold;
	private InProgressBalanceDAO inprogressbalanceholdDAO;
	private ProcessedCollection transactions;
	private TempCollection incomingdebittransfer;
	private CollectionBalanceHistory deductbalance;
	private CheckerWithdrawHistoryDAOImpl checkerwithdrawalhistoryDAO;
	private CollectionBalanceDAOImpl collectionDAO;
	private CollectionRefundDAOImpl collectionrefundDAO;
	private CollectionRefundHistoryDAOImpl collectionrefundhistoryDAO;
	private CollectionRefund collectionrefund;
	private CollectionRefundHistory refundhistory;
	private CashWithdrawal withdrawalrequest;
	private ManagementAccount managementaccount;
	private CollectionBalanceHistory newbalance;
	private boolean response;
	private JsonElement root2 = null;
	private Account account = null;
	private CacheManager cacheManager;
	private Cache transactionStatusCache, countryCache;
	private HttpSession session;
	private String transactioinid = "", success = "S000", unknownerror = "00032", inprogress = "S001",
			rejectedtransaction = "00029", reversedtransaction = "R000", statusdescription = "", statuscode = "",
			recipientcurrency = "", originatecurrency = "", receivedresponse = "";
	private double imtmasterbalance = 0, amount = 0, countryamount = 0, convertedamount = 0, exchangerate = 0;

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		// emailValidator = EmailValidator.getInstance();
		collectionUpdateStatusDAO = CollectionUpdateStatusDAOImpl.getinstance();
		managementaccountDAO = ManageAccountDAO.getInstance();
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
		transactionstatusDAO = TransactionStatusDAO.getInstance();
		processedtransactionDAO = ProcessedTransactionDAOImpl.getInstance();
		tempincollectiontransactionDAO = TempIncomingDAOImpl.getInstance();
		collectionbalanceDAO = CollectionBalanceDAOImpl.getInstance();
		checkerwithdrawalhistoryDAO = CheckerWithdrawHistoryDAOImpl.getInstance();
		inprogressbalanceholdDAO = InProgressBalanceDAO.getInstance();
		collectionrefundDAO = CollectionRefundDAOImpl.getInstance();
		collectionrefundhistoryDAO = CollectionRefundHistoryDAOImpl.getInstance();
		collectionDAO = CollectionBalanceDAOImpl.getInstance();
		cacheManager = CacheManager.getInstance();
		systemlogDAO = SystemLogDAO.getInstance();

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_PARAMETERS, paramHash);

		// No First currency provided
		if (StringUtils.isBlank(statusuuid)) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY,
					ERROR_NO_CHECKERCHNAGESTATUSUUID);

			// No Base rate provided
		} else if (StringUtils.isBlank(username)) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, ERROR_NO_USERNAME);

		} else if (!addforex()) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, ERROR_NOT_ALLOWED);

		} else {

			String receivedresponse = UpdateTransactionStatus();

			root2 = new JsonParser().parse(receivedresponse);
			statuscode = root2.getAsJsonObject().get("status_code").getAsString();
			statusdescription = root2.getAsJsonObject().get("status_description").getAsString();

			if (statuscode.equalsIgnoreCase(success)) {

				// If we get this far then all parameter checks are ok.
				session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_SUCCESS_KEY, "Successfull");

				// Reduce our session data
				session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_PARAMETERS, null);
				session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, null);

			} else {
				session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, statusdescription);
			}

		}

		// response.sendRedirect("addAccount.jsp");a
		response.sendRedirect("changestatuscollection.jsp");

	}

	/**
	 *
	 */
	private String UpdateTransactionStatus() {
		Gson g = new Gson();

		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		Element element;
		List keys;

		TransactionStatus status;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status.getUuid(), status.getStatus());
		}

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status = (TransactionStatus) element.getObjectValue();
			transactionStatusuuidHash.put(status.getStatus(), status.getUuid());
		}

		// fetch from cache
		Country country;
		keys = countryCache.getKeys();
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			currencyCode.put(country.getUuid(), country.getCurrencycode());
		}

		try {

			checkercollectionupdatestatus = collectionUpdateStatusDAO.getCollectionUpdateStatusUuid(statusuuid);

			updatestatus = transactionStatusHash.get(checkercollectionupdatestatus.getUpdatestatus());

			updatecollectionstatusobject = transactionstatusDAO
					.getTransactionStatus(checkercollectionupdatestatus.getUpdatestatus());

			currentstatus = transactionStatusHash.get(checkercollectionupdatestatus.getCurrentstatus());

			// transactions = processedtransactionDAO.getTransactionReference1(statusuuid);
			transactions = processedtransactionDAO.getProcessedTrans(statusuuid);

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("status_description", "VERY SERIOUS ERROR WHEN CHECKING RECORD " + statusuuid);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}
		if (checkercollectionupdatestatus == null) {
			expected.put("status_code", "00032");
			expected.put("status_description", ERROR_NORECORDS);
			jsonResult = g.toJson(expected);
		}
		if (transactions == null) {

			expected.put("status_code", "00032");
			expected.put("status_description", ERROR_NORECORDS + " THE statusuuid " + statusuuid);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// ========================
		// PREPARE OBJECT FOR COLLECTIONUPDATE HISTORY
		// ===================
		collectionupdatehistory = new CollectionUpdateStatus();
		deductbalance = new CollectionBalanceHistory();
		putcollectionbalanceonhold = new InProgressMasterBalanceHoldHist();
		withdrawalrequest = new CashWithdrawal();
		incomingdebittransfer = new TempCollection();
		collectionrefund = new CollectionRefund();
		refundhistory = new CollectionRefundHistory();
		newbalance = new CollectionBalanceHistory();


		collectionupdatehistory.setUuid(checkercollectionupdatestatus.getUuid());
		collectionupdatehistory
				.setOriginatetransactionuuid(checkercollectionupdatestatus.getOriginatetransactionuuid());
		collectionupdatehistory.setDebitcurrency(checkercollectionupdatestatus.getDebitcurrency());
		collectionupdatehistory.setOriginatecurrency(checkercollectionupdatestatus.getOriginatecurrency());
		collectionupdatehistory.setOriginateamount(checkercollectionupdatestatus.getOriginateamount());
		collectionupdatehistory.setAmount(checkercollectionupdatestatus.getAmount());
		collectionupdatehistory.setCurrentstatus(checkercollectionupdatestatus.getCurrentstatus());
		collectionupdatehistory.setUpdatestatus(checkercollectionupdatestatus.getUpdatestatus());
		collectionupdatehistory.setCreditaccountuuid(checkercollectionupdatestatus.getCreditaccountuuid());

		// ===========================
		// PREPARE TEMPCOLLECTIONOBJECT
		// ===========================

		// server time
		Date now = new Date();

		incomingdebittransfer.setUuid(transactions.getUuid());
		incomingdebittransfer.setNetworkuuid(transactions.getNetworkuuid());
		incomingdebittransfer.setCreditaccountuuid(transactions.getCreditaccountuuid());
		incomingdebittransfer.setTransactiontype(transactions.getTransactiontype());
		incomingdebittransfer.setDebitorname(transactions.getDebitorname());
		incomingdebittransfer.setCollectiondefineuuid(transactions.getCollectiondefineuuid());
		incomingdebittransfer.setDebitcurrency(transactions.getDebitcurrency());
		incomingdebittransfer.setDebitcountry(transactions.getDebitcountry());
		incomingdebittransfer.setAmount(transactions.getAmount());
		incomingdebittransfer.setAccountreference(transactions.getAccountreference());
		incomingdebittransfer.setOriginatetransactionuuid(transactions.getOriginatetransactionuuid());
		incomingdebittransfer.setReceivertransactionuuid(transactions.getReceivertransactionuuid());
		incomingdebittransfer.setDebitedaccount(transactions.getDebitedaccount());
		incomingdebittransfer.setOriginateamount(transactions.getOriginateamount());
		incomingdebittransfer.setOriginatecurrency(transactions.getOriginatecurrency());
		incomingdebittransfer.setEndpointstatusdescription("This Transaction is Reinitiated by System admin");
		incomingdebittransfer.setServertime(now);

		collectionrefund.setUuid(transactions.getUuid());
		collectionrefund.setAccountuuid(transactions.getCreditaccountuuid());
		collectionrefund.setReferencenumber(transactions.getOriginatetransactionuuid());
		collectionrefund.setRefundreason("Refund done by System administrator");
		collectionrefund.setCurrencyUuid(transactions.getDebitcurrency());
		collectionrefund.setAmount(transactions.getAmount());
		collectionrefund.setDateadded(new Date());

		refundhistory.setUuid(transactions.getUuid());
		refundhistory.setAccountuuid(transactions.getCreditaccountuuid());
		refundhistory.setRefundreason("Manual system initiated refund/Refund done by System administrator");
		refundhistory.setCurrencyUuid(transactions.getDebitcurrency());
		refundhistory.setAmount(transactions.getAmount());
		refundhistory.setComission(0);
		refundhistory.setComissioncurrencyUuid(transactions.getDebitcurrency());
		refundhistory.setAdminextrainformation("Commission calculated off system");
		refundhistory.setTransactionStatusUuid(transactionStatusuuidHash.get(success));
		refundhistory.setAuthorisedmaker(managementaccount.getUuid());
		refundhistory.setDeductamount(transactions.getAmount()); // consider adding
		// with commission
		refundhistory.setReferencenumber(transactions.getOriginatetransactionuuid());
		refundhistory.setTransactionrefunduuid(transactioinid);
		refundhistory.setDateadded(new Date());

		deductbalance.setAccountuuid(transactions.getCreditaccountuuid());
		deductbalance.setCountryuuid(transactions.getDebitcountry());
		deductbalance.setAmount(transactions.getAmount());

		putcollectionbalanceonhold.setUuid(transactioinid);
		putcollectionbalanceonhold.setAccountUuid(transactions.getCreditaccountuuid());
		putcollectionbalanceonhold.setAmount(transactions.getAmount());
		putcollectionbalanceonhold.setCurrency(transactions.getDebitcurrency());
		putcollectionbalanceonhold.setTransactionuuid(transactions.getUuid());
		putcollectionbalanceonhold.setRefundedback(true);
		putcollectionbalanceonhold.setProcessed(true);
		putcollectionbalanceonhold.setTopuptime(new Date());
		

		newbalance.setUuid(transactions.getUuid());
		newbalance.setAccountuuid(transactions.getCreditaccountuuid());
		newbalance.setCountryuuid(transactions.getDebitcurrency());
		newbalance.setAmount(transactions.getAmount());
		newbalance.setTransactionuuid(transactions.getUuid());
		newbalance.setTopupTime(new Date());

		withdrawalrequest.setUuid(transactions.getUuid());
		withdrawalrequest.setAccountuuid(transactions.getCreditaccountuuid());
		withdrawalrequest.setCurrencyUuid(transactions.getDebitcurrency());
		withdrawalrequest.setTocurrencyUuid(transactions.getDebitcurrency());
		withdrawalrequest.setExtrainformation("This is an adminin initiated Transactions");
		withdrawalrequest.setAmount(transactions.getAmount());
		withdrawalrequest.setComission(transactions.getCommission());
		withdrawalrequest.setComissioncurrencyUuid(transactions.getDebitcurrency());
		withdrawalrequest.setAdminextrainformation("This withdrawal is system initiated");
		withdrawalrequest.setSystemexchangerate(0);
		withdrawalrequest.setTransactionStatusUuid(transactionStatusuuidHash.get(success));
		withdrawalrequest.setBankwithdrawexchangerate(0);
		withdrawalrequest.setAuthorisedchecker(managementaccount.getUuid());
		withdrawalrequest.setAuthorisedmaker(managementaccount.getUuid());
		withdrawalrequest.setTransfercharges(0);
		withdrawalrequest.setTransferchargecurrencyUuid(transactions.getDebitcurrency());
		withdrawalrequest.setReceivableamount(transactions.getAmount());
		withdrawalrequest.setTransactionDate(new Date());

		// Check for the Balance by country
		// ##################################################
		// for a float based system the below is needed
		// ##################################################

		// fetch the list containing balance by country with the respective
		// balances

		// check current and update status

		// 1st Query current status of transaction is in progress and the Update is
		// failed transaction.
		// involves returning of balance from hold account.
		// involves checking transaction forex table
		if (currentstatus.equalsIgnoreCase(rejectedtransaction)) {

			// CHANGE STATUS OF REJECTED TRANSACTION TO REVERSED
			if (updatestatus.equalsIgnoreCase(reversedtransaction)) {

				// UPDATE STATUS ON PROCESSED TABLE
				// UPDATE TRANSACTION STATUS IN PROCESSED TABLE THEN REMOVE TRANSACTION FROM
				// COLLECTIONUPDATE TABLE
				if (!collectionUpdateStatusDAO.addCollectionUpdateStatusHistory(collectionupdatehistory)) {
					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_FAIL);
					jsonResult = g.toJson(expected);
				} else if (!processedtransactionDAO.updateTransactionStatus(transactions.getUuid(),
						updatecollectionstatusobject)) {
					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_FAIL);
					jsonResult = g.toJson(expected);

				} else {
					// remove from transactionupdatestatus
					collectionUpdateStatusDAO.deleteCollectionUpdateStatus(statusuuid);
					expected.put("status_code", "S000");
					expected.put("status_description", SUCCESS);
					jsonResult = g.toJson(expected);
				}
				// BELOW IS USED IN CASES WHERE TRANSACTION IS SUCCESSFULL ON NETWORK END BUT
				// MARKED AS FAILED ON SWITCH SIDE
			} else if (updatestatus.equalsIgnoreCase(inprogress)) {

				// MOVE TRANSACTION TO TEMP TRANSCTION TO ENABLE IT TO BE QUERIED BACK TO
				// NETWORK AGAIN
				// REMOVE TRANSACTION FROM PROCESSED TABLE
				// MOVE THE TRANSACTION TO COLLECTIONUPDATE HISTORY
				// REMOVE TRANSACTION FROM COLLECTIONUPDATE TABLE
				incomingdebittransfer.setProcessingstatus(transactionStatusuuidHash.get(inprogress));
				incomingdebittransfer.setTransactionstatusuuid(transactionStatusuuidHash.get(inprogress));// leave open

				//if (!collectionUpdateStatusDAO.addCollectionUpdateStatusHistory(collectionupdatehistory)) {
				//Because of reference between collectionupdatehistorytable and processed transaction table which we delete this transaction from
				//We save the transactionupdatestaus under deleteprocessedupdatestatushistory table to avoid foreigh key error
				if (!collectionUpdateStatusDAO.adddeleteprocessedUpdateStatusHistory(collectionupdatehistory)) {
				
					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_NOTEFECTED);
					jsonResult = g.toJson(expected);

				} else if (!tempincollectiontransactionDAO.putTempIncomingTrans(incomingdebittransfer)) {
					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_NOTEFECTED);
					jsonResult = g.toJson(expected);

				} else if (!collectionUpdateStatusDAO.deleteCollectionUpdateStatus(statusuuid)) {
					expected.put("status_code", "00032");
					expected.put("status_description", "UNABLE TO DELETE FROM COLLECTIONUPDATE TABLE");
					jsonResult = g.toJson(expected);
				} else {
					// remove from transactionupdatestatus
					processedtransactionDAO.DeleteProcessedTrans(statusuuid);
					// BEACAUSE THE TWO TABLES ARE RELATED PROCCESSED AND COLLECTIONUPDATE WE HAVE
					// TO DELETE COLLECTION UPDATE FIRST
					// BEFORE DELETING PROCESSED TRANSACTION
					expected.put("status_code", "S000");
					expected.put("status_description", SUCCESS);
					jsonResult = g.toJson(expected);
				}

			}else if (updatestatus.equalsIgnoreCase(success)) {
				//WHEN THE STATUS IS CHANGED TO SUCCESS
				// UPDATE STATUS ON PROCESSED TABLE
				// UPDATE TRANSACTION STATUS IN PROCESSED TABLE THEN REMOVE TRANSACTION FROM
				// COLLECTIONUPDATE TABLE
				if (!collectionUpdateStatusDAO.addCollectionUpdateStatusHistory(collectionupdatehistory)) {
					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_FAIL);
					jsonResult = g.toJson(expected);
				}else if(!collectionDAO.putCollectionBalance(newbalance)) {
					
					expected.put("status_code", "00032");
					expected.put("status_description", "Unable to Add the Balance");
					jsonResult = g.toJson(expected);
				} else if (!processedtransactionDAO.updateTransactionStatus(transactions.getUuid(),
						updatecollectionstatusobject)) {
					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_FAIL);
					jsonResult = g.toJson(expected);

				} else {
					// remove from transactionupdatestatus
					collectionUpdateStatusDAO.deleteCollectionUpdateStatus(statusuuid);
					expected.put("status_code", "S000");
					expected.put("status_description", SUCCESS);
					jsonResult = g.toJson(expected);
				}
			}

		} else if (currentstatus.equalsIgnoreCase(unknownerror)) {

			// ADJUST STATUS FROM UNKNOWN TO REJECTED TRANSACTION.MOSTLY USED ON NETWORK
			// TIMEOUTS AND CONFIRMATION IS DONE TO ASSURE TRANSACTION FAILED
			// UPDATE STATUS ON PROCESSED TABLE
			// UPDATE TRANSACTION STATUS IN PROCESSED TABLE THEN REMOVE TRANSACTION FROM
			// COLLECTIONUPDATE TABLE
			if (updatestatus.equalsIgnoreCase(rejectedtransaction)) {

				// UPDATE STATUS ON PROCESSED TABLE
				// UPDATE TRANSACTION STATUS IN PROCESSED TABLE THEN REMOVE TRANSACTION FROM
				// COLLECTIONUPDATE TABLE
				if (!collectionUpdateStatusDAO.addCollectionUpdateStatusHistory(collectionupdatehistory)) {
					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_FAIL);
					jsonResult = g.toJson(expected);
				} else if (!processedtransactionDAO.updateTransactionStatus(transactions.getUuid(),
						updatecollectionstatusobject)) {
					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_FAIL);
					jsonResult = g.toJson(expected);

				} else {
					// remove from transactionupdatestatus
					collectionUpdateStatusDAO.deleteCollectionUpdateStatus(statusuuid);
					expected.put("status_code", "S000");
					expected.put("status_description", SUCCESS);
					jsonResult = g.toJson(expected);
				}
				// BELOW IS USED IN CASES WHERE TRANSACTION IS SUCCESSFULL ON NETWORK END BUT
				// MIGHT HAVE FAILED DUE TO NETWORK DISCONNECTION

			} else if (updatestatus.equalsIgnoreCase(inprogress)) {

				// MOVE TRANSACTION TO TEMP TRANSCTION TO ENABLE IT TO BE QUERIED BACK TO
				// NETWORK AGAIN
				// REMOVE TRANSACTION FROM PROCESSED TABLE
				// MOVE THE TRANSACTION TO COLLECTIONUPDATE HISTORY
				// REMOVE TRANSACTION FROM COLLECTIONUPDATE TABLE
				incomingdebittransfer.setProcessingstatus(transactionStatusuuidHash.get(inprogress));
				incomingdebittransfer.setTransactionstatusuuid(transactionStatusuuidHash.get(inprogress));// leave open

				//if (!collectionUpdateStatusDAO.addCollectionUpdateStatusHistory(collectionupdatehistory)) {
				//Because of reference between collectionupdatehistorytable and processed transaction table which we delete this transaction from
				//We save the transactionupdatestaus under deleteprocessedupdatestatushistory table to avoid foreigh key error
				if (!collectionUpdateStatusDAO.adddeleteprocessedUpdateStatusHistory(collectionupdatehistory)) {
					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_NOTEFECTED);
					jsonResult = g.toJson(expected);

				} else if (!tempincollectiontransactionDAO.putTempIncomingTrans(incomingdebittransfer)) {
					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_NOTEFECTED);
					jsonResult = g.toJson(expected);

				} else if (!collectionUpdateStatusDAO.deleteCollectionUpdateStatus(statusuuid)) {
					expected.put("status_code", "00032");
					expected.put("status_description", "UNABLE TO DELETE FROM COLLECTIONUPDATE TABLE");
					jsonResult = g.toJson(expected);
				} else {
					// remove from transactionupdatestatus
					processedtransactionDAO.DeleteProcessedTrans(statusuuid);
					// BEACAUSE THE TWO TABLES ARE RELATED PROCCESSED AND COLLECTIONUPDATE WE HAVE
					// TO DELETE COLLECTION UPDATE FIRST
					// BEFORE DELETING PROCESSED TRANSACTION
					expected.put("status_code", "S000");
					expected.put("status_description", SUCCESS);
					jsonResult = g.toJson(expected);
				}

			} else if (updatestatus.equalsIgnoreCase(reversedtransaction)) {

				// UPDATE STATUS ON PROCESSED TABLE
				// UPDATE TRANSACTION STATUS IN PROCESSED TABLE THEN REMOVE TRANSACTION FROM
				// COLLECTIONUPDATE TABLE
				if (!collectionUpdateStatusDAO.addCollectionUpdateStatusHistory(collectionupdatehistory)) {
					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_FAIL);
					jsonResult = g.toJson(expected);
				} else if (!processedtransactionDAO.updateTransactionStatus(transactions.getUuid(),
						updatecollectionstatusobject)) {
					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_FAIL);
					jsonResult = g.toJson(expected);

				} else {
					// remove from transactionupdatestatus
					collectionUpdateStatusDAO.deleteCollectionUpdateStatus(statusuuid);
					expected.put("status_code", "S000");
					expected.put("status_description", SUCCESS);
					jsonResult = g.toJson(expected);
				}
				// BELOW IS USED IN CASES WHERE TRANSACTION IS SUCCESSFULL ON NETWORK END BUT
				// MARKED AS FAILED ON SWITCH SIDE
			}

		} else if (currentstatus.equalsIgnoreCase(success)) {

			// MAKE TRANSACTION ONLY ABLE TO MOVE FROM SUCCESS TO REVERSED ONLY

			if (updatestatus.equalsIgnoreCase(reversedtransaction)) {
				// ADD COLLECTION TO REFUND TABLE
				// UPDATE STATUS ON PROCESSED TABLE
				// UPDATE TRANSACTION STATUS IN PROCESSED TABLE THEN REMOVE TRANSACTION FROM
				// COLLECTIONUPDATE TABLE
				// DEDUCT CLIENT BALANCE FIRST CHECK IF THEY HAVE ENOUGH MONEY IN THAT CURRENCY

				// SEND BALANCE TO TEMPORARY HOLD TABLE
				// DEDUCT BALANCE
				// SEND REQUEST TO EXTERNAL SYSTEM

				// ADD TO REFUND TABLE

				if (!collectionrefundhistoryDAO.putCollectionRefundHistory(refundhistory)) {
					expected.put("status_code", "00032");
					expected.put("status_description", "UNABLE TO ADD TO COLLECTION REFUND HISTORY TABLE");
					jsonResult = g.toJson(expected);
					// return jsonResult+transactions.getCreditaccountuuid();
				} else if (!checkerwithdrawalhistoryDAO.putCheckerWithdrawHistory(withdrawalrequest)) {
					expected.put("status_code", "00032");
					expected.put("status_description", "UNABLE TO ADD TO WITHDRAW HISTORY");
					jsonResult = g.toJson(expected);
				} else if (!collectionbalanceDAO.deductCollectionBalance(deductbalance)) {
					expected.put("status_code", "00032");
					expected.put("status_description", "UNABLE TO DEDUCT BALANCE");
					jsonResult = g.toJson(expected);
					// Add Balance into withdrawalbalanceHoldhistory table
				} else if (!inprogressbalanceholdDAO.putBalanceOnHoldAccount(putcollectionbalanceonhold)) {
					expected.put("status_code", "00032");
					expected.put("status_description", "UNABLE TO ADD BALANCE INTO WITHDRAW HOLD");
					jsonResult = g.toJson(expected);
					// Update Transaction Status in processed transaction Table
				} else if (!processedtransactionDAO.updateTransactionStatus(transactions.getUuid(),
						updatecollectionstatusobject)) {
					expected.put("status_code", "00032");
					expected.put("status_description", "UNABLE TO UPDATE PROCESSEDTRANS TABLE");
					jsonResult = g.toJson(expected);
					// Add Details about the Status Change into collectionupdatestatushistory Table
				} else if (!collectionUpdateStatusDAO.addCollectionUpdateStatusHistory(collectionupdatehistory)) {

					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", "UNABLE TO ADD DETAILS INTO COLLECTIONHIST");
					jsonResult = g.toJson(expected);

				} else if (!collectionUpdateStatusDAO.deleteCollectionUpdateStatus(statusuuid)) {
					expected.put("status_code", "00032");
					expected.put("status_description", "UNABLE TO DELETE FROM COLLECTIONUPDATE TABLE");
					jsonResult = g.toJson(expected);
				} else if (!inprogressbalanceholdDAO.removeBalanceHoldSuccess(transactions.getUuid())) {
					expected.put("status_code", "00032");
					expected.put("status_description", "UNABLE TO DELETE FROM withdrawalbalanceHold TABLE");
					jsonResult = g.toJson(expected);
				} else {
					// remove from transactionupdatestatus

					// inprogressbalanceholdDAO.removeBalanceHoldSuccess(transactions.getUuid());

					// collectionUpdateStatusDAO.deleteCollectionUpdateStatus(statusuuid);
					expected.put("status_code", "S000");
					expected.put("status_description", SUCCESS);
					jsonResult = g.toJson(expected);
				}

				// System.out.print(transactions.getUuid());
				// expected.put("status_code", "00032");
				// expected.put("status_description", "we are facing a problem
				// "+refundhistory.toString());
				// jsonResult = g.toJson(expected);
				// return jsonResult;
			}

		} else {

			// Not allowed is the current setup
			// means the process failed
			expected.put("status_code", "00032");
			expected.put("status_description", ERROR_NOTEFECTED);
			jsonResult = g.toJson(expected);
		}

		SystemLog systemlog = new SystemLog();

		systemlog.setUsername(username);
		systemlog.setUuid(transactioinid);
		systemlog.setAction(username + " approved the currencypair " + " to spreadrate= " + " the marketrate= ");

		// response = false;
		return jsonResult;
	}

	public boolean addforex() {

		if (StringUtils.equals(username, PropertiesConfig.getConfigValue("ADMIN_USERNAME"))) {
			response = true;
		} else {
			managementaccount = managementaccountDAO.getAccountName(username);

			response = managementaccount.isChecker();
		}

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		statusuuid = StringUtils.trimToEmpty(request.getParameter("approve"));
		username = StringUtils.trimToEmpty(request.getParameter("username"));

	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		// response.sendRedirect("../index.jsp");
	}
}

/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */

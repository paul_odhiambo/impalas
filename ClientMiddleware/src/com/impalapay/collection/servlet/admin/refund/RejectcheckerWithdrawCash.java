package com.impalapay.collection.servlet.admin.refund;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.accountmgmt.ManageAccountDAO;
import com.impalapay.airtel.persistence.systemlog.SystemLogDAO;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.collection.beans.balance.CashWithdrawal;
import com.impalapay.collection.persistence.cashwithdraw.CheckerWithdrawDAOImpl;
import com.impalapay.collection.persistence.cashwithdraw.CheckerWithdrawHistoryDAOImpl;
import com.impalapay.collection.persistence.cashwithdraw.ClientWithdrawDAOImpl;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add/update forex.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Nov 24, 2014
 *
 * @author <a href="mailto:mike@impalapay.com">Michael Wakahe</a>
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 *
 */
public class RejectcheckerWithdrawCash extends HttpServlet {

	final String ERROR_NO_BALANCEUUID = "Please provide the client withdrawal Unique indetifier.";
	final String ERROR_NO_USERNAME = "No username provided";
	final String ERROR_UNABLE_DELETE = "unable to Reject client Withdrawal Request";
	final String ERROR_NOT_ALLOWED = "You dont have access rights to perform this Action,Please Contact System Administrator";
	final String SUCCESS = "You have successfully Rejected a withdrawal request as a checker";
	private String clientwithdrawuuid, createdpair, username;
	private double baserate = 0, impalarate = 0;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;

	private SystemLogDAO systemlogDAO;
	private ManageAccountDAO managementaccountDAO;
	private ClientWithdrawDAOImpl cashwithdrawDAO;
	private CheckerWithdrawDAOImpl checkerwithdrawalDAO;
	private CheckerWithdrawHistoryDAOImpl checkerwithdrawalhistoryDAO;
	private boolean response;
	private CashWithdrawal withdrawalhistoryrequest, clientwithdrawalobject;
	private Cache accountsCache;
	private HttpSession session;
	private ManagementAccount account = null;
	private String transactioinid = "", rejectedtransactionuuid = "6f017761-5ed2-47b6-b585-f525bfbd3664";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		cashwithdrawDAO = ClientWithdrawDAOImpl.getInstance();
		checkerwithdrawalDAO = CheckerWithdrawDAOImpl.getInstance();
		checkerwithdrawalhistoryDAO = CheckerWithdrawHistoryDAOImpl.getInstance();
		managementaccountDAO = ManageAccountDAO.getInstance();

		CacheManager.getInstance();
		systemlogDAO = SystemLogDAO.getInstance();
		accountsCache = mgr.getCache(CacheVariables.CACHE_MANAGEMENTACCOUNTS_BY_USERNAME);

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		session = request.getSession(true);

		setClassParameters(request);

		initParamHash();

		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (ManagementAccount) element.getObjectValue();
		}
		session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, paramHash);

		// No First currency provided
		if (StringUtils.isBlank(clientwithdrawuuid)) {
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, ERROR_NO_BALANCEUUID);

			// No Base rate provided
		} else if (StringUtils.isBlank(username)) {
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, ERROR_NO_USERNAME);

		} else if (!addwithdrawal()) {
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, ERROR_NOT_ALLOWED);

		} else {

			if (!deletecheckerForex()) {
				session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, ERROR_UNABLE_DELETE);
			}

			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_SUCCESS_KEY, SUCCESS);

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, null);

		}

		response.sendRedirect("clientwithdrawal.jsp");

	}

	/**
	 *
	 */
	private boolean deletecheckerForex() {
		response = false;
		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		clientwithdrawalobject = cashwithdrawDAO.getClientWithdraw(clientwithdrawuuid);

		if (clientwithdrawalobject != null) {

			withdrawalhistoryrequest = new CashWithdrawal();
			try {
				withdrawalhistoryrequest.setUuid(transactioinid);
				withdrawalhistoryrequest.setAccountuuid(clientwithdrawalobject.getAccountuuid());
				withdrawalhistoryrequest.setCurrencyUuid(clientwithdrawalobject.getCurrencyUuid());
				withdrawalhistoryrequest.setTocurrencyUuid(clientwithdrawalobject.getTocurrencyUuid());
				withdrawalhistoryrequest.setExtrainformation(clientwithdrawalobject.getExtrainformation());
				withdrawalhistoryrequest.setAmount(clientwithdrawalobject.getAmount());
				withdrawalhistoryrequest.setComission(0);
				withdrawalhistoryrequest.setComissioncurrencyUuid(clientwithdrawalobject.getCurrencyUuid());
				withdrawalhistoryrequest.setAdminextrainformation("This transaction was Rejected");
				withdrawalhistoryrequest.setSystemexchangerate(0);
				withdrawalhistoryrequest.setTransactionStatusUuid(rejectedtransactionuuid);
				withdrawalhistoryrequest.setBankwithdrawexchangerate(0);
				withdrawalhistoryrequest.setAuthorisedchecker(account.getUuid());
				withdrawalhistoryrequest.setAuthorisedmaker(account.getUuid());
				withdrawalhistoryrequest.setTransfercharges(0);
				withdrawalhistoryrequest.setTransferchargecurrencyUuid(clientwithdrawalobject.getTocurrencyUuid());
				withdrawalhistoryrequest.setReceivableamount(0);

				withdrawalhistoryrequest.setTransactionDate(new Date());

			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("The account we are looking at " + account.getAccountName());
			}

			SystemLog systemlog = new SystemLog();
			transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');
			systemlog.setUsername(username);
			systemlog.setUuid(transactioinid);
			systemlog.setAction(
					username + " deleted the checker withdrawal request with transactionuuid" + transactioinid);

			if (checkerwithdrawalhistoryDAO.putCheckerWithdrawHistory(withdrawalhistoryrequest)
					&& cashwithdrawDAO.deleteClientWithdraw(clientwithdrawuuid)) {
				systemlogDAO.putsystemlog(systemlog);
				response = true;

			}

		}

		return response;
	}

	public boolean addwithdrawal() {

		if (StringUtils.equals(username, PropertiesConfig.getConfigValue("ADMIN_USERNAME"))) {
			response = true;
		} else {
			ManagementAccount status = managementaccountDAO.getAccountName(username);

			response = status.isChecker();
		}

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		clientwithdrawuuid = StringUtils.trimToEmpty(request.getParameter("reject"));
		username = StringUtils.trimToEmpty(request.getParameter("username"));

	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		paramHash = new HashMap<>();

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		// response.sendRedirect("../index.jsp");
	}
}

/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */

package com.impalapay.collection.servlet.admin.refund;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.accountmgmt.ManageAccountDAO;
import com.impalapay.airtel.persistence.systemlog.SystemLogDAO;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.collection.beans.balance.CashWithdrawal;
import com.impalapay.collection.persistence.accountmgmt.inprogressbalance.InProgressBalanceDAO;
import com.impalapay.collection.persistence.cashwithdraw.CheckerWithdrawHistoryDAOImpl;
import com.impalapay.collection.persistence.cashwithdraw.ClientWithdrawDAOImpl;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add/update forex.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Nov 24, 2014
 *
 * @author <a href="mailto:mike@impalapay.com">Michael Wakahe</a>
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 *
 */
public class RejectmakerWithdrawCash extends HttpServlet {

	final String ERROR_NO_BALANCEUUID = "Please provide the client withdrawal Unique indetifier.";
	final String ERROR_NO_USERNAME = "No username provided";
	final String ERROR_UNABLE_DELETE = "unable to Reject client Withdrawal Request";
	final String ERROR_NOT_ALLOWED = "You dont have access rights to perform this Action,Please Contact System Administrator";
	final String SUCCESS = "You have successfully Rejected a withdrawal request as a maker";
	private String transactionwithdrawhistoryuuid, createdpair, username;
	private double baserate = 0, impalarate = 0;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;

	private SystemLogDAO systemlogDAO;
	private InProgressBalanceDAO inprogressbalancehold;
	private ManageAccountDAO managementaccountDAO;
	private ClientWithdrawDAOImpl cashwithdrawDAO;
	private CheckerWithdrawHistoryDAOImpl checkerwithdrawalhistoryDAO;
	private boolean response;
	private TransactionStatus statusoftransaction;
	private CashWithdrawal clientwithdrawalobject;
	private Cache accountsCache;
	private HttpSession session;
	private ManagementAccount account = null;
	private String transactioinid = "", rejectedtransactionuuid = "6f017761-5ed2-47b6-b585-f525bfbd3664";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		checkerwithdrawalhistoryDAO = CheckerWithdrawHistoryDAOImpl.getInstance();
		managementaccountDAO = ManageAccountDAO.getInstance();
		inprogressbalancehold = InProgressBalanceDAO.getInstance();

		CacheManager.getInstance();
		systemlogDAO = SystemLogDAO.getInstance();
		accountsCache = mgr.getCache(CacheVariables.CACHE_MANAGEMENTACCOUNTS_BY_USERNAME);

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		session = request.getSession(true);

		setClassParameters(request);

		initParamHash();

		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (ManagementAccount) element.getObjectValue();
		}
		session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, paramHash);

		// No First currency provided
		if (StringUtils.isBlank(transactionwithdrawhistoryuuid)) {
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, ERROR_NO_BALANCEUUID);

			// No Base rate provided
		} else if (StringUtils.isBlank(username)) {
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, ERROR_NO_USERNAME);

		} else if (!addwithdrawal()) {
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, ERROR_NOT_ALLOWED);

		} else {

			if (!deletecheckerForex()) {
				session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, ERROR_UNABLE_DELETE);
			}

			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_SUCCESS_KEY, SUCCESS);

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, null);

		}

		response.sendRedirect("inprogresswithdrawal.jsp");

	}

	/**
	 *
	 */
	private boolean deletecheckerForex() {
		response = false;
		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');
		statusoftransaction = new TransactionStatus();
		statusoftransaction.setUuid(rejectedtransactionuuid);

		clientwithdrawalobject = checkerwithdrawalhistoryDAO
				.getWithdrawalHistoryTransaction(transactionwithdrawhistoryuuid);

		if (clientwithdrawalobject != null) {

			SystemLog systemlog = new SystemLog();
			transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');
			systemlog.setUsername(username);
			systemlog.setUuid(transactioinid);
			systemlog.setAction(username + "Rejected a withdrawal that was already processed everything is now reversed"
					+ transactioinid);

			if (checkerwithdrawalhistoryDAO.updateTransactionStatus(transactionwithdrawhistoryuuid, statusoftransaction)
					&& inprogressbalancehold.removeBalanceHoldFail(transactionwithdrawhistoryuuid)) {
				systemlogDAO.putsystemlog(systemlog);
				response = true;

			}

		}

		return response;
	}

	public boolean addwithdrawal() {

		if (StringUtils.equals(username, PropertiesConfig.getConfigValue("ADMIN_USERNAME"))) {
			response = true;
		} else {
			ManagementAccount status = managementaccountDAO.getAccountName(username);

			response = status.isChecker();
		}

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		transactionwithdrawhistoryuuid = StringUtils.trimToEmpty(request.getParameter("reject"));
		username = StringUtils.trimToEmpty(request.getParameter("username"));

	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		paramHash = new HashMap<>();

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		// response.sendRedirect("../index.jsp");
	}
}

/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */

package com.impalapay.collection.servlet.admin.refund;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;
import com.impalapay.airtel.beans.forex.ForexEngine;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.systemlog.SystemLogDAO;
import com.impalapay.collection.beans.balance.CashWithdrawal;
import com.impalapay.collection.beans.refund.CollectionRefund;
import com.impalapay.collection.persistence.cashwithdraw.CheckerWithdrawDAOImpl;
import com.impalapay.collection.persistence.cashwithdraw.CheckerWithdrawHistoryDAOImpl;
import com.impalapay.collection.persistence.cashwithdraw.ClientWithdrawDAOImpl;
import com.impalapay.collection.persistence.refund.CollectionRefundDAOImpl;
import com.impalapay.collection.persistence.refund.CollectionTempRefundDAOImpl;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to create a new account.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Nov 24, 2014
 *
 * @author <a href="mailto:mike@impalapay.com">Michael Wakahe</a>
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 *
 */
public class CheckerCollectionrefundCash extends HttpServlet {

	final String ERROR_NO_USERNAME = "Please provide a Username.";
	final String ERROR_NO_BALANCEUUID = "Please provide the client withdrawal Unique indetifier.";
	final String ERROR_NO_COMMISSION = "Please provide the Total Commissions from this transaction";
	final String ERROR_NO_LESSAMOUNT = "Please advice the account holder to have enough balance,the commission for this transaction equals to or exceeds the total balance available";
	final String ERROR_NO_EXTRAINFO = "Please Provide the extra infiormation for Banking(e.g AccountName,AccountNumber,BankName,BankCode,SwiftCode...e.t.c).";
	final String ERROR_NO_WITHDRAWFAIL = "Unable to finalise Your withdrawal Request Please contact the ystem administrator or try after a few minutes";

	private String username, clientrefunduuid, terminatecurrency, comission, adminextrainfo,inprogresstransactionuuid = "5c9b8b0b-a035-4a07-bfd8-eccd4f039d53";

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;
	private HashMap<String, Double> systemforexmap = new HashMap<>();
	private HashMap<String, String> countrycurrencycodemap = new HashMap<>();

	private EmailValidator emailValidator;
	private SystemLogDAO systemlogDAO;
	
	private CollectionTempRefundDAOImpl collectiontempDAO;
	
	private CheckerWithdrawHistoryDAOImpl checkerwithdrawalhistoryDAO;
	
	private CollectionRefundDAOImpl collectionrefundDAO;
	
	private CollectionRefund refundrequest, refundrequestobject;
	
	private CashWithdrawal withdrawalrequest, checkerwithdrawalobject;
	
	
	private String transactioinid = "";
	private String alluuid = "", currencypair = "";
	private String redirectpage = "clientwithdraw.jsp";
	private CacheManager cacheManager;
	private Cache forexCache, countryCache, accountsCache;
	private HttpSession session;
	private boolean response;
	private ManagementAccount account;

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		emailValidator = EmailValidator.getInstance();

		collectiontempDAO = CollectionTempRefundDAOImpl.getInstance();

		collectionrefundDAO = CollectionRefundDAOImpl.getInstance();
		
		checkerwithdrawalhistoryDAO = CheckerWithdrawHistoryDAOImpl.getInstance();

		
		systemlogDAO = SystemLogDAO.getInstance();

		cacheManager = CacheManager.getInstance();
		countryCache = cacheManager.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);

		forexCache = cacheManager.getCache(CacheVariables.CACHE_FOREX_BY_UUID);
		accountsCache = cacheManager.getCache(CacheVariables.CACHE_MANAGEMENTACCOUNTS_BY_USERNAME);

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);

		setClassParameters(request);
		account = new ManagementAccount();

		initParamHash();
		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (ManagementAccount) element.getObjectValue();
		}

		try {
			refundrequestobject = collectionrefundDAO.getCollectionRefund(clientrefunduuid);

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error while trying to fetch client Refund request");
		}

		System.out.println("the username is " + username + "the accountobjectis " + account);
		session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_PARAMETERS, paramHash);

		// forex with curency pairs
		List keys;

		// fetch from cache
		Country country;
		keys = countryCache.getKeys();
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countrycurrencycodemap.put(country.getUuid(), country.getCurrencycode());
		}

		ForexEngine forexengine;
		keys = forexCache.getKeys();
		for (Object key : keys) {
			element = forexCache.get(key);
			forexengine = (ForexEngine) element.getObjectValue();
			systemforexmap.put(forexengine.getCurrencypair(), forexengine.getSpreadrate());

		}

		// No First Name provided
		if (StringUtils.isBlank(username)) {
			session.setAttribute(SessionConstants.ADMIN_CHECKERREFUNDREQUEST_ERROR_KEY, ERROR_NO_USERNAME);

			// No Unique Name provided
		} else if (StringUtils.isBlank(clientrefunduuid)) {
			session.setAttribute(SessionConstants.ADMIN_CHECKERREFUNDREQUEST_ERROR_KEY, ERROR_NO_BALANCEUUID);

			// No website login password provided
		} else if (StringUtils.isBlank(comission)) {
			session.setAttribute(SessionConstants.ADMIN_CHECKERREFUNDREQUEST_ERROR_KEY, ERROR_NO_COMMISSION);

		} else if (StringUtils.isBlank(adminextrainfo)) {
			session.setAttribute(SessionConstants.ADMIN_CHECKERREFUNDREQUEST_ERROR_KEY, ERROR_NO_EXTRAINFO);

		//} else if (clientwithdrawalobject.getAmount() <= Double.parseDouble(comission)) {
			//session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, ERROR_NO_LESSAMOUNT);

		} else if (!addAccount()) {

			session.setAttribute(SessionConstants.ADMIN_CHECKERREFUNDREQUEST_ERROR_KEY, ERROR_NO_WITHDRAWFAIL);

		} else {

			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_CHECKERREFUNDREQUEST_SUCCESS_KEY, "s");

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_CHECKERREFUNDREQUEST_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_CHECKERREFUNDREQUEST_ERROR_KEY, null);
		}

		response.sendRedirect("checkerapproverefund.jsp");
	}


	/**
	 *
	 */
	private boolean addAccount() {
		response = false;
		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		if (refundrequestobject == null) {

			System.out.println("we failed on this stage fetching client refund transaction");
			return false;

		}

		

		refundrequest = new CollectionRefund();

		refundrequest.setUuid(transactioinid);
		refundrequest.setAccountuuid(refundrequestobject.getAccountuuid());
		refundrequest.setRefundreason(refundrequestobject.getRefundreason());
		refundrequest.setCurrencyUuid(refundrequestobject.getCurrencyUuid());
		refundrequest.setAmount(refundrequestobject.getAmount());
		
		
		refundrequest.setComission(Double.parseDouble(comission));
		refundrequest.setComissioncurrencyUuid(refundrequestobject.getCurrencyUuid());
		refundrequest.setAdminextrainformation(adminextrainfo);
		refundrequest.setTransactionStatusUuid(inprogresstransactionuuid);
		refundrequest.setAuthorisedmaker(account.getUuid());
		refundrequest.setDeductamount(refundrequestobject.getAmount()); //consider adding with commission
		
		refundrequest.setReferencenumber(refundrequestobject.getReferencenumber());
		refundrequest.setReceivertransactionuuid(refundrequestobject.getReferencenumber());//doesnt really matter
		
		refundrequest.setDateadded(new Date());
		
		withdrawalrequest = new CashWithdrawal();
		
		withdrawalrequest.setUuid(transactioinid);
		withdrawalrequest.setAccountuuid(refundrequestobject.getAccountuuid());
		withdrawalrequest.setCurrencyUuid(refundrequestobject.getCurrencyUuid());
		withdrawalrequest.setTocurrencyUuid(refundrequestobject.getCurrencyUuid());
		withdrawalrequest.setExtrainformation("COLLECTION_REFUND");
		withdrawalrequest.setAmount(refundrequestobject.getAmount());
		withdrawalrequest.setComission(refundrequestobject.getComission());
		withdrawalrequest.setComissioncurrencyUuid(refundrequestobject.getCurrencyUuid());
		withdrawalrequest.setAdminextrainformation("COLLECTION_REFUND");
		withdrawalrequest.setSystemexchangerate(0);

		withdrawalrequest.setTransactionStatusUuid(inprogresstransactionuuid);
		withdrawalrequest.setBankwithdrawexchangerate(0);
		withdrawalrequest.setAuthorisedchecker(account.getUuid());
		withdrawalrequest.setAuthorisedmaker(account.getUuid());
		withdrawalrequest.setTransfercharges(0);
		withdrawalrequest.setTransferchargecurrencyUuid(refundrequestobject.getCurrencyUuid());
		withdrawalrequest.setReceivableamount(refundrequestobject.getAmount());

		withdrawalrequest.setTransactionDate(new Date());
		

		SystemLog systemlog = new SystemLog();
		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');
		systemlog.setUsername(username);
		systemlog.setUuid(transactioinid);
		systemlog.setAction(
				username + " as achecker successfully approved client Refund with transactionid " + transactioinid);

		if (checkerwithdrawalhistoryDAO.putCheckerWithdrawHistory(withdrawalrequest)&&collectiontempDAO.putCollectionTempRefund(refundrequest)
				&& collectionrefundDAO.deleteCollectionRefund(clientrefunduuid)) {
			systemlogDAO.putsystemlog(systemlog);
			response = true;

		}

		return response;
	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {

		username = StringUtils.trimToEmpty(request.getParameter("username"));
		clientrefunduuid = StringUtils.trimToEmpty(request.getParameter("clientwithdrawuuid"));
		comission = StringUtils.trimToEmpty(request.getParameter("comission"));
		adminextrainfo = StringUtils.trimToEmpty(request.getParameter("adminextrainfo"));
	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		paramHash = new HashMap<>();

		paramHash.put("username", username);
		paramHash.put("clientwithdrawuuid", clientrefunduuid);
		paramHash.put("comission", comission);
		paramHash.put("adminextrainfo", adminextrainfo);

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		// response.sendRedirect("../index.jsp");
	}
}

/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */
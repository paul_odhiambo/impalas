package com.impalapay.collection.servlet.admin.accounts;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.collection.beans.network.CollectionDefine;
import com.impalapay.collection.beans.network.CollectionNetworkSubaccount;
import com.impalapay.collection.persistence.network.CollectionDefineDAOImpl;
import com.impalapay.collection.persistence.networksubaccount.CollectionNetworkSubaccountDAOImpl;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add an msisdn to a specific country.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 11, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class AddAccountToCollection extends HttpServlet {

	final String ERROR_NO_NETWORK = "Please provide a value for the Network ";
	final String ERROR_NO_ACCOUNT = "Please provide a value for the account .";
	final String ERROR_NO_FOREX = "Please state if forex will be allowed or not.";
	final String ERROR_NO_SETTLEMENT = "Please state if settlement will be pre or post-settlement.";
	final String ERROR_NO_FIXEDCOMMISSION = "Please state if Fixed Commission is supported or not .";
	final String ERROR_NO_COMMISSION = "Please provide a value for the Commission.";
	final String ERROR_LISTENING_URI = "Please provide a Valid Listening URI.";
	final String ERROR_UNABLE_ADD = "Unable to add the account to collection network";
	// These represent form parameters
	private String networkuuid, accountuuid, supportforex, presettlement, fixedcommission, commission;
	private String addDay, addMonth, addYear, collectionnetworksubaccountuuid, listeninguri, referenceprefix;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> PrefixparamHash;

	private Cache collectiondefineCache;
	private CollectionDefineDAOImpl collectiondefineDAO;
	private CollectionNetworkSubaccountDAOImpl networksubaccntDAO;

	private Logger logger;

	private String alluuid = "";

	private String transactioinid = "";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		collectiondefineCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_DEFINE);

		collectiondefineDAO = CollectionDefineDAOImpl.getInstance();

		networksubaccntDAO = CollectionNetworkSubaccountDAOImpl.getInstance();

		logger = Logger.getLogger(this.getClass());
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_PARAMETERS, PrefixparamHash);

		if (StringUtils.isBlank(accountuuid)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_ERROR_KEY, ERROR_NO_ACCOUNT);

		} else if (StringUtils.isBlank(fixedcommission)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_ERROR_KEY, ERROR_NO_FIXEDCOMMISSION);

		} else if (StringUtils.isBlank(presettlement)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_ERROR_KEY, ERROR_NO_SETTLEMENT);

		} else if (StringUtils.isBlank(commission)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_ERROR_KEY, ERROR_NO_COMMISSION);

		} else if (StringUtils.isBlank(listeninguri)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_ERROR_KEY, ERROR_LISTENING_URI);

		} else if (!addRouteDefine()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_ERROR_KEY, ERROR_UNABLE_ADD);

		} else {
			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_SUCCESS_KEY, "s");

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_ERROR_KEY, null);

		}

		response.sendRedirect("collectiondefine.jsp");

		// purchasesCache.put(new
		// Element(CacheVariables.CACHE_PURCHASEPERCOUNTRY_KEY,
		// accountPurchaseDAO.getAllClientPurchasesByCountry()));
	}

	/**
	 * Add amount added to each country float.
	 * 
	 * @return boolean indicating if addition has been added or not.
	 */
	private boolean addRouteDefine() {

		Account account = new Account();

		account.setUuid(accountuuid);

		CollectionDefine routedefine = collectiondefineDAO.getCollectionRouteUuid(collectionnetworksubaccountuuid,
				referenceprefix);

		CollectionNetworkSubaccount collectionsubaccnt = networksubaccntDAO
				.getCollectionNetworkSubAcct(collectionnetworksubaccountuuid);

		networkuuid = collectionsubaccnt.getNetworkuuid();

		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		if (routedefine != null) {

			alluuid = routedefine.getUuid();
		} else {
			alluuid = transactioinid;
		}

		CollectionDefine routing = new CollectionDefine();

		routing.setUuid(alluuid);
		routing.setAddedbyuuid(accountuuid);
		routing.setAccountuuid(accountuuid);
		routing.setNetworkuuid(networkuuid);
		routing.setCollectionnetworksubaccountuuid(collectionnetworksubaccountuuid);
		routing.setSupportforex(Boolean.parseBoolean(supportforex));
		routing.setFixedcommission(Boolean.parseBoolean(fixedcommission));
		routing.setPresettlement(Boolean.parseBoolean(presettlement));
		routing.setCommission(Double.parseDouble(commission));
		routing.setListeninguri(listeninguri);
		routing.setReferenceprefix(referenceprefix);
		Calendar c = Calendar.getInstance();
		c.set(NumberUtils.toInt(addYear), NumberUtils.toInt(addMonth) - 1, NumberUtils.toInt(addDay));
		routing.setDateadded(c.getTime());

		boolean response = collectiondefineDAO.UpdateCollectionDefine(alluuid, routing);

		collectiondefineCache.put(new Element(routing.getUuid(), routing));

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		accountuuid = StringUtils.trimToEmpty(request.getParameter("accountuuid"));
		supportforex = StringUtils.trimToEmpty(request.getParameter("optionforex"));
		fixedcommission = StringUtils.trimToEmpty(request.getParameter("optionfixedcommision"));
		presettlement = StringUtils.trimToEmpty(request.getParameter("optionsettlement"));
		commission = StringUtils.trimToEmpty(request.getParameter("commission"));
		collectionnetworksubaccountuuid = StringUtils
				.trimToEmpty(request.getParameter("collectionnetworksubaccountuuid"));
		listeninguri = StringUtils.trimToEmpty(request.getParameter("listeninguri"));
		referenceprefix = StringUtils.trimToEmpty(request.getParameter("referenceprefix"));
		addDay = StringUtils.trimToEmpty(request.getParameter("addDay"));
		addMonth = StringUtils.trimToEmpty(request.getParameter("addMonth"));
		addYear = StringUtils.trimToEmpty(request.getParameter("addYear"));

	}

	/**
	 * Place some of the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		PrefixparamHash = new HashMap<>();
		PrefixparamHash.put("networkuuid", networkuuid);
		PrefixparamHash.put("accountuuid", accountuuid);
		PrefixparamHash.put("supportforex", supportforex);
		PrefixparamHash.put("fixedcommission", fixedcommission);
		PrefixparamHash.put("presettlement", presettlement);
		PrefixparamHash.put("commission", commission);

	}

}

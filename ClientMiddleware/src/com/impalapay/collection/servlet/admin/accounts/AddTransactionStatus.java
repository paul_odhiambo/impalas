package com.impalapay.collection.servlet.admin.accounts;

import java.io.IOException;

import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.persistence.accountmgmt.ManageAccountDAO;

import com.impalapay.airtel.persistence.systemlog.SystemLogDAO;
import com.impalapay.airtel.persistence.transaction.TransactionDAO;
import com.impalapay.airtel.servlet.util.PropertiesConfig;

import net.sf.ehcache.CacheManager;

import org.apache.commons.lang3.StringUtils;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add bulk airtime to the master account.
 * <p>
 * Copyright (c) Shujaa Solutions Ltd., Oct 11, 2013
 *
 * @author <a href="mailto:anthonym@shujaa.co.ke">Antony Wafula</a>
 * @version %I%, %G%
 */
public class AddTransactionStatus extends HttpServlet {

	final String ERROR_NO_TRANSACTIONUUID = "Please provide a value for the transaction uuid";
	final String ERROR_NO_TRANSACTIONSTATUS = "Please provide a valid transaction status uuid";
	final String ERROR_UNKNOWN_TRANSACTIONUUID = "Provided transaction uuid does not match any in the database";
	final String ERROR_UNABLE_TO_UPDATE = "Unable to update the transaction status";
	final String ERROR_NOT_ALLOWED = "You dont have access rights to perform this Action,Please Contact System Administrator";

	// These represent form parameters
	private String transactionUuid, transactionstatusUuid, username, transactioinid;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;

	private TransactionDAO transactionDAO;
	private ManageAccountDAO managementaccountDAO;
	private SystemLogDAO systemlogDAO;

	private boolean response;

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		managementaccountDAO = ManageAccountDAO.getInstance();
		systemlogDAO = SystemLogDAO.getInstance();
		transactionDAO = TransactionDAO.getInstance();
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_PARAMETERS, paramHash);

		if (StringUtils.isBlank(transactionUuid)) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, ERROR_NO_TRANSACTIONSTATUS);

		} else if (StringUtils.isBlank(transactionstatusUuid)) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, ERROR_NO_TRANSACTIONSTATUS);

		} else if (!checktransaction()) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY,
					ERROR_UNKNOWN_TRANSACTIONUUID);

		} else if (!performReversal()) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, ERROR_NOT_ALLOWED);

		} else if (!updatestatus()) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, ERROR_UNABLE_TO_UPDATE);

		} else {
			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_SUCCESS_KEY,
					"Transaction status succesfully adjusted");

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, null);

		}

		response.sendRedirect("updatestatus.jsp");

	}

	/**
	 * Add airtime purchased in bulk
	 *
	 */
	private boolean updatestatus() {
		TransactionStatus transactionstatus = new TransactionStatus();
		transactionstatus.setUuid(transactionstatusUuid);

		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		boolean response = transactionDAO.updateTransactionStatus(transactionUuid, transactionstatus);

		if (response == true) {
			SystemLog systemlogs = new SystemLog();
			systemlogs.setUuid(transactioinid);
			systemlogs.setAction(username + " updated new status of transaction " + transactionUuid + " to "
					+ transactionstatusUuid);
			systemlogDAO.putsystemlog(systemlogs);

		}

		return response;

	}

	public boolean performReversal() {
		response = false;

		if (StringUtils.equals(username, PropertiesConfig.getConfigValue("ADMIN_USERNAME"))) {
			response = true;
		} else {
			ManagementAccount status = managementaccountDAO.getAccountName(username);

			response = status.isUpdatereversal();
		}

		return response;

	}

	public boolean checktransaction() {
		Transaction veve = transactionDAO.getTransactionbyuuid(transactionUuid);

		if (veve != null) {
			response = true;
		} else {

			response = false;
		}

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		transactionUuid = StringUtils.trimToEmpty(request.getParameter("transactionUuid"));
		transactionstatusUuid = StringUtils.trimToEmpty(request.getParameter("transactionstatusUuid"));
		username = StringUtils.trimToEmpty(request.getParameter("username"));
	}

	/**
	 * Place some of the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		paramHash = new HashMap<>();

		paramHash.put("amount", transactionstatusUuid);
	}
}

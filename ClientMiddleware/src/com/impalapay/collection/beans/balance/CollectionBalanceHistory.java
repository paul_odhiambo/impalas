package com.impalapay.collection.beans.balance;

import java.util.Date;

public class CollectionBalanceHistory extends CollectionBalance {

	private double amount;
	private String transactionuuid;
	private Date topupTime;

	public CollectionBalanceHistory() {
		super();
		// TODO Auto-generated constructor st

		amount = 0;
		transactionuuid = "";
		topupTime = new Date();
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getTransactionuuid() {
		return transactionuuid;
	}

	public void setTransactionuuid(String transactionuuid) {
		this.transactionuuid = transactionuuid;
	}

	public Date getTopupTime() {
		return topupTime;
	}

	public void setTopupTime(Date topupTime) {
		this.topupTime = topupTime;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CollectionBalanceHistory [getUuid()=");
		builder.append(getUuid());
		builder.append(", getAccountuuid()=");
		builder.append(getAccountuuid());
		builder.append(", getCountryuuid()=");
		builder.append(getCountryuuid());
		builder.append(", getBalance()=");
		builder.append(getBalance());
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", transactionuuid=");
		builder.append(transactionuuid);
		builder.append(", topupTime=");
		builder.append(topupTime);
		builder.append("]");
		return builder.toString();
	}

}

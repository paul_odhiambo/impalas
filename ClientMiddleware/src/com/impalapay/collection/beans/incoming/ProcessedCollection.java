package com.impalapay.collection.beans.incoming;

public class ProcessedCollection extends TempCollection {

	private double commission;

	public ProcessedCollection() {
		super();

		commission = 0;

	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProcessedCollection [commission=");
		builder.append(commission);
		builder.append(", getUuid()=");
		builder.append(getUuid());
		builder.append(", getNetworkuuid()=");
		builder.append(getNetworkuuid());
		builder.append(", getCreditaccountuuid()=");
		builder.append(getCreditaccountuuid());
		builder.append(", getTransactiontype()=");
		builder.append(getTransactiontype());
		builder.append(", getDebitorname()=");
		builder.append(getDebitorname());
		builder.append(", getCollectiondefineuuid()=");
		builder.append(getCollectiondefineuuid());
		builder.append(", getDebitcurrency()=");
		builder.append(getDebitcurrency());
		builder.append(", getDebitcountry()=");
		builder.append(getDebitcountry());
		builder.append(", getAmount()=");
		builder.append(getAmount());
		builder.append(", getAccountreference()=");
		builder.append(getAccountreference());
		builder.append(", getTransactionstatusuuid()=");
		builder.append(getTransactionstatusuuid());
		builder.append(", getDebitedaccount()=");
		builder.append(getDebitedaccount());
		builder.append(", getOriginatetransactionuuid()=");
		builder.append(getOriginatetransactionuuid());
		builder.append(", getReceivertransactionuuid()=");
		builder.append(getReceivertransactionuuid());
		builder.append(", getVendorunique()=");
		builder.append(getVendorunique());
		builder.append(", getOriginateamount()=");
		builder.append(getOriginateamount());
		builder.append(", getOriginatecurrency()=");
		builder.append(getOriginatecurrency());
		builder.append(", getProcessingstatus()=");
		builder.append(getProcessingstatus());
		builder.append(", getEndpointstatusdescription()=");
		builder.append(getEndpointstatusdescription());
		builder.append(", getServertime()=");
		builder.append(getServertime());
		builder.append("]");
		return builder.toString();
	}

}

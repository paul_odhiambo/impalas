package com.impalapay.collection.beans.incoming;

public class CollectionUpdateStatus extends TempCollection {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6810250093199874462L;
	public String currentstatus;
	public String updatestatus;


	public CollectionUpdateStatus() {
		currentstatus = "";
		updatestatus = "";
	
	}

	public String getCurrentstatus() {
		return currentstatus;
	}

	public void setCurrentstatus(String currentstatus) {
		this.currentstatus = currentstatus;
	}

	public String getUpdatestatus() {
		return updatestatus;
	}

	public void setUpdatestatus(String updatestatus) {
		this.updatestatus = updatestatus;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CollectionUpdateStatus [currentstatus=");
		builder.append(currentstatus);
		builder.append(", updatestatus=");
		builder.append(updatestatus);
		builder.append(", getCreditaccountuuid()=");
		builder.append(getCreditaccountuuid());
		builder.append(", getDebitcurrency()=");
		builder.append(getDebitcurrency());
		builder.append(", getAmount()=");
		builder.append(getAmount());
		builder.append(", getOriginatetransactionuuid()=");
		builder.append(getOriginatetransactionuuid());
		builder.append(", getOriginateamount()=");
		builder.append(getOriginateamount());
		builder.append(", getOriginatecurrency()=");
		builder.append(getOriginatecurrency());
		builder.append(", getUuid()=");
		builder.append(getUuid());
		builder.append("]");
		return builder.toString();
	}


}

package com.impalapay.collection.beans.network;

import java.util.Date;

import com.impalapay.airtel.beans.StorableBean;

public class CollectionDefine extends StorableBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7417775696903496598L;

	private String accountuuid;

	private String collectionnetworksubaccountuuid;

	private String networkuuid;

	private boolean supportforex;

	private boolean fixedcommission;

	private boolean presettlement;

	private String addedbyuuid;

	private String referenceprefix;

	private double commission;

	private String listeninguri;

	private Date dateadded;

	public CollectionDefine() {
		super();

		accountuuid = "";

		networkuuid = "";

		collectionnetworksubaccountuuid = "";

		addedbyuuid = "";

		referenceprefix = "";

		fixedcommission = true;

		presettlement = true;

		supportforex = true;

		listeninguri = "";

		dateadded = new Date();
	}

	public String getAccountuuid() {
		return accountuuid;
	}

	public void setAccountuuid(String accountuuid) {
		this.accountuuid = accountuuid;
	}

	public String getCollectionnetworksubaccountuuid() {
		return collectionnetworksubaccountuuid;
	}

	public void setCollectionnetworksubaccountuuid(String collectionnetworksubaccountuuid) {
		this.collectionnetworksubaccountuuid = collectionnetworksubaccountuuid;
	}

	public String getNetworkuuid() {
		return networkuuid;
	}

	public void setNetworkuuid(String networkuuid) {
		this.networkuuid = networkuuid;
	}

	public boolean isSupportforex() {
		return supportforex;
	}

	public void setSupportforex(boolean supportforex) {
		this.supportforex = supportforex;
	}

	public boolean isFixedcommission() {
		return fixedcommission;
	}

	public void setFixedcommission(boolean fixedcommission) {
		this.fixedcommission = fixedcommission;
	}

	public boolean isPresettlement() {
		return presettlement;
	}

	public void setPresettlement(boolean presettlement) {
		this.presettlement = presettlement;
	}

	public String getAddedbyuuid() {
		return addedbyuuid;
	}

	public void setAddedbyuuid(String addedbyuuid) {
		this.addedbyuuid = addedbyuuid;
	}

	public String getReferenceprefix() {
		return referenceprefix;
	}

	public void setReferenceprefix(String referenceprefix) {
		this.referenceprefix = referenceprefix;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public String getListeninguri() {
		return listeninguri;
	}

	public void setListeninguri(String listeninguri) {
		this.listeninguri = listeninguri;
	}

	public Date getDateadded() {
		return dateadded;
	}

	public void setDateadded(Date dateadded) {
		this.dateadded = dateadded;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CollectionDefine [getUuid()=");
		builder.append(getUuid());
		builder.append(", accountuuid=");
		builder.append(accountuuid);
		builder.append(", collectionnetworksubaccountuuid=");
		builder.append(collectionnetworksubaccountuuid);
		builder.append(", networkuuid=");
		builder.append(networkuuid);
		builder.append(", supportforex=");
		builder.append(supportforex);
		builder.append(", fixedcommission=");
		builder.append(fixedcommission);
		builder.append(", presettlement=");
		builder.append(presettlement);
		builder.append(", addedbyuuid=");
		builder.append(addedbyuuid);
		builder.append(", referenceprefix=");
		builder.append(referenceprefix);
		builder.append(", commission=");
		builder.append(commission);
		builder.append(", listeninguri=");
		builder.append(listeninguri);
		builder.append(", dateadded=");
		builder.append(dateadded);
		builder.append("]");
		return builder.toString();
	}

}

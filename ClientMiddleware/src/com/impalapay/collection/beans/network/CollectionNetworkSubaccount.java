package com.impalapay.collection.beans.network;

import java.util.Date;

import com.impalapay.airtel.beans.StorableBean;

public class CollectionNetworkSubaccount extends StorableBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 31151657450053136L;
	private String networkuuid;
	private String collectionnumber;
	private boolean sharedconfiguration;
	private boolean presettlement;
	private String currency;
	private int referencesplitlength;
	private Date dateadded;

	public CollectionNetworkSubaccount() {
		super();
		// TODO Auto-generated constructor stub
		networkuuid = "";
		collectionnumber = "";
		sharedconfiguration = false;
		currency = "";
		referencesplitlength = 0;
		dateadded = new Date();
	}

	public String getNetworkuuid() {
		return networkuuid;
	}

	public String getCollectionnumber() {
		return collectionnumber;
	}

	public boolean isSharedconfiguration() {
		return sharedconfiguration;
	}

	public String getCurrency() {
		return currency;
	}

	public int getReferencesplitlength() {
		return referencesplitlength;
	}

	public Date getDateadded() {
		return dateadded;
	}

	public void setNetworkuuid(String networkuuid) {
		this.networkuuid = networkuuid;
	}

	public void setCollectionnumber(String collectionnumber) {
		this.collectionnumber = collectionnumber;
	}

	public void setSharedconfiguration(boolean sharedconfiguration) {
		this.sharedconfiguration = sharedconfiguration;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setReferencesplitlength(int referencesplitlength) {
		this.referencesplitlength = referencesplitlength;
	}

	public void setDateadded(Date dateadded) {
		this.dateadded = dateadded;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CollectionNetworkSubaccount [getUuid()=");
		builder.append(getUuid());
		builder.append(", networkuuid=");
		builder.append(networkuuid);
		builder.append(", collectionnumber=");
		builder.append(collectionnumber);
		builder.append(", sharedconfiguration=");
		builder.append(sharedconfiguration);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", referencesplitlength=");
		builder.append(referencesplitlength);
		builder.append(", dateadded=");
		builder.append(dateadded);
		builder.append("]");
		return builder.toString();
	}

}

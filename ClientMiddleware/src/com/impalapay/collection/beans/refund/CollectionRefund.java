package com.impalapay.collection.beans.refund;

import java.util.Date;

import com.impalapay.airtel.beans.StorableBean;

/**
 * A generic balance of account
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class CollectionRefund extends StorableBean {

	private String accountuuid;
	private String referencenumber;
	private String refundreason;
	private String currencyUuid;
	private String receivertransactionuuid;
	private double amount;
	private double comission;
	private String comissioncurrencyUuid;
	private String adminextrainformation;
	private String transactionStatusUuid;
	private String authorisedmaker;
	private double deductamount; //amount+commission
	private Date dateadded;


	/**
	 * 
	 */
	public CollectionRefund() {
		super();
		refundreason = "";
		referencenumber = "";
		accountuuid = "";
		currencyUuid ="";
		receivertransactionuuid="";
		amount=0;
		comission=0;
		comissioncurrencyUuid="";
		adminextrainformation="";
		transactionStatusUuid="";
		authorisedmaker="";
		deductamount=0;
		dateadded = new Date();
	}


	public String getAccountuuid() {
		return accountuuid;
	}


	public void setAccountuuid(String accountuuid) {
		this.accountuuid = accountuuid;
	}


	public String getReferencenumber() {
		return referencenumber;
	}


	public void setReferencenumber(String referencenumber) {
		this.referencenumber = referencenumber;
	}


	public String getRefundreason() {
		return refundreason;
	}


	public void setRefundreason(String refundreason) {
		this.refundreason = refundreason;
	}


	public String getCurrencyUuid() {
		return currencyUuid;
	}


	public void setCurrencyUuid(String currencyUuid) {
		this.currencyUuid = currencyUuid;
	}


	public String getReceivertransactionuuid() {
		return receivertransactionuuid;
	}


	public void setReceivertransactionuuid(String receivertransactionuuid) {
		this.receivertransactionuuid = receivertransactionuuid;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public double getComission() {
		return comission;
	}


	public void setComission(double comission) {
		this.comission = comission;
	}


	public String getComissioncurrencyUuid() {
		return comissioncurrencyUuid;
	}


	public void setComissioncurrencyUuid(String comissioncurrencyUuid) {
		this.comissioncurrencyUuid = comissioncurrencyUuid;
	}


	public String getAdminextrainformation() {
		return adminextrainformation;
	}


	public void setAdminextrainformation(String adminextrainformation) {
		this.adminextrainformation = adminextrainformation;
	}


	public String getTransactionStatusUuid() {
		return transactionStatusUuid;
	}


	public void setTransactionStatusUuid(String transactionStatusUuid) {
		this.transactionStatusUuid = transactionStatusUuid;
	}


	public String getAuthorisedmaker() {
		return authorisedmaker;
	}


	public void setAuthorisedmaker(String authorisedmaker) {
		this.authorisedmaker = authorisedmaker;
	}


	public double getDeductamount() {
		return deductamount;
	}


	public void setDeductamount(double deductamount) {
		this.deductamount = deductamount;
	}


	public Date getDateadded() {
		return dateadded;
	}


	public void setDateadded(Date dateadded) {
		this.dateadded = dateadded;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CollectionRefund [accountuuid=");
		builder.append(accountuuid);
		builder.append(", referencenumber=");
		builder.append(referencenumber);
		builder.append(", refundreason=");
		builder.append(refundreason);
		builder.append(", currencyUuid=");
		builder.append(currencyUuid);
		builder.append(", receivertransactionuuid=");
		builder.append(receivertransactionuuid);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", comission=");
		builder.append(comission);
		builder.append(", comissioncurrencyUuid=");
		builder.append(comissioncurrencyUuid);
		builder.append(", adminextrainformation=");
		builder.append(adminextrainformation);
		builder.append(", transactionStatusUuid=");
		builder.append(transactionStatusUuid);
		builder.append(", authorisedmaker=");
		builder.append(authorisedmaker);
		builder.append(", deductamount=");
		builder.append(deductamount);
		builder.append(", dateadded=");
		builder.append(dateadded);
		builder.append(", getUuid()=");
		builder.append(getUuid());
		builder.append("]");
		return builder.toString();
	}
		
	
}

package com.impalapay.collection.beans.refund;


public class CollectionRefundHistory extends CollectionRefund {
	
	private String transactionrefunduuid;

	

	public CollectionRefundHistory() {
		super();
		// TODO Auto-generated constructor st

		transactionrefunduuid = "";
	}

	public String getTransactionrefunduuid() {
		return transactionrefunduuid;
	}

	public void setTransactionrefunduuid(String transactionrefunduuid) {
		this.transactionrefunduuid = transactionrefunduuid;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CollectionRefundHistory [transactionrefunduuid=");
		builder.append(transactionrefunduuid);
		builder.append(", getAccountuuid()=");
		builder.append(getAccountuuid());
		builder.append(", getReferencenumber()=");
		builder.append(getReferencenumber());
		builder.append(", getRefundreason()=");
		builder.append(getRefundreason());
		builder.append(", getCurrencyUuid()=");
		builder.append(getCurrencyUuid());
		builder.append(", getReceivertransactionuuid()=");
		builder.append(getReceivertransactionuuid());
		builder.append(", getAmount()=");
		builder.append(getAmount());
		builder.append(", getComission()=");
		builder.append(getComission());
		builder.append(", getComissioncurrencyUuid()=");
		builder.append(getComissioncurrencyUuid());
		builder.append(", getAdminextrainformation()=");
		builder.append(getAdminextrainformation());
		builder.append(", getTransactionStatusUuid()=");
		builder.append(getTransactionStatusUuid());
		builder.append(", getAuthorisedmaker()=");
		builder.append(getAuthorisedmaker());
		builder.append(", getDeductamount()=");
		builder.append(getDeductamount());
		builder.append(", getDateadded()=");
		builder.append(getDateadded());
		builder.append(", getUuid()=");
		builder.append(getUuid());
		builder.append("]");
		return builder.toString();
	}
	
	
}

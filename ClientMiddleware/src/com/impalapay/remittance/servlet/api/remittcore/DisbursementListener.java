package com.impalapay.remittance.servlet.api.remittcore;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.accountmgmt.inprogressbalance.InProgressBalanceDAO;
import com.impalapay.airtel.persistence.transaction.TransactionDAO;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.mno.servlet.api.remit.AutoTransactionDispatcher;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
import com.impalapay.beans.network.Network;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class DisbursementListener extends HttpServlet {

	private PostWithIgnoreSSL postMinusThread;

	private Logger logger;

	private Cache networkCache, transactionStatusCache;

	private TransactionDAO transactionDAO;

	private InProgressBalanceDAO inprogressbalanaceDAO;

	private TransactionForexDAO transactionforexDAO;

	private HashMap<String, String> transactionStatusHash = new HashMap<>();

	private HashMap<String, String> transactionStatusuuidHash = new HashMap<>();

	private HashMap<String, String> networkQueryUrlmap = new HashMap<>();

	private HashMap<String, String> networkBridgeQueryUrlmap = new HashMap<>();

	private HashMap<String, String> networksupportquerymap = new HashMap<>();

	private HashMap<String, String> networkUsernamemap = new HashMap<>();

	private HashMap<String, String> networkPasswordmap = new HashMap<>();

	private String CLIENT_URL = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());
		transactionDAO = TransactionDAO.getInstance();
		transactionforexDAO = TransactionForexDAO.getinstance();
		inprogressbalanaceDAO = InProgressBalanceDAO.getInstance();

		networkCache = mgr.getCache(CacheVariables.CACHE_NETWORK_BY_UUID);
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		String responseobject = "";

		// joined json string
		String join = "";
		JsonElement root = null;

		Transaction transactions = null;

		TransactionStatus statusoftransaction = null;

		TransactionForexrate transactionforex = null;

		// These represent parameters received over the network
		String transactionid = "", networkid = "", statusdescription = "", receiverreferenceid = "",
				receiverstatuscode = "", receiverstatusdesciption = "";

		String statusuuid = "", statusuuiddb = "", creditinprogresscode = "S001", jsonResult = "", fail = "00029",
				success = "S000";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		logger.error(".....................................................");
		logger.error(" DISBURSEMENT LISTENERCORE BRIDGE INCOMING REQUEST :" + join);
		logger.error(".....................................................");

		// ####################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			receiverreferenceid = root.getAsJsonObject().get("am_referenceid").getAsString();

			receiverstatuscode = root.getAsJsonObject().get("status_code").getAsString();

			receiverstatusdesciption = root.getAsJsonObject().get("status_description").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(receiverreferenceid) || StringUtils.isBlank(receiverstatuscode)) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		Element element;

		List keys;

		// **************Network Cache****************//

		Network network;
		// network and remiturl
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkQueryUrlmap.put(network.getUuid(), network.getQueryip());
		}
		// network and username
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkUsernamemap.put(network.getUuid(), network.getUsername());
		}

		// network and password
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkPasswordmap.put(network.getUuid(), network.getPassword());
		}

		// network and bridgeremiturl
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkBridgeQueryUrlmap.put(network.getUuid(), network.getBridgequeryip());
		}

		// network and support querying
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networksupportquerymap.put(network.getUuid(), String.valueOf(network.isSupportquerycheck()));
		}

		// ======================================================
		// Populate with the mapping of Transaction Statuses.
		// The key is a UUID of the status
		// ======================================================

		TransactionStatus status1;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status1.getStatus(), status1.getUuid());
		}

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusuuidHash.put(status1.getUuid(), status1.getStatus());
		}

		// WES TART HERE CHECK IF TRANSACTION STATUS IS STILL INPROGRESS
		statusoftransaction = new TransactionStatus();
		statusuuid = transactionStatusHash.get(creditinprogresscode);
		statusoftransaction.setUuid(statusuuid);

		transactions = transactionDAO.getTransactionstatusbyreceiverreference(receiverreferenceid, statusoftransaction);
		// String transactionref = transactions.getReferenceNumber();
		if ((transactions == null)) {
			expected.put("command_status", "fail");
			expected.put("status_code", fail);
			expected.put("status_description", "We have a problem at Disbursement listener under fetching transactions");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		transactionid = transactions.getUuid();

		transactionforex = transactionforexDAO.getTransactionForexsUuid(transactionid);
		if ((transactionforex == null)) {
			expected.put("command_status",
					"fail");
			expected.put("status_code", fail);
			expected.put("status_description", "We have a problem at Disbursement Listener under fetching transactionforex rate transaction");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		// System.out.println(switchresponse);

		if (!transactionStatusHash.containsKey(receiverstatuscode)) {
			receiverstatuscode = "00032";
			statusdescription = "UNKNOWN_ERROR";
		} // Transaction forex

		// set the status UUID
		statusuuiddb = transactionStatusHash.get(receiverstatuscode);

		statusoftransaction = new TransactionStatus();

		if (receiverstatuscode.equalsIgnoreCase(success) || receiverstatuscode.equalsIgnoreCase(fail)) {

			if (receiverstatuscode.equals(success)) {

				new AutoTransactionDispatcher(transactions, transactionforex).start();

			} else {
				// delete the transaction forex from db
				// transactionforexDAO.deleteTransactionForex(transactionid);
				// return balances owed
				// inprogressbalanaceDAO.removeBalanceHoldFail(transactionid);
				// update that the system approved this balance
				if (transactionforexDAO.deleteTransactionForex(transactionid)
						&& inprogressbalanaceDAO.removeBalanceHoldFail(transactionid)) {

				} else {
					System.out.println("This transaction failed to update succesfully " + transactionid);
				}

			}

			statusoftransaction.setUuid(statusuuiddb);
			transactionDAO.updateTransactionStatus(transactionid, statusoftransaction);

			expected.put("transaction_id", transactionid);
			expected.put("status_code", receiverstatuscode);
			expected.put("command_status", APIConstants.COMMANDSTATUS_OK);
			expected.put("status_description", statusdescription);

			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL);
		expected.put("remit_status", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}// end of inprogress status

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

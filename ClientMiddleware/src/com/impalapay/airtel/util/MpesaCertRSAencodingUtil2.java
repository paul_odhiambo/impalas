package com.impalapay.airtel.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class MpesaCertRSAencodingUtil2 {

	public String encodePassword(String filepath, String password) {
		String statuscode = "", jsonResult = "", success = "S000", failed = "00029", encodedkey = "",
				safaricompassword = "", encodedinitiatorMessage = "", statusdescription = "";
		JsonObject responserequest = null;
		Gson g = new GsonBuilder().disableHtmlEscaping().create();

		try {


	            FileInputStream fin = new FileInputStream(filepath);
	            CertificateFactory cf = CertificateFactory.getInstance("X.509");
	            X509Certificate certificate = (X509Certificate) cf.generateCertificate(fin);
	            //Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
	            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	            //Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding","BC");

	            PublicKey pk = certificate.getPublicKey();
	            cipher.init(Cipher.ENCRYPT_MODE, pk);
	            //cipher.init(Cipher.ENCRYPT_MODE, pk, secureRandom);

	            
	            byte[] safaricompasswordMessageBytes = password.getBytes(StandardCharsets.UTF_8);

	            byte[] encryptedsafaricompasswordMessageBytes = cipher.doFinal(safaricompasswordMessageBytes);

	            // Convert the resulting encrypted byte array into a string using base64 encoding
	            //encodedkey = Base64.encode(encryptedsafaricompasswordMessageBytes).trim();
	            //return encodedkey;
	            encodedinitiatorMessage = Base64.getEncoder().encodeToString(encryptedsafaricompasswordMessageBytes);
	            
	            statuscode = success;
				statusdescription = "encryptionsuccess";
				encodedkey = encodedinitiatorMessage;
				
				

		} catch (FileNotFoundException e) {
			statuscode = failed;
			statusdescription = "FileNotFoundException";

		} catch (CertificateException e) {
			statuscode = failed;
			statusdescription = "CertificateException";
		} catch (NoSuchAlgorithmException e) {
			statuscode = failed;
			statusdescription = "NoSuchAlgorithmException";
		} catch (NoSuchPaddingException e) {
			statuscode = failed;
			statusdescription = "NoSuchPaddingException";
		} catch (InvalidKeyException e) {
			statuscode = failed;
			statusdescription = "InvalidKeyException";
		} catch (IllegalBlockSizeException e) {
			statuscode = failed;
			statusdescription = "IllegalBlockSizeException";
		} catch (BadPaddingException e) {
			statuscode = failed;
			statusdescription = "BadPaddingException";
		//}catch (NoSuchProviderException e) {
			//statuscode = failed;
			//statusdescription = "NoSuchProviderException";
		}catch (Exception e) {
			statuscode = failed;
			statusdescription = "NoSuchProviderException";
		}

		responserequest = new JsonObject();
		responserequest.addProperty("status_code", statuscode);
		responserequest.addProperty("status_description", statusdescription);
		responserequest.addProperty("encodedkey", encodedkey);
		jsonResult = g.toJson(responserequest);

		return jsonResult;
	}

}

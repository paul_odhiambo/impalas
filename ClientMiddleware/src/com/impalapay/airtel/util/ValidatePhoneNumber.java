package com.impalapay.airtel.util;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class ValidatePhoneNumber {
	private String jsonResult = "", validnumber = "", extractcode = "", secondpart = "", status = "", success = "S000",
			fail = "00032";
	private Gson g = new GsonBuilder().setPrettyPrinting().serializeNulls()
			.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
	private JsonObject responseobject;

	private int length = 0;

	public String ValidwithcodeNumber(String countrycode, String number) {

		length = String.valueOf(number).length();

		// If the leghth is equal to 9 add countrycode
		if (length == 9) {
			validnumber = countrycode + number;
			status = success;
			responseobject = new JsonObject();

			responseobject.addProperty("number", validnumber);
			responseobject.addProperty("status_code", status);

			jsonResult = g.toJson(responseobject);

		} else if (length == 12) {
			// check if the countrycode matches
			extractcode = number.substring(0, 3);

			// System.out.println(extractcode);

			if (extractcode.equalsIgnoreCase(countrycode)) {

				validnumber = number;
				status = success;
				responseobject = new JsonObject();

				responseobject.addProperty("number", validnumber);
				responseobject.addProperty("status_code", status);

				jsonResult = g.toJson(responseobject);
			} else {
				validnumber = number;
				status = fail;
				responseobject = new JsonObject();

				responseobject.addProperty("number", validnumber);
				responseobject.addProperty("status_code", status);

				jsonResult = g.toJson(responseobject);

			}
		} else {
			validnumber = number;
			status = fail;
			responseobject = new JsonObject();

			responseobject.addProperty("number", validnumber);
			responseobject.addProperty("status_code", status);

			jsonResult = g.toJson(responseobject);
		}

		return jsonResult;
	}

	public String ValidminuscodeNumber(String countrycode, String number) {

		length = String.valueOf(number).length();

		// If the leghth is equal to 9 add countrycode
		if (length == 9) {
			validnumber = number;
			status = success;
			responseobject = new JsonObject();

			responseobject.addProperty("number", validnumber);
			responseobject.addProperty("status_code", status);

			jsonResult = g.toJson(responseobject);
		} else if (length == 12) {
			// check if the countrycode matches
			extractcode = number.substring(0, 3);
			secondpart = number.substring(3, number.length()); // which is essentially the same as

			// System.out.println(extractcode);

			if (extractcode.equalsIgnoreCase(countrycode)) {

				validnumber = secondpart;
				status = success;
				responseobject = new JsonObject();

				responseobject.addProperty("number", validnumber);
				responseobject.addProperty("status_code", status);

				jsonResult = g.toJson(responseobject);
			} else {
				validnumber = number;
				status = fail;
				responseobject = new JsonObject();

				responseobject.addProperty("number", validnumber);
				responseobject.addProperty("status_code", status);

				jsonResult = g.toJson(responseobject);

			}
		} else {
			validnumber = number;
			status = fail;
			responseobject = new JsonObject();

			responseobject.addProperty("number", validnumber);
			responseobject.addProperty("status_code", status);

			jsonResult = g.toJson(responseobject);
		}

		return jsonResult;
	}

}

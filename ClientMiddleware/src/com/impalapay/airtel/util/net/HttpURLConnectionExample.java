package com.impalapay.airtel.util.net;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class HttpURLConnectionExample {

	private final String USER_AGENT = "Mozilla/5.0";

	public static void main(String[] args) throws Exception {

		HttpURLConnectionExample http = new HttpURLConnectionExample();

		System.out.println("Testing 1 - Send Http GET request");
		http.sendGet();

		// System.out.println("\nTesting 2 - Send Http POST request");
		// http.sendPost();

	}

	// HTTP GET request
	private void sendGet() throws Exception {

		// String url = "https://app.beyonic.com/api/payments/12345?format=json";
		String url = "https://api.trade.gov/consolidated_screening_list/search?api_key=KEQq8QqPfkmdToHi8MEw5T7x&name=mwau";
		String token = "Token " + "c97ffbc5b80e32ecdeb61f556461768bdd048ebb";

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		// add request header
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Content-Type", "application/json;charset=utf-8");
		// con.setRequestProperty("Authorization", token);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		System.out.println(response.toString());

	}

}
package com.impalapay.airtel.util.net;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.log4j.Logger;

/**
 * can send POST text data, to HTTPS
 * <p>
 * Copyright (c) ImpalapayLtd.,Sep 13, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class PostWithIgnoreSSLPokea {

	private String httpsUrl;
	private String params;
	private String token;
	boolean retry;
	private Logger logger;

	public PostWithIgnoreSSLPokea() {
		logger = Logger.getLogger(this.getClass());
	}

	public PostWithIgnoreSSLPokea(String httpsUrl, String params, String token) {
		super();
		this.httpsUrl = httpsUrl;
		this.params = params;
		this.token = token;

	}

	public String doPost() {
		URL url;
		String response = "";

		try {
			// Create a context that doesn't check certificates.
			SSLContext sslContext = SSLContext.getInstance("TLS");
			TrustManager[] trustMgr = getTrustManager();

			sslContext.init(null, // key manager
					trustMgr, // trust manager
					new SecureRandom()); // random number generator
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

			url = new URL(httpsUrl);
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Authorization", token);
			con.setRequestMethod("POST");
			con.setDoOutput(true);

			// return request.getHeader("user-agent");
			/*
			 * Map<String, List<String>> hdrs = con.getHeaderFields(); Set<String> hdrKeys =
			 * hdrs.keySet();
			 * 
			 * for (String k : hdrKeys) System.out.println("Key: " + k + "  Value: " +
			 * hdrs.get(k));
			 */
			//

			// ######################################################################
			// Send data to the output
			// ######################################################################
			sendData(con, params);

			// ######################################################################
			// Dump all the content
			// #######################################################################
			response = getContent(con);

		} catch (MalformedURLException e) {
			System.err.println("MalformedURLException");
			e.printStackTrace();

		} catch (IOException e) {
			System.err.println("IOException");
			e.printStackTrace();

		} catch (NoSuchAlgorithmException e) {
			System.err.println("NoSuchAlgorithmException");
			e.printStackTrace();

		} catch (KeyManagementException e) {
			System.err.println("KeyManagementException");
			e.printStackTrace();
		}

		return response;
	}

	public String doGet() {
		URL url;
		String response = "";

		try {
			// Create a context that doesn't check certificates.
			SSLContext sslContext = SSLContext.getInstance("TLS");
			TrustManager[] trustMgr = getTrustManager();

			sslContext.init(null, // key manager
					trustMgr, // trust manager
					new SecureRandom()); // random number generator
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

			url = new URL(httpsUrl);
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Authorization", "Bearer " + token);
			con.setRequestMethod("GET");
			con.setDoOutput(true);
			// ######################################################################
			// Send data to the output
			// ######################################################################
			// sendData(con, params);

			// ######################################################################
			// Dump all the content
			// #######################################################################
			response = getContent(con);

			System.out.println(response);

		} catch (MalformedURLException e) {
			System.err.println("MalformedURLException");
			e.printStackTrace();

		} catch (IOException e) {
			System.err.println("IOException");
			e.printStackTrace();

		} catch (NoSuchAlgorithmException e) {
			System.err.println("NoSuchAlgorithmException");
			e.printStackTrace();

		} catch (KeyManagementException e) {
			System.err.println("KeyManagementException");
			e.printStackTrace();
		}

		return response;
	}

	// ##################################################################
	/**
	 * Send data to the url
	 * 
	 * @param con
	 */
	// #################################################################
	private void sendData(HttpsURLConnection con, String args) {
		if (con != null) {

			try {
				// send data to output
				OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());

				writer.write(args);
				writer.flush();
				writer.close();

			} catch (IOException e) {
				System.err.println("IOException");
				e.printStackTrace();
			}
		}
	}

	// ###############################################################
	/**
	 * @param con
	 * @throws IOException
	 */
	// ###############################################################
	private String getContent(HttpsURLConnection con) throws IOException {
		StringBuffer buff = new StringBuffer("");

		if (con != null) {

			try {

				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

				String input;

				while ((input = br.readLine()) != null) {
					buff.append(input + "\n");
				}
				br.close();

			} catch (IOException e) {
				// e.printStackTrace();
				// return buff.toString().trim();
				int code = con.getResponseCode();

				if (code == 400 || code == 401 || code == 403 || code == 404 || code == 422 || code == 500
						|| code == 503) {
					InputStream is = null;
					is = con.getErrorStream();

					// Create an InputStream in order to extract the response
					// object

					String result = convertStreamToString(is);

					buff.append(result + "\n");

				} else {
					e.printStackTrace();
				}

			}
		} // end 'if(con != null)'

		return buff.toString().trim();
	}

	// ################################################################
	/**
	 * @return {@link TrustManager}
	 */
	// ################################################################
	private TrustManager[] getTrustManager() {

		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };

		return certs;
	}

	private static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

}
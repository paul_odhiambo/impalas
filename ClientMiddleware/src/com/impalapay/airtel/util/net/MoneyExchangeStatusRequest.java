/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impalapay.airtel.util.net;

import java.io.IOException;

import javax.xml.soap.MessageFactory;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;;

/**
 *
 * @author mulikevs
 */
public class MoneyExchangeStatusRequest {

	private String httpsURL;
	private String reference;

	public MoneyExchangeStatusRequest(String url, String reference) {
		this.httpsURL = url;
		this.reference = reference;

	}

	public SOAPMessage createCheckSOAPRequest() throws SOAPException, IOException {
		MessageFactory mf = MessageFactory.newInstance();
		SOAPMessage sm = mf.createMessage();

		SOAPPart soapPart = sm.getSOAPPart();

		String myNamespace = "urn";
		String myNamespaceURI = "urn:WSmoneyIntf-IWSmoney";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);

		// Soap Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("MEGetTransactionMoEx", myNamespace);

		SOAPElement login = soapBodyElem.addChildElement("login");
		login.addChildElement("Username").addTextNode("7500");
		login.addChildElement("Password").addTextNode("I8383AQL08NI");// Add to config properties
		login.addChildElement("Version").addTextNode("1");

		SOAPElement refElem = soapBodyElem.addChildElement("Reference").addTextNode(reference);

		sm.saveChanges();
		// sm.writeTo(System.out);

		return sm;

	}

	public SOAPBody callCheckSOAPWebService() {
		try {
			SOAPConnectionFactory scf = SOAPConnectionFactory.newInstance();
			SOAPConnection sc = scf.createConnection();

			SOAPMessage sresponse = sc.call(createCheckSOAPRequest(), httpsURL);
			// Print the SOAP Response

			return sresponse.getSOAPBody();

		} catch (Exception e) {
			System.err.println("Error!! Unable to process request");
			e.printStackTrace();
			return null;
		}
	}

}

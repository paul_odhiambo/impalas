package com.impalapay.airtel.util.net;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.InputSource;

public class YoUgandaRequestUtil {

	public static String getXMLValue(String results, String xpExpr) throws XPathExpressionException {
		String value = null;
		try {
			InputStream is = new ByteArrayInputStream(results.getBytes());

			XPath xp = XPathFactory.newInstance().newXPath();
			InputSource isource = new InputSource(is);
			value = xp.evaluate(xpExpr, isource);

		} catch (Exception e) {
			// catch it
			e.printStackTrace();
			value = "";
		}

		return value;

	}
	
	public static String createCheckNameXML(String msisdn,String username,String password){
		try{
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
			 
	        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

	        Document document = documentBuilder.newDocument();
	        
	        Element root = document.createElement("AutoCreate");
	        document.appendChild(root);
	        
	        //Request Element
	        Element request = document.createElement("Request");
	        root.appendChild(request);
	        
	        //Child elements
	        
	        Element usernamechild = document.createElement("APIUsername");
	        usernamechild.appendChild(document.createTextNode(username));//From config
	        request.appendChild(usernamechild);
	        
	        Element passwordchild = document.createElement("APIPassword");
	        passwordchild.appendChild(document.createTextNode(password));//From Config
	        request.appendChild(passwordchild);
	        
	        Element methodchild = document.createElement("Method");
	        methodchild.appendChild(document.createTextNode("acgetmsisdnkycinfo"));//constant
	        request.appendChild(methodchild);
	        
	        Element referencechild = document.createElement("Msisdn");
	        referencechild.appendChild(document.createTextNode(msisdn));
	        request.appendChild(referencechild);
	        
	       
	        TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        Transformer transformer = transformerFactory.newTransformer();
	        
	        StringWriter sw = new StringWriter();
	        
	        transformer.transform(new DOMSource(document), new StreamResult(sw));
	        return sw.toString();
	        
			} catch(Exception e){
				System.out.println("ERROR WHILE CONSTRUCTING YO UGANDA createCheckNameXML");
				return null;
			}
	}

	public static String createWithdrawFundsXML(String msisdn, String amount,String username,String password) {
		try {
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

			Document document = documentBuilder.newDocument();

			Element root = document.createElement("AutoCreate");
			document.appendChild(root);

			// Request Element
			Element request = document.createElement("Request");
			root.appendChild(request);

			// Child elements

			Element usernamechild = document.createElement("APIUsername");
			usernamechild.appendChild(document.createTextNode(username));// From config
			request.appendChild(usernamechild);

			Element passwordchild = document.createElement("APIPassword");
			passwordchild.appendChild(document.createTextNode(password));// From Config
			request.appendChild(passwordchild);

			Element methodchild = document.createElement("Method");
			methodchild.appendChild(document.createTextNode("acwithdrawfunds"));// constant
			request.appendChild(methodchild);

			Element amountchild = document.createElement("Amount");
			amountchild.appendChild(document.createTextNode(amount));
			request.appendChild(amountchild);

			Element accountchild = document.createElement("Account");
			accountchild.appendChild(document.createTextNode(msisdn));
			request.appendChild(accountchild);

			Element narrativechild = document.createElement("Narrative");
			narrativechild.appendChild(document.createTextNode("Remit"));// constant
			request.appendChild(narrativechild);

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();

			StringWriter sw = new StringWriter();

			transformer.transform(new DOMSource(document), new StreamResult(sw));
			return sw.toString();

		} catch (Exception e) {
			System.out.println("ERROR WHILE CONSTRUCTING YO UGANDA createWithdrawFundsXML");
			return null;
		}

	}
	
	public static String createCheckTransactionStatusXML(String reference,String apiusername,String apipassword){
		try{
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
			 
	        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

	        Document document = documentBuilder.newDocument();
	        
	        Element root = document.createElement("AutoCreate");
	        document.appendChild(root);
	        
	        //Request Element
	        Element request = document.createElement("Request");
	        root.appendChild(request);
	        
	        //Child elements
	        
	        Element usernamechild = document.createElement("APIUsername");
	        usernamechild.appendChild(document.createTextNode(apiusername));//From config
	        request.appendChild(usernamechild);
	        
	        Element passwordchild = document.createElement("APIPassword");
	        passwordchild.appendChild(document.createTextNode(apipassword));//From Config
	        request.appendChild(passwordchild);
	        
	        Element methodchild = document.createElement("Method");
	        methodchild.appendChild(document.createTextNode("actransactioncheckstatus"));//constant
	        request.appendChild(methodchild);
	        
	        Element referencechild = document.createElement("TransactionReference");
	        referencechild.appendChild(document.createTextNode(reference));
	        request.appendChild(referencechild);
	        
	       
	        TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        Transformer transformer = transformerFactory.newTransformer();
	        
	        StringWriter sw = new StringWriter();
	        
	        transformer.transform(new DOMSource(document), new StreamResult(sw));
	        return sw.toString();
	        
			} catch(Exception e){
				System.out.println("ERROR WHILE CONSTRUCTING YO UGANDA createCheckTransactionStatusXML");
				return null;
			}
		
	}
	
	public static String createDepositFundsCollectionXML(String msisdn, String amount,String apiusername,String apipassword){
		try{
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

			Document document = documentBuilder.newDocument();

			Element root = document.createElement("AutoCreate");
			document.appendChild(root);

			//Request Element
			Element request = document.createElement("Request");
			root.appendChild(request);

			//Child elements

			Element usernamechild = document.createElement("APIUsername");
			usernamechild.appendChild(document.createTextNode(apiusername));//From config ----LIVE
			request.appendChild(usernamechild);

			Element passwordchild = document.createElement("APIPassword");
			passwordchild.appendChild(document.createTextNode(apipassword));//From Config
			request.appendChild(passwordchild);


			Element methodchild = document.createElement("Method");
			methodchild.appendChild(document.createTextNode("acdepositfunds"));//constant
			request.appendChild(methodchild);

			Element nonblocking = document.createElement("NonBlocking");
			nonblocking.appendChild(document.createTextNode("FALSE"));
			request.appendChild(nonblocking);


			Element amountchild = document.createElement("Amount");
			amountchild.appendChild(document.createTextNode(amount));
			request.appendChild(amountchild);


			Element accountchild = document.createElement("Account");
			accountchild.appendChild(document.createTextNode(msisdn));
			request.appendChild(accountchild);

			Element narrativechild = document.createElement("Narrative");
			narrativechild.appendChild(document.createTextNode("Remit"));//constant
			request.appendChild(narrativechild);

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();

			StringWriter sw = new StringWriter();

			transformer.transform(new DOMSource(document), new StreamResult(sw));
			return sw.toString();

		} catch(Exception e){
			System.out.println("ERROR WHILE CONSTRUCTING YO UGANDA createDepositFundsCollectionXML");
			return null;
		}

	}

}

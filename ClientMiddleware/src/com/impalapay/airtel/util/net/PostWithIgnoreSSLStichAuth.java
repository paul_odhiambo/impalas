package com.impalapay.airtel.util.net;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
//import org.apache.http.util.EntityUtils;

import org.apache.http.util.EntityUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * A thread that can send POST text data, to both unsecure HTTP and secure HTTPS
 * <p>
 * Copyright (c) ImpalapayLtd.,Sep 13, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class PostWithIgnoreSSLStichAuth extends Thread {

	// Number of seconds to wait before Posting a message after an unsuccessful
	// try
	final int RETRY_WAIT = 60;

	// Number of times to retry before quitting
	// This is currently calculated as the number of seconds in a day divided by
	// the RETRY_WAIT
	final int RETRY_NUMBER = 1450;

	private String urlStr;
	private String params;
	boolean retry;
	private Logger logger;
	private URL url;
	String response = "";

	/**
	 * Disable default constructor
	 */
	private PostWithIgnoreSSLStichAuth() {
	}

	/**
	 * @param urlStr
	 * @param params
	 * @param retry
	 *            whether or not to keep retrying if the POST fails
	 */
	public PostWithIgnoreSSLStichAuth(String urlStr, String params) {
		this.urlStr = urlStr;
		this.params = params;
		logger = Logger.getLogger(this.getClass());
	}

	/**
	 * 
	 */

	public String runurl() {

		try {
			url = new URL(urlStr);

			if (StringUtils.equalsIgnoreCase(url.getProtocol(), "http")) {
				response = doPost();

			} else if (StringUtils.equalsIgnoreCase(url.getProtocol(), "https")) {
				response = doPostSecure();
				//response = doPost();
			}

		} catch (MalformedURLException e) {
			logger.error("MalformedURLException for URL: '" + urlStr + "'");
			logger.error(ExceptionUtils.getStackTrace(e));
		}

		return response;
	}

	/**
	 * 
	 * @param url
	 * @param params
	 * @param retry
	 * @return
	 */
	private String doPost() {
		String jsonResult = "";

		try {
			
		
			// Create a context that doesn't check certificates.
			//SSLContext sslContext = SSLContext.getInstance("TLS");
			//TrustManager[] trustMgr = getTrustManager();

			//sslContext.init(null, // key manager
					//trustMgr, // trust manager
					//new SecureRandom()); // random number generator
			//HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

			url = new URL(urlStr);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			//con.setRequestProperty("content-type", "text/xml; charset=utf-8");
			//con.setRequestProperty("SOAPAction", "http://www.example.org/MuniGateway/Topup");
			con.setRequestMethod("POST");
			con.setConnectTimeout(15000); // set timeout to 30 seconds
			con.setReadTimeout(15000);
			con.setDoOutput(true);
			
			
			// Create a context that doesn't check certificates.
			//SSLContext sslContext = SSLContext.getInstance("TLS");
			//TrustManager[] trustMgr = getTrustManager();

			//sslContext.init(null, // key manager
					//trustMgr, // trust manager
					//new SecureRandom()); // random number generator
			//HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
	        //HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

			
			// Create all-trusting host name verifier
	       

			//url = new URL(urlStr);
			//HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			//con.setRequestProperty("content-type", "text/xml; charset=utf-8");
			//con.setRequestProperty("SOAPAction", "http://www.example.org/MuniGateway/Topup");
			//con.setRequestMethod("POST");
			//con.setConnectTimeout(15000); // set timeout to 30 seconds
			//con.setReadTimeout(15000);
			//con.setDoOutput(true);
			// ######################################################################
			// Send data to the output
			// ######################################################################
			sendData(con, params);

			// ######################################################################
			// Dump all the content
			// #######################################################################
			response = getContent(con);

		} catch (MalformedURLException e) {
			System.err.println("MalformedURLException");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IOException");
			e.printStackTrace();

		//} catch (NoSuchAlgorithmException e) {
			//System.err.println("NoSuchAlgorithmException");
			//e.printStackTrace();

		//} catch (KeyManagementException e) {
			//System.err.println("KeyManagementException");
			//e.printStackTrace();
		}

		return response;
	}
	
	// Create all-trusting host name verifier
    HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

	// ##################################################################
	/**
	 * Send data to the url
	 * 
	 * @param con
	 */
	// #################################################################
	private void sendData(HttpURLConnection con, String args) {
		if (con != null) {

			try {
				// send data to output
				// DataOutputStream writer = new DataOutputStream(con.getOutputStream());

				// writer.writeBytes(args);
				// send data to output
				OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());

				writer.write(args);
				writer.flush();
				writer.close();

			} catch (IOException e) {
				System.err.println("IOException");
				e.printStackTrace();
			}
		}
	}

	// ###############################################################
	/**
	 * @param con
	 */
	// ###############################################################
	private String getContent(HttpURLConnection con) {
		StringBuffer buff = new StringBuffer("");
		String jsonResult = "";

		if (con != null) {

			try {

				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

				String input;

				while ((input = br.readLine()) != null) {
					buff.append(input + "\n");
				}
				jsonResult = buff.toString().trim();
				br.close();

			} catch (IOException e) {
				// e.printStackTrace();
				logger.error("The Real Response throwing an exception XXXXXX" + jsonResult+"/n"+"===========87878");

				//String replacedStr1 = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
						//+ "   <soapenv:Body>\n"
						//+ "      <TopupResponse xmlns=\"http://www.example.org/MuniGateway/\">\n"
						//+ "         <topupResponse xmlns=\"\">\n" + "         <topupSummary>"
						//+ "          <responseCode>" + "0" + "</responseCode>\n" + "          <responseDesc>"
						//+ "SUCCESS_WITH_EXCEPTION" + "</responseDesc>\n" + "         </topupSummary>\n"
						//+ "         </topupResponse>\n" + "      </TopupResponse>\n" + "   </soapenv:Body>\n"
						//+ "</soapenv:Envelope>";

				//jsonResult = replacedStr1;

			}
		} // end 'if(con != null)'

		return jsonResult;
	}

	// ################################################################
	/**
	 * @return {@link TrustManager}
	 */
	// ################################################################
	private TrustManager[] getTrustManager() {

		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };

		return certs;
	}

	/**
	 * 
	 * @param url
	 * @param params
	 * @param retry
	 * @return
	 */
	public String doPostSecure() {

		
		String response = "";

		try {
			// Create a context that doesn't check certificates.
			SSLContext sslContext = SSLContext.getInstance("TLS");
			TrustManager[] trustMgr = getTrustManager();

			sslContext.init(null, // key manager
					trustMgr, // trust manager
					new SecureRandom()); // random number generator
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

			url = new URL(urlStr);
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestMethod("POST");
			con.setDoOutput(true);

			//

			// ######################################################################
			// Send data to the output
			// ######################################################################
			sendData(con, params);

			// ######################################################################
			// Dump all the content
			// #######################################################################
			response = getContent(con);

		} catch (MalformedURLException e) {
			System.err.println("MalformedURLException");
			e.printStackTrace();

		} catch (IOException e) {
			System.err.println("IOException");
			e.printStackTrace();

		} catch (NoSuchAlgorithmException e) {
			System.err.println("NoSuchAlgorithmException");
			e.printStackTrace();

		} catch (KeyManagementException e) {
			System.err.println("KeyManagementException");
			e.printStackTrace();
		}

		return response;
	}

	/**
	 * 
	 * @param url
	 * @param params
	 * @param retry
	 * @return
	 */
	public String doGetSecure() {

		// CloseableHttpClient httpclient = HttpClients.createDefault();
		StringEntity entity;
		String out = "", jsonResult = "";
		Gson g = new GsonBuilder().setPrettyPrinting().serializeNulls()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		JsonArray errorarray = new JsonArray();
		JsonObject errorobject = new JsonObject();
		JsonObject errordata = new JsonObject();
		JsonObject errorarrayobject = new JsonObject();

		try {
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());

			// Trust own CA and all self-signed certs
			SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(trustStore, new TrustSelfSignedStrategy())
					.build();

			// Allow TLSv1 protocol only
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] { "TLSv1" },
					null, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

			// entity = new StringEntity(params);
			HttpGet httpget = new HttpGet(urlStr);
			// httppost.setEntity(entity);
			httpget.setHeader("Content-Type", "application/json");
			HttpResponse response = httpclient.execute(httpget);
			/**
			 * // for debugging System.out.println(entity.getContentType());
			 * System.out.println(entity.getContentLength());
			 * System.out.println(EntityUtils.toString(entity));
			 * System.out.println(EntityUtils.toByteArray(entity).length);
			 * 
			 * // System.out.println("----------------------------------------");
			 * System.out.println(response.getStatusLine()); System.out.println(url);
			 **/
			HttpEntity responseEntity = response.getEntity();
			if (responseEntity != null) {
				out = EntityUtils.toString(responseEntity);
			}

			try {
				JsonElement root = new JsonParser().parse(out);
			} catch (Exception e) {

				errorarrayobject.addProperty("status_code", "00032");
				errorarrayobject.addProperty("status_description", "HUB_LEVEL_INVALID_RESPONSE_FROM_WALLET");
				errorarray.add(errorarrayobject);

				for (int i = 0; i < errorarray.size(); i++) {
					errordata = errorarray.get(i).getAsJsonObject();
				}
				errorobject.add("errors", errordata);
				jsonResult = g.toJson(errorobject);

				out = jsonResult;
			}
			// System.out.println(out);

		} catch (UnsupportedEncodingException e) {
			logger.error("UnsupportedEncodingException for URL: '" + url + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} catch (ClientProtocolException e) {
			logger.error("ClientProtocolException for URL: '" + url + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} catch (IOException e) {
			logger.error("IOException for URL: '" + url + "'");
			logger.error(ExceptionUtils.getStackTrace(e));
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return out;
		// return usernames;
	}
}

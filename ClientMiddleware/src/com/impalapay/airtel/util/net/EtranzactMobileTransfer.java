/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impalapay.airtel.util.net;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

/**
 *
 * @author mulikevs
 */
public class EtranzactMobileTransfer {

	/**
	 * Starting point for the SAAJ - SOAP Client Testing
	 */
	public String Mobilepayout(String direction, String action, String terminalId, String destination, String endPoint,
			String pin, String amount, String reference, String description, String remiturlss)
			throws IOException, SOAPException, Exception {

		String switchresponse = null;

		// Create SOAP Connection
		String resp = "";
		SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
		SOAPConnection soapConnection = soapConnectionFactory.createConnection();

		// Send SOAP Message to SOAP Server

		// String url = "https://demo.etranzact.com/FGate/ws?wsdl";
		SSLContext sslContext = SSLContext.getInstance("TLS");
		TrustManager[] trustMgr = getTrustManager();

		sslContext.init(null, // key manager
				trustMgr, // trust manager
				new SecureRandom()); // random number generator
		HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
		SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(direction, action, terminalId, destination,
				endPoint, pin, amount, reference, description), remiturlss);

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		soapResponse.writeTo(out);
		// System.out.println("Kasambe"+out.toString());
		String xml = out.toString();
		JsonObject root2 = null;
		Gson g = new GsonBuilder().disableHtmlEscaping().create();

		SAXBuilder builder = new SAXBuilder();
		Reader in = new StringReader(xml);
		Document doc = null;
		Element root = null;
		Element error = null;
		Element status_message = null;
		Element status_reference = null;
		String status_code = "";
		String message = "";
		String response_reference = "";
		try {
			doc = builder.build(in);
			root = doc.getRootElement();
			Element body = root.getChild("Body",
					Namespace.getNamespace("S", "http://schemas.xmlsoap.org/soap/envelope/"));
			Element processResponse = body.getChild("processResponse",
					Namespace.getNamespace("ns2", "http://ws.fundgate.etranzact.com/"));
			Element response = processResponse.getChild("response");
			error = response.getChild("error");
			status_message = response.getChild("message");
			status_reference = response.getChild("otherReference");
			status_code = error.getText();
			message = status_message.getText();
			response_reference = status_reference.getText();

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("status_code: " + status_code + "\nmessage: " + message);

		System.out.println(response_reference);

		root2 = new JsonObject();

		root2.addProperty("status_code", status_code);
		root2.addProperty("message", message);
		root2.addProperty("reference", response_reference);

		switchresponse = g.toJson(root2);

		return switchresponse;
	}

	private static SOAPMessage createSOAPRequest(String direction, String action, String terminalId, String destination,
			String endPoint, String pin, String amount, String reference, String description) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String serverURI = "http://ws.fundgate.etranzact.com/";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("ws", serverURI);

		SOAPBody soapBody = envelope.getBody();
		SOAPElement ElemProcess = soapBody.addChildElement("process", "ws");
		SOAPElement ElemRequest = ElemProcess.addChildElement("request");

		SOAPElement ElemDirection = ElemRequest.addChildElement("direction");
		ElemDirection.addTextNode(direction);

		SOAPElement ElemAction = ElemRequest.addChildElement("action");
		ElemAction.addTextNode(action);

		SOAPElement ElemTerminalId = ElemRequest.addChildElement("terminalId");
		ElemTerminalId.addTextNode(terminalId);

		SOAPElement ElemTransaction = ElemRequest.addChildElement("transaction");

		SOAPElement ElemPin = ElemTransaction.addChildElement("pin");
		ElemPin.addTextNode(pin);

		SOAPElement ElemAmount = ElemTransaction.addChildElement("amount");
		ElemAmount.addTextNode(amount);

		SOAPElement ElemDescription = ElemTransaction.addChildElement("description");
		ElemDescription.addTextNode(description);

		SOAPElement Elemdestination = ElemTransaction.addChildElement("destination");
		Elemdestination.addTextNode(destination);

		SOAPElement ElemEndPoint = ElemTransaction.addChildElement("endPoint");
		ElemEndPoint.addTextNode(endPoint);

		SOAPElement ElemReference = ElemTransaction.addChildElement("reference");
		ElemReference.addTextNode(reference);

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", serverURI + "RequestTransactionByTimeInterval");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}

	// ################################################################
	/**
	 * @return {@link TrustManager}
	 */
	// ################################################################
	private TrustManager[] getTrustManager() {

		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };

		return certs;
	}

}

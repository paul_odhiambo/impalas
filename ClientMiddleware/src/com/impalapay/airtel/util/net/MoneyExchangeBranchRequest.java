/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impalapay.airtel.util.net;

import java.io.IOException;

import javax.xml.soap.MessageFactory;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.commons.io.output.ByteArrayOutputStream;;

/**
 *
 * @author mulikevs
 */
public class MoneyExchangeBranchRequest {

	private String httpsURL;
	private String country;
	
	public MoneyExchangeBranchRequest(String url, String country){
		this.httpsURL = url;
		this.country = country;
		
	}
	
	public SOAPMessage createCheckSOAPRequest() throws SOAPException, IOException {
		MessageFactory mf = MessageFactory.newInstance();
		SOAPMessage sm = mf.createMessage();

		SOAPPart soapPart = sm.getSOAPPart();

		String myNamespace = "urn";
		String myNamespaceURI = "urn:WSmoneyIntf-IWSmoney";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);
	
		//Soap Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("MEGetActiveBranchesMoEx", myNamespace);
		
		SOAPElement login = soapBodyElem.addChildElement("login");
		login.addChildElement("Username").addTextNode("8320");
		login.addChildElement("Password").addTextNode("H8DUX5TE01NX");//Add to config properties
		login.addChildElement("Version").addTextNode("1");
		
		soapBodyElem.addChildElement("IdCountry").addTextNode(country);
		
		sm.saveChanges();
		
		sm.writeTo(System.out);
		
		System.out.println("THE SOAP REQUEST NOW "+sm.toString());
		
		return sm;
		
	}
	public SOAPBody callCheckSOAPWebService() {
		try {
			SOAPConnectionFactory scf = SOAPConnectionFactory.newInstance();
			SOAPConnection sc = scf.createConnection();

			SOAPMessage sresponse = sc.call(createCheckSOAPRequest(), httpsURL);
			// Print the SOAP Response
			ByteArrayOutputStream soapResponseBaos = new ByteArrayOutputStream();
			sresponse.writeTo(soapResponseBaos);
			  String soapResponseXml = soapResponseBaos.toString();
			  
			  System.out.print("tushatoboaMACHETTE  "+soapResponseXml);
			

			return sresponse.getSOAPBody();

		} catch (Exception e) {
			System.err.println("Error!! Unable to process request");
			e.printStackTrace();
			return null;
		}
}

}

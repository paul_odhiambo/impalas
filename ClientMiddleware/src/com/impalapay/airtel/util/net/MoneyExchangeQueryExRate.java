package com.impalapay.airtel.util.net;

import java.io.IOException;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.commons.io.output.ByteArrayOutputStream;

public class MoneyExchangeQueryExRate {

	private String httpsURL;

	public MoneyExchangeQueryExRate(String url) {
		this.httpsURL = url;

	}

	private SOAPMessage createCheckSOAPRequest() throws SOAPException, IOException {
		MessageFactory mf = MessageFactory.newInstance();
		SOAPMessage sm = mf.createMessage();

		SOAPPart soapPart = sm.getSOAPPart();

		String myNamespace = "urn";
		String myNamespaceURI = "urn:WSmoneyIntf-IWSmoney";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);

		/*
		 * <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 * xmlns:xsd="http://www.w3.org/2001/XMLSchema"
		 * xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
		 * xmlns:urn="urn:WSmoneyIntf-IWSmoney"> <soapenv:Header/> <soapenv:Body>
		 * <urn:MEGetForeignExchangeRates
		 * soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"> <login
		 * xsi:type="urn:TWSauth" xmlns:urn="urn:WSmoneyClasses"> <Username
		 * xsi:type="xsd:string">7500</Username> <Password
		 * xsi:type="xsd:string">I8383AQL08NI</Password> <Version
		 * xsi:type="xsd:string">1</Version> </login> </urn:MEGetForeignExchangeRates>
		 * </soapenv:Body> </soapenv:Envelope>
		 */

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("MEGetForeignExchangeRates", myNamespace);
		SOAPElement cancelrequest = soapBodyElem.addChildElement("login");

		cancelrequest.addChildElement("Username").addTextNode("8320");
		cancelrequest.addChildElement("Password").addTextNode("H8DUX5TE01NX");// Add to config properties
		cancelrequest.addChildElement("Version").addTextNode("1");

		sm.saveChanges();

		sm.writeTo(System.out);
		
		System.out.println("THE SOAP REQUEST NOW "+sm.toString());


		return sm;

	}

	public SOAPBody callFXRateSOAPWebService() {
		try {
			SOAPConnectionFactory scf = SOAPConnectionFactory.newInstance();
			SOAPConnection sc = scf.createConnection();

			SOAPMessage sresponse = sc.call(createCheckSOAPRequest(), httpsURL);
			// Print the SOAP Response
			//ByteArrayOutputStream soapResponseBaos = new ByteArrayOutputStream();
			//sresponse.writeTo(soapResponseBaos);
			  //String soapResponseXml = soapResponseBaos.toString();
			  
			  //System.out.print("tushatoboaMACHETTE  "+soapResponseXml);
			

			return sresponse.getSOAPBody();

		} catch (Exception e) {
			System.err.println("Error!! Unable to process request");
			e.printStackTrace();
			return null;
		}
	}

}

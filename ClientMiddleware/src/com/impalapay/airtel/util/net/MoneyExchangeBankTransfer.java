package com.impalapay.airtel.util.net;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.commons.io.output.ByteArrayOutputStream;


public class MoneyExchangeBankTransfer {
	
	
	private String httpsURL;
	private HashMap<String, String> params;

	public MoneyExchangeBankTransfer(String url, HashMap<String, String> params) {
		this.params = params;
		this.httpsURL = url;

	}

	public SOAPMessage createAddSOAPRequest() throws SOAPException, IOException {
		MessageFactory mf = MessageFactory.newInstance();
		SOAPMessage sm = mf.createMessage();

		SOAPPart soapPart = sm.getSOAPPart();

		String myNamespace = "urn";
		String myNamespaceURI = "urn:WSmoneyIntf-IWSmoney";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("MEAddTransaction", myNamespace);
		
		SOAPElement login = soapBodyElem.addChildElement("login");
		login.addChildElement("Username").addTextNode("8320");
		login.addChildElement("Password").addTextNode("H8DUX5TE01NX");//Add to config properties
		login.addChildElement("Version").addTextNode("1");

		SOAPElement transaction = soapBodyElem.addChildElement("transaction");

		Set set = params.entrySet();
		Iterator ite = set.iterator();
		while (ite.hasNext()) {
			Map.Entry me = (Map.Entry) ite.next();
			transaction.addChildElement((String) me.getKey()).addTextNode((String) me.getValue());

		}

		sm.saveChanges();
		sm.writeTo(System.out);
		
		System.out.println("THE SOAP REQUEST NOW "+sm.toString());

		
		return sm;
	}

	public SOAPBody callAddSOAPWebService() {
		try {
			SOAPConnectionFactory scf = SOAPConnectionFactory.newInstance();
			SOAPConnection sc = scf.createConnection();

			SOAPMessage sresponse = sc.call(createAddSOAPRequest(), httpsURL);
			// Print the SOAP Response
			
			ByteArrayOutputStream soapResponseBaos = new ByteArrayOutputStream();
			sresponse.writeTo(soapResponseBaos);
			  String soapResponseXml = soapResponseBaos.toString();
			  
			 System.out.print("tushatoboaMACHETTE  "+soapResponseXml);

			return sresponse.getSOAPBody();

		} catch (Exception e) {
			System.err.println("Error!! Unable to process request");
			e.printStackTrace();
			return null;
		}

	}

}

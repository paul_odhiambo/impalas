package com.impalapay.airtel.util;

public class NameSplitUtil {

	String lastName = "";
	String firstName = "";

	public NameSplitUtil(String name) {
		// This constructor has one parameter, name.
		// System.out.println("Name chosen is :" + name );

		if (name.split("\\w+").length > 1) {

			lastName = name.substring(name.lastIndexOf(" ") + 1);
			firstName = name.substring(0, name.lastIndexOf(' '));
		} else {
			firstName = name;
		}

		// System.out.println(firstName);
	}

	public void setFirstName() {

	}

	public String getFirstName() {
		return firstName;
	}

	public void setLastName() {

	}

	public String getLastName() {
		return lastName;
	}

}

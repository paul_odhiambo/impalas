package com.impalapay.airtel.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class MpesaCertRSAencodingUtil {

	public String encodePassword(String filepath, String password) {
		String statuscode = "", jsonResult = "", success = "S000", failed = "00029", encodedkey = "",
				safaricompassword = "", encodedinitiatorMessage = "", statusdescription = "";
		JsonObject responserequest = null;
		Gson g = new GsonBuilder().disableHtmlEscaping().create();

		try {

			CertificateFactory fac = CertificateFactory.getInstance("X.509");
			// FileInputStream is = new
			// FileInputStream("/home/eugene/Programs/SandboxCertificate.cer");
			FileInputStream is = new FileInputStream(filepath);
			X509Certificate cert = (X509Certificate) fac.generateCertificate(is);
			// GET THE CYPHER
			// Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");

			// a Cipher object initialized for encryption with the public key
			Cipher encryptCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			encryptCipher.init(Cipher.ENCRYPT_MODE, cert.getPublicKey());

			safaricompassword = password;

			// invoke the doFinal method to encrypt our message. Note that it accepts only
			// byte array arguments,
			// so we need to transform our string before:

			//byte[] safaricompasswordMessageBytes = safaricompassword.getBytes(StandardCharsets.UTF_8);
			byte[] safaricompasswordMessageBytes = safaricompassword.getBytes();

			byte[] encryptedsafaricompasswordMessageBytes = encryptCipher.doFinal(safaricompasswordMessageBytes);

			// Now, our message is successfully encoded. If we'd like to store it in a
			// database or send it via REST API,
			// it would be more convenient to encode it with the Base64 Alphabet:
			encodedinitiatorMessage = Base64.getEncoder().encodeToString(encryptedsafaricompasswordMessageBytes);

			// Now, let's see how we can decrypt the message to its original form. For this,
			// we'll need another Cipher instance.
			// This time we'll initialize it with a decryption mode and a private key:

			// Cipher decryptCipher = Cipher.getInstance("RSA");
			// decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);
			// We'll invoke the cipher as previously with the doFinal method:

			// [] decryptedMessageBytes = decryptCipher.doFinal(encryptedMessageBytes);
			// String decryptedMessage = new String(decryptedMessageBytes,
			// StandardCharsets.UTF_8);

			// System.out.println("From: " + cert.getNotBefore());
			// System.out.println("Until: " + cert.getNotAfter());
			// System.out.println("public key: " + cert.getPublicKey());
			// System.out.println("ENCODED INITIATOR :"+encodedinitiatorMessage);
			// return encodedinitiatorMessage;
			statuscode = success;
			statusdescription = "encryptionsuccess";
			encodedkey = encodedinitiatorMessage;

		} catch (FileNotFoundException e) {
			statuscode = failed;
			statusdescription = "FileNotFoundException";

		} catch (CertificateException e) {
			statuscode = failed;
			statusdescription = "CertificateException";
		} catch (NoSuchAlgorithmException e) {
			statuscode = failed;
			statusdescription = "NoSuchAlgorithmException";
		} catch (NoSuchPaddingException e) {
			statuscode = failed;
			statusdescription = "NoSuchPaddingException";
		} catch (InvalidKeyException e) {
			statuscode = failed;
			statusdescription = "InvalidKeyException";
		} catch (IllegalBlockSizeException e) {
			statuscode = failed;
			statusdescription = "IllegalBlockSizeException";
		} catch (BadPaddingException e) {
			statuscode = failed;
			statusdescription = "BadPaddingException";
		}

		responserequest = new JsonObject();
		responserequest.addProperty("status_code", statuscode);
		responserequest.addProperty("status_description", statusdescription);
		responserequest.addProperty("encodedkey", encodedkey);
		jsonResult = g.toJson(responserequest);

		return jsonResult;
	}

}

package com.impalapay.airtel.util;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class TestXml {
	public static void main(String arg[]) throws Exception {
		String xmlRecords1 = "<data><employee><name>A</name>" + "<title>Manager</title></employee></data>";

		String xmlRecords = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><makePaymentResponse><transactionId>12345</transactionId><responseId>12345</responseId><code>200</code><status>Success</status></makePaymentResponse>";

		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(xmlRecords));

		Document doc = db.parse(is);
		NodeList nodes = doc.getElementsByTagName("makePaymentResponse");

		for (int i = 0; i < nodes.getLength(); i++) {
			Element element = (Element) nodes.item(i);

			NodeList name = element.getElementsByTagName("transactionId");
			Element line = (Element) name.item(0);
			System.out.println("Name: " + getCharacterDataFromElement(line));

			NodeList title = element.getElementsByTagName("code");
			line = (Element) title.item(0);
			System.out.println("Title: " + getCharacterDataFromElement(line));
		}

	}

	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}
}

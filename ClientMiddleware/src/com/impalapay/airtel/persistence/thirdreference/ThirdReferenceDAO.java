package com.impalapay.airtel.persistence.thirdreference;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.thirdreference.ThirdPartyReference;
import com.impalapay.airtel.persistence.GenericDAO;

public class ThirdReferenceDAO extends GenericDAO implements AirtelThirdreference {
	public static ThirdReferenceDAO thirdreferenceDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return {@link ThirdReferenceDAO}
	 */
	public static ThirdReferenceDAO getInstance() {

		if (thirdreferenceDAO == null) {
			thirdreferenceDAO = new ThirdReferenceDAO();
		}

		return thirdreferenceDAO;
	}

	/**
	 * 
	 */
	public ThirdReferenceDAO() {
		super();

	}

	/**
	 * 
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public ThirdReferenceDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);
	}

	@Override
	public List<ThirdPartyReference> getReference(String uuid) {

		List<ThirdPartyReference> list = new LinkedList<>();

		ThirdPartyReference reference = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM thirdreference WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				reference = beanProcessor.toBean(rset, ThirdPartyReference.class);

				list.add(reference);
			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching reference with uuid" + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<ThirdPartyReference> getAllReference() {
		List<ThirdPartyReference> list = new LinkedList<>();

		ThirdPartyReference reference = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM thirdreference;");

			rset = pstmt.executeQuery();

			while (rset.next()) {
				reference = beanProcessor.toBean(rset, ThirdPartyReference.class);
				list.add(reference);

			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all references");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<ThirdPartyReference> getAllReference(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean addReferences(ThirdPartyReference references) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO thirdreference (uuid, transactionuuid,referencenumber, servertime) VALUES (?, ?, ?, ?);");

			pstmt.setString(1, references.getUuid());
			pstmt.setString(2, references.getTransactionuuid());
			pstmt.setString(3, references.getReferencenumber());
			pstmt.setTimestamp(4, new Timestamp(references.getServerTime().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + references);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public ThirdPartyReference getTransactionReference(String transactionuuid) {
		ThirdPartyReference reference = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM thirdreference WHERE transactionuuid=?;");
			pstmt.setString(1, transactionuuid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				reference = beanProcessor.toBean(rset, ThirdPartyReference.class);

			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching reference with transactionuuid" + transactionuuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return reference;
	}

}

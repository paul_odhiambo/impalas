package com.impalapay.airtel.persistence.thirdreference;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.thirdreference.ThirdPartyReference;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.TransactionStatus;

public interface AirtelThirdreference {

	/**
	 * Gets all {@link TransactionReferences}s which have an UUID matching the
	 * argument. The list returned is not arranged in any particular order.
	 * 
	 * @param uuid
	 * @return TransactionReferences request(s)
	 */
	public List<ThirdPartyReference> getReference(String uuid);

	/**
	 * Gets all transaction requests
	 *
	 * @return List<{@link TransactionReferences}> all transaction requests
	 */
	public List<ThirdPartyReference> getAllReference();

	/**
	 * Gets all transactions requests between the specified fromIndex, inclusive,
	 * and toIndex, exclusive.
	 *
	 * @param fromIndex
	 * @param toIndex
	 * 
	 * @return List<{@link TransactionReferences}> all TransactionReferences
	 *         requests
	 */
	public List<ThirdPartyReference> getAllReference(int fromIndex, int toIndex);

	/**
	 *
	 * @param TransactionReferences
	 * @return ThirdPartyReference
	 */
	public boolean addReferences(ThirdPartyReference references);

	/**
	 * Gets the transaction request(s) status with the provided transactionuuid
	 * 
	 *
	 * @param transactionreference
	 * @return {ThirdPartyReference} transaction request(s)
	 */
	public ThirdPartyReference getTransactionReference(String transactionuuid);

}

package com.impalapay.airtel.persistence.bank;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.bank.BankCodes;
import com.impalapay.airtel.beans.clientipaddress.ClientIP;
import com.impalapay.airtel.beans.geolocation.CountryMsisdn;
import com.impalapay.airtel.persistence.GenericDAO;

public class BankCodesDAO extends GenericDAO implements AirtelBankCodes {
	public static BankCodesDAO bankDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return {@link ThirdReferenceDAO}
	 */
	public static BankCodesDAO getInstance() {

		if (bankDAO == null) {
			bankDAO = new BankCodesDAO();
		}

		return bankDAO;
	}

	/**
	 * 
	 */
	public BankCodesDAO() {
		super();

	}

	/**
	 * 
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public BankCodesDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);
	}

	@Override
	public List<BankCodes> getBankCodes(String uuid) {

		List<BankCodes> list = new LinkedList<>();

		BankCodes bank = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM bank WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				bank = beanProcessor.toBean(rset, BankCodes.class);

				list.add(bank);
			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching bank with uuid" + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<BankCodes> getAllBankCodes() {
		List<BankCodes> list = new LinkedList<>();

		BankCodes bank = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM bank;");

			rset = pstmt.executeQuery();

			while (rset.next()) {
				bank = beanProcessor.toBean(rset, BankCodes.class);
				list.add(bank);

			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all bank");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<BankCodes> getAllBankCodes(int fromIndex, int toIndex) {
		List<BankCodes> list = new ArrayList<>();
		BankCodes s;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM bank ORDER BY countryuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, BankCodes.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all bankcodes from index " + fromIndex + " to index "
					+ toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public boolean addBankCodes(BankCodes bank) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO bank (uuid, bankname, countryuuid, networkuuid, bankcode, branchcode, iban, dateadded) VALUES (?, ?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, bank.getUuid());
			pstmt.setString(2, bank.getBankname());
			pstmt.setString(3, bank.getCountryuuid());
			pstmt.setString(4, bank.getNetworkuuid());
			pstmt.setString(5, bank.getBankcode());
			pstmt.setString(6, bank.getBranchcode());
			pstmt.setString(7, bank.getIban());
			pstmt.setTimestamp(8, new Timestamp(bank.getDateadded().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + bank);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public BankCodes getBankCodes(BankCodes bankcodes) {
		BankCodes c = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM bank WHERE bankcode=?;");
			pstmt.setString(1, bankcodes.getBankcode());
			// pstmt.setString(2, bankcodes.getUuid());

			rset = pstmt.executeQuery();

			if (rset.next()) {
				c = beanProcessor.toBean(rset, BankCodes.class);
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting a bank '" + bankcodes + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return c;
	}

	@Override
	public boolean updateBankCodes(String uuid, BankCodes bankcodes) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM bank WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				pstmt2 = conn.prepareStatement("UPDATE bank SET bankname=?, "
						+ "countryuuid=?,networkuuid=?,bankcode=?,branchcode=?,iban=? " + "WHERE uuid=?;");

				pstmt2.setString(1, bankcodes.getBankname());
				pstmt2.setString(2, bankcodes.getCountryuuid());
				pstmt2.setString(3, bankcodes.getNetworkuuid());
				pstmt2.setString(4, bankcodes.getBankcode());
				pstmt2.setString(5, bankcodes.getBranchcode());
				pstmt2.setString(6, bankcodes.getIban());
				pstmt2.setString(7, uuid);

				pstmt2.executeUpdate();

			} else {
				success = addBankCodes(bankcodes);
			}

		} catch (SQLException e) {
			logger.error(
					"SQLException when trying to update bankcodes with uuid '" + uuid + "' with " + bankcodes + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}
	
	/**
	 * 
	 * @param uuid
	 * @return
	 */
	@Override
	public boolean deletebankcode(String uuid) {
		boolean success = true;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("DELETE FROM bank WHERE uuid=?;");

			pstmt.setString(1, uuid);

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while Deleting " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}


}

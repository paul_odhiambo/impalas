package com.impalapay.airtel.persistence.geolocation;

import com.impalapay.airtel.beans.geolocation.Country;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Ignore;

import java.util.List;

/**
 * Tests the com.impalapay.airtel.persistence.country.CountryDAO
 * <p>
 * Copyright (c) impalapay Ltd., june 24, 2014
 * 
 * @author <a href="mailto:eugenechimita@impalapay.com">Eugene Chimita</a>
 * @author <a href="mailto:michael@impalapay.com">Michael Wakahe</a>
 * 
 */
public class TestCountryDAO {

	final String DB_NAME = "airteldblive";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "airtellive";
	final String DB_PASSWD = "ThejKoyb34";
	final int DB_PORT = 5432;

	final String Country_UUID = "1e0ea2bfe4294e78b63fdd1b27ea3b1d";
	final String Country_NAME = "Burkina Faso";
	final int Country_COUNT = 17;

	final String Country_UUID2 = "5db5fa02790e4ee0a8d7a538b4df820a";
	final String Country_balanceip = "test11";
	final String Country_remitip = "test211";
	final String Country_verifyip = "test31";

	final String Username = "machettee11";
	final String Password = "machettee1";

	private CountryDAO storage;

	/**
	 * Test method for
	 * com.impalapay.airtel.persistence.country.CountryDAO#getCountry(java.lang.String).
	 */
	@Ignore
	@Test
	public void testCountryString() {
		storage = new CountryDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		Country country = storage.getCountry(Country_UUID);
		assertEquals(country.getUuid(), Country_UUID);
		assertEquals(country.getName(), Country_NAME);

	}

	/**
	 * Test method for
	 * com.impalapay.airtel.persistence.country.CountryDAO#getAllCountries()
	 */
	@Test
	@Ignore
	public void testGetAllCountries() {
		storage = new CountryDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		List<Country> list = storage.getAllCountries();
		assertEquals(list.size(), Country_COUNT);

	}

	// @Ignore
	@Test
	public void testUpdateCountry() {
		storage = new CountryDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		Country testcountry = new Country();

		testcountry.setCountrybalanceip(Country_balanceip);
		testcountry.setCountryremitip(Country_remitip);
		testcountry.setCountryverifyip(Country_verifyip);
		testcountry.setUsername(Username);
		testcountry.setPassword(Password);
		testcountry.setUuid(Country_UUID2);

		assertTrue(storage.updateCountry(Country_UUID2, testcountry));
	}
}

package com.impalapay.airtel.persistence.commission;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.beans.commission.CommisionEstimate;

public class CommissionDAO extends GenericDAO implements SystemCommission {

	private static CommissionDAO commissionDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	public static CommissionDAO getInstance() {

		if (commissionDAO == null) {
			commissionDAO = new CommissionDAO();
		}

		return commissionDAO;
	}

	protected CommissionDAO() {
		super();
	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public CommissionDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

	}

	@Override
	public CommisionEstimate getsystemlog(String uuid) {
		CommisionEstimate s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM commissionestimate WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, CommisionEstimate.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting commission with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public boolean putCommission(CommisionEstimate commissionestimate) {
		boolean success = true;
		Date date;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO commissionestimate(uuid,transactionUuid,commissioncurrency,commision,receivercommission,"
							+ "receiverfullamount,statusuuid) VALUES (?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, commissionestimate.getUuid());
			pstmt.setString(2, commissionestimate.getTransactionUuid());
			pstmt.setString(3, commissionestimate.getCommissioncurrency());
			pstmt.setDouble(4, commissionestimate.getCommision());
			pstmt.setDouble(5, commissionestimate.getReceivercommission());
			pstmt.setDouble(6, commissionestimate.getReceiverfullamount());
			pstmt.setString(7, commissionestimate.getStatusuuid());
			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + commissionestimate);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<CommisionEstimate> getAllCommission() {
		List<CommisionEstimate> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM commissionestimate ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CommisionEstimate.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all commissionestimate.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<CommisionEstimate> getAllCommission(int fromIndex, int toIndex) {
		List<CommisionEstimate> list = new ArrayList<>();
		CommisionEstimate s;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn
					.prepareStatement("SELECT * FROM commissionestimate ORDER BY serverTime DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CommisionEstimate.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all commissionestimate from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

}

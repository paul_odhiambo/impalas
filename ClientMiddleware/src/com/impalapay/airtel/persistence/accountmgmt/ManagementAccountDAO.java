package com.impalapay.airtel.persistence.accountmgmt;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;

/**
 * Persistence description for {@link EscrowAccount}
 * <p>
 * Copyright (c) ImpalaPay LTD., June 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 *
 */
public interface ManagementAccountDAO {

	/**
	 *
	 * @param uuid
	 * @return EscrowAccount
	 */
	public ManagementAccount getAccount(String uuid);

	/**
	 *
	 * @param username
	 * @return EscrowAccount
	 */
	public ManagementAccount getAccountName(String username);

	/**
	 *
	 * @return List<EscrowAccount>
	 */
	public List<ManagementAccount> getAllAccounts();

	/**
	 *
	 * @param account
	 * @return boolean whether the EscrowAccount was added successfully or not.
	 */
	public boolean addAccount(ManagementAccount account);

	/**
	 *
	 * @param accountUuid
	 *            the uuid of the account to be modified
	 * 
	 * @param newAccount
	 *            the settings of the modifications. Some details such as creation
	 *            date cannot be modified.
	 * 
	 * @return boolean whether the account was updated successfully or not.
	 */
	public boolean updateAccount(String accountUuid, ManagementAccount newAccount);

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<ManagementAccount> getAllAccounts(int fromIndex, int toIndex);
}

/*
 ** Local Variables: mode: java c-basic-offset: 2 tab-width: 2 indent-tabs-mode:
 * nil End:
 **
 ** ex: set softtabstop=2 tabstop=2 expandtab:
 **
 */
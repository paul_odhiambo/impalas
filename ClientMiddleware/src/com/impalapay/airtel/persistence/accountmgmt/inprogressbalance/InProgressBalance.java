package com.impalapay.airtel.persistence.accountmgmt.inprogressbalance;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressBalancebyCountryHold;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressBalancebyCountryHoldHist;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressMasterBalanceHold;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressMasterBalanceHoldHist;

/**
 * Abstraction for persistence of balance in an the master account.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public interface InProgressBalance {

	/**
	 * We will start with Master Balanace.
	 */

	boolean deleteMasterBalanceOnSuccess(String transactionid);

	boolean putBalanceOnHoldAccount(InProgressMasterBalanceHoldHist masteraccount,
			InProgressBalancebyCountryHoldHist balancebycountry);

	boolean removeBalanceHoldSuccess(String transactionuuid);

	boolean removeBalanceHoldFail(String transactionuuid);

	// Balance by country on hold
	public List<InProgressBalancebyCountryHold> getAllBalancebyCountryHold(int fromIndex, int toIndex);

	public List<InProgressBalancebyCountryHoldHist> getAllBalancebyCountryHoldHist(int fromIndex, int toIndex);

	// Main float on hold
	public List<InProgressMasterBalanceHold> getAllMasterBalanceHold(int fromIndex, int toIndex);

	public List<InProgressMasterBalanceHoldHist> getAllMasterBalanceHoldHist(int fromIndex, int toIndex);

}

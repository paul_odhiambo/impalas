package com.impalapay.airtel.persistence.accountmgmt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;
import com.impalapay.airtel.persistence.GenericDAO;

/**
 * Persistence abstraction for {@link Account}
 * <p>
 * Copyright (c) ImpalaPay LTD., June 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class ManageAccountDAO extends GenericDAO implements ManagementAccountDAO {

	private static ManageAccountDAO accountDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return AccountDAO
	 */
	public static ManageAccountDAO getInstance() {
		if (accountDAO == null) {
			accountDAO = new ManageAccountDAO();
		}

		return accountDAO;
	}

	/**
	 *
	 */
	protected ManageAccountDAO() {
		super();
	}

	/**
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public ManageAccountDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);
	}

	/**
	 * @see com.impalapay.airtel.persistence.accountmgmt.ManagementAccountDAO#getAccount(java.lang.String)
	 */
	@Override
	public ManagementAccount getAccount(String uuid) {
		ManagementAccount a = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM managementaccount WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				a = beanProcessor.toBean(rset, ManagementAccount.class);
				// a.setId(rset.getInt("accountId"));
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting managementaccount with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return a;
	}

	/**
	 * @see com.impalapay.airtel.persistence.accountmgmt.ManagementAccountDAO#getAccount(String)
	 */
	@Override
	public ManagementAccount getAccountName(String username) {
		ManagementAccount a = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM managementaccount WHERE username = ?;");
			pstmt.setString(1, username);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				a = beanProcessor.toBean(rset, ManagementAccount.class);
				// a.setId(rset.getInt("accountId"));
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting managementaccount with unique name '" + username + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return a;
	}

	/**
	 * @see com.impalapay.airtel.persistence.accountmgmt.ManagementAccountDAO#getAllAccounts()
	 */
	@Override
	public List<ManagementAccount> getAllAccounts() {
		List<ManagementAccount> list = new ArrayList<>();
		ManagementAccount a = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM managementaccount;");
			rset = pstmt.executeQuery();

			while (rset.next()) {
				a = beanProcessor.toBean(rset, ManagementAccount.class);
				// a.setId(rset.getInt("accountId"));

				list.add(a);
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all managementaccount.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	/**
	 * @see com.impalapay.airtel.persistence.accountmgmt.ManagementAccountDAO#addAccount(com.impalapay.hub.beans.accountmgmt.EscrowAccount)
	 */
	@Override
	public boolean addAccount(ManagementAccount account) {
		boolean success = true;
		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn
					.prepareStatement("INSERT INTO managementaccount (uuid, accountStatusUuid, accountName, username,"
							+ "loginPasswd, updateforex, updatebalance, updatereversal,checker) VALUES(?,?,?,?,?,?,?,?,?);");

			pstmt.setString(1, account.getUuid());
			pstmt.setString(2, account.getAccountStatusUuid());
			pstmt.setString(3, account.getAccountName());
			pstmt.setString(4, account.getUsername());
			pstmt.setString(5, account.getLoginPasswd());
			pstmt.setBoolean(6, account.isUpdateforex());
			pstmt.setBoolean(7, account.isUpdatebalance());
			pstmt.setBoolean(8, account.isUpdatereversal());
			pstmt.setBoolean(9, account.isChecker());

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException when trying to put: " + account);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			} /**
				 * if (pstmt2 != null) { try { pstmt2.close(); } catch (SQLException e) { } }
				 **/

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	/**
	 * @see ManagementAccountDAO#updateAccount(java.lang.String,
	 *      com.impalapay.hub.beans.accountmgmt.EscrowAccount)
	 */
	@Override
	public boolean updateAccount(String uuid, ManagementAccount a) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM managementaccount WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				pstmt2 = conn.prepareStatement("UPDATE managementaccount SET accountstatusuuid=?, "
						+ "accountname=?, username=?, loginPasswd=?, updateforex=?, updatebalance=?, updatereversal=?, "
						+ "checker=? WHERE uuid=?;");

				pstmt2.setString(1, a.getAccountStatusUuid());
				pstmt2.setString(2, a.getAccountName());
				pstmt2.setString(3, a.getUsername());
				pstmt2.setString(4, a.getLoginPasswd());
				pstmt2.setBoolean(5, a.isUpdateforex());
				pstmt2.setBoolean(6, a.isUpdatebalance());
				pstmt2.setBoolean(7, a.isUpdatereversal());
				pstmt2.setBoolean(8, a.isChecker());
				pstmt2.setString(9, a.getUuid());

				pstmt2.executeUpdate();

			} else {
				success = addAccount(a);
			}

		} catch (SQLException e) {
			logger.error(
					"SQLException when trying to update managementaccount with uuid '" + uuid + "' with " + a + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<ManagementAccount> getAllAccounts(int fromIndex, int toIndex) {
		List<ManagementAccount> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn
					.prepareStatement("SELECT * FROM managementaccount ORDER BY creationdate DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, ManagementAccount.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all managementaccount from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

}

/*
 ** Local Variables: mode: java c-basic-offset: 2 tab-width: 2 indent-tabs-mode:
 * nil End:
 **
 ** ex: set softtabstop=2 tabstop=2 expandtab:
 **
 */
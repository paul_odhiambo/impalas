package com.impalapay.airtel.persistence.billpayment;

import java.util.List;

import com.impalapay.airtel.beans.billpayment.BillPaymentCodes;

public interface AirtelBillpaymentCodes {

	/**
	 * Gets all {@link AirtelBillpaymentCodes}s which have an UUID matching the
	 * argument. The list returned is not arranged in any particular order.
	 * 
	 * @param uuid
	 * @return getBankCodes request(s)
	 */
	public List<BillPaymentCodes> getBillPaymentCodes(String uuid);

	/**
	 * Gets all transaction requests
	 *
	 * @return List<{@link BankCodess}> all transaction requests
	 */
	public List<BillPaymentCodes> getAllBillPaymentCodes();

	/**
	 * Gets all transactions requests between the specified fromIndex, inclusive,
	 * and toIndex, exclusive.
	 *
	 * @param fromIndex
	 * @param toIndex
	 * 
	 * @return List<{@link AirtelBillpaymentCodes}> all TransactionReferences
	 *         requests
	 */
	public List<BillPaymentCodes> getAllBillPaymentCodes(int fromIndex, int toIndex);

	public BillPaymentCodes getBillPaymentCodes(BillPaymentCodes billpayment);

	/**
	 *
	 * @param AirtelBillpaymentCodes
	 * @return BankCodes
	 */
	public boolean addBillPaymentCodes(BillPaymentCodes billpayment);

	/**
	 * 
	 * @param uuid
	 * @param bankcodes
	 * @return
	 */
	public boolean updateBillPaymentCodes(String uuid, BillPaymentCodes billpayment);

}

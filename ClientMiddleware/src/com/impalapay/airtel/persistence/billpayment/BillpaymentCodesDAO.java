package com.impalapay.airtel.persistence.billpayment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.billpayment.BillPaymentCodes;
import com.impalapay.airtel.persistence.GenericDAO;

public class BillpaymentCodesDAO extends GenericDAO implements AirtelBillpaymentCodes {
	public static BillpaymentCodesDAO bankDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return {@link ThirdReferenceDAO}
	 */
	public static BillpaymentCodesDAO getInstance() {

		if (bankDAO == null) {
			bankDAO = new BillpaymentCodesDAO();
		}

		return bankDAO;
	}

	/**
	 * 
	 */
	public BillpaymentCodesDAO() {
		super();

	}

	/**
	 * 
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public BillpaymentCodesDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);
	}

	@Override
	public List<BillPaymentCodes> getBillPaymentCodes(String uuid) {

		List<BillPaymentCodes> list = new LinkedList<>();

		BillPaymentCodes billpayment = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM merchantbillcode WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				billpayment = beanProcessor.toBean(rset, BillPaymentCodes.class);

				list.add(billpayment);
			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching merchantbillcode with uuid" + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<BillPaymentCodes> getAllBillPaymentCodes() {
		List<BillPaymentCodes> list = new LinkedList<>();

		BillPaymentCodes billpayment = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM merchantbillcode;");

			rset = pstmt.executeQuery();

			while (rset.next()) {
				billpayment = beanProcessor.toBean(rset, BillPaymentCodes.class);
				list.add(billpayment);

			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all merchantbillcode");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<BillPaymentCodes> getAllBillPaymentCodes(int fromIndex, int toIndex) {
		List<BillPaymentCodes> list = new ArrayList<>();
		BillPaymentCodes s;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM merchantbillcode ORDER BY countryuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, BillPaymentCodes.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all merchantbillcode from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public boolean addBillPaymentCodes(BillPaymentCodes billpayment) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO merchantbillcode (uuid, merchantname, countryuuid, networkuuid, merchantcode, merchantid, msisdn, dateadded) VALUES (?, ?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, billpayment.getUuid());
			pstmt.setString(2, billpayment.getMerchantname());
			pstmt.setString(3, billpayment.getCountryuuid());
			pstmt.setString(4, billpayment.getNetworkuuid());
			pstmt.setString(5, billpayment.getMerchantcode());
			pstmt.setString(6, billpayment.getMerchantid());
			pstmt.setString(7, billpayment.getMsisdn());
			pstmt.setTimestamp(8, new Timestamp(billpayment.getDateadded().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + billpayment);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public BillPaymentCodes getBillPaymentCodes(BillPaymentCodes billpayment) {
		BillPaymentCodes c = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM merchantbillcode WHERE merchantcode=?;");
			pstmt.setString(1, billpayment.getMerchantcode());
			// pstmt.setString(2, bankcodes.getUuid());

			rset = pstmt.executeQuery();

			if (rset.next()) {
				c = beanProcessor.toBean(rset, BillPaymentCodes.class);
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting a merchantbillcode '" + billpayment + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return c;
	}

	@Override
	public boolean updateBillPaymentCodes(String uuid, BillPaymentCodes billpayment) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM merchantbillcode WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				pstmt2 = conn.prepareStatement("UPDATE merchantbillcode SET merchantname=?, "
						+ "countryuuid=?,networkuuid=?,merchantcode=?,merchantid=?,msisdn=? " + "WHERE uuid=?;");

				pstmt2.setString(1, billpayment.getMerchantname());
				pstmt2.setString(2, billpayment.getCountryuuid());
				pstmt2.setString(3, billpayment.getNetworkuuid());
				pstmt2.setString(4, billpayment.getMerchantcode());
				pstmt2.setString(5, billpayment.getMerchantid());
				pstmt2.setString(6, billpayment.getMsisdn());
				pstmt2.setString(7, uuid);

				pstmt2.executeUpdate();

			} else {
				success = addBillPaymentCodes(billpayment);
			}

		} catch (SQLException e) {
			logger.error("SQLException when trying to update merchantbillcode with uuid '" + uuid + "' with "
					+ billpayment + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

}

package com.impalapay.airtel.persistence.forex;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

import org.apache.poi.ss.formula.functions.Now;
import org.junit.Ignore;
import org.junit.Test;

import com.impalapay.airtel.beans.forex.Forex;
import com.impalapay.airtel.beans.geolocation.Country;

public class TestUsdForexDAO {

	final String DB_NAME = "airteldblive";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "airtellive";
	final String DB_PASSWD = "ThejKoyb34";
	final int DB_PORT = 5432;

	final String FOREX_UUID = "691160b2eb346a88d0100249dba8ae6r232242";
	final String COUNTRY_UUID = "0c115d1bc7804592b2fdae320476b960";

	final String FOREX_UUID2 = "691160b2eb346a88d010";
	final String COUNTRY_UUID2 = "11316c574b734c1f89165f886bd37fe";
	final double BASERATE = 90;
	final double IMPALARATE = 52;
	final int USDRATE_COUNT = 1;
	final Date USD_DATE_NEW = new Date(new Long("1367597206000").longValue()); // Fri
																				// May
																				// 03
																				// 19:06:46
																				// EAT
																				// 2013

	final double UPDATE_BASERATE = 22;

	final double UPDATE_IMPALARATE = 24;

	private UsdForexDAO storage;

	/**
	 * Test method for
	 * com.impalapay.airtel.persistence.forex.UsdForexDAO#getUsdForex
	 * (java.lang.String).
	 */
	@Ignore
	@Test
	public void testUsdForexString() {
		storage = new UsdForexDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		Forex forex = storage.getUsdForex(FOREX_UUID);
		// assertEquals(forex.getBaserate(), BASERATE);

		assertEquals(forex.getBaserate(), BASERATE, 0.01);
		;

	}

	@Test
	@Ignore
	public void testgetCountryUsdForex() {
		storage = new UsdForexDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		Country countrytest = new Country();

		countrytest.setUuid("d4a676822f4546a0bee789e83070f788");

		Forex forex = storage.getCountryUsdForex(countrytest);

		System.out.println(forex.toString());
	}

	/**
	 * Test method for
	 * com.impalapay.airtel.persistence.forex.UsdForexDAO#getAllUsdForex()
	 */
	@Test
	@Ignore
	public void testGetAllClientIp() {
		storage = new UsdForexDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		List<Forex> list = storage.getAllUsdForex();
		assertEquals(list.size(), USDRATE_COUNT);

	}

	@Test
	@Ignore
	public void testPutUsdForex() {
		storage = new UsdForexDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		Forex forex = new Forex();

		forex.setUuid("fyeywtey565436");
		forex.setCountryUuid("977f6e8fceed43e0a3c1716750171442");
		forex.setBaserate(123);
		forex.setImpalarate(140);

		assertTrue(storage.PutUsdForex(forex));

	}

	@Test
	@Ignore
	public void testUpdateUSDForex() {
		storage = new UsdForexDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		Forex forex = new Forex();

		forex.setUuid(FOREX_UUID2);
		forex.setCountryUuid(COUNTRY_UUID2);
		forex.setBaserate(UPDATE_BASERATE);
		forex.setImpalarate(UPDATE_IMPALARATE);

		assertTrue(storage.UpdateUsdForex(FOREX_UUID2, forex));

	}

	@Test
	// @Ignore
	public void testUpdateUSDForex2() {
		storage = new UsdForexDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		Forex forex = new Forex();

		forex.setUuid(FOREX_UUID);
		forex.setCountryUuid(COUNTRY_UUID);
		forex.setBaserate(UPDATE_BASERATE);
		forex.setImpalarate(UPDATE_IMPALARATE);
		// forex.setUploadDate(new Date());

		assertTrue(storage.UpdateUsdForex2(COUNTRY_UUID, forex));

	}

}

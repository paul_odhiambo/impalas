package com.impalapay.airtel.persistence.forex;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

import org.apache.poi.ss.formula.functions.Now;
import org.junit.Ignore;
import org.junit.Test;

import com.impalapay.airtel.beans.forex.ForexEngine;
import com.impalapay.airtel.beans.geolocation.Country;

public class TestForexEngineDAO {

	final String DB_NAME = "remittancedb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "remittance";
	final String DB_PASSWD = "gertedNen2";
	final int DB_PORT = 5432;

	final String FOREX_UUID = "691160b2eb346a88d0100249dba8ae6r232242";
	final String COUNTRY_UUID = "0c115d1bc7804592b2fdae320476b960";

	final String FOREX_UUID2 = "fyeywtey565436";
	final String COUNTRY_UUID2 = "11316c574b734c1f89165f886bd37fe";
	final double BASERATE = 90;
	final double IMPALARATE = 52;
	final int USDRATE_COUNT = 1;
	final Date USD_DATE_NEW = new Date(new Long("1367597206000").longValue()); // Fri
																				// May
																				// 03
																				// 19:06:46
																				// EAT
																				// 2013

	final double UPDATE_BASERATE = 22;

	final double UPDATE_IMPALARATE = 24;

	private ForexEngineDAO storage;

	/**
	 * Test method for
	 * com.impalapay.airtel.persistence.forex.UsdForexDAO#getUsdForex
	 * (java.lang.String).
	 */
	@Ignore
	@Test
	public void testUsdForexString() {
		storage = new ForexEngineDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		ForexEngine forex = storage.getForex(FOREX_UUID);
		// assertEquals(forex.getBaserate(), BASERATE);

		assertEquals(forex.getMarketrate(), BASERATE, 0.01);
		;

	}

	@Test
	// @Ignore
	public void testgetCountryUsdForex() {
		storage = new ForexEngineDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		// Country countrytest = new Country();

		// countrytest.setUuid("d4a676822f4546a0bee789e83070f788");

		ForexEngine forex = storage.getCurrencyPairForex("USD/KES");

		System.out.println(forex.toString());
	}

	/**
	 * Test method for
	 * com.impalapay.airtel.persistence.forex.UsdForexDAO#getAllUsdForex()
	 */
	@Test
	@Ignore
	public void testGetAllClientIp() {
		storage = new ForexEngineDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		List<ForexEngine> list = storage.getAllForex();
		assertEquals(list.size(), USDRATE_COUNT);

	}

	@Test
	// @Ignore
	public void testPutForex() {
		storage = new ForexEngineDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		ForexEngine forex = new ForexEngine();

		forex.setUuid("fyeywtey565436899");
		forex.setCurrencypair("USD/GBP");
		forex.setMarketrate(123);
		forex.setSpreadrate(140);

		assertTrue(storage.PutForex(forex));

	}

	@Test
	@Ignore
	public void testUpdateUSDForex() {
		storage = new ForexEngineDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		ForexEngine forex = new ForexEngine();

		forex.setUuid(FOREX_UUID2);
		forex.setCurrencypair("KES/USD");
		forex.setMarketrate(UPDATE_BASERATE);
		forex.setSpreadrate(UPDATE_IMPALARATE);

		assertTrue(storage.UpdateForex(FOREX_UUID2, forex));

	}

}

package com.impalapay.airtel.persistence.transaction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.transaction.TransactiontransferType;
import com.impalapay.airtel.persistence.GenericDAO;

public class TransactionTypeDAO extends GenericDAO implements AirtelTransactionType {
	public static TransactionTypeDAO bankDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return {@link ThirdReferenceDAO}
	 */
	public static TransactionTypeDAO getInstance() {

		if (bankDAO == null) {
			bankDAO = new TransactionTypeDAO();
		}

		return bankDAO;
	}

	/**
	 * 
	 */
	public TransactionTypeDAO() {
		super();

	}

	/**
	 * 
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public TransactionTypeDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);
	}

	@Override
	public List<TransactiontransferType> getTransactionType(String uuid) {

		List<TransactiontransferType> list = new LinkedList<>();

		TransactiontransferType banktransaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM transactiontransfertype WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				banktransaction = beanProcessor.toBean(rset, TransactiontransferType.class);

				list.add(banktransaction);
			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching transactiontransfertype with uuid" + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<TransactiontransferType> getAllBankTransactionType() {
		List<TransactiontransferType> list = new LinkedList<>();

		TransactiontransferType banktransaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM transactiontransfertype;");

			rset = pstmt.executeQuery();

			while (rset.next()) {
				banktransaction = beanProcessor.toBean(rset, TransactiontransferType.class);
				list.add(banktransaction);

			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transactiontransfertype");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<TransactiontransferType> getAllTransactionType(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean addTransactionType(TransactiontransferType banktransaction) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO transactiontransfertype (uuid, transactionuuid, transactiontypeuuid, sourcecountrycode, senderfirstname, senderlastname, senderaddress, sendercity, recipientfirstname, recipientlastname, recipientmobile, recipientaccount, servertime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, banktransaction.getUuid());
			pstmt.setString(2, banktransaction.getTransactionUuid());
			pstmt.setString(3, banktransaction.getTransactiontypeUuid());
			pstmt.setString(4, banktransaction.getSourcecountrycode());
			pstmt.setString(5, banktransaction.getSenderfirstname());
			pstmt.setString(6, banktransaction.getSenderlastname());
			pstmt.setString(7, banktransaction.getSenderaddress());
			pstmt.setString(8, banktransaction.getSendercity());
			pstmt.setString(9, banktransaction.getRecipientfirstname());
			pstmt.setString(10, banktransaction.getRecipientlastname());
			pstmt.setString(11, banktransaction.getRecipientmobile());
			pstmt.setString(12, banktransaction.getRecipientaccount());
			pstmt.setTimestamp(13, new Timestamp(banktransaction.getServertime().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + banktransaction);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

}

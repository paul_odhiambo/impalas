package com.impalapay.airtel.persistence.transaction.forex;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.persistence.GenericDAO;

public class TransactionForexDAO extends GenericDAO implements AirtelTransactionForex {

	public static TransactionForexDAO transactionforexDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	public static TransactionForexDAO getinstance() {
		if (transactionforexDAO == null) {
			transactionforexDAO = new TransactionForexDAO();
		}

		return transactionforexDAO;

	}

	public TransactionForexDAO() {
		super();
	}

	/**
	 * 
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public TransactionForexDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);
	}

	@Override
	public List<TransactionForexrate> getTransactionForexs(String uuid) {

		List<TransactionForexrate> list = new LinkedList<>();

		TransactionForexrate transactionforexrate = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM transactionforex WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transactionforexrate = beanProcessor.toBean(rset, TransactionForexrate.class);

				list.add(transactionforexrate);
			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching transactionforexrate with uuid" + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<TransactionForexrate> getTransactionForexs(Account account) {
		// TODO Auto-generated method stub

		List<TransactionForexrate> list = new LinkedList<>();

		TransactionForexrate transactionforexrate = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM transactionforex WHERE account=?;");
			pstmt.setString(1, account.getUuid());
			rset = pstmt.executeQuery();

			while (rset.next()) {
				transactionforexrate = beanProcessor.toBean(rset, TransactionForexrate.class);
				// transaction.setId(rset.getInt("id"));
				list.add(transactionforexrate);

			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting transactionforex of" + account);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<TransactionForexrate> getTransactionForexsCountry(Country country) {

		List<TransactionForexrate> list = new LinkedList<>();

		TransactionForexrate transactionforexrate = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM transactionforex WHERE recipientcountry=?;");
			pstmt.setString(1, country.getUuid());

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transactionforexrate = beanProcessor.toBean(rset, TransactionForexrate.class);
				// transaction.setId(rset.getInt("id"));
				list.add(transactionforexrate);

			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting transactionforex of" + country);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<TransactionForexrate> getAllTransactionForexs() {
		// TODO Auto-generated method stub
		List<TransactionForexrate> list = new LinkedList<>();

		TransactionForexrate transactionforex = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM transactionforex;");

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transactionforex = beanProcessor.toBean(rset, TransactionForexrate.class);
				// transaction.setId(rset.getInt("id"));
				list.add(transactionforex);

			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transactions");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<TransactionForexrate> getAllTransactionForexs(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		List<TransactionForexrate> list = new LinkedList<>();

		// Transaction transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn
					.prepareStatement("SELECT * FROM transactionforex ORDER BY  serverTime DESC LIMIT ? OFFSET ? ;");

			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				list = beanProcessor.toBeanList(rset, TransactionForexrate.class);
				// transaction = b.toBean(rset, Transaction.class);
				// transaction.setId(rset.getInt("sessionid"));

				// list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting transactionforex with uuid  from " + fromIndex + " to "
					+ toIndex);
			logger.error(e.toString());

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public boolean addTransactionForex(TransactionForexrate transactionforexrate) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO transactionforex (uuid,transactionUuid,recipientcountry, account , localamount, accounttype, "
							+ "convertedamount, impalarate, baserate, receivermsisdn, surplus, serverTime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, transactionforexrate.getUuid());
			pstmt.setString(2, transactionforexrate.getTransactionUuid());
			pstmt.setString(3, transactionforexrate.getRecipientcountry());
			pstmt.setString(4, transactionforexrate.getAccount());
			pstmt.setDouble(5, transactionforexrate.getLocalamount());
			pstmt.setString(6, transactionforexrate.getAccounttype());
			pstmt.setDouble(7, transactionforexrate.getConvertedamount());
			pstmt.setDouble(8, transactionforexrate.getImpalarate());
			pstmt.setDouble(9, transactionforexrate.getBaserate());
			pstmt.setString(10, transactionforexrate.getReceivermsisdn());
			pstmt.setDouble(11, transactionforexrate.getSurplus());
			pstmt.setTimestamp(12, new Timestamp(transactionforexrate.getServerTime().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + transactionforexrate);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;

	}

	@Override
	public List<TransactionForexrate> getTransactionForexs(Account account, int fromIndex, int toIndex) {

		List<TransactionForexrate> list = new LinkedList<>();

		TransactionForexrate transactionforex = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM transactionforex WHERE account=? " + "ORDER BY servertime  DESC LIMIT ? OFFSET ?;");
			pstmt.setString(1, account.getUuid());
			pstmt.setInt(2, toIndex - fromIndex);
			pstmt.setInt(3, fromIndex);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transactionforex = beanProcessor.toBean(rset, TransactionForexrate.class);
				// transaction.setId(rset.getInt("id"));

				list.add(transactionforex);
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting transactionforex for account ' " + account + "' from "
					+ fromIndex + " to " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;

	}

	@Override
	public boolean deleteTransactionForex(String transactionid) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("DELETE FROM transactionforex WHERE transactionUuid=?;");

			pstmt.setString(1, transactionid);

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while deleting " + transactionid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public TransactionForexrate getTransactionForexsUuid(String transactionuuid) {
		TransactionForexrate transactionforexrate = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM transactionforex WHERE transactionUuid=?;");
			pstmt.setString(1, transactionuuid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transactionforexrate = beanProcessor.toBean(rset, TransactionForexrate.class);

			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching transactionforexrate with transactionuuid" + transactionuuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return transactionforexrate;
	}

	@Override
	public List<TransactionForexrate> getAllTransactionForexs(String uuid, int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		List<TransactionForexrate> list = new LinkedList<>();

		// Transaction transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM transactionforex WHERE Uuid=? ORDER BY  serverTime DESC LIMIT ? OFFSET ? ;");
			pstmt.setString(1, uuid);
			pstmt.setInt(2, toIndex - fromIndex);
			pstmt.setInt(3, fromIndex);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				list = beanProcessor.toBeanList(rset, TransactionForexrate.class);
				// transaction = b.toBean(rset, Transaction.class);
				// transaction.setId(rset.getInt("sessionid"));

				// list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting transactionforex with uuid  from " + fromIndex + " to "
					+ toIndex);
			logger.error(e.toString());

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

}

package com.impalapay.airtel.persistence.transaction.forex;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;

public interface AirtelTransactionForex {
	/**
	 * Gets all {@link TransactionForexrate}s which have an UUID matching the
	 * argument. The list returned is not arranged in any particular order.
	 * 
	 * @param uuid
	 * @return transaction request(s)
	 */
	public List<TransactionForexrate> getTransactionForexs(String uuid);

	/**
	 * Gets all transaction request(s) that belong to a particular account holder.
	 *
	 * @param account
	 * @return List<{@link TransactionForexrate}> transaction request(s)
	 */
	public List<TransactionForexrate> getTransactionForexs(Account account);

	/**
	 * Gets all transaction request(s) that belong to a particular account username.
	 *
	 *
	 * @param account
	 * @return List<{@link Transaction}> transaction request(s)
	 * 
	 *         public List<Transaction> getAllTransactions(String username);
	 */
	/**
	 * Gets all transactions request(s) that correspond to a particular country
	 *
	 * @param country
	 * @return country transaction request(s)
	 */
	public List<TransactionForexrate> getTransactionForexsCountry(Country country);

	/**
	 * Gets all transaction requests
	 *
	 * @return List<{@link TransactionForexrate}> all transaction requests
	 */
	public List<TransactionForexrate> getAllTransactionForexs();

	/**
	 * Gets all transactionForexs requests between the specified fromIndex,
	 * inclusive, and toIndex, exclusive.
	 *
	 * @param fromIndex
	 * @param toIndex
	 * 
	 * @return List<{@link TransactionForexrate}> all transaction requests
	 */
	public List<TransactionForexrate> getAllTransactionForexs(int fromIndex, int toIndex);

	/**
	 *
	 * @param transaction
	 * @return transaction
	 */
	public boolean addTransactionForex(TransactionForexrate transactionforexrate);

	/**
	 * Returns a view of the portion of an Account's Transaction activity between
	 * the specified fromIndex, inclusive, and toIndex, exclusive.
	 *
	 * @param account
	 * @param fromIndex
	 * @param toIndex
	 * @return List<{@link TransactionForexrate}> all transaction requests
	 */
	public List<TransactionForexrate> getTransactionForexs(Account account, int fromIndex, int toIndex);

	/**
	 * 
	 * @param uuid
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<TransactionForexrate> getAllTransactionForexs(String uuid, int fromIndex, int toIndex);

	/**
	 * Returns a view of the portion of an Account's username Transaction activity
	 * between the specified fromIndex, inclusive, and toIndex, exclusive.
	 *
	 * @param account
	 * @param fromIndex
	 * @param toIndex
	 * @return List<{@link Transaction}> all transaction requests
	 * 
	 *         public List<Transaction> getTransactions(String username, int
	 *         fromIndex, int toIndex);
	 **/
	public boolean deleteTransactionForex(String transactionid);

	/**
	 * 
	 * @param transactionuuid
	 * @return
	 */
	public TransactionForexrate getTransactionForexsUuid(String transactionuuid);

}

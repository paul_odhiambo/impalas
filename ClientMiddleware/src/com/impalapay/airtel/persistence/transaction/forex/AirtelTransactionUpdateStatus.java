package com.impalapay.airtel.persistence.transaction.forex;

import java.util.List;

import com.impalapay.airtel.beans.transaction.forexrate.TransactionUpdateStatus;

public interface AirtelTransactionUpdateStatus {

	/**
	 * Gets all transaction requests
	 *
	 * @return List<{@link TransactionForexrate}> all transaction requests
	 */
	public List<TransactionUpdateStatus> getAllTransactionUpdateStatus();

	/**
	 * Gets all transactionForexs requests between the specified fromIndex,
	 * inclusive, and toIndex, exclusive.
	 *
	 * @param fromIndex
	 * @param toIndex
	 * 
	 * @return List<{@link TransactionForexrate}> all transaction requests
	 */
	public List<TransactionUpdateStatus> getAllTransactionUpdateStatus(int fromIndex, int toIndex);

	/**
	 *
	 * @param transaction
	 * @return transaction
	 */
	public boolean addTransactionUpdateStatus(TransactionUpdateStatus transactionupdatetable);

	/**
	 * 
	 * @param transactionupdatetable
	 * @return
	 */
	public boolean addTransactionUpdateStatusHistory(TransactionUpdateStatus transactionupdatetable);

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<TransactionUpdateStatus> getAllTransactionUpdateStatusHistory(int fromIndex, int toIndex);

	/**
	 * Returns a view of the portion of an Account's username Transaction activity
	 * between the specified fromIndex, inclusive, and toIndex, exclusive.
	 *
	 * @param account
	 * @param fromIndex
	 * @param toIndex
	 * @return List<{@link Transaction}> all transaction requests
	 * 
	 *         public List<Transaction> getTransactions(String username, int
	 *         fromIndex, int toIndex);
	 **/
	public boolean deleteTransactionUpdateStatus(String transactionid);

	/**
	 * 
	 * @param transactionuuid
	 * @return
	 */
	public TransactionUpdateStatus getTransactionUpdateStatusUuid(String transactionuuid);

	/**
	 * 
	 * @param transactionuuid
	 * @return
	 */
	public TransactionUpdateStatus getTransactionUpdateStatusUuidHistory(String transactionuuid);

}

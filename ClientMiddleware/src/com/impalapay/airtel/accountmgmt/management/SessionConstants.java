package com.impalapay.airtel.accountmgmt.management;

/**
 * Constants which are used in session management of the Administrator account.
 * <p>
 * Copyright (c) ImpalaPay Ltd., June 23, 2014
 * 
 * @author <a href="mailto:michael@impalapay.com">Michael Wakahe</a>
 * @version %I%, %G%
 * 
 */
public class SessionConstants {

	public static int SESSION_TIMEOUT = 500; // Number of seconds for which a session is active.
	final public static String ESCROW_ACCOUNT_SIGN_IN_KEY = "Escrow Account Signin Key";

	// The value associated with the following key is the Unix time in seconds when
	// the user logged in.
	final public static String ESCROW_ACCOUNT_SIGN_IN_TIME = "Escrow Account Signin Time";

	final public static String ESCROW_ACCOUNT_SIGN_IN_ERROR_KEY = "Error Login";
	final public static String ESCROW_ACCOUNT_SIGN_IN_NO_EMAIL = "Sorry, there is no user with that username. Please try again.";
	final public static String ESCROW_ACCOUNT_SIGN_IN_WRONG_PASSWORD = "Sorry, the username and password do not match. Please try again.";
	final public static String ESCROW_ACCOUNT_CHANGE_PASSWORD_KEY = "Escrow Account change password key";
	final public static String ERROR_INVALID_EMAIL = "Invalid username,try again.";

	// messages associated with passwords;
	final public static String INCORRECT_PASSWORD = "The password you gave is incorrect.";
	final public static String CORRECT_PASSWORD = "Password Changed";
	final public static String MISMATCHED_PASSWORD = "The passwords supplied do not match";

	final public static String ESCROW_ACCOUNT_EDIT_CODE_ID = "Escrow Account Edit Code Id";

	// account associated with deactivated account.
	final public static String ESCROW_ACCOUNT_DEACTIVATED = "Your account has been dectivated.Please contact the system Administrator";

}

/*
 ** Local Variables: mode: java c-basic-offset: 2 tab-width: 2 indent-tabs-mode:
 * nil End:
 **
 ** ex: set softtabstop=2 tabstop=2 expandtab:
 **
 */
/**
 * Copyright 2015 Tawi Commercial Services Ltd
 * 
 * Licensed under the Open Software License, Version 3.0  you may
 * not use this file except in compliance with the License. You may obtain a copy
 * of the License at:
 * http://opensource.org/licenses/OSL-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an AS IS BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 * 
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package com.impalapay.airtel.accountmgmt.pagination.mainfloathistory;

import java.util.List;

import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.persistence.accountmgmt.AccountDAO;
import com.impalapay.airtel.persistence.util.CountUtils;
import com.impalapay.mno.beans.accountmgmt.balance.MasterAccountFloatPurchase;
import com.impalapay.mno.persistence.accountmgmt.balance.AccountPurchaseDAO;

/**
 * Paginate an Inbox HTML view.
 * <p>
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 */
public class MainfloatPaginator {

	/**
	 *
	 */
	// public static final int PAGESIZE = 15; // The number of Incoming SMS to
	// display per page
	public static final int PAGESIZE = 13;
	private Account account;
	private AccountPurchaseDAO accountpurchaseDAO;
	private AccountDAO accountDAO;
	private CountUtils countUtils;
	private Logger logger = Logger.getLogger(this.getClass());
	private String username;

	public MainfloatPaginator() {

	}

	/**
	 *
	 * @param accountuuid
	 */
	public MainfloatPaginator(String username) {

		countUtils = CountUtils.getInstance();

		accountDAO = AccountDAO.getInstance();

		accountpurchaseDAO = AccountPurchaseDAO.getInstance();

		account = accountDAO.getAccountName(username);

		this.username = username;
	}

	/**
	 *
	 * @param username
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public MainfloatPaginator(String username, String dbName, String dbHost, String dbUsername, String dbPasswd,
			int dbPort) {

		// initialize the DAOs
		this.username = username;

		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);

		accountpurchaseDAO = new AccountPurchaseDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);

		accountDAO = new AccountDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);

		account = accountDAO.getAccountName(username);
	}

	/**
	 *
	 * @return
	 */
	public MainfloatPage getFirstPage() {

		MainfloatPage page = new MainfloatPage();

		List<MasterAccountFloatPurchase> accountList = accountpurchaseDAO.getAllMasterFloatHistory(account, 0,
				PAGESIZE);

		page = new MainfloatPage(1, getTotalPage(), PAGESIZE, accountList);
		// result = new IncomingSMSPage (1, getTotalPage(), PAGESIZE, smsList);

		return page;
	}

	/**
	 * Provides the last page of the accounts report
	 *
	 * @return accounts page
	 */
	public MainfloatPage getLastPage() {
		MainfloatPage page = new MainfloatPage();

		List<MasterAccountFloatPurchase> accountList = null;
		int accountCount, startIndex;
		int totalPage = getTotalPage();

		startIndex = (totalPage - 1) * PAGESIZE;
		accountCount = countUtils.getAllMasterFloatHistory(account);

		accountList = accountpurchaseDAO.getAllMasterFloatHistory(account, startIndex, accountCount);

		page = new MainfloatPage(totalPage, totalPage, PAGESIZE, accountList);

		return page;
	}

	/**
	 * Moves you forward to the page of the Accounts that comes after the current
	 * page
	 *
	 * @param currentPage
	 * @return an Account page
	 */
	public MainfloatPage getNextPage(final MainfloatPage currentPage) {
		int totalPage = getTotalPage();

		MainfloatPage page = new MainfloatPage();

		List<MasterAccountFloatPurchase> accountList = accountpurchaseDAO.getAllMasterFloatHistory(account,
				currentPage.getPageNum() * PAGESIZE, ((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		page = new MainfloatPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, accountList);

		return page;
	}

	/**
	 * Moves you backward to the page of the Accounts that comes before the current
	 * page
	 *
	 * @param currentPage
	 * @return an Account page
	 */
	public MainfloatPage getPrevPage(final MainfloatPage currentPage) {
		int totalPage = getTotalPage();

		MainfloatPage page = new MainfloatPage();

		List<MasterAccountFloatPurchase> accountList = accountpurchaseDAO.getAllMasterFloatHistory(account,
				(currentPage.getPageNum() - 2) * PAGESIZE, ((currentPage.getPageNum() - 1) * PAGESIZE));

		page = new MainfloatPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, accountList);

		return page;
	}

	/**
	 * Calculates the total number of pages that would be printed for the Account
	 * sessions that belong to the logged-in account
	 *
	 * @return an integer
	 */
	public int getTotalPage() {
		int totalSize = 0;

		// get the number of all sessions belonging to this email
		totalSize = countUtils.getAllMasterFloatHistory(account);

		// TODO: divide by the page size and add one to take care of remainders
		// and what else?
		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

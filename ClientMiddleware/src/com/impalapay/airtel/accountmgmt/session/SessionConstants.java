package com.impalapay.airtel.accountmgmt.session;

/**
 * Constants which are used in session management of the airtel api.
 * <p>
 * Copyright (c) ImpalaPay LTD., June 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * @version %I%, %G%
 */
public class SessionConstants {

	public static int SESSION_TIMEOUT = 500; // Number of seconds for which a
												// session is active.
	final public static String ACCOUNT_SIGN_IN_KEY = "Account Signin Key";

	// The value associated with the following key is the Unix time in seconds
	// when the user logged in.
	final public static String ACCOUNT_SIGN_IN_TIME = "Account Signin Time";

	final public static String ACCOUNT_SIGN_IN_ERROR_KEY = "Error Login";
	final public static String ACCOUNT_SIGN_IN_NO_EMAIL = "Sorry, there is no user with that username. Please try again.";
	final public static String ACCOUNT_SIGN_IN_WRONG_PASSWORD = "Sorry, the username and password do not match. Please try again.";
	final public static String ACCOUNT_CHANGE_PASSWORD_KEY = "Account change password key";
	final public static String ERROR_INVALID_EMAIL = "Invalid username,try again.";

	// messages associated with passwords;
	final public static String INCORRECT_PASSWORD = "The password you gave is incorrect.";
	final public static String CORRECT_PASSWORD = "Password Changed";
	final public static String MISMATCHED_PASSWORD = "The passwords supplied do not match";

	final public static String ACCOUNT_EDIT_CODE_ID = "Account Edit Code Id";

	// account associated with deactivated account.
	final public static String ACCOUNT_DEACTIVATED = "Your account has been dectivated.Please contact the system Administrator";

	// constants used by email notifications/quartz job scheduler
	final public static String SESSION_EMAIL_KEY = "Session Email";
	final public static String JOB_EXISTS = "Sorry,email notification already set";
	final public static String JOB_NOT_EXISTS = "Sorry, email notification does not exist";
	// final public static String GROUP_NAME_BALANCE_THRESHOLD = "Balance
	// Threshold";
	// A key corresponding to the email address of the recipient
	final public static String RECIPIENT_KEY = "RECIPIENT_KEY";

	// A key relating quartz job scheduler error messages
	final public static String QUARTZ_ADD_JOB_KEY = "Error key for adding quartz jobs";
	final public static String QUARTZ_UPDATE_JOB_KEY = "Error key for updating balance notification jobs";

	final public static String MESSAGE_TO_SEND_KEY = "message to be sent";

	final public static String DELETE_SUCCESSFUL = "Email notification deleted successfully !";
	final public static String ADD_BALANCE_THRESHOLD = "Add Balance Threshold";
	final public static String VALIDATE_EMAIL_ADDRESS = "Validate email address";
	final public static String BALANCE_THRESHOLD_KEY = "balance threshold";
	final public static String QUARTZ_ADD_THRESHOLD_KEY = "Error key for adding balance threshold jobs";
	final public static String QUARTZ_UPDATE_THRESHOLD_KEY = "Error key for updating balance threshold jobs";
	final public static String CHECK_BALANCE_CRONEXPRESSION = "0 0/1 * * * ? *";

	// Constant for various types of transactions.
	final public static String MOBILE_TRANSFER = "691160b2eb346a88d0100249dba8ae6r";
	final public static String BILLPAY_TRANSFER = "7882490b9f894803b37a6a684302da28";
	final public static String BANK_TRANSFER = "4b3cdfc861994332a8e0c86e0710900a";
	final public static String CASH_PICKUP = "3ba7d51a363c44c1b650222afbcdbc6d";
	final public static String AIRTIME_TRANSFER = "cbd170b3935749e7b65b0f4e2f2869cb";

	// Constant for collection channels.
	final public static String MOBILE_DEBIT = "2c7fd25b-f011-40fe-8c50-11e247aae3f4";
	final public static String CREDITCARD_DEBIT = "4b3cdfc861994332a8e0c86e0710900a";
	final public static String PAYBILL_DEBIT = "3ba7d51a363c44c1b650222afbcdbc6d";

	// Cash Withdraw
	final public static String CLIENT_WITHDRAWALREQUEST_ERROR_KEY = "Withdrawal Request Error";
	final public static String CLIENT_WITHDRAWALREQUEST_SUCCESS_KEY = "Withdrawal Request Success";
	final public static String CLIENT_WITHDRAWALREQUEST_PARAMETERS = "Withdrawal Request Parameters";
	
	final public static String MOBILE_ENDPOINT ="mobile";
	final public static String AIRTIME_ENDPONT = "airtime";
	

}

/*
 ** Local Variables: mode: java c-basic-offset: 2 tab-width: 2 indent-tabs-mode:
 * nil End:
 **
 ** ex: set softtabstop=2 tabstop=2 expandtab:
 **
 */
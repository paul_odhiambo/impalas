package com.impalapay.airtel.accountmgmt.session;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.beans.network.Network;

/**
 * This object holds data that is displayed on the portal once a user has logged
 * in. It is meant to be cached while a session is still active to avoid
 * expensive computations with the RDBMS.
 * <p>
 *
 * Copyright (c) Impalapay Ltd., August 15, 2014
 *
 * @author <a href="mailto:eugene@impalapay">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class SessionStatistics implements Serializable {

	private int transactionCountTotal;

	private int AlltransactionCountTotal;

	private int transactionCountSuccess;

	private int transactionByUuidCount;

	private int transactionByMsisdnCount;

	private int forexhistoryCountTotal;

	private int transactionforexCountTotal;

	// addition
	private int topupCountTotal;

	private int alltopupCountTotal;

	private int AccountCountTotal;

	private int ManagementAccountCountTotal;

	private int mnoprefixCountTotal;

	private int networkcountTotal;

	private int routedefineCountTotal;

	private int clientipCountTotal;

	private int clientsessionUrlCountTotal;

	private int systemlogcountTotal;

	private int countryCountTotal;

	private int countrymsisdnCountTotal;

	private int bankCountTotal;

	private int merchantCountTotal;

	private int commissionCountTotal;

	// manage mainbalance
	private int mainfloatCountTotal;

	private int mainfloatbycountryCountTotal;

	private int mainfloathistoryCountTotal;

	private int mainfloatbycountryhistoryCountTotal;

	private int networkbalanceCountTotal;

	private int networkfloathistoryCountTotal;

	private int checkermainfloatCountTotal;

	private int checkerforexrateCountTotal;

	private int forexrateCountTotal;

	private int forexratehistoryCountTotal;

	private int clientmainbalancehistoryCountTotal;

	private int checkerbalancebycountryCountTotal;

	private int floatCountTotal;

	private int floatbycountryCountTotal;

	private int floatCountHistoryTotal;

	private int floatbycountryHistoryCountTotal;

	private int updatetransactionCountTotal;

	private int updatetransactionHistoryCountTotal;

	// Collection.
	private int collectionnetworkcountTotal;

	private int collectiondefineCountTotal;

	private int collectionsubaccountCountTotal;

	private int collectionbalanceCountTotal;

	private int collectionbalanceHistoryCountTotal;

	private int collectionprocessedtransCountTotal;
	
	private int updatecollectionCountTotal;

	private int updatecollectionHistoryCountTotal;


	// withdrawal
	private int clientwithdrawalCountTotal;

	private int checkerwithdrawalCountTotal;

	private int withdrawalhistoryCountTotal;

	private int withdrawalonHoldCountTotal;
	private int withdrawalonHoldHistCountTotal;
	
	//Refund Collection
	private int refundCountTotal;
	
	private int refundHistCountTotal;
	
	
	//Collection
	private int collectionsuccessCountTotal;
	
	private int collectionreverseCountTotal;
	
	 private int allcollectionCountTotal;


	// This is used to keep a count of all Transaction against a country(status
	// is "Accepted for delivery")
	private Map<Country, Integer> countryTransactionCountTotal;

	// This is used to keep a count of Transaction against a Country
	// for Transaction requests that have been successful(status is "Transaction
	// status success")
	private Map<Country, Integer> countryTransactionCountSuccess;

	// This is used to keep a value of Transaction amount against a Country
	// for Transaction requests that have been successful(status is "Transaction
	// status success")
	private Map<Country, Double> countryTransactionAmountSuccess;

	private Map<Country, Double> countryTransactionAmountDays;

	// These are used to keep a value of Transaction amount against Country on
	// particular days
	// They are used to generate bar charts
	private Map<String, Map<Country, Double>> countryTransactionAmountDay;

	private HashMap<Network, Integer> networkTransactionCountTotal;

	private HashMap<Account, Integer> accountTransactionCountTotal;

	/**
	 * Default constructor,intializes objects
	 *
	 */
	public SessionStatistics() {
		countryTransactionCountTotal = new HashMap<>();
		countryTransactionCountSuccess = new HashMap<>();
		countryTransactionAmountSuccess = new HashMap<>();
		countryTransactionAmountDay = new HashMap<>();
		countryTransactionAmountDays = new HashMap<>();
		networkTransactionCountTotal = new HashMap<>();
		accountTransactionCountTotal = new HashMap<>();
	}

	public int getTransactionforexCountTotal() {
		return transactionforexCountTotal;
	}

	public int getForexhistoryCountTotal() {
		return forexhistoryCountTotal;
	}

	public int getTransactionCountTotal() {

		return transactionCountTotal;

	}

	public int getTransactionCountSuccess() {

		return transactionCountSuccess;
	}

	public int getTransactionByUuidCount() {
		return transactionByUuidCount;
	}

	public int getTransactionByMsisdnCount() {
		return transactionByMsisdnCount;
	}

	public int getTopupCountTotal() {
		return topupCountTotal;
	}

	public int getAlltopupCountTotal() {
		return alltopupCountTotal;
	}

	public int getAccountCountTotal() {
		return AccountCountTotal;
	}

	public int getManagementAccountCountTotal() {
		return ManagementAccountCountTotal;
	}

	public int getMnoprefixCountTotal() {
		return mnoprefixCountTotal;
	}

	public int getNetworkcountTotal() {
		return networkcountTotal;
	}

	public int getRoutedefineCountTotal() {
		return routedefineCountTotal;
	}

	public int getMainfloatCountTotal() {
		return mainfloatCountTotal;
	}

	public int getMainfloatbycountryCountTotal() {
		return mainfloatbycountryCountTotal;
	}

	public int getMainfloathistoryCountTotal() {
		return mainfloathistoryCountTotal;
	}

	public int getMainfloatbycountryhistoryCountTotal() {
		return mainfloatbycountryhistoryCountTotal;
	}

	public int getCheckermainfloatCountTotal() {
		return checkermainfloatCountTotal;
	}

	public int getClientipCountTotal() {
		return clientipCountTotal;
	}

	public int getClientsessionUrlCountTotal() {
		return clientsessionUrlCountTotal;
	}

	public int getCountryCountTotal() {
		return countryCountTotal;
	}

	public int getCountrymsisdnCountTotal() {
		return countrymsisdnCountTotal;
	}

	public int getBankCountTotal() {
		return bankCountTotal;
	}

	public int getMerchantCountTotal() {
		return merchantCountTotal;
	}

	public int getSystemlogcountTotal() {
		return systemlogcountTotal;
	}

	public int getNetworkbalanceCountTotal() {
		return networkbalanceCountTotal;
	}

	public int getNetworkfloathistoryCountTotal() {
		return networkfloathistoryCountTotal;
	}

	public int getCheckerforexrateCountTotal() {
		return checkerforexrateCountTotal;
	}

	public int getForexratehistoryCountTotal() {
		return forexratehistoryCountTotal;
	}

	public int getForexrateCountTotal() {
		return forexrateCountTotal;
	}

	public int getClientmainbalancehistoryCountTotal() {
		return clientmainbalancehistoryCountTotal;
	}

	public int getCheckerbalancebycountryCountTotal() {
		return checkerbalancebycountryCountTotal;
	}

	public int getFloatCountTotal() {
		return floatCountTotal;
	}

	public int getFloatbycountryCountTotal() {
		return floatbycountryCountTotal;
	}

	public int getFloatCountHistoryTotal() {
		return floatCountHistoryTotal;
	}

	public int getFloatbycountryHistoryCountTotal() {
		return floatbycountryHistoryCountTotal;
	}

	public int getAlltransactionCountTotal() {
		return AlltransactionCountTotal;
	}

	public int getCommissionCountTotal() {
		return commissionCountTotal;
	}

	public int getUpdatetransactionCountTotal() {
		return updatetransactionCountTotal;
	}

	public int getUpdatetransactionHistoryCountTotal() {
		return updatetransactionHistoryCountTotal;
	}

	public int getCollectionnetworkcountTotal() {
		return collectionnetworkcountTotal;
	}

	public void setCollectionnetworkcountTotal(int collectionnetworkcountTotal) {
		this.collectionnetworkcountTotal = collectionnetworkcountTotal;
	}

	public int getCollectiondefineCountTotal() {
		return collectiondefineCountTotal;
	}

	public int getCollectionbalanceCountTotal() {
		return collectionbalanceCountTotal;
	}

	public int getCollectionbalanceHistoryCountTotal() {
		return collectionbalanceHistoryCountTotal;
	}

	public int getCollectionprocessedtransCountTotal() {
		return collectionprocessedtransCountTotal;
	}

	public int getClientwithdrawalCountTotal() {
		return clientwithdrawalCountTotal;
	}

	public int getCheckerwithdrawalCountTotal() {
		return checkerwithdrawalCountTotal;
	}

	public int getWithdrawalhistoryCountTotal() {
		return withdrawalhistoryCountTotal;
	}

	public int getWithdrawalonHoldCountTotal() {
		return withdrawalonHoldCountTotal;
	}

	public void setWithdrawalonHoldCountTotal(int withdrawalonHoldCountTotal) {
		this.withdrawalonHoldCountTotal = withdrawalonHoldCountTotal;
	}
	
	

	public int getRefundCountTotal() {
		return refundCountTotal;
	}

	

	public int getWithdrawalonHoldHistCountTotal() {
		return withdrawalonHoldHistCountTotal;
	}
	
	public void setRefundCountTotal(int refundCountTotal) {
		this.refundCountTotal = refundCountTotal;
	}
	
	
	public int getRefundHistCountTotal() {
		return refundHistCountTotal;
	}
	
	public int getUpdatecollectionCountTotal() {
		return updatecollectionCountTotal;
	}
	
	public int getUpdatecollectionHistoryCountTotal() {
		return updatecollectionHistoryCountTotal;
	}

	public void setRefundHistCountTotal(int refundHistCountTotal) {
		this.refundHistCountTotal = refundHistCountTotal;
	}

	public void setWithdrawalonHoldHistCountTotal(int withdrawalonHoldHistCountTotal) {
		this.withdrawalonHoldHistCountTotal = withdrawalonHoldHistCountTotal;
	}

	public void setWithdrawalhistoryCountTotal(int withdrawalhistoryCountTotal) {
		this.withdrawalhistoryCountTotal = withdrawalhistoryCountTotal;
	}

	public void setCheckerwithdrawalCountTotal(int checkerwithdrawalCountTotal) {
		this.checkerwithdrawalCountTotal = checkerwithdrawalCountTotal;
	}

	public void setClientwithdrawalCountTotal(int clientwithdrawalCountTotal) {
		this.clientwithdrawalCountTotal = clientwithdrawalCountTotal;
	}

	public void setCollectionprocessedtransCountTotal(int collectionprocessedtransCountTotal) {
		this.collectionprocessedtransCountTotal = collectionprocessedtransCountTotal;
	}

	public void setCollectionbalanceCountTotal(int collectionbalanceCountTotal) {
		this.collectionbalanceCountTotal = collectionbalanceCountTotal;
	}

	public void setCollectionbalanceHistoryCountTotal(int collectionbalanceHistoryCountTotal) {
		this.collectionbalanceHistoryCountTotal = collectionbalanceHistoryCountTotal;
	}

	public void setCollectiondefineCountTotal(int collectiondefineCountTotal) {
		this.collectiondefineCountTotal = collectiondefineCountTotal;
	}

	public int getCollectionsubaccountCountTotal() {
		return collectionsubaccountCountTotal;
	}

	public void setCollectionsubaccountCountTotal(int collectionsubaccountCountTotal) {
		this.collectionsubaccountCountTotal = collectionsubaccountCountTotal;
	}

	public void setUpdatetransactionHistoryCountTotal(int updatetransactionHistoryCountTotal) {
		this.updatetransactionHistoryCountTotal = updatetransactionHistoryCountTotal;
	}

	public void setUpdatetransactionCountTotal(int updatetransactionCountTotal) {
		this.updatetransactionCountTotal = updatetransactionCountTotal;
	}

	public void setCommissionCountTotal(int commissionCountTotal) {
		this.commissionCountTotal = commissionCountTotal;
	}

	public void setAlltransactionCountTotal(int alltransactionCountTotal) {
		AlltransactionCountTotal = alltransactionCountTotal;
	}

	public void setFloatCountHistoryTotal(int floatCountHistoryTotal) {
		this.floatCountHistoryTotal = floatCountHistoryTotal;
	}

	public void setFloatbycountryHistoryCountTotal(int floatbycountryHistoryCountTotal) {
		this.floatbycountryHistoryCountTotal = floatbycountryHistoryCountTotal;
	}

	public void setFloatCountTotal(int floatCountTotal) {
		this.floatCountTotal = floatCountTotal;
	}

	public void setFloatbycountryCountTotal(int floatbycountryCountTotal) {
		this.floatbycountryCountTotal = floatbycountryCountTotal;
	}

	public void setCheckerbalancebycountryCountTotal(int checkerbalancebycountryCountTotal) {
		this.checkerbalancebycountryCountTotal = checkerbalancebycountryCountTotal;
	}

	public void setClientmainbalancehistoryCountTotal(int clientmainbalancehistoryCountTotal) {
		this.clientmainbalancehistoryCountTotal = clientmainbalancehistoryCountTotal;
	}

	public void setForexratehistoryCountTotal(int forexratehistoryCountTotal) {
		this.forexratehistoryCountTotal = forexratehistoryCountTotal;
	}

	public void setForexrateCountTotal(int forexrateCountTotal) {
		this.forexrateCountTotal = forexrateCountTotal;
	}

	public void setCheckerforexrateCountTotal(int checkerforexrateCountTotal) {
		this.checkerforexrateCountTotal = checkerforexrateCountTotal;
	}

	public void setNetworkbalanceCountTotal(int networkbalanceCountTotal) {
		this.networkbalanceCountTotal = networkbalanceCountTotal;
	}

	public void setNetworkfloathistoryCountTotal(int networkfloathistoryCountTotal) {
		this.networkfloathistoryCountTotal = networkfloathistoryCountTotal;
	}

	public void setCheckermainfloatCountTotal(int checkermainfloatCountTotal) {
		this.checkermainfloatCountTotal = checkermainfloatCountTotal;
	}

	public void setSystemlogcountTotal(int systemlogcountTotal) {
		this.systemlogcountTotal = systemlogcountTotal;
	}

	public void setTransactionforexCountTotal(int transactionforexCountTotal) {
		this.transactionforexCountTotal = transactionforexCountTotal;
	}

	public void setForexhistoryCountTotal(int forexhistoryCountTotal) {
		this.forexhistoryCountTotal = forexhistoryCountTotal;
	}

	public void setTransactionCountTotal(int transactionCountTotal) {
		this.transactionCountTotal = transactionCountTotal;
	}

	public void setTransactionCountSuccess(int transactionCountSuccess) {
		this.transactionCountSuccess = transactionCountSuccess;
	}

	public void setTransactionByUuidCount(int transactionByUuidCount) {
		this.transactionByUuidCount = transactionByUuidCount;
	}

	public void setTransactionByMsisdnCount(int transactionByMsisdnCount) {
		this.transactionByMsisdnCount = transactionByMsisdnCount;
	}

	public void setTopupCountTotal(int topupCountTotal) {
		this.topupCountTotal = topupCountTotal;
	}

	public void setAlltopupCountTotal(int alltopupCountTotal) {
		this.alltopupCountTotal = alltopupCountTotal;
	}

	public void setManagementAccountCountTotal(int managementAccountCountTotal) {
		ManagementAccountCountTotal = managementAccountCountTotal;
	}

	public void setAccountCountTotal(int accountCountTotal) {
		AccountCountTotal = accountCountTotal;
	}

	public void setMnoprefixCountTotal(int mnoprefixCountTotal) {
		this.mnoprefixCountTotal = mnoprefixCountTotal;
	}

	public void setNetworkcountTotal(int networkcountTotal) {
		this.networkcountTotal = networkcountTotal;
	}

	public void setRoutedefineCountTotal(int routedefineCountTotal) {
		this.routedefineCountTotal = routedefineCountTotal;
	}

	public void setClientipCountTotal(int clientipCountTotal) {
		this.clientipCountTotal = clientipCountTotal;
	}

	public void setClientsessionUrlCountTotal(int clientsessionUrlCountTotal) {
		this.clientsessionUrlCountTotal = clientsessionUrlCountTotal;
	}

	public void setCountryCountTotal(int countryCountTotal) {
		this.countryCountTotal = countryCountTotal;
	}

	public void setCountrymsisdnCountTotal(int countrymsisdnCountTotal) {
		this.countrymsisdnCountTotal = countrymsisdnCountTotal;
	}

	public void setBankCountTotal(int bankCountTotal) {
		this.bankCountTotal = bankCountTotal;
	}

	public void setMerchantCountTotal(int merchantCountTotal) {
		this.merchantCountTotal = merchantCountTotal;
	}

	public void setMainfloatCountTotal(int mainfloatCountTotal) {
		this.mainfloatCountTotal = mainfloatCountTotal;
	}

	public void setMainfloatbycountryCountTotal(int mainfloatbycountryCountTotal) {
		this.mainfloatbycountryCountTotal = mainfloatbycountryCountTotal;
	}

	public void setMainfloathistoryCountTotal(int mainfloathistoryCountTotal) {
		this.mainfloathistoryCountTotal = mainfloathistoryCountTotal;
	}

	public void setMainfloatbycountryhistoryCountTotal(int mainfloatbycountryhistoryCountTotal) {
		this.mainfloatbycountryhistoryCountTotal = mainfloatbycountryhistoryCountTotal;
	}

	public HashMap<Account, Integer> getAccountTransactionCountTotal() {
		return accountTransactionCountTotal;
	}

	public Map<Network, Integer> getNetworkTransactionCountTotal() {
		return networkTransactionCountTotal;
	}

	public void setNetworkTransactionCountTotal(Network network, int count) {
		networkTransactionCountTotal.put(network, new Integer(count));
	}

	public void setAccountTransactionCountTotal(Account account, int count) {
		accountTransactionCountTotal.put(account, count);
	}
	
	

	public void setUpdatecollectionCountTotal(int updatecollectionCountTotal) {
		this.updatecollectionCountTotal = updatecollectionCountTotal;
	}

	

	public void setUpdatecollectionHistoryCountTotal(int updatecollectionHistoryCountTotal) {
		this.updatecollectionHistoryCountTotal = updatecollectionHistoryCountTotal;
	}

	/**
	 * 
	 * @return
	 */
	public Map<Country, Integer> getCountryTransactionCountTotal() {
		return countryTransactionCountTotal;
	}

	/**
	 * add transaction activity of {@link Country}.
	 *
	 * @param country
	 *            the receiver country
	 * @param count
	 *            total number of Transaction requests
	 */
	public void addCountryTransactionCountTotal(Country country, int count) {
		countryTransactionCountTotal.put(country, new Integer(count));
	}

	/**
	 * 
	 * @return
	 */
	public Map<Country, Integer> getCountryTransactionCountSuccess() {
		return countryTransactionCountSuccess;
	}

	/**
	 * add transaction activity of {@link Country}.
	 *
	 * @param country
	 *            the receiver country
	 * @param count
	 *            number of transaction requests that have been successful
	 */
	public void addCountryTransactionCountSuccess(Country country, int count) {
		countryTransactionCountSuccess.put(country, new Integer(count));
	}

	/**
	 * 
	 * @return
	 */
	public Map<Country, Double> getCountryTransactionAmountSuccess() {
		return countryTransactionAmountSuccess;
	}

	/**
	 * add transaction amount of {@link Country}.
	 *
	 * @param country
	 *            the receiver country
	 * @param amount
	 *            the value of Transaction requests that have been successful
	 */
	public void addCountryTransactionAmountSuccess(Country country, double amount) {
		countryTransactionAmountSuccess.put(country, new Double(amount));
	}

	/**
	 * 
	 * @return
	 */
	public Map<String, Map<Country, Double>> getCountryTransactionAmountDay() {
		return countryTransactionAmountDay;
	}

	/**
	 * 
	 * @return
	 */
	public Map<Country, Double> getCountryTransactionAmountDays() {
		return countryTransactionAmountDays;
	}

	/**
	 *
	 * @param day
	 * @param country
	 * @param amount
	 */
	public void addCountryTransactionAmountDay(String day, Country country, int amount) {
		Map<Country, Double> countryMap = countryTransactionAmountDay.get(day);

		if (countryMap == null) {
			countryMap = new HashMap<>();
		}

		countryMap.put(country, new Double(amount));
		countryTransactionAmountDay.put(day, countryMap);
	}

	public void addCountryTransactionAmountDays(Country country, double amount) {
		countryTransactionAmountDays.put(country, amount);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SessionStatistics [transactionCountTotal=");
		builder.append(transactionCountTotal);
		builder.append(", AlltransactionCountTotal=");
		builder.append(AlltransactionCountTotal);
		builder.append(", transactionCountSuccess=");
		builder.append(transactionCountSuccess);
		builder.append(", transactionByUuidCount=");
		builder.append(transactionByUuidCount);
		builder.append(", transactionByMsisdnCount=");
		builder.append(transactionByMsisdnCount);
		builder.append(", forexhistoryCountTotal=");
		builder.append(forexhistoryCountTotal);
		builder.append(", transactionforexCountTotal=");
		builder.append(transactionforexCountTotal);
		builder.append(", topupCountTotal=");
		builder.append(topupCountTotal);
		builder.append(", alltopupCountTotal=");
		builder.append(alltopupCountTotal);
		builder.append(", AccountCountTotal=");
		builder.append(AccountCountTotal);
		builder.append(", ManagementAccountCountTotal=");
		builder.append(ManagementAccountCountTotal);
		builder.append(", mnoprefixCountTotal=");
		builder.append(mnoprefixCountTotal);
		builder.append(", networkcountTotal=");
		builder.append(networkcountTotal);
		builder.append(", routedefineCountTotal=");
		builder.append(routedefineCountTotal);
		builder.append(", clientipCountTotal=");
		builder.append(clientipCountTotal);
		builder.append(", clientsessionUrlCountTotal=");
		builder.append(clientsessionUrlCountTotal);
		builder.append(", systemlogcountTotal=");
		builder.append(systemlogcountTotal);
		builder.append(", countryCountTotal=");
		builder.append(countryCountTotal);
		builder.append(", countrymsisdnCountTotal=");
		builder.append(countrymsisdnCountTotal);
		builder.append(", bankCountTotal=");
		builder.append(bankCountTotal);
		builder.append(", merchantCountTotal=");
		builder.append(merchantCountTotal);
		builder.append(", commissionCountTotal=");
		builder.append(commissionCountTotal);
		builder.append(", mainfloatCountTotal=");
		builder.append(mainfloatCountTotal);
		builder.append(", mainfloatbycountryCountTotal=");
		builder.append(mainfloatbycountryCountTotal);
		builder.append(", mainfloathistoryCountTotal=");
		builder.append(mainfloathistoryCountTotal);
		builder.append(", mainfloatbycountryhistoryCountTotal=");
		builder.append(mainfloatbycountryhistoryCountTotal);
		builder.append(", networkbalanceCountTotal=");
		builder.append(networkbalanceCountTotal);
		builder.append(", networkfloathistoryCountTotal=");
		builder.append(networkfloathistoryCountTotal);
		builder.append(", checkermainfloatCountTotal=");
		builder.append(checkermainfloatCountTotal);
		builder.append(", checkerforexrateCountTotal=");
		builder.append(checkerforexrateCountTotal);
		builder.append(", forexrateCountTotal=");
		builder.append(forexrateCountTotal);
		builder.append(", forexratehistoryCountTotal=");
		builder.append(forexratehistoryCountTotal);
		builder.append(", clientmainbalancehistoryCountTotal=");
		builder.append(clientmainbalancehistoryCountTotal);
		builder.append(", checkerbalancebycountryCountTotal=");
		builder.append(checkerbalancebycountryCountTotal);
		builder.append(", floatCountTotal=");
		builder.append(floatCountTotal);
		builder.append(", floatbycountryCountTotal=");
		builder.append(floatbycountryCountTotal);
		builder.append(", floatCountHistoryTotal=");
		builder.append(floatCountHistoryTotal);
		builder.append(", floatbycountryHistoryCountTotal=");
		builder.append(floatbycountryHistoryCountTotal);
		builder.append(", updatetransactionCountTotal=");
		builder.append(updatetransactionCountTotal);
		builder.append(", updatetransactionHistoryCountTotal=");
		builder.append(updatetransactionHistoryCountTotal);
		builder.append(", collectionnetworkcountTotal=");
		builder.append(collectionnetworkcountTotal);
		builder.append(", collectiondefineCountTotal=");
		builder.append(collectiondefineCountTotal);
		builder.append(", collectionsubaccountCountTotal=");
		builder.append(collectionsubaccountCountTotal);
		builder.append(", collectionbalanceCountTotal=");
		builder.append(collectionbalanceCountTotal);
		builder.append(", collectionbalanceHistoryCountTotal=");
		builder.append(collectionbalanceHistoryCountTotal);
		builder.append(", collectionprocessedtransCountTotal=");
		builder.append(collectionprocessedtransCountTotal);
		builder.append(", updatecollectionCountTotal=");
		builder.append(updatecollectionCountTotal);
		builder.append(", updatecollectionHistoryCountTotal=");
		builder.append(updatecollectionHistoryCountTotal);
		builder.append(", clientwithdrawalCountTotal=");
		builder.append(clientwithdrawalCountTotal);
		builder.append(", checkerwithdrawalCountTotal=");
		builder.append(checkerwithdrawalCountTotal);
		builder.append(", withdrawalhistoryCountTotal=");
		builder.append(withdrawalhistoryCountTotal);
		builder.append(", withdrawalonHoldCountTotal=");
		builder.append(withdrawalonHoldCountTotal);
		builder.append(", withdrawalonHoldHistCountTotal=");
		builder.append(withdrawalonHoldHistCountTotal);
		builder.append(", refundCountTotal=");
		builder.append(refundCountTotal);
		builder.append(", refundHistCountTotal=");
		builder.append(refundHistCountTotal);
		builder.append(", countryTransactionCountTotal=");
		builder.append(countryTransactionCountTotal);
		builder.append(", countryTransactionCountSuccess=");
		builder.append(countryTransactionCountSuccess);
		builder.append(", countryTransactionAmountSuccess=");
		builder.append(countryTransactionAmountSuccess);
		builder.append(", countryTransactionAmountDays=");
		builder.append(countryTransactionAmountDays);
		builder.append(", countryTransactionAmountDay=");
		builder.append(countryTransactionAmountDay);
		builder.append(", networkTransactionCountTotal=");
		builder.append(networkTransactionCountTotal);
		builder.append(", accountTransactionCountTotal=");
		builder.append(accountTransactionCountTotal);
		builder.append("]");
		return builder.toString();
	}
	
	

	


	

}

/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */
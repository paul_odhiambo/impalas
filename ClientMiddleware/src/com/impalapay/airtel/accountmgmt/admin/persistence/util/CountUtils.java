package com.impalapay.airtel.accountmgmt.admin.persistence.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.beans.network.Network;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

/**
 * Database utilities used for counting.
 * <p>
 * Copyright (c) ImpalaPay LTD., June 14, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class CountUtils extends GenericDAO {

	private static CountUtils countUtils;
	private Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 
	 * @return {@link CountUtils}
	 */
	public static CountUtils getInstance() {
		if (countUtils == null) {
			countUtils = new CountUtils();
		}

		return countUtils;
	}

	/**
	 *
	 */
	protected CountUtils() {
		super();

	}

	/**
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public CountUtils(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

	}

	/**
	 * Gets the count of all transactions belonging to all account holders
	 * 
	 * @return the count of all transactions
	 */
	public int getAllTransactionCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM transaction ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transactions count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	/**
	 * Gets the count of all forexhistory
	 * 
	 * @return the count of all forexhistory
	 */
	public int getAllForexHistoryCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM forexhistory ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all forexhistory count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllForexrateHistoryCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM forexratehistory ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all forexratehistory count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllForexrateCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM forexrate ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all forexrate count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllCheckerForexCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM checkerforexrate ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all checkerforexrate count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	/**
	 * Gets the count of all transactionforex
	 * 
	 * @return the count of all transactionforex
	 */
	public int getAllTransactionForexrateCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM transactionforex ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transactionforex count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	/**
	 * 
	 * @param msisdn
	 * @return
	 */
	public int getAllTransactionByRecipientMsisdnCount(String msisdn) {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM transaction WHERE recipientmobile=?;");
			pstmt.setString(1, msisdn);

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transaction count matching phone number " + msisdn);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllTransactionByUuidCount(String uuid) {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM transaction WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transaction count matching uuid " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllTransactionByReceiverCountry(Country country) {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(DISTINCT uuid) FROM transaction WHERE recipientcountryuuid=?;");
			pstmt.setString(1, country.getUuid());

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transaction count with receiver country " + country);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;

	}

	public int getAllTopupCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM topup;");
			// pstmt.setString(1, account.getUuid());

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all topup count '" + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	/**
	 * Gets the count of all accounts.
	 * 
	 * @return the count of all accounts
	 */
	public int getAllAccountCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM account ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all accounts count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	/**
	 * Gets the count of all accounts.
	 * 
	 * @return the count of all accounts
	 */
	public int getAllMnoPrefixCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM prefix ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all prefix count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllNetworkCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM network ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all network count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllRouteDefineCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM routedefine ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all routedefine count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllMasterBalanceCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM clientmainbalance ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all clientmainbalance count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllMasterBalanceHoldCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM clientmainbalancehold ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all clientmainbalancehold count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllMasterBalanceHoldHistCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM clientmainbalanceholdhistory ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all clientmainbalanceholdhistory count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllMasterBalanceCount2() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM clientbalance ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all clientbalance count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllMasterBalanceHistoryCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM topup ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all topup count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllBalanceByCountryCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM balancebycountry ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all balancebycountry count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllBalanceByCountryGHoldCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM balancebycountryhold;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all balancebycountryhold count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllBalanceByCountryHoldHistCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM balancebycountryholdhistory;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all balancebycountryholdhistory count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllCheckerBalanceByCountryCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM checkerbalancebycountry ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all checkerbalancebycountry count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllBalanceByCountryHistoryCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM topupbycountry ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all topupbycountry count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllIpCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM clientipaddress;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all clientipaddress count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllSystemLogCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM systemlog;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all systemlog count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllClientUrlCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM clienturl ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all clienturl count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllCountryCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM country ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all clienturl count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllCountryMsisdnCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM msisdnbycountry ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all msisdnbycountry count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllBankCodeCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM bank;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all banks count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllBillPaymentCodeCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM merchantbillcode;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all merchantbillcode count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllManageAccountCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM managementaccount");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all managementaccount count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllBalanceByNetworkCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM networkbalancebycountry ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all networkbalancebycountry count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllBalanceByNetworkHistoryCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM networktopupbycountry ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all networktopupbycountry count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllCheckerMasterBalanceCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM checkerclientmainbalance ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all checkerclientmainbalance count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllClientMainBalanceHistoryCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM clientmainbalancehistory ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all clientmainbalancehistory count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllCommissionCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM commissionestimate ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all commissionestimate count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	// for charts and statistics

	/**
	 * Gets the count of all success transaction by country belonging to this
	 * account.
	 *
	 * @param country
	 * @param status
	 *
	 * @return int the count of all transactions success
	 */
	public int getTransactionCount(Country country, TransactionStatus status) {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT COUNT(DISTINCT uuid) FROM transaction WHERE transactionstatusuuid = ?"
					+ " AND recipientcountryuuid = ?;");

			pstmt.setString(1, status.getUuid());
			pstmt.setString(2, country.getUuid());

			rset = pstmt.executeQuery();
			rset.next();
			count = rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transactions of '" + "' status '" + status
					+ "' and '" + country + "'.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getTransactionAmount(Country country, TransactionStatus status) {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT SUM(amount) FROM transaction WHERE transactionstatusuuid = ?"
					+ " AND recipientcountryuuid = ?;");

			pstmt.setString(1, status.getUuid());
			pstmt.setString(2, country.getUuid());

			rset = pstmt.executeQuery();
			rset.next();
			count = rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transactions of '" + "' status '" + status
					+ "' and '" + country + "'.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getTransactionCount(Network network) {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT COUNT(DISTINCT uuid) FROM transaction WHERE networkuuid = ?;");
			pstmt.setString(1, network.getUuid());

			rset = pstmt.executeQuery();
			rset.next();
			count = rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transactions of '" + "' Network '" + network + "'.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	/**
	 * 
	 * @param account
	 * @return
	 */
	public int getTransactionCount(Account account) {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT COUNT(DISTINCT uuid) FROM transaction WHERE accountuuid = ?;");
			pstmt.setString(1, account.getUuid());

			rset = pstmt.executeQuery();
			rset.next();
			count = rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transactions of '" + "' Account '" + account + "'.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllTransactionUpdateStatus() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM transactionupdatestatus ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transactionupdatestatus count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllTransactionUpdateStatusHistory() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM transactionupdatestatushistory ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transactionupdatestatushistory count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	// #########################collection_networkd#####################
	//
	// #################################################################
	public int getAllCollectionNetworkCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM collection_network;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collection_network count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllCollectionDefineCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM collectiondefine ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collectiondefine count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	/**
	 * Gets the count of all accounts.
	 * 
	 * @return the count of all accounts
	 */
	public int getAllCollectionSubAccntCount() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM collection_networksubaccount ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collection_networksubaccount count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	// Handling withdrawing of funds.
	public int getAllClientWithdrawal() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM client_withdrawal;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all client_withdrawal count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllCheckerWithdrawal() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM checker_withdrawal;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all checker_withdrawal count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllWithdrawalHistory() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM withdrawal_history;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all withdrawal_history count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllProcessedCollection() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM processedtransaction;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all processedtransaction count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}
	
	
	
	public int getAllUnresolvedCollection() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM temp_unresolvedtransaction;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all temp_unresolvedtransaction count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllCollectionBalance() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM collectionbalance;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collectionbalance count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllCollectionBalanceHistory() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM collectionbalancehistory;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collectionbalancehistory count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllWithdrwalBalanceHold() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM withdrawalbalanceHold;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all withdrawalbalanceHold count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllWithdrwalBalanceHoldHistory() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM withdrawalbalanceHoldhistory;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all withdrawalbalanceHoldhistory count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}
	
	
	//REFUNDS
	public int getAllCollectionRefund() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM collectionrefund;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collectionrefund count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}
	
	public int getAllCollectionRefundHistory() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM collectionrefundhistory;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collectionrefundhistory count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}
	
	
	public int getAllCollectionUpdateStatus() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM collectionupdatestatus;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collectionupdatestatus count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllCollectionUpdateStatusHistory() {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM collectionupdatestatushistory ;");

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collectionupdatestatushistory count ");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}
	
	
	public int getAllCollectionByDebitedAccountCount(String msisdn) {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM processedtransaction WHERE debitedaccount=?;");
			pstmt.setString(1, msisdn);

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transaction count matching phone number " + msisdn);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}
	
	
	public int getAllCollectionByUUIDCount(String uuid) {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM processedtransaction WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transaction count matching uuid " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

	public int getAllCollectionByOriginateUuidCount(String originateuuid) {
		int count = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT count(*) FROM processedtransaction WHERE originatetransactionuuid=?;");
			pstmt.setString(1, originateuuid);

			rset = pstmt.executeQuery();
			rset.next();
			count = count + rset.getInt(1);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transaction count matching originateuuid " + originateuuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return count;
	}

}



/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */
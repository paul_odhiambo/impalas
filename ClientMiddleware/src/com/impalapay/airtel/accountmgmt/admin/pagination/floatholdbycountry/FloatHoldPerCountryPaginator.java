/**
 * Copyright 2015 Tawi Commercial Services Ltd
 * 
 * Licensed under the Open Software License, Version 3.0  you may
 * not use this file except in compliance with the License. You may obtain a copy
 * of the License at:
 * http://opensource.org/licenses/OSL-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an AS IS BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 * 
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package com.impalapay.airtel.accountmgmt.admin.pagination.floatholdbycountry;

import java.util.List;
import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressBalancebyCountryHold;
import com.impalapay.airtel.persistence.accountmgmt.inprogressbalance.InProgressBalanceDAO;

/**
 * Paginate an Inbox HTML view.
 * <p>
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 */
public class FloatHoldPerCountryPaginator {

	/**
	 *
	 */
	// public static final int PAGESIZE = 15; // The number of Incoming SMS to
	// display per page
	public static final int PAGESIZE = 13;
	private final InProgressBalanceDAO floatholdpercountryDAO;
	private final CountUtils countUtils;

	/**
	 *
	 * @param accountuuid
	 */
	public FloatHoldPerCountryPaginator() {

		countUtils = CountUtils.getInstance();
		floatholdpercountryDAO = InProgressBalanceDAO.getInstance();

	}

	/**
	 *
	 * @param email
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public FloatHoldPerCountryPaginator(String email, String dbName, String dbHost, String dbUsername, String dbPasswd,
			int dbPort) {

		// initialize the DAOs
		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);
		floatholdpercountryDAO = new InProgressBalanceDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);
	}

	/**
	 *
	 * @return
	 */
	public FloatHoldPerCountryPage getFirstPage() {

		FloatHoldPerCountryPage page = new FloatHoldPerCountryPage();

		List<InProgressBalancebyCountryHold> prefixList = floatholdpercountryDAO.getAllBalancebyCountryHold(0,
				PAGESIZE);

		page = new FloatHoldPerCountryPage(1, getTotalPage(), PAGESIZE, prefixList);

		return page;
	}

	/**
	 * Provides the last page of the accounts report
	 *
	 * @return accounts page
	 */
	public FloatHoldPerCountryPage getLastPage() {
		FloatHoldPerCountryPage page = new FloatHoldPerCountryPage();

		List<InProgressBalancebyCountryHold> prefixList = null;
		int prefixCount, startIndex;
		int totalPage = getTotalPage();

		startIndex = (totalPage - 1) * PAGESIZE;
		prefixCount = countUtils.getAllBalanceByCountryGHoldCount();

		prefixList = floatholdpercountryDAO.getAllBalancebyCountryHold(startIndex, prefixCount);

		page = new FloatHoldPerCountryPage(totalPage, totalPage, PAGESIZE, prefixList);

		return page;
	}

	/**
	 * Moves you forward to the page of the Accounts that comes after the current
	 * page
	 *
	 * @param currentPage
	 * @return an Account page
	 */
	public FloatHoldPerCountryPage getNextPage(final FloatHoldPerCountryPage currentPage) {
		int totalPage = getTotalPage();

		FloatHoldPerCountryPage page = new FloatHoldPerCountryPage();

		List<InProgressBalancebyCountryHold> prefixList = floatholdpercountryDAO.getAllBalancebyCountryHold(
				currentPage.getPageNum() * PAGESIZE, ((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		page = new FloatHoldPerCountryPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, prefixList);

		return page;
	}

	/**
	 * Moves you backward to the page of the Accounts that comes before the current
	 * page
	 *
	 * @param currentPage
	 * @return an Account page
	 */
	public FloatHoldPerCountryPage getPrevPage(final FloatHoldPerCountryPage currentPage) {
		int totalPage = getTotalPage();

		FloatHoldPerCountryPage page = new FloatHoldPerCountryPage();

		List<InProgressBalancebyCountryHold> prefixList = floatholdpercountryDAO.getAllBalancebyCountryHold(
				(currentPage.getPageNum() - 2) * PAGESIZE, ((currentPage.getPageNum() - 1) * PAGESIZE));

		page = new FloatHoldPerCountryPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, prefixList);

		return page;
	}

	/**
	 * Calculates the total number of pages that would be printed for the Account
	 * sessions that belong to the logged-in account
	 *
	 * @return an integer
	 */
	public int getTotalPage() {
		int totalSize = 0;

		// get the number of all sessions belonging to this email
		totalSize = countUtils.getAllBalanceByCountryGHoldCount();

		// TODO: divide by the page size and add one to take care of remainders
		// and what else?
		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

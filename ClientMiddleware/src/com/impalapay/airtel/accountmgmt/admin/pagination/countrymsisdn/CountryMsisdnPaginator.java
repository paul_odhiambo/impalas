/**
 * Copyright 2015 Tawi Commercial Services Ltd
 * 
 * Licensed under the Open Software License, Version 3.0  you may
 * not use this file except in compliance with the License. You may obtain a copy
 * of the License at:
 * http://opensource.org/licenses/OSL-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an AS IS BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 * 
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package com.impalapay.airtel.accountmgmt.admin.pagination.countrymsisdn;

import java.util.List;

import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.airtel.beans.geolocation.CountryMsisdn;
import com.impalapay.mno.persistence.geolocation.CountryMsisdnDAO;

/**
 * Pagination of Sent HTML view.
 * <p>
 * 
 * @author <a href="mailto:michael@tawi.mobi">Michael Wakahe</a>
 */
public class CountryMsisdnPaginator {

	public static final int PAGESIZE = 15; // The number of Transactions to
											// display per page
	private CountUtils countUtils;
	private CountryMsisdnDAO countrymsisdnDAO;

	/**
	 *
	 * @param username
	 */
	public CountryMsisdnPaginator() {

		countUtils = CountUtils.getInstance();

		countrymsisdnDAO = CountryMsisdnDAO.getInstance();

	}

	/**
	 *
	 * @param username
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public CountryMsisdnPaginator(String dbName, String dbHost, String dbUsername, String dbPasswd, int dbPort) {

		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);

		countrymsisdnDAO = new CountryMsisdnDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);

	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getFirstPage()
	 */
	public CountryMsisdnPage getFirstPage() {
		CountryMsisdnPage result = new CountryMsisdnPage();
		List<CountryMsisdn> topupList;

		topupList = countrymsisdnDAO.getAllCountryMsisdn(0, PAGESIZE);

		result = new CountryMsisdnPage(1, getTotalPage(), PAGESIZE, topupList);

		return result;
	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getLastPage()
	 */
	public CountryMsisdnPage getLastPage() {

		CountryMsisdnPage result = new CountryMsisdnPage();
		List<CountryMsisdn> topupList;
		int transactionCount, startIndex;
		int totalPage = getTotalPage();

		startIndex = (totalPage - 1) * PAGESIZE;
		transactionCount = countUtils.getAllCountryMsisdnCount();
		topupList = countrymsisdnDAO.getAllCountryMsisdn(startIndex, transactionCount);

		result = new CountryMsisdnPage(totalPage, totalPage, PAGESIZE, topupList);

		return result;

	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getNextPage(com.impalapay.airtel.accountmgmt.pagination.TransactionPage)
	 */
	public CountryMsisdnPage getNextPage(final CountryMsisdnPage currentPage) {
		int totalPage = getTotalPage();

		CountryMsisdnPage result = new CountryMsisdnPage();

		List<CountryMsisdn> topupList = countrymsisdnDAO.getAllCountryMsisdn(currentPage.getPageNum() * PAGESIZE,
				((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		result = new CountryMsisdnPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, topupList);

		return result;

	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getPrevPage(com.impalapay.airtel.accountmgmt.pagination.TransactionPage)
	 */
	public CountryMsisdnPage getPrevPage(final CountryMsisdnPage currentPage) {
		int totalPage = getTotalPage();

		CountryMsisdnPage result = new CountryMsisdnPage();
		List<CountryMsisdn> topupList = countrymsisdnDAO.getAllCountryMsisdn((currentPage.getPageNum() - 2) * PAGESIZE,
				((currentPage.getPageNum() - 1) * PAGESIZE));

		result = new CountryMsisdnPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, topupList);

		return result;

	}

	/**
	 *
	 * @return int
	 */
	public int getTotalPage() {
		int totalSize = 0;

		totalSize = countUtils.getAllCountryMsisdnCount();

		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

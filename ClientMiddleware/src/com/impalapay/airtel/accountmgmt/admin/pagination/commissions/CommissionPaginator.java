/**
 * Copyright 2015 Tawi Commercial Services Ltd
 * 
 * Licensed under the Open Software License, Version 3.0  you may
 * not use this file except in compliance with the License. You may obtain a copy
 * of the License at:
 * http://opensource.org/licenses/OSL-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an AS IS BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 * 
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package com.impalapay.airtel.accountmgmt.admin.pagination.commissions;

import java.util.List;
import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.airtel.persistence.commission.CommissionDAO;
import com.impalapay.beans.commission.CommisionEstimate;

/**
 * Pagination of Sent HTML view.
 * <p>
 * 
 * @author <a href="mailto:michael@tawi.mobi">Michael Wakahe</a>
 */
public class CommissionPaginator {

	public static final int PAGESIZE = 15; // The number of Transactions to display per page
	private CountUtils countUtils;
	private CommissionDAO commissionDAO;

	/**
	 *
	 * @param username
	 */
	public CommissionPaginator() {

		// this.username = username;

		countUtils = CountUtils.getInstance();

		// accountDAO = AccountDAO.getInstance();

		// account = accountDAO.getAccountName(username);

		commissionDAO = CommissionDAO.getInstance();

	}

	/**
	 *
	 * @param username
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public CommissionPaginator(String dbName, String dbHost, String dbUsername, String dbPasswd, int dbPort) {

		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);

		commissionDAO = new CommissionDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);

	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getFirstPage()
	 */
	public CommissionPage getFirstPage() {
		CommissionPage result = new CommissionPage();
		List<CommisionEstimate> topupList;

		topupList = commissionDAO.getAllCommission(0, PAGESIZE);

		result = new CommissionPage(1, getTotalPage(), PAGESIZE, topupList);

		return result;
	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getLastPage()
	 */
	public CommissionPage getLastPage() {

		CommissionPage result = new CommissionPage();
		List<CommisionEstimate> topupList;
		int transactionCount, startIndex;
		int totalPage = getTotalPage();

		startIndex = (totalPage - 1) * PAGESIZE;
		transactionCount = countUtils.getAllCommissionCount();
		topupList = commissionDAO.getAllCommission(startIndex, transactionCount);

		result = new CommissionPage(totalPage, totalPage, PAGESIZE, topupList);

		return result;

	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getNextPage(com.impalapay.airtel.accountmgmt.pagination.TransactionPage)
	 */
	public CommissionPage getNextPage(final CommissionPage currentPage) {
		int totalPage = getTotalPage();

		CommissionPage result = new CommissionPage();

		List<CommisionEstimate> topupList = commissionDAO.getAllCommission(currentPage.getPageNum() * PAGESIZE,
				((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		result = new CommissionPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, topupList);

		return result;

	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getPrevPage(com.impalapay.airtel.accountmgmt.pagination.TransactionPage)
	 */
	public CommissionPage getPrevPage(final CommissionPage currentPage) {
		int totalPage = getTotalPage();

		CommissionPage result = new CommissionPage();
		List<CommisionEstimate> topupList = commissionDAO.getAllCommission((currentPage.getPageNum() - 2) * PAGESIZE,
				((currentPage.getPageNum() - 1) * PAGESIZE));

		result = new CommissionPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, topupList);

		return result;

	}

	/**
	 *
	 * @return int
	 */
	public int getTotalPage() {
		int totalSize = 0;

		totalSize = countUtils.getAllCommissionCount();

		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

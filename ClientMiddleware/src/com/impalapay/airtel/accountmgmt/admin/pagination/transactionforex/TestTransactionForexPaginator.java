package com.impalapay.airtel.accountmgmt.admin.pagination.transactionforex;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;

public class TestTransactionForexPaginator {

	final String DB_NAME = "airteldblive";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "airtellive";
	final String DB_PASSWD = "ThejKoyb34";
	final int DB_PORT = 5432;

	/**
	 * Test method for getting firstpage
	 */
	@Ignore
	@Test
	public void testGetFirstPage() {
		TransactionForexPaginator transactionforexPaginator = new TransactionForexPaginator(DB_NAME, DB_HOST,
				DB_USERNAME, DB_PASSWD, DB_PORT);

		TransactionForexPage firstPage = transactionforexPaginator.getFirstPage();
		List<TransactionForexrate> transactionforexList = firstPage.getContents();
		assertEquals(transactionforexList.size(), TransactionForexPaginator.PAGESIZE);

		for (TransactionForexrate s : transactionforexList) {
			System.out.println(s);
		}
	}

	/**
	 * Test method for getting lastpage
	 */
	// @Ignore
	@Test
	public void testGetLastPage() {
		TransactionForexPaginator transactionforexPaginator = new TransactionForexPaginator(DB_NAME, DB_HOST,
				DB_USERNAME, DB_PASSWD, DB_PORT);

		TransactionForexPage lastPage = transactionforexPaginator.getLastPage();
		List<TransactionForexrate> transactionforexList = lastPage.getContents();

		for (TransactionForexrate s : transactionforexList) {
			System.out.println(s);
		}
	}

	@Ignore
	@Test
	public void testGetNextPage() {
		TransactionForexPaginator Paginator = new TransactionForexPaginator(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD,
				DB_PORT);
		int currentPageNum = 3;

		TransactionForexPage page = new TransactionForexPage(currentPageNum, 100, TransactionForexPaginator.PAGESIZE,
				new ArrayList<TransactionForexrate>());
		TransactionForexPage nextPage = Paginator.getNextPage(page);
		List<TransactionForexrate> transactionforexList = nextPage.getContents();

		assertEquals(transactionforexList.size(), TransactionForexPaginator.PAGESIZE);

		for (TransactionForexrate s : transactionforexList) {
			System.out.println(s);
		}
	}

}

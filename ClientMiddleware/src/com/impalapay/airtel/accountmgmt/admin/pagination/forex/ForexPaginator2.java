package com.impalapay.airtel.accountmgmt.admin.pagination.forex;

import com.impalapay.airtel.beans.forex.ForexHistory;

import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;

import com.impalapay.airtel.persistence.forex.ForexHistoryDAO;

import java.util.List;

/**
 * Class responsible for breaking down a {@link java.util.List} of
 * {@link com.impalapay.airtel.beans.transaction.Forex} into
 * {@link com.impalapay.airtel.accountmgmt.pagination.TransactionPage} in the
 * admin section.
 *
 * <p>
 * Copyright (c) ImpalaPay LTD., June 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class ForexPaginator2 implements ForexPaginating {

	public static final int PAGESIZE = 15; // The number of forexhistory to display per page//15

	private CountUtils countUtils;

	private ForexHistoryDAO forexhistoryDAO;

	/**
	 * Disable the default constructor
	 */
	public ForexPaginator2() {

		countUtils = CountUtils.getInstance();

		forexhistoryDAO = ForexHistoryDAO.getinstance();

	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public ForexPaginator2(String dbName, String dbHost, String dbUsername, String dbPasswd, int dbPort) {

		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);

		forexhistoryDAO = new ForexHistoryDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);
	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.admin.pagination.ForexPaginating#getFirstPage()
	 */
	@Override
	public ForexPage getFirstPage() {
		ForexPage result = new ForexPage();

		List<ForexHistory> forexhistoryList;

		forexhistoryList = forexhistoryDAO.getAllForexHistory(0, PAGESIZE);

		// result = new ForexPage(1, getTotalPage(), PAGESIZE, forexhistoryList);

		return result;
	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.admin.pagination.ForexPaginating#getLastPage()
	 */
	@Override
	public ForexPage getLastPage() {
		int forexhistoryCount, startIndex;
		int totalPage = getTotalPage();

		ForexPage result = new ForexPage();
		List<ForexHistory> forexhistoryList;

		startIndex = (totalPage - 1) * PAGESIZE;
		forexhistoryCount = countUtils.getAllForexHistoryCount();
		forexhistoryList = forexhistoryDAO.getAllForexHistory(startIndex, forexhistoryCount);

		// result = new ForexPage(totalPage, totalPage, PAGESIZE, forexhistoryList);

		return result;
	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.admin.pagination.ForexPaginating#getNextPage(com.impalapay.airtel.accountmgmt.pagination.TransactionPage)
	 */
	@Override
	public ForexPage getNextPage(final ForexPage currentPage) {
		int totalPage = getTotalPage();

		ForexPage result = new ForexPage();
		List<ForexHistory> forexhistoryList;

		forexhistoryList = forexhistoryDAO.getAllForexHistory(currentPage.getPageNum() * PAGESIZE,
				(currentPage.getPageNum() * PAGESIZE) + PAGESIZE);

		// result = new ForexPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE,
		// forexhistoryList);

		return result;
	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.admin.pagination.ForexPaginating#getPrevPage(com.impalapay.airtel.accountmgmt.pagination.TransactionPage)
	 */
	@Override
	public ForexPage getPrevPage(final ForexPage currentPage) {
		int totalPage = getTotalPage();

		ForexPage result = new ForexPage();
		List<ForexHistory> forexhistoryList;

		forexhistoryList = forexhistoryDAO.getAllForexHistory((currentPage.getPageNum() - 2) * PAGESIZE,
				(currentPage.getPageNum() - 1) * PAGESIZE);

		// result = new ForexPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE,
		// forexhistoryList);

		return result;
	}

	/**
	 *
	 * @return int
	 */
	public int getTotalPage() {
		int totalSize = 0;

		totalSize = countUtils.getAllForexHistoryCount();

		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

/*
 ** Local Variables: mode: java c-basic-offset: 2 tab-width: 2 indent-tabs-mode:
 * nil End:
 **
 ** ex: set softtabstop=2 tabstop=2 expandtab:
 **
 */
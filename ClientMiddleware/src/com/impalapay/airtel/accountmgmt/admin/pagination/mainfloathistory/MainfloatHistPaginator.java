/**
 * Copyright 2015 Tawi Commercial Services Ltd
 * 
 * Licensed under the Open Software License, Version 3.0  you may
 * not use this file except in compliance with the License. You may obtain a copy
 * of the License at:
 * http://opensource.org/licenses/OSL-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an AS IS BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 * 
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package com.impalapay.airtel.accountmgmt.admin.pagination.mainfloathistory;

import java.util.List;

import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.mno.beans.accountmgmt.balance.MasterAccountFloatPurchase;
import com.impalapay.mno.persistence.accountmgmt.balance.AccountPurchaseDAO;

/**
 * Paginate an Inbox HTML view.
 * <p>
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 */
public class MainfloatHistPaginator {

	/**
	 *
	 */
	// public static final int PAGESIZE = 15; // The number of Incoming SMS to
	// display per page
	public static final int PAGESIZE = 13;
	private final AccountPurchaseDAO accountpurchaseDAO;
	private final CountUtils countUtils;

	/**
	 *
	 * @param accountuuid
	 */
	public MainfloatHistPaginator() {

		countUtils = CountUtils.getInstance();
		accountpurchaseDAO = AccountPurchaseDAO.getInstance();

	}

	/**
	 *
	 * @param email
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public MainfloatHistPaginator(String email, String dbName, String dbHost, String dbUsername, String dbPasswd,
			int dbPort) {

		// initialize the DAOs
		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);
		accountpurchaseDAO = new AccountPurchaseDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);
	}

	/**
	 *
	 * @return
	 */
	public MainfloatHistPage getFirstPage() {

		MainfloatHistPage page = new MainfloatHistPage();

		List<MasterAccountFloatPurchase> accountList = accountpurchaseDAO.getAllMasterFloatHistory(0, PAGESIZE);

		page = new MainfloatHistPage(1, getTotalPage(), PAGESIZE, accountList);
		// result = new IncomingSMSPage (1, getTotalPage(), PAGESIZE, smsList);

		return page;
	}

	/**
	 * Provides the last page of the accounts report
	 *
	 * @return accounts page
	 */
	public MainfloatHistPage getLastPage() {
		MainfloatHistPage page = new MainfloatHistPage();

		List<MasterAccountFloatPurchase> accountList = null;
		int accountCount, startIndex;
		int totalPage = getTotalPage();

		startIndex = (totalPage - 1) * PAGESIZE;
		accountCount = countUtils.getAllClientMainBalanceHistoryCount();

		accountList = accountpurchaseDAO.getAllMasterFloatHistory(startIndex, accountCount);

		page = new MainfloatHistPage(totalPage, totalPage, PAGESIZE, accountList);

		return page;
	}

	/**
	 * Moves you forward to the page of the Accounts that comes after the current
	 * page
	 *
	 * @param currentPage
	 * @return an Account page
	 */
	public MainfloatHistPage getNextPage(final MainfloatHistPage currentPage) {
		int totalPage = getTotalPage();

		MainfloatHistPage page = new MainfloatHistPage();

		List<MasterAccountFloatPurchase> accountList = accountpurchaseDAO.getAllMasterFloatHistory(
				currentPage.getPageNum() * PAGESIZE, ((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		page = new MainfloatHistPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, accountList);

		return page;
	}

	/**
	 * Moves you backward to the page of the Accounts that comes before the current
	 * page
	 *
	 * @param currentPage
	 * @return an Account page
	 */
	public MainfloatHistPage getPrevPage(final MainfloatHistPage currentPage) {
		int totalPage = getTotalPage();

		MainfloatHistPage page = new MainfloatHistPage();

		List<MasterAccountFloatPurchase> accountList = accountpurchaseDAO.getAllMasterFloatHistory(
				(currentPage.getPageNum() - 2) * PAGESIZE, ((currentPage.getPageNum() - 1) * PAGESIZE));

		page = new MainfloatHistPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, accountList);

		return page;
	}

	/**
	 * Calculates the total number of pages that would be printed for the Account
	 * sessions that belong to the logged-in account
	 *
	 * @return an integer
	 */
	public int getTotalPage() {
		int totalSize = 0;

		// get the number of all sessions belonging to this email
		totalSize = countUtils.getAllClientMainBalanceHistoryCount();

		// TODO: divide by the page size and add one to take care of remainders
		// and what else?
		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

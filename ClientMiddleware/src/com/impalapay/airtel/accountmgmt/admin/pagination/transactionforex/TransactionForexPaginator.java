package com.impalapay.airtel.accountmgmt.admin.pagination.transactionforex;

import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;

import java.util.List;

/**
 * Class responsible for breaking down a {@link java.util.List} of
 * {@link com.impalapay.airtel.beans.transaction.Forex} into
 * {@link com.impalapay.airtel.accountmgmt.admin.pagination.transactionforex.TransactionForexPage}
 * in the admin section.
 *
 * <p>
 * Copyright (c) ImpalaPay LTD., June 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class TransactionForexPaginator implements TransactionForexPaginating {

	public static final int PAGESIZE = 15; // The number of forexhistory to display per page//15
	private CountUtils countUtils;
	private TransactionForexDAO transactionforexDAO;

	/**
	 * Disable the default constructor
	 */
	public TransactionForexPaginator() {

		countUtils = CountUtils.getInstance();

		transactionforexDAO = TransactionForexDAO.getinstance();

	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public TransactionForexPaginator(String dbName, String dbHost, String dbUsername, String dbPasswd, int dbPort) {

		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);

		transactionforexDAO = new TransactionForexDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);
	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.admin.pagination.transactionforex.TransactionForexPaginating#getFirstPage()
	 */
	@Override
	public TransactionForexPage getFirstPage() {
		TransactionForexPage result = new TransactionForexPage();

		List<TransactionForexrate> transactionforexList;

		transactionforexList = transactionforexDAO.getAllTransactionForexs(0, PAGESIZE);

		result = new TransactionForexPage(1, getTotalPage(), PAGESIZE, transactionforexList);

		return result;
	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.admin.pagination.transactionforex.TransactionForexPaginating#getLastPage()
	 */
	@Override
	public TransactionForexPage getLastPage() {
		int transactionforexCount, startIndex;
		int totalPage = getTotalPage();

		TransactionForexPage result = new TransactionForexPage();
		List<TransactionForexrate> transactionforexList;

		startIndex = (totalPage - 1) * PAGESIZE;
		transactionforexCount = countUtils.getAllTransactionForexrateCount();
		transactionforexList = transactionforexDAO.getAllTransactionForexs(startIndex, transactionforexCount);

		result = new TransactionForexPage(totalPage, totalPage, PAGESIZE, transactionforexList);

		return result;
	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.admin.pagination.transactionforex.TransactionForexPaginating#getNextPage(com.impalapay.airtel.accountmgmt.pagination.TransactionPage)
	 */
	@Override
	public TransactionForexPage getNextPage(final TransactionForexPage currentPage) {
		int totalPage = getTotalPage();

		TransactionForexPage result = new TransactionForexPage();
		List<TransactionForexrate> transactionforexList;

		transactionforexList = transactionforexDAO.getAllTransactionForexs(currentPage.getPageNum() * PAGESIZE,
				(currentPage.getPageNum() * PAGESIZE) + PAGESIZE);

		result = new TransactionForexPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, transactionforexList);

		return result;
	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.admin.pagination.ForexPaginating#getPrevPage(com.impalapay.airtel.accountmgmt.pagination.TransactionPage)
	 */
	@Override
	public TransactionForexPage getPrevPage(final TransactionForexPage currentPage) {
		int totalPage = getTotalPage();

		TransactionForexPage result = new TransactionForexPage();
		List<TransactionForexrate> transactionforexList;

		transactionforexList = transactionforexDAO.getAllTransactionForexs((currentPage.getPageNum() - 2) * PAGESIZE,
				(currentPage.getPageNum() - 1) * PAGESIZE);

		result = new TransactionForexPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, transactionforexList);

		return result;
	}

	/**
	 *
	 * @return int
	 */
	public int getTotalPage() {
		int totalSize = 0;

		totalSize = countUtils.getAllTransactionForexrateCount();

		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

/*
 ** Local Variables: mode: java c-basic-offset: 2 tab-width: 2 indent-tabs-mode:
 * nil End:
 **
 ** ex: set softtabstop=2 tabstop=2 expandtab:
 **
 */
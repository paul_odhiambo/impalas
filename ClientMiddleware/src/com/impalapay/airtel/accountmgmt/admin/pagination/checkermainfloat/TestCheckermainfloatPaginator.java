package com.impalapay.airtel.accountmgmt.admin.pagination.checkermainfloat;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.impalapay.airtel.beans.forex.ForexEngineHistory;
import com.impalapay.mno.beans.accountmgmt.balance.MasterAccountFloatPurchase;

public class TestCheckermainfloatPaginator {

	final String DB_NAME = "remittancedb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "remittance";
	final String DB_PASSWD = "gertedNen2";
	final int DB_PORT = 5432;

	/**
	 * Test method for getting firstpage
	 */
	// @Ignore
	@Test
	public void testGetFirstPage() {
		MainfloatPaginator topupPaginator = new MainfloatPaginator("EMAIL", DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD,
				DB_PORT);

		MainfloatPage firstPage = topupPaginator.getFirstPage();
		List<MasterAccountFloatPurchase> topupList = firstPage.getContents();
		assertEquals(topupList.size(), MainfloatPaginator.PAGESIZE);

		for (MasterAccountFloatPurchase s : topupList) {
			System.out.println(topupList.size());
		}
	}

}

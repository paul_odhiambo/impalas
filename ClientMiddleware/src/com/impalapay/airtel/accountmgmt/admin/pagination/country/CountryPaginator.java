/**
 * Copyright 2015 Tawi Commercial Services Ltd
 * 
 * Licensed under the Open Software License, Version 3.0  you may
 * not use this file except in compliance with the License. You may obtain a copy
 * of the License at:
 * http://opensource.org/licenses/OSL-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an AS IS BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 * 
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package com.impalapay.airtel.accountmgmt.admin.pagination.country;

import java.util.List;

import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.mno.persistence.geolocation.CountryDAO;

/**
 * Pagination of Sent HTML view.
 * <p>
 * 
 * @author <a href="mailto:michael@tawi.mobi">Michael Wakahe</a>
 */
public class CountryPaginator {

	public static final int PAGESIZE = 15; // The number of Transactions to display per page
	private CountUtils countUtils;
	private CountryDAO countryDAO;

	/**
	 *
	 * @param username
	 */
	public CountryPaginator() {

		// this.username = username;

		countUtils = CountUtils.getInstance();

		// accountDAO = AccountDAO.getInstance();

		// account = accountDAO.getAccountName(username);

		countryDAO = CountryDAO.getInstance();

	}

	/**
	 *
	 * @param username
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public CountryPaginator(String dbName, String dbHost, String dbUsername, String dbPasswd, int dbPort) {

		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);

		countryDAO = new CountryDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);

	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getFirstPage()
	 */
	public CountryPage getFirstPage() {
		CountryPage result = new CountryPage();
		List<Country> topupList;

		topupList = countryDAO.getAllCountries(0, PAGESIZE);

		result = new CountryPage(1, getTotalPage(), PAGESIZE, topupList);

		return result;
	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getLastPage()
	 */
	public CountryPage getLastPage() {

		CountryPage result = new CountryPage();
		List<Country> topupList;
		int transactionCount, startIndex;
		int totalPage = getTotalPage();

		startIndex = (totalPage - 1) * PAGESIZE;
		transactionCount = countUtils.getAllCountryCount();
		topupList = countryDAO.getAllCountries(startIndex, transactionCount);

		result = new CountryPage(totalPage, totalPage, PAGESIZE, topupList);

		return result;

	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getNextPage(com.impalapay.airtel.accountmgmt.pagination.TransactionPage)
	 */
	public CountryPage getNextPage(final CountryPage currentPage) {
		int totalPage = getTotalPage();

		CountryPage result = new CountryPage();

		List<Country> topupList = countryDAO.getAllCountries(currentPage.getPageNum() * PAGESIZE,
				((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		result = new CountryPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, topupList);

		return result;

	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getPrevPage(com.impalapay.airtel.accountmgmt.pagination.TransactionPage)
	 */
	public CountryPage getPrevPage(final CountryPage currentPage) {
		int totalPage = getTotalPage();

		CountryPage result = new CountryPage();
		List<Country> topupList = countryDAO.getAllCountries((currentPage.getPageNum() - 2) * PAGESIZE,
				((currentPage.getPageNum() - 1) * PAGESIZE));

		result = new CountryPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, topupList);

		return result;

	}

	/**
	 *
	 * @return int
	 */
	public int getTotalPage() {
		int totalSize = 0;

		totalSize = countUtils.getAllCountryCount();

		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

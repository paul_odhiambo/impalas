package com.impalapay.airtel.beans.geolocation;

import com.impalapay.airtel.beans.StorableBean;

import java.util.Date;

/**
 * Represents a collection of misdn for each account in the 17 countries.
 * <p>
 * Copyright (c) ImpalaPay LTD., Feb 14, 2015
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */

public class CountryMsisdn extends StorableBean {

	private String countryUuid;

	private String accountUuid;

	private String networkUuid;

	private String msisdn;

	private Date creationDate;

	public CountryMsisdn() {
		super();

		countryUuid = "";
		accountUuid = "";
		networkUuid = "";
		msisdn = "";
		creationDate = new Date();
	}

	/**
	 * 
	 * @return
	 */
	public String getCountryUuid() {
		return countryUuid;
	}

	/**
	 * 
	 * @param countryUuid
	 */
	public void setCountryUuid(String countryUuid) {
		this.countryUuid = countryUuid;
	}

	/**
	 * 
	 * @return
	 */
	public String getAccountUuid() {
		return accountUuid;
	}

	/**
	 * 
	 * @param accountUuid
	 */
	public void setAccountUuid(String accountUuid) {
		this.accountUuid = accountUuid;
	}

	/**
	 * 
	 * @return
	 */
	public String getNetworkUuid() {
		return networkUuid;
	}

	/**
	 * 
	 * @param networkUuid
	 */
	public void setNetworkUuid(String networkUuid) {
		this.networkUuid = networkUuid;
	}

	/**
	 * 
	 * @return
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * 
	 * @param msisdn
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	/**
	 * 
	 * @return
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * 
	 * @param creationDate
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CountryMsisdn [getUuid()=");
		builder.append(getUuid());
		builder.append(", countryUuid=");
		builder.append(countryUuid);
		builder.append(", accountUuid=");
		builder.append(accountUuid);
		builder.append(", networkUuid=");
		builder.append(networkUuid);
		builder.append(", msisdn=");
		builder.append(msisdn);
		builder.append(", creationDate=");
		builder.append(creationDate);
		builder.append("]");
		return builder.toString();
	}
}

package com.impalapay.airtel.beans.geolocation;

import com.impalapay.airtel.beans.StorableBean;

/**
 * represents a country country
 * <p>
 * Copyright (c) impalapay Ltd., June 24, 2014
 * 
 * @author <a href="mailto:eugenechimita@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class Country2 extends StorableBean {

	private String name;
	private String countrycode;
	private String currency;
	private String currencycode;

	/**
	 * 
	 */
	public Country2() {
		super();
		name = "";
		countrycode = "";
		currency = "";
		currencycode = "";

	}

	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return
	 */
	public String getCountrycode() {
		return countrycode;
	}

	/**
	 * 
	 * @param countrycode
	 */
	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}

	/**
	 * 
	 * @return
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * 
	 * @param currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/*
	 * 
	 */
	public String getCurrencycode() {
		return currencycode;
	}

	/**
	 * 
	 * @param currencycode
	 */
	public void setCurrencycode(String currencycode) {
		this.currencycode = currencycode;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Country [getUuid()=");
		builder.append(getUuid());
		builder.append(", name=");
		builder.append(name);
		builder.append(", countrycode=");
		builder.append(countrycode);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", currencycode=");
		builder.append(currencycode);
		builder.append("]");
		return builder.toString();
	}

}

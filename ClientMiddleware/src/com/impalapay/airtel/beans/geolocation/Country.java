package com.impalapay.airtel.beans.geolocation;

import com.impalapay.airtel.beans.StorableBean;

/**
 * represents a country country
 * <p>
 * Copyright (c) impalapay Ltd., June 24, 2014
 * 
 * @author <a href="mailto:eugenechimita@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class Country extends StorableBean {

	private String name;
	private String countrycode;
	private String currency;
	private String currencycode;
	private String airtelnetwork;
	private String countryremitip;
	private String countrybalanceip;
	private String countryverifyip;
	private String username;
	private String password;
	private int mobilesplitlength;

	/**
	 * 
	 */
	public Country() {
		super();
		name = "";
		countrycode = "";
		currency = "";
		currencycode = "";
		airtelnetwork = "";
		countryremitip = "";
		countrybalanceip = "";
		countryverifyip = "";
		username = "";
		password = "";
		mobilesplitlength = 0;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountrycode() {
		return countrycode;
	}

	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCurrencycode() {
		return currencycode;
	}

	public void setCurrencycode(String currencycode) {
		this.currencycode = currencycode;
	}

	public String getAirtelnetwork() {
		return airtelnetwork;
	}

	public void setAirtelnetwork(String airtelnetwork) {
		this.airtelnetwork = airtelnetwork;
	}

	public String getCountryremitip() {
		return countryremitip;
	}

	public void setCountryremitip(String countryremitip) {
		this.countryremitip = countryremitip;
	}

	public String getCountrybalanceip() {
		return countrybalanceip;
	}

	public void setCountrybalanceip(String countrybalanceip) {
		this.countrybalanceip = countrybalanceip;
	}

	public String getCountryverifyip() {
		return countryverifyip;
	}

	public void setCountryverifyip(String countryverifyip) {
		this.countryverifyip = countryverifyip;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getMobilesplitlength() {
		return mobilesplitlength;
	}

	public void setMobilesplitlength(int mobilesplitlength) {
		this.mobilesplitlength = mobilesplitlength;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Country [getUuid()=");
		builder.append(getUuid());
		builder.append(", name=");
		builder.append(name);
		builder.append(", countrycode=");
		builder.append(countrycode);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", currencycode=");
		builder.append(currencycode);
		builder.append(", airtelnetwork=");
		builder.append(airtelnetwork);
		builder.append(", countryremitip=");
		builder.append(countryremitip);
		builder.append(", countrybalanceip=");
		builder.append(countrybalanceip);
		builder.append(", countryverifyip=");
		builder.append(countryverifyip);
		builder.append(", username=");
		builder.append(username);
		builder.append(", password=");
		builder.append(password);
		builder.append(", mobilesplitlength=");
		builder.append(mobilesplitlength);
		builder.append("]");
		return builder.toString();
	}

}

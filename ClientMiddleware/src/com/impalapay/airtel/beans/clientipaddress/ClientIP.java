package com.impalapay.airtel.beans.clientipaddress;

import com.impalapay.airtel.beans.StorableBean;

public class ClientIP extends StorableBean {
	private String accountUuid;
	private String ipAddress;

	public ClientIP() {
		super();
		accountUuid = "";
		ipAddress = "";

	}

	/**
	 * 
	 * @return
	 */
	public String getAccountUuid() {
		return accountUuid;
	}

	/**
	 * 
	 * @return
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * 
	 * @param accountUuid
	 */
	public void setAccountUuid(String accountUuid) {
		this.accountUuid = accountUuid;
	}

	/**
	 * 
	 * @param ipAddress
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ClientIP [getUuid()=");
		builder.append(getUuid());
		builder.append(", accountUuid=");
		builder.append(accountUuid);
		builder.append(", ipAddress=");
		builder.append(ipAddress);
		builder.append("]");
		return builder.toString();
	}

}

package com.impalapay.airtel.beans.accountmgmt.inprogressbalance;

public class InProgressMasterBalanceHold extends InProgressBalanceHold {

	private String currency;

	public InProgressMasterBalanceHold() {
		super();

		currency = "";

	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InProgressMasterBalanceHold [getUuid()=");
		builder.append(getUuid());
		builder.append(", getAccountUuid()=");
		builder.append(getAccountUuid());
		builder.append(", getTransactionuuid()=");
		builder.append(getTransactionuuid());
		builder.append(", getAmount()=");
		builder.append(getAmount());
		builder.append(", currency=");
		builder.append(currency);
		builder.append("]");
		return builder.toString();
	}

}

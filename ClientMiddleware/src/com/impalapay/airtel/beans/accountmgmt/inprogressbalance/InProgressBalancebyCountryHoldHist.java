package com.impalapay.airtel.beans.accountmgmt.inprogressbalance;

public class InProgressBalancebyCountryHoldHist extends InProgressBalanceHoldHistory {

	private String countryuuid;

	public InProgressBalancebyCountryHoldHist() {
		super();

		countryuuid = "";

	}

	public String getCountryuuid() {
		return countryuuid;
	}

	public void setCountryuuid(String countryuuid) {
		this.countryuuid = countryuuid;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InProgressBalancebyCountryHoldHist [getUuid()=");
		builder.append(getUuid());
		builder.append(", getTopuptime()=");
		builder.append(getTopuptime());
		builder.append(", isRefundedback()=");
		builder.append(isRefundedback());
		builder.append(", isProcessed()=");
		builder.append(isProcessed());
		builder.append(", getAccountUuid()=");
		builder.append(getAccountUuid());
		builder.append(", getTransactionuuid()=");
		builder.append(getTransactionuuid());
		builder.append(", getAmount()=");
		builder.append(getAmount());
		builder.append(", countryuuid=");
		builder.append(countryuuid);
		builder.append(", getCountryuuid()=");
		builder.append(getCountryuuid());
		builder.append("]");
		return builder.toString();
	}

}

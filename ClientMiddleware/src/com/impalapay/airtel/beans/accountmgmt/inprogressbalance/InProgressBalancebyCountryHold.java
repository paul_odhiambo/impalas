package com.impalapay.airtel.beans.accountmgmt.inprogressbalance;

public class InProgressBalancebyCountryHold extends InProgressBalanceHold {

	private String countryuuid;

	public InProgressBalancebyCountryHold() {
		super();

		countryuuid = "";

	}

	public String getCountryuuid() {
		return countryuuid;
	}

	public void setCountryuuid(String countryuuid) {
		this.countryuuid = countryuuid;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InProgressBalancebyCountryHold [getUuid()=");
		builder.append(getUuid());
		builder.append(", getAccountUuid()=");
		builder.append(getAccountUuid());
		builder.append(", getTransactionuuid()=");
		builder.append(getTransactionuuid());
		builder.append(", getAmount()=");
		builder.append(getAmount());
		builder.append(", countryuuid=");
		builder.append(countryuuid);
		builder.append("]");
		return builder.toString();
	}

}

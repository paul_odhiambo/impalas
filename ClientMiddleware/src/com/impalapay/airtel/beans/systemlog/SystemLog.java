package com.impalapay.airtel.beans.systemlog;

import java.util.Date;

import com.impalapay.airtel.beans.StorableBean;

public class SystemLog extends StorableBean {

	private String username;
	private String action;
	private Date creationdate;

	public SystemLog() {
		super();
		username = "";
		action = "";
		creationdate = new Date();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SystemLog [getUuid()=");
		builder.append(getUuid());
		builder.append(", username=");
		builder.append(username);
		builder.append(", action=");
		builder.append(action);
		builder.append(", creationdate=");
		builder.append(creationdate);
		builder.append("]");
		return builder.toString();
	}

}

package com.impalapay.airtel.beans.forex;

import com.impalapay.airtel.beans.StorableBean;

public class Forex extends StorableBean {

	private String countryUuid;
	private double baserate;
	private double impalarate;
	// private Date uploadDate;

	public Forex() {
		super();
		countryUuid = "";
		baserate = 0;
		impalarate = 0;
		;
		// uploadDate = new Date();

	}

	/**
	 * 
	 * @return
	 */
	public String getCountryUuid() {
		return countryUuid;
	}

	/**
	 * 
	 * @return
	 */
	public double getBaserate() {
		return baserate;
	}

	/**
	 * 
	 * @return
	 */
	public double getImpalarate() {
		return impalarate;
	}

	/**
	 * 
	 * @param countryUuid
	 */
	public void setCountryUuid(String countryUuid) {
		this.countryUuid = countryUuid;
	}

	/**
	 * 
	 * @param baserate
	 */
	public void setBaserate(double baserate) {
		this.baserate = baserate;
	}

	/**
	 * 
	 * @param impalarate
	 */
	public void setImpalarate(double impalarate) {
		this.impalarate = impalarate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Forex [getUuid()=");
		builder.append(getUuid());
		builder.append(", countryUuid=");
		builder.append(countryUuid);
		builder.append(", baserate=");
		builder.append(baserate);
		builder.append(", impalarate=");
		builder.append(impalarate);
		builder.append("]");
		return builder.toString();
	}

}

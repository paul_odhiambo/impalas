package com.impalapay.airtel.beans.forex;

import com.impalapay.airtel.beans.StorableBean;

public class ForexEngine extends StorableBean {

	private String basecurrency;
	private String quotecurrency;
	private String currencypair;
	private double marketrate;
	private double spreadrate;
	// private Date uploadDate;

	public ForexEngine() {
		super();
		basecurrency = "";
		quotecurrency = "";
		currencypair = "";
		marketrate = 0;
		spreadrate = 0;
		;
		// uploadDate = new Date();

	}

	public String getBasecurrency() {
		return basecurrency;
	}

	public void setBasecurrency(String basecurrency) {
		this.basecurrency = basecurrency;
	}

	public String getQuotecurrency() {
		return quotecurrency;
	}

	public void setQuotecurrency(String quotecurrency) {
		this.quotecurrency = quotecurrency;
	}

	public String getCurrencypair() {
		return currencypair;
	}

	public void setCurrencypair(String currencypair) {
		this.currencypair = currencypair;
	}

	public double getMarketrate() {
		return marketrate;
	}

	public void setMarketrate(double marketrate) {
		this.marketrate = marketrate;
	}

	public double getSpreadrate() {
		return spreadrate;
	}

	public void setSpreadrate(double spreadrate) {
		this.spreadrate = spreadrate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ForexEngine [getUuid()=");
		builder.append(getUuid());
		builder.append(", basecurrency=");
		builder.append(basecurrency);
		builder.append(", quotecurrency=");
		builder.append(quotecurrency);
		builder.append(", currencypair=");
		builder.append(currencypair);
		builder.append(", marketrate=");
		builder.append(marketrate);
		builder.append(", spreadrate=");
		builder.append(spreadrate);
		builder.append("]");
		return builder.toString();
	}

}

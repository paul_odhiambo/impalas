package com.impalapay.airtel.beans.billpayment;

import java.util.Date;

import com.impalapay.airtel.beans.StorableBean;

public class BillPaymentCodes extends StorableBean {

	private String merchantname;
	private String countryuuid;
	private String networkuuid;
	private String merchantcode;
	private String merchantid;
	private String msisdn;
	private Date dateadded;

	public BillPaymentCodes() {
		super();
		merchantname = "";
		countryuuid = "";
		networkuuid = "";
		merchantcode = "";
		merchantid = "";
		msisdn = "";
		dateadded = new Date();
	}

	public String getMerchantname() {
		return merchantname;
	}

	public String getCountryuuid() {
		return countryuuid;
	}

	public String getNetworkuuid() {
		return networkuuid;
	}

	public String getMerchantcode() {
		return merchantcode;
	}

	public String getMerchantid() {
		return merchantid;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public Date getDateadded() {
		return dateadded;
	}

	public void setMerchantname(String merchantname) {
		this.merchantname = merchantname;
	}

	public void setCountryuuid(String countryuuid) {
		this.countryuuid = countryuuid;
	}

	public void setNetworkuuid(String networkuuid) {
		this.networkuuid = networkuuid;
	}

	public void setMerchantcode(String merchantcode) {
		this.merchantcode = merchantcode;
	}

	public void setMerchantid(String merchantid) {
		this.merchantid = merchantid;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public void setDateadded(Date dateadded) {
		this.dateadded = dateadded;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BillPaymentCodes [getUuid()=");
		builder.append(getUuid());
		builder.append(", merchantname=");
		builder.append(merchantname);
		builder.append(", countryuuid=");
		builder.append(countryuuid);
		builder.append(", networkuuid=");
		builder.append(networkuuid);
		builder.append(", merchantcode=");
		builder.append(merchantcode);
		builder.append(", merchantid=");
		builder.append(merchantid);
		builder.append(", msisdn=");
		builder.append(msisdn);
		builder.append(", dateadded=");
		builder.append(dateadded);
		builder.append("]");
		return builder.toString();
	}

}

package com.impalapay.airtel.beans.thirdreference;

import java.util.Date;

import com.impalapay.airtel.beans.StorableBean;

public class ThirdPartyReference extends StorableBean {
	private String transactionuuid;
	private String referencenumber;
	private Date serverTime;
	// private Date uploadDate;

	public ThirdPartyReference() {
		super();
		transactionuuid = "";
		referencenumber = "";
		serverTime = new Date();

	}

	public String getTransactionuuid() {
		return transactionuuid;
	}

	public String getReferencenumber() {
		return referencenumber;
	}

	public Date getServerTime() {
		return new Date(serverTime.getTime());
	}

	public void setTransactionuuid(String transactionuuid) {
		this.transactionuuid = transactionuuid;
	}

	public void setReferencenumber(String referencenumber) {
		this.referencenumber = referencenumber;
	}

	public void setServerTime(Date d) {
		if (d != null) {
			serverTime = new Date(d.getTime());
		}
	}

}

package com.impalapay.airtel.beans.transaction;

import java.util.Date;

import com.impalapay.airtel.beans.StorableBean;

public class TransactiontransferType extends StorableBean {

	private String transactionUuid;
	private String transactiontypeUuid;
	private String sourcecountrycode;
	private String senderfirstname;
	private String senderlastname;
	private String senderaddress;
	private String sendercity;
	private String recipientfirstname;
	private String recipientlastname;
	private String recipientmobile;
	private String recipientaccount;
	private Date servertime;

	public TransactiontransferType() {
		super();
		transactionUuid = "";
		transactiontypeUuid = "";
		sourcecountrycode = "";
		senderfirstname = "";
		senderlastname = "";
		senderaddress = "";
		sendercity = "";
		recipientfirstname = "";
		recipientlastname = "";
		recipientmobile = "";
		recipientaccount = "";
		servertime = new Date();

	}

	public String getTransactionUuid() {
		return transactionUuid;
	}

	public void setTransactionUuid(String transactionUuid) {
		this.transactionUuid = transactionUuid;
	}

	public String getTransactiontypeUuid() {
		return transactiontypeUuid;
	}

	public String getSourcecountrycode() {
		return sourcecountrycode;
	}

	public String getSenderfirstname() {
		return senderfirstname;
	}

	public String getSenderlastname() {
		return senderlastname;
	}

	public String getSenderaddress() {
		return senderaddress;
	}

	public String getSendercity() {
		return sendercity;
	}

	public String getRecipientfirstname() {
		return recipientfirstname;
	}

	public String getRecipientlastname() {
		return recipientlastname;
	}

	public String getRecipientmobile() {
		return recipientmobile;
	}

	public String getRecipientaccount() {
		return recipientaccount;
	}

	public Date getServertime() {
		return servertime;
	}

	public void setTransactiontypeUuid(String transactiontypeUuid) {
		this.transactiontypeUuid = transactiontypeUuid;
	}

	public void setSourcecountrycode(String sourcecountrycode) {
		this.sourcecountrycode = sourcecountrycode;
	}

	public void setSenderfirstname(String senderfirstname) {
		this.senderfirstname = senderfirstname;
	}

	public void setSenderlastname(String senderlastname) {
		this.senderlastname = senderlastname;
	}

	public void setSenderaddress(String senderaddress) {
		this.senderaddress = senderaddress;
	}

	public void setSendercity(String sendercity) {
		this.sendercity = sendercity;
	}

	public void setRecipientfirstname(String recipientfirstname) {
		this.recipientfirstname = recipientfirstname;
	}

	public void setRecipientlastname(String recipientlastname) {
		this.recipientlastname = recipientlastname;
	}

	public void setRecipientmobile(String recipientmobile) {
		this.recipientmobile = recipientmobile;
	}

	public void setRecipientaccount(String recipientaccount) {
		this.recipientaccount = recipientaccount;
	}

	public void setServertime(Date servertime) {
		this.servertime = servertime;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TransactiontransferType [getUuid()=");
		builder.append(getUuid());
		builder.append(", transactionUuid=");
		builder.append(transactionUuid);
		builder.append(", transactiontypeUuid=");
		builder.append(transactiontypeUuid);
		builder.append(", sourcecountrycode=");
		builder.append(sourcecountrycode);
		builder.append(", senderfirstname=");
		builder.append(senderfirstname);
		builder.append(", senderlastname=");
		builder.append(senderlastname);
		builder.append(", senderaddress=");
		builder.append(senderaddress);
		builder.append(", sendercity=");
		builder.append(sendercity);
		builder.append(", recipientfirstname=");
		builder.append(recipientfirstname);
		builder.append(", recipientlastname=");
		builder.append(recipientlastname);
		builder.append(", recipientmobile=");
		builder.append(recipientmobile);
		builder.append(", recipientaccount=");
		builder.append(recipientaccount);
		builder.append(", servertime=");
		builder.append(servertime);
		builder.append("]");
		return builder.toString();
	}

}

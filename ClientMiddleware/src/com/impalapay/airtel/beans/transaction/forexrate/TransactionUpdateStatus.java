package com.impalapay.airtel.beans.transaction.forexrate;

public class TransactionUpdateStatus extends TransactionForexrate {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6810250093199874462L;
	public String currentstatus;
	public String updatestatus;
	public String sourcecurrency;
	public String recipientcurrency;

	public TransactionUpdateStatus() {
		currentstatus = "";
		updatestatus = "";
		sourcecurrency = "";
		recipientcurrency = "";

	}

	public String getCurrentstatus() {
		return currentstatus;
	}

	public void setCurrentstatus(String currentstatus) {
		this.currentstatus = currentstatus;
	}

	public String getUpdatestatus() {
		return updatestatus;
	}

	public void setUpdatestatus(String updatestatus) {
		this.updatestatus = updatestatus;
	}

	public String getSourcecurrency() {
		return sourcecurrency;
	}

	public void setSourcecurrency(String sourcecurrency) {
		this.sourcecurrency = sourcecurrency;
	}

	public String getRecipientcurrency() {
		return recipientcurrency;
	}

	public void setRecipientcurrency(String recipientcurrency) {
		this.recipientcurrency = recipientcurrency;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TransactionUpdateStatus [currentstatus=");
		builder.append(currentstatus);
		builder.append(", updatestatus=");
		builder.append(updatestatus);
		builder.append(", sourcecurrency=");
		builder.append(sourcecurrency);
		builder.append(", recipientcurrency=");
		builder.append(recipientcurrency);
		builder.append(", getUuid()=");
		builder.append(getUuid());
		builder.append(", getTransactionUuid()=");
		builder.append(getTransactionUuid());
		builder.append(", getImpalarate()=");
		builder.append(getImpalarate());
		builder.append("]");
		return builder.toString();
	}

}

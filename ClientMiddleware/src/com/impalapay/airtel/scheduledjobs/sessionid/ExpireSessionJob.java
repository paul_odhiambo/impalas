package com.impalapay.airtel.scheduledjobs.sessionid;

import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;

import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Description of class.
 * <p>
 * Copyright (c) ImpalaPay LTD., Sep 24, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */

public class ExpireSessionJob implements Job {

	// The number of minutes a Session Id is allowed to be active
	final int SESSIONID_MINUTES_ALIVE = 60;

	private SessionLogDAO sessionLogDAO;

	/**
	 * Empty constructor for job initialization
	 * <p>
	 * Quartz requires a public empty constructor so that the scheduler can
	 * instantiate the class whenever it needs.
	 */
	public ExpireSessionJob() {
		sessionLogDAO = SessionLogDAO.getInstance();
	}

	/**
	 * <p>
	 * Called by the <code>{@link org.quartz.Scheduler}</code> when a
	 * <code>{@link org.quartz.Trigger}</code> fires that is associated with the
	 * <code>Job</code>.
	 * </p>
	 * 
	 * @throws JobExecutionException
	 *             if there is an exception while executing the job.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {

		DateTime dateTime = new DateTime().minusMinutes(SESSIONID_MINUTES_ALIVE);

		sessionLogDAO.expireSessionLogs(dateTime.toGregorianCalendar().getTime());

		// System.out.println("invalide session");

		// now this is perfect.
	}
}

package com.impalapay.airtel.scheduledjobs.sessionid;

import org.apache.commons.lang3.exception.ExceptionUtils;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;
import static org.quartz.SimpleScheduleBuilder.*;

import org.apache.log4j.Logger;

/**
 * Triggers the job that expires Session Ids.
 * <p>
 * Copyright (c) Tawi Commercial Services Ltd., Sep 23, 2014
 * 
 * @author <a href="mailto:michael@tawi.mobi">Michael Wakahe</a>
 * 
 */
public class SessionMgmtLauncher extends Thread {

	private Logger logger;

	/**
	 * 
	 */
	public SessionMgmtLauncher() {
		logger = Logger.getLogger(this.getClass());
	}

	/**
	 * 
	 */
	@Override
	public void run() {
		try {
			// Grab the Scheduler instance from the Factory
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

			// define the job and tie it to our Job class
			JobDetail job = newJob(ExpireSessionJob.class).withIdentity("myJob", "group1").build();

			// Trigger the job to run now, and then every 360 seconds
			Trigger trigger = newTrigger().withIdentity("myTrigger", "group1").startNow()
					.withSchedule(simpleSchedule().withIntervalInSeconds(420).repeatForever()).build();

			// Tell quartz to schedule the job using our trigger
			scheduler.scheduleJob(job, trigger);

			// and start it off
			scheduler.start();

		} catch (SchedulerException e) {
			logger.error("SQLException while triggering the Session Id expiration job.");
			logger.error(ExceptionUtils.getStackTrace(e));
		}
	}

}

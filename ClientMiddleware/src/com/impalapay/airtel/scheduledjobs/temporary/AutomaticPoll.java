package com.impalapay.airtel.scheduledjobs.temporary;

import java.util.HashMap;
import java.util.List;
import com.google.gson.Gson;
import com.impalapay.airtel.persistence.transaction.TransactionDAO;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;

import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This Quartz job is used to check for Session Ids in the SessionLog database
 * table for expiry. Session Ids are expired after a fixed duration of time.
 * <p>
 * Copyright (c) Tawi Commercial Services Ltd., Sep 23, 2014
 * 
 * @author <a href="mailto:michael@tawi.mobi">Michael Wakahe</a>
 * 
 */

public class AutomaticPoll implements Job {

	private PostWithIgnoreSSL postMinusThread;

	private TransactionDAO transactionDAO;

	private String TRANSACTIONSTATUS_UUID = "5c9b8b0b-a035-4a07-bfd8-eccd4f039d53";

	private String CLIENT_URL = "";
	private String responseobject = "";

	private HashMap<String, String> expected = new HashMap<>();
	
	//NEW ADDITION
	//numnber of minutes the transaction should have been in the system(3DAYS) to be Queried
	//anything older than that is not queried.
	final int SESSIONID_MINUTES_ALIVE = 4320;


	/**
	 * Empty constructor for job initialization
	 * <p>
	 * Quartz requires a public empty constructor so that the scheduler can
	 * instantiate the class whenever it needs.
	 */
	public AutomaticPoll() {

		transactionDAO = TransactionDAO.getInstance();
	}

	/**
	 * <p>
	 * Called by the <code>{@link org.quartz.Scheduler}</code> when a
	 * <code>{@link org.quartz.Trigger}</code> fires that is associated with the
	 * <code>Job</code>.
	 * </p>
	 * 
	 * @throws JobExecutionException
	 *             if there is an exception while executing the job.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Gson g = new Gson();
		
		DateTime dateTime = new DateTime().minusMinutes(SESSIONID_MINUTES_ALIVE);

		// DateTime dateTime = new
		// DateTime().minusMinutes(SESSIONID_MINUTES_ALIVE);

		// categorise the transaction as invalid
		// temporaryDAO.expireTempTransaction(dateTime.toGregorianCalendar().getTime());
		// System.out.println("This will definately Rock");

		// delete all the invalidated transactions from the database.
		// temporaryDAO.deleteTempTransactionByTime();

		TransactionStatus ts = new TransactionStatus();
		ts.setUuid(TRANSACTIONSTATUS_UUID);
		//List<Transaction> transactionByStatus = transactionDAO.getTransactionByStatusUuid(ts, 4);
		//List<Transaction> transactionByStatus = transactionDAO.getTransactionByStatusUuid(ts, 15);
		List<Transaction> transactionByStatus = transactionDAO.getTransactionByStatusUuidDate(dateTime.toGregorianCalendar().getTime(), ts, 15);
		

		for (Transaction t : transactionByStatus) {

			expected.put("transaction_id", t.getUuid());
			expected.put("networkuuid", t.getNetworkuuid());
			expected.put("receiverreferenceuuid", t.getReceivertransactionUuid());

			String jsonforxData = g.toJson(expected);

			CLIENT_URL = PropertiesConfig.getConfigValue("AUTOPOLL_URL");
			try {
				postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonforxData);

				// capture the switch respoinse.
				responseobject = postMinusThread.doPost();

				System.out.println(jsonforxData + " response " + responseobject + " ");
			} catch (Exception e) {
				System.out.println(jsonforxData + " automatic query error" + responseobject);
			}

			// Perform post request.
		}

	}
}

package com.impalapay.airtel.servlet.admin.report.chart.pie;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.impalapay.airtel.accountmgmt.session.SessionStatistics;

import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.beans.network.Network;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Allows for generating chart data through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class TransByNetworkCountPie extends HttpServlet {

	private Cache statisticsCache;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		statisticsCache = mgr.getCache(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS);
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(check().getBytes());
		out.flush();
		out.close();

	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String check() {

		Gson g = new GsonBuilder().disableHtmlEscaping().create();

		HashMap<String, Integer> countnetworkHash = new HashMap<>();
		Iterator<String> keyIter;
		String key;
		Element element;
		SessionStatistics statistics = null;

		if ((element = statisticsCache.get(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS_KEY)) != null) {
			statistics = (SessionStatistics) element.getObjectValue();
		}

		Map<Network, Integer> networkTransactionCount = statistics.getNetworkTransactionCountTotal();
		Iterator<Network> networktransactionIter = networkTransactionCount.keySet().iterator();
		Network network;

		while (networktransactionIter.hasNext()) {
			network = networktransactionIter.next();
			countnetworkHash.put(network.getNetworkname(), networkTransactionCount.get(network));
		}

		String jsonResult = g.toJson(countnetworkHash);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}
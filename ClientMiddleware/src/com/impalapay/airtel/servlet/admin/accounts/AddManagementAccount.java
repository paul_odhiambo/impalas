package com.impalapay.airtel.servlet.admin.accounts;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.accountmgmt.ManageAccountDAO;
import com.impalapay.airtel.util.SecurityUtil;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to create a new account.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Nov 24, 2014
 *
 * @author <a href="mailto:mike@impalapay.com">Michael Wakahe</a>
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 *
 */
public class AddManagementAccount extends HttpServlet {

	final String ERROR_NO_ACCOUNTNAME = "Please provide Account Name.";
	final String ERROR_NO_USERNAME = "Please provide a Username.";
	final String ERROR_NO_LOGIN_PASSWD = "Please provide account login password.";
	final String ERROR_NO_FOREX = "Please provide check value for add forex.";
	final String ERROR_NO_REVERSAL = "Please provide check value for add reversal.";
	final String ERROR_NO_BALANCE = "Please provide check value for add balance.";
	final String ERROR_NO_CHECKER = "Please provide value for checker.";
	final String ERROR_LOGIN_PASSWD_MISMATCH = "The account login passwords that you have provided do not match.";
	final String ERROR_UNIQUENAME_EXISTS = "The Unique Name provided already exists in the system.";
	final String ERROR_FAILED_ACTION = "The System Action Failed";
	final String SUCCESS_ACTION = "Action Succ";

	private String accountName, username, loginPasswd, loginPasswd2, accountStatusUuid, updateforex, updatebalance,
			updatereversal, updatehecker;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;

	private ManageAccountDAO managementaccountDAO;

	private CacheManager cacheManager;
	private HttpSession session;

	private String alluuid = "";

	private String transactioinid = "";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		managementaccountDAO = ManageAccountDAO.getInstance();

		cacheManager = CacheManager.getInstance();
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_ADD_MANAGEMENTACCOUNT_PARAMETERS, paramHash);

		// No First Name provided
		if (StringUtils.isBlank(accountName)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_MANAGEMENTACCOUNT_ERROR_KEY, ERROR_NO_ACCOUNTNAME);

			// No Unique Name provided
		} else if (StringUtils.isBlank(username)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_MANAGEMENTACCOUNT_ERROR_KEY, ERROR_NO_USERNAME);

			// No website login password provided
		} else if (StringUtils.isBlank(loginPasswd) || StringUtils.isBlank(loginPasswd2)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_MANAGEMENTACCOUNT_ERROR_KEY, ERROR_NO_LOGIN_PASSWD);

			// The website login passwords provided do not match
		} else if (!StringUtils.equals(loginPasswd, loginPasswd2)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_MANAGEMENTACCOUNT_ERROR_KEY, ERROR_LOGIN_PASSWD_MISMATCH);

		} else if (StringUtils.isBlank(updateforex)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_MANAGEMENTACCOUNT_ERROR_KEY, ERROR_NO_FOREX);

		} else if (StringUtils.isBlank(updatebalance)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_MANAGEMENTACCOUNT_ERROR_KEY, ERROR_NO_BALANCE);

		} else if (StringUtils.isBlank(updatereversal)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_MANAGEMENTACCOUNT_ERROR_KEY, ERROR_NO_REVERSAL);

		} else if (StringUtils.isBlank(updatehecker)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_MANAGEMENTACCOUNT_ERROR_KEY, ERROR_NO_CHECKER);

		} else if (!addAccount()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_MANAGEMENTACCOUNT_ERROR_KEY, ERROR_FAILED_ACTION);

			// The Unique Name already exists in the system
			/**
			 * } else if (existsUniqueName(username)) {
			 * session.setAttribute(SessionConstants.ADMIN_ADD_MANAGEMENTACCOUNT_ERROR_KEY,
			 * ERROR_UNIQUENAME_EXISTS);
			 **/

		} else {
			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_ADD_MANAGEMENTACCOUNT_SUCCESS_KEY, SUCCESS_ACTION);

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_ADD_MANAGEMENTACCOUNT_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_ADD_MANAGEMENTACCOUNT_ERROR_KEY, null);

			// addAccount();
		}

		response.sendRedirect("addManagementAccount.jsp");
	}

	/**
	 *
	 */
	private boolean addAccount() {

		ManagementAccount existmanagementAccount = managementaccountDAO.getAccountName(username);

		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		if (existmanagementAccount != null) {

			alluuid = existmanagementAccount.getUuid();
		} else {
			alluuid = transactioinid;
		}

		ManagementAccount managementaccount1 = new ManagementAccount();

		managementaccount1.setUuid(alluuid);
		managementaccount1.setAccountName(accountName);
		managementaccount1.setUsername(username);
		managementaccount1.setLoginPasswd(SecurityUtil.getMD5Hash(loginPasswd));
		managementaccount1.setAccountStatusUuid(accountStatusUuid);
		managementaccount1.setUpdateforex(Boolean.parseBoolean(updateforex));
		managementaccount1.setUpdatereversal(Boolean.parseBoolean(updatereversal));
		managementaccount1.setUpdatebalance(Boolean.parseBoolean(updatebalance));
		managementaccount1.setChecker(Boolean.parseBoolean(updatehecker));

		// managementaccountDAO.addAccount(managementaccount1);

		boolean response = managementaccountDAO.updateAccount(alluuid, managementaccount1);

		updateManagementAccountCache(managementaccount1);

		return response;
	}

	/**
	 *
	 * @param acc
	 */
	private void updateManagementAccountCache(ManagementAccount acc) {
		cacheManager.getCache(CacheVariables.CACHE_MANAGEMENTACCOUNTS_BY_USERNAME)
				.put(new Element(acc.getUsername(), acc));
		cacheManager.getCache(CacheVariables.CACHE_MANAGEMENTACCOUNTS_BY_UUID).put(new Element(acc.getUuid(), acc));

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		accountName = StringUtils.trimToEmpty(request.getParameter("accountName"));
		username = StringUtils.trimToEmpty(request.getParameter("username"));
		loginPasswd = StringUtils.trimToEmpty(request.getParameter("loginPasswd"));
		loginPasswd2 = StringUtils.trimToEmpty(request.getParameter("loginPasswd2"));
		accountStatusUuid = request.getParameter("accountStatus");
		updateforex = StringUtils.trimToEmpty(request.getParameter("optionforex"));
		updatebalance = StringUtils.trimToEmpty(request.getParameter("optionbalance"));
		updatereversal = StringUtils.trimToEmpty(request.getParameter("optionreversal"));
		updatehecker = StringUtils.trimToEmpty(request.getParameter("optionchecker"));

	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		paramHash = new HashMap<>();

		paramHash.put("accountName", accountName);
		paramHash.put("username", username);
		paramHash.put("loginPasswd", loginPasswd);
		paramHash.put("loginPasswd2", loginPasswd2);
		paramHash.put("updateforex", updateforex);
		paramHash.put("updatebalance", updatebalance);
		paramHash.put("updatereversal", updatereversal);
		paramHash.put("optionchecker", updatehecker);

	}

	/**
	 *
	 * @param name
	 * @return whether or not the unique name exists in the system
	 */
	/**
	 * private boolean existsUniqueName(final String name) { boolean exists = false;
	 * 
	 * if (managementaccountDAO.getAccountName(name) != null) { exists = true; }
	 * 
	 * return exists; }
	 **/

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		// response.sendRedirect("../index.jsp");
	}
}

/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */
package com.impalapay.airtel.servlet.admin.accounts;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.clientipaddress.ClientIP;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.clientipaddress.ClientIpaddressDAO;

public class AddClientIpAddress extends HttpServlet {

	final String ERROR_NO_IMTIP = "Please provide the IP.";
	final String ERROR_NO_UNABLETOUPDATEIMTIP = "unable to add IMT IP";
	final String ERROR_IPADDRESS_EXISTS = "The IP Address provided already exists in the system.";

	private String accountUuid, imtIpAddress;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;

	private ClientIpaddressDAO clientIpDAO;
	private CacheManager cacheManager;
	private HttpSession session;

	private Cache clientIpCache;

	private String alluuid = "";

	private String transactioinid = "";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		clientIpDAO = ClientIpaddressDAO.getInstance();

		clientIpCache = mgr.getCache(CacheVariables.CACHE_IPADDRESS_BY_UUID);

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_ADD_IMTIP_PARAMETERS, paramHash);

		// No IMTip provided
		if (StringUtils.isBlank(imtIpAddress)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_IMTIP_ERROR_KEY, ERROR_NO_IMTIP);
		} else if (existsIpaddress(imtIpAddress)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_IMTIP_ERROR_KEY, ERROR_IPADDRESS_EXISTS);

		} else if (!addIMTaddress()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_IMTIP_ERROR_KEY, ERROR_NO_UNABLETOUPDATEIMTIP);

		} else {

			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_ADD_IMTIP_SUCCESS_KEY, "s");

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_ADD_IMTIP_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_ADD_IMTIP_ERROR_KEY, null);

		}

		response.sendRedirect("addSessionIp.jsp");

	}

	/**
	 *
	 */
	private boolean addIMTaddress() {

		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		Account account = new Account();

		account.setUuid(accountUuid);
		/**
		 * ClientIP clientip = clientIpDAO.getIpaddress(account);
		 * 
		 * if (clientip != null) {
		 * 
		 * alluuid = clientip.getUuid(); } else { alluuid = transactioinid; }
		 **/
		alluuid = transactioinid;

		ClientIP clientip2 = new ClientIP();

		clientip2.setUuid(alluuid);
		clientip2.setAccountUuid(accountUuid);
		clientip2.setIpAddress(imtIpAddress);

		boolean response = clientIpDAO.updateClientIp(alluuid, clientip2);

		clientIpCache.put(new Element(clientip2.getUuid(), clientip2));

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		accountUuid = StringUtils.trimToEmpty(request.getParameter("accountUuid"));
		imtIpAddress = StringUtils.trimToEmpty(request.getParameter("imtIpAddress"));

	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		paramHash = new HashMap<>();

		paramHash.put("imtIpAddress", imtIpAddress);

	}

	/**
	 *
	 * @param name
	 * @return whether or not the unique name exists in the system
	 */
	private boolean existsIpaddress(final String imtIpAddress) {
		boolean exists = false;

		if (clientIpDAO.getClientIpaddress(imtIpAddress) != null) {
			exists = true;
		}

		return exists;
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		// response.sendRedirect("../index.jsp");
	}

}

package com.impalapay.airtel.servlet.admin.accounts;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.mno.persistence.geolocation.CountryDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add/update forex.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Nov 24, 2014
 *
 * @author <a href="mailto:mike@impalapay.com">Michael Wakahe</a>
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 *
 */
public class AddCountryInfo extends HttpServlet {
	final String ERROR_NO_COUNTRYCODE = "Please provide the country Code";
	final String ERROR_NO_MOBILESPLIT = "Please provide the Split lenght for phone numbers";
	final String ERROR_NO_COUNTRCURRENCYCODE = "Please provide the country currency code";
	final String ERROR_NO_COUNTRYCURRENCY = "Please provide the country Currency name";
	final String ERROR_NO_COUNTRYNAME = "please provide the country Name";
	final String ERROR_NO_UNABLETOUPDATECOUNTRY = "unable to update country";

	private String countryname, countrycode, currencyname, currencycode, countryUuid, mobilesplitlength;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;

	private CountryDAO countryDAO;
	private CacheManager cacheManager;
	private HttpSession session;

	private Cache countryCache;

	private String alluuid = "";

	private String transactioinid = "";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		countryDAO = CountryDAO.getInstance();
		// cacheManager = CacheManager.getInstance();
		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_PARAMETERS, paramHash);

		// No country remitip provided
		if (StringUtils.isBlank(countryname)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_ERROR_KEY, ERROR_NO_COUNTRYNAME);

			// No country verifyip provided
		} else if (StringUtils.isBlank(countrycode)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_ERROR_KEY, ERROR_NO_COUNTRYCODE);

			// No country balanceip provided
		} else if (StringUtils.isBlank(currencyname)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_ERROR_KEY, ERROR_NO_COUNTRYCURRENCY);

			// No country username provided
		} else if (StringUtils.isBlank(currencycode)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_ERROR_KEY, ERROR_NO_COUNTRCURRENCYCODE);
			// No country username provided
		} else if (StringUtils.isBlank(mobilesplitlength)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_ERROR_KEY, ERROR_NO_MOBILESPLIT);

		} else if (!addCountryUpdate()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_ERROR_KEY,
					ERROR_NO_UNABLETOUPDATECOUNTRY + countryUuid);

		} else {

			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_SUCCESS_KEY, "s");

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_ERROR_KEY, null);

		}

		response.sendRedirect("addCountry.jsp");

	}

	/**
	 *
	 */
	private boolean addCountryUpdate() {

		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		Country country = new Country();

		Country country2 = countryDAO.getCountry(countryUuid);

		if (country2 != null) {

			alluuid = country2.getUuid();
		} else {
			alluuid = transactioinid;
		}

		country.setUuid(alluuid);
		country.setName(countryname);
		country.setCurrencycode(currencycode);
		country.setCountrycode(countrycode);
		country.setCurrency(currencyname);
		country.setMobilesplitlength(Integer.parseInt(mobilesplitlength));

		boolean response = countryDAO.updateCountry(alluuid, country);

		countryCache.put(new Element(country.getUuid(), country));

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		countryname = StringUtils.trimToEmpty(request.getParameter("countryname"));
		countryUuid = StringUtils.trimToEmpty(request.getParameter("countryUuid"));
		currencyname = StringUtils.trimToEmpty(request.getParameter("currencyname"));
		currencycode = StringUtils.trimToEmpty(request.getParameter("currencycode"));
		countrycode = StringUtils.trimToEmpty(request.getParameter("countrycode"));
		mobilesplitlength = StringUtils.trimToEmpty(request.getParameter("mobilesplitlength"));

	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		paramHash = new HashMap<>();

		paramHash.put("countryname", countryname);
		paramHash.put("countryUuid", countryUuid);
		paramHash.put("currencyname", currencyname);
		paramHash.put("currencycode", currencycode);
		paramHash.put("countrycode", countrycode);
		paramHash.put("countryUuid", countryUuid);
		paramHash.put("mobilesplitlength", mobilesplitlength);

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		// response.sendRedirect("../index.jsp");
	}
}

/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */

package com.impalapay.airtel.servlet.admin.accounts;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.beans.clientipaddress.ClientIP;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.clientipaddress.ClientIpaddressDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;

import org.apache.log4j.Logger;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add an msisdn to a specific country.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 11, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class DeleteClientIpAddress extends HttpServlet {

	final String ERROR_NO_IP = "Please provide a value for the IP ";

	final String ERROR_UNABLE_DELETE = "Unable to Delete the Clients IP";

	final String SUCCESS_DELETE = "Client Ip succesfully Deleted";
	// These represent form parameters
	private String ClientIp = "";

	private Cache clientIpCache;

	private ClientIpaddressDAO clientIpDAO;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;

	private Logger logger;

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		clientIpDAO = ClientIpaddressDAO.getInstance();

		clientIpCache = mgr.getCache(CacheVariables.CACHE_IPADDRESS_BY_UUID);

		logger = Logger.getLogger(this.getClass());
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_DELETE_IMTIP_PARAMETERS, paramHash);

		if (StringUtils.isBlank(ClientIp)) {
			session.setAttribute(SessionConstants.ADMIN_DELETE_IMTIP_ERROR_KEY, ERROR_NO_IP);

		} else if (!deleteClientIP()) {
			session.setAttribute(SessionConstants.ADMIN_DELETE_IMTIP_ERROR_KEY, ERROR_UNABLE_DELETE);

		} else {
			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_DELETE_IMTIP_SUCCESS_KEY, SUCCESS_DELETE);

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_DELETE_IMTIP_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_DELETE_IMTIP_ERROR_KEY, null);

		}

		response.sendRedirect("addSessionIp.jsp");

		// purchasesCache.put(new
		// Element(CacheVariables.CACHE_PURCHASEPERCOUNTRY_KEY,
		// accountPurchaseDAO.getAllClientPurchasesByCountry()));
	}

	/**
	 * Add amount added to each country float.
	 * 
	 * @return boolean indicating if addition has been added or not.
	 */
	private boolean deleteClientIP() {

		ClientIP clientip = clientIpDAO.getClientIpaddress(ClientIp);

		boolean response = clientIpDAO.DeleteClientIp(ClientIp);

		// clear cache or the removed element.
		// clientIpCache.put(new Element(clientip2.getUuid(), clientip2));
		clientIpCache.remove(clientip.getUuid());

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		ClientIp = StringUtils.trimToEmpty(request.getParameter("imtIpAddress"));

	}

	/**
	 * Place some of the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		paramHash = new HashMap<>();

		paramHash.remove("imtIpAddress", ClientIp);

	}

}

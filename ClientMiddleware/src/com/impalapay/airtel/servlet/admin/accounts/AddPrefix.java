package com.impalapay.airtel.servlet.admin.accounts;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.geolocation.CountryMsisdn;
import com.impalapay.airtel.persistence.geolocation.CountryMsisdnDAO;
import com.impalapay.beans.network.Network;
import com.impalapay.beans.prefix.PrefixSplit;
import com.impalapay.mno.persistence.network.NetworkDAO;
import com.impalapay.mno.persistence.prefix.PrefixDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add an msisdn to a specific country.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 11, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class AddPrefix extends HttpServlet {

	final String ERROR_NO_NETWORK = "Please provide a value for the Network ";
	final String ERROR_NO_COUNTRY = "Please provide a value for the Country .";
	final String ERROR_NO_PREFIX = "Please provide a value for the Prefix .";
	final String ERROR_NO_WALLET = "Please provide a value for the Wallet Type .";
	// final String ERROR_NO_SPLIT = "Please provide a value for the Split legth
	// .";
	final String ERROR_INVALID_SPLIT = "Please provide a Valid  Split legth type .";
	final String ERROR_UNABLE_ADD = "Unable to add the prefix split";
	// These represent form parameters
	private String networkUuid, countryUuid, prefix, wallettype;
	private String addDay, addMonth, addYear;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> PrefixparamHash;

	private Cache prefixCache;
	private PrefixDAO prefixDAO;
	private NetworkDAO networkDAO;

	private Logger logger;

	private String alluuid = "";

	private String transactioinid = "";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		prefixCache = mgr.getCache(CacheVariables.CACHE_PREFIX_BY_UUID);

		prefixDAO = PrefixDAO.getInstance();
		networkDAO = NetworkDAO.getInstance();
		logger = Logger.getLogger(this.getClass());
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_PARAMETERS, PrefixparamHash);

		if (StringUtils.isBlank(networkUuid)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_ERROR_KEY, ERROR_NO_NETWORK);
			/**
			 * } else if (StringUtils.isBlank(countryUuid)) {
			 * session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_ERROR_KEY,
			 * ERROR_NO_COUNTRY);
			 **/

		} else if (StringUtils.isBlank(wallettype)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_ERROR_KEY, ERROR_NO_WALLET);
			/**
			 * } else if (!StringUtils.isNumeric(splitlength)) {
			 * session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_ERROR_KEY,
			 * ERROR_INVALID_SPLIT);
			 **/

		} else if (StringUtils.isBlank(prefix)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_ERROR_KEY, ERROR_NO_PREFIX);

		} else if (!addSplitPrefix()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_ERROR_KEY, ERROR_UNABLE_ADD);

		} else {
			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_SUCCESS_KEY, "s");

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_ERROR_KEY, null);

		}

		response.sendRedirect("addmnoprefix.jsp");

		// purchasesCache.put(new
		// Element(CacheVariables.CACHE_PURCHASEPERCOUNTRY_KEY,
		// accountPurchaseDAO.getAllClientPurchasesByCountry()));
	}

	/**
	 * Add amount added to each country float.
	 * 
	 * @return boolean indicating if addition has been added or not.
	 */
	private boolean addSplitPrefix() {

		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		Network retrievecountry = networkDAO.getNetwork(networkUuid);
		
		countryUuid = retrievecountry.getCountryUuid();
		
		
		//INITIAL CHECK
		PrefixSplit initialcheck = prefixDAO.getPrefixSplits1(prefix, wallettype);
		
		
		if(initialcheck !=null) {
			
			if(!initialcheck.getNetworkUuid().equalsIgnoreCase(networkUuid)) {
				
				//MEANS A DUPLICATE WITH SAME PREFIX WILL BE CREATED
				//STOP TRANSACTION
				
				return false;
			}
			
		}

		Network network = new Network();

		network.setUuid(networkUuid);

		Country country = new Country();

		country.setUuid(countryUuid);

		//PrefixSplit prefixsplit = prefixDAO.getprefixsplit(network, country, prefix);
		
		PrefixSplit prefixsplit = prefixDAO.getprefixsplit(network, country, prefix, wallettype);

		if (prefixsplit != null) {

			alluuid = prefixsplit.getUuid();
		} else {
			alluuid = transactioinid;
		}

		PrefixSplit p = new PrefixSplit();

		p.setNetworkUuid(networkUuid);
		;
		p.setCountryUuid(countryUuid);
		p.setPrefix(prefix);
		// p.setSplitLength(Integer.parseInt(splitlength));
		p.setWalletType(wallettype);
		Calendar c = Calendar.getInstance();
		c.set(NumberUtils.toInt(addYear), NumberUtils.toInt(addMonth) - 1, NumberUtils.toInt(addDay));
		p.setDateadded(c.getTime());

		System.out.print(p);

		boolean response = prefixDAO.updatePrefixSplit(alluuid, p);

		prefixCache.put(new Element(p.getUuid(), p));

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		networkUuid = StringUtils.trimToEmpty(request.getParameter("networkUuid"));
		// countryUuid =
		// StringUtils.trimToEmpty(request.getParameter("countryUuid"));
		prefix = StringUtils.trimToEmpty(request.getParameter("prefix"));
		wallettype = StringUtils.trimToEmpty(request.getParameter("wallettype"));
		// splitlength =
		// StringUtils.trimToEmpty(request.getParameter("splitlength"));
		addDay = StringUtils.trimToEmpty(request.getParameter("addDay"));
		addMonth = StringUtils.trimToEmpty(request.getParameter("addMonth"));
		addYear = StringUtils.trimToEmpty(request.getParameter("addYear"));

	}

	/**
	 * Place some of the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		PrefixparamHash = new HashMap<>();

		PrefixparamHash.put("networkUuid", networkUuid);
		PrefixparamHash.put("countryUuid", countryUuid);
		PrefixparamHash.put("prefix", prefix);
		PrefixparamHash.put("wallettype", wallettype);
		// PrefixparamHash.put("splitlength", splitlength);
	}

}

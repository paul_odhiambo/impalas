package com.impalapay.airtel.servlet.admin.adjuststatus;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionUpdateStatus;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.accountmgmt.ManageAccountDAO;
import com.impalapay.airtel.persistence.accountmgmt.inprogressbalance.InProgressBalanceDAO;
import com.impalapay.airtel.persistence.systemlog.SystemLogDAO;
import com.impalapay.airtel.persistence.transaction.TransactionDAO;
import com.impalapay.airtel.persistence.transaction.TransactionStatusDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionUpdateStatusDAO;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.mno.beans.accountmgmt.balance.ClientAccountBalanceByCountry;
import com.impalapay.mno.beans.accountmgmt.balance.MasterAccountBalance;
import com.impalapay.mno.persistence.accountmgmt.balance.AccountBalanceDAO;
import com.impalapay.mno.servlet.api.remit.AsyncTransactionDispatcher;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add/update forex.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Nov 24, 2014
 *
 * @author <a href="mailto:mike@impalapay.com">Michael Wakahe</a>
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 *
 */
public class ApproveChangeTransactionStatus extends HttpServlet {

	final String ERROR_NO_CHECKERCHNAGESTATUSUUID = "No checker change transactionstatus UUID";
	final String ERROR_NO_USERNAME = "No username provided";
	final String ERROR_NOTEFECTED = "This change is currently not allowed for the selcted status update";
	final String ERROR_FAIL = "Operation failed";
	final String ERROR_UNABLE_UPDATE = "unable to update Change .";
	final String ERROR_NORECORDS = "No records found on the updatetransaction table";
	final String ERROR_NOT_ALLOWED = "You dont have access rights to perform this Action,Please Contact System Administrator";
	final String SUCCESS = "You have successfully approved the transaction status change request";
	private String statusuuid, currentstatus, username, updatestatus, jsonResult;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;
	private Map<String, String> expected = new HashMap<>();
	private HashMap<String, String> transactionStatusHash = new HashMap<>();
	private HashMap<String, String> currencyCode = new HashMap<>();
	private Map<String, Double> balancemap = new HashMap<>();
	private SystemLogDAO systemlogDAO;
	private ManageAccountDAO managementaccountDAO;
	private TransactionUpdateStatus checkertransactionupdatestatus;
	private TransactionUpdateStatusDAO transactionUpdateStatusDAO;
	private TransactionForexDAO transactionforexDAO;
	private InProgressBalanceDAO inprogressbalanaceDAO;
	private AccountBalanceDAO accountbalanceDAO;
	private Transaction transaction;
	private TransactionStatus updatetransactionstatusobject;
	private TransactionDAO transactionDAO;
	private TransactionStatusDAO transactionstatusDAO;
	private boolean response;
	private JsonElement root2 = null;
	private Account account = null;
	private CacheManager cacheManager;
	private Cache transactionStatusCache, countryCache;
	private HttpSession session;
	private String transactioinid = "", success = "S000", unknownerror = "00032", inprogress = "S001",
			failedtransaction = "00029", reversedtransaction = "R000", statusdescription = "", statuscode = "",
			recipientcurrency = "", originatecurrency = "";
	private double imtmasterbalance = 0, amount = 0, countryamount = 0, convertedamount = 0, exchangerate = 0;
	private List<ClientAccountBalanceByCountry> clientBalances;

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		// emailValidator = EmailValidator.getInstance();
		transactionUpdateStatusDAO = TransactionUpdateStatusDAO.getinstance();
		transactionDAO = TransactionDAO.getInstance();
		managementaccountDAO = ManageAccountDAO.getInstance();
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
		transactionforexDAO = TransactionForexDAO.getinstance();
		transactionstatusDAO = TransactionStatusDAO.getInstance();
		inprogressbalanaceDAO = InProgressBalanceDAO.getInstance();
		accountbalanceDAO = AccountBalanceDAO.getInstance();
		cacheManager = CacheManager.getInstance();
		systemlogDAO = SystemLogDAO.getInstance();

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_PARAMETERS, paramHash);

		// No First currency provided
		if (StringUtils.isBlank(statusuuid)) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY,
					ERROR_NO_CHECKERCHNAGESTATUSUUID);

			// No Base rate provided
		} else if (StringUtils.isBlank(username)) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, ERROR_NO_USERNAME);

		} else if (!addforex()) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, ERROR_NOT_ALLOWED);

		} else {

			String receivedresponse = UpdateTransactionStatus();

			root2 = new JsonParser().parse(receivedresponse);
			statuscode = root2.getAsJsonObject().get("status_code").getAsString();
			statusdescription = root2.getAsJsonObject().get("status_description").getAsString();

			if (statuscode.equalsIgnoreCase(success)) {

				// If we get this far then all parameter checks are ok.
				session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_SUCCESS_KEY, "Successfull");

				// Reduce our session data
				session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_PARAMETERS, null);
				session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, null);

			} else {
				session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, statusdescription);
			}

		}

		// response.sendRedirect("addAccount.jsp");a
		response.sendRedirect("changestatus.jsp");

	}

	/**
	 *
	 */
	private String UpdateTransactionStatus() {
		Gson g = new Gson();

		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		Element element;
		List keys;

		TransactionStatus status;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status.getUuid(), status.getStatus());
		}

		// fetch from cache
		Country country;
		keys = countryCache.getKeys();
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			currencyCode.put(country.getUuid(), country.getCurrencycode());
		}

		try {

			checkertransactionupdatestatus = transactionUpdateStatusDAO.getTransactionUpdateStatusUuid(statusuuid);
			// Fetch from transaction table. by uuid
			transaction = transactionDAO.getTransactionbyuuid(checkertransactionupdatestatus.getTransactionUuid());

			updatestatus = transactionStatusHash.get(checkertransactionupdatestatus.getUpdatestatus());

			updatetransactionstatusobject = transactionstatusDAO
					.getTransactionStatus(checkertransactionupdatestatus.getUpdatestatus());

			currentstatus = transactionStatusHash.get(checkertransactionupdatestatus.getCurrentstatus());

			recipientcurrency = checkertransactionupdatestatus.getRecipientcurrency();

			originatecurrency = checkertransactionupdatestatus.getSourcecurrency();

			amount = checkertransactionupdatestatus.getLocalamount();

			convertedamount = checkertransactionupdatestatus.getConvertedamount();

			exchangerate = checkertransactionupdatestatus.getImpalarate();

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("status_description", "VERY SERIOUS ERROR WHEN CHECKING RECORD " + statusuuid);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		if (transaction == null) {
			expected.put("status_code", "00032");
			expected.put("status_description", ERROR_NORECORDS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// set up an account object
		account = new Account();
		account.setUuid(transaction.getAccountUuid());

		// fetch the transaction details from the table (because we have to start async
		// transaction steps)
		MasterAccountBalance masterbalance = accountbalanceDAO.getMasterAccountBalance(account,
				currencyCode.get(originatecurrency));

		try {
			imtmasterbalance = masterbalance.getBalance();
			// #######################################################
			// Introduce a check for Master Balance to prevent one from
			// sending what he/She doesnt have
			// #######################################################

			if (imtmasterbalance <= amount) {

				expected.put("status_code", "00032");
				expected.put("status_description", APIConstants.COMMANDSTATUS_LESS_MASTERBALANCE);
				jsonResult = g.toJson(expected);

				return jsonResult;
			}

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_NO_MASTERBALANCE);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		// Check for the Balance by country
		// ##################################################
		// for a float based system the below is needed
		// ##################################################

		// fetch the list containing balance by country with the respective
		// balances
		try {
			clientBalances = accountbalanceDAO.getClientBalanceByCountry(account);

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_NO_BALANCE_COUNTRIES);
			jsonResult = g.toJson(expected);
		}

		for (ClientAccountBalanceByCountry balance : clientBalances) {
			balancemap.put(balance.getCountryUuid(), balance.getBalance());
		}
		try {

			countryamount = balancemap.get(recipientcurrency);

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_NO_BALANCE);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		if (countryamount <= convertedamount) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.STATUS_CODE_INSUFFICIENT_BALANCE);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check current and update status

		// 1st Query current status of transaction is in progress and the Update is
		// failed transaction.
		// involves returning of balance from hold account.
		// involves checking transaction forex table
		if (currentstatus.equalsIgnoreCase(inprogress)) {

			if (updatestatus.equalsIgnoreCase(failedtransaction)) {

				if (!transactionforexDAO.deleteTransactionForex(statusuuid)
						&& !inprogressbalanaceDAO.removeBalanceHoldFail(statusuuid)) {
					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_FAIL);
					jsonResult = g.toJson(expected);

				} else {
					// means everything went according to plan and all the money has been returned
					// back
					// update the transaction status on the table
					transactionDAO.updateTransactionStatus(transaction.getUuid(), updatetransactionstatusobject);

					// forward to transactionupdatestatushistory
					transactionUpdateStatusDAO.addTransactionUpdateStatusHistory(checkertransactionupdatestatus);
					// remove from transactionupdatestatus
					transactionUpdateStatusDAO.deleteTransactionUpdateStatus(statusuuid);

					expected.put("status_code", "S000");
					expected.put("status_description", SUCCESS);
					jsonResult = g.toJson(expected);
				}
			} else {

				// means the process failed
				expected.put("status_code", "00032");
				expected.put("status_description", ERROR_NOTEFECTED);
				jsonResult = g.toJson(expected);

			}

		} else if (currentstatus.equalsIgnoreCase(unknownerror)) {

			// Start building the transaction forex object.
			TransactionForexrate transactionratehistory = new TransactionForexrate();

			transactionratehistory.setUuid(transactioinid);
			transactionratehistory.setTransactionUuid(statusuuid);
			transactionratehistory.setAccount(transaction.getAccountUuid());
			transactionratehistory.setRecipientcountry(recipientcurrency);
			transactionratehistory.setLocalamount(amount);
			transactionratehistory.setAccounttype(currencyCode.get(originatecurrency));
			transactionratehistory.setConvertedamount(convertedamount);
			transactionratehistory.setImpalarate(exchangerate);
			transactionratehistory.setBaserate(0);
			transactionratehistory.setReceivermsisdn(transaction.getRecipientMobile());
			transactionratehistory.setSurplus(0);
			transactionratehistory.setServerTime(new Date());

			// first instance the update status is transaction inprogress.
			if (updatestatus.equalsIgnoreCase(inprogress)) {

				if (transactionDAO.updateTransactionStatus(transaction.getUuid(), updatetransactionstatusobject)
						&& transactionUpdateStatusDAO.addTransactionUpdateStatusHistory(checkertransactionupdatestatus)
						&& transactionUpdateStatusDAO.deleteTransactionUpdateStatus(statusuuid)) {
					new AsyncTransactionDispatcher(transactionratehistory).start();

					// then remove from the status update table
					// add to the status update history table.
					expected.put("status_code", "S000");
					expected.put("status_description", SUCCESS);
					jsonResult = g.toJson(expected);
				} else {

					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_FAIL);
					jsonResult = g.toJson(expected);
				}

			} else if (updatestatus.equalsIgnoreCase(failedtransaction)) {

				if (transactionDAO.updateTransactionStatus(transaction.getUuid(), updatetransactionstatusobject)
						&& transactionUpdateStatusDAO.addTransactionUpdateStatusHistory(checkertransactionupdatestatus)
						&& transactionUpdateStatusDAO.deleteTransactionUpdateStatus(statusuuid)) {
					expected.put("status_code", "S000");
					expected.put("status_description", SUCCESS);
					jsonResult = g.toJson(expected);
				} else {
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_FAIL);
					jsonResult = g.toJson(expected);
				}

			} else {

				// means the process failed
				System.out.println("This transaction failed to update succesfully");
				expected.put("status_code", "00032");
				expected.put("status_description", ERROR_FAIL);
				jsonResult = g.toJson(expected);

			}

		} else if (currentstatus.equalsIgnoreCase(success)) {

			if (updatestatus.equalsIgnoreCase(reversedtransaction)) {

				if (transactionDAO.updateTransactionStatus(transaction.getUuid(), updatetransactionstatusobject)
						&& transactionUpdateStatusDAO.addTransactionUpdateStatusHistory(checkertransactionupdatestatus)
						&& transactionUpdateStatusDAO.deleteTransactionUpdateStatus(statusuuid)) {

					expected.put("status_code", "S000");
					expected.put("status_description", SUCCESS);
					jsonResult = g.toJson(expected);
				} else {

					// means the process failed
					expected.put("status_code", "00032");
					expected.put("status_description", ERROR_FAIL);
					jsonResult = g.toJson(expected);
				}

			} else {
				// means the process failed
				expected.put("status_code", "00032");
				expected.put("status_description", ERROR_NOTEFECTED);
				jsonResult = g.toJson(expected);

			}

		} else {

			// Not allowed is the current setup
			// means the process failed
			expected.put("status_code", "00032");
			expected.put("status_description", ERROR_NOTEFECTED);
			jsonResult = g.toJson(expected);
		}

		SystemLog systemlog = new SystemLog();

		systemlog.setUsername(username);
		systemlog.setUuid(transactioinid);
		systemlog.setAction(username + " approved the currencypair " + " to spreadrate= " + " the marketrate= ");

		// response = false;
		return jsonResult;
	}

	public boolean addforex() {

		if (StringUtils.equals(username, PropertiesConfig.getConfigValue("ADMIN_USERNAME"))) {
			response = true;
		} else {
			ManagementAccount status = managementaccountDAO.getAccountName(username);

			response = status.isChecker();
		}

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		statusuuid = StringUtils.trimToEmpty(request.getParameter("approve"));
		username = StringUtils.trimToEmpty(request.getParameter("username"));

	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		// response.sendRedirect("../index.jsp");
	}
}

/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */

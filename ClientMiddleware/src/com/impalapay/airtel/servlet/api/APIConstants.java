package com.impalapay.airtel.servlet.api;

/**
 * A list of constants that are used by the Airtel API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 14, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class APIConstants {

	public final static int DEFAULT_MESSAGE_ID = 0;

	// Delimiter for destination phone numbers for a message destined to go to a
	// mobile subscriber
	public final static String MSISDN_DELIMITER = ";", RESPONSE_DELIMITER = ";";

	// Valid values for the parameter "network"
	public final static String[] PARAM_VALID_NETWORKS = { "safaricom", "airtel", "orange" };

	// These must match what is in the database table 'SMSC'.
	// They are put in the order of the networks listed above
	public final static String[] PARAM_NETWORK_UUIDS = { "bf0b6cd7-9552-4429-a0e3-8a6cd9eb5a31",
			"bf0b6cd7-9552-4429-a0e3-8a6cd9eb5a31", "7965cb28-7b54-43b8-97f9-21abac2a06f6" };

	// Command Status Codes
	public final static String COMMANDSTATUS_FAIL = "FAILED_TRANSACTION";
	public final static String COMMANDSTATUS_OK = "OK";
	public final static String COMMANDSTATUS_INVALID_PARAMETERS = "INVALID_PARAMETERS";
	public final static String COMMANDSTATUS_UNKNOWN_USERNAME = "UNKNOWN_USERNAME";
	public final static String COMMANDSTATUS_INVALID_PASSWORD = "INVALID_PASSWORD";
	public final static String STATUS_CODE_ACCOUNT_NOT_ACTIVE = "INACTIVE_ACCOUNT";
	public final static String STATUS_CODE_INVALID_SOURCE = "INVALID_SOURCE";
	public final static String STATUS_CODE_INSUFFICIENT_BALANCE = "IMT_INSUFFICIENT_COUNTRY_FLOAT";
	public final static String STATUS_CODE_UNKNOWN_MESSAGEID = "UNKNOWN_MESSAGEID";
	public final static String STATUS_CODE_ACCEPTED = "ACCEPTED";
	public final static String STATUS_CODE_NO_BALANCE = "MISSING_COUNTRY_VIRTUAL_FLOAT";
	public final static String COMMANDSTATUS_INVALIDEMPTY_PARAMETERS = "EMPTY_PARAMETERS";

	// added status code
	public final static String COMMANDSTATUS_INVALID_SESSIONID = "INVALID_SESSION_ID";
	public final static String COMMANDSTATUS_INVALID_REFERENCENUMBER = "INVALID_REFERENCE_NUMBER";
	public final static String COMMANDSTATUS_INVALID_CURRENCYCODE = "CURRENCY_UNAVAILABLE";
	public final static String COMMANDSTATUS_INVALID_COUNTRYCODE = "COUNTRYCODE_UNAVAILABLE";
	public final static String COMMANDSTATUS_CURRENCY_COUNTRYMISMATCH = "PROVIDED_RECIPIENT_CURRENCY_AND_RECIPIENTCOUNTRY_MISMATCH";
	public final static String COMMANDSTATUS_REMIT_SUCCESS = "SUCCESS";
	public final static String COMMANDSTATUS_DUPLICATE_REFERENCE = "DUPLICATE_REFERENCE_NUMBER";
	public final static String COMMANDSTATUS_INVALID_DATE = "INVALID_CLIENT_DATE";
	public final static String COMMANDSTATUS_UNOPERATIONAL_COUNTRY = "COUNTRY_NON_OPERATIONAL";
	public final static String COMMANDSTATUS_NO_MSISDN_NETWORK_MATCH = "NO_NETWORK_MATCH_FOR_PROVIDED_MSISDN";
	public final static String COMMANDSTATUS_INVALID_REMIT_NUMBER = "COUNTRY_REMIT_MSISDN_ERROR";
	public final static String COMMANDSTATUS_INSUFFICIENT_MASTERBALANCE = "INSUFFICIENT_MASTERBALANCE";
	public final static String COMMANDSTATUS_FOREX_ERROR = "FOREX_MODULE_ERROR";
	public final static String COMMANDSTATUS_COUNTRYAUTH_ERROR = "COUNTRY_AUTH_CREDENTIALS_ERROR";
	public final static String COMMANDSTATUS_LOW_MASTERBALANCE = "IMT_BALANCE_BELOW_ALLOWED_LEVEL";
	public final static String COMMANDSTATUS_LOW_BALANCEBYCOUNTRY = "IMT_BALANCE_BELOW_ALLOWED_LEVEL_FOR_THIS_ROUTE";
	public final static String COMMANDSTATUS_LESS_MASTERBALANCE = "IMT_MASTERFLOAT_LESS_THAN_AMOUNTSENT";
	public final static String COMMANDSTATUS_NO_MASTERBALANCE = "IMT_LACK_MASTERFLOAT";
	public final static String COMMANDSTATUS_NO_BALANCE = "IMT_BALANCE_NOT_DEFINED_INCOUNTRY";
	public final static String COMMANDSTATUS_NO_BALANCE_COUNTRIES = "IMT_COUNTRYBALANCE_NOT_DEFINED";
	public final static String COMMANDSTATUS_INTERNAL_ERROR = "RECEIVING_SERVER_ERROR";
	public final static String COMMANDSTATUS_ASYNC_ERROR = "ASYNCHRONOUS_NOT_ALLOWED";
	public final static String COMMANDSTATUS_BANKCODE_ERROR = "BANKCODE_UNAVAILABLE";
	public final static String COMMANDSTATUS_BANKCODE_COUNTRYMISMATCH_ERROR = "PROVIDED_BANKCODE_AND_RECIPIENTCOUNTRY_MISMATCH";
	public final static String COMMANDSTATUS_BILLPAYMENTCODE_ERROR = "BILLPAYMENTCODE_UNAVAILABLE";
	public final static String COMMANDSTATUS_BILLPAYMENTCODE_COUNTRYMISMATCH_ERROR = "PROVIDED_BILLPAYMENTCODE_AND_RECIPIENTCOUNTRY_MISMATCH";
	public final static String COMMANDSTATUS_CANCELED_TRANSACTION = "CANCELED_TRANSACTION";
	public final static String COMMANDSTATUS_ROUTEDEFINE_ERROR = "ROUTE_NOT_YET_DEFINED";
	public final static String COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS = "RECEIVER_SERVER_ERROR";
	public final static String COMMANDSTATUS_MOBILE_PREFIX_MISMATCH = "INVALID_MOBILE_NUMBER";
	public final static String COMMANDSTATUS_RECEIVER_ENDPOINT_AUTHENTICATE_ERROR = "RECEIVER_AUTHENTICATION_ERROR";
	public final static String COMMANDSTATUS_COUNTRY_NO_NETWORK_ERROR = "COUNTRY_NETWORK_MISMATCH_NUMBER_BELONGS_TO_";
	public final static String COMMANDSTATUS_VENDORUNIQUE_ERROR = "VENDOR_UNIQUE_OBJECT_MANDATORY";
	public final static String COMMANDSTATUS_VENDORUNIQUE_PARAMETERS_ERROR = "VENDOR_UNIQUE_PARAMETER_MISSING";

	// rate limiting commands.
	public final static String COMMANDSTATUS_SESSION_LIMIT_ERROR = "TOO_MANY_SESSIONREQUEST_WAIT_FOR_";

	// Topup Status Code UUIDs
	public final static String UUID_INVALID_PARAMETERS = "857d1e0b-df0d-487c-b7bb-6e9d8c3eb212";
	public final static String UUID_UNKNOWN_USERNAME = "4f3d09eb-10e0-428a-b8c1-38f476a4ab31";
	public final static String UUID_WRONG_PASSWORD = "9454a86a-c1ac-410c-882e-d9a6e9048c81";
	public final static String UUID_ACCEPTED_FOR_DELIVERY = "3b6edb35-654d-4049-b7ea-0f1db29c6e77";
	public final static String UUID_ACCOUNT_SUSPENDED = "397554ce-6935-4699-8b99-727574b6a49f";
	public final static String UUID_INSUFFICIENT_FUNDS = "ea430328-ef8b-4459-95c6-7102ba16ddc5";
	public final static String UUID_TOPUP_SUCESS = "4a991e99-ffa2-4fcd-91ed-27e6ce078832";
	public final static String UUID_TOPUP_FAILURE = "796e797d-6721-4f5e-938f-241b4b4c5f6c";
	public final static String UUID_INVALIDEMPTY_PARAMETERS = "6721-4f5e-938f-ef8b-4459c5f6c";
	// added status code UUIDs
	public final static String UUID_INVALID_SESSIONID = "20015d61-0c54-4c52-8f8a-0eeae04889b5";
	public final static String UUID_INVALID_REFERENCENUMBER = "2340c8e1-8e45-4465-953d-cb70aa7724aa";
	public final static String UUID_INVALID_CURRENCYCODE = "42057feb-09dd-430f-954a-9df0c4d2555a";
	public final static String UUID_INVALID_COUNTRYCODE = "3bc49c18-70f9-42b6-8d5c-f0948c7bca24";
	public final static String UUID_CURRENCY_COUNTRYMISMATCH = "0273bcf7-bafb-4026-84f2-fc8b5f1a9527";
	public final static String UUID_COMMANDSTATUS_REMIT_SUCCESS = "7fadf886ef044a14bacdbdf38ffff4c8";
	public final static String UUID_DUPLICATE_REFERENCE = "a8a2bd716d4f4f1ca9f733c94cc88053";
	public final static String UUID_INVALID_DATE = "692846bdb8c64fc0beb39ad95f4539f4";
	public final static String UUID_UNOPERATIONAL_COUNTRY = "71a5f69214b8409ea8605d2ca5a42317";
	public final static String UUID_ASYNC_ERROR = "a500efbf-391a-4765-9091-c5e0f73cd077";
	public final static String UUID_BANKCODE_ERROR = "18-70f9-42b6-8d5c-f0948c7bca24-u786h";
	public final static String UUID_NO_BALANCE = "70f9-2b6-d5c-f0948c7bca24-u78";
	public final static String UUID_NO_BALANCE_COUNTRIES = "9091-c5e0f73c45b0cf72-658d";
	public final static String UUID_CANCELED_TRANSACTION = "6935-4699-8b99-f0948c7bca24-u78";
	public final static String UUID_NO_MASTERBALANCE = "45b0cf72-658d-43db-938f-3ef638c4b9fc";
	public final static String UUID_ROUTEDEFINE = "43db-938f-3ef638c4b9fc-45b0cf72-658d-";
	public final static String UUID_RECEIVER_SERVER_ERROR_PARAMETERS = "ffa2-4fcd-91ed-27e6ce078832-u786h";
	public final static String UUID_MOBILE_PREFIX_MISMATCH = "91ed-ef638c4b9fc-45b0cf72-658d";
	public final static String UUID_RECEIVER_ENDPOINT_AUTHENTICATE_ERROR = "970f9-42b6-8d5c-f0948c7bca24-u786h";
	public final static String UUID_COUNTRY_NO_NETWORK_ERROR = "u786h-8d5c-f0948c7bca24-u786h";
	public final static String UUID_NO_MSISDN_NETWORK_MATCH = "91ed-27e6ceu786h-8d5cu786h";
	public final static String UUID_VENDORUNIQUE_ERROR = "91ed-27e6ce078832u786h-8d5c";
	public final static String UUID_VENDORUNIQUE_PARAMETERS_ERROR = "4fc0beb39ad95f4539f4938f-3ef638c4b9fc";

	// worldremit status code
	public final static String COMMANDSTATUS_INVALID_IPADDRESS = "IP_ADDRESS_NOT_ALLOWED";
	public final static String COMMANDSTATUS_IPADDRESS_MISMATCH = "PROVIDED_IP_ADDRESS_AND_ACCOUNT_MISMATCH";

	// worldremit status code UUIDs
	public final static String UUID_INVALID_IPADDRESS = "fe9dfbe0b7894fcda9a06dcabc8089eb";
	public final static String UUID_IPADDRESS_MISMATCH = "06dcabc8089eb94fcda9a06dcabc8089eb";

	// COLLECTIONROUTESTATUSCODES
	public final static String COMMANDSTATUS_INVALID_COLLECTIONAME = "INVALID_COLLECTION_NAME";
	public final static String COMMANDSTATUS_DIRECTDEBIT_NOTSUPPORTED = "DIRECT_DEBIT_NOT_SUPPORTED";

}

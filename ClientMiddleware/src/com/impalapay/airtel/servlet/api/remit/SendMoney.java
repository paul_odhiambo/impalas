package com.impalapay.airtel.servlet.api.remit;

import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.accountmgmt.balance.ClientAccountBalanceByCountry;
import com.impalapay.airtel.beans.accountmgmt.balance.MasterAccountBalance;
import com.impalapay.airtel.beans.forex.Forex;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.geolocation.CountryMsisdn;
import com.impalapay.airtel.beans.sessionlog.SessionLog;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.persistence.accountmgmt.balance.AccountBalanceDAO;
import com.impalapay.airtel.persistence.forex.GbpForexDAO;
import com.impalapay.airtel.persistence.forex.UsdForexDAO;
import com.impalapay.airtel.persistence.geolocation.CountryMsisdnDAO;
import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;
import com.impalapay.airtel.persistence.transaction.TransactionDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.util.SecurityUtil;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
import com.impalapay.airtel.util.CurrencyConvertUtil;
//import com.impalapay.airtel.util.net.PostMinusThread;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * Allows for sending through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class SendMoney extends HttpServlet {

	private PostWithIgnoreSSL postMinusThread;

	private TransactionDAO transactionDAO;

	private Cache accountsCache, countryCache, transactionStatusCache, usdforexCache, gbpforexCache;

	private SessionLogDAO sessionlogDAO;

	private AccountBalanceDAO accountbalanceDAO;

	private CountryMsisdnDAO countryMsisdnDAO;

	private TransactionForexDAO transactionforexDAO;

	private UsdForexDAO usdForexDAO;

	private GbpForexDAO gbpForexDAO;

	private HashMap<String, String> countryHash = new HashMap<>();

	private HashMap<String, String> countryCode = new HashMap<>();

	private HashMap<String, String> countryUuid = new HashMap<>();

	private HashMap<String, String> countryIp = new HashMap<>();

	private HashMap<String, String> countryUsername = new HashMap<>();

	private HashMap<String, String> countryPassword = new HashMap<>();

	private Map<String, String> toairtel = new HashMap<>();

	private HashMap<String, String> transactionStatusHash = new HashMap<>();

	private HashMap<String, String> statusDescriptionHash = new HashMap<>();

	private HashMap<String, Double> usdForexmap = new HashMap<>();

	private HashMap<String, Double> gbpForexmap = new HashMap<>();

	private HashMap<String, Double> usdForexbasemap = new HashMap<>();

	private HashMap<String, Double> gbpForexbasemap = new HashMap<>();

	private String CLIENT_URL = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		transactionDAO = TransactionDAO.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);

		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);

		usdforexCache = mgr.getCache(CacheVariables.CACHE_USDFOREX_BY_UUID);

		gbpforexCache = mgr.getCache(CacheVariables.CACHE_GBPFOREX_BY_UUID);

		sessionlogDAO = SessionLogDAO.getInstance();

		accountbalanceDAO = AccountBalanceDAO.getInstance();

		countryMsisdnDAO = CountryMsisdnDAO.getInstance();

		transactionforexDAO = TransactionForexDAO.getinstance();

		usdForexDAO = UsdForexDAO.getinstance();

		gbpForexDAO = GbpForexDAO.getinstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(sendMoney(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return JSon response
	 * @throws IOException
	 */
	private String sendMoney(HttpServletRequest request) throws IOException {
		Account account = null;

		double impalaexchangecalculate = 0;

		double baseexchange = 0;

		double convertedamountToWallet = 0;

		int finalconvertedamount = 0;

		// joined json string
		String join = "";
		JsonElement root = null;

		// These represent parameters received over the network
		String username = "", sessionid = "", sourcecountrycode = "", sendername = "", recipientmobile = "",
				recipientcurrencycode = "", recipientcountrycode = "", referencenumber = "", clienttime = "",
				unifiedstatusdescription = "", sendertoken = "";

		double amount = 0;

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), " ");

		// ###########################################################
		// instantiate the JSon
		// ##########################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("api_username").getAsString();

			sessionid = root.getAsJsonObject().get("session_id").getAsString();

			sourcecountrycode = root.getAsJsonObject().get("source_country_code").getAsString();

			sendername = root.getAsJsonObject().get("sendername").getAsString();

			recipientmobile = root.getAsJsonObject().get("recipient_mobile").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("recipient_currency_code").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("recipient_country_code").getAsString();

			referencenumber = root.getAsJsonObject().get("reference_number").getAsString();

			sendertoken = root.getAsJsonObject().get("sendertoken").getAsString();

			amount = root.getAsJsonObject().get("amount").getAsDouble();

			clienttime = root.getAsJsonObject().get("client_datetime").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// ####################################################################
		// check for the presence of all required parameters
		// ####################################################################

		if (StringUtils.isBlank(username) || StringUtils.isBlank(sessionid) || StringUtils.isBlank(sourcecountrycode)
				|| StringUtils.isBlank(sendername) || StringUtils.isBlank(recipientmobile)
				|| StringUtils.isBlank(recipientcurrencycode) || StringUtils.isBlank(recipientcountrycode)
				|| StringUtils.isBlank(referencenumber) || StringUtils.isBlank(sendertoken)
				|| StringUtils.isBlank(clienttime) || amount <= 0) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		if (account == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// test for invalid sessionid
		SessionLog sessionlog = sessionlogDAO.getValidSessionLog(account);

		// ################################################################
		// Guard against all invalid sessionid error
		// ################################################################
		if (sessionlog == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		String session = sessionlog.getSessionUuid();

		if (!StringUtils.equals(SecurityUtil.getMD5Hash(sessionid), session)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		List keys;

		// fetch from cache
		Country country;
		keys = countryCache.getKeys();
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryHash.put(country.getCountrycode(), country.getCurrencycode());
		}

		// country and country uuid
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryCode.put(country.getCountrycode(), country.getUuid());
		}
		// country uuid and country code
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryUuid.put(country.getUuid(), country.getCountrycode());
		}

		// country and country ip
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryIp.put(country.getCountrycode(), country.getCountryremitip());
		}

		// country and username
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryUsername.put(country.getCountrycode(), country.getUsername());
		}

		// country and country password
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryPassword.put(country.getCountrycode(), country.getPassword());
		}

		// fetch usd forex from cache.
		Forex forex;
		keys = usdforexCache.getKeys();
		for (Object key : keys) {
			element = usdforexCache.get(key);
			forex = (Forex) element.getObjectValue();
			usdForexmap.put(countryUuid.get(forex.getCountryUuid()), forex.getImpalarate());
		}

		keys = usdforexCache.getKeys();
		for (Object key : keys) {
			element = usdforexCache.get(key);
			forex = (Forex) element.getObjectValue();
			usdForexbasemap.put(countryUuid.get(forex.getCountryUuid()), forex.getBaserate());
		}

		keys = gbpforexCache.getKeys();
		for (Object key : keys) {
			element = gbpforexCache.get(key);
			forex = (Forex) element.getObjectValue();
			gbpForexmap.put(countryUuid.get(forex.getCountryUuid()), forex.getImpalarate());
		}

		keys = gbpforexCache.getKeys();
		for (Object key : keys) {
			element = gbpforexCache.get(key);
			forex = (Forex) element.getObjectValue();
			gbpForexbasemap.put(countryUuid.get(forex.getCountryUuid()), forex.getBaserate());
		}

		// ======================================================
		// Populate with the mapping of Transaction Statuses.
		// The key is a UUID of the status
		// ======================================================

		TransactionStatus status;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status.getStatus(), status.getUuid());
		}

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status = (TransactionStatus) element.getObjectValue();
			statusDescriptionHash.put(status.getStatus(), status.getDescription());
		}

		// checks for the provide currencyCode(invalid)
		if (!countryHash.containsValue(recipientcurrencycode)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_CURRENCYCODE);

			return g.toJson(expected);
		}

		// checks for the provided countryCode(invalid)
		if (!countryHash.containsKey(recipientcountrycode)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_COUNTRYCODE);

			return g.toJson(expected);
		}

		// =========================================================
		// determines if the provided recipient currencyCode doesn't
		// correspond to the countryCode
		// =========================================================

		if (!StringUtils.equalsIgnoreCase(countryHash.get(recipientcountrycode), recipientcurrencycode)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_CURRENCY_COUNTRYMISMATCH);

			return g.toJson(expected);
		}

		// retrieve from countrycode hashmap uuid representing the provided
		// country
		String countrycodetodb = countryCode.get(recipientcountrycode);

		// =============================================================================
		// Test to see if the provided reference number has previously been
		// used.
		// if reference number is a duplicate return duplicate reference number
		// response.
		// =============================================================================

		List referencetest = transactionDAO.getTransactionstatus(referencenumber);

		int size = referencetest.size();

		if (size != 0) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_DUPLICATE_REFERENCE);

			return g.toJson(expected);

		}
		// ###############################################################
		// fetch the countrymsisdn by account then place them in a hashmap
		// ################################################################

		List<CountryMsisdn> countrysourcenumber = countryMsisdnDAO.getCountryMsisdn(account);

		// convert resultant list into a hashmap.
		HashMap<String, String> countryMsisdnmap = new HashMap<>();

		for (CountryMsisdn countrynumber : countrysourcenumber)
			countryMsisdnmap.put(countryUuid.get(countrynumber.getCountryUuid()), countrynumber.getMsisdn());

		String sourcemsisdn = countryMsisdnmap.get(recipientcountrycode);

		if (StringUtils.isEmpty(sourcemsisdn)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_REMIT_NUMBER);

			return g.toJson(expected);
		}

		String apipassword = countryPassword.get(recipientcountrycode);

		String apiusername = countryUsername.get(recipientcountrycode);

		String accounttype = account.getAccounttype();

		if (StringUtils.isEmpty(apipassword) || StringUtils.isEmpty(apiusername)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_COUNTRYAUTH_ERROR);

			return g.toJson(expected);
		}

		// ###########################################################
		// fetch forex Module
		// ###########################################################

		// perform forex validation checks

		if (accounttype.equalsIgnoreCase("USD")) {
			if (!usdForexmap.containsKey(recipientcountrycode)) {
				expected.put("command_status", APIConstants.COMMANDSTATUS_FOREX_ERROR);
				return g.toJson(expected);
			}

			double impalausdrate = usdForexmap.get(recipientcountrycode);

			double baseusdrate = usdForexbasemap.get(recipientcountrycode);

			impalaexchangecalculate = impalausdrate;
			baseexchange = baseusdrate;
			// multiply the received amount from IMT with the country rate to
			// get the amount to be sent to wallet
			convertedamountToWallet = CurrencyConvertUtil.multiplyForex(amount, impalaexchangecalculate);

		}
		if (accounttype.equalsIgnoreCase("GBP")) {
			if (!gbpForexmap.containsKey(recipientcountrycode)) {
				expected.put("command_status", APIConstants.COMMANDSTATUS_FOREX_ERROR);
				return g.toJson(expected);
			}

			double impalagbprate = gbpForexmap.get(recipientcountrycode);

			double basegbprate = gbpForexbasemap.get(recipientcountrycode);

			impalaexchangecalculate = impalagbprate;
			baseexchange = basegbprate;
			// multiply the received amount from IMT with the country rate to
			// get the amount to be sent to wallet
			convertedamountToWallet = CurrencyConvertUtil.multiplyForex(amount, impalaexchangecalculate);

		}

		// Check master balance.
		MasterAccountBalance masterbalance = accountbalanceDAO.getMasterAccountBalance(account);

		double imtmasterbalance = masterbalance.getBalance();

		if (imtmasterbalance <= 1000) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_LOW_MASTERBALANCE);
			return g.toJson(expected);

		}

		// ##################################################
		// for a float based system the below is needed
		// ##################################################

		// fetch the list containing balance by country with the respective
		// balances
		List<ClientAccountBalanceByCountry> clientBalances = accountbalanceDAO.getClientBalanceByCountry(account);

		// convert the resultant list to a hashmap.
		Map<String, Double> balancemap = new LinkedHashMap<>();

		for (ClientAccountBalanceByCountry balance : clientBalances)
			balancemap.put(countryUuid.get(balance.getCountryUuid()), balance.getBalance());

		double countryamount = balancemap.get(recipientcountrycode);

		if (countryamount <= convertedamountToWallet) {
			expected.put("command_status", APIConstants.STATUS_CODE_INSUFFICIENT_BALANCE);
			return g.toJson(expected);
		}

		// final converted amount(to be sent to the wallet).
		// finalconvertedamount =
		// CurrencyConvertUtil.round2(convertedamountToWallet, 0);

		finalconvertedamount = CurrencyConvertUtil.doubleToInteger(convertedamountToWallet);

		double remainderamount = CurrencyConvertUtil.subtractDeficit(convertedamountToWallet, finalconvertedamount);

		Country countrys = new Country();
		countrys.setUuid(countrycodetodb);

		// convert amount from double to string
		String amountstring = String.valueOf(finalconvertedamount);

		// generate UUID then save transaction and sending to comviva wallet.
		String transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		// generate UUID then save transaction and sending to
		// transactionhistory.
		String transactionforexhistoryuuid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		// retrieve the countryip to be used as URL
		String countryremitip = countryIp.get(recipientcountrycode);

		// ###############################################
		// information to be sent to wallet.
		// ##############################################
		toairtel.put("username", apiusername);
		toairtel.put("password", apipassword);
		toairtel.put("transaction_id", transactioinid);
		toairtel.put("source_msisdn", sourcemsisdn);
		toairtel.put("beneficiary_msisdn", recipientmobile);
		toairtel.put("Sender_Name", sendername + " via " + username + " from " + sourcecountrycode);
		toairtel.put("amount", amountstring);
		toairtel.put("url", countryremitip);

		String jsonData = g.toJson(toairtel);

		if (StringUtils.isNotEmpty(countryremitip)) {

			// assign the remit url from properties.config
			CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");

			postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonData);

			// capture the switch respoinse.
			String responseobject = postMinusThread.doPost();

			// pass the returned json string
			JsonElement roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			String switchresponse = roots.getAsJsonObject().get("status_code").getAsString();

			// exctract a specific json element from the object(status_code)
			String statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

			if (!transactionStatusHash.containsKey(switchresponse)) {
				switchresponse = "00032";
				statusdescription = "UNKNOWN_ERROR";
			}

			// set the status UUID
			String statusuuid = transactionStatusHash.get(switchresponse);

			String success = "S000";

			// the account UUID
			String accountuuid = account.getUuid();

			Transaction saved = new Transaction();

			// server time
			Date now = new Date();

			saved.setUuid(transactioinid);
			saved.setAccountUuid(accountuuid);
			saved.setSourceCountrycode(sourcecountrycode);
			saved.setSenderName(sendername);
			saved.setRecipientMobile(recipientmobile);
			saved.setAmount(finalconvertedamount);
			saved.setCurrencyCode(recipientcurrencycode);
			saved.setRecipientCountryUuid(countrycodetodb);
			saved.setSenderToken(sendertoken);
			saved.setClientTime(clienttime);
			saved.setServerTime(now);
			saved.setTransactionStatusUuid(statusuuid);
			saved.setReferenceNumber(referencenumber);

			// testing to see if adding of transaction is successful(it's
			// failing to return true)
			if (!transactionDAO.addTransaction(saved)) {
				expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL);
				String jsonResult = g.toJson(expected);
				return jsonResult;
			}

			// response when the transaction is a success to deduct balance.

			if (switchresponse.equalsIgnoreCase(success)) {

				// log the results into a db.
				TransactionForexrate transactionratehistory = new TransactionForexrate();

				// saved.setId(ID);
				transactionratehistory.setUuid(transactionforexhistoryuuid);
				transactionratehistory.setTransactionUuid(transactioinid);
				// transactionratehistory.setAccountUuid(accountuuid);
				// transactionratehistory.setRecipientcountryUuid(countrycodetodb);
				transactionratehistory.setAccount(accountuuid);
				transactionratehistory.setRecipientcountry(countrycodetodb);
				transactionratehistory.setLocalamount(amount);
				transactionratehistory.setAccounttype(accounttype);
				transactionratehistory.setConvertedamount(finalconvertedamount);
				transactionratehistory.setImpalarate(impalaexchangecalculate);
				transactionratehistory.setBaserate(baseexchange);
				transactionratehistory.setReceivermsisdn(recipientmobile);
				transactionratehistory.setSurplus(remainderamount);
				transactionratehistory.setServerTime(now);

				new TransactionDispatcher(saved, transactionratehistory).start();
				expected.put("api_username", username);
				expected.put("transaction_id", transactioinid);
				expected.put("command_status", APIConstants.COMMANDSTATUS_OK);
				expected.put("status_code", switchresponse);
				expected.put("remit_status", statusdescription);
				String jsonResult = g.toJson(expected);

				return jsonResult;

			}

			expected.put("api_username", username);
			expected.put("transaction_id", transactioinid);
			expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL);
			expected.put("status_code", switchresponse);
			expected.put("remit_status", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}

		expected.put("api_username", username);
		expected.put("command_status", APIConstants.COMMANDSTATUS_UNOPERATIONAL_COUNTRY);
		String jsonResult = g.toJson(expected);
		return jsonResult;
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.airtel.servlet.api.forex;

import java.util.HashMap;

import javax.validation.constraints.Size;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class vevbevb {

	public static void main(String args[]) {

		HashMap<String, Double> forexmarketratemap = new HashMap<>();

		forexmarketratemap.put("KES", 50.0);
		forexmarketratemap.put("UGX", 50.0);
		forexmarketratemap.put("TZS", 50.0);

		Gson gson = new Gson();
		Gson gsons = new GsonBuilder().setPrettyPrinting().serializeNulls()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		String json = gson.toJson(forexmarketratemap);

		JsonObject start = new JsonObject();
		start.addProperty("servertime", "veve");

		JsonArray amount = new JsonArray();

		JsonParser parser = new JsonParser();
		JsonObject amountdataset4 = parser.parse(json).getAsJsonObject();

		amount.add(amountdataset4);

		JsonObject amountdata4 = null;

		for (int i = 0; i < amount.size(); i++) {
			amountdata4 = amount.get(i).getAsJsonObject();
		}

		start.add("forex", amountdata4);
		// o.add(property, value);
		// JsonObject amountdata4 = null;

		// JsonObject eugene = new JsonObject();
		// eugene.addProperty("forex", o);

		String json2 = gsons.toJson(start);

		System.out.println(json2);

	}
}

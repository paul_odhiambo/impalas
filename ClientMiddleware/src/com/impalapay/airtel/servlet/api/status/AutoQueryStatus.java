package com.impalapay.airtel.servlet.api.status;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.accountmgmt.inprogressbalance.InProgressBalanceDAO;
import com.impalapay.airtel.persistence.transaction.TransactionDAO;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.mno.servlet.api.remit.AutoTransactionDispatcher;
//import com.impalapay.airtel.servlet.api.status.TransactionDispatcher;
import com.impalapay.mno.servlet.api.remit.TransactionDispatcher;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
import com.impalapay.beans.network.Network;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class AutoQueryStatus extends HttpServlet {

	private PostWithIgnoreSSL postMinusThread;

	private Cache networkCache, transactionStatusCache;

	private TransactionDAO transactionDAO;

	private InProgressBalanceDAO inprogressbalanaceDAO;

	private TransactionForexDAO transactionforexDAO;

	private HashMap<String, String> transactionStatusHash = new HashMap<>();

	private HashMap<String, String> transactionStatusuuidHash = new HashMap<>();

	private HashMap<String, String> networkQueryUrlmap = new HashMap<>();

	private HashMap<String, String> networkBridgeQueryUrlmap = new HashMap<>();

	private HashMap<String, String> networksupportquerymap = new HashMap<>();

	private HashMap<String, String> networkUsernamemap = new HashMap<>();

	private HashMap<String, String> networkPasswordmap = new HashMap<>();

	private String CLIENT_URL = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		transactionDAO = TransactionDAO.getInstance();
		transactionforexDAO = TransactionForexDAO.getinstance();
		inprogressbalanaceDAO = InProgressBalanceDAO.getInstance();

		networkCache = mgr.getCache(CacheVariables.CACHE_NETWORK_BY_UUID);
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		String responseobject = "";

		// joined json string
		String join = "";
		JsonElement root = null;

		// These represent parameters received over the network
		String transactionid = "", networkid = "", switchresponse = "", statusdescription = "",
				receiverreferenceid = "";

		//
		String receiverusername = "", receiverpassword = "", receiverqueryurl = "", receiverbridgequeryurl = "",
				supportquerycheck = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ####################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			transactionid = root.getAsJsonObject().get("transaction_id").getAsString();

			networkid = root.getAsJsonObject().get("networkuuid").getAsString();

			receiverreferenceid = root.getAsJsonObject().get("receiverreferenceuuid").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(transactionid) || StringUtils.isBlank(networkid)) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		Element element;

		List keys;

		// **************Network Cache****************//

		Network network;
		// network and remiturl
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkQueryUrlmap.put(network.getUuid(), network.getQueryip());
		}
		// network and username
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkUsernamemap.put(network.getUuid(), network.getUsername());
		}

		// network and password
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkPasswordmap.put(network.getUuid(), network.getPassword());
		}

		// network and bridgeremiturl
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkBridgeQueryUrlmap.put(network.getUuid(), network.getBridgequeryip());
		}

		// network and support querying
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networksupportquerymap.put(network.getUuid(), String.valueOf(network.isSupportquerycheck()));
		}

		// ======================================================
		// Populate with the mapping of Transaction Statuses.
		// The key is a UUID of the status
		// ======================================================

		TransactionStatus status1;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status1.getStatus(), status1.getUuid());
		}

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusuuidHash.put(status1.getUuid(), status1.getStatus());
		}

		// First step check if network support quering of url.
		supportquerycheck = networksupportquerymap.get(networkid);
		String booleansupport = "true";

		if (!supportquerycheck.equalsIgnoreCase(booleansupport)) {
			expected.put("command_status", "Route does not support query status");
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		Transaction transactions = transactionDAO.getTransactionbyuuid(transactionid);
		// String transactionref = transactions.getReferenceNumber();
		if ((transactions == null)) {
			expected.put("command_status", "We have a problem at autoquery under fetching transactions");
			String jsonResult = g.toJson(expected);
			return jsonResult;
		}

		TransactionForexrate transactionforex = transactionforexDAO.getTransactionForexsUuid(transactionid);
		if ((transactionforex == null)) {
			expected.put("command_status",
					"We have a problem at autoquery under fetching transactionforex rate transaction");
			String jsonResult = g.toJson(expected);
			return jsonResult;
		}

		// String receiveruuid = transactions.getReceivertransactionid();

		// TransactionStatus transactionstatus =
		// transactionstatusDAO.getTransactionStatus(transtatusuuid);
		// String status = transactionstatus.getStatus();

		// String routename = transactions.getRemitroute();

		// *******************************************************************
		// fetch parameters from hashmaps
		// *******************************************************************
		receiverusername = networkUsernamemap.get(networkid);
		receiverpassword = networkPasswordmap.get(networkid);
		receiverqueryurl = networkQueryUrlmap.get(networkid);
		receiverbridgequeryurl = networkBridgeQueryUrlmap.get(networkid);

		// #############################################################################################
		// construct a Mega-Json Object to route-transactions to internal
		// Servlet routing transactions.
		// #############################################################################################
		JsonObject queryrequest = new JsonObject();

		queryrequest.addProperty("username", receiverusername);
		queryrequest.addProperty("password", receiverpassword);
		queryrequest.addProperty("transactionid", transactionid);
		queryrequest.addProperty("receivertransactionid", receiverreferenceid);
		queryrequest.addProperty("receiverqueryurl", receiverqueryurl);

		String results2 = g.toJson(queryrequest);

		CLIENT_URL = receiverbridgequeryurl;

		postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, results2);

		// capture the switch respoinse.
		responseobject = postMinusThread.doPost();

		// ##########################################################################
		// confirm if the returned response is a Json object
		// #########################################################################
		/**
		 * boolean walletresponse = jsoncheck.isValid(responseobject);
		 * if(!walletresponse){
		 * 
		 * expected.put("command_status",APIConstants.
		 * COMMANDSTATUS_RECEIVER_SERVER_ERROR); String jsonResult = g.toJson(expected);
		 * 
		 * return jsonResult; }
		 **/

		// pass the returned json string
		// JsonElement roots = new JsonParser().parse(responseobject);

		JsonElement roots = null;

		roots = new JsonParser().parse(responseobject);

		try {
			// ===============================================================
			// an object that will contain parameters for provisional response
			// ===============================================================
			// exctract a specific json element from the object(status_code)
			switchresponse = roots.getAsJsonObject().get("status_code").getAsString();

			// exctract a specific json element from the object(status_code)
			statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("command_status", APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// System.out.println(switchresponse);

		if (!transactionStatusHash.containsKey(switchresponse)) {
			switchresponse = "00032";
			statusdescription = "UNKNOWN_ERROR";
		} // Transaction forex

		// set the status UUID
		String statusuuid = transactionStatusHash.get(switchresponse);

		String success = "S000";
		String fail = "00029";

		TransactionStatus statusoftransaction = new TransactionStatus();

		if (switchresponse.equalsIgnoreCase(success) || switchresponse.equalsIgnoreCase(fail)) {

			if (switchresponse.equals(success)) {

				new AutoTransactionDispatcher(transactions, transactionforex).start();

			} else {
				// delete the transaction forex from db
				// transactionforexDAO.deleteTransactionForex(transactionid);
				// return balances owed
				// inprogressbalanaceDAO.removeBalanceHoldFail(transactionid);
				// update that the system approved this balance
				if (transactionforexDAO.deleteTransactionForex(transactionid)
						&& inprogressbalanaceDAO.removeBalanceHoldFail(transactionid)) {

				} else {
					System.out.println("This transaction failed to update succesfully " + transactionid);
				}

			}

			statusoftransaction.setUuid(statusuuid);
			transactionDAO.updateTransactionStatus(transactionid, statusoftransaction);

			expected.put("transaction_id", "");
			expected.put("status_code", switchresponse);
			expected.put("command_status", APIConstants.COMMANDSTATUS_OK);
			expected.put("remit_status", statusdescription);

			String jsonResult = g.toJson(expected);

			return jsonResult;

		}

		expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL);
		expected.put("remit_status", statusdescription);
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}// end of inprogress status

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

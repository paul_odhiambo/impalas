package com.impalapay.airtel.servlet.api.session;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.sessionlog.SessionLog;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;
import com.impalapay.airtel.servlet.api.session.SessionIdDispatcher;
import com.impalapay.airtel.util.SecurityUtil;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

/**
 * Responsible for giving client applications Session Ids over HTTPS.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class SessionIdProvider extends HttpServlet {

	private Cache accountsCache;
	private SessionLogDAO sessionLogDAO;
	private SessionLog oldSession;
	private final long SESSIONID_MINUTES_ALIVE = 1;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);
		sessionLogDAO = SessionLogDAO.getInstance();
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(getSessionId(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * Receives an HTTP request and processes it further. In a successful case, a
	 * Transaction Id and status are returned.
	 * 
	 * @param request
	 * @return String
	 * @throws IOException
	 */
	private String getSessionId(HttpServletRequest request) throws IOException {
		String username = "", password = "";

		Account account = null;
		DateTime dateTime = null;
		Date date = null;
		String join = "";
		JsonElement root = null;
		long diffInMillis = 0, diffMinutes = 0;
		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ####################################################################
		// instantiate the JSon
		// ####################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("api_username").getAsString();
			password = root.getAsJsonObject().get("api_password").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details then check against username and password
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		if (account == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Invalid password for hashed password
		if (!StringUtils.equals(SecurityUtil.getMD5Hash(password), account.getApiPasswd())) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PASSWORD);
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}

		if (oldSession != null) {
			oldSession = sessionLogDAO.getValidSessionLog(account);
			// Rate limit the number of sessionrequests within a period of time.
			dateTime = new DateTime();
			date = dateTime.toGregorianCalendar().getTime();
			diffInMillis = date.getTime() - oldSession.getCreationTime().getTime();
			diffMinutes = diffInMillis / (60 * 1000) % 60;

			if (diffMinutes <= SESSIONID_MINUTES_ALIVE) {
				expected.put("command_status",
						APIConstants.COMMANDSTATUS_SESSION_LIMIT_ERROR + SESSIONID_MINUTES_ALIVE + "_MINUTE");
				String jsonResult = g.toJson(expected);

				return jsonResult;
			}

		}

		// This means that everything is ok
		new SessionIdDispatcher(account).start();
		expected.put("command_status", APIConstants.COMMANDSTATUS_OK);
		String jsonResult = g.toJson(expected);

		return jsonResult;
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.airtel.servlet.api.balance;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.geolocation.CountryMsisdn;
import com.impalapay.airtel.beans.sessionlog.SessionLog;
import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.util.SecurityUtil;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.beans.network.Network;
import com.impalapay.airtel.persistence.accountmgmt.balance.AccountBalanceDAO;
import com.impalapay.airtel.persistence.geolocation.CountryMsisdnDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of balance through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryBalance3 extends HttpServlet {

	private PostWithIgnoreSSL postMinusThread;

	private Cache accountsCache, countryCache, networkCache;

	private SessionLogDAO sessionlogDAO;

	private CountryMsisdnDAO countryMsisdnDAO;

	private HashMap<String, String> countryHash = new HashMap<>();

	private HashMap<String, String> countryUuid = new HashMap<>();

	private HashMap<String, String> countryIp = new HashMap<>();

	private HashMap<String, String> countryUsername = new HashMap<>();

	private HashMap<String, String> countryPassword = new HashMap<>();

	private HashMap<String, String> networkBalanceUrlmap = new HashMap<>();

	private HashMap<String, String> networkBridgeBalanceUrlmap = new HashMap<>();

	private HashMap<String, String> networksupportbalancemap = new HashMap<>();

	private HashMap<String, String> networkUsernamemap = new HashMap<>();

	private HashMap<String, String> networkPasswordmap = new HashMap<>();

	private HashMap<String, String> countryMsisdnmap = new HashMap<>();

	private Map<String, String> toreceivesystem = new HashMap<>();

	private String CLIENT_URL = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

		sessionlogDAO = SessionLogDAO.getInstance();

		AccountBalanceDAO.getInstance();

		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);

		countryMsisdnDAO = CountryMsisdnDAO.getInstance();

		networkCache = mgr.getCache(CacheVariables.CACHE_NETWORK_BY_UUID);
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkBalance(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkBalance(HttpServletRequest request) throws IOException {
		Account account = null;
		// joined json string
		String join = "", responseobject = "";
		JsonElement root = null, roots = null;

		// for balance response
		String statusdescription = "", switchresponse = "", switchbalance = "";
		String statusdescription2 = "", switchresponse2 = "";

		// These represent parameters received over the network
		String username = "", sessionid = "", networkid = "";

		//
		String receiverusername = "", receiverpassword = "", receiverbalanceurl = "", receiverbridgebalanceurl = "",
				supportquerycheck = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ##############################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ##############################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);
			networkid = root.getAsJsonObject().get("network_uuid").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(sessionid) || StringUtils.isBlank(networkid)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check if network is available.

		// check if network supports balance query.

		// forward query details to Network.

		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		if (account == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}
		// test for invalid sessionid
		SessionLog sessionlog = sessionlogDAO.getValidSessionLog(account);

		// ################################################################
		// Guard against all invalid sessionid error
		// ################################################################

		if (sessionlog == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		String session = sessionlog.getSessionUuid();

		if (!StringUtils.equals(SecurityUtil.getMD5Hash(sessionid), session)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}

		List keys;

		// **************Network Cache****************//

		Network network;
		// network and balance Query Url
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkBalanceUrlmap.put(network.getUuid(), network.getBalanceip());
		}
		// network and username
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkUsernamemap.put(network.getUuid(), network.getUsername());
		}

		// network and password
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkPasswordmap.put(network.getUuid(), network.getPassword());
		}

		// network and bridge balance url
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkBridgeBalanceUrlmap.put(network.getUuid(), network.getBridgebalanceip());
		}

		// network and support balance querying
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networksupportbalancemap.put(network.getUuid(), String.valueOf(network.isSupportbalancecheck()));
		}

		// First step check if network support quering of url.
		supportquerycheck = networksupportbalancemap.get(networkid);
		String booleansupport = "true";

		if (!supportquerycheck.equalsIgnoreCase(booleansupport)) {
			expected.put("command_status", "Route does not support query balance");
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// *******************************************************************
		// fetch parameters from hashmaps
		// *******************************************************************
		receiverusername = networkUsernamemap.get(networkid);
		receiverpassword = networkPasswordmap.get(networkid);
		receiverbalanceurl = networkBalanceUrlmap.get(networkid);
		receiverbridgebalanceurl = networkBridgeBalanceUrlmap.get(networkid);

		// ###############################################################
		// fetch the countrymsisdn by account then place them in a hashmap
		// ################################################################

		List<CountryMsisdn> countrysourcenumber = countryMsisdnDAO.getCountryMsisdn(account);

		for (CountryMsisdn countrynumber : countrysourcenumber)
			countryMsisdnmap.put(countrynumber.getNetworkUuid(), countrynumber.getMsisdn());

		String sourcemsisdn = countryMsisdnmap.get(networkid);

		if (StringUtils.isEmpty(sourcemsisdn)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_REMIT_NUMBER);

			return g.toJson(expected);
		}

		// generate UUID a transaction UUID
		toreceivesystem.put("username", receiverusername);
		toreceivesystem.put("password", receiverpassword);
		toreceivesystem.put("receiverqueryurl", receiverbalanceurl);
		toreceivesystem.put("sourceaccount", sourcemsisdn);

		String jsonData = g.toJson(toreceivesystem);

		CLIENT_URL = receiverbridgebalanceurl;

		try {
			postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonData);

			// capture the switch respoinse.
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			switchbalance = roots.getAsJsonObject().get("balance").getAsString();

			switchresponse = roots.getAsJsonObject().get("status_code").getAsString();

			// exctract a specific json element from the object(status_code)
			statusdescription = roots.getAsJsonObject().get("status_description").getAsString();
		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("command_status", APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}

		if ((switchresponse.equalsIgnoreCase("S000") || switchresponse.equalsIgnoreCase("ok"))) {
			statusdescription2 = "SUCCESS";
			switchresponse2 = "S000";

			expected.put("api_username", username);
			expected.put("balance", switchbalance);
			expected.put("status_code", switchresponse2);
			expected.put("remit_status", statusdescription2);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("api_username", username);
		expected.put("command_status", APIConstants.COMMANDSTATUS_UNOPERATIONAL_COUNTRY);
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.airtel.servlet.api.manualupdate;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.api.status.AsynDeductions;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.beans.thirdreference.ThirdPartyReference;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.persistence.thirdreference.ThirdReferenceDAO;
import com.impalapay.airtel.persistence.transaction.TransactionDAO;
import com.impalapay.airtel.persistence.transaction.TransactionStatusDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class UpdateQueryStatus extends HttpServlet {

	private Cache accountsCache, transactionStatusCache;

	private TransactionDAO transactionDAO;

	private TransactionForexDAO transactionforexDAO;

	private TransactionStatusDAO transactionstatusDAO;

	private ThirdReferenceDAO thirdreferenceDAO;

	private HashMap<String, String> transactionStatusHash = new HashMap<>();

	private HashMap<String, String> transactionStatusuuidHash = new HashMap<>();

	String recipientcountrycode = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

		SessionLogDAO.getInstance();
		transactionDAO = TransactionDAO.getInstance();
		transactionstatusDAO = TransactionStatusDAO.getInstance();
		thirdreferenceDAO = ThirdReferenceDAO.getInstance();
		transactionforexDAO = TransactionForexDAO.getinstance();

		mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null;

		// These represent parameters received over the network
		String username = "", referencenumber = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ####################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("api_username").getAsString();

			referencenumber = root.getAsJsonObject().get("reference_number").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(referencenumber)) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details then check against username and sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		if (account == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// At this point we check to see if there is no transaction with the
		// given reference number.
		List<Transaction> transaction = transactionDAO.getTransactionstatus(referencenumber);

		if (transaction.size() == 0) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_REFERENCENUMBER);
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}
		List keys;

		// ======================================================
		// Populate with the mapping of Transaction Statuses.
		// The key is a UUID of the status
		// ======================================================

		TransactionStatus status1;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status1.getStatus(), status1.getUuid());
		}

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusuuidHash.put(status1.getUuid(), status1.getStatus());
		}

		Transaction transactions = transactionDAO.getTransactionstatus1(referencenumber);
		String transactionref = transactions.getReferenceNumber();

		String thirdrefuuid = transactions.getUuid();

		// fetch the third partyreference number
		ThirdPartyReference thirdpartyref = thirdreferenceDAO.getTransactionReference(thirdrefuuid);
		String fetchrefuuid = thirdpartyref.getReferencenumber();

		String transtatusuuid = transactions.getTransactionStatusUuid();
		TransactionStatus transactionstatus = transactionstatusDAO.getTransactionStatus(transtatusuuid);
		String status = transactionstatus.getStatus();

		TransactionForexrate transactionforexhistory = transactionforexDAO.getTransactionForexsUuid(thirdrefuuid);

		String success = "S000";
		String fail = "00126";
		String inprogress = "S001";

		// set the status UUID
		String statusuuid = transactionStatusHash.get(success);

		transactions.setTransactionStatusUuid(statusuuid);
		// transactionDAO.updateTransactionStatus(thirdrefuuid, transactions);

		// deduct balance from country
		new AsynDeductions(transactions, transactionforexhistory).start();
		expected.put("transaction_id", thirdrefuuid);
		expected.put("refrence_id", referencenumber);
		expected.put("command_status", APIConstants.COMMANDSTATUS_OK);

		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}
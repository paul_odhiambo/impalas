package com.impalapay.airtel.servlet.api.temporarysend;

import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.X509Certificate;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

public class SoapCreation {

	/**
	 * Starting point for the SAAJ - SOAP Client Testing
	 * 
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */

	public String QuerryOutputProcess(String amount, String source_mssidn, String benefitiary_mssidn,
			String transaction_id) throws NoSuchAlgorithmException, KeyManagementException {
		int vundi = 0;

		String state = "";
		String resp = "";

		try {
			// Create a trust manager that does not validate certificate chains

			System.setProperty("javax.net.ssl.trustStore", " opt\\jdks\\jdk1.8.0\\jre\\lib\\security\\cacerts");
			System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
			System.setProperty("javax.net.ssl.keyStore", "keystore.jks");
			System.setProperty("javax.net.ssl.keyStorePassword", "password");
			System.setProperty("javax.net.ssl.keyStoreType", "JKS");

			System.setProperty("https.proxySet", "true");
			System.setProperty("https.proxyHost", "proxy.org.net");
			System.setProperty("https.proxyPort", "8074");
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
						throws CertificateException {
					// TODO Auto-generated method stub

				}

				@Override
				public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
						throws CertificateException {
					// TODO Auto-generated method stub

				}
			} };
			// Install the all-trusting trust manager
			final SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			URL url = new URL("https://android.googleapis.com/gcm/send");
			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setRequestMethod("POST");

			// com.sun.net.ssl.internal.ssl.Provider()
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			// Create SOAP Connection

			// Trust to certificates
			// Create a trust manager that does not validate certificate chains

			// Send SOAP Message to SOAP Server
			String urli = "http://196.216.73.147:8084/service/wsdl/";
			SOAPMessage soapResponse = soapConnection
					.call(createSOAPRequest(amount, source_mssidn, benefitiary_mssidn, transaction_id), urli);

			// Process the SOAP Response

			printSOAPResponse(soapResponse);
			resp = soapResponse.getSOAPBody().getElementsByTagName("status").item(0).getFirstChild().getNodeValue();
			// Get the response
			System.out.println(resp);

			soapConnection.close();

		} catch (Exception e) {
			System.err.println("Error occurred while sending SOAP Request to Server");
			// String Success = "success";
			e.printStackTrace();
			// return Success;
		}
		// System.out.println("state");
		return resp;
	}

	private static SOAPMessage createSOAPRequest(String amount, String source_mssidn, String benefitiary_mssidn,
			String transaction_id) throws Exception {

		MessageFactory messageFactory = MessageFactory.newInstance();

		SOAPMessage soapMessage = messageFactory.createMessage();
		soapMessage.getSOAPHeader().setPrefix("soapenv");
		SOAPPart soapPart = soapMessage.getSOAPPart();
		String serverURI = "http://196.216.73.147:8084/";

		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("soapenv", serverURI);
		envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
		envelope.addNamespaceDeclaration("xsi", "http://www.w3.org/2001/XMLSchema-instance");
		envelope.addNamespaceDeclaration("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
		envelope.addNamespaceDeclaration("urn", "impala");

		SOAPHeader header = envelope.getHeader();
		header.detachNode();

		SOAPBody soapBody1 = envelope.getBody();
		QName bodyName = new QName("http://schemas.xmlsoap.org/soap/encoding/", "pushPayment", "urn");
		SOAPElement soapBodyElemX = soapBody1.addChildElement(bodyName);

		SOAPElement soapBodyElemXp = soapBodyElemX.addChildElement("PushPaymentParams");
		soapBodyElemXp.setAttribute("xsi:type", "urn:PushPaymentParams");

		SOAPElement soapBodyElemX11 = soapBodyElemXp.addChildElement("auth");
		soapBodyElemX11.setAttribute("xsi:type", "xsd:auth");
		SOAPElement soapBodyElem11 = soapBodyElemX11.addChildElement("username");
		soapBodyElem11.addTextNode("dubaibank").setAttribute("xsi:type", "xsd:string");
		SOAPElement soapBodyElem21 = soapBodyElemX11.addChildElement("password");
		soapBodyElem21.addTextNode("password").setAttribute("xsi:type", "xsd:string");
		SOAPElement soapBodyElem31 = soapBodyElemX11.addChildElement("secret");
		soapBodyElem31.addTextNode("cookiecutter").setAttribute("xsi:type", "xsd:string");
		SOAPElement soapBodyElem41 = soapBodyElemX11.addChildElement("companyCode");
		soapBodyElem41.addTextNode("DUBS7789").setAttribute("xsi:type", "xsd:string");

		SOAPElement soapBodyElemX1 = soapBodyElemXp.addChildElement("PaymentParams");

		soapBodyElemX1.setAttribute("xsi:type", "xsd:pushParams");
		SOAPElement soapBodyElem1 = soapBodyElemX1.addChildElement("senderFullName");
		soapBodyElem1.addTextNode("Impalapay IMT").setAttribute("xsi:type", "xsd:string");

		SOAPElement soapBodyElem2 = soapBodyElemX1.addChildElement("senderID");
		soapBodyElem2.addTextNode("28497598").setAttribute("xsi:type", "xsd:string");

		SOAPElement soapBodyElem3 = soapBodyElemX1.addChildElement("senderPhoneNumber");
		soapBodyElem3.addTextNode("254733190378").setAttribute("xsi:type", "xsd:string");

		SOAPElement soapBodyElem4 = soapBodyElemX1.addChildElement("Amount");
		soapBodyElem4.addTextNode(amount).setAttribute("xsi:type", "xsd:string");

		SOAPElement soapBodyElem5 = soapBodyElemX1.addChildElement("receiverFullName");
		soapBodyElem5.addTextNode("Dubai1 Demo1").setAttribute("xsi:type", "xsd:string");

		SOAPElement soapBodyElem6 = soapBodyElemX1.addChildElement("receiverIDNumber");
		soapBodyElem6.addTextNode("4548484").setAttribute("xsi:type", "xsd:string");

		SOAPElement soapBodyElem7 = soapBodyElemX1.addChildElement("receiverPhoneNumber");
		soapBodyElem7.addTextNode(benefitiary_mssidn).setAttribute("xsi:type", "xsd:string");

		SOAPElement soapBodyElem8 = soapBodyElemX1.addChildElement("transactionRefNumber");
		soapBodyElem8.addTextNode(transaction_id).setAttribute("xsi:type", "xsd:string");

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", serverURI + "PushPaymentParams");
		System.out.print("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();
		return soapMessage;

	}

	private static void printSOAPResponse(SOAPMessage soapResponse) throws Exception {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Source sourceContent = soapResponse.getSOAPPart().getContent();
		System.out.print("\nResponse SOAP Message = ");
		StreamResult result = new StreamResult(System.out);
		transformer.transform(sourceContent, result);
	}

}

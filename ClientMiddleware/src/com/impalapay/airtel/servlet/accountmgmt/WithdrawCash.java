package com.impalapay.airtel.servlet.accountmgmt;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.impalapay.airtel.accountmgmt.session.SessionConstants;
import com.impalapay.collection.beans.balance.CashWithdrawal;
import com.impalapay.collection.beans.balance.CollectionBalanceHistory;
import com.impalapay.collection.persistence.balance.CollectionBalanceDAOImpl;
import com.impalapay.collection.persistence.cashwithdraw.ClientWithdrawDAOImpl;
import org.apache.commons.lang3.StringUtils;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to create a new account.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Nov 24, 2014
 *
 * @author <a href="mailto:mike@impalapay.com">Michael Wakahe</a>
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 *
 */
public class WithdrawCash extends HttpServlet {

	final String ERROR_NO_USERNAME = "Please provide a Username.";
	final String ERROR_NO_BALANCEUUID = "Please provide the balance Unique indetifier.";
	final String ERROR_NO_TERMINATECURRENCY = "Please provide the terminate Currency.";
	final String ERROR_NO_AMOUNT = "Please provide the amount to be withdrawn";
	final String ERROR_NO_LESSAMOUNT = "Please make sure your account has enough funds for this withdrawal,your current balance is ";
	final String ERROR_NO_EXTRAINFO = "Please Provide the extra infiormation for Banking(e.g AccountName,AccountNumber,BankName,BankCode,SwiftCode...e.t.c).";
	final String ERROR_NO_WITHDRAWFAIL = "Unable to finalise Your withdrawal Request Please contact the ystem administrator or try after a few minutes";

	private String username, collectionbalanceuuid, terminatecurrency, amount, extrainfo;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;
	private CollectionBalanceDAOImpl collectionbalanceDAO;
	private ClientWithdrawDAOImpl cashwithdrawDAO;
	private CollectionBalanceHistory checkcollectionbalance;
	private CashWithdrawal withdrawalrequest;
	private String transactioinid = "";
	private Double currentbalance;
	private HttpSession session;

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		collectionbalanceDAO = CollectionBalanceDAOImpl.getInstance();

		cashwithdrawDAO = ClientWithdrawDAOImpl.getInstance();

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.CLIENT_WITHDRAWALREQUEST_PARAMETERS, paramHash);
		try {
			checkcollectionbalance = collectionbalanceDAO.getCollectionBalance(collectionbalanceuuid);
			currentbalance = checkcollectionbalance.getBalance();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error while trying to fetch accountbalance");
		}

		// No First Name provided
		if (StringUtils.isBlank(username)) {
			session.setAttribute(SessionConstants.CLIENT_WITHDRAWALREQUEST_ERROR_KEY, ERROR_NO_USERNAME);

			// No Unique Name provided
		} else if (StringUtils.isBlank(collectionbalanceuuid)) {
			session.setAttribute(SessionConstants.CLIENT_WITHDRAWALREQUEST_ERROR_KEY, ERROR_NO_BALANCEUUID);

			// No website login password provided
		} else if (StringUtils.isBlank(terminatecurrency)) {
			session.setAttribute(SessionConstants.CLIENT_WITHDRAWALREQUEST_ERROR_KEY, ERROR_NO_TERMINATECURRENCY);

			// The website login passwords provided do not match
		} else if (StringUtils.isBlank(amount)) {
			session.setAttribute(SessionConstants.CLIENT_WITHDRAWALREQUEST_ERROR_KEY, ERROR_NO_AMOUNT);

		} else if (currentbalance <= Double.parseDouble(amount)) {
			session.setAttribute(SessionConstants.CLIENT_WITHDRAWALREQUEST_ERROR_KEY,
					ERROR_NO_LESSAMOUNT + currentbalance + " and you are trying to withdraw " + amount);
			// No API password provided
		} else if (StringUtils.isBlank(extrainfo)) {
			session.setAttribute(SessionConstants.CLIENT_WITHDRAWALREQUEST_ERROR_KEY, ERROR_NO_EXTRAINFO);

		} else if (!addAccount()) {

			session.setAttribute(SessionConstants.CLIENT_WITHDRAWALREQUEST_ERROR_KEY, ERROR_NO_WITHDRAWFAIL);

		} else {

			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.CLIENT_WITHDRAWALREQUEST_SUCCESS_KEY,
					"You have successfully made a withdrawal Request");

			// Reduce our session data
			session.setAttribute(SessionConstants.CLIENT_WITHDRAWALREQUEST_PARAMETERS, null);
			session.setAttribute(SessionConstants.CLIENT_WITHDRAWALREQUEST_ERROR_KEY, null);
		}

		// response.sendRedirect("addAccount.jsp");accountsIndex.jsp
		response.sendRedirect("collectionbalance.jsp");
	}

	/**
	 *
	 */
	private boolean addAccount() {
		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		if (checkcollectionbalance == null) {
			return false;

		}

		withdrawalrequest = new CashWithdrawal();

		withdrawalrequest.setUuid(transactioinid);
		withdrawalrequest.setAccountuuid(checkcollectionbalance.getAccountuuid());
		withdrawalrequest.setCurrencyUuid(checkcollectionbalance.getCountryuuid());
		withdrawalrequest.setTocurrencyUuid(terminatecurrency);
		withdrawalrequest.setExtrainformation(extrainfo);
		withdrawalrequest.setAmount(Double.parseDouble(amount));
		withdrawalrequest.setTransactionDate(new Date());

		// accountDAO.addAccount(a);
		boolean response = cashwithdrawDAO.putClientWithdraw(withdrawalrequest);

		return response;
	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {

		username = StringUtils.trimToEmpty(request.getParameter("username"));
		collectionbalanceuuid = StringUtils.trimToEmpty(request.getParameter("collectionbalanceuuid"));
		terminatecurrency = StringUtils.trimToEmpty(request.getParameter("terminatecurrency"));
		amount = StringUtils.trimToEmpty(request.getParameter("amount"));
		extrainfo = StringUtils.trimToEmpty(request.getParameter("extrainfo"));
	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		paramHash = new HashMap<>();

		paramHash.put("username", username);
		paramHash.put("collectionbalanceuuid", collectionbalanceuuid);
		paramHash.put("terminatecurrency", terminatecurrency);
		paramHash.put("amount", amount);
		paramHash.put("extrainfo", extrainfo);

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		// response.sendRedirect("../index.jsp");
	}
}

/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */
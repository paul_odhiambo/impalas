package com.impalapay.airtel.cache;

/**
 * Various global settings to be used in manipulating the cache. We are using
 * <a href="http://www.ehcache.org">Ehcache</a> as our library.
 * <p>
 * Copyright (c) ImpalaPay LTD., June 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @author <a href="mailto:mike@impalapay.com">Michael Wakahe</a>
 */
public class CacheVariables {

	// Names of caches

	// The following cache uses an account username as the key for the object
	public final static String CACHE_ACCOUNTS_BY_USERNAME = "AccountsUsername";
	public final static String CACHE_ACCOUNTS_BY_EMAIL = "AccountsEmail";

	public final static String CACHE_MANAGEMENTACCOUNTS_BY_USERNAME = "ManagementAccountsUsername";
	public final static String CACHE_MANAGEMENTACCOUNTS_BY_EMAIL = "ManagementAccountsEmail";

	// The following caches have an Uuid as the key for the object
	public final static String CACHE_ACCOUNTS_BY_UUID = "AccountsUuid";
	public final static String CACHE_MANAGEMENTACCOUNTS_BY_UUID = "ManagementAccountsUuid";
	public static String CACHE_TRANSACTIONSTATUS_BY_UUID = "TransactionStatusUuid";
	public static String CACHE_COUNTRY_BY_UUID = "CountryUuid";
	public static String CACHE_IPADDRESS_BY_UUID = "IpaddressUuid";

	// Checker cache.
	public final static String CACHE_CHECKERFOREX_BY_UUID = "CheckerUuid";
	public final static String CACHE_CHECKERFOREXHISTORY_BY_UUID = "CheckerhistoryUuid";

	// forex cache uuids
	public static String CACHE_USDFOREX_BY_UUID = "usdforexUuid";
	public static String CACHE_GBPFOREX_BY_UUID = "gbpforexUuid";
	public static String CACHE_FOREX_BY_UUID = "forexUuid";
	// reroute cache uuids
	public static String CACHE_PREFIX_BY_UUID = "prefixUuid";
	public static String CACHE_NETWORK_BY_UUID = "networkUuid";

	public static String CACHE_ALLTRANSACTIONUPDATE_BY_UUID = "updateUuid";

	// This cache is used to hold statistical information while a user is logged
	// in
	public final static String CACHE_STATISTICS_BY_USERNAME = "StatisticsUsername";
	public static String CACHE_STATISTICS_BY_ACCOUNTUUID = "StatisticsUuid";
	public static String CACHE_FLOATPURCHASEPERCOUNTRY_BY_ACCOUNTUUID = "PurchaseCountryUuid";
	public static String CACHE_FLOATPURCHASEPERNETWORK = "PurchaseNetworkUuid";
	public static String CACHE_BALANCEPERCOUNTRY_BY_ACCOUNTUUID = "CountryBalanceUuid";
	public final static String CACHE_MANAGEMENTACCOUNTS_STATISTICS_BY_USERNAME = "ManagementStatisticsUsername";

	// Cache variables to be used in the admin section
	public static String CACHE_ALL_ACCOUNTS_STATISTICS_KEY = "CacheAllAccountsStatisticsKey";
	public static String CACHE_ALL_ACCOUNTS_STATISTICS = "CacheAllAccountsStatistics";

	public static String CACHE_MASTERPURCHASE = "CacheMasterPurchase";
	public static String CACHE_MASTERPURCHASE_KEY = "CacheMasterPurchaseKey";
	public static String CACHE_MASTERBALANCE = "CacheMasterBalance";
	public static String CACHE_MASTERBALANCE_KEY = "CacheMasterBalanceKey";
	public static Object CACHE_PURCHASEPERCOUNTRY_KEY = "CachePurchasePerCountryKey";

	// additions.
	public static String CACHE_COUNTRYALPHA3_BY_UUID = "Countrycodealpha3Uuid";
	public static String CACHE_BANK_BY_UUID = "BankUuid";
	public static String CACHE_ROUTEDEFINED_BY_UUID = "RouteUuid";
	public static String CACHE_BILLPAYMENTCODES_BY_UUID = "BillpaymentUuid";
	
	public static String CACHE_TRANSFERTYPE_BY_UUID="TransfertypeUuid";

	// Collection Cache.
	public static String CACHE_COLLECTION_NETWORK = "collectionnetworkuuid";
	public static String CACHE_COLLECTION_NETWORK_SUBACCOUNT = "collectionnetworksubaccountuuid";
	public static String CACHE_COLLECTION_DEFINE = "collectiondefineuuid";
	public static String CACHE_COLLECTION_CHANNEL = "collectionchanneluuid";

}

package com.impalapay.beans.commission;

import java.util.Date;

import com.impalapay.airtel.beans.StorableBean;

/**
 * @author eugene
 *
 */
public class CommisionEstimate extends StorableBean {
	private String transactionUuid;
	private String commissioncurrency;
	private String statusuuid;
	private double commision;
	private double receivercommission;
	private double receiverfullamount;
	private Date dateadded;

	public CommisionEstimate() {
		super();
		transactionUuid = "";
		statusuuid = "";
		commissioncurrency = "";
		commision = 0;
		receivercommission = 0;
		receiverfullamount = 0;
		dateadded = new Date();
	}

	public String getTransactionUuid() {
		return transactionUuid;
	}

	public String getCommissioncurrency() {
		return commissioncurrency;
	}

	public String getStatusuuid() {
		return statusuuid;
	}

	public double getCommision() {
		return commision;
	}

	public double getReceivercommission() {
		return receivercommission;
	}

	public double getReceiverfullamount() {
		return receiverfullamount;
	}

	public Date getDateadded() {
		return dateadded;
	}

	public void setTransactionUuid(String transactionUuid) {
		this.transactionUuid = transactionUuid;
	}

	public void setCommissioncurrency(String commissioncurrency) {
		this.commissioncurrency = commissioncurrency;
	}

	public void setStatusuuid(String statusuuid) {
		this.statusuuid = statusuuid;
	}

	public void setCommision(double commision) {
		this.commision = commision;
	}

	public void setReceivercommission(double receivercommission) {
		this.receivercommission = receivercommission;
	}

	public void setReceiverfullamount(double receiverfullamount) {
		this.receiverfullamount = receiverfullamount;
	}

	public void setDateadded(Date dateadded) {
		this.dateadded = dateadded;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CommisionEstimate [getUuid()=");
		builder.append(getUuid());
		builder.append(", transactionUuid=");
		builder.append(transactionUuid);
		builder.append(", commissioncurrency=");
		builder.append(commissioncurrency);
		builder.append(", statusuuid=");
		builder.append(statusuuid);
		builder.append(", commision=");
		builder.append(commision);
		builder.append(", receivercommission=");
		builder.append(receivercommission);
		builder.append(", receiverfullamount=");
		builder.append(receiverfullamount);
		builder.append(", dateadded=");
		builder.append(dateadded);
		builder.append("]");
		return builder.toString();
	}

}

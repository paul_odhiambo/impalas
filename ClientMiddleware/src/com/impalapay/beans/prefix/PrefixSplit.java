package com.impalapay.beans.prefix;

import com.impalapay.airtel.beans.StorableBean;

import java.util.Date;

/**
 * Represents route networks can include mno or bank.
 * <p>
 * Copyright (c) ImpalaPay LTD., Feb 14, 2015
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */

public class PrefixSplit extends StorableBean {

	private String networkUuid;
	private String countryUuid;
	private String prefix;
	private String walletType;
	// private int splitLength;
	private Date dateadded;

	public PrefixSplit() {
		super();

		countryUuid = "";
		networkUuid = "";
		prefix = "";
		walletType = "";
		// splitLength = 0;
		dateadded = new Date();
	}

	public String getNetworkUuid() {
		return networkUuid;
	}

	public String getCountryUuid() {
		return countryUuid;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getWalletType() {
		return walletType;
	}

	public Date getDateadded() {
		return dateadded;
	}

	public void setNetworkUuid(String networkUuid) {
		this.networkUuid = networkUuid;
	}

	public void setCountryUuid(String countryUuid) {
		this.countryUuid = countryUuid;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public void setWalletType(String walletType) {
		this.walletType = walletType;
	}

	public void setDateadded(Date dateadded) {
		this.dateadded = dateadded;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PrefixSplit [getUuid()=");
		builder.append(getUuid());
		builder.append(", networkUuid=");
		builder.append(networkUuid);
		builder.append(", countryUuid=");
		builder.append(countryUuid);
		builder.append(", prefix=");
		builder.append(prefix);
		builder.append(", walletType=");
		builder.append(walletType);
		builder.append(", dateadded=");
		builder.append(dateadded);
		builder.append("]");
		return builder.toString();
	}

}

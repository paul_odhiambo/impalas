package com.impalapay.persistence.routing;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.beans.network.Network;
import com.impalapay.beans.route.RouteDefine;

public interface ImpalaRoute {

	/**
	 * 
	 * @param id
	 * @return RoutingInfo
	 */
	public RouteDefine getRoute(String id);

	/**
	 * 
	 * @param routename
	 * @return
	 */
	public RouteDefine getRoutes(String networkuuid);

	/**
	 * 
	 * @param hubroute
	 * @return
	 */
	public boolean putRoute(RouteDefine rotedefine);

	/**
	 * 
	 * @param networkuuid
	 * @param account
	 * @return
	 */
	public RouteDefine getRoutes(String networkuuid, Account account);

	/**
	 * 
	 * @return whether the action was successful or not
	 */
	public List<RouteDefine> getAllRoutes();

	/**
	 * 
	 * @param account
	 * @return
	 */
	public List<RouteDefine> getAllRoute(Account account);

	/**
	 * 
	 * @param rotedefine
	 * @return
	 */
	public boolean updateRoute(String uuid, RouteDefine rotedefine);

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<RouteDefine> getAllRoute(int fromIndex, int toIndex);

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public boolean deleteaccountroute(String uuid);

}

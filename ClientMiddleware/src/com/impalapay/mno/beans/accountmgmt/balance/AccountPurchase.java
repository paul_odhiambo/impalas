package com.impalapay.mno.beans.accountmgmt.balance;

import java.util.Date;

import com.impalapay.airtel.beans.StorableBean;

/**
 * A generic float purchase of account
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */

public class AccountPurchase extends StorableBean {

	private String accountUuid;
	private String currency;
	private double amount;
	private double balance;
	private Date purchaseDate;
	private Date topuptime;

	/**
	 * Default constructor
	 * 
	 */
	public AccountPurchase() {
		super();

		accountUuid = "";
		currency = "";
		amount = 0;
		balance = 0;
		purchaseDate = new Date();
		topuptime = new Date();

	}

	public String getAccountUuid() {
		return accountUuid;
	}

	public String getCurrency() {
		return currency;
	}

	/**
	 * 
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * 
	 * @return the purchaseDate
	 */
	public Date getPurchaseDate() {
		return new Date(purchaseDate.getTime());
	}

	public Date getTopuptime() {
		return new Date(topuptime.getTime());
	}

	public double getBalance() {
		return balance;
	}

	public void setAccountUuid(String accountUuid) {
		this.accountUuid = accountUuid;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * 
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(double amount) {
		if (amount >= 0) {
			this.amount = amount;
		}
	}

	public void setBalance(double balance) {
		if (balance >= 0) {
			this.balance = balance;
		}
	}

	/**
	 * 
	 * @param date
	 *            the purchaseDate to set
	 */
	public void setPurchaseDate(Date date) {
		if (date != null) {
			purchaseDate = new Date(date.getTime());
		}
	}

	public void setTopuptime(Date date) {
		if (date != null) {
			topuptime = new Date(date.getTime());
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccountPurchase [getUuid()=");
		builder.append(getUuid());
		builder.append(", accountUuid=");
		builder.append(accountUuid);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", balance=");
		builder.append(balance);
		builder.append(", purchaseDate=");
		builder.append(purchaseDate);
		builder.append(", topuptime=");
		builder.append(topuptime);
		builder.append("]");
		return builder.toString();
	}

}

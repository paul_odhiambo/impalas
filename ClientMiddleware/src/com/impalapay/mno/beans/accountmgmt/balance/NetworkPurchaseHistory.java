package com.impalapay.mno.beans.accountmgmt.balance;

import java.util.Date;

import com.impalapay.airtel.beans.StorableBean;

/**
 * A generic float purchase of account
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */

public class NetworkPurchaseHistory extends StorableBean {

	private String networkUuid;
	private double balance;
	private Date topuptime;

	/**
	 * Default constructor
	 * 
	 */
	public NetworkPurchaseHistory() {
		super();

		networkUuid = "";
		balance = 0;
		topuptime = new Date();

	}

	public String getNetworkUuid() {
		return networkUuid;
	}

	public double getBalance() {
		return balance;
	}

	public Date getTopuptime() {
		return topuptime;
	}

	public void setNetworkUuid(String networkUuid) {
		this.networkUuid = networkUuid;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void setTopuptime(Date topuptime) {
		this.topuptime = topuptime;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NetworkPurchaseHistory [getUuid()=");
		builder.append(getUuid());
		builder.append(", networkUuid=");
		builder.append(networkUuid);
		builder.append(", balance=");
		builder.append(balance);
		builder.append(", topuptime=");
		builder.append(topuptime);
		builder.append("]");
		return builder.toString();
	}

}

package com.impalapay.mno.persistence.accountmgmt.balance;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.accountmgmt.balance.AccountPurchaseByCountry;
import com.impalapay.airtel.beans.accountmgmt.balance.MasterAccountBalance;
import com.impalapay.airtel.beans.accountmgmt.balance.MasterAccountFloatPurchase;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.beans.network.Network;
import com.impalapay.mno.beans.accountmgmt.balance.NetworkPurchaseHistory;

public interface NetworksPurchaseHistory {

	/**
	 * @param uuid
	 * @return
	 */
	public NetworkPurchaseHistory getByNetworkPurchaseHistory(String uuid);

	/**
	 * To be called when a client redeem/recharge float on a country.
	 * 
	 * @param purchase
	 * @return whether or not the recharge action was successful
	 */
	public boolean putNetworkPurchaseHistory(NetworkPurchaseHistory purchase);

	/**
	 * This method is to be used infrequently, for example when there is an
	 * accidental entry of a float in a country.
	 * 
	 * @param uuid
	 * @return whether or not the delete action was successful
	 */
	public boolean deleteNetworkPurchaseHistory(String uuid);

	/**
	 * Gets a list of {@link AccountPurchaseByCountry}s belonging to this account
	 * holder.
	 * 
	 * @param account
	 * @return a list of recharge done by this client account.
	 */
	public List<NetworkPurchaseHistory> getNetworkPurchaseHistory(Network network);

	/**
	 * Returns a list of all {@link AccountPurchaseByCountry}s.
	 * 
	 * @return a list of recharge done by all client accounts.
	 */
	public List<NetworkPurchaseHistory> getAllNetworkPurchaseHistory();

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<NetworkPurchaseHistory> getAllNetworkPurchaseHistory(int fromIndex, int toIndex);

}

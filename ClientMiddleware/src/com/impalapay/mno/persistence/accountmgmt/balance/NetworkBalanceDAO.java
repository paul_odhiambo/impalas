package com.impalapay.mno.persistence.accountmgmt.balance;

import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.beans.network.Network;
import com.impalapay.mno.beans.accountmgmt.balance.NetworkBalance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

/**
 * Persistence description for
 * {@link com.impalapay.airtel.beans.accountmgmt.balance.AccountBalance}
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class NetworkBalanceDAO extends GenericDAO implements NetworksBalance {

	public static NetworkBalanceDAO accountbalanceDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return {@link TransactionDAO}
	 */
	public static NetworkBalanceDAO getInstance() {

		if (accountbalanceDAO == null) {
			accountbalanceDAO = new NetworkBalanceDAO();
		}

		return accountbalanceDAO;
	}

	/**
	 * 
	 */
	public NetworkBalanceDAO() {
		super();

	}

	/**
	 * 
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public NetworkBalanceDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);
	}

	/**
	 * 
	 */
	@Override
	public boolean hasBalance(Network network, double balance) {
		boolean hasBalance = false;
		int accountBalance = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT balance FROM networkbalancebycountry WHERE networkUuid=?;");
			pstmt.setString(1, network.getUuid());

			rset = pstmt.executeQuery();

			if (rset.next()) {
				accountBalance = rset.getInt("balance");
			}

			if (accountBalance >= balance) {
				hasBalance = true;
			}

		} catch (SQLException e) {
			logger.error(
					"SQLException exception while checking whether '" + network + "' has balance of " + balance + ".");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return hasBalance;
	}

	/**
	 * 
	 * 
	 */

	@Override
	public boolean deductBalance(Network network, double amount) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("UPDATE networkbalancebycountry "
					+ "SET balance = (SELECT balance FROM networkbalancebycountry WHERE networkUuid=?) " + "- ? "
					+ "WHERE uuid = (SELECT uuid FROM networkbalancebycountry WHERE networkUuid=?);");

			pstmt.setString(1, network.getUuid());
			pstmt.setDouble(2, amount);
			pstmt.setString(3, network.getUuid());
			pstmt.executeUpdate();

		} catch (SQLException e) {
			logger.error("SQLException exception while deducting balance");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public boolean addBalance(Network network, double amount) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();

			// Check whether this account has an existing balance or not.
			pstmt = conn.prepareStatement("SELECT uuid FROM networkbalancebycountry WHERE networkUuid=?;");
			pstmt.setString(1, network.getUuid());
			rset = pstmt.executeQuery();

			// The account has a pre-existing balance
			if (rset.next()) {
				pstmt = conn.prepareStatement("UPDATE networkbalancebycountry "
						+ "SET balance = (SELECT balance FROM networkbalancebycountry WHERE networkUuid=?) " + "+ ? "
						+ "WHERE uuid = (SELECT uuid FROM networkbalancebycountry WHERE networkUuid=?);");

				pstmt.setString(1, network.getUuid());
				pstmt.setDouble(2, amount);
				pstmt.setString(3, network.getUuid());
				pstmt.executeUpdate();

				// The account does not have a pre-existing balance
			} else {
				pstmt2 = conn.prepareStatement(
						"INSERT INTO networkbalancebycountry(uuid, networkuuid," + "balance) VALUES(?,?,?);");

				pstmt2.setString(1, UUID.randomUUID().toString());
				pstmt2.setString(2, network.getUuid());
				pstmt2.setDouble(3, amount);
				pstmt2.execute();
			}

		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("SQLException exception while adding the balance of '" + network + "' of amount " + amount
					+ "' of network " + network + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public double getBalance(Network account) {
		NetworkBalance balance = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		BeanProcessor b = new BeanProcessor();

		try {
			conn = dbCredentials.getConnection();

			// Get all balances on the account
			pstmt = conn.prepareStatement("SELECT * FROM networkbalancebycountry WHERE networkUuid=?;");
			pstmt.setString(1, account.getUuid());
			rset = pstmt.executeQuery();

			while (rset.next()) {
				balance = b.toBean(rset, NetworkBalance.class);
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting balances of '" + account + "'.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return balance.getBalance();
	}

	@Override
	public NetworkBalance getNetworkBalance(Network network) {
		NetworkBalance balance = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		BeanProcessor b = new BeanProcessor();

		try {
			conn = dbCredentials.getConnection();

			// Get all balances on the account
			pstmt = conn.prepareStatement("SELECT * FROM networkbalancebycountry WHERE networkUuid=?;");
			pstmt.setString(1, network.getUuid());
			rset = pstmt.executeQuery();

			while (rset.next()) {
				balance = b.toBean(rset, NetworkBalance.class);
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting balances of '" + network + "'.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return balance;
	}

	@Override
	public List<NetworkBalance> getAllNetworkBalance() {
		List<NetworkBalance> list = new ArrayList<>();
		NetworkBalance bl;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		BeanProcessor b = new BeanProcessor();

		try {
			conn = dbCredentials.getConnection();

			// Get all balances on the account
			pstmt = conn.prepareStatement("SELECT * FROM networkbalancebycountry;");
			rset = pstmt.executeQuery();

			while (rset.next()) {
				bl = b.toBean(rset, NetworkBalance.class);
				// bl.setId(rset.getInt("balanceId"));

				list.add(bl);
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting network balance");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<NetworkBalance> getAllNetworkBalance(int fromIndex, int toIndex) {
		List<NetworkBalance> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM networkbalancebycountry ORDER BY networkuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, NetworkBalance.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all networkbalancebycountry from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

}

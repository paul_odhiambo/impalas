package com.impalapay.mno.persistence.accountmgmt.balance;

import com.impalapay.beans.network.Network;
import com.impalapay.mno.beans.accountmgmt.balance.NetworkBalance;

import java.util.List;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

/**
 * Tests the {@link NetworkBalanceDAO}
 * <p>
 * Copyright (c) ImpalaPay LTD., Sep 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class TestNetworkBalanceDAO {

	final String DB_NAME = "remittancedb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "remittance";
	final String DB_PASSWD = "gertedNen2";
	final int DB_PORT = 5432;

	// Account holders' Uuids
	public static final String DEMO = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	public static final String KAKUZI = "48e249c2-856a-4269-820f-7b72c76b4957";
	public static final String BLUE_TRIANGLE = "81bf3078-4495-4bec-a50d-c91a7c512d78";
	public static final String SOFT_TOUCH = "7967107d-d61c-43dd-bc5b-aa12fd08497b";
	public static final String MOBISOKO = "91fc8aae-cb76-4c64-ac45-48448fb5673f";

	final String CLIENTBALANCE_UUID = "61a86ead-98a4-4bc6-b00f-3028e61abc69";
	final String CLIENTBALANCE_ACCOUNT_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final int CLIENTBALANCE_AMOUNT = 48_079_021;

	final int ALL_CLIENTS_BALANCE_COUNT = 150;
	final double AMOUNT = 100000;
	final int AMOUNT2 = 46271257;

	private NetworkBalanceDAO storage = new NetworkBalanceDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

	// test for master accounts balances
	@Ignore
	@Test
	public void testGetAllMasterBalance() {

		int clientCount = 5;
		List<NetworkBalance> allmasterBalances = storage.getAllNetworkBalance();
		for (NetworkBalance data : allmasterBalances) {
			System.out.println(data);
		}
		assertEquals(allmasterBalances.size(), clientCount);

	}

	@Ignore
	@Test
	public void testGetMasterAccountBalance() {

		Network network = new Network();
		network.setUuid("81bf3078-4495-4bec-a50d-c91a7c512d78");
		int clientCount = 1;

		NetworkBalance clientBalancesbyuuid = storage.getNetworkBalance(network);

		System.out.println(clientBalancesbyuuid.getBalance());

		// assertEquals(clientBalances.size(), clientCount);

	}

	@Ignore
	@Test
	public void testDeductBalance() {

		Network network = new Network();
		network.setUuid("81bf3078-4495-4bec-a50d-c91a7c512d78");

		assertTrue(storage.deductBalance(network, AMOUNT));
	}

	// @Ignore
	@Test
	public void testAddBalance() {
		Network network = new Network();
		network.setUuid("81bf3078-4495-4bec-a50d-c91a7c512d78");

		assertTrue(storage.addBalance(network, AMOUNT));
	}

	@Ignore
	@Test
	public void testHasBalance() {
		Network network = new Network();
		network.setUuid("81bf3078-4495-4bec-a50d-c91a7c512d78");
		;

		assertTrue(storage.hasBalance(network, AMOUNT));
		// assertFalse(storage.hasBalance(account,country, 56_271_259));
	}

}

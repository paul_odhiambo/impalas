package com.impalapay.mno.persistence.prefix;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.beans.network.Network;
import com.impalapay.beans.prefix.PrefixSplit;

public class PrefixDAO extends GenericDAO implements ImpalaPayPrefixDAO {
	private static PrefixDAO prefixDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return CountryMsisdnDAO
	 */
	public static PrefixDAO getInstance() {
		if (prefixDAO == null) {
			prefixDAO = new PrefixDAO();
		}

		return prefixDAO;
	}

	protected PrefixDAO() {
		super();
	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public PrefixDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

	}

	/**
	 * 
	 */
	@Override
	public PrefixSplit getPrefixSplit(String uuid) {
		PrefixSplit s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM prefix WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, PrefixSplit.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting prefix with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	/**
	 * 
	 */
	@Override
	public boolean putPrefixSplit(PrefixSplit prefixsplit) {
		boolean success = true;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("INSERT INTO prefix(uuid,networkuuid,countryuuid,prefix,"
					+ "wallettype,dateadded) VALUES (?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, prefixsplit.getUuid());
			pstmt.setString(2, prefixsplit.getNetworkUuid());
			pstmt.setString(3, prefixsplit.getCountryUuid());
			pstmt.setString(4, prefixsplit.getPrefix());
			pstmt.setString(5, prefixsplit.getWalletType());
			// pstmt.setInt(6, prefixsplit.getSplitLength());
			pstmt.setTimestamp(6, new Timestamp(prefixsplit.getDateadded().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + prefixsplit);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	/**
	 * 
	 */
	@Override
	public boolean updatePrefixSplit(String uuid, PrefixSplit a) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM prefix WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				pstmt2 = conn.prepareStatement("UPDATE prefix SET networkuuid=?,countryuuid=?,prefix=?,wallettype=?,"
						+ "dateadded=?" + "WHERE uuid=?;");
				pstmt2.setString(1, a.getNetworkUuid());
				pstmt2.setString(2, a.getCountryUuid());
				pstmt2.setString(3, a.getPrefix());
				pstmt2.setString(4, a.getWalletType());
				pstmt2.setTimestamp(5, new Timestamp(a.getDateadded().getTime()));
				pstmt2.setString(6, a.getUuid());

				pstmt2.executeUpdate();

			} else {
				success = putPrefixSplit(a);
			}

		} catch (SQLException e) {
			logger.error("SQLException when trying to update prefix with uuid '" + uuid + "' with " + a + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<PrefixSplit> getAllPrefixSplit() {
		List<PrefixSplit> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM prefix ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, PrefixSplit.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all prefix.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<PrefixSplit> getAllPrefixSplit(int fromIndex, int toIndex) {
		List<PrefixSplit> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM prefix ORDER BY countryuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, PrefixSplit.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all prefixes from index " + fromIndex + " to index "
					+ toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public PrefixSplit getprefixsplit(Network network, Country country, String prefix) {
		PrefixSplit s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM prefix WHERE networkuuid = ? AND countryuuid=? AND prefix=?;");
			pstmt.setString(1, network.getUuid());
			pstmt.setString(2, country.getUuid());
			pstmt.setString(3, prefix);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, PrefixSplit.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting prefix with uuid '" + prefix + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public PrefixSplit getprefixsplit(String prefix) {
		PrefixSplit s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM prefix WHERE prefix = ?;");
			pstmt.setString(1, prefix);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, PrefixSplit.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting prefix with prefix '" + prefix + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public boolean deleteprefix(String uuid) {
		boolean success = true;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("DELETE FROM prefix WHERE uuid=?;");

			pstmt.setString(1, uuid);

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while Deleting " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	/**
	 * 
	 */
	@Override
	public List<PrefixSplit> getPrefixSplits(String prefix, String countryuuid) {
		List<PrefixSplit> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM prefix WHERE prefix=? AND countryuuid=? ORDER BY id ASC;");
			pstmt.setString(1, prefix);
			pstmt.setString(2, countryuuid);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, PrefixSplit.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all prefixes.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<PrefixSplit> getPrefixSplits(String prefix, String countryuuid, String wallettype) {
		// TODO Auto-generated method stub
		List<PrefixSplit> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM prefix WHERE prefix=? AND countryuuid=? AND wallettype=? ORDER BY id ASC;");
			pstmt.setString(1, prefix);
			pstmt.setString(2, countryuuid);
			pstmt.setString(3, wallettype);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, PrefixSplit.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all prefixes.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public PrefixSplit getprefixsplit(Network network, Country country, String prefix, String wallettype) {
		// TODO Auto-generated method stub
		PrefixSplit s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM prefix WHERE networkuuid = ? AND countryuuid=? AND prefix=? AND wallettype=?;");
			pstmt.setString(1, network.getUuid());
			pstmt.setString(2, country.getUuid());
			pstmt.setString(3, prefix);
			pstmt.setString(4, wallettype);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, PrefixSplit.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting prefix with uuid '" + prefix + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public PrefixSplit getPrefixSplits1(String prefix, String wallettype) {
		// TODO Auto-generated method stub
		PrefixSplit s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM prefix WHERE prefix=? AND wallettype=? ORDER BY id ASC;");
			pstmt.setString(1, prefix);
			pstmt.setString(2, wallettype);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, PrefixSplit.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting all prefixes.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

}

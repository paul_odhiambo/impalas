package com.impalapay.mno.persistence.prefix;

import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.beans.network.Network;
import com.impalapay.beans.prefix.PrefixSplit;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

/**
 * Tests the com.impalapay.airtel.persistence.country.CountryDAO
 * <p>
 * Copyright (c) impalapay Ltd., june 24, 2014
 * 
 * @author <a href="mailto:eugenechimita@impalapay.com">Eugene Chimita</a>
 * @author <a href="mailto:michael@impalapay.com">Michael Wakahe</a>
 * 
 */
public class TestPrefixDAO {

	final String DB_NAME = "remittancedb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "remittance";
	final String DB_PASSWD = "gertedNen2";
	final int DB_PORT = 5432;

	final String Balance_IP = "20";
	final String UUID = "1hjhyiyuiyiyiy8d0100249dba8ae6r";

	final String Country_UUID = "d4a676822f4546a0bee789e83070f788";
	final String Network_UUID = "81bf3078-4495-4bec-a50d-c91a7c512d78";
	final String Prefix = "100";
	final String Wallet_Type = "99";
	final int Splitlength = 20;

	final String Account_UUID2 = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String UUID2 = "3ec83cb1-b030-44be-a8bc-0df73d0628bf";
	final String Country_MSISDN2 = "25473348678";
	final String Country_UUID2 = "5db5fa02790e4ee0a8d7a538b4df820a";

	final int Country_COUNT = 17;

	private PrefixDAO storage;

	/**
	 * Test method for
	 * com.impalapay.airtel.persistence.country.CountryDAO#getCountry(java.lang.
	 * String).
	 */
	@Ignore
	@Test
	public void testPrefixSplitString() {
		storage = new PrefixDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		PrefixSplit prefixsplit = storage.getPrefixSplit(UUID);
		assertEquals(prefixsplit.getUuid(), UUID);
		assertEquals(prefixsplit.getNetworkUuid(), Network_UUID);
		assertEquals(prefixsplit.getCountryUuid(), Country_UUID);
		assertEquals(prefixsplit.getWalletType(), Wallet_Type);
		// assertEquals(prefixsplit.getSplitLength(), Splitlength);

	}

	@Ignore
	@Test
	public void testPrefixSplit() {
		storage = new PrefixDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);
		PrefixSplit prefixsplit = new PrefixSplit();

		prefixsplit.setUuid("245342525");
		prefixsplit.setNetworkUuid(Network_UUID);
		prefixsplit.setCountryUuid(Country_UUID);
		prefixsplit.setWalletType("365tweywey");
		// prefixsplit.setSplitLength(Splitlength);

		assertTrue(storage.putPrefixSplit(prefixsplit));

	}

	@Ignore
	@Test
	public void testUpdatePrefixSplit() {
		storage = new PrefixDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);
		PrefixSplit prefixsplit = new PrefixSplit();

		prefixsplit.setUuid("245342525");
		prefixsplit.setNetworkUuid(Network_UUID);
		prefixsplit.setCountryUuid(Country_UUID);
		prefixsplit.setWalletType("365tweywey");
		// prefixsplit.setSplitLength(Splitlength);
		prefixsplit.setPrefix("256715");

		assertTrue(storage.updatePrefixSplit("245342525", prefixsplit));

	}

	@Ignore
	@Test
	public void testPrefixSplit2() {
		storage = new PrefixDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);
		// PrefixSplit prefixsplit = new PrefixSplit();

		System.out.println(storage.getAllPrefixSplit(0, 2));

	}

	@Ignore
	@Test
	public void testGetPrefixSplit() {
		storage = new PrefixDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		Network network = new Network();
		network.setUuid("81bf3078-4495-4bec-a50d-c91a7c512d78");
		Country country = new Country();
		country.setUuid("d4a676822f4546a0bee789e83070f788");

		System.out.println(storage.getprefixsplit(network, country, "25472"));

	}

	@Ignore
	@Test
	public void testGetPrefix() {
		storage = new PrefixDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);
		System.out.println(storage.getprefixsplit("254715"));

	}

	// @Ignore
	@Test
	public void testGetPrefixes() {
		storage = new PrefixDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		List<PrefixSplit> results = storage.getPrefixSplits("25471", "d4a676822f4546a0bee789e83070f788");

		for (PrefixSplit result : results) {

			System.out.println(result.getNetworkUuid());
		}

	}

}

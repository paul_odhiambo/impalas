package com.impalapay.mno.persistence.network;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.beans.network.Network;
import com.impalapay.beans.prefix.PrefixSplit;

public class NetworkDAO extends GenericDAO implements ImpalaPayNetworkDAO {
	private static NetworkDAO networkDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return CountryMsisdnDAO
	 */
	public static NetworkDAO getInstance() {
		if (networkDAO == null) {
			networkDAO = new NetworkDAO();
		}

		return networkDAO;
	}

	protected NetworkDAO() {
		super();
	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public NetworkDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

	}

	/**
	 * 
	 */
	@Override
	public Network getNetwork(String uuid) {
		Network s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM network WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, Network.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting network with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	/**
	 * 
	 */
	@Override
	public boolean putNetwork(Network network) {
		boolean success = true;
		Date date;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("INSERT INTO network(uuid,countryuuid,remitip,bridgeremitip,"
					+ "queryip,bridgequeryip,balanceip,bridgebalanceip,reversalip,bridgereversalip,forexip,bridgeforexip,accountcheckip,bridgeaccountcheckip,extraurl,"
					+ "username,password,networkname,partnername,commission,supportforex,supportreversal,supportaccountcheck,supportquerycheck,supportbalancecheck,supportcommissionpercentage,networkStatusUuid,dateadded) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, network.getUuid());
			pstmt.setString(2, network.getCountryUuid());
			pstmt.setString(3, network.getRemitip());
			pstmt.setString(4, network.getBridgeremitip());
			pstmt.setString(5, network.getQueryip());
			pstmt.setString(6, network.getBridgequeryip());
			pstmt.setString(7, network.getBalanceip());
			pstmt.setString(8, network.getBridgebalanceip());
			pstmt.setString(9, network.getReversalip());
			pstmt.setString(10, network.getBridgereversalip());
			pstmt.setString(11, network.getForexip());
			pstmt.setString(12, network.getBridgeforexip());
			pstmt.setString(13, network.getAccountcheckip());
			pstmt.setString(14, network.getBridgeaccountcheckip());
			pstmt.setString(15, network.getExtraurl());
			pstmt.setString(16, network.getUsername());
			pstmt.setString(17, network.getPassword());
			pstmt.setString(18, network.getNetworkname());
			pstmt.setString(19, network.getPartnername());
			pstmt.setDouble(20, network.getCommission());
			pstmt.setBoolean(21, network.isSupportforex());
			pstmt.setBoolean(22, network.isSupportreversal());
			pstmt.setBoolean(23, network.isSupportreversal());
			pstmt.setBoolean(24, network.isSupportquerycheck());
			pstmt.setBoolean(25, network.isSupportbalancecheck());
			pstmt.setBoolean(26, network.isSupportcommissionpercentage());
			pstmt.setString(27, network.getNetworkStatusUuid());
			pstmt.setTimestamp(28, new Timestamp(network.getDateadded().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + network);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	/**
	 * 
	 */
	@Override
	public boolean updateNetwork(String uuid, Network a) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM network WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				pstmt2 = conn.prepareStatement(
						"UPDATE network SET countryuuid=?,remitip=?,bridgeremitip=?,queryip=?,bridgequeryip=?,balanceip=?,bridgebalanceip=?,reversalip=?,bridgereversalip=?,forexip=?,bridgeforexip=?,accountcheckip=?,bridgeaccountcheckip=?,extraurl=?,"
								+ "username=?,password=?,networkname=?,partnername=?,commission=?,supportforex=?,supportreversal=?,supportaccountcheck=?,supportquerycheck=?,supportbalancecheck=?,supportcommissionpercentage=?,networkStatusUuid=?,dateadded=?"
								+ "WHERE uuid=?;");

				pstmt2.setString(1, a.getCountryUuid());
				pstmt2.setString(2, a.getRemitip());
				pstmt2.setString(3, a.getBridgeremitip());
				pstmt2.setString(4, a.getQueryip());
				pstmt2.setString(5, a.getBridgequeryip());
				pstmt2.setString(6, a.getBalanceip());
				pstmt2.setString(7, a.getBridgebalanceip());
				pstmt2.setString(8, a.getReversalip());
				pstmt2.setString(9, a.getBridgereversalip());
				pstmt2.setString(10, a.getForexip());
				pstmt2.setString(11, a.getBridgeforexip());
				pstmt2.setString(12, a.getAccountcheckip());
				pstmt2.setString(13, a.getBridgeaccountcheckip());
				pstmt2.setString(14, a.getExtraurl());
				pstmt2.setString(15, a.getUsername());
				pstmt2.setString(16, a.getPassword());
				pstmt2.setString(17, a.getNetworkname());
				pstmt2.setString(18, a.getPartnername());
				pstmt2.setDouble(19, a.getCommission());
				pstmt2.setBoolean(20, a.isSupportforex());
				pstmt2.setBoolean(21, a.isSupportreversal());
				pstmt2.setBoolean(22, a.isSupportaccountcheck());
				pstmt2.setBoolean(23, a.isSupportquerycheck());
				pstmt2.setBoolean(24, a.isSupportbalancecheck());
				pstmt2.setBoolean(25, a.isSupportcommissionpercentage());
				pstmt2.setString(26, a.getNetworkStatusUuid());
				pstmt2.setTimestamp(27, new Timestamp(a.getDateadded().getTime()));
				pstmt2.setString(28, a.getUuid());

				pstmt2.executeUpdate();

			} else {
				success = putNetwork(a);
			}

		} catch (SQLException e) {
			logger.error("SQLException when trying to update network with uuid '" + uuid + "' with " + a + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<Network> getAllNetwork() {
		List<Network> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM network ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, Network.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all network.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<Network> getAllNetwork(int fromIndex, int toIndex) {
		List<Network> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM network ORDER BY countryuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, Network.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all network from index " + fromIndex + " to index "
					+ toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	/**
	 * 
	 */
	@Override
	public Network getNetwork(Network name) {
		Network s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM network WHERE networkname = ?;");
			pstmt.setString(1, name.getNetworkname());

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, Network.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting network with uuid '" + name.getUuid() + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public List<Network> getAllNetwork(String uuid, int fromIndex, int toIndex) {
		List<Network> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn
					.prepareStatement("SELECT * FROM network WHERE uuid=? ORDER BY countryuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setString(1, uuid);
			pstmt.setInt(2, toIndex - fromIndex);
			pstmt.setInt(3, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, Network.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all network from index " + fromIndex
					+ " to index by uuid " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

}

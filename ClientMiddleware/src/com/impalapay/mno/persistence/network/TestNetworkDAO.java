package com.impalapay.mno.persistence.network;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.beans.network.Network;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Ignore;

import java.util.Date;
import java.util.List;

/**
 * Tests the com.impalapay.airtel.persistence.country.CountryDAO
 * <p>
 * Copyright (c) impalapay Ltd., june 24, 2014
 * 
 * @author <a href="mailto:eugenechimita@impalapay.com">Eugene Chimita</a>
 * @author <a href="mailto:michael@impalapay.com">Michael Wakahe</a>
 * 
 */
public class TestNetworkDAO {

	final String DB_NAME = "remittancedb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "remittance";
	final String DB_PASSWD = "gertedNen2";
	final int DB_PORT = 5432;

	final String Balance_IP = "20";
	final String UUID = "81bf3078-4495-4bec-a50d-c91a7c512d78";

	final String Country_UUID = "d4a676822f4546a0bee789e83070f788";
	final String Remit_Ip = "1234646";
	final String Query_Ip = "1234646";
	final String Balance_Ip = "1234646";
	final String Reversal_Ip = "1234646";
	final String Forex_Ip = "1234646";
	final String Accountcheck_Ip = "1234646";
	final String extra_Url = "1234646";
	final String username = "njkljlk";
	final String password = "njkljlk";
	final String partnername = "njkljlk";
	final boolean supportforex = true;
	final boolean supportreversal = true;
	final boolean supportaccountcheck = true;

	final String Account_UUID2 = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String UUID2 = "3ec83cb1-b030-44be-a8bc-0df73d0628bf";
	final String Country_MSISDN2 = "25473348678";
	final String Country_UUID2 = "5db5fa02790e4ee0a8d7a538b4df820a";

	final int Country_COUNT = 17;

	private NetworkDAO storage;

	/**
	 * Test method for
	 * com.impalapay.airtel.persistence.country.CountryDAO#getCountry(java.lang.
	 * String).
	 */
	@Ignore
	@Test
	public void testNetworkString() {
		storage = new NetworkDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		Network network = storage.getNetwork(UUID);
		assertEquals(network.getUuid(), UUID);
		assertEquals(network.getBalanceip(), Balance_IP);
		assertEquals(network.getCountryUuid(), Country_UUID);
		assertEquals(network.getUsername(), username);

	}

	@Ignore
	@Test
	public void testAddNetwork() {
		storage = new NetworkDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);
		Network network = new Network();
		network.setUuid("52365263526356");
		network.setCountryUuid(Country_UUID);
		network.setRemitip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setBridgeremitip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setQueryip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setBridgequeryip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setBalanceip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setBridgebalanceip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setReversalip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setBridgereversalip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setForexip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setBridgeforexip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setAccountcheckip("https://localhost:8456/AirtelRemittanceImpalas/comvivaverify");
		network.setBridgeaccountcheckip("https://localhost:8456/AirtelRemittanceImpalas/comvivaverify");
		network.setExtraurl("wqu28");
		network.setUsername(DB_USERNAME);
		network.setPassword(password);
		network.setNetworkname("AIRTEL");
		network.setPartnername("AIRTEL-KENYA");
		network.setSupportforex(true);
		network.setSupportreversal(true);
		network.setSupportaccountcheck(true);
		network.setSupportquerycheck(false);
		network.setSupportbalancecheck(true);
		network.setSupportcommissionpercentage(true);
		network.setCommission(50);
		network.setNetworkStatusUuid("123445565656555");

		assertTrue(storage.putNetwork(network));

	}

	// @Ignore
	@Test
	public void testUpdateNetwork() {
		storage = new NetworkDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);
		Network network = new Network();
		network.setUuid("52365263526356");
		network.setCountryUuid(Country_UUID);
		network.setRemitip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setBridgeremitip("https://localhost:8456/AirtelRemittanceImpalas/airtelAfricaTransfer");
		network.setQueryip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setBridgequeryip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setBalanceip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setBridgebalanceip("https://localhost:8456/AirtelRemittanceImpalas/airtelAfricaTransfer");
		network.setReversalip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setBridgereversalip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setForexip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setBridgeforexip("https://localhost:8456/AirtelRemittanceImpalas/comviva");
		network.setAccountcheckip("https://localhost:8456/AirtelRemittanceImpalas/comvivaverify");
		network.setBridgeaccountcheckip("https://localhost:8456/AirtelRemittanceImpalas/comvivaverify");
		network.setExtraurl("wqu28");
		network.setUsername(DB_USERNAME);
		network.setPassword(password);
		network.setNetworkname("AIRTEL");
		network.setPartnername("AIRTEL-KENYA");
		network.setSupportforex(Boolean.parseBoolean("false"));
		network.setSupportreversal(Boolean.parseBoolean("true"));
		network.setSupportaccountcheck(Boolean.parseBoolean("false"));
		network.setSupportcommissionpercentage(true);
		network.setCommission(20);
		network.setNetworkStatusUuid("12344556565655566");

		assertTrue(storage.updateNetwork("52365263526356", network));

	}

}

package com.impalapay.mno.servlet.api.remit;

import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressBalancebyCountryHoldHist;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressMasterBalanceHoldHist;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.persistence.accountmgmt.inprogressbalance.InProgressBalanceDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;
import com.impalapay.mno.persistence.accountmgmt.balance.AccountBalanceDAO;

/**
 * Responsible to dispatch a new Session Id to a client URL.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class AsyncTransactionDispatcher extends Thread {
	private Account account;
	private Country coutruuuid;
	private TransactionForexrate transactionforexrate;
	private InProgressMasterBalanceHoldHist masterPurchase;
	private InProgressBalancebyCountryHoldHist balancebycountry;
	private AccountBalanceDAO accountbalanceDAO;
	private TransactionForexDAO transactionforexDAO;
	private InProgressBalanceDAO inprogressbalanceDAO;
	private double convertedamount, originateamount;
	private String originatecurrency = "";

	/**
	 * 
	 */
	private AsyncTransactionDispatcher() {
	}

	/**
	 * @param account
	 */
	public AsyncTransactionDispatcher(TransactionForexrate transactionforexrate) {
		this.transactionforexrate = transactionforexrate;
		masterPurchase = new InProgressMasterBalanceHoldHist();
		balancebycountry = new InProgressBalancebyCountryHoldHist();
		transactionforexDAO = TransactionForexDAO.getinstance();
		accountbalanceDAO = AccountBalanceDAO.getInstance();
		inprogressbalanceDAO = InProgressBalanceDAO.getInstance();
		convertedamount = 0;
		originateamount = 0;
	}

	/**
	 * 
	 */
	@Override
	public void run() {

		ReentrantLock lock = new ReentrantLock();
		lock.lock();

		try {

			System.out.println("resource locked " + lock.isHeldByCurrentThread());
			account = new Account();
			account.setUuid(transactionforexrate.getAccount());
			coutruuuid = new Country();
			coutruuuid.setUuid(transactionforexrate.getRecipientcountry());

			convertedamount = transactionforexrate.getConvertedamount();
			originateamount = transactionforexrate.getLocalamount();
			originatecurrency = transactionforexrate.getAccounttype();

			transactionforexDAO.addTransactionForex(transactionforexrate);

			masterPurchase.setUuid(UUID.randomUUID().toString());
			masterPurchase.setAccountUuid(transactionforexrate.getAccount());
			masterPurchase.setCurrency(originatecurrency);
			masterPurchase.setAmount(originateamount);
			masterPurchase.setTopuptime(transactionforexrate.getServerTime());
			masterPurchase.setTransactionuuid(transactionforexrate.getTransactionUuid());
			masterPurchase.setRefundedback(false);
			masterPurchase.setProcessed(false);

			balancebycountry.setUuid(UUID.randomUUID().toString());
			balancebycountry.setAccountUuid(transactionforexrate.getAccount());
			balancebycountry.setCountryuuid(transactionforexrate.getRecipientcountry());
			balancebycountry.setAmount(convertedamount);
			balancebycountry.setTopuptime(transactionforexrate.getServerTime());
			balancebycountry.setTransactionuuid(transactionforexrate.getTransactionUuid());
			balancebycountry.setRefundedback(false);
			balancebycountry.setProcessed(false);
			// deduct the balance.

			if (accountbalanceDAO.deductBalanceByCountry(account, coutruuuid, convertedamount)
					&& accountbalanceDAO.deductBalance(account, originateamount, originatecurrency)) {
				// put the balance on the various tables.
				inprogressbalanceDAO.putBalanceOnHoldAccount(masterPurchase, balancebycountry);
			}
			//

		} finally {
			System.out.println("resource locked finally released ");
			lock.unlock();

		}

	}

}

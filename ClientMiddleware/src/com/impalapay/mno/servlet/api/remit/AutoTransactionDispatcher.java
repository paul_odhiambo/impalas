package com.impalapay.mno.servlet.api.remit;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.mno.persistence.accountmgmt.balance.AccountBalanceDAO;
import com.impalapay.airtel.persistence.accountmgmt.inprogressbalance.InProgressBalanceDAO;
import com.impalapay.airtel.persistence.commission.CommissionDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;
import com.impalapay.beans.commission.CommisionEstimate;
import com.impalapay.beans.network.Network;
import com.impalapay.beans.route.RouteDefine;
import com.impalapay.mno.persistence.accountmgmt.balance.NetworkBalanceDAO;
import com.impalapay.mno.persistence.network.NetworkDAO;
import com.impalapay.persistence.routing.RouteDAO;

/**
 * Responsible to dispatch a new Session Id to a client URL.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class AutoTransactionDispatcher extends Thread {

	private Account account;
	private Country coutruuuid;
	private Network network, testnetwork;
	private CommisionEstimate commisionEstimate;

	private Transaction transaction;
	private TransactionForexrate transactionforexrate;
	private TransactionForexDAO transactionforexDAO;
	private NetworkBalanceDAO networkbalanceDAO;
	private CommissionDAO commissionDAO;
	private InProgressBalanceDAO inprogressbalanaceDAO;

	private HashMap<String, String> routeaccountnetworkmap = new HashMap<>();
	private HashMap<String, Boolean> routenetworkuuidmap = new HashMap<>();
	private HashMap<String, Boolean> routenetcommisiontypemap = new HashMap<>();
	private HashMap<String, Double> routenetcommisionmap = new HashMap<>();

	private NetworkDAO networkDAO;
	private RouteDAO routeDAO;
	private double commission;
	private double sentbalance;
	private double fullbalance;
	private String value = "", networkrouteuuid = "", commissiontransactioinid = "", originatecurrency = "",
			status = "", realtransactionid = "";
	private double switchcommission, switchcommissionrate, receivercommission, convertedamount, originateamount;
	private boolean fixedcommission;

	/**
	 * 
	 */
	private AutoTransactionDispatcher() {
	}

	/**
	 * @param account
	 */
	public AutoTransactionDispatcher(Transaction transaction, TransactionForexrate transactionforexrate) {
		this.transaction = transaction;
		this.transactionforexrate = transactionforexrate;
		inprogressbalanaceDAO = InProgressBalanceDAO.getInstance();
		transactionforexDAO = TransactionForexDAO.getinstance();
		networkbalanceDAO = NetworkBalanceDAO.getInstance();
		networkDAO = NetworkDAO.getInstance();
		routeDAO = RouteDAO.getInstance();
		commissionDAO = CommissionDAO.getInstance();
		commission = 0;
		sentbalance = 0;
		fullbalance = 0;
		switchcommission = 0;
		switchcommissionrate = 0;
		receivercommission = 0;
		convertedamount = 0;
		originateamount = 0;

	}

	/**
	 * 
	 */
	@Override
	public void run() {
		network = new Network();
		network.setUuid(transaction.getNetworkuuid());

		account = new Account();
		account.setUuid(transaction.getAccountUuid());
		// ################################################################
		// Fetch route set up details
		// #################################################################
		List<RouteDefine> routedefine = routeDAO.getAllRoute(account);

		for (RouteDefine routenetworkuuid : routedefine) {
			routeaccountnetworkmap.put(routenetworkuuid.getNetworkUuid(), routenetworkuuid.getUuid());
			routenetworkuuidmap.put(routenetworkuuid.getUuid(), routenetworkuuid.isSupportforex());
			routenetcommisiontypemap.put(routenetworkuuid.getUuid(), routenetworkuuid.isFixedcommission());
			routenetcommisionmap.put(routenetworkuuid.getUuid(), routenetworkuuid.getCommission());
		}

		coutruuuid = new Country();
		coutruuuid.setUuid(transaction.getRecipientCountryUuid());
		testnetwork = networkDAO.getNetwork(transaction.getNetworkuuid());

		commission = testnetwork.getCommission();
		sentbalance = transaction.getAmount();

		// ********************************************
		// Variable assignment and definitions
		// ********************************************
		status = "acecb9fa-7e21-455d-8abb-c61a840cdbec";
		commissiontransactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');
		realtransactionid = transaction.getUuid();
		convertedamount = transaction.getAmount();
		originateamount = transactionforexrate.getLocalamount();
		originatecurrency = transactionforexrate.getAccounttype();

		if (testnetwork.isSupportcommissionpercentage()) {
			receivercommission = (commission * sentbalance) / 100;
			fullbalance = receivercommission + sentbalance;
			value = "percent of";

		} else {
			receivercommission = commission;
			fullbalance = sentbalance + receivercommission;

		}

		// **************************************************
		// Update the temp balance tables on success
		// **************************************************
		inprogressbalanaceDAO.removeBalanceHoldSuccess(realtransactionid);

		// accountbalanceDAO.deductBalanceByCountry(account, coutruuuid,
		// convertedamount);//to be removed in asynchronous version
		// transactionforexDAO.addTransactionForex(transactionforexrate);

		try {
			networkbalanceDAO.deductBalance(network, fullbalance);

			System.out.println("the amount sent is " + convertedamount + " the commission charged is " + commission
					+ " " + value + " the total amount deducted is " + fullbalance);

		} catch (Exception e) {
			System.out.println("network balance is below limit");
		}

		// check if the network is already configured in the routes table if not
		networkrouteuuid = routeaccountnetworkmap.get(transaction.getNetworkuuid());
		System.out.println("This is the network uuid checks " + networkrouteuuid);
		fixedcommission = routenetcommisiontypemap.get(networkrouteuuid);
		System.out.println("is it fixed commission " + fixedcommission);
		switchcommissionrate = routenetcommisionmap.get(networkrouteuuid);
		// boolean forexstatus = routenetworkuuidmap.get(networkrouteuuid);

		if (fixedcommission) {
			switchcommission = switchcommissionrate;
		} else {
			switchcommission = (switchcommissionrate * originateamount) / 100;
		}

		// if(accountbalanceDAO.deductBalance(account, originateamount,
		// originatecurrency)) {//to be removed in asynchronous version
		// add the switch commission
		commisionEstimate = new CommisionEstimate();
		commisionEstimate.setUuid(commissiontransactioinid);
		commisionEstimate.setTransactionUuid(realtransactionid);
		commisionEstimate.setCommissioncurrency(originatecurrency);
		commisionEstimate.setCommision(switchcommission);
		commisionEstimate.setReceivercommission(receivercommission);
		commisionEstimate.setReceiverfullamount(fullbalance);
		commisionEstimate.setStatusuuid(status);

		if (commissionDAO.putCommission(commisionEstimate)) {
			System.out.println("added commission successfully");
		}

		// } else {
		// System.out.println("error in deducting");
		// }

	}
}

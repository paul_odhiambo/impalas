package com.impalapay.mno.servlet.api.remit;

import com.impalapay.airtel.servlet.api.APIConstants;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static org.junit.Assert.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

/**
 * Tests the {@link SendMoney}
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class TestSendMoney {

	final String CGI_URL = "https://localhost:8456/ImpalasRemittance/mnoTransfer";
	// final String CGI_URL = "https://airtel2.tawi.mobi/remit";

	// final String CGI_URL = "https://localhost:8443/AirtelRemittance2/remit";

	//final String CGI_URL = "https://staging.impalapay.net/mnoTransfer";

	/**
	 * Test method for
	 * {@link com.impalapay.airtel.servlet.api.status.QueryStatus#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 * .
	 */
	@Test
	public void testDoPostHttpServletRequestHttpServletResponse() {

		Gson g = new Gson();

		Map<String, String> user3 = new HashMap<>();

		String transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');
		user3.put("api_username", "eugenechimita");
		user3.put("session_id", "917684ce07ca4071969d746b7a670a16");
		user3.put("source_country_code", "AU");
		user3.put("sendername", "Vallery mundal");
		user3.put("recipient_mobile", "254715290374");// 254731520630 //254733159840
		user3.put("amount", "1");
		user3.put("recipient_currency_code", "KES");
		user3.put("recipient_country_code", "KE");
		user3.put("reference_number", transactioinid);
		user3.put("sendertoken", "6jhgjhjjj4");
		user3.put("client_datetime", "2017-06-08T18:44:27+11:00");

		String jsonData3 = g.toJson(user3);

	
		
		
		for( int i=0;i<1000;i++){
			
			
			
			System.out.println(getResponse(CGI_URL, jsonData3));
			
		}
		
		
	}

	/**
	 * @param httpsUrl
	 * @param args
	 */
	
	private String getResponse(String httpsUrl, String args) {
		URL url;
		String response = "";

		try {
			// Create a context that doesn't check certificates.
			SSLContext sslContext = SSLContext.getInstance("TLS");
			TrustManager[] trustMgr = getTrustManager();

			sslContext.init(null, // key manager
					trustMgr, // trust manager
					new SecureRandom()); // random number generator
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

			url = new URL(httpsUrl);
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			con.setRequestMethod("POST");
			con.setDoOutput(true);

			// Guard against "bad hostname" errors during handshake.
			con.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost")) {
						return true;
					} else {
						return false;
					}
				}
			});

			// Send data to the output
			sendData(con, args);

			// Dump all cert info
			// printHttpsCert(con);

			// Dump all the content
			response = getContent(con);

		} catch (MalformedURLException e) {
			System.err.println("MalformedURLException");
			e.printStackTrace();

		} catch (IOException e) {
			System.err.println("IOException");
			e.printStackTrace();

		} catch (NoSuchAlgorithmException e) {
			System.err.println("NoSuchAlgorithmException");
			e.printStackTrace();

		} catch (KeyManagementException e) {
			System.err.println("KeyManagementException");
			e.printStackTrace();
		}

		return response;
	}

	/**
	 * Send data to the url
	 * 
	 * @param con
	 */
	private void sendData(HttpsURLConnection con, String args) {
		if (con != null) {

			try {
				// send data to output
				OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());

				writer.write(args);
				writer.flush();
				writer.close();

			} catch (IOException e) {
				System.err.println("IOException");
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param con
	 */
	private String getContent(HttpsURLConnection con) {
		StringBuffer buff = new StringBuffer("");

		if (con != null) {

			try {

				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

				String input;

				while ((input = br.readLine()) != null) {
					buff.append(input + "\n");
				}
				br.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		} // end 'if(con != null)'

		return buff.toString().trim();
	}

	/**
	 * @return {@link TrustManager}
	 */
	private TrustManager[] getTrustManager() {

		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };

		return certs;
	}

	/**
	 * @param con
	 */
	private void printHttpsCert(HttpsURLConnection con) {
		if (con != null) {

			try {
				System.out.println("Response Code : " + con.getResponseCode());
				System.out.println("Cipher Suite : " + con.getCipherSuite());
				System.out.println("\n");

				Certificate[] certs = con.getServerCertificates();

				for (Certificate cert : certs) {
					System.out.println("Cert Type : " + cert.getType());
					System.out.println("Cert Hash Code : " + cert.hashCode());
					System.out.println("Cert Public Key Algorithm : " + cert.getPublicKey().getAlgorithm());
					System.out.println("Cert Public Key Format : " + cert.getPublicKey().getFormat());
					System.out.println("\n");
				}

			} catch (SSLPeerUnverifiedException e) {
				System.err.println("SSLPeerUnverifiedException");
				e.printStackTrace();

			} catch (IOException e) {
				System.err.println("IOException");
				e.printStackTrace();
			}

		} // end 'if(con != null)'
	}
}

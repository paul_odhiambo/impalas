package com.impalapay.mno.servlet.api.hubinterface;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JsonCheck {

	public static void main(String[] args) {

		Gson g = new GsonBuilder().setPrettyPrinting().serializeNulls()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();

		JsonObject responseobject = new JsonObject();

		JsonArray responsearray = new JsonArray();

		JsonObject arrayobject = new JsonObject();

		JsonObject senderdata = new JsonObject();

		arrayobject.addProperty("status_code", "S001");
		arrayobject.addProperty("status_description", "PENDING");
		arrayobject.addProperty("title", "Invalid");

		JsonObject erro2 = new JsonObject();

		erro2.addProperty("status_code", "S002");
		erro2.addProperty("status_description", "RECEIVED");
		erro2.addProperty("title", "Invalid");

		responsearray.add(arrayobject);
		responsearray.add(erro2);

		for (int i = 0; i < responsearray.size(); i++) {
			senderdata = responsearray.get(i).getAsJsonObject();
		}

		responseobject.add("errors", responsearray);
		responseobject.add("errors", senderdata);

		String jsonResult = g.toJson(responseobject);

		// Read data drom a json array

		// used to format/join incoming JSon string
		JsonElement roots = new JsonParser().parse(jsonResult);

		JsonObject provitional2 = roots.getAsJsonObject();
		JsonArray jarray = null;

		if (provitional2.has("errors")) {

			if (provitional2.isJsonObject()) {
				try {
					provitional2 = roots.getAsJsonObject().get("errors").getAsJsonObject();
					jsonResult = provitional2.getAsJsonObject().get("status_description").getAsString();
					System.out.println("isjsonobject");

				} catch (Exception e) {
					jarray = provitional2.getAsJsonArray("errors");
					provitional2 = jarray.get(0).getAsJsonObject();
					jsonResult = provitional2.getAsJsonObject().get("status_description").getAsString();
					System.out.println("isjsonArray");
				}

			} else {

			}

			System.out.println(jsonResult);

		} else {

			System.out.println(jsonResult + "veve");
		}

	}

}

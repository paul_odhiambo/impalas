package com.impalapay.mno.servlet.api.hubinterface;

import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;

/**
 * Responsible to dispatch a new Session Id to a client URL.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class AsyncTransactionDispatcher extends Thread {
	private TransactionForexrate transactionforexrate;
	private TransactionForexDAO transactionforexDAO;

	/**
	 * 
	 */
	private AsyncTransactionDispatcher() {
	}

	/**
	 * @param account
	 */
	public AsyncTransactionDispatcher(TransactionForexrate transactionforexrate) {
		this.transactionforexrate = transactionforexrate;

		transactionforexDAO = TransactionForexDAO.getinstance();

	}

	/**
	 * 
	 */
	@Override
	public void run() {
		transactionforexDAO.addTransactionForex(transactionforexrate);

	}
}

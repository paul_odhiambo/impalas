package com.impalapay.mno.servlet.api.hubinterface;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.clientipaddress.ClientIP;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.persistence.transaction.TransactionDAO;
import com.impalapay.airtel.persistence.transaction.TransactionStatusDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryStatus extends HttpServlet {

	private Cache accountsCache, clientIpCache;

	private TransactionDAO transactionDAO;

	private TransactionStatusDAO transactionstatusDAO;

	private HashMap<String, String> clientipHash = new HashMap<>();

	private HashMap<String, String> clientipaccountHash = new HashMap<>();

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);
		clientIpCache = mgr.getCache(CacheVariables.CACHE_IPADDRESS_BY_UUID);

		transactionDAO = TransactionDAO.getInstance();
		transactionstatusDAO = TransactionStatusDAO.getInstance();
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "", jsonResult = "";
		JsonElement root = null;

		// These represent parameters received over the network
		String username = "", referencenumber = "", password = "", sendertransactionid = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new GsonBuilder().setPrettyPrinting().serializeNulls()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		JsonArray responsearray = new JsonArray();
		JsonObject responseobject2 = new JsonObject();
		JsonObject senderdata2 = new JsonObject();
		JsonObject arrayobject = new JsonObject();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			username = root.getAsJsonObject().get("password").getAsString();
			sendertransactionid = root.getAsJsonObject().get("transaction_id").getAsString();
			referencenumber = root.getAsJsonObject().get("hubtransaction_id").getAsString();

		} catch (Exception e) {

			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata2 = responsearray.get(i).getAsJsonObject();
			}
			responseobject2.add("errors", senderdata2);
			jsonResult = g.toJson(responseobject2);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(referencenumber)) {

			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description", APIConstants.COMMANDSTATUS_INVALIDEMPTY_PARAMETERS);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata2 = responsearray.get(i).getAsJsonObject();
			}
			responseobject2.add("errors", senderdata2);
			jsonResult = g.toJson(responseobject2);

			return jsonResult;
		}

		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		if (account == null) {
			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata2 = responsearray.get(i).getAsJsonObject();
			}
			responseobject2.add("errors", senderdata2);
			jsonResult = g.toJson(responseobject2);

			return jsonResult;
		}

		// ip address module
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}

		List keys;

		// fetch from cache
		ClientIP clientIP;
		keys = clientIpCache.getKeys();
		for (Object key : keys) {
			element = clientIpCache.get(key);
			clientIP = (ClientIP) element.getObjectValue();
			clientipHash.put(clientIP.getUuid(), clientIP.getIpAddress());
		}

		keys = clientIpCache.getKeys();
		for (Object key : keys) {
			element = clientIpCache.get(key);
			clientIP = (ClientIP) element.getObjectValue();
			clientipaccountHash.put(clientIP.getIpAddress(), clientIP.getAccountUuid());
		}

		// compare remote address with the one stored in propertiesconfig
		if (!clientipHash.containsValue(ip)) {
			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description", APIConstants.COMMANDSTATUS_INVALID_IPADDRESS + " : " + ip);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata2 = responsearray.get(i).getAsJsonObject();
			}
			responseobject2.add("errors", senderdata2);
			jsonResult = g.toJson(responseobject2);

			return jsonResult;

		}

		// ####################################################################
		// Check if the Provided Ip address matches with account used.
		// ####################################################################

		if (!StringUtils.equalsIgnoreCase(clientipaccountHash.get(ip), account.getUuid())) {
			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description", APIConstants.COMMANDSTATUS_IPADDRESS_MISMATCH);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata2 = responsearray.get(i).getAsJsonObject();
			}
			responseobject2.add("errors", senderdata2);
			jsonResult = g.toJson(responseobject2);

			return jsonResult;
		}

		// At this point we check to see if there is no transaction with the
		// given reference number.
		List<Transaction> transaction = transactionDAO.getTransactionstatus(referencenumber, account);

		if (transaction.size() == 0) {
			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description",
					APIConstants.COMMANDSTATUS_INVALID_REFERENCENUMBER + sendertransactionid);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata2 = responsearray.get(i).getAsJsonObject();
			}
			responseobject2.add("errors", senderdata2);
			jsonResult = g.toJson(responseobject2);

			return jsonResult;

		}

		Transaction transactions = transactionDAO.getTransactionstatus1(referencenumber);
		String transactionref = transactions.getReferenceNumber();

		String transtatusuuid = transactions.getTransactionStatusUuid();
		TransactionStatus transactionstatus = transactionstatusDAO.getTransactionStatus(transtatusuuid);
		String status = transactionstatus.getStatus();
		String statusdescription = transactionstatus.getDescription();

		// This means that everything is ok
		String transactionuuid = transactions.getUuid();
		double amount = transactions.getAmount();
		String finalamount = String.valueOf(amount);
		String sender = transactions.getSenderName();
		// String receivermobile = transactions.getRecipientMobile();

		expected.put("transaction_id", transactionuuid);
		expected.put("status_code", status);
		expected.put("status_description", statusdescription);

		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

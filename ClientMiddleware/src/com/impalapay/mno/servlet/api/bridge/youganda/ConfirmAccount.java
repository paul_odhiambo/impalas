package com.impalapay.mno.servlet.api.bridge.youganda;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;

import com.impalapay.airtel.util.net.PostWithIgnoreSSLYoPayment;
import com.impalapay.airtel.util.net.YoUgandaRequestUtil;

import net.sf.ehcache.CacheManager;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of balance through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class ConfirmAccount extends HttpServlet {

	private PostWithIgnoreSSLYoPayment postMinusThread;

	private Map<String, String> toairtel = new HashMap<>();

	private String CLIENT_URL = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkBalance(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkBalance(HttpServletRequest request) throws IOException {
		Account account = null;
		// joined json string
		String join = "", responseobject = "";

		JsonElement root = null, roots = null;
		
		JsonObject vendorfields = null, root2 = null,queryresponse=null,vendorfield=null;

		// for balance response
		String recipientdetails = "", switchresponse = "", statusdescription = "";

		// These represent parameters received over the network
		String username = "", password = "", receiverqueryurl = "", receivermsisdn = "", firstname = "", lastname = "",
				secondname = "",xmlData="",gender="",firstname2="",lastname2="",results2="";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ##############################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ##############################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			firstname = root.getAsJsonObject().get("first_name").getAsString();
			secondname = root.getAsJsonObject().get("second_name").getAsString();
			lastname = root.getAsJsonObject().get("last_name").getAsString();
			username = root.getAsJsonObject().get("username").getAsString();
			password = root.getAsJsonObject().get("password").getAsString();
			receivermsisdn = root.getAsJsonObject().get("receiveraccount").getAsString();
			receiverqueryurl = root.getAsJsonObject().get("receivercheckurl").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(receiverqueryurl)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// generate UUID a transaction UUID
		String transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		// retrieve the countryip to be used as URL

		xmlData = YoUgandaRequestUtil.createCheckNameXML(receivermsisdn,username,password);
		CLIENT_URL = receiverqueryurl;

		postMinusThread = new PostWithIgnoreSSLYoPayment(CLIENT_URL, xmlData);

		try {
			// capture the switch respoinse.
			responseobject = postMinusThread.doPost();

			// exctract a specific  element from the object(status_code)
			

			switchresponse = YoUgandaRequestUtil.getXMLValue(responseobject, "/AutoCreate/Response/Status");


		} catch (Exception e) {
			// ================================================
			// Missing fields in response from receiver system
			// ================================================

			switchresponse = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;

		}

		if (switchresponse.equalsIgnoreCase("OK")) {
			try {
			gender = YoUgandaRequestUtil.getXMLValue(responseobject, "/AutoCreate/Response/AccountInformation/PersonalInformation/Gender");
			firstname2 = YoUgandaRequestUtil.getXMLValue(responseobject, "/AutoCreate/Response/AccountInformation/PersonalInformation/Names/FirstName");
		    lastname2 = YoUgandaRequestUtil.getXMLValue(responseobject, "/AutoCreate/Response/AccountInformation/PersonalInformation/Names/LastName");
			statusdescription = "TRUE";
             
			queryresponse = new JsonObject();
			vendorfield = new JsonObject();
			
			vendorfield.addProperty("firstname", firstname2);
			vendorfield.addProperty("lastname", lastname2);
			vendorfield.addProperty("gender", gender);


			
			queryresponse.addProperty("status_code", "400");
			queryresponse.addProperty("status_description", statusdescription);
			queryresponse.add("vendor_uniquefields", vendorfield);
			
			results2 = g.toJson(queryresponse);
			} catch (Exception e) {
				// TODO: handle exception
				switchresponse = "00032";
				statusdescription = "ERROR_DECIPHERING_RESPONSE";
				
				expected.put("status_code", switchresponse);
				expected.put("status_description", statusdescription);
				results2 = g.toJson(expected);

			}
		}else {
			expected.put("status_code", "600");
			expected.put("status_description", "FALSE");
			results2 = g.toJson(expected);

		}
	
	

		expected.put("status_code", switchresponse);
		expected.put("status_description", statusdescription);
		results2 = g.toJson(expected);


		return results2;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.mno.servlet.api.bridge.centricgateway;

import java.util.HashMap;
import com.google.gson.Gson;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This Quartz job is used to check for Session Ids in the SessionLog database
 * table for expiry. Session Ids are expired after a fixed duration of time.
 * <p>
 * Copyright (c) Tawi Commercial Services Ltd., Sep 23, 2014
 * 
 * @author <a href="mailto:michael@tawi.mobi">Michael Wakahe</a>
 * 
 */

public class CentricPoll implements Job {

	private PostWithIgnoreSSL postMinusThread;

	private String CLIENT_URL = "";
	private String responseobject = "", receiverurl = "", username = "", password = "", clientid = "",
			clientsecret = "", zeegranttype = "";

	private HashMap<String, String> expected = new HashMap<>();

	/**
	 * Empty constructor for job initialization
	 * <p>
	 * Quartz requires a public empty constructor so that the scheduler can
	 * instantiate the class whenever it needs.
	 */
	public CentricPoll() {

	}

	/**
	 * <p>
	 * Called by the <code>{@link org.quartz.Scheduler}</code> when a
	 * <code>{@link org.quartz.Trigger}</code> fires that is associated with the
	 * <code>Job</code>.
	 * </p>
	 * 
	 * @throws JobExecutionException
	 *             if there is an exception while executing the job.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Gson g = new Gson();

		receiverurl = PropertiesConfig.getConfigValue("CENTRICAUTHURL");
		username = PropertiesConfig.getConfigValue("CENTRIC_USERNAME");
		password = PropertiesConfig.getConfigValue("CENTRIC_PASSWORD");
		clientid = PropertiesConfig.getConfigValue("CENTRICCLIENTID");
		clientsecret = PropertiesConfig.getConfigValue("CENTRICCLIENTSECRET");
		zeegranttype = PropertiesConfig.getConfigValue("CENTRICGRANTYPE");
		CLIENT_URL = PropertiesConfig.getConfigValue("CENTRICINTERNAL_AUTHURL");

		expected.put("username", username);
		expected.put("password", password);
		expected.put("receiverqueryurl", receiverurl);
		expected.put("clientsecret", clientsecret);
		expected.put("clientid", clientid);
		expected.put("granttype", zeegranttype);

		String jsonforxData = g.toJson(expected);

		try {
			postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonforxData);

			// capture the switch respoinse.
			responseobject = postMinusThread.doPost();

			System.out.println(jsonforxData + " response " + responseobject + " ");
		} catch (Exception e) {
			System.out.println(jsonforxData + " CENTRICGATEWAY session Request error" + responseobject);
		}

		// Perform post request.
	}

}

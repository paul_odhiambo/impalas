/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impalapay.mno.servlet.api.bridge.ubpay;


import com.google.gson.*;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostWithIgnoreUbpay;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BankRouteRemit extends HttpServlet {

	private Cache accountsCache;
	private String CLIENT_URL1 = "", CLIENT_URL = "", CLIENT_URL2 = "",TOKEN="";
	private PostWithIgnoreUbpay postMinusThread;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		// joined json string
		Account account = null;

		String join = "";
		JsonElement root = null, roots = null;
		JsonObject root2 = null, creditrequest = null, vendorfields = null;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", sourcecountrycode = "",
				recipientcurrencycode = "", recipientcountrycode = "", sourcemsisdn = "", recipientmobile = "",
				sendername = "", amountstring = "", remiturlss = "", responseobject = "", statuscode = "",
				statusdescription = "", referencenumber = "", senderaddress = "", sendercity = "", accountnumber = "",
				bankcode = "", recipientname = "", branchcode = "", iban = "", thirdreference = "", statuscode1 = "",
				transactionNumber="",narration="";

		String meanstype = "", clientid = "", sessionkey = "";

		// vendor Unique parameters for Wari
		String recipientfirstname = "", recipientlastname = "", recipientemail = "", senderemail = "",callbackurl="";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			sourcecountrycode = root.getAsJsonObject().get("sourcecountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("recipientcurrencycode").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("recipientcountrycode").getAsString();

			sourcemsisdn = root.getAsJsonObject().get("source_msisdn").getAsString();

			recipientmobile = root.getAsJsonObject().get("beneficiary_msisdn").getAsString();

			sendername = root.getAsJsonObject().get("Sender_Name").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();

			senderaddress = root.getAsJsonObject().get("sender_address").getAsString();

			sendercity = root.getAsJsonObject().get("sender_city").getAsString();

			bankcode = root.getAsJsonObject().get("bank_code").getAsString();

			branchcode = root.getAsJsonObject().get("branch_code").getAsString();

			iban = root.getAsJsonObject().get("iban").getAsString();

			accountnumber = root.getAsJsonObject().get("account_number").getAsString();

			recipientname = root.getAsJsonObject().get("recipient_name").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(sourcecountrycode)
				|| StringUtils.isBlank(recipientcurrencycode) || StringUtils.isBlank(sendername)
				|| StringUtils.isBlank(amountstring) || StringUtils.isBlank(remiturlss)
				|| StringUtils.isBlank(senderaddress) || StringUtils.isBlank(recipientmobile)
				|| StringUtils.isBlank(sourcemsisdn) || StringUtils.isBlank(sendercity) || StringUtils.isBlank(bankcode)
				|| StringUtils.isBlank(accountnumber) || StringUtils.isBlank(recipientcountrycode)
				|| StringUtils.isBlank(recipientname)) {

			expected.put("status_code", "server error");
			expected.put("status_description", "missing parameters");
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ###############################################################################
		callbackurl = PropertiesConfig.getConfigValue("UBPAY_BANK_CALLBACK_URL");


		creditrequest = new JsonObject();

		creditrequest.addProperty("customerReference", transactioinid);
		creditrequest.addProperty("paymentType", "BANK");
		creditrequest.addProperty("fromCountry", sourcecountrycode);
		creditrequest.addProperty("toCountry", recipientcountrycode);
		creditrequest.addProperty("narration", "Payment One");
		creditrequest.addProperty("senderPhone", sourcemsisdn);
		creditrequest.addProperty("senderName", sendername);
		creditrequest.addProperty("recipientPhone", recipientmobile);
		creditrequest.addProperty("recipientName", recipientname);
		creditrequest.addProperty("language", "en");
		creditrequest.addProperty("bankCode", bankcode);
		creditrequest.addProperty("accountNumber", accountnumber);
		creditrequest.addProperty("currency", recipientcurrencycode);
		creditrequest.addProperty("amount", amountstring);
		creditrequest.addProperty("callbackUrl", callbackurl);

		String jsonData = g.toJson(creditrequest);

		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");
		// CLIENT_URL = remiturlss;
		// CLIENT_URL =
		// "https://apps.ub-pay.net/test/thirdPartyServer/b2cn";
		TOKEN = PropertiesConfig.getConfigValue("UBPAY_TOKEN");
		CLIENT_URL = remiturlss;

		postMinusThread = new PostWithIgnoreUbpay(CLIENT_URL, jsonData, TOKEN);

		// *******************************************************
		// capture the switch response.
		// *******************************************************
		// {"funds":{"currency":"XOF","amount":100.0,"fees":50.0},"transactionID":"1964503","requestID":"3456789","statusMessage":"SUCCESS","statusCode":"000"}

		try {
			responseobject = postMinusThread.doPost();
			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			statuscode = roots.getAsJsonObject().get("responseCode").getAsString();

		} catch (Exception e) {
			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			referencenumber = "investigateTransaction";
			statuscode = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
		}

		if ( statuscode.equalsIgnoreCase("1")) {
			statuscode = "S001";
		}
		if ( statuscode.equalsIgnoreCase("0")) {
			statuscode = "S000";
		}
		if (statuscode.equalsIgnoreCase("3")) {
			statuscode = "00029";

		}

		String success = "S000";

		if (statuscode.equalsIgnoreCase(success)) {


			narration = roots.getAsJsonObject().get("narration").getAsString();
			transactionNumber = roots.getAsJsonObject().get("transactionNumber").getAsString();
			referencenumber = roots.getAsJsonObject().get("customerReference").getAsString();



			expected.put("am_referenceid", transactionNumber);
			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", narration);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}
		expected.put("am_referenceid", transactionNumber);
		expected.put("am_timestamp", username);
		expected.put("status_code", statuscode);
		expected.put("status_description", narration);
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}
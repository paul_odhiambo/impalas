package com.impalapay.mno.servlet.api.bridge.tola;

import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.NameSplitUtil;
import com.impalapay.airtel.util.PhonenumberSplitUtil;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLSoftTeller;

public class RouteMobileCollection extends HttpServlet {

	// private PostMinusThread postMinusThread;
	private PostWithIgnoreSSLSoftTeller postMinusThread;
	private Cache accountsCache;
	private String CLIENT_URL = "", TOLA_SECRET = "", APPLLICATION = "", TEXT_PATH = "", CALLBACK_URI = "",
			phoneresults = "", phoneresults2 = "", TOLA_PAYBILL = "", date = "";
	private Logger logger;
	private SimpleDateFormat sdf;
	private MessageDigest md, mdSha;
	private StringBuffer sb, sbSha;
	private PhonenumberSplitUtil phonenumbersplit;
	private NameSplitUtil splitnames;
	private Calendar cal;
	private SimpleDateFormat now;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);
		sdf = new SimpleDateFormat("YYYY-MM-dd");
		logger = Logger.getLogger(this.getClass());
		cal = Calendar.getInstance();
		now = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		date = now.format(cal.getTime());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonObject root2 = null, creditrequest = null, data = null, paymentServiceInfo = null;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", sourcecountrycode = "",
				email = "", recipientcurrencycode = "", recipientcountrycode = "", sourcemsisdn = "",
				recipientmobile = "", sendername = "", amountstring = "", remiturlss = "", responseobject = "",
				statuscode = "", statusdescription = "", referencenumber = "", token = "", debitreferenceno = "",
				networkname = "", transactionuuid = "", mytimestamp = "", encryptedPassword = "",
				passwordtoEncrypt = "", jsonResult = "", pass = "", mno = "", password = "";
		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("debitcountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("debitcurrencycode").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			sendername = root.getAsJsonObject().get("debitname").getAsString();

			recipientmobile = root.getAsJsonObject().get("debitaccount").getAsString();

			debitreferenceno = root.getAsJsonObject().get("debitreferencenumber").getAsString();

			networkname = root.getAsJsonObject().get("networkname").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(recipientcurrencycode)
				|| StringUtils.isBlank(sendername) || StringUtils.isBlank(amountstring)
				|| StringUtils.isBlank(remiturlss) || StringUtils.isBlank(recipientcountrycode)) {

			expected.put("status_code", "00032");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

		}

		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("status_code", "00032");
			expected.put("status_description", "unauthorised user");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		TOLA_SECRET = PropertiesConfig.getConfigValue("TOLA_SECRET");
		TOLA_PAYBILL = PropertiesConfig.getConfigValue("TOLA_PAYBILL");

		password = recipientmobile + ":" + date + ":" + TOLA_SECRET;

		// Hashing password

		try {

			md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());
			byte byteData[] = md.digest();
			// convert the byte to hex format method 1
			sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("status_description", "MD5 encryption failure");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Result of hashing
		pass = sb.toString();

		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################

		creditrequest = new JsonObject();

		creditrequest.addProperty("amount", amountstring);
		creditrequest.addProperty("amounttype", "unit");
		creditrequest.addProperty("channel", networkname);
		creditrequest.addProperty("currency", recipientcurrencycode);
		creditrequest.addProperty("sourcereference", transactioinid);
		creditrequest.addProperty("msisdn", recipientmobile);
		creditrequest.addProperty("type", "charge");
		creditrequest.addProperty("target", TOLA_PAYBILL);
		creditrequest.addProperty("mac", pass);
		creditrequest.addProperty("date", date);
		creditrequest.addProperty("username", username);
		creditrequest.addProperty("password", apipassword);

		/**
		 * if (vendorfields != null) { creditrequest.add("vendor_uniquefields",
		 * vendorfields); }
		 **/

		// assign the remit url from properties.config
		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");

		CLIENT_URL = remiturlss;

		String jsonData = g.toJson(creditrequest);

		// postMinusThread = new PostMinusThread(CLIENT_URL, jsonData);
		postMinusThread = new PostWithIgnoreSSLSoftTeller(CLIENT_URL, jsonData);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {

			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			statuscode = roots.getAsJsonObject().get("status").getAsString();

			statusdescription = roots.getAsJsonObject().get("message").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			referencenumber = "investigateTransaction";
			statuscode = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
		}

		logger.error(".....................................................");
		logger.error("SOFTELLER DEBIT REQUEST FROM BRIDGE :" + jsonData + "\n");
		logger.error("SOFTELLER DEBIT RESPONSE:" + roots + "\n");
		logger.error(".....................................................");

		// map ACCEPTED FOR PROCESSING
		if (statuscode.equalsIgnoreCase("pending")) {
			statuscode = "S001";
		} else if (statuscode.equalsIgnoreCase("success")) {

			statuscode = "S000";

		} else {
			statuscode = "00029";

			// System.out.println(creditrequest);
		}

		String success = "S000", pending = "S001", failed = "00029";

		if (statuscode.equalsIgnoreCase(success) || statuscode.equalsIgnoreCase(pending)
				|| statuscode.equalsIgnoreCase(failed)) {

			expected.put("am_referenceid", transactionuuid);
			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);

			// System.out.println("MZEEE HIII NDIO DATA TUNAGET "+transactionuuid+"\n"+"THE
			// CALLBACKURI KAKA "+CALLBACK_URI+"\n");

			return jsonResult;
		}
		expected.put("am_referenceid", transactioinid);
		expected.put("am_timestamp", username);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	// {
	// "code": 200,
	// "status": "PENDING",
	// "message": "Transaction Initiated",
	// "data": {
	// "transaction_id": "1515729895"
	// }
	// }

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

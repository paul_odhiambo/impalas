package com.impalapay.mno.servlet.api.bridge.magma;

import java.util.HashMap;
import com.google.gson.Gson;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostThreadUniversalUpdated;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This Quartz job is used to check for Session Ids in the SessionLog database
 * table for expiry. Session Ids are expired after a fixed duration of time.
 * <p>
 * Copyright (c) Tawi Commercial Services Ltd., Sep 23, 2014
 * 
 * @author <a href="mailto:michael@tawi.mobi">Michael Wakahe</a>
 * 
 */

public class MagmaPoll implements Job {

	private PostWithIgnoreSSL postMinusThread;
	//private PostThreadUniversalUpdated postMinusThread;


	private String CLIENT_URL = "";
	private String responseobject = "", receiverurl = "", username = "", password = "";
	private Logger logger;
	private HashMap<String, String> expected = new HashMap<>();

	/**
	 * Empty constructor for job initialization
	 * <p>
	 * Quartz requires a public empty constructor so that the scheduler can
	 * instantiate the class whenever it needs.
	 */
	public MagmaPoll() {
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * <p>
	 * Called by the <code>{@link org.quartz.Scheduler}</code> when a
	 * <code>{@link org.quartz.Trigger}</code> fires that is associated with the
	 * <code>Job</code>.
	 * </p>
	 * 
	 * @throws JobExecutionException
	 *             if there is an exception while executing the job.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Gson g = new Gson();

		receiverurl = PropertiesConfig.getConfigValue("MAGMAAUTHURL");
		username = PropertiesConfig.getConfigValue("MAGMA_USERNAME");
		password = PropertiesConfig.getConfigValue("MAGMA_PASSWORD");

		CLIENT_URL = PropertiesConfig.getConfigValue("MAGMAINTERNAL_AUTHURL");

		expected.put("username", username);
		expected.put("password", password);
		expected.put("receiverqueryurl", receiverurl);

		String jsonforxData = g.toJson(expected);

		logger.error(".....................................................");
		logger.error(" START AUTOMATIC MAGMA SESSIONREQUEST" + "\n");
		logger.error(" MAGMA SESSION REQUEST THE URL "+ CLIENT_URL+ "\n");
		logger.error(".....................................................");

		try {

			postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonforxData);
			//postMinusThread = new PostThreadUniversalUpdated(CLIENT_URL, jsonforxData);


			// capture the switch respoinse.
			responseobject = postMinusThread.doPost();
			//responseobject = postMinusThread.runurl();


			System.out.println("MAGMA Session request " + jsonforxData + "\n" + " MAGMA session response "
					+ responseobject + " " + "\n");
		} catch (Exception e) {
			logger.error(".....................................................");
			logger.error(" ERROR OCCURED DURING AUTOMATIC MAGMA SESSIONREQUEST" + "\n");
			logger.error(responseobject);
			logger.error(".....................................................");
			System.out.println(jsonforxData + " MAGMA session Request error" + responseobject);
		}

		logger.error(".....................................................");
		logger.error(" RESPONSE FOR AUTOMATIC MAGMA SESSIONREQUEST" + "\n");
		logger.error(responseobject);
		logger.error(".....................................................");
	}

}

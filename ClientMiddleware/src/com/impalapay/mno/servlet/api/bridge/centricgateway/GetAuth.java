package com.impalapay.mno.servlet.api.bridge.centricgateway;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLCentricAuth;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

public class GetAuth extends HttpServlet {

	private String CLIENT_URL = "", TEXT_PATH = "";
	private PostWithIgnoreSSLCentricAuth postMinusThread;
	Properties prop = new Properties();
	OutputStream output = null;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		// CacheManager mgr = CacheManager.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(authentication(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String authentication(HttpServletRequest request) throws IOException {
		// Account account = null;

		// joined json string
		String join = "";
		String token = "";
		JsonElement root = null, roots = null;
		String responseobject = "", results2 = "", jsonResult = "";

		// These represent parameters received over the network
		String username = "", sessionid = "", receiverquery = "", clientsecret = "", clientid = "", granttype = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		JsonArray jsonarray = new JsonArray();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);
			username = root.getAsJsonObject().get("username").getAsString();
			sessionid = root.getAsJsonObject().get("password").getAsString();
			receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();
			clientsecret = root.getAsJsonObject().get("clientsecret").getAsString();
			clientid = root.getAsJsonObject().get("clientid").getAsString();
			granttype = root.getAsJsonObject().get("granttype").getAsString();
		} catch (Exception e) {

			// expected.put("command_status",
			// APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			expected.put("command_status", "COMMANDSTATUS_INVALID_PARAMETERS");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		results2 = "grant_type=" + URLEncoder.encode(granttype, "UTF-8") + "&client_id="
				+ URLEncoder.encode(clientid, "UTF-8") + "&client_secret=" + URLEncoder.encode(clientsecret, "UTF-8");

		System.out.println(results2);

		TEXT_PATH = PropertiesConfig.getConfigValue("CENTRICTEXT_PATH");
		CLIENT_URL = receiverquery;
		postMinusThread = new PostWithIgnoreSSLCentricAuth(CLIENT_URL, results2);

		try {

			// capture the switch respoinse.
			responseobject = postMinusThread.doPost();
			// System.out.println(responseobject);

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			token = roots.getAsJsonObject().get("access_token").getAsString();

			// System.out.println(token);

			try {
				FileOutputStream writer = new FileOutputStream(TEXT_PATH);
				writer.write(("").getBytes());
				writer.close();

				PrintWriter fileWriter = new PrintWriter(new FileOutputStream(TEXT_PATH, true));
				fileWriter.println(token);
				// out.println("file saved");
				fileWriter.close();
			} catch (IOException io) {
				io.printStackTrace();
			} finally {
				if (output != null) {
					try {
						output.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			}

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			// String configFile = getServletContext().getRealPath("/") +
			// getInitParameter("config-file");
			expected.put("command_status", "COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS");
			jsonResult = g.toJson(expected);

			System.out.println(roots);

			return jsonResult;

		}

		return token;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

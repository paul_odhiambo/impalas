package com.impalapay.mno.servlet.api.bridge.hub2;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.CacheManager;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostThreadUniversalUpdated;

public class ReceivePushListener extends HttpServlet {

	// private PostWithIgnoreSSL postMinusThread;
	private PostThreadUniversalUpdated postMinusThread;
	private String CLIENT_URL = "";
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonObject root2 = null, creditrequest = null, responsetoreceiver = null, stkcallback = null, sdkbody = null;

		String transactiontype = "", transactioinid = "", thirdptransactioinid = "", transactiontime = "",
				amountstring = "", shortcode = "", referencenumber = "", invoicenumber = "", accountbalance = "",
				sourcemsisdn = "", firstname = "", middlename = "", lastname = "", remiturlss = "", responseobject = "",
				statuscode = "", statusdescription = "", jsonResult = "", mpesatransactionid = "", responsecode = "",
				jsonData = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		logger.error(".....................................................");
		logger.error("HUBTEL RECEIVECASH INCOMING REQUEST :" + join);
		logger.error(".....................................................");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			responsecode = root.getAsJsonObject().get("ResponseCode").getAsString();
			sdkbody = root.getAsJsonObject().get("Data").getAsJsonObject();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (sdkbody.equals(null)) {

			expected.put("status_code", "server error");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// EXTRACT SENT DETAILS FROM SAFARICOM

		try {

			mpesatransactionid = sdkbody.getAsJsonObject().get("TransactionId").getAsString();
			statusdescription = sdkbody.getAsJsonObject().get("Description").getAsString();

		} catch (Exception e) {
			// TODO: handle exception
			expected.put("status_code", "00032");
			expected.put("status_description", "missing parameters on vendor unique field");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################

		// map SUCCESS TRANSACTION
		if (statuscode.equalsIgnoreCase("0000")) {
			statuscode = "S000";
			statusdescription = "SUCCESS";
		} else if (statuscode.equalsIgnoreCase("0005") || statuscode.equalsIgnoreCase("3008")
				|| statuscode.equalsIgnoreCase("3022") || statuscode.equalsIgnoreCase("3009")
				|| statuscode.equalsIgnoreCase("3012") || statuscode.equalsIgnoreCase("3013")
				|| statuscode.equalsIgnoreCase("3024") || statuscode.equalsIgnoreCase("4010")
				|| statuscode.equalsIgnoreCase("4101") || statuscode.equalsIgnoreCase("4103")
				|| statuscode.equalsIgnoreCase("4105") || statuscode.equalsIgnoreCase("4075")
				|| statuscode.equalsIgnoreCase("4080") || statuscode.equalsIgnoreCase("4505")
				|| statuscode.equalsIgnoreCase("2050") || statuscode.equalsIgnoreCase("2051")
				|| statuscode.equalsIgnoreCase("2001") || statuscode.equalsIgnoreCase("2100")
				|| statuscode.equalsIgnoreCase("2101") || statuscode.equalsIgnoreCase("2102")
				|| statuscode.equalsIgnoreCase("2103") || statuscode.equalsIgnoreCase("2152")
				|| statuscode.equalsIgnoreCase("2153") || statuscode.equalsIgnoreCase("2154")
				|| statuscode.equalsIgnoreCase("2200") || statuscode.equalsIgnoreCase("2201")) {
			statuscode = "00029";
			// statusdescription = "FAILED_TRANSACTION";
		} else {
			statuscode = "S001";
			statusdescription = "CREDIT_INPROGRESS";

			return "This transaction is still pending";
		}

		creditrequest = new JsonObject();

		creditrequest.addProperty("am_timestamp", "demosdkpush");
		creditrequest.addProperty("am_referenceid", mpesatransactionid);
		creditrequest.addProperty("status_code", statuscode);
		creditrequest.addProperty("status_description", statusdescription);

		// assign the remit url from properties.config
		CLIENT_URL = PropertiesConfig.getConfigValue("COLLECTION_SDKBRIDGEURL");

		jsonData = g.toJson(creditrequest);

		// postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonData);
		postMinusThread = new PostThreadUniversalUpdated(CLIENT_URL, jsonData);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {

			// *******************************************************
			// capture the switch response.
			// *******************************************************
			// responseobject = postMinusThread.doPost();
			responseobject = postMinusThread.runurl();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			statuscode = roots.getAsJsonObject().get("status_code").getAsString();

			// exctract a specific json element from the object(status_code)
			statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			referencenumber = "investigateTransaction";
			statuscode = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
		}

		logger.error(".....................................................");
		logger.error("HUBTEL LISTENER REQUEST RESPONSE FROM BRIDGE ");
		logger.error(".....................................................");
		logger.error("RESPONSE " + roots + "\n");

		// =====================================================================
		// construct object to return to sender system
		// =====================================================================
		responsetoreceiver = new JsonObject();
		responsetoreceiver.addProperty("am_referenceid", transactioinid);
		responsetoreceiver.addProperty("status_code", statuscode);
		responsetoreceiver.addProperty("status_description", statusdescription);

		return responsetoreceiver.toString();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}
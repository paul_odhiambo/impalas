package com.impalapay.mno.servlet.api.bridge.moneyexchange;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.util.net.MoneyExchangeQueryExRate;

import net.sf.ehcache.CacheManager;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPBody;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Allows for querying of balance through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryRates extends HttpServlet {

	private MoneyExchangeQueryExRate rateCheck;

	private String CLIENT_URL = "";

	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkBalance(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkBalance(HttpServletRequest request) throws IOException {
		Account account = null;
		// joined json string
		String join = "";

		JsonElement root = null, roots = null;
		JsonArray branchesarray;
		JsonObject branchesresult = null;


		// for balance response
		String switchbalance = "", switchresponse = "", statuscode = "", statusdescription = "", referencenumber = "",result="";

		// These represent parameters received over the network
		String username = "", password = "", receiverqueryurl = "", sourcemsisdn = "", reference = "", jsonResult = "",recipientcountrycode="";

		SOAPBody responseobject = null;

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		logger.error(".....................................................");
		logger.error("MONEYEXCHANGE EXCHANGERATE INQUIRY BRIDGE REQUEST :" + join);
		logger.error(".....................................................");

		// ##############################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ##############################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			password = root.getAsJsonObject().get("password").getAsString();
			receiverqueryurl = root.getAsJsonObject().get("receiverqueryurl").getAsString();

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(receiverqueryurl)) {

			expected.put("status_code", "00032");
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// generate UUID a transaction UUID
		//CLIENT_URL = receiverqueryurl;
		CLIENT_URL="https://ws.moneyexchange.es:42710/soap/IWSmoney";

		try {
			rateCheck = new MoneyExchangeQueryExRate(CLIENT_URL);
			// capture the switch respoinse.
			responseobject = rateCheck.callFXRateSOAPWebService();
			
			statuscode = MoneyExchangeApiUtil.getTagValue(responseobject, "xmlresultFX");

			
		} catch (Exception e) {
			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("status_code", "00032");
			expected.put("command_status", APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}
		
		logger.error(".....................................................");
		logger.error("MONEYEXCHANGE RATEQUERY BRIDGE REQUEST FROM BRIDGE ");
		logger.error(".....................................................");
		logger.error("REQUEST " + rateCheck + "\n");
		logger.error(".....................................................");
		logger.error("MONEYEXCHANGE RATEQUERY BRIDGE RESPONSE FROM MONEYEXCHANGE ");
		logger.error(".....................................................");
		logger.error("RESPONSE " + responseobject + "\n");

		
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		expected.put("am_referenceid", referencenumber);
		jsonResult = g.toJson(expected);

		return jsonResult;
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.mno.servlet.api.bridge.telkom;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLTELKOM;

import net.sf.ehcache.CacheManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Allows for querying of balance through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class ConfirmAccount extends HttpServlet {

	private PostWithIgnoreSSLTELKOM postThread;

	private Map<String, String> toairtel = new HashMap<>();

	private String CLIENT_URL ="",TOKEN = "", TEXT_PATH = "";
	
	private Logger logger;


	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkBalance(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkBalance(HttpServletRequest request) throws IOException {
		Account account = null;
		// joined json string
		String join = "", responseobject = "";

		JsonElement root = null, roots = null;
		
		JsonObject creditinternalrequest = null, creditrequest = null, receiverresponse =null;

		// for balance response
		String recipientdetails = "", switchresponse = "", statusdescription = "";

		// These represent parameters received over the network
		String username = "", password = "", receiverqueryurl = "", receivermsisdn = "", firstname = "", lastname = "",
				secondname = "",token = "",jsonResult = "",jsonData = "",transactioinid="";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ##############################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ##############################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			firstname = root.getAsJsonObject().get("first_name").getAsString();
			secondname = root.getAsJsonObject().get("second_name").getAsString();
			lastname = root.getAsJsonObject().get("last_name").getAsString();
			username = root.getAsJsonObject().get("username").getAsString();
			password = root.getAsJsonObject().get("password").getAsString();
			receivermsisdn = root.getAsJsonObject().get("receiveraccount").getAsString();
			receiverqueryurl = root.getAsJsonObject().get("receivercheckurl").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(receiverqueryurl)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}
		
		TEXT_PATH = PropertiesConfig.getConfigValue("TELKOMTEXT_PATH");
		
		
		try {

			File file = new File(TEXT_PATH);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			token = bufferedReader.readLine();

			System.out.println("The file Path " + TEXT_PATH + " The token " + token);
			// System.out.println(token);
			//close file reader
			fileReader.close();
			bufferedReader.close();

		} catch (Exception e) {
			// TODO: handle exception
			expected.put("status_code", "00032");
			expected.put("status_description", "Authentication Token Failure");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}
		
		TOKEN = token;
		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		
		//Create the External json ibject
		creditrequest = new JsonObject();
		creditinternalrequest = new JsonObject();
		

		creditinternalrequest.addProperty("username", username);// fetch from config
		creditinternalrequest.addProperty("password",password);// get from request
		creditinternalrequest.addProperty("msisdn", receivermsisdn);
		creditinternalrequest.addProperty("amount", 10);
		//creditinternalrequest.addProperty("brandId", "888999");
		creditinternalrequest.addProperty("brandId", "1045");
		creditinternalrequest.addProperty("info1", "");
		creditinternalrequest.addProperty("info2", "");
 		creditinternalrequest.addProperty("info3", "");
		creditinternalrequest.addProperty("externalRef", transactioinid);
		
		creditrequest.add("nameLookupRequest", creditinternalrequest);
	

		//CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_VERIFYURL");
		
		CLIENT_URL = receiverqueryurl;
		
		jsonData = g.toJson(creditrequest);
		
		postThread = new PostWithIgnoreSSLTELKOM(CLIENT_URL, jsonData, TOKEN);
		
		logger.error(".....................................................");
		logger.error("VERIFY REQUEST TO TELKOMKENYA ");
		logger.error(".....................................................");
		logger.error("VERIFY REQUEST TO TELKOMKENYA URI " + CLIENT_URL + "\n");
		logger.error("VERIFY REQUEST TO TELKOMKENYA REQUEST OBJECT " + jsonData + "\n");


		try {
			// capture the switch respoinse.
			responseobject = postThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			receiverresponse = roots.getAsJsonObject().get("nameLookupResponse").getAsJsonObject();

			switchresponse = receiverresponse.getAsJsonObject().get("resultCode").getAsString();

			statusdescription = receiverresponse.getAsJsonObject().get("destinationMSISDN").getAsString();

		} catch (Exception e) {
			// ================================================
			// Missing fields in response from receiver system
			// ================================================

			switchresponse = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;

			// System.out.println("URL "+CLIENT_URL+" request "+jsonData+ "response
			// "+responseobject);
		}
		
		logger.error(".....................................................");
		logger.error("VERIFY RESPONSE FROM TELKOMKENYA ");
		logger.error(".....................................................");
		logger.error("VERIFY RESPONSE FROM TELKOMKENYA OBJECT " + roots + "\n");
		
		if(switchresponse.equalsIgnoreCase("0")) {
			switchresponse = "S000";
			statusdescription = "TRUE";
		}else {
			switchresponse = "00029";
			statusdescription = "FALSE";
                        //switchresponse = "S000";
                        //statusdescription = "TRUE";
			
		}

		expected.put("status_code", switchresponse);
		expected.put("status_description", statusdescription);

		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

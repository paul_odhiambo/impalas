package com.impalapay.mno.servlet.api.bridge.mono;

import com.google.gson.*;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLFincra;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 *
 * @author Kelvin Muli
 */
public class BankRouteRemit extends HttpServlet {

	private PostWithIgnoreSSLFincra postwithignoresslfincra;
	private Cache accountsCache;
	private String CLIENT_URL = "" , CALLBACKURI = "",TOKEN = "",TEXT_PATH = "";
	private Logger logger;


	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());
		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		// joined json string
		Account account = null;
		boolean responsestatus = false;

		String join = "";
		JsonElement root = null, roots = null;
		JsonObject root2 = null, creditrequest = null;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", sourcecountrycode = "",
				recipientcurrencycode = "", recipientcountrycode = "", sourcemsisdn = "", recipientmobile = "",
				sendername = "", amountstring = "", remiturlss = "", responseobject = "", statuscode = "",
				statusdescription = "", referencenumber = "", senderaddress = "", sendercity = "", accountnumber = "",
				bankcode = "", recipientname = "", branchcode = "", iban = "",jsonData="";

		// Unique fields for pagatech
		String hashkey = "", principal = "", credentials = "", saltedToken = "", hash = "",token = "",jsonResult="";
		MessageDigest md = null;
		int responseCode = 0;

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			sourcecountrycode = root.getAsJsonObject().get("sourcecountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("recipientcurrencycode").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("recipientcountrycode").getAsString();

			sendername = root.getAsJsonObject().get("Sender_Name").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();

			sourcemsisdn = root.getAsJsonObject().get("source_msisdn").getAsString();

			recipientmobile = root.getAsJsonObject().get("beneficiary_msisdn").getAsString();

			senderaddress = root.getAsJsonObject().get("sender_address").getAsString();

			sendercity = root.getAsJsonObject().get("sender_city").getAsString();

			bankcode = root.getAsJsonObject().get("bank_code").getAsString();

			branchcode = root.getAsJsonObject().get("branch_code").getAsString();

			iban = root.getAsJsonObject().get("iban").getAsString();

			accountnumber = root.getAsJsonObject().get("account_number").getAsString();

			recipientname = root.getAsJsonObject().get("recipient_name").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(sourcecountrycode)
				|| StringUtils.isBlank(recipientcurrencycode) || StringUtils.isBlank(sendername)
				|| StringUtils.isBlank(amountstring) || StringUtils.isBlank(remiturlss)
				|| StringUtils.isBlank(senderaddress) || StringUtils.isBlank(recipientmobile)
				|| StringUtils.isBlank(sourcemsisdn) || StringUtils.isBlank(sendercity) || StringUtils.isBlank(bankcode)
				|| StringUtils.isBlank(accountnumber) || StringUtils.isBlank(recipientcountrycode)
				|| StringUtils.isBlank(recipientname)) {

			expected.put("status_code", "server error");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

		}

		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("status_code", "server error");
			expected.put("status_description", "unauthorised user");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}
		// ################################################################################
		// fetch credentials from property file.
		// ###############################################################################
		//CALLBACKURI = PropertiesConfig.getConfigValue("COOPCALLBACKURL");
	
		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################
		JsonObject beneficiaryObj = new JsonObject();
		beneficiaryObj.addProperty("firstName", recipientname);
		beneficiaryObj.addProperty("lastName", recipientname);
		beneficiaryObj.addProperty("accountHolderName", recipientname);
		beneficiaryObj.addProperty("country", recipientcountrycode);
		beneficiaryObj.addProperty("phone", recipientmobile);
		beneficiaryObj.addProperty("accountNumber", accountnumber);
		beneficiaryObj.addProperty("type", "individual");
		beneficiaryObj.addProperty("email", recipientname+"@gmail.com");
		beneficiaryObj.addProperty("bankCode", bankcode);

		
		creditrequest = new JsonObject();
		creditrequest.addProperty("business", apiusername);
		creditrequest.addProperty("sourceCurrency", recipientcurrencycode);
		creditrequest.addProperty("destinationCurrency", recipientcurrencycode);
		creditrequest.addProperty("amount", amountstring);
		creditrequest.addProperty("description", "i want to pay my vendor");
		creditrequest.addProperty("paymentDestination", "bank_account");
		creditrequest.addProperty("customerReference", transactioinid);
		creditrequest.add("beneficiary", beneficiaryObj);




		/**
		 * if (vendorfields != null) { creditrequest.add("vendor_uniquefields",
		 * vendorfields); }
		 **/

		// assign the remit url from properties.config
		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");
		CLIENT_URL = remiturlss;

		jsonData = g.toJson(creditrequest);

		postwithignoresslfincra = new PostWithIgnoreSSLFincra(CLIENT_URL, jsonData, apipassword);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {
			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postwithignoresslfincra.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			responsestatus = roots.getAsJsonObject().get("success").getAsBoolean();
			//statusdescription = statuscode;


		} catch (Exception e) {
			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			logger.error("FINCRA BANK DISBURSEMENT FAILURE RESPONSE :" + roots + "\n");
			referencenumber = "investigateTransaction";
			responsestatus = false;
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
		}
		
		logger.error(".....................................................");
		logger.error("FINCRA BANK DISBURSEMENT REQUEST URL :" + CLIENT_URL);
		logger.error("FINCRA BANK DISBURSEMENT REQUEST FROM BRIDGE :" + jsonData + "\n");
		logger.error("FINCRA BANK  DISBURSEMENT RESPONSE:" + roots + "\n");
		logger.error(".....................................................");

		// map ACCEPTED FOR PROCESSING
		if (responsestatus) {
			statuscode = "S000";
			statusdescription="";
		} else if (!responsestatus) {
			statuscode = "00029";
			statusdescription="";
		}else {
			statuscode = "00032";
			statusdescription="";

		}

		// =====================================================================
		// construct object to return to sender system
		// =====================================================================
		expected.put("am_referenceid", referencenumber);
		expected.put("am_timestamp", username);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}
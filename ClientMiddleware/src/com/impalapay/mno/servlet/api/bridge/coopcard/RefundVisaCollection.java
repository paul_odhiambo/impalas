package com.impalapay.mno.servlet.api.bridge.coopcard;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostThreadUniversalAirtel;
import com.impalapay.airtel.util.net.PostThreadUniversalEquity;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLUGmart;

public class RefundVisaCollection extends HttpServlet {

    // private PostMinusThread postMinusThread;
    //private PostWithIgnoreSSLUGmart postMinusThread;
    private PostThreadUniversalEquity postMinusThread;
    private Cache accountsCache;
    private String CLIENT_URL = "", TOKEN = "", ACCOUNTCODE = "", APPLLICATION = "", TEXT_PATH = "", CALLBACK_URI = "";

    private Logger logger;
    /**
     *
     * @param config
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        CacheManager mgr = CacheManager.getInstance();

        accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

        logger = Logger.getLogger(this.getClass());

    }

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     *             , IOException
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        OutputStream out = response.getOutputStream();

        response.setContentType("text/plain;charset=UTF-8");
        response.setDateHeader("Expires", new Date().getTime()); // Expiration
        // date
        response.setDateHeader("Date", new Date().getTime()); // Date and time
        // that the
        // message was
        // sent
        out.write(SendRequest(request).getBytes());
        out.flush();
        out.close();
    }

    /**
     *
     * @param request
     * @return
     * @throws IOException
     */
    private String SendRequest(HttpServletRequest request) throws IOException {
        Account account = null;

        // joined json string
        String join = "";
        JsonElement root = null, roots = null, rootsresponse = null;
        JsonObject root2 = null, creditrequest = null, vendoruniqueobject = null, returnserver = null, data = null;

        String apiusername = "", apipassword = "", username = "",receiveruuid="", transactioinid = "", remiturlss = "", responseobject = "", statuscode = "",
                statusdescription = "", referencenumber = "", token = "", debitreferenceno = "", payment_url = "",
                networkname = "", jsonResult = "",callbackurl="";

        String debitcontact = "", cardtype = "", cardcvv = "", cardexpmonth = "", cardexpyear = "";

        // Get all parameters, the keys of the parameters are specified
        List<String> lines = IOUtils.readLines(request.getReader());

        // used to format/join incoming JSon string
        join = StringUtils.join(lines.toArray(), "");

        // ###############################################################################################
        // instantiate the JSon
        // Note
        // The = sign is encoded to \u003d. Hence you need to use
        // disableHtmlEscaping().
        // ###############################################################################################

        Gson g = new GsonBuilder().disableHtmlEscaping().create();
        // Gson g = new Gson();
        Map<String, String> expected = new HashMap<>();

        try {
            // parse the JSon string
            root = new JsonParser().parse(join);

            apiusername = root.getAsJsonObject().get("username").getAsString();

            apipassword = root.getAsJsonObject().get("password").getAsString();

            username = root.getAsJsonObject().get("sendingIMT").getAsString();

            referencenumber = root.getAsJsonObject().get("referencenumber").getAsString();

            receiveruuid = root.getAsJsonObject().get("receivertransactionid").getAsString();

            remiturlss = root.getAsJsonObject().get("refundurl").getAsString();

            root2 = root.getAsJsonObject();

        } catch (Exception e) {
            expected.put("status_code", "00032");
            expected.put("status_description", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
            jsonResult = g.toJson(expected);

            return jsonResult;
        }

        // check for the presence of all required parameters
        if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
                || StringUtils.isBlank(referencenumber) || StringUtils.isBlank(receiveruuid)
                || StringUtils.isBlank(remiturlss)) {

            expected.put("status_code", "00032");
            expected.put("status_description", "missing parameters");
            jsonResult = g.toJson(expected);

            return jsonResult;
        }

        if (root2.has("vendor_uniquefields")) {

        }

        // Retrieve the account details
        Element element;
        if ((element = accountsCache.get(username)) != null) {
            account = (Account) element.getObjectValue();
        }

        // Secure against strange servers making request(future upgrade should
        // lock on IP)
        if (account == null) {
            expected.put("status_code", "00032");
            expected.put("status_description", "unauthorised user");
            jsonResult = g.toJson(expected);

            return jsonResult;
        }



        // ################################################################################
        // construct a Mega-Json Object to route-transactions to recipient
        // systems.
        // ################################################################################

        ACCOUNTCODE = PropertiesConfig.getConfigValue("ACCOUNT_CODE");
        APPLLICATION = PropertiesConfig.getConfigValue("APPLICATION");
        CALLBACK_URI = PropertiesConfig.getConfigValue("INTERNAL_LISTENERURI");

        creditrequest = new JsonObject();

        creditrequest.addProperty("imt_identifier", username);
        creditrequest.addProperty("reference_number", receiveruuid);


        /**
         * if (vendorfields != null) { creditrequest.add("vendor_uniquefields",
         * vendorfields); }
         **/

        // assign the remit url from properties.config
        // CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");

        CLIENT_URL = remiturlss;

        String jsonData = g.toJson(creditrequest);

        postMinusThread = new PostThreadUniversalEquity(CLIENT_URL, jsonData,"token");
        //postMinusThread = new PostWithIgnoreSSLUGmart(CLIENT_URL, jsonData, TOKEN);

        // =============================================================================
        // if step one does not execute it means the response is synchronous
        // the try catch is used to guard against bad response from the receiver
        // system.
        // =============================================================================
        logger.error(".....................................................");
        logger.error("COOP BANK VISA/MASTERCARD REFUND REQUEST FROM BRIDGE ");
        logger.error(".....................................................");
        logger.error("COOP BANK VISA/MASTERCARD BRIDGE REFUND REQUEST BRIDGE URI " + CLIENT_URL + "\n");
        logger.error("COOP BANK VISA/MASTERCARD BRIDGE REFUND REQUEST OBJECT " + jsonData + "\n");

        try {

            // *******************************************************
            // capture the switch response.
            // *******************************************************
            responseobject = postMinusThread.runurl();

            // pass the returned json string
            roots = new JsonParser().parse(responseobject);

            statuscode = roots.getAsJsonObject().get("status").getAsString();

            statusdescription = roots.getAsJsonObject().get("message").getAsString();

            referencenumber =
                    roots.getAsJsonObject().get("am_referenceid").getAsString();

            System.out.println("The file Path " + TEXT_PATH + " The token " + token);

        } catch (Exception e) {

            logger.error(".....................................................");
            logger.error("COOP BANK VISA/MASTERCARD FAILED REFUND REQUEST RESPONSE ");
            logger.error(".....................................................");
            logger.error("RESPONSE " + roots.toString() + "\n");

            // ================================================
            // Missing fields in response from receiver system
            // ================================================
            referencenumber = "investigateTransaction";
            statuscode = "00032";
            statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
        }

        // map ACCEPTED FOR PROCESSING
        if (statuscode.equalsIgnoreCase("pending")) {
            statuscode = "S001";
        }

        if (statuscode.equalsIgnoreCase("success")) {
            statuscode = "S000";
        }

        // map AUTHENTICATION_FAILED
        if (statuscode.equalsIgnoreCase("failed")) {
            statuscode = "00029";

            // System.out.println(creditrequest);
        }

        String success = "S000";

        if (statuscode.equalsIgnoreCase(success)) {


            expected.put("am_referenceid", referencenumber);
            expected.put("am_timestamp", username);
            expected.put("status_code", "S000");
            expected.put("status_description", "REFUND_SUCCESS");

            jsonResult = g.toJson(expected);

            return jsonResult;

        }
        expected.put("am_referenceid", transactioinid);
        expected.put("am_timestamp", username);
        expected.put("status_code", statuscode);
        expected.put("status_description", statusdescription);
        jsonResult = g.toJson(expected);

        return jsonResult;

    }

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     *             , IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

}


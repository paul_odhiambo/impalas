package com.impalapay.mno.servlet.api.bridge.vitesse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLVitesse;

public class BankRouteRemit extends HttpServlet {

	private Cache accountsCache;
	private String CLIENT_URL = "", TOKEN = "", TEXT_PATH = "";
	private PostWithIgnoreSSLVitesse postMinusThread;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		// joined json string
		Account account = null;

		String join = "";
		JsonElement root = null, roots = null;
		JsonObject creditrequest = null, root2 = null;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", sourcecountrycode = "",
				recipientcurrencycode = "", recipientcountrycode = "", sourcemsisdn = "", recipientmobile = "",
				sendername = "", amountstring = "", remiturlss = "", responseobject = "", statuscode = "",
				statusdescription = "", senderaddress = "", sendercity = "", accountnumber = "", bankcode = "",
				recipientname = "", branchcode = "", iban = "", thirdreference = "", statuscode1 = "",
				service_type = "Bank", jsonData = "", token = "";

		String meanstype = "", clientid = "", sessionkey = "", jsonResult = "";

		// vendor Unique parameters for Wari
		String recipientfirstname = "", recipientlastname = "", recipientemail = "", senderemail = "",
				publickeyprop = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			sourcecountrycode = root.getAsJsonObject().get("sourcecountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("recipientcurrencycode").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("recipientcountrycode").getAsString();

			sourcemsisdn = root.getAsJsonObject().get("source_msisdn").getAsString();

			recipientmobile = root.getAsJsonObject().get("beneficiary_msisdn").getAsString();

			sendername = root.getAsJsonObject().get("Sender_Name").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();

			senderaddress = root.getAsJsonObject().get("sender_address").getAsString();

			sendercity = root.getAsJsonObject().get("sender_city").getAsString();

			bankcode = root.getAsJsonObject().get("bank_code").getAsString();

			branchcode = root.getAsJsonObject().get("branch_code").getAsString();

			iban = root.getAsJsonObject().get("iban").getAsString();

			accountnumber = root.getAsJsonObject().get("account_number").getAsString();

			recipientname = root.getAsJsonObject().get("recipient_name").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(sourcecountrycode)
				|| StringUtils.isBlank(recipientcurrencycode) || StringUtils.isBlank(sendername)
				|| StringUtils.isBlank(amountstring) || StringUtils.isBlank(remiturlss)
				|| StringUtils.isBlank(senderaddress) || StringUtils.isBlank(recipientmobile)
				|| StringUtils.isBlank(sourcemsisdn) || StringUtils.isBlank(sendercity) || StringUtils.isBlank(bankcode)
				|| StringUtils.isBlank(accountnumber) || StringUtils.isBlank(recipientcountrycode)
				|| StringUtils.isBlank(recipientname)) {

			expected.put("status_code", "server error");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

		}

		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		TEXT_PATH = PropertiesConfig.getConfigValue("VITESSETEXT_TOKEN");

		

		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################
		TOKEN = TEXT_PATH;

		JsonObject originatorObj = new JsonObject();
		originatorObj.addProperty("Name", sendername);
		originatorObj.addProperty("Address", senderaddress);
		originatorObj.addProperty("Identifier", username);

		JsonObject accountObj = new JsonObject();
		accountObj.addProperty("BankName", branchcode);
		accountObj.addProperty("Swift", iban);
		accountObj.addProperty("AccountNumber", accountnumber);
		accountObj.addProperty("AbaCode", bankcode);

		JsonObject recipientObj = new JsonObject();
		recipientObj.addProperty("Name", recipientname);
		recipientObj.addProperty("Country", recipientcountrycode);
		recipientObj.addProperty("Currency", recipientcurrencycode);
		recipientObj.addProperty("RecipientReference", "Remittance");
		recipientObj.add("Account", accountObj);

		creditrequest = new JsonObject();
		creditrequest.addProperty("SendCurrency", recipientcurrencycode);
		creditrequest.addProperty("SendValue", amountstring);
		creditrequest.add("Originator", originatorObj);
		creditrequest.add("Recipient", recipientObj);
		/**
		 * if (vendorfields != null) { creditrequest.add("vendor_uniquefields",
		 * vendorfields); }
		 **/

		// assign the remit url from properties.config
		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");

		CLIENT_URL = remiturlss;

		jsonData = g.toJson(creditrequest);

		// postMinusThread = new PostMinusThread(CLIENT_URL, jsonData);
		postMinusThread = new PostWithIgnoreSSLVitesse(CLIENT_URL, jsonData, TOKEN);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {

			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			statuscode = roots.getAsJsonObject().get("Status").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			statuscode = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
		}

		// map ACCEPTED FOR PROCESSING
		if (statuscode.equalsIgnoreCase("Succeeded")) {
			statuscode = "S000";
		}

		if (statuscode.equalsIgnoreCase("Pending")) {
			statuscode = "S001";
		}

		if (statuscode.equalsIgnoreCase("Invalid")) {
			statuscode = "00029";
		}

		String success = "S000", inprogress = "S001", failed = "00029";

		if (statuscode.equalsIgnoreCase(success)) {
			try {
				transactioinid = roots.getAsJsonObject().get("TransactionId").getAsString();
			} catch (Exception e) {
				// TODO: handle exception
				expected.put("status_code", "error retriving transaction id");
				expected.put("status_description", "error retriving transaction id");
				jsonResult = g.toJson(expected);

				return jsonResult;
			}

			expected.put("am_referenceid", transactioinid);
			expected.put("am_timestamp", username);
			expected.put("status_code", "S000");
			expected.put("status_description", "SUCCESS");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (statuscode.equalsIgnoreCase(inprogress)) {

			try {
				transactioinid = roots.getAsJsonObject().get("TransactionId").getAsString();
			} catch (Exception e) {
				// TODO: handle exception
				expected.put("status_code", "error retriving transaction id");
				expected.put("status_description", "error retriving transaction id");
				jsonResult = g.toJson(expected);

				return jsonResult;
			}

			expected.put("am_referenceid", transactioinid);
			expected.put("am_timestamp", username);
			expected.put("status_code", "S001");
			expected.put("status_description", "CREDIT_INPROGRESS");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("am_referenceid", transactioinid);
		expected.put("am_timestamp", username);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

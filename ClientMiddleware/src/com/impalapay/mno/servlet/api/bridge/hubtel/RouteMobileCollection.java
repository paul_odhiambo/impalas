package com.impalapay.mno.servlet.api.bridge.hubtel;


import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.commons.codec.binary.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.PhonenumberSplitUtil;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLHubtel;

public class RouteMobileCollection extends HttpServlet {

	// private PostMinusThread postMinusThread;
	private PostWithIgnoreSSLHubtel postMinusThread;
	private PhonenumberSplitUtil phonenumbersplit;
	private Cache accountsCache;
	private SimpleDateFormat sdf;
	private Timestamp timestamp;
	private String CLIENT_URL = "", TOKEN = "", TEXT_PATH = "", CALLBACKURI = "",ACCOUNTNO="";
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		timestamp = new Timestamp(System.currentTimeMillis());
		logger = Logger.getLogger(this.getClass());

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonObject root2 = null, creditrequest = null,hubtelobject=null;

		double amount = 0;

		int finalconvertedamount = 0;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", recipientcurrencycode = "",
				recipientcountrycode = "", recipientmobile = "", sendername = "", amountstring = "", remiturlss = "",
				responseobject = "", statuscode = "", statusdescription = "", referencenumber = "", token = "",
				debitreferenceno = "", networkname = "", mytimestamp = "", baseencode = "", real_pass = "",
				jsonResult = "", jsonData = "", TransactionType = "",phoneresults = "",mno="";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("debitcountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("debitcurrencycode").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			sendername = root.getAsJsonObject().get("debitname").getAsString();

			recipientmobile = root.getAsJsonObject().get("debitaccount").getAsString();

			debitreferenceno = root.getAsJsonObject().get("debitreferencenumber").getAsString();

			networkname = root.getAsJsonObject().get("networkname").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(recipientcurrencycode)
				|| StringUtils.isBlank(sendername) || StringUtils.isBlank(amountstring)
				|| StringUtils.isBlank(remiturlss) || StringUtils.isBlank(recipientcountrycode)) {

			expected.put("status_code", "00032");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

		}

		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("status_code", "00032");
			expected.put("status_description", "unauthorised user");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		TOKEN = PropertiesConfig.getConfigValue("HUBTEL_AUTHKEY");
		ACCOUNTNO = PropertiesConfig.getConfigValue("HUBTEL_ACCOUNTNO");
		CALLBACKURI = PropertiesConfig.getConfigValue("HUBTELCALLBACKURL");


		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################
		phonenumbersplit = new PhonenumberSplitUtil();
		phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 5);

		if (phoneresults.equalsIgnoreCase("23320") || phoneresults.equalsIgnoreCase("23350")) {

			mno = "vodafone-gh";

		} else if (phoneresults.equalsIgnoreCase("23324") || phoneresults.equalsIgnoreCase("23354")
				|| phoneresults.equalsIgnoreCase("23355") || phoneresults.equalsIgnoreCase("23359")) {
			mno = "mtn-gh";
		} else if (phoneresults.equalsIgnoreCase("23356") || phoneresults.equalsIgnoreCase("23327")
				|| phoneresults.equalsIgnoreCase("23357")) {
			mno = "tigo-gh";

		} else {
			// 23326, 23356 for Airtel
			mno = "airtel-gh";

		}
		

		// apipassword =
		// "c36a828352e8153eedfec9c70d17e268588d429a1e5a888f7bf079d4c54929bc";
		// String serviceProviderID = "208703";

		
		// amount = Double.parseDouble(amountstring);

		// finalconvertedamount = CurrencyConvertUtil.doubleToInteger(amount);

		creditrequest = new JsonObject();

		creditrequest.addProperty("CustomerName", sendername);
		creditrequest.addProperty("CustomerEmail", sendername+"@gmail.com");
		creditrequest.addProperty("Channel", mno);
		creditrequest.addProperty("Amount", amountstring);
		creditrequest.addProperty("CustomerMsisdn", recipientmobile);
		creditrequest.addProperty("PrimaryCallbackUrl", CALLBACKURI);
		creditrequest.addProperty("ClientReference", debitreferenceno);
		creditrequest.addProperty("Description", "Union Dues");
		

		/**
		 * if (vendorfields != null) { creditrequest.add("vendor_uniquefields",
		 * vendorfields); }
		 **/

		// assign the remit url from properties.config
		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");

		//CLIENT_URL = remiturlss;
		//https://rmp.hubtel.com/merchantaccount/merchants/accountnumber/receive/mobilemoney
		CLIENT_URL = remiturlss+"/"+ACCOUNTNO+"/receive/mobilemoney";

		//System.out.println("THE ACCOUNT URL "+TOKEN+"\n");
		jsonData = g.toJson(creditrequest);

		// postMinusThread = new PostMinusThread(CLIENT_URL, jsonData);
		postMinusThread = new PostWithIgnoreSSLHubtel(CLIENT_URL, jsonData, TOKEN);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {

			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			statuscode = roots.getAsJsonObject().get("ResponseCode").getAsString();

			hubtelobject = roots.getAsJsonObject().get("Data").getAsJsonObject();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			referencenumber = "investigateTransaction";
			statuscode = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
		}

		// System.out.println("What we are receiving from Requesting");
		logger.error(".....................................................");
		logger.error("HUBTEL DEBIT REQUEST FROM BRIDGE :" + jsonData + "\n");
		logger.error("HUBTEL DEBIT RESPONSE:" + roots + "\n");
		logger.error(".....................................................");

		// map ACCEPTED FOR PROCESSING
		if (statuscode.equalsIgnoreCase("0001")) {
			referencenumber = hubtelobject.getAsJsonObject().get("TransactionId").getAsString();
			statuscode = "S001";
		} else {
			statuscode = "00029";
			statusdescription = "REJECTED_TRANSACTION";
			// System.out.println("Testinh Hapa"+roots+"\n"+"The token "+TOKEN);
		}

		String success = "S001";

		if (statuscode.equalsIgnoreCase(success)) {

			expected.put("am_referenceid", referencenumber);
			expected.put("am_timestamp", username);
			expected.put("status_code", "S001");
			expected.put("status_description", "CREDIT_IN_PROGRESS");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}
		expected.put("am_referenceid", referencenumber);
		expected.put("am_timestamp", username);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.mno.servlet.api.bridge.ugandamart;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLUGmart;
import com.impalapay.airtel.beans.accountmgmt.Account;

import net.sf.ehcache.CacheManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryStatus extends HttpServlet {

	private String CLIENT_URL = "", TOKEN = "", TEXT_PATH = "";
	private PostWithIgnoreSSLUGmart postMinusThread;
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonArray responsetoobject = null;
		JsonObject jsonobject = null, queryrequest = null, data = null;
		String responseobject = "", results2 = "", jsonResult = "", token = "";

		// These represent parameters received over the network
		String username = "", sessionid = "", referencenumber = "", receiveruuid = "", receiverquery = "",
				statuscode1 = "", statusdescription1 = "", statuscode2 = "", statusdescription2 = "", statuscode = "",
				statusdescription = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		logger.error(".....................................................");
		logger.error("QUERY UGMART REQUEST :" + join);
		logger.error("....................................................." + "\n" + "\n");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		JsonArray jsonarray = new JsonArray();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			sessionid = root.getAsJsonObject().get("password").getAsString();
			referencenumber = root.getAsJsonObject().get("transactionid").getAsString();
			receiveruuid = root.getAsJsonObject().get("receivertransactionid").getAsString();
			receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		TEXT_PATH = PropertiesConfig.getConfigValue("TEXT_PATH");

		try {

			File file = new File(TEXT_PATH);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			token = bufferedReader.readLine();
			//close file reader
			fileReader.close();
			bufferedReader.close();
		} catch (Exception e) {
			// TODO: handle exception
			expected.put("status_code", "00032");
			expected.put("status_description", "Authentication Token Failure");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		TOKEN = token;

		queryrequest = new JsonObject();

		results2 = g.toJson(queryrequest);

		CLIENT_URL = receiverquery + receiveruuid;
		// CLIENT_URL =
		// "https://wallet.ugmart.ug/transactions/ae3386d65e2745359ec5e4b5a48c1a60";
		postMinusThread = new PostWithIgnoreSSLUGmart(CLIENT_URL, results2, TOKEN);

		try {

			// capture the switch respoinse.
			responseobject = postMinusThread.doGet();

			roots = new JsonParser().parse(responseobject);

			// START THE HARDWORK.
			statuscode1 = roots.getAsJsonObject().get("status").getAsString();

			statusdescription1 = roots.getAsJsonObject().get("message").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("command_status", APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		logger.error(".....................................................");
		logger.error("Query UGMART RESPONSE " + responseobject + " extracted STATUS " + statuscode);
		logger.error("....................................................." + "\n");

		if (!statuscode1.equalsIgnoreCase("SUCCESS")) {
			statuscode = "S001";
			statusdescription = statusdescription1;

		} else {
			// Extaract the Second batch of information.

			data = roots.getAsJsonObject().get("data").getAsJsonObject();
			statuscode2 = data.getAsJsonObject().get("status").getAsString();
			statusdescription2 = data.getAsJsonObject().get("status_message").getAsString();

		}

		// map MISSING FIELDS
		if (statuscode2.equalsIgnoreCase("COMPLETE")) {
			statuscode = "S000";
			statusdescription = "SUCCESS";
		}

		// map FAILED TRANSACTION
		if (statuscode2.equalsIgnoreCase("FAILED")) {
			statuscode = "00029";
			statusdescription = statusdescription2;
		}

		// map TRANSACTION PENDING
		if (statuscode2.equalsIgnoreCase("PENDING")) {
			statuscode = "S001";
			statusdescription = statusdescription2;
		}

		String success = "S000", fail = "00029", inprogress = "S001";

		if (statuscode.equalsIgnoreCase(success)) {

			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (statuscode.equalsIgnoreCase(fail)) {
			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (statuscode.equalsIgnoreCase(inprogress)) {
			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("am_timestamp", username);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

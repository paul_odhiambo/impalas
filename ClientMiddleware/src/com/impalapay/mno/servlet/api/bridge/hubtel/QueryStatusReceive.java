package com.impalapay.mno.servlet.api.bridge.hubtel;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostThreadUniversalHubtel;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLHubtel;
import com.impalapay.airtel.beans.accountmgmt.Account;

import net.sf.ehcache.CacheManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryStatusReceive extends HttpServlet {

	private String CLIENT_URL = "", TOKEN = "", TEXT_PATH = "";
	private PostThreadUniversalHubtel postMinusThread;
	private SimpleDateFormat sdf;
	private Timestamp timestamp;
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		timestamp = new Timestamp(System.currentTimeMillis());
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
	        JsonObject root2 =null;
		String responseobject = "";

		// These represent parameters received over the network
		String apiusername = "", apipassword = "", referencenumber = "", receiveruuid = "", receiverquery = "",
				statuscode = "", statusdescription = "", mpesatransactionid = "", token = "", debitreferenceno = "",
				networkname = "", mytimestamp = "", baseencode = "", real_pass = "", requestid = "", jsonResult = "",
				results2 = "",ACCOUNTNO="";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();
			apipassword = root.getAsJsonObject().get("password").getAsString();
			referencenumber = root.getAsJsonObject().get("transactionid").getAsString();
			receiveruuid = root.getAsJsonObject().get("receivertransactionid").getAsString();
			receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();

		} catch (Exception e) {

			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		TOKEN = PropertiesConfig.getConfigValue("HUBTEL_AUTHKEY");
		ACCOUNTNO = PropertiesConfig.getConfigValue("HUBTEL_ACCOUNTNO");

		//https://smrsc.hubtel.com/api/merchants/accountnumber/transactions/status?

		CLIENT_URL = receiverquery+"/"+ACCOUNTNO+"/transactions/status?hubtelTransactionId="+receiveruuid;
		//TOKEN = token;

	
		postMinusThread = new PostThreadUniversalHubtel(CLIENT_URL, results2, TOKEN);

		try {

			// capture the switch respoinse.
			responseobject = postMinusThread.doGetSecure();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			//root2= roots.getAsJsonObject();
			// ===============================================================
			// an object that will contain parameters for provisional response
			// ===============================================================
			statuscode = roots.getAsJsonObject().get("ResponseCode").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			//if(root2.has("errorCode")) {
				//System.out.println("we are extracting the error code here");
				
				// = roots.getAsJsonObject().get("errorCode").getAsString();
				//statusdescription = roots.getAsJsonObject().get("errorMessage").getAsString();
			//}else {
				//System.out.print("The response returned from Mpesa Query online " + roots);

				statuscode = "00032";
				statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
				
			//}

		}

		logger.error(".....................................................");
		logger.error("HUBTEL QUERY REQUEST FROM BRIDGE :" + CLIENT_URL + "\n");
		logger.error("HUBTEL QUERY RESPONSE:" + roots + "\n");
		logger.error(".....................................................");

		// map SUCCESS TRANSACTION
		if (statuscode.equalsIgnoreCase("0000")) {
			statuscode = "S000";
			statusdescription = "SUCCESS";
		}else if (statuscode.equalsIgnoreCase("0005") || statuscode.equalsIgnoreCase("3008")
				|| statuscode.equalsIgnoreCase("3022") || statuscode.equalsIgnoreCase("3009")
				|| statuscode.equalsIgnoreCase("3012") || statuscode.equalsIgnoreCase("3013")
				|| statuscode.equalsIgnoreCase("3024") || statuscode.equalsIgnoreCase("4010")
				|| statuscode.equalsIgnoreCase("4101") || statuscode.equalsIgnoreCase("4103")
				|| statuscode.equalsIgnoreCase("4105") || statuscode.equalsIgnoreCase("4075")
				|| statuscode.equalsIgnoreCase("4080") || statuscode.equalsIgnoreCase("4505")
				|| statuscode.equalsIgnoreCase("2050") || statuscode.equalsIgnoreCase("2051")
				|| statuscode.equalsIgnoreCase("2001") || statuscode.equalsIgnoreCase("2100")
				|| statuscode.equalsIgnoreCase("2101") || statuscode.equalsIgnoreCase("2102")
				|| statuscode.equalsIgnoreCase("2103") || statuscode.equalsIgnoreCase("2152")
				|| statuscode.equalsIgnoreCase("2153") || statuscode.equalsIgnoreCase("2154")
				|| statuscode.equalsIgnoreCase("2200") || statuscode.equalsIgnoreCase("2201")) {
			statuscode = "00029";
			//statusdescription = "FAILED_TRANSACTION";500.001.1001 brings problems
			statusdescription = requestid;
		}else {
			statuscode = "S001";
			statusdescription = "CREDIT_INPROGRESS";
		}

		String success = "S000";
		String fail = "00029";
		if (statuscode.equalsIgnoreCase(success)) {

			expected.put("am_timestamp", apiusername);
			expected.put("am_referenceid", mpesatransactionid);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (statuscode.equalsIgnoreCase(fail)) {
			expected.put("am_timestamp", apiusername);
			expected.put("am_referenceid", mpesatransactionid);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("am_timestamp", apiusername);
		expected.put("am_referenceid", mpesatransactionid);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

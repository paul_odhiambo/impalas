package com.impalapay.mno.servlet.api.bridge.venturegarden;

import org.apache.commons.lang3.exception.ExceptionUtils;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;


import org.quartz.JobDetail;
import org.quartz.Trigger;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;
import static org.quartz.SimpleScheduleBuilder.*;

import org.apache.log4j.Logger;

/**
 * Triggers the job that expires Session Ids.
 * <p>
 * Copyright (c) Tawi Commercial Services Ltd., Sep 23, 2014
 * 
 * @author <a href="mailto:michael@tawi.mobi">Michael Wakahe</a>
 * 
 */
public class VentureGardenPollLauncher extends Thread {

	private Logger logger;

	/**
	 * 
	 */
	public VentureGardenPollLauncher() {
		logger = Logger.getLogger(this.getClass());
	}

	/**
	 * 
	 */
	@Override
	public void run() {
		try {
			// Grab the Scheduler instance from the Factory
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

			// define the job and tie it to our Job class
			JobDetail job = newJob(VentureGardenPoll.class).withIdentity("ventureauthJob", "ventureauth").build();

			// Trigger the job to run now, and then every 60 seconds
			Trigger trigger = newTrigger().withIdentity("ventureauthTrigger", "ventureauth").startNow()
					.withSchedule(simpleSchedule().withIntervalInSeconds(3100).repeatForever()).build();//5minutes

			// Tell quartz to schedule the job using our trigger
			scheduler.scheduleJob(job, trigger);

			// and start it off
			scheduler.start();

		} catch (SchedulerException e) {
			logger.error("SQLException while triggering the Venture session Request Session.");
			logger.error(ExceptionUtils.getStackTrace(e));
		}
	}

}

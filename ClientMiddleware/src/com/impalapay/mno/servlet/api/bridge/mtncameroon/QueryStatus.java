package com.impalapay.mno.servlet.api.bridge.mtncameroon;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLMtnCameroon;
import com.impalapay.airtel.beans.accountmgmt.Account;

import net.sf.ehcache.CacheManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryStatus extends HttpServlet {

	private String CLIENT_URL = "", TOKEN = "", TEXT_PATH = "";
	private PostWithIgnoreSSLMtnCameroon postMinusThread;
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonArray responsetoobject = null;
		JsonObject jsonobject = null, queryrequest = null, data = null, payoptionorder = null;
		String responseobject = "", results2 = "", jsonResult = "", token = "",jsonData="";
		
		String environment = "", subscriptionKey = "";

		// These represent parameters received over the network
		String username = "", sessionid = "", referencenumber = "", receiveruuid = "", receiverquery = "",
				statuscode1 = "", statusdescription1 = "", statuscode2 = "", statusdescription2 = "", statuscode = "",
				statusdescription = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		logger.error(".....................................................");
		logger.error("QUERY MTN CAMEROON DISBURSEMENT REQUEST :" + join);
		logger.error("....................................................." + "\n" + "\n");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		JsonArray jsonarray = new JsonArray();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			sessionid = root.getAsJsonObject().get("password").getAsString();
			referencenumber = root.getAsJsonObject().get("transactionid").getAsString();
			receiveruuid = root.getAsJsonObject().get("receivertransactionid").getAsString();
			receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}
		
		TEXT_PATH = PropertiesConfig.getConfigValue("MTNCAMEROON_PATH");
		environment = PropertiesConfig.getConfigValue("MTN_ENVIRONMENT");
		subscriptionKey = PropertiesConfig.getConfigValue("MTN_DISBURSMENT_SUB_KEY");

		try {

			File file = new File(TEXT_PATH);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			token = bufferedReader.readLine();

			System.out.println("The file Path " + TEXT_PATH + " The token " + token);
			// System.out.println(token);
			//close file reader
			fileReader.close();
			bufferedReader.close();

		} catch (Exception e) {
			// TODO: handle exception
			expected.put("status_code", "00032");
			expected.put("status_description", "Authentication Token Failure");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		TOKEN = token;

		CLIENT_URL = receiverquery+"/"+referencenumber;
		
		queryrequest = new JsonObject();
		queryrequest.addProperty("referenceUUID", referencenumber);
		jsonData = g.toJson(queryrequest);


	
		postMinusThread = new PostWithIgnoreSSLMtnCameroon(CLIENT_URL, jsonData, TOKEN, "", referencenumber,
				environment, subscriptionKey);
		try {

			// capture the switch respoinse.
			responseobject = postMinusThread.doGet();

			roots = new JsonParser().parse(responseobject);


			// START THE HARDWORK.
			statuscode =  roots.getAsJsonObject().get("status").getAsString();
			
            statusdescription= roots.getAsJsonObject().get("reason").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("command_status", APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}
		
		
		logger.error(".....................................................");
		logger.error("MTN CAMEROON DISBURSEMENT QUERY REQUEST FROM BRIDGE :" + jsonData + "\n");
		logger.error("MTN CAMEROON DISBURSEMENT QUERY  RESPONSE " + responseobject + " extracted STATUS " + statuscode);
		logger.error("....................................................." + "\n");

		if (statuscode.equalsIgnoreCase("success") ) {
			statuscode = "S000";
		}
                if (statuscode.equalsIgnoreCase("failed")  ) {
			statuscode = "00029";
		}
                if (statuscode.equalsIgnoreCase("pending") ) {
			statuscode = "S001";
		}
                if (statuscode.equalsIgnoreCase("timeout") ) {
			statuscode = "S001";
		}
                 if (statuscode.equalsIgnoreCase("rejected") ) {
			statuscode = "00029";
		}

		String success = "S000", fail = "00029", inprogress = "S001";

		if (statuscode.equalsIgnoreCase(success) || statuscode.equalsIgnoreCase(fail)
				|| statuscode.equalsIgnoreCase(inprogress)) {

			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("am_timestamp", username);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

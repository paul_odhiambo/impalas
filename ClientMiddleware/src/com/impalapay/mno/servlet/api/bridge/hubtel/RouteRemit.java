package com.impalapay.mno.servlet.api.bridge.hubtel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.PhonenumberSplitUtil;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLHubtel;

public class RouteRemit extends HttpServlet {

	private PostWithIgnoreSSLHubtel postMinusThread;
	private Cache accountsCache;
	private String CLIENT_URL = "", TOKEN = "", TEXT_PATH = "", CALLBACKURI = "", CERT_PATH = "",ACCOUNTNO="";
	private Logger logger;
	private PhonenumberSplitUtil phonenumbersplit;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null, encodedpass = null;
		JsonObject root2 = null, creditrequest = null, responsetoreceiver = null, responserequest = null,
				hubtelobject = null;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", sourcecountrycode = "",
				recipientcurrencycode = "", recipientcountrycode = "", sourcemsisdn = "", recipientmobile = "",
				sendername = "", amountstring = "", remiturlss = "", responseobject = "", statuscode = "",
				statusdescription = "", referencenumber = "", token = "", jsonResult = "", encodedpasswordjson = "",
				encodestatuscode = "", encodestatusdescription = "", encryptedpassword = "", failed = "00029",
				phoneresults = "", mno = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			sourcecountrycode = root.getAsJsonObject().get("sourcecountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("recipientcurrencycode").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("recipientcountrycode").getAsString();

			sendername = root.getAsJsonObject().get("Sender_Name").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();

			sourcemsisdn = root.getAsJsonObject().get("source_msisdn").getAsString();

			recipientmobile = root.getAsJsonObject().get("beneficiary_msisdn").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(sourcecountrycode)
				|| StringUtils.isBlank(recipientcurrencycode) || StringUtils.isBlank(sendername)
				|| StringUtils.isBlank(amountstring) || StringUtils.isBlank(remiturlss)
				|| StringUtils.isBlank(recipientcountrycode)) {

			expected.put("status_code", "server error");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

		}

		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("status_code", "server error");
			expected.put("status_description", "unauthorised user");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		TOKEN = PropertiesConfig.getConfigValue("HUBTEL_AUTHKEY");
		ACCOUNTNO = PropertiesConfig.getConfigValue("HUBTEL_ACCOUNTNO");
        CALLBACKURI = PropertiesConfig.getConfigValue("HUBTELCALLBACKURL");

		// CERT_PATH = PropertiesConfig.getConfigValue("MPESACERTIFICATEURL");

		
		// MTN: 024, 054, 059, 055
		// Vodafone: 020, 050
		// AirtelTigo: 026, 056, 027, 057,
		// Glo: 023

		// Assign mno field Value.
		phonenumbersplit = new PhonenumberSplitUtil();
		phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 5);

		if (phoneresults.equalsIgnoreCase("23320") || phoneresults.equalsIgnoreCase("23350")) {

			mno = "vodafone-gh";

		} else if (phoneresults.equalsIgnoreCase("23324") || phoneresults.equalsIgnoreCase("23354")
				|| phoneresults.equalsIgnoreCase("23355") || phoneresults.equalsIgnoreCase("23359")) {
			mno = "mtn-gh";
		} else if (phoneresults.equalsIgnoreCase("23356") || phoneresults.equalsIgnoreCase("23327")
				|| phoneresults.equalsIgnoreCase("23357")) {
			mno = "tigo-gh";

		} else {
			// 23326, 23356 for Airtel
			mno = "airtel-gh";


		}
		//TOKEN = token;

		// ###############################################################
		// Fetch the initiator Password
		// ##############################################################
		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################

		creditrequest = new JsonObject();

		creditrequest.addProperty("RecipientName", sendername);// USE PHP SCRIPT TO ENCRYPT SAFARICOM PORTAL PASSWORD
		creditrequest.addProperty("CustomerEmail", sendername + "@gmail.com");
		creditrequest.addProperty("Amount", amountstring);
		creditrequest.addProperty("Channel", mno);//
		creditrequest.addProperty("RecipientMsisdn", recipientmobile);
		creditrequest.addProperty("Description", "Disbursement");
		creditrequest.addProperty("PrimaryCallbackUrl", CALLBACKURI);
		creditrequest.addProperty("ClientReference", transactioinid);

		/**
		 * if (vendorfields != null) { creditrequest.add("vendor_uniquefields",
		 * vendorfields); }
		 **/

		// assign the remit url from properties.config
		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");
		//CLIENT_URL = remiturlss;
		
		//https://smp.hubtel.com/api/merchants/2016475/send/mobilemoney
		CLIENT_URL = remiturlss+"/"+ACCOUNTNO+"/send/mobilemoney";

		String jsonData = g.toJson(creditrequest);

		// postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonData);
		postMinusThread = new PostWithIgnoreSSLHubtel(CLIENT_URL, jsonData, TOKEN);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {
			// check for successfull

			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			statuscode = roots.getAsJsonObject().get("ResponseCode").getAsString();

			hubtelobject = roots.getAsJsonObject().get("Data").getAsJsonObject();

		} catch (Exception e) {

			// This transaction might have failed check error log
			responserequest = roots.getAsJsonObject();

			if (responserequest.has("Errors")) {
				statuscode = "00029";
				statusdescription = roots.getAsJsonObject().get("message").getAsString();
				// referencenumber =
				// responserequest.getAsJsonObject().get("requestId").getAsString();
			} else {
				referencenumber = "investigateTransaction";
				statuscode = "00032";
				statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
			}
			// ================================================
			// Missing fields in response from receiver system
			// ================================================

		}

		logger.error(".....................................................");
		logger.error("HUBTEL DISBURSEMENT REQUEST FROM BRIDGE :" + jsonData + "\n");
		logger.error("HUBTEL DISBURSEMENT RESPONSE:" + roots + "\n");
		logger.error(".....................................................");

		if (statuscode.equalsIgnoreCase("0001")) {
			referencenumber = hubtelobject.getAsJsonObject().get("TransactionId").getAsString();
			statuscode = "S001";
			statusdescription = "CREDIT_IN_PROGRESS";

		}

		// =====================================================================
		// construct object to return to sender system
		// =====================================================================
		responsetoreceiver = new JsonObject();
		responsetoreceiver.addProperty("am_referenceid", referencenumber);
		responsetoreceiver.addProperty("status_code", statuscode);
		responsetoreceiver.addProperty("status_description", statusdescription);

		return responsetoreceiver.toString();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

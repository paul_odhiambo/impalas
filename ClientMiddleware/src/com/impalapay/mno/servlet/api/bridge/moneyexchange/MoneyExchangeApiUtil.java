package com.impalapay.mno.servlet.api.bridge.moneyexchange;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.soap.SOAPBody;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
public class MoneyExchangeApiUtil {
	
	public static String getTagValue(SOAPBody body, String tagName){
		NodeList nodes = body.getElementsByTagName(tagName);
		
		Node node = nodes.item(0);
		
		return node !=null ? node.getTextContent() : "";
		
	}
	
	public static String getXMLValue(String results, String xpExpr) throws XPathExpressionException{
		String value = null;
		try{
		InputStream is = new ByteArrayInputStream(results.getBytes());
		
		XPath xp = XPathFactory.newInstance().newXPath();
		InputSource isource = new InputSource(is);
		value = xp.evaluate(xpExpr, isource);

		}catch (Exception e){
			//catch it
		}
		
		return value;

	}
	public static JsonArray getTagValues(SOAPBody body, String tagName){
		JsonArray array = new JsonArray();
		NodeList nodes = body.getElementsByTagName(tagName);
		
		
		for(int i = 0; i<nodes.getLength(); i++){
			Node node = (Node) nodes.item(i);
			System.out.println("Node: " + i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				JsonObject root = new JsonObject();
				NodeList childnodes = node.getChildNodes();
				
				for(int j = 0; j<childnodes.getLength(); j++){
					Node childnode = (Node) childnodes.item(j);
					
					//do node.getTextContext() because getNodeValue() returns null
					root.addProperty(childnode.getNodeName(), childnode !=null ? childnode.getTextContent() : "");	
					
				}
				array.add(root);
				
			}
		}
		return array;
		
	}
	

}

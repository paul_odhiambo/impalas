package com.impalapay.mno.servlet.api.bridge.kendy;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLKendy;

import net.sf.ehcache.CacheManager;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class SessionRequest extends HttpServlet {

	private String CLIENT_URL = "";
	private PostWithIgnoreSSLKendy postIgnoreSsl;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		String responseobject = "";

		// These represent parameters received over the network
		String username = "", password = "", accounttype = "", accountID = "", salt = "", sessionkey = "", message = "",
				requestDate = "", requestTime = "", shaUser = "", shaPass = "", usernames = "", passwords = "",
				newusername = "", newpassword = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			password = root.getAsJsonObject().get("password").getAsString();
			accounttype = root.getAsJsonObject().get("accountType").getAsString();
			accountID = root.getAsJsonObject().get("accountID").getAsString();
			salt = root.getAsJsonObject().get("salt").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Date
		requestDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
		// Time
		requestTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());

		shaUser = requestDate + username;

		shaPass = requestTime + password;

		// Hashing
		usernames = DigestUtils.sha256Hex(shaUser);
		passwords = DigestUtils.sha256Hex(shaPass);

		newusername = usernames + salt;
		newpassword = passwords + salt;

		JsonObject sessionrequest = new JsonObject();

		sessionrequest.addProperty("user", newusername);
		sessionrequest.addProperty("pass", newpassword);
		sessionrequest.addProperty("accountType", accounttype);
		sessionrequest.addProperty("accountID", accountID);
		sessionrequest.addProperty("requestDate", requestDate);
		sessionrequest.addProperty("requestTime", requestTime);

		// sessionrequest.addProperty("Payment", accountID);

		String results2 = g.toJson(sessionrequest);

		// String transactionidcheck = accountID;

		// CLIENT_URL = receiverquery;
		CLIENT_URL = "http://46.101.131.249:7140/MTSAPI/rest/test/v1/getSessionKey";

		postIgnoreSsl = new PostWithIgnoreSSLKendy(CLIENT_URL, results2);

		// capture the switch respoinse.
		responseobject = postIgnoreSsl.doPost();

		try {
			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			sessionkey = roots.getAsJsonObject().get("sessionKey").getAsString();
			message = roots.getAsJsonObject().get("message").getAsString();

		} catch (Exception e) {

			expected.put("command_status",
					APIConstants.COMMANDSTATUS_INTERNAL_ERROR + responseobject + "request " + sessionrequest);

			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("status_code", sessionkey);
		expected.put("status_description", message);
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impalapay.mno.servlet.api.bridge.ugandamart;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.collection.beans.incoming.TempCollection;
import com.impalapay.collection.persistence.tempincoming.TempIncomingDAOImpl;

public class ListenerVisaCollection extends HttpServlet {
	// private PostMinusThread postMinusThread;
	private Cache transactionStatusCache;
	private TransactionStatus ts, statusoftransaction;
	private TempCollection transactionByStatus = null;
	private TempIncomingDAOImpl transactionDAO;
	private HashMap<String, String> transactionStatusHash = new HashMap<>();
	private String TRANSACTIONSTATUS_UUID = "5c9b8b0b-a035-4a07-bfd8-eccd4f039d53";
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		transactionDAO = TempIncomingDAOImpl.getInstance();
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {

		// joined json string
		String join = "";
		JsonElement root = null;
		JsonObject root2 = null, data = null;

		String statuscode = "", statusdescription = "", message = "", transactioinid = "", transactionuuid = "",
				jsonResult = "", statusuuid = "", success = "S000", failed = "00029";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		logger.error("UgandaMart Visa/MasterCard Lister Request :" + join);

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {

			statusdescription = request.getParameter("status");

			message = request.getParameter("message");

			transactioinid = request.getParameter("transaction_id");

			root2 = root.getAsJsonObject();

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(statusdescription) || StringUtils.isBlank(message)
				|| StringUtils.isBlank(transactioinid)) {

			expected.put("status_code", "00032");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check from the database if this transaction exist and status is still
		// inprogress
		ts = new TransactionStatus();
		ts.setUuid(TRANSACTIONSTATUS_UUID);

		transactionByStatus = transactionDAO.getTransactionstatus(transactioinid, ts);

		if (transactionByStatus == null) {

			expected.put("status_code", "00032");
			expected.put("status_description", "receiver transaction id doesnt exist in system table");
			jsonResult = g.toJson(expected);

			logger.error("9999999999999999999999999999999999999847858........+jsonResult");

			return jsonResult;

		}

		Element element;

		List keys;

		// ======================================================
		// Populate with the mapping of Transaction Statuses.
		// The key is a UUID of the status
		// ======================================================

		TransactionStatus status1;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status1.getStatus(), status1.getUuid());
		}

		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################
		// map ACCEPTED FOR PROCESSING
		if (statusdescription.equalsIgnoreCase("SUCCESS")) {
			statuscode = "S000";
		} else if (statusdescription.equalsIgnoreCase("FAILED") || statusdescription.equalsIgnoreCase("CANCELLED")) {
			statuscode = "00029";
		} else {

			statuscode = "S001";

		}

		if (statuscode.equalsIgnoreCase(success) || statuscode.equalsIgnoreCase(failed)) {

			System.out.println("This tra saction was " + statusdescription);

			statusuuid = transactionStatusHash.get(statuscode);

			statusoftransaction = new TransactionStatus();
			statusoftransaction.setUuid(statusuuid);
			statusoftransaction.setDescription(message);

			transactionuuid = transactionByStatus.getUuid();

			transactionDAO.updateTempIncomingTransactionStatus(transactionuuid, statusoftransaction);

			expected.put("am_referenceid", transactionuuid);
			expected.put("am_timestamp", message);
			expected.put("status_code", statuscode);
			expected.put("status_description", message);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		expected.put("am_referenceid", transactioinid);
		expected.put("am_timestamp", message);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

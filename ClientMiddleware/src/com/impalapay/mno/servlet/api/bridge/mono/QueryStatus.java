package com.impalapay.mno.servlet.api.bridge.mono;

import com.google.gson.*;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLMono;

import net.sf.ehcache.CacheManager;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryStatus extends HttpServlet {

	private String CLIENT_URL = "", TOKEN = "", TEXT_PATH = "";
	private PostWithIgnoreSSLMono postMinusThread;
	private Map<String, String> tocoop = new HashMap<>();
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonArray responsetoobject = null;
		JsonObject jsonobject = null, queryrequest = null, data = null,responsdataeObject=null;
		String responseobject = "", results2 = "", jsonResult = "", token = "",jsonData="";

		// These represent parameters received over the network
		String username = "", sessionid = "", referencenumber = "", receiveruuid = "", receiverquery = "",
				statuscode1 = "", statusdescription1 = "", statuscode2 = "", statusdescription2 = "", statuscode = "",
				statusdescription = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		logger.error(".....................................................");
		logger.error("QUERY MONO REQUEST :" + join);
		logger.error("....................................................." + "\n" + "\n");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		JsonArray jsonarray = new JsonArray();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			sessionid = root.getAsJsonObject().get("password").getAsString();
			referencenumber = root.getAsJsonObject().get("transactionid").getAsString();
			receiveruuid = root.getAsJsonObject().get("receivertransactionid").getAsString();
			receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		TOKEN = PropertiesConfig.getConfigValue("MONO_AUTHKEY");



		queryrequest = new JsonObject();
		queryrequest.addProperty("reference", referencenumber); //amount is measured in kobo
		jsonData = g.toJson(queryrequest);


		CLIENT_URL = receiverquery;

		postMinusThread = new PostWithIgnoreSSLMono(CLIENT_URL, jsonData, TOKEN);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {
			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);
			responsdataeObject = roots.getAsJsonObject().get("data").getAsJsonObject();
			statuscode = responsdataeObject.getAsJsonObject().get("status").getAsString();
			statusdescription = responsdataeObject.getAsJsonObject().get("message").getAsString();
			
			//{
				  //"type": "onetime-debit",
				  //"data": {
				    //"status": "pending",
				    //"message": "This payment is pending, awaiting action from the user"
				  //}
				//}
			
			
			//SUCCESSFUL	This status means that the payment has been successful, and you can go ahead to give value to your users or customers.
			//PENDING	The payment is pending as we would be awaiting action from the user to confirm with their pin/token or OTP after a successful login.
			//FAILED	This status means that the payment has failed due to a bank failure, switch downtime or mono downtime.
			//CANCELLED	This status is returned when a customer closes the widget without completing the transaction.
			//PROCESSING	This status implies that you should wait, as the payment been made is currently in the process of being verified.
			//ACTIVE	This status implies that an authorised recurring payment is active.
			//ABANDONED	This implies that the payment session has been abandoned by your user.
			


		} catch (Exception e) {
			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			referencenumber = "investigateTransaction";
			statuscode = "unknown";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
		}
		
		logger.error(".....................................................");
		logger.error("MONO QUERY REQUEST FROM BRIDGE :" + jsonData + "\n");
		logger.error("MONO QUERY RESPONSE:" + roots + "\n");
		logger.error(".....................................................");

		// map ACCEPTED FOR PROCESSING
		if (statuscode.equalsIgnoreCase("SUCCESSFUL") ) {
			statuscode = "S000";
		} else if (statuscode.equalsIgnoreCase("PENDING") ||
				statuscode.equalsIgnoreCase("PROCESSING")) {
			statuscode = "S001";
		}
		else if (statuscode.equalsIgnoreCase("FAILED")||(statuscode.equalsIgnoreCase("CANCELLED"))||(statuscode.equalsIgnoreCase("ABANDONED"))) {
			statuscode = "00029";
		}
		else {

			statuscode = "00032";

		}

		// =====================================================================
		// construct object to return to sender system
		// =====================================================================
		expected.put("am_referenceid", referencenumber);
		expected.put("am_timestamp", username);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		 jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}
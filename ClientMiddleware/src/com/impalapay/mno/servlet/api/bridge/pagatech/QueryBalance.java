package com.impalapay.mno.servlet.api.bridge.pagatech;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLPagatech;

import net.sf.ehcache.CacheManager;

import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of balance through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryBalance extends HttpServlet {

	private PostWithIgnoreSSLPagatech postwithignoresslpaga;

	private Map<String, String> toairtel = new HashMap<>();

	private String CLIENT_URL = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkBalance(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkBalance(HttpServletRequest request) throws IOException {
		Account account = null;
		// joined json string
		String join = "", responseobject = "";

		JsonElement root = null, roots = null;

		// for balance response
		String switchbalance = "", statusdescription = "", statuscode = "";

		// These represent parameters received over the network
		String username = "", password = "", receiverqueryurl = "", sourcemsisdn = "";

		// Unique fields for pagatech
		String hashkey = "", principal = "", credentials = "", saltedToken = "", hash = "";
		MessageDigest md = null;
		int responseCode = 0;

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ##############################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ##############################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			password = root.getAsJsonObject().get("password").getAsString();
			receiverqueryurl = root.getAsJsonObject().get("receiverqueryurl").getAsString();
			sourcemsisdn = root.getAsJsonObject().get("sourceaccount").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(receiverqueryurl)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// generate UUID a transaction UUID
		String transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		// ################################################################################
		// fetch credentials from property file.
		// ###############################################################################
		hashkey = PropertiesConfig.getConfigValue("HASH_KEY");
		principal = PropertiesConfig.getConfigValue("PRINCIPAL");
		credentials = PropertiesConfig.getConfigValue("CREDENTIAL");
		// ################################################################################
		//
		// ################################################################################
		saltedToken = transactioinid + hashkey;

		try {
			md = MessageDigest.getInstance("SHA-512");
			md.update(saltedToken.getBytes());

		} catch (Exception e) {
			expected.put("command_status", "HASHING_ERROR_CONTACT_ADMIN");
			String jsonResult = g.toJson(expected);
			return jsonResult;
		}
		byte byteData[] = md.digest();
		// convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}
		// System.out.println("Hex format : " + sb.toString());
		hash = sb.toString();

		// retrieve the countryip to be used as URL

		toairtel.put("username", username);
		String jsonData = g.toJson(toairtel);

		CLIENT_URL = receiverqueryurl;

		try {
			postwithignoresslpaga = new PostWithIgnoreSSLPagatech(CLIENT_URL, jsonData, principal, credentials, hash);
			// capture the switch respoinse.
			responseobject = postwithignoresslpaga.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			switchbalance = roots.getAsJsonObject().get("availableBalance").getAsString();
			responseCode = roots.getAsJsonObject().get("responseCode").getAsInt();
			statuscode = Integer.toString(responseCode);
			statusdescription = roots.getAsJsonObject().get("message").getAsString();
		} catch (Exception e) {
			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("command_status", APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// map ACCEPTED FOR PROCESSING
		if (statuscode.equalsIgnoreCase("0")) {
			statuscode = "S000";
			// statusdescription = "SUCCESS";
		} else {

			statuscode = "00029";
		}

		// =====================================================================
		// construct object to return to sender system
		// =====================================================================

		expected.put("balance", switchbalance);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

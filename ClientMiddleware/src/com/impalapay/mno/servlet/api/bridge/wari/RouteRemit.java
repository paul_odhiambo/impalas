package com.impalapay.mno.servlet.api.bridge.wari;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLWari;;

public class RouteRemit extends HttpServlet {

	// private PostWithIgnoreSSLBeyonic postIgnoreSslBeyonic;
	private Cache accountsCache;
	private String CLIENT_URL1 = "", CLIENT_URL = "", CLIENT_URL2 = "";
	private PostWithIgnoreSSL postMinusThread;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null, roots3 = null;
		JsonObject root2 = null, creditrequest = null, funds = null, sender = null, beneficiary = null,
				paymentMeans = null, receiptMeans = null, confirmtransaction = null, vendorfields = null,
				requestData = null;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", sourcecountrycode = "",
				recipientcurrencycode = "", recipientcountrycode = "", sourcemsisdn = "", recipientmobile = "",
				sendername = "", amountstring = "", remiturlss = "", responseobject1 = "", responseobject = "",
				responseobject2 = "", statuscode = "", statuscode1 = "", statusdescription = "", referencenumber = "",
				thirdreference = "";
		String meanstype = "", walletpovider = "", clientid = "", merchantid = "", sessionkey = "", message = "",
				localsessionurl = "", remotesessionurl = "", confirmationcode = "", localconfirmurl = "",
				remoteconfirmurl = "";

		// vendor Unique parameters for Wari
		String recipientfirstname = "", recipientlastname = "", recipientemail = "", senderemail = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			sourcecountrycode = root.getAsJsonObject().get("sourcecountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("recipientcurrencycode").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("recipientcountrycode").getAsString();

			sendername = root.getAsJsonObject().get("Sender_Name").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();

			sourcemsisdn = root.getAsJsonObject().get("source_msisdn").getAsString();

			recipientmobile = root.getAsJsonObject().get("beneficiary_msisdn").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(sourcecountrycode)
				|| StringUtils.isBlank(recipientcurrencycode) || StringUtils.isBlank(sendername)
				|| StringUtils.isBlank(amountstring) || StringUtils.isBlank(remiturlss)
				|| StringUtils.isBlank(recipientcountrycode)) {

			expected.put("am_referenceid", "");
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALIDEMPTY_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("am_referenceid", "");
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		creditrequest = new JsonObject();
		creditrequest.addProperty("api_username", apiusername);
		creditrequest.addProperty("api_password", apipassword);
		creditrequest.addProperty("sendingIMT", username);
		creditrequest.addProperty("transaction_id", transactioinid);
		creditrequest.addProperty("sourcecountrycode", sourcecountrycode);
		creditrequest.addProperty("recipientcurrencycode", recipientcurrencycode);
		creditrequest.addProperty("recipientcountrycode", recipientcountrycode);
		creditrequest.addProperty("source_msisdn", sourcemsisdn);
		creditrequest.addProperty("beneficiary_msisdn", recipientmobile);
		creditrequest.addProperty("Sender_Name", sendername);
		creditrequest.addProperty("amount", amountstring);
		creditrequest.addProperty("url", remiturlss);
		creditrequest.addProperty("recipient_name", "From ImpalaPay");

		String jsonData = g.toJson(creditrequest);

		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");
		// CLIENT_URL = remiturlss;
		// CLIENT_URL =
		// "http://46.101.131.249:7140/MTSAPI/rest/test/v1/startTransaction";
		CLIENT_URL = remiturlss;

		postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonData);

		// *******************************************************
		// capture the switch response.
		// *******************************************************
		// {"funds":{"currency":"XOF","amount":100.0,"fees":50.0},"transactionID":"1964503","requestID":"3456789","statusMessage":"SUCCESS","statusCode":"000"}

		try {
			responseobject = postMinusThread.doPost();
			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			thirdreference = roots.getAsJsonObject().get("am_referenceid").getAsString();
			statuscode = roots.getAsJsonObject().get("status_code").getAsString();
			statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

		} catch (Exception e) {
			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			referencenumber = "investigateTransaction";
			statuscode = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
		}

		if (statuscode.equalsIgnoreCase("S000")) {
			statuscode = "S000";
		} else {
			statuscode = "00029";
		}

		expected.put("am_referenceid", thirdreference);
		expected.put("am_timestamp", username);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

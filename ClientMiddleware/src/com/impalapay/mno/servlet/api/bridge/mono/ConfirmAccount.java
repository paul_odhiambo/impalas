package com.impalapay.mno.servlet.api.bridge.mono;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLFincra;
import com.impalapay.airtel.util.net.YoUgandaRequestUtil;

import net.sf.ehcache.CacheManager;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Allows for querying of balance through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class ConfirmAccount extends HttpServlet {

	private PostWithIgnoreSSLFincra postMinusThread;

	private Map<String, String> toairtel = new HashMap<>();

	private String CLIENT_URL = "";
	private Logger logger;


	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkBalance(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkBalance(HttpServletRequest request) throws IOException {
		Account account = null;
		// joined json string
		String join = "", responseobject = "";

		boolean responsestatus = false;

		JsonElement root = null, roots = null;

		JsonObject vendorfields = null, root2 = null, queryresponse = null, vendorfield = null, confirmrequest = null;

		// for balance response
		String recipientdetails = "", switchresponse = "", statusdescription = "";

		// These represent parameters received over the network
		String username = "", password = "", receiverqueryurl = "", receivermsisdn = "", firstname = "", lastname = "",
				secondname = "", xmlData = "", gender = "", firstname2 = "", lastname2 = "", results2 = "",
				jsonResult = "", bankcode = "", jsonData = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ##############################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ##############################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			firstname = root.getAsJsonObject().get("first_name").getAsString();
			secondname = root.getAsJsonObject().get("second_name").getAsString();
			lastname = root.getAsJsonObject().get("last_name").getAsString();
			username = root.getAsJsonObject().get("username").getAsString();
			password = root.getAsJsonObject().get("password").getAsString();
			receivermsisdn = root.getAsJsonObject().get("receiveraccount").getAsString();
			receiverqueryurl = root.getAsJsonObject().get("receivercheckurl").getAsString();
			
			root2 = root.getAsJsonObject();


		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(receiverqueryurl)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (!root2.has("vendor_uniquefields")) {
			expected.put("status_code", "00032");
			expected.put("status_description",
					"vendor_uniquefields Object missing should have the following key/value(bankcode)");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		try {
			vendorfields = root.getAsJsonObject().get("vendor_uniquefields").getAsJsonObject();
			bankcode = vendorfields.getAsJsonObject().get("bank_code").getAsString();

		} catch (Exception e) {
			// TODO: handle exception
			expected.put("status_code", "00032");
			expected.put("status_description", "missing parameters on vendor unique field");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (StringUtils.isBlank(bankcode)) {

			expected.put("status_code", "00032");
			expected.put("status_description", "empty parameters on vendor_unique field object");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// generate UUID a transaction UUID
		confirmrequest = new JsonObject();
		confirmrequest.addProperty("accountNumber", receivermsisdn);
		confirmrequest.addProperty("bankCode", bankcode);
		jsonData = g.toJson(confirmrequest);

		// retrieve the countryip to be used as URL
		CLIENT_URL = receiverqueryurl;

		postMinusThread = new PostWithIgnoreSSLFincra(CLIENT_URL, jsonData, password);

		try {
			// capture the switch respoinse.
			responseobject = postMinusThread.doPost();
			roots = new JsonParser().parse(responseobject);
			responsestatus = roots.getAsJsonObject().get("success").getAsBoolean();

		} catch (Exception e) {
			// ================================================
			// Missing fields in response from receiver system
			// ================================================

			responsestatus = false;
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;

		}
		
		logger.error(".....................................................");
		logger.error("FINCRA CONFIRM BANK REQUEST URL :" + CLIENT_URL + "\n");
		logger.error("FINCRA CONFIRM BANK REQUEST FROM BRIDGE :" + jsonData + "\n");
		logger.error("FINCRA CONFIRM BANK DEBIT RESPONSE:" + roots + "\n");
		logger.error(".....................................................");

		if (responsestatus) {

			statusdescription = "TRUE";

			queryresponse = new JsonObject();

			queryresponse.addProperty("status_code", "400");
			queryresponse.addProperty("status_description", statusdescription);

			results2 = g.toJson(queryresponse);

		} else {
			expected.put("status_code", "600");
			expected.put("status_description", "FALSE");
			results2 = g.toJson(expected);

		}

		expected.put("status_code", switchresponse);
		expected.put("status_description", statusdescription);
		results2 = g.toJson(expected);

		return results2;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

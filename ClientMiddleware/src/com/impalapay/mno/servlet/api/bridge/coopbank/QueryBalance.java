package com.impalapay.mno.servlet.api.bridge.coopbank;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLCOOP;
import net.sf.ehcache.CacheManager;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.*;

/**
 *
 * @author Kelvin Muli
 */
public class QueryBalance extends HttpServlet {

	private PostWithIgnoreSSLCOOP postwithignoresslcoop;

	private Map<String, String> tocoop = new HashMap<>();

	private String CLIENT_URL = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkBalance(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkBalance(HttpServletRequest request) throws IOException {
		Account account = null;
		// joined json string
		String join = "", responseobject = "";

		JsonElement root = null, roots = null;

		// for balance response
		String switchbalance = "", statusdescription = "", statuscode = "";

		// These represent parameters received over the network
		String username = "", password = "", receiverqueryurl = "", sourcemsisdn = "";

		// Unique fields for COOP
		String hashkey = "", accountnumber = "", credentials = "", saltedToken = "", hash = "";
		MessageDigest md = null;
		int responseCode = 0;

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ##############################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ##############################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			password = root.getAsJsonObject().get("password").getAsString();
			receiverqueryurl = root.getAsJsonObject().get("receiverqueryurl").getAsString();
			sourcemsisdn = root.getAsJsonObject().get("sourceaccount").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(receiverqueryurl)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// generate UUID a transaction UUID
		String transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		// ################################################################################
		// fetch credentials from property file.
		// ###############################################################################
		hashkey = PropertiesConfig.getConfigValue("COOPTEXT_PATH");




		// retrieve the countryip to be used as URL

		tocoop.put("MessageReference", sourcemsisdn);
		tocoop.put("AccountNumber", accountnumber);
		String jsonData = g.toJson(tocoop);

		CLIENT_URL = receiverqueryurl;

		try {
			postwithignoresslcoop = new PostWithIgnoreSSLCOOP(CLIENT_URL, jsonData, hashkey);
			// capture the switch respoinse.
			responseobject = postwithignoresslcoop.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);


			statuscode =roots.getAsJsonObject().get("MessageDescription").getAsString();

		} catch (Exception e) {
			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("command_status", APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// map ACCEPTED FOR PROCESSING
		if (statuscode.equalsIgnoreCase("Success")) {
			statuscode = "S000";
			// statusdescription = "SUCCESS";
			switchbalance =roots.getAsJsonObject().get("ClearedBalance").getAsString();
			statusdescription =roots.getAsJsonObject().get("MessageDescription").getAsString();
		} else {

			statuscode = "00029";
		}

		// =====================================================================
		// construct object to return to sender system
		// =====================================================================

		expected.put("balance", switchbalance);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}
package com.impalapay.mno.servlet.api.bridge.tola;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;

public class ListenerMobileCollection extends HttpServlet {

	// private PostMinusThread postMinusThread;
	private Cache transactionStatusCache;
	private HashMap<String, String> transactionStatusHash = new HashMap<>();
	private Logger logger;
	private PostWithIgnoreSSL postMinusThread;
	private String CLIENT_URL = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {

		// joined json string
		String join = "", jsonResult = "";
		JsonElement root = null, roots = null;
		;
		JsonObject root2 = null, data = null, creditrequest = null;

		String accountname = "", amount = "", amounttype = "", network = "", currency = "", customerreference = "",
				date = "", msisdn = "", operatorreference = "", reference = "", target = "", type = "", statuscode = "",
				statusdescription = "", sourcereference = "", responseobject = "", referencenumber = "",
				debitorname = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		logger.error("PesaChoise Lister Request :" + join);

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			accountname = root.getAsJsonObject().get("accountname").getAsString();

			amount = root.getAsJsonObject().get("amount").getAsString();

			amounttype = root.getAsJsonObject().get("amounttype").getAsString();

			network = root.getAsJsonObject().get("channel").getAsString();

			currency = root.getAsJsonObject().get("currency").getAsString();

			customerreference = root.getAsJsonObject().get("customerreference").getAsString();

			date = root.getAsJsonObject().get("date").getAsString();

			msisdn = root.getAsJsonObject().get("msisdn").getAsString();

			operatorreference = root.getAsJsonObject().get("operatorreference").getAsString();

			reference = root.getAsJsonObject().get("reference").getAsString();

			sourcereference = root.getAsJsonObject().get("sourcereference").getAsString();

			target = root.getAsJsonObject().get("target").getAsString();

			type = root.getAsJsonObject().get("type").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(sourcereference) || StringUtils.isBlank(reference) || StringUtils.isBlank(amount)
				|| StringUtils.isBlank(customerreference) || StringUtils.isBlank(msisdn)
				|| StringUtils.isBlank(operatorreference)) {

			expected.put("status_code", "00032");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		Element element;

		List keys;

		// ======================================================
		// Populate with the mapping of Transaction Statuses.
		// The key is a UUID of the status
		// ======================================================

		TransactionStatus status1;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status1.getStatus(), status1.getUuid());
		}

		// Check if name is inluded
		if (StringUtils.isBlank(accountname)) {
			debitorname = "NoName Provided";
		} else {
			debitorname = accountname;
		}

		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################
		// map ACCEPTED FOR PROCESSING
		creditrequest = new JsonObject();
		creditrequest.addProperty("collection_number", target);
		creditrequest.addProperty("reference_number", customerreference);
		creditrequest.addProperty("debitor_account", msisdn);
		creditrequest.addProperty("debitor_currency_code", currency);
		creditrequest.addProperty("debitor_transaction_id", reference);
		creditrequest.addProperty("amount", amount);
		creditrequest.addProperty("debitor_name", debitorname);
		creditrequest.addProperty("collection_datetime", date);

		CLIENT_URL = PropertiesConfig.getConfigValue("COLLECTION_BRIDGEURL");

		String jsonData = g.toJson(creditrequest);

		postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonData);

		try {

			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			statuscode = roots.getAsJsonObject().get("status_code").getAsString();

			// exctract a specific json element from the object(status_code)
			statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

		} catch (Exception e) {
			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			referencenumber = "investigateTransaction";
			statuscode = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
		}

		logger.error(".....................................................");
		logger.error("TOLA LISTENER REQUEST RESPONSE FROM BRIDGE ");
		logger.error(".....................................................");
		logger.error("RESPONSE " + roots + "\n");

		if (!statuscode.equalsIgnoreCase("S000")) {
			// means this failed
			expected.put("success", "false");

			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		expected.put("success", "true");
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.mno.servlet.api.bridge.tola;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLSoftTeller;

import net.sf.ehcache.CacheManager;
import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryStatus extends HttpServlet {

	private String CLIENT_URL = "", USERID = "";
	private PostWithIgnoreSSLSoftTeller postMinusThread;
	private MessageDigest md, mdSha;
	private Logger logger;
	private StringBuffer sb, sbSha;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonObject creditrequest = null;
		String responseobject = "", statuscode = "", statusdescription = "";

		// These represent parameters received over the network
		String username = "", sessionid = "", referencenumber = "", receiveruuid = "", receiverquery = "",
				jsonResult = "", passwordtoEncrypt = "", pass = "", jsonData = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			sessionid = root.getAsJsonObject().get("password").getAsString();
			referencenumber = root.getAsJsonObject().get("transactionid").getAsString();
			receiveruuid = root.getAsJsonObject().get("receivertransactionid").getAsString();
			receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		USERID = PropertiesConfig.getConfigValue("SOFTELLER_USERID");

		// Hashing password

		try {

			md = MessageDigest.getInstance("MD5");
			md.update(USERID.getBytes());
			byte byteData[] = md.digest();
			// convert the byte to hex format method 1
			sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("status_description", "MD5 encryption failure");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// System.out.println("Digest(in hex format):: " + sb.toString());

		// Combine hashed user with password
		passwordtoEncrypt = sb.toString() + sessionid;

		try {
			mdSha = MessageDigest.getInstance("SHA-256");
			mdSha.update(passwordtoEncrypt.getBytes());

			byte byteDataSha[] = mdSha.digest();

			// convert the byte to hex format method 1
			sbSha = new StringBuffer();
			for (int i = 0; i < byteDataSha.length; i++) {
				sbSha.append(Integer.toString((byteDataSha[i] & 0xff) + 0x100, 16).substring(1));
			}

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("status_description", "SHA-256 encryption failure");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// System.out.println("Hex format : " + sbSha.toString());

		// Result of hashing
		pass = sbSha.toString();
		creditrequest = new JsonObject();

		creditrequest.addProperty("Login", username);
		creditrequest.addProperty("Password", pass);
		creditrequest.addProperty("TransactionCode", receiveruuid);

		CLIENT_URL = receiverquery;

		jsonData = g.toJson(creditrequest);

		postMinusThread = new PostWithIgnoreSSLSoftTeller(CLIENT_URL, jsonData);

		try {

			// capture the switch respoinse.
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			statuscode = roots.getAsJsonObject().get("status").getAsString();

			statusdescription = roots.getAsJsonObject().get("message").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("command_status", responseobject);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		logger.error(".....................................................");
		logger.error("SOFTELLER QUERY REQUEST FROM BRIDGE :" + jsonData + "\n");
		logger.error("SOFTELLER QUERY RESPONSE:" + roots + "\n");
		logger.error(".....................................................");

		// map ACCEPTED FOR PROCESSING
		if (statuscode.equalsIgnoreCase("01")) {

			statuscode = "S000";

		}

		if (statuscode.equalsIgnoreCase("100")) {

			statuscode = "00029";

		}

		if (statuscode.equalsIgnoreCase("1000")) {

			statuscode = "S001";

		}

		String success = "S000", failed = "00029", pending = "S001";

		if (statuscode.equalsIgnoreCase(success) || statuscode.equalsIgnoreCase(failed)
				|| statuscode.equalsIgnoreCase(pending)) {

			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		expected.put("am_timestamp", username);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

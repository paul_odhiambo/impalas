package com.impalapay.mno.servlet.api.bridge.youganda;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLYoPayment;
import com.impalapay.airtel.util.net.YoUgandaRequestUtil;

import net.sf.ehcache.CacheManager;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryStatus extends HttpServlet {

	private String CLIENT_URL = "";
	private PostWithIgnoreSSLYoPayment postMinusThread;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		String responseobject = "";

		// These represent parameters received over the network
		String username = "", sessionid = "", referencenumber = "", receiveruuid = "", receiverquery = "",
				statuscode = "", statusdescription = "", yotransactionid = "", xmlData = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			sessionid = root.getAsJsonObject().get("password").getAsString();
			referencenumber = root.getAsJsonObject().get("transactionid").getAsString();
			receiveruuid = root.getAsJsonObject().get("receivertransactionid").getAsString();
			receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		xmlData = YoUgandaRequestUtil.createCheckTransactionStatusXML(receiveruuid, username, sessionid);
		CLIENT_URL = receiverquery;

		postMinusThread = new PostWithIgnoreSSLYoPayment(CLIENT_URL, xmlData);

		try {

			// capture the switch respoinse.
			responseobject = postMinusThread.doPost();

			// ===============================================================
			// an object that will contain parameters for provisional response
			// ===============================================================
			// exctract a specific json element from the object(status_code)
			statuscode = YoUgandaRequestUtil.getXMLValue(responseobject, "/AutoCreate/Response/StatusCode");

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("command_status", APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}

		// map SUCCESS TRANSACTION
		if (statuscode.equalsIgnoreCase("0")) {
			statuscode = "S000";
			statusdescription = "SUCCESS";
		}

		// map FAILED TRANSACTION
		if (statuscode.equalsIgnoreCase("2") || statuscode.equalsIgnoreCase("7") || statuscode.equalsIgnoreCase("10")
				|| statuscode.equalsIgnoreCase("14") || statuscode.equalsIgnoreCase("18")
				|| statuscode.equalsIgnoreCase("22") || statuscode.equalsIgnoreCase("24")
				|| statuscode.equalsIgnoreCase("26") || statuscode.equalsIgnoreCase("28")) {
			statuscode = "00029";
			statusdescription = "FAILED_TRANSACTION";
		}

		String success = "S000";
		String fail = "00029";
		if (statuscode.equalsIgnoreCase(success)) {

			expected.put("am_timestamp", username);
			expected.put("am_referenceid",receiveruuid);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (statuscode.equalsIgnoreCase(fail)) {
			expected.put("am_timestamp", username);
			expected.put("am_referenceid", receiveruuid);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("am_timestamp", username);
		expected.put("am_referenceid", receiveruuid);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

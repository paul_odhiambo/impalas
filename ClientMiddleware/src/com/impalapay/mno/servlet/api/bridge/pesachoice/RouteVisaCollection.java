package com.impalapay.mno.servlet.api.bridge.pesachoice;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLUGmart;

public class RouteVisaCollection extends HttpServlet {

	// private PostMinusThread postMinusThread;
	private PostWithIgnoreSSLUGmart postMinusThread;
	private Cache accountsCache;
	private String CLIENT_URL = "", TOKEN = "";
	private SimpleDateFormat sdf;
	private Timestamp timestamp;
	private SecretKeySpec key;
	private byte[] bytesEncoded, bytes;
	private Mac mac;
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);
		sdf = new SimpleDateFormat("YYYY-MM-dd");
		timestamp = new Timestamp(System.currentTimeMillis());
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null, rootsresponse = null;
		JsonObject root2 = null, creditrequest = null, vendoruniqueobject = null, returnserver = null, data = null,
				paymentServiceInfo = null, card = null, cardDetails = null, defaultSource = null;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", sourcecountrycode = "",
				email = "", encryptedPassword = "", recipientcurrencycode = "", recipientcountrycode = "",
				sourcemsisdn = "", recipientmobile = "", sendername = "", amountstring = "", remiturlss = "",
				responseobject = "", statuscode = "", mytimestamp = "", statusdescription = "", referencenumber = "",
				token = "", debitreferenceno = "", payment_url = "", networkname = "", jsonResult = "",
				transactionuuid = "", actionType = "THIRD_PARTY_PAYMENT", products = "Event", companyId = "Impalapay",
				cardtype = "", cardcvv = "", cardexpmonth = "", cardexpyear = "", debitcontact = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("debitcountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("debitcurrencycode").getAsString();

			debitcontact = root.getAsJsonObject().get("debitcontact").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			sendername = root.getAsJsonObject().get("debitname").getAsString();

			recipientmobile = root.getAsJsonObject().get("debitaccount").getAsString();

			debitreferenceno = root.getAsJsonObject().get("debitreferencenumber").getAsString();

			networkname = root.getAsJsonObject().get("networkname").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();
			cardtype = root.getAsJsonObject().get("card_type").getAsString();
			cardcvv = root.getAsJsonObject().get("card_cvv").getAsString();
			cardexpmonth = root.getAsJsonObject().get("card_expmonth").getAsString();
			cardexpyear = root.getAsJsonObject().get("card_expyear").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(recipientcurrencycode)
				|| StringUtils.isBlank(sendername) || StringUtils.isBlank(amountstring)
				|| StringUtils.isBlank(remiturlss) || StringUtils.isBlank(recipientcountrycode)) {

			expected.put("status_code", "00032");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}
		// check for the special parameters.
		if (StringUtils.isBlank(cardtype) || StringUtils.isBlank(cardcvv) || StringUtils.isBlank(cardexpmonth)
				|| StringUtils.isBlank(cardexpyear)) {

			expected.put("status_code", "00032");
			expected.put("status_description",
					"The following field are manadotory on this endpoint (card_type,card_cvv,card_expmonth,card_expyear)");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("status_code", "00032");
			expected.put("status_description", "unauthorised user");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		email = apiusername;

		mytimestamp = sdf.format(timestamp);
		bytesEncoded = Base64.encodeBase64(email.getBytes());

		key = new SecretKeySpec((apipassword).getBytes("UTF-8"), "HmacSHA512");
		try {
			mac = Mac.getInstance("HmacSHA512");
			mac.init(key);
		} catch (Exception e) {
			expected.put("command_status", "HASHING_ERROR_CONTACT_ADMIN");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		bytes = mac.doFinal(bytesEncoded);

		encryptedPassword = Hex.encodeHexString(bytes);

		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################

		creditrequest = new JsonObject();
		cardDetails = new JsonObject();
		defaultSource = new JsonObject();

		paymentServiceInfo = new JsonObject();
		paymentServiceInfo.addProperty("price", amountstring);
		paymentServiceInfo.addProperty("customerPhoneNumber", debitcontact);
		paymentServiceInfo.addProperty("customerName", sendername);

		card.addProperty("classType", "PCCardInfo");
		card.addProperty("number", recipientmobile);
		card.addProperty("monthOfExpiration", cardexpmonth);
		card.addProperty("yearOfExpiration", cardexpyear);
		card.addProperty("cvv", cardcvv);
		card.addProperty("cardType", cardtype);
		// "cardType":"Mastercard"
		// "paymentType": "debit_card"
		cardDetails.add("cardDetails", card);
		defaultSource.add("defaultSource", cardDetails);
		defaultSource.addProperty("paymentType", "debit_card");

		creditrequest.addProperty("companyId", email);
		creditrequest.addProperty("password", encryptedPassword);
		creditrequest.addProperty("country", sourcecountrycode);
		creditrequest.addProperty("actionType", actionType);
		creditrequest.addProperty("products", products);
		creditrequest.addProperty("description", "payment");
		creditrequest.addProperty("price", amountstring);
		creditrequest.addProperty("contactEmail", sendername + "@gmail.com");
		creditrequest.addProperty("contactPhoneNumber", debitcontact);
		creditrequest.addProperty("date", mytimestamp);
		creditrequest.add("paymentServiceInfo", paymentServiceInfo);
		creditrequest.add("paymentInfo", defaultSource);

		/**
		 * if (vendorfields != null) { creditrequest.add("vendor_uniquefields",
		 * vendorfields); }
		 **/

		// assign the remit url from properties.config
		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");

		CLIENT_URL = remiturlss;

		String jsonData = g.toJson(creditrequest);

		// postMinusThread = new PostMinusThread(CLIENT_URL, jsonData);
		postMinusThread = new PostWithIgnoreSSLUGmart(CLIENT_URL, jsonData, TOKEN);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {

			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			statuscode = roots.getAsJsonObject().get("transactionStatus").getAsString();

			statusdescription = roots.getAsJsonObject().get("transactionStatus").getAsString();

			transactionuuid = roots.getAsJsonObject().get("transactionId").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			referencenumber = "investigateTransaction";
			statuscode = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
		}

		// map ACCEPTED FOR PROCESSING
		if (statuscode.equalsIgnoreCase("IN_PROGRESS")) {
			statuscode = "S001";
		} else if (statuscode.equalsIgnoreCase("SUCCESS")) {

			statuscode = "S000";

		} else {
			statuscode = "00029";

			// System.out.println(creditrequest)
		}

		String success = "S000";

		if (statuscode.equalsIgnoreCase(success)) {

			returnserver.addProperty("am_referenceid", transactioinid);
			returnserver.addProperty("am_timestamp", username);
			returnserver.addProperty("status_code", "S000");
			returnserver.addProperty("status_description", "SUCCESS");

			jsonResult = g.toJson(returnserver);

			return jsonResult;

		}
		expected.put("am_referenceid", transactioinid);
		expected.put("am_timestamp", username);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

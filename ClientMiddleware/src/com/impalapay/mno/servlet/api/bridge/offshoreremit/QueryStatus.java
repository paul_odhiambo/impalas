package com.impalapay.mno.servlet.api.bridge.offshoreremit;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
import com.impalapay.airtel.beans.accountmgmt.Account;

import net.sf.ehcache.CacheManager;
import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryStatus extends HttpServlet {

	private String CLIENT_URL = "", TOKEN = "", TEXT_PATH = "";
	private PostWithIgnoreSSL postMinusThread;
	private Logger logger;
	private MessageDigest md;
	private OffshoreSecretUtil myEncryptor;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonArray responsetoobject = null;
		JsonObject jsonobject = null, queryrequest = null, transactionrequestobject = null, queryobject = null,
				beforeencryptobject = null;
		String responseobject = "", results2 = "", jsonResult = "", token = "";

		// These represent parameters received over the network
		String username = "", sessionid = "", referencenumber = "", receiveruuid = "", receiverquery = "",
				statuscode1 = "", statusdescription1 = "", statuscode2 = "", statusdescription2 = "", statuscode = "",
				statusdescription = "", toencrypt = "", jsonEncrypt = "", data = "", jsonData = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		logger.error(".....................................................");
		logger.error("QUERY OFFSHORE REQUEST :" + join);
		logger.error("....................................................." + "\n" + "\n");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		JsonArray jsonarray = new JsonArray();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			sessionid = root.getAsJsonObject().get("password").getAsString();
			referencenumber = root.getAsJsonObject().get("transactionid").getAsString();
			receiveruuid = root.getAsJsonObject().get("receivertransactionid").getAsString();
			receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// String merchantcode= "20180327001";
		// String encryptseed= "PoPR}QAUrCyadKLfQdpxM4ZW";
		// String signseed= "PoPR}QAUrCyadKLfQdpxM4ZW";
		// remiturlss= "http://47.75.126.20:8080/offshore/api/query";
		transactionrequestobject = new JsonObject();
		beforeencryptobject = new JsonObject();
		toencrypt = "merchant=" + username + "&merchantBatchId=" + referencenumber + "&orderId=" + receiveruuid
				+ sessionid;

		// md = MessageDigest.getInstance("MD5");
		md.update(toencrypt.getBytes());

		byte byteData[] = md.digest();

		// convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}

		// System.out.println("Digest(in hex format):: " + sb.toString());

		// convert the byte to hex format method 2
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			String hex = Integer.toHexString(0xff & byteData[i]);
			if (hex.length() == 1)
				hexString.append('0');
			hexString.append(hex);
		}

		beforeencryptobject.addProperty("merchant", username);
		beforeencryptobject.addProperty("merchantBatchId", referencenumber);
		beforeencryptobject.addProperty("orderId", receiveruuid);
		beforeencryptobject.addProperty("sign", hexString.toString());

		jsonEncrypt = g.toJson(beforeencryptobject);

		try {
			myEncryptor = new OffshoreSecretUtil();
			data = myEncryptor.encrypt(jsonEncrypt);
		} catch (Exception e) {
			expected.put("command_status", "HASHING_ERROR_CONTACT_ADMIN");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		transactionrequestobject.addProperty("merchant", username);
		transactionrequestobject.addProperty("data", data);

		CLIENT_URL = receiverquery;

		jsonData = g.toJson(transactionrequestobject);

		// CLIENT_URL =
		// "https://wallet.ugmart.ug/transactions/ae3386d65e2745359ec5e4b5a48c1a60";
		postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonData);

		try {

			// capture the switch respoinse.
			responseobject = postMinusThread.doPost();

			roots = new JsonParser().parse(responseobject);

			statuscode = roots.getAsJsonObject().get("code").getAsString();

			statusdescription = roots.getAsJsonObject().get("message").getAsString();
		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("command_status", APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		logger.error(".....................................................");
		logger.error("Query OFFSHORE STATUS RESPONSE " + responseobject + " extracted STATUS " + statuscode);
		logger.error("....................................................." + "\n");

		if (statuscode.equalsIgnoreCase("0000")) {
			statuscode = "S000";
			System.out.println("Transaction Successful");
		} else {
			statuscode = "00029";
			statusdescription = "FAILED_TRANSACTION";
		}

		String success = "S000", fail = "00029", inprogress = "S001";

		if (statuscode.equalsIgnoreCase(success)) {

			expected.put("am_timestamp", String.valueOf(new Date().getTime()));
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("am_timestamp", String.valueOf(new Date().getTime()));
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

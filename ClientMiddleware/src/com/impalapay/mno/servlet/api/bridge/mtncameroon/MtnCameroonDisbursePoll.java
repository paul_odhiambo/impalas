package com.impalapay.mno.servlet.api.bridge.mtncameroon;

import java.util.HashMap;
import com.google.gson.Gson;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This Quartz job is used to check for Session Ids in the SessionLog database
 * table for expiry. Session Ids are expired after a fixed duration of time.
 * <p>
 * Copyright (c) Tawi Commercial Services Ltd., Sep 23, 2014
 * 
 * @author <a href="mailto:michael@tawi.mobi">Michael Wakahe</a>
 * 
 */

public class MtnCameroonDisbursePoll implements Job {

	private PostWithIgnoreSSL postMinusThread;

	private String CLIENT_URL = "";
	private String responseobject = "", receiverurl = "", username = "", password = "";
	private Logger logger;
	private HashMap<String, String> expected = new HashMap<>();

	/**
	 * Empty constructor for job initialization
	 * <p>
	 * Quartz requires a public empty constructor so that the scheduler can
	 * instantiate the class whenever it needs.
	 */
	public MtnCameroonDisbursePoll() {
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * <p>
	 * Called by the <code>{@link org.quartz.Scheduler}</code> when a
	 * <code>{@link org.quartz.Trigger}</code> fires that is associated with the
	 * <code>Job</code>.
	 * </p>
	 * 
	 * @throws JobExecutionException
	 *             if there is an exception while executing the job.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Gson g = new Gson();

		receiverurl = PropertiesConfig.getConfigValue("MTNCAMEROONDISBURSE_AUTHURL");
		username = PropertiesConfig.getConfigValue("MTNCAMEROONCOLLECTION_USERNAME");
		password = PropertiesConfig.getConfigValue("MTNCAMEROONCOLLECTION_PASSWORD");

		CLIENT_URL = PropertiesConfig.getConfigValue("MTNCAMEROONDISBURSEINTERNAL_AUTHURL");

		expected.put("username", username);
		expected.put("password", password);
		expected.put("receiverqueryurl", receiverurl);

		String jsonforxData = g.toJson(expected);

		logger.error(".....................................................");
		logger.error(" START AUTOMATIC MTNCAMEROONDISBURSE SESSIONREQUEST" + "\n");
		logger.error(" MTNCAMEROONDISBURSE SESSION REQUEST THE URL "+ CLIENT_URL+ "\n");
		logger.error(".....................................................");

		try {

			postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonforxData);

			// capture the switch respoinse.
			responseobject = postMinusThread.doPost();

			System.out.println("Mtn Session request " + jsonforxData + "\n" + " Mtn session response "
					+ responseobject + " " + "\n");
		} catch (Exception e) {
			logger.error(".....................................................");
			logger.error(" ERROR OCCURED DURING AUTOMATIC MTNCAMEROONDISBURSE SESSIONREQUEST" + "\n");
			logger.error(responseobject);
			logger.error(".....................................................");
			System.out.println(jsonforxData + " MTNCAMEROONDISBURSE session Request error" + responseobject);
		}

		logger.error(".....................................................");
		logger.error(" RESPONSE FOR AUTOMATIC MTNCAMEROONDISBURSE SESSIONREQUEST" + "\n");
		logger.error(responseobject);
		logger.error(".....................................................");
	}

}

package com.impalapay.mno.servlet.api.bridge.vitesse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLVitesse;
import com.impalapay.airtel.beans.accountmgmt.Account;

import net.sf.ehcache.CacheManager;


import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryStatus extends HttpServlet {

	private String CLIENT_URL = "", TOKEN = "", TEXT_PATH = "";
	private PostWithIgnoreSSLVitesse postMinusThread;
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonObject queryrequest = null;
		String responseobject = "", results2 = "", jsonResult = "", token = "";

		// These represent parameters received over the network
		String username = "", sessionid = "", referencenumber = "", receiveruuid = "", receiverquery = "",
				statuscode1 = "", statusdescription1 = "", statuscode = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		logger.error(".....................................................");
		logger.error("QUERY VITTESSE REQUEST :" + join);
		logger.error("....................................................." + "\n" + "\n");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		JsonArray jsonarray = new JsonArray();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			sessionid = root.getAsJsonObject().get("password").getAsString();
			referencenumber = root.getAsJsonObject().get("transactionid").getAsString();
			receiveruuid = root.getAsJsonObject().get("receivertransactionid").getAsString();
			receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		TEXT_PATH = PropertiesConfig.getConfigValue("VITESSETEXT_TOKEN");

		
		TOKEN = TEXT_PATH;

		queryrequest = new JsonObject();

		results2 = g.toJson(queryrequest);

		CLIENT_URL = receiverquery + receiveruuid;
		// CLIENT_URL =
		// "https://wallet.ugmart.ug/transactions/ae3386d65e2745359ec5e4b5a48c1a60";
		postMinusThread = new PostWithIgnoreSSLVitesse(CLIENT_URL, results2, TOKEN);

		try {

			// capture the switch respoinse.
			responseobject = postMinusThread.doGet();

			roots = new JsonParser().parse(responseobject);

			// START THE HARDWORK.
			statuscode = roots.getAsJsonObject().get("Status").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("command_status", APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		logger.error(".....................................................");
		logger.error("Query VITTESSE RESPONSE " + responseobject + " extracted STATUS " + statuscode);
		logger.error("....................................................." + "\n");

		// map ACCEPTED FOR PROCESSING
		// map ACCEPTED FOR PROCESSING
				if (statuscode.equalsIgnoreCase("Succeeded")) {
					statuscode = "S000";
				}

				if (statuscode.equalsIgnoreCase("Pending")) {
					statuscode = "S001";
				}

				if (statuscode.equalsIgnoreCase("Invalid")) {
					statuscode = "00029";
				}

				String success = "S000", inprogress = "S001", failed = "00029";
		if (statuscode.equalsIgnoreCase(success)) {

			expected.put("am_referenceid", referencenumber);
			expected.put("am_timestamp", username);
			expected.put("status_code", "S000");
			expected.put("status_description", statusdescription1);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (statuscode.equalsIgnoreCase(inprogress)) {
			expected.put("status_code", "S001");
			expected.put("status_description", statusdescription1);
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription1);

		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.mno.servlet.api.bridge.magma;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.CacheManager;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostThreadUniversalUpdated;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;

public class MagmaDisburseListener extends HttpServlet {

	// private PostWithIgnoreSSL postMinusThread;
	private PostThreadUniversalUpdated postMinusThread;
	private String CLIENT_URL = "";
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonObject root2 = null, creditrequest = null, responsetoreceiver = null, resultbody = null;

		String transactioinid = "", responseobject = "", invoicenumber = "", statuscode = "", statusdescription = "",
				jsonResult = "", mpesatransactionid = "", jsonData = "";

		/**{
			"transaction_id": "string",
			"status": "string",
			"operator_transaction_id": "string",
			"comment": "string",
			"amount": "integer"
		}**/
		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		logger.error(".....................................................");
		logger.error("MAGMA DISBURSEMENT INCOMING REQUEST :" + join);
		logger.error(".....................................................");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			// {"stkCallback":
			// {"MerchantRequestID":"22847-13990269-1","CheckoutRequestID":"ws_CO_12012022203543908195","ResultCode":1032,"ResultDesc":"Request
			// cancelled by user"}
			// }
			invoicenumber = resultbody.getAsJsonObject().get("operator_transaction_id").getAsString();

			mpesatransactionid = resultbody.getAsJsonObject().get("transaction_id").getAsString();
			
			statuscode = resultbody.getAsJsonObject().get("status").getAsString();

			statusdescription = resultbody.getAsJsonObject().get("comment").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (resultbody.equals(null)) {

			expected.put("status_code", "server error");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################

		// map SUCCESS TRANSACTION
		if (statuscode.equalsIgnoreCase("SUCCESS")) {
			statuscode = "S000";
			statusdescription = "SUCCESS";
		} else if (statuscode.equalsIgnoreCase("1") || statuscode.equalsIgnoreCase("2")
				|| statuscode.equalsIgnoreCase("3") || statuscode.equalsIgnoreCase("4")
				|| statuscode.equalsIgnoreCase("5") || statuscode.equalsIgnoreCase("7")
				|| statuscode.equalsIgnoreCase("8") || statuscode.equalsIgnoreCase("12")
				|| statuscode.equalsIgnoreCase("14") || statuscode.equalsIgnoreCase("15")
				|| statuscode.equalsIgnoreCase("1032") || statuscode.equalsIgnoreCase("2001")) {
			statuscode = "00029";
			// statusdescription = "FAILED_TRANSACTION";
		} else {
			statuscode = "S001";
			statusdescription = "CREDIT_INPROGRESS";

			return "This transaction is still pending";
		}

		creditrequest = new JsonObject();

		creditrequest.addProperty("am_timestamp", "demosdkpush");
		creditrequest.addProperty("am_referenceid", mpesatransactionid);
		creditrequest.addProperty("status_code", statuscode);
		creditrequest.addProperty("status_description", statusdescription);

		// assign the remit url from properties.config
		CLIENT_URL = PropertiesConfig.getConfigValue("DISBURSEMENTLISTENER_BRIDGEURL");

		jsonData = g.toJson(creditrequest);

		postMinusThread = new PostThreadUniversalUpdated(CLIENT_URL, jsonData);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {

			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postMinusThread.runurl();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			statuscode = roots.getAsJsonObject().get("status_code").getAsString();

			// exctract a specific json element from the object(status_code)
			statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			transactioinid = "investigateTransaction";
			statuscode = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
		}
		logger.error(".....................................................");
		logger.error("MAGMA DISBURSEMENT LISTENER REQUEST RESPONSE FROM BRIDGE ");
		logger.error("THE URL " + CLIENT_URL + " THE REQUEST " + jsonData);
		logger.error(".....................................................");
		logger.error("RESPONSE " + roots + "\n");

		// =====================================================================
		// construct object to return to sender system
		// =====================================================================
		responsetoreceiver = new JsonObject();
		responsetoreceiver.addProperty("am_referenceid", transactioinid);
		responsetoreceiver.addProperty("status_code", statuscode);
		responsetoreceiver.addProperty("status_description", statusdescription);

		return responsetoreceiver.toString();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}
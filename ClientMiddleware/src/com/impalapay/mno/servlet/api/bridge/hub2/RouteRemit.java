package com.impalapay.mno.servlet.api.bridge.hub2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.PhonenumberSplitUtil;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLHub2;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLHubtel;

public class RouteRemit extends HttpServlet {

	private PostWithIgnoreSSLHub2 postMinusThread;
	private Cache accountsCache;
	private String CLIENT_URL = "", TOKEN = "", TEXT_PATH = "", CALLBACKURI = "", CERT_PATH = "";
	private Logger logger;
	private PhonenumberSplitUtil phonenumbersplit;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null, encodedpass = null;
		JsonObject root2 = null, creditrequest = null, responsetoreceiver = null, responserequest = null,
				hubtelobject = null, destinationobject = null;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", sourcecountrycode = "",
				recipientcurrencycode = "", recipientcountrycode = "", sourcemsisdn = "", recipientmobile = "",
				sendername = "", amountstring = "", remiturlss = "", responseobject = "", statuscode = "",
				statusdescription = "", referencenumber = "", token = "", jsonResult = "", encodedpasswordjson = "",
				encodestatuscode = "", encodestatusdescription = "", encryptedpassword = "", failed = "00029",
				phoneresults = "", mno = "", jsonData = "",merchantid="";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			sourcecountrycode = root.getAsJsonObject().get("sourcecountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("recipientcurrencycode").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("recipientcountrycode").getAsString();

			sendername = root.getAsJsonObject().get("Sender_Name").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();

			sourcemsisdn = root.getAsJsonObject().get("source_msisdn").getAsString();

			recipientmobile = root.getAsJsonObject().get("beneficiary_msisdn").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(sourcecountrycode)
				|| StringUtils.isBlank(recipientcurrencycode) || StringUtils.isBlank(sendername)
				|| StringUtils.isBlank(amountstring) || StringUtils.isBlank(remiturlss)
				|| StringUtils.isBlank(recipientcountrycode)) {

			expected.put("status_code", "server error");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

		}

		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("status_code", "server error");
			expected.put("status_description", "unauthorised user");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		CALLBACKURI = PropertiesConfig.getConfigValue("DISBURSEMENTHUB2CALLBACKURL");
		token = PropertiesConfig.getConfigValue("HUB2AUTHKEY");
		merchantid = PropertiesConfig.getConfigValue("HUB2MERCHANTID");

		

		// Assign mno field Value.
		phonenumbersplit = new PhonenumberSplitUtil();
		if (recipientcountrycode.equalsIgnoreCase("CI")) {// Côte d'Ivoire
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 5);

			if (phoneresults.equalsIgnoreCase("22507")) {

				mno = "orange";

			} else if (phoneresults.equalsIgnoreCase("22505")) {
				mno = "mtn";
			} else if (phoneresults.equalsIgnoreCase("22501")) {
				mno = "moov";

			} else {
				// return error unkmowm mobile prefix
			}

		}

		if (recipientcountrycode.equalsIgnoreCase("GN")) {// guinea
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 5);

			if (phoneresults.equalsIgnoreCase("22462") || phoneresults.equalsIgnoreCase("224610")
					|| phoneresults.equalsIgnoreCase("224611")) {
				mno = "orange";

			} else {
				// 22466
				mno = "mtn";
			}

		}

		if (recipientcountrycode.equalsIgnoreCase("BF")) {// bukina faso
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 5);

			if (phoneresults.equalsIgnoreCase("22606") || phoneresults.equalsIgnoreCase("22607")
					|| phoneresults.equalsIgnoreCase("22654") || phoneresults.equalsIgnoreCase("22655")
					|| phoneresults.equalsIgnoreCase("22657") || phoneresults.equalsIgnoreCase("22664")
					|| phoneresults.equalsIgnoreCase("22665") || phoneresults.equalsIgnoreCase("22666")
					|| phoneresults.equalsIgnoreCase("22667") || phoneresults.equalsIgnoreCase("22674")
					|| phoneresults.equalsIgnoreCase("22675") || phoneresults.equalsIgnoreCase("22676")
					|| phoneresults.equalsIgnoreCase("22677")) {
				mno = "orange";
			} else {
				// 22601, 22602, 22651, 22652, 22653, 22660, 22661, 22662, 22663, 22670, 22671,
				// 22672, 22673
				mno = "mobicash";
			}

		}

		if (recipientcountrycode.equalsIgnoreCase("BJ")) {// benin
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 5);

			if (phoneresults.equalsIgnoreCase("22997") || phoneresults.equalsIgnoreCase("22996")
					|| phoneresults.equalsIgnoreCase("22962") || phoneresults.equalsIgnoreCase("22966")
					|| phoneresults.equalsIgnoreCase("22996") || phoneresults.equalsIgnoreCase("22961")) {
				mno = "mtn";

			} else {
				// 22963, 22964, 22965
				mno = "moov";// flooz
			}

		}

		if (recipientcountrycode.equalsIgnoreCase("ML")) {// MALI
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 5);

			if (phoneresults.equalsIgnoreCase("22370") || phoneresults.equalsIgnoreCase("22371")
					|| phoneresults.equalsIgnoreCase("22372") || phoneresults.equalsIgnoreCase("22373")
					|| phoneresults.equalsIgnoreCase("22374") || phoneresults.equalsIgnoreCase("22375")
					|| phoneresults.equalsIgnoreCase("22376") || phoneresults.equalsIgnoreCase("22377")
					|| phoneresults.equalsIgnoreCase("22378") || phoneresults.equalsIgnoreCase("22379")
					|| phoneresults.equalsIgnoreCase("22380") || phoneresults.equalsIgnoreCase("22381")
					|| phoneresults.equalsIgnoreCase("22382") || phoneresults.equalsIgnoreCase("22383")
					|| phoneresults.equalsIgnoreCase("22384") || phoneresults.equalsIgnoreCase("22390")
					|| phoneresults.equalsIgnoreCase("22391") || phoneresults.equalsIgnoreCase("22392")
					|| phoneresults.equalsIgnoreCase("22393") || phoneresults.equalsIgnoreCase("22394")) {
				mno = "orange";

			} else {
				// 223 60 to 223 69, 223 85 to 223 89, 223 95 to 223 99
				mno = "mobicash";// flooz
			}

		}

		if (recipientcountrycode.equalsIgnoreCase("CM")) {// CAMEROON
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 6);

			if (phoneresults.equalsIgnoreCase("237670") || phoneresults.equalsIgnoreCase("237671")
					|| phoneresults.equalsIgnoreCase("237672") || phoneresults.equalsIgnoreCase("237673")
					|| phoneresults.equalsIgnoreCase("237674") || phoneresults.equalsIgnoreCase("237675")
					|| phoneresults.equalsIgnoreCase("237676") || phoneresults.equalsIgnoreCase("237677")
					|| phoneresults.equalsIgnoreCase("237678") || phoneresults.equalsIgnoreCase("237679")
					|| phoneresults.equalsIgnoreCase("237680") || phoneresults.equalsIgnoreCase("237681")
					|| phoneresults.equalsIgnoreCase("237650") || phoneresults.equalsIgnoreCase("237651")
					|| phoneresults.equalsIgnoreCase("237652") || phoneresults.equalsIgnoreCase("237653")
					|| phoneresults.equalsIgnoreCase("237654")) {
				mno = "mtn";

			} else {
//237 655, 237 656, 237 657, 237 658, 237 6590, 237 6591, 237 6592, 237 6593, 237 6594, 237 69
				mno = "orange";// flooz
			}

		}

		if (recipientcountrycode.equalsIgnoreCase("TG")) {// TOGO
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 5);

			if (phoneresults.equalsIgnoreCase("22870") || phoneresults.equalsIgnoreCase("22890")
					|| phoneresults.equalsIgnoreCase("22891") || phoneresults.equalsIgnoreCase("22892")
					|| phoneresults.equalsIgnoreCase("22893")) {
				mno = "tmoney";
			} else {
				// 228 79, 228 96, 228 97, 228 98, 228 99
				mno = "moov";// flooz
			}

		}

		if (recipientcountrycode.equalsIgnoreCase("SN")) {// SENEGAL
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 6);
			if (phoneresults.equalsIgnoreCase("221771") || phoneresults.equalsIgnoreCase("221772")
					|| phoneresults.equalsIgnoreCase("221773") || phoneresults.equalsIgnoreCase("221774")
					|| phoneresults.equalsIgnoreCase("221775") || phoneresults.equalsIgnoreCase("221776")
					|| phoneresults.equalsIgnoreCase("221777") || phoneresults.equalsIgnoreCase("221778")
					|| phoneresults.equalsIgnoreCase("221779") || phoneresults.equalsIgnoreCase("221781")
					|| phoneresults.equalsIgnoreCase("221782") || phoneresults.equalsIgnoreCase("221783")
					|| phoneresults.equalsIgnoreCase("221784") || phoneresults.equalsIgnoreCase("221785")
					|| phoneresults.equalsIgnoreCase("221786") || phoneresults.equalsIgnoreCase("221787")
					|| phoneresults.equalsIgnoreCase("221788") || phoneresults.equalsIgnoreCase("221789")) {
				mno = "orange";
			} else if (phoneresults.equalsIgnoreCase("221761") || phoneresults.equalsIgnoreCase("221762")
					|| phoneresults.equalsIgnoreCase("221763") || phoneresults.equalsIgnoreCase("221764")
					|| phoneresults.equalsIgnoreCase("221765") || phoneresults.equalsIgnoreCase("221766")
					|| phoneresults.equalsIgnoreCase("221767") || phoneresults.equalsIgnoreCase("221768")
					|| phoneresults.equalsIgnoreCase("221769")) {

				mno = "free";

			} else {
				// 221 701 to 221 709
				mno = "emoney";

			}

		}

		TOKEN = token;

		// ###############################################################
		// Fetch the initiator Password
		// ##############################################################
		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################

		creditrequest = new JsonObject();
		destinationobject = new JsonObject();

		creditrequest.addProperty("reference", transactioinid);
		creditrequest.addProperty("amount", Integer.valueOf(amountstring));
		creditrequest.addProperty("currency", recipientcurrencycode);//
		creditrequest.addProperty("description", "Disbursement");

		destinationobject.addProperty("type", "mobile_money");
		destinationobject.addProperty("country", recipientcountrycode);
		destinationobject.addProperty("msisdn", recipientmobile);
		destinationobject.addProperty("provider", mno);

		creditrequest.add("destination", destinationobject);
		
		


		/**
		 * if (vendorfields != null) { creditrequest.add("vendor_uniquefields",
		 * vendorfields); }
		 **/

		// assign the remit url from properties.config
		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");
		CLIENT_URL = remiturlss;

		jsonData = g.toJson(creditrequest);
		
		//return "VEVE EUGENE"+jsonData+" TOKEN "+TOKEN+" merchantid "+merchantid;


		// postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonData);
		postMinusThread = new PostWithIgnoreSSLHub2(CLIENT_URL, jsonData, TOKEN, merchantid);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {
			// check for successfull

			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			statuscode = roots.getAsJsonObject().get("status").getAsString();
			referencenumber = roots.getAsJsonObject().get("id").getAsString();
			
			System.out.println("THE RESPONSE WE ARE GETTING1"+roots);


		} catch (Exception e) {
			
			System.out.println("THE RESPONSE WE ARE GETTING "+roots);

			// This transaction might have failed check error log
			responserequest = roots.getAsJsonObject();
			
			//{"statusCode":400,"message":["amount must be an integer number","msisdn is invalid"],"error":"Bad Request"}

			if (responserequest.has("code")) {
				statuscode = "00029";
				statusdescription = roots.getAsJsonObject().get("code").getAsString();
				// referencenumber =
				// responserequest.getAsJsonObject().get("requestId").getAsString();
			} else if (responserequest.has("statusCode")) {
				statuscode = "00029";
				statusdescription = roots.getAsJsonObject().get("error").getAsString();

			} else {

				referencenumber = "investigateTransaction";
				statuscode = "00032";
				statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
			}
			// ================================================
			// Missing fields in response from receiver system
			// ================================================

		}

		logger.error(".....................................................");
		logger.error("HUB2 DISBURSEMENT REQUEST FROM BRIDGE :" + jsonData + "\n");
		logger.error("HUB2 DISBURSEMENT RESPONSE:" + roots + "\n");
		logger.error(".....................................................");
		
		//return "weweweeeeeeeeeeeeeeeeeeeee";
		
		if (statuscode.equalsIgnoreCase("pending") || statuscode.equalsIgnoreCase("created")) {
			statuscode = "S001";
			statusdescription = "CREDIT_IN_PROGRESS";

		} else if (statuscode.equalsIgnoreCase("successful")) {
			statuscode = "S000";
			statusdescription = "SUCCESS";

		} else {
			statuscode = "00029";
			statusdescription = "REJECTED_TRANSACTION";
		}

		// =====================================================================
		// construct object to return to sender system
		// =====================================================================
		responsetoreceiver = new JsonObject();
		responsetoreceiver.addProperty("am_referenceid", referencenumber);
		responsetoreceiver.addProperty("status_code", statuscode);
		responsetoreceiver.addProperty("status_description", statusdescription);

		return responsetoreceiver.toString();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.mno.servlet.api.bridge.equitybank;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostThreadUniversalEquity;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.commons.codec.binary.Base64;

public class GetAuth extends HttpServlet {

	private String CLIENT_URL = "", TEXT_PATH = "";
	private PostThreadUniversalEquity postMinusThread;
	Properties prop = new Properties();
	OutputStream output = null;
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		// CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(authentication(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String authentication(HttpServletRequest request) throws IOException {
		// Account account = null;

		// joined json string
		String join = "";
		String token = "";
		JsonElement root = null, roots = null;
		JsonObject queryrequest = null,authrequest=null;
		String responseobject = "", results2 = "", jsonResult = "", jsonRequest = "";

		// These represent parameters received over the network
		String username = "", sessionid = "", receiverquery = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		JsonArray jsonarray = new JsonArray();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			sessionid = root.getAsJsonObject().get("password").getAsString();
			receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();

		} catch (Exception e) {

			// expected.put("command_status",
			// APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			expected.put("command_status", "COMMANDSTATUS_INVALID_PARAMETERS");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		authrequest = new JsonObject();
		
		authrequest.addProperty("username", username);
		authrequest.addProperty("password", sessionid);

		
		jsonRequest = g.toJson(authrequest);


		TEXT_PATH = PropertiesConfig.getConfigValue("EQUITYTEXT_PATH");
		CLIENT_URL = receiverquery;
		// CLIENT_URL =
		// "https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials";
		postMinusThread = new PostThreadUniversalEquity(CLIENT_URL, jsonRequest,"token");

		try {

			// capture the switch respoinse.
			
			responseobject = postMinusThread.runurl();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);
			
			// exctract a specific json element from the object(status_code)
			token = roots.getAsJsonObject().get("accessToken").getAsString();

			expected.put("command_status", "SUCCESS_EQUITY_SESSION");
			jsonResult = g.toJson(expected);

			try {
				FileOutputStream writer = new FileOutputStream(TEXT_PATH);
				writer.write(("").getBytes());
				writer.close();

				PrintWriter fileWriter = new PrintWriter(new FileOutputStream(TEXT_PATH, true));
				fileWriter.println(token);
				// out.println("file saved");
				fileWriter.close();
			} catch (IOException io) {
				io.printStackTrace();
			} finally {
				if (output != null) {
					try {
						output.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			}

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("command_status", "COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS");
			jsonResult = g.toJson(expected);
			

			// return jsonResult;

		}

		logger.error(".....................................................");
		logger.error("EQUITY QUERY GET AUTH FROM BRIDGE :" + jsonRequest + "\n");
		logger.error("EQUITYQUERY GET AUTH  RESPONSE:" + jsonResult + "\n");
		logger.error(".....................................................");

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

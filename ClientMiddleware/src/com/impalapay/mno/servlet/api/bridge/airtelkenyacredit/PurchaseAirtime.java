package com.impalapay.mno.servlet.api.bridge.airtelkenyacredit;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.ValidatePhoneNumber;
import com.impalapay.airtel.util.net.PostThreadUniversalAirtel;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;

/**
 * Responsible for sending speciakl session id on the same call.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Aprl 14, 2015
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class PurchaseAirtime extends HttpServlet {

	// additions for worldremit

	private Logger logger;
	private Cache accountsCache;
	private String CLIENT_URL = "";
	private ValidatePhoneNumber mobileresults;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(getSessionId(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * Receives an HTTP request and processes it further. In a successful case, a
	 * Transaction Id and status are returned.
	 * 
	 * @param request
	 * @return String
	 * @throws IOException
	 */
	private String getSessionId(HttpServletRequest request) throws IOException {
		Account account = null;

		String apiusername = "", apipassword = "", jsonResult = "", jsonData = "", fetchloginkey = "", CLIENT_URL = "",
				switchresponse = "", statusdescription = "", responseobject = "", deviceserial = "", sourcemsisdn = "",
				recipientmobile = "", soapRequest = "", replacedStr1 = "", replacedStr2 = "", remiturlss = "",
				sendername = "", transactioinid = "", sourcecountrycode = "", recipientcurrencycode = "",
				recipientcountrycode = "", amountstring = "", username = "", returnedtransactionid = "",success = "S000", fail = "00032",statuscode = "",
				modifiedsourcemobileno="",modifiedrecipientmobileno="";
		
		String type = "", PIN = "", LOGINID = "", PASSWORD = "", EXTNWCODE = "";
		double amount = 0;
		int finalamount = 0, responseCode = 0;

		String join = "";
		JsonElement root = null;
		JsonObject creditrequest = null, root2 = null;
		JSONObject data = null, env = null, body = null, topup = null, topupResponse = null, topupSummary = null;
		// PostMuni postwithignoressl;
		PostThreadUniversalAirtel postwithignoressl;

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// #######################################################################
		// instantiate the JSon
		// #######################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			sourcecountrycode = root.getAsJsonObject().get("sourcecountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("recipientcurrencycode").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("recipientcountrycode").getAsString();

			sendername = root.getAsJsonObject().get("Sender_Name").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();

			sourcemsisdn = root.getAsJsonObject().get("source_msisdn").getAsString();

			recipientmobile = root.getAsJsonObject().get("beneficiary_msisdn").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {
			
			expected.put("am_referenceid", transactioinid);
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(sourcecountrycode)
				|| StringUtils.isBlank(recipientcurrencycode) || StringUtils.isBlank(sendername)
				|| StringUtils.isBlank(amountstring) || StringUtils.isBlank(remiturlss)
				|| StringUtils.isBlank(recipientcountrycode)) {
			
			expected.put("am_referenceid", transactioinid);
			expected.put("status_code", "00032");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

		}

		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("am_referenceid", transactioinid);
			expected.put("status_code", "00032");
			expected.put("status_description", "unauthorised user");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// finalamount = (int) amount;

		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");
		CLIENT_URL = remiturlss;
		//CLIENT_URL ="https://41.223.58.202:8093/pretups/C2SReceiver?REQUEST_GATEWAY_CODE=IMPL&REQUEST_GATEWAY_TYPE=EXTGW&LOGIN=pretups&PASSWORD=d3f14d78c0f0f39e587604ba5ab2c095&SOURCE_TYPE=EXTGW&SERVICE_PORT=190";
		
		type = PropertiesConfig.getConfigValue("EXRCTRFREQ");
		PIN = PropertiesConfig.getConfigValue("PIN");
		EXTNWCODE ="KE";
		
		// ##################################################################
		// modify the mobile number from any point
		// ##################################################################
		mobileresults = new ValidatePhoneNumber();
		
		try {
			// check the group which the customer belongs to.(in future)
			jsonResult = mobileresults.ValidminuscodeNumber("254", recipientmobile);
			root = new JsonParser().parse(jsonResult);
			statuscode = root.getAsJsonObject().get("status_code").getAsString();
		} catch (Exception e) {
			statuscode = fail;

		}

		if (!statuscode.equalsIgnoreCase(success)) {
			expected.put("am_referenceid", transactioinid);
			expected.put("status_code", "00032");
			expected.put("status_description", "INVALID_RECIPIENT_MSISDN_FORMAT");

			jsonResult = g.toJson(expected);
			return jsonResult;
		}
		
		modifiedrecipientmobileno = root.getAsJsonObject().get("number").getAsString();
		
		
		//SAME FOR RECIPIENT MSISDN
		try {
			// check the group which the customer belongs to.(in future)
			jsonResult = mobileresults.ValidminuscodeNumber("254", sourcemsisdn);
			root = new JsonParser().parse(jsonResult);
			statuscode = root.getAsJsonObject().get("status_code").getAsString();
		} catch (Exception e) {
			statuscode = fail;

		}

		if (!statuscode.equalsIgnoreCase(success)) {
			expected.put("am_referenceid", transactioinid);
			expected.put("status_code", "00032");
			expected.put("status_description", "INVALID_SOURCE_MSISDN_FORMAT");

			jsonResult = g.toJson(expected);
			return jsonResult;
		}
		
		modifiedsourcemobileno = root.getAsJsonObject().get("number").getAsString();

		
		sourcemsisdn = modifiedsourcemobileno;
		recipientmobile = modifiedrecipientmobileno;

		// Create todays Date
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
/**
		soapRequest = "<?xml version=\"1.0\"?>\n"

				+ "<COMMAND>\n" + "<TYPE>" + type + "</TYPE>\n" + "<DATE>" + dateFormat.format(date) + "</DATE>\n"
				+ "<EXTNWCODE>" + EXTNWCODE + "</EXTNWCODE>\n" + "<MSISDN>" + sourcemsisdn + "</MSISDN>\n" + "<PIN>"
				+ PIN + "</PIN>\n" + "<LOGINID>" + LOGINID + "</LOGINID>\n" + "<PASSWORD>" + PASSWORD + "</PASSWORD>\n"
				+ "<EXTCODE>" + sourcemsisdn + "</EXTCODE>\n" + "<EXTREFNUM>" + transactioinid + "</EXTREFNUM>\n"
				+ "<MSISDN2>" + recipientmobile + "</MSISDN2>\n" + "<AMOUNT>" + amountstring + "</AMOUNT>\n"
				+ "<LANGUAGE1>" + 1 + "</LANGUAGE1>\n" + "<LANGUAGE2>" + 1 + "</LANGUAGE2>\n" + "<SELECTOR>" + 1
				+ "</SELECTOR>\n" + "</COMMAND>\n";

		logger.error(" Soap Request sent to Airtel AirtimeExternalPoint" + soapRequest + "\n");**/

		//fetchloginkey = CLIENT_URL;
		
		
		creditrequest = new JsonObject();

		creditrequest.addProperty("currentdate", dateFormat.format(date));
		creditrequest.addProperty("source_msisdn", sourcemsisdn);
		creditrequest.addProperty("amount", amountstring);
		creditrequest.addProperty("recipient_msisdn", recipientmobile);
		creditrequest.addProperty("transaction_id", transactioinid);
		
		jsonData = g.toJson(creditrequest);

		if (StringUtils.isEmpty(CLIENT_URL)) {
			switchresponse = "00032";
			statusdescription = "UNABLE TO SEND TO AIRTEL  SYSTEM(URI EMPTY)";
			expected.put("am_referenceid", transactioinid);
			expected.put("status_code", switchresponse);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		try {


			//postwithignoressl = new PostThreadUniversalAirtel(CLIENT_URL, soapRequest);
			postwithignoressl = new PostThreadUniversalAirtel(CLIENT_URL, jsonData);


			responseobject = postwithignoressl.runurl();

			System.out.println("This is the response " + responseobject);
			logger.error(" Soap Response received from Airtel AirtimeExternalPoint" + responseobject.toString() + "\n");

		} catch (Exception e) {
			logger.error("error during posting to Airtel Airtimegateway TIMEOUT:" + e);
			switchresponse = "00032";
			statusdescription = "TIME_OUT_ON_AIRTEL AIRTIME_SYSTEM";
			
			expected.put("am_referenceid", transactioinid);
			expected.put("status_code", switchresponse);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		try {

			data = XML.toJSONObject(responseobject);

			env = data.getJSONObject("COMMAND");
			statusdescription = env.getString("MESSAGE");
			responseCode = env.getInt("TXNSTATUS");
		} catch (Exception e) {
			// TODO: handle exception
			switchresponse = "00032";
			expected.put("am_referenceid", transactioinid);
			statusdescription = "INTERNAL_ERROR WHEN PARSING_RESPONSE "+CLIENT_URL;
			expected.put("status_code", switchresponse);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		switchresponse = Integer.toString(responseCode);

		if (switchresponse.equalsIgnoreCase("200")) {
			// fetch the transaction id

			switchresponse = "S000";
			returnedtransactionid = String.valueOf(env.getString("TXNID"));
		} else {
			returnedtransactionid = transactioinid;
			switchresponse = "00029";
		}

		expected.put("am_referenceid", returnedtransactionid);
		expected.put("status_code", switchresponse);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

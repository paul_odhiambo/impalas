package com.impalapay.mno.servlet.api.bridge.wari;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLWari;

import net.sf.ehcache.CacheManager;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class ConfirmRequest extends HttpServlet {

	private String CLIENT_URL = "";
	private PostWithIgnoreSSLWari postignoresslwari;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		String responseobject = "";
		JsonObject requestData = null, confirmtransaction = null;

		// These represent parameters received over the network
		String requestid = "", amount = "", currency = "", transactionid = "", meanstype = "", meansvalue = "",
				jsonResult = "", merchantid = "", url = "", clientid = "", sessionkey = "", statusmessage = "",
				statuscode = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			requestid = root.getAsJsonObject().get("requestID").getAsString();
			amount = root.getAsJsonObject().get("amount").getAsString();
			currency = root.getAsJsonObject().get("currency").getAsString();
			transactionid = root.getAsJsonObject().get("transID").getAsString();
			meanstype = root.getAsJsonObject().get("meansType").getAsString();
			meansvalue = root.getAsJsonObject().get("meansValue").getAsString();
			merchantid = root.getAsJsonObject().get("merchantID").getAsString();
			url = root.getAsJsonObject().get("confirmurl").getAsString();
			clientid = root.getAsJsonObject().get("clieintID").getAsString();
			sessionkey = root.getAsJsonObject().get("SessionID").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		requestData = new JsonObject();

		requestData.addProperty("requestID", transactionid);
		requestData.addProperty("amount", amount);
		requestData.addProperty("currency", currency);
		requestData.addProperty("merchantID", merchantid);
		requestData.addProperty("cardnumber", "5058880100029011");
		requestData.addProperty("expiredate", "1707");
		requestData.addProperty("cvv", "353");
		requestData.addProperty("voucherProvider", "allvoucher");
		requestData.addProperty("callBackUrl", "");
		requestData.addProperty("transID", transactionid);
		requestData.addProperty("cardOwner", "Kelvin Muli");
		requestData.addProperty("Lastname", "Muli");
		requestData.addProperty("codeNeo", "");
		requestData.addProperty("Firstname", "Kelvin");
		requestData.addProperty("uid", "W0110000606");
		requestData.addProperty("phoneNumber", "254733190378");
		requestData.addProperty("clientIp", "");
		requestData.addProperty("codeVoucher", "");
		requestData.addProperty("email", "kmuli@impalapay.com");
		requestData.addProperty("desc", "Send cash");

		// confirm Transaction.
		confirmtransaction = new JsonObject();

		confirmtransaction.add("requestData", requestData);
		confirmtransaction.addProperty("meansType", meanstype);
		confirmtransaction.addProperty("meansValue", meanstype);
		confirmtransaction.addProperty("lang", "en");
		confirmtransaction.addProperty("source", meanstype);

		jsonResult = g.toJson(confirmtransaction);

		CLIENT_URL = url;
		postignoresslwari = new PostWithIgnoreSSLWari(CLIENT_URL, jsonResult, sessionkey, clientid);

		try {

			responseobject = postignoresslwari.doPost();

			roots = new JsonParser().parse(responseobject);

			statusmessage = roots.getAsJsonObject().get("statusMessage").getAsString();
			statuscode = roots.getAsJsonObject().get("statusCode").getAsString();

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INTERNAL_ERROR + "_CONFIRMPAYMENT");

			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// capture session and return proper response if successfull.
		if (statuscode.equalsIgnoreCase("000")) {
			statuscode = "S000";
		} else {
			statuscode = "00029";
		}
		expected.put("status_code", statuscode);
		expected.put("status_description", statusmessage);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

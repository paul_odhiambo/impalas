package com.impalapay.mno.servlet.api.bridge.pesachoice;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import java.sql.Timestamp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLUGmart;

public class RouteMobileCollection extends HttpServlet {

	// private PostMinusThread postMinusThread;
	private PostWithIgnoreSSLUGmart postMinusThread;
	private Cache accountsCache;
	private String CLIENT_URL = "", TOKEN = "", ACCOUNTCODE = "", APPLLICATION = "", TEXT_PATH = "", CALLBACK_URI = "";
	private Logger logger;
	private SimpleDateFormat sdf;
	private Timestamp timestamp;
	private SecretKeySpec key;
	private byte[] bytesEncoded, bytes;
	private Mac mac;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);
		sdf = new SimpleDateFormat("YYYY-MM-dd");
		timestamp = new Timestamp(System.currentTimeMillis());
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonObject root2 = null, creditrequest = null, data = null, paymentServiceInfo = null;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", sourcecountrycode = "",
				email = "", recipientcurrencycode = "", recipientcountrycode = "", sourcemsisdn = "",
				recipientmobile = "", sendername = "", amountstring = "", remiturlss = "", responseobject = "",
				statuscode = "", statusdescription = "", referencenumber = "", token = "", debitreferenceno = "",
				networkname = "", jsonResult = "", transactionuuid = "", mytimestamp = "", encryptedPassword = "",
				actionType = "THIRD_PARTY_PAYMENT", products = "Event", companyId = "Impalapay";
		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("debitcountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("debitcurrencycode").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			sendername = root.getAsJsonObject().get("debitname").getAsString();

			recipientmobile = root.getAsJsonObject().get("debitaccount").getAsString();

			debitreferenceno = root.getAsJsonObject().get("debitreferencenumber").getAsString();

			networkname = root.getAsJsonObject().get("networkname").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(recipientcurrencycode)
				|| StringUtils.isBlank(sendername) || StringUtils.isBlank(amountstring)
				|| StringUtils.isBlank(remiturlss) || StringUtils.isBlank(recipientcountrycode)) {

			expected.put("status_code", "00032");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

		}

		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("status_code", "00032");
			expected.put("status_description", "unauthorised user");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		email = apiusername;

		mytimestamp = sdf.format(timestamp);
		bytesEncoded = Base64.encodeBase64(email.getBytes());

		key = new SecretKeySpec((apipassword).getBytes("UTF-8"), "HmacSHA512");
		try {
			mac = Mac.getInstance("HmacSHA512");
			mac.init(key);
		} catch (Exception e) {
			expected.put("command_status", "HASHING_ERROR_CONTACT_ADMIN");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		bytes = mac.doFinal(bytesEncoded);

		encryptedPassword = Hex.encodeHexString(bytes);

		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################

		paymentServiceInfo = new JsonObject();

		paymentServiceInfo.addProperty("price", amountstring);
		paymentServiceInfo.addProperty("customerPhoneNumber", recipientmobile);
		paymentServiceInfo.addProperty("customerName", sendername);

		creditrequest = new JsonObject();

		creditrequest.addProperty("companyId", email);
		creditrequest.addProperty("actionType", actionType);

		creditrequest.addProperty("products", "money transfer");

		creditrequest.addProperty("password", encryptedPassword);
		creditrequest.addProperty("country", sourcecountrycode);

		creditrequest.addProperty("description", "payment");
		creditrequest.addProperty("price", amountstring);

		creditrequest.addProperty("contactPhoneNumber", sourcemsisdn);

		creditrequest.add("paymentServiceInfo", paymentServiceInfo);

		/**
		 * if (vendorfields != null) { creditrequest.add("vendor_uniquefields",
		 * vendorfields); }
		 **/

		// assign the remit url from properties.config
		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");

		CLIENT_URL = remiturlss;

		String jsonData = g.toJson(creditrequest);

		// postMinusThread = new PostMinusThread(CLIENT_URL, jsonData);
		postMinusThread = new PostWithIgnoreSSLUGmart(CLIENT_URL, jsonData, TOKEN);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {

			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			statuscode = roots.getAsJsonObject().get("transactionStatus").getAsString();

			statusdescription = roots.getAsJsonObject().get("transactionStatus").getAsString();

			transactionuuid = roots.getAsJsonObject().get("transactionId").getAsString();

			// referencenumber =
			// roots.getAsJsonObject().get("am_referenceid").getAsString();

			// System.out.println("The file Path " + TEXT_PATH + " The token " + token);

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			referencenumber = "investigateTransaction";
			statuscode = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
		}

		logger.error(".....................................................");
		logger.error("PESACHOICE DEBIT REQUEST FROM BRIDGE :" + jsonData + "\n");
		logger.error("PESACHOICE DEBIT RESPONSE:" + roots + "\n");
		logger.error(".....................................................");

		// map ACCEPTED FOR PROCESSING
		if (statuscode.equalsIgnoreCase("IN_PROGRESS")) {
			statuscode = "S001";
		} else if (statuscode.equalsIgnoreCase("SUCCESS")) {

			statuscode = "S000";

		} else {
			statuscode = "00029";

			// System.out.println(creditrequest);
		}

		String success = "S001";

		if (statuscode.equalsIgnoreCase(success)) {

			expected.put("am_referenceid", transactionuuid);
			expected.put("am_timestamp", username);
			expected.put("status_code", "S001");
			expected.put("status_description", "CREDIT_IN_PROGRESS");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}
		expected.put("am_referenceid", transactioinid);
		expected.put("am_timestamp", username);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	// {
	// "code": 200,
	// "status": "PENDING",
	// "message": "Transaction Initiated",
	// "data": {
	// "transaction_id": "1515729895"
	// }
	// }

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

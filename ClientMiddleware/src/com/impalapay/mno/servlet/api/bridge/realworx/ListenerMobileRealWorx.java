package com.impalapay.mno.servlet.api.bridge.realworx;

import com.google.gson.*;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.accountmgmt.inprogressbalance.InProgressBalanceDAO;
import com.impalapay.airtel.persistence.transaction.TransactionDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.mno.servlet.api.remit.AutoTransactionDispatcher;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListenerMobileRealWorx extends HttpServlet {

	// private PostMinusThread postMinusThread;
	private Cache transactionStatusCache;
	private TransactionStatus ts, statusoftransaction;
	private TransactionDAO transactionDAO;
	private Transaction transactionByStatus;
	private InProgressBalanceDAO inprogressbalanaceDAO;

	private TransactionForexDAO transactionforexDAO;
	private HashMap<String, String> transactionStatusHash = new HashMap<>();
	private String TRANSACTIONSTATUS_UUID = "5c9b8b0b-a035-4a07-bfd8-eccd4f039d53";
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		transactionDAO = TransactionDAO.getInstance();
		transactionforexDAO = TransactionForexDAO.getinstance();
		inprogressbalanaceDAO = InProgressBalanceDAO.getInstance();
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {

		// joined json string
		String join = "";
		JsonElement root = null;
		JsonObject root2 = null, data = null;

		String statuscode = "", statusdescription = "", message = "", transactioinid = "", transactionuuid = "",
				jsonResult = "", statusuuid = "", success = "S000", failed = "00029";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		logger.error("RelWorx Lister Request :" + join);

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			//statuscode = root.getAsJsonObject().get("code").getAsString();

			statusdescription = root.getAsJsonObject().get("status").getAsString();

			message = root.getAsJsonObject().get("message").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(statusdescription) || StringUtils.isBlank(message)) {

			expected.put("status_code", "00032");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}




			transactioinid = root.getAsJsonObject().get("internal_reference").getAsString();

			// check from the database if this transaction exist and status is still
			// inprogress
			ts = new TransactionStatus();
			ts.setUuid(TRANSACTIONSTATUS_UUID);
			
	

			transactionByStatus = transactionDAO.getTransactionstatusbyreceiverreference(transactioinid, ts);

			if (transactionByStatus == null) {

				expected.put("status_code", "00032");
				expected.put("status_description", "receiver transaction id doesnt exist in system table");
				jsonResult = g.toJson(expected);

				logger.error("Relworx Response"+jsonResult);

				return jsonResult;

			}
			
			transactionuuid = transactionByStatus.getUuid();
			
			TransactionForexrate transactionforex = transactionforexDAO.getTransactionForexsUuid(transactionuuid);
			if ((transactionforex == null)) {
				expected.put("command_status",
						"We have a problem at autoquery under fetching transactionforex rate transaction");
				jsonResult = g.toJson(expected);
				return jsonResult;
			}

			

		Element element;

		List keys;

		// ======================================================
		// Populate with the mapping of Transaction Statuses.
		// The key is a UUID of the status
		// ======================================================

		TransactionStatus status1;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status1.getStatus(), status1.getUuid());
		}

		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################
		// map ACCEPTED FOR PROCESSING
		if (statusdescription.equalsIgnoreCase("SUCCESS")) {
			statuscode = "S000";
		} else if (statusdescription.equalsIgnoreCase("FAILED")) {
			statuscode = "00029";
		} else {

			statuscode = "S001";

		}

		
		TransactionStatus statusoftransaction = new TransactionStatus();
		
		statusuuid = transactionStatusHash.get(statuscode);

		if (statuscode.equalsIgnoreCase(success) || statuscode.equalsIgnoreCase(failed)) {

			if (statuscode.equals(success)) {

				new AutoTransactionDispatcher(transactionByStatus, transactionforex).start();

			} else {
				// delete the transaction forex from db
				// transactionforexDAO.deleteTransactionForex(transactionid);
				// return balances owed
				// inprogressbalanaceDAO.removeBalanceHoldFail(transactionid);
				// update that the system approved this balance
				if (transactionforexDAO.deleteTransactionForex(transactionuuid)
						&& inprogressbalanaceDAO.removeBalanceHoldFail(transactionuuid)) {

				} else {
					System.out.println("This transaction failed to update succesfully " + transactionuuid);
				}

			}

			statusoftransaction.setUuid(statusuuid);
			transactionDAO.updateTransactionStatus(transactionuuid, statusoftransaction);

			expected.put("transaction_id", "");
			expected.put("status_code", statuscode);
			expected.put("command_status", APIConstants.COMMANDSTATUS_OK);
			expected.put("remit_status", statusdescription);

			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		expected.put("am_referenceid", transactioinid);
		expected.put("am_timestamp", message);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}


	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}
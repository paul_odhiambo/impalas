package com.impalapay.mno.servlet.api.bridge.flocash;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLFloCash;

public class RouteVisaCollectionPCI extends HttpServlet {

	// private PostMinusThread postMinusThread;
	private PostWithIgnoreSSLFloCash postMinusThread;
	private Cache accountsCache;
	private String CLIENT_URL = "", TOKEN = "", ACCOUNTCODE = "", APPLLICATION = "", TEXT_PATH = "", CALLBACK_URI = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null, rootsresponse = null;
		JsonObject root2 = null, creditrequest = null, vendoruniqueobject = null, returnserver = null, data = null,
				order = null, merchant = null, payer = null, payoptionorder = null, payoption = null,
				payoptionrequest = null, payoptionorder2 = null, redirectobject = null, cardinfo = null;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", sourcecountrycode = "",
				recipientcurrencycode = "", recipientcountrycode = "", sourcemsisdn = "", recipientmobile = "",
				sendername = "", amountstring = "", remiturlss = "", responseobject = "", statuscode = "",
				statusdescription = "", referencenumber = "", token = "", debitreferenceno = "", payment_url = "",
				networkname = "", jsonResult = "", jsonData = "", returnedtransactionid = "";

		String debitcontact = "", cardtype = "", cardcvv = "", cardexpmonth = "", cardexpyear = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("debitcountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("debitcurrencycode").getAsString();

			debitcontact = root.getAsJsonObject().get("debitcontact").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			sendername = root.getAsJsonObject().get("debitname").getAsString();

			recipientmobile = root.getAsJsonObject().get("debitaccount").getAsString();

			debitreferenceno = root.getAsJsonObject().get("debitreferencenumber").getAsString();

			networkname = root.getAsJsonObject().get("networkname").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();
			cardtype = root.getAsJsonObject().get("card_type").getAsString();
			cardcvv = root.getAsJsonObject().get("card_cvv").getAsString();
			cardexpmonth = root.getAsJsonObject().get("card_expmonth").getAsString();
			cardexpyear = root.getAsJsonObject().get("card_expyear").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(recipientcurrencycode)
				|| StringUtils.isBlank(sendername) || StringUtils.isBlank(amountstring)
				|| StringUtils.isBlank(remiturlss) || StringUtils.isBlank(recipientcountrycode)) {

			expected.put("status_code", "00032");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

		}

		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("status_code", "00032");
			expected.put("status_description", "unauthorised user");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################
		order = new JsonObject();
		merchant = new JsonObject();
		payer = new JsonObject();
		payoption = new JsonObject();
		cardinfo = new JsonObject();

		payoptionrequest = new JsonObject();
		payoptionorder2 = new JsonObject();

		// cardrequest= new JsonObject();
		creditrequest = new JsonObject();

		order.addProperty("amount", Integer.parseInt(amountstring));
		order.addProperty("orderId", referencenumber);
		order.addProperty("item_name", "impalapaydebit");
		order.addProperty("item_price", amountstring);
		order.addProperty("quantity", "1");
		order.addProperty("currency", recipientcurrencycode);

		merchant.addProperty("merchantAccount", apiusername);

		payer.addProperty("country", recipientcountrycode);
		payer.addProperty("firstName", sendername);
		payer.addProperty("lastName", sendername);
		payer.addProperty("mobile", debitcontact);
		payer.addProperty("email", sendername + "@gmail.com");

		payoption.addProperty("id", "123");

		cardinfo.addProperty("cardHolder", sendername);
		cardinfo.addProperty("cardNumber", recipientmobile);
		cardinfo.addProperty("expireMonth", cardexpmonth);
		cardinfo.addProperty("expireYear", cardexpyear);
		cardinfo.addProperty("cvv", cardcvv);

		creditrequest.add("order", order);
		creditrequest.add("merchant", merchant);
		creditrequest.add("payer", payer);
		creditrequest.add("payOption", payoption);
		creditrequest.add("cardInfo", cardinfo);

		/**
		 * if (vendorfields != null) { creditrequest.add("vendor_uniquefields",
		 * vendorfields); }
		 **/

		// assign the remit url from properties.config
		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");

		CLIENT_URL = remiturlss;
		TOKEN = apipassword;
		jsonData = g.toJson(creditrequest);

		// postMinusThread = new PostMinusThread(CLIENT_URL, jsonData);
		postMinusThread = new PostWithIgnoreSSLFloCash(CLIENT_URL, jsonData, TOKEN);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {

			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			payoptionorder = roots.getAsJsonObject().get("order").getAsJsonObject();

			statuscode = payoptionorder.getAsJsonObject().get("status").getAsString();

			returnedtransactionid = payoptionorder.getAsJsonObject().get("traceNumber").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			referencenumber = "investigateTransaction";
			statuscode = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
		}

		// map ACCEPTED FOR PROCESSING
		if (statuscode.equalsIgnoreCase("0001")) {
			statuscode = "S001";
		}

		String success = "S001";

		if (statuscode.equalsIgnoreCase(success)) {

			expected.put("am_referenceid", returnedtransactionid);
			expected.put("am_timestamp", username);
			expected.put("status_code", "S001");
			expected.put("status_description", "CREDIT_IN_PROGRESS");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}
		expected.put("am_referenceid", transactioinid);
		expected.put("am_timestamp", username);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

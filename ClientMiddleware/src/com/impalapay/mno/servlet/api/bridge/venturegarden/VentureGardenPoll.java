package com.impalapay.mno.servlet.api.bridge.venturegarden;

import java.util.HashMap;
import com.google.gson.Gson;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostThreadUniversalUpdated;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This Quartz job is used to check for Session Ids in the SessionLog database
 * table for expiry. Session Ids are expired after a fixed duration of time.
 * <p>
 * Copyright (c) Tawi Commercial Services Ltd., Sep 23, 2014
 * 
 * @author <a href="mailto:michael@tawi.mobi">Michael Wakahe</a>
 * 
 */

public class VentureGardenPoll implements Job {

	//private PostWithIgnoreSSL postMinusThread;
	private PostThreadUniversalUpdated postMinusThread;

	private String CLIENT_URL = "";
	private String responseobject = "", receiverurl = "", username = "", password = "";
	private Logger logger;
	private HashMap<String, String> expected = new HashMap<>();

	/**
	 * Empty constructor for job initialization
	 * <p>
	 * Quartz requires a public empty constructor so that the scheduler can
	 * instantiate the class whenever it needs.
	 */
	public VentureGardenPoll() {
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * <p>
	 * Called by the <code>{@link org.quartz.Scheduler}</code> when a
	 * <code>{@link org.quartz.Trigger}</code> fires that is associated with the
	 * <code>Job</code>.
	 * </p>
	 * 
	 * @throws JobExecutionException
	 *             if there is an exception while executing the job.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Gson g = new Gson();

		receiverurl = PropertiesConfig.getConfigValue("VENTUREGARDENAUTHURL");
		username = PropertiesConfig.getConfigValue("VGN_USERNAME");
		password = PropertiesConfig.getConfigValue("VGN_PASSWORD");

		CLIENT_URL = PropertiesConfig.getConfigValue("VGNINTERNAL_AUTHURL");

		expected.put("username", username);
		expected.put("password", password);
		expected.put("receiverqueryurl", receiverurl);

		String jsonforxData = g.toJson(expected);

		logger.error(".....................................................");
		logger.error(" START AUTOMATIC VGN SESSIONREQUEST" + "\n");
		logger.error(" VGN SESSION REQUEST THE URL "+ CLIENT_URL+ "\n");
		logger.error(".....................................................");

		try {

			//postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonforxData);
			postMinusThread = new PostThreadUniversalUpdated(CLIENT_URL, jsonforxData);
			

			// capture the switch respoinse.
			//responseobject = postMinusThread.doPost();
			responseobject = postMinusThread.runurl();

			System.out.println("VentureGarden Session request " + jsonforxData + "\n" + " VGN session response "
					+ responseobject + " " + "\n");
		} catch (Exception e) {
			logger.error(".....................................................");
			logger.error(" ERROR OCCURED DURING AUTOMATIC VGN SESSIONREQUEST" + "\n");
			logger.error(responseobject);
			logger.error(".....................................................");
			System.out.println(jsonforxData + " VGN session Request error" + responseobject);
		}

		logger.error(".....................................................");
		logger.error(" RESPONSE FOR AUTOMATIC VGN SESSIONREQUEST" + "\n");
		logger.error(responseobject);
		logger.error(".....................................................");
	}

}

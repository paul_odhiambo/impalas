package com.impalapay.mno.servlet.api.bridge.coopcard;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.servlet.api.APIConstants;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;

import net.sf.ehcache.CacheManager;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryStatus extends HttpServlet {

	private String CLIENT_URL = "";
	private PostWithIgnoreSSL postMinusThread;
	private Logger logger;


	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());


	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		String responseobject = "";

		// These represent parameters received over the network
		String username = "", sessionid = "", referencenumber = "", receiveruuid = "", receiverquery = "",
				statuscode = "", statusdescription = "", am_referenceid = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			referencenumber = root.getAsJsonObject().get("transactionid").getAsString();
			receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		JsonObject queryrequest = new JsonObject();

		queryrequest.addProperty("am_referenceid", referencenumber);


		String results2 = g.toJson(queryrequest);

		CLIENT_URL = receiverquery;

		postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, results2);

		try {

			// capture the switch respoinse.
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// ===============================================================
			// an object that will contain parameters for provisional response
			// ===============================================================
			// exctract a specific json element from the object(status_code)
			statuscode = roots.getAsJsonObject().get("status").getAsString();

			// exctract a specific json element from the object(status_code)
			statusdescription = roots.getAsJsonObject().get("message").getAsString();

			am_referenceid = roots.getAsJsonObject().get("am_referenceid").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("command_status", APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS);
			String jsonResult = g.toJson(expected);
            logger.error("COOP CARD DISBURSEMENT AUTOQUERY STATUS RESPONSE THE RESPPONSE "+responseobject+" THE EXCEPTION "+e.toString());
			return jsonResult;

		}
		
		logger.error(".....................................................");
		logger.error("COOP CARD DISBURSEMENT QUERY REQUEST FROM BRIDGE :" + results2 + "\n");
		logger.error("COOP CARD DISBURSEMENT QUERY RESPONSE:" + roots + "\n");
		logger.error(".....................................................");

			     /*
    AUTHORIZED
    PARTIAL_AUTHORIZED
    AUTHORIZED_PENDING_REVIEW
    AUTHORIZED_RISK_DECLINED
    PENDING_AUTHENTICATION
    PENDING_REVIEW
    DECLINED
    INVALID_REQUEST
*/

		// map SUCCESS TRANSACTION
		if (statuscode.equalsIgnoreCase("AUTHORIZED")) {
			statuscode = "S000";
			statusdescription = "SUCCESS";
		}

		// map FAILED TRANSACTION
		if (statuscode.equalsIgnoreCase("DECLINED") || statuscode.equalsIgnoreCase("INVALID_REQUEST") ) {
			statuscode = "00029";
			statusdescription = "FAILED_TRANSACTION";
		}

		String success = "S000";
		String fail = "00029";
		if (statuscode.equalsIgnoreCase(success)) {

			expected.put("am_timestamp", username);
			expected.put("am_referenceid", am_referenceid);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (statuscode.equalsIgnoreCase(fail)) {
			expected.put("am_timestamp", username);
			expected.put("am_referenceid", am_referenceid);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("am_timestamp", username);
		expected.put("am_referenceid", am_referenceid);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}
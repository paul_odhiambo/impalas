package com.impalapay.mno.servlet.api.bridge.hub2;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.PhonenumberSplitUtil;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLHub2;

public class RouteMobileCollection extends HttpServlet {

	// private PostMinusThread postMinusThread;
	private PostWithIgnoreSSLHub2 postMinusThread;
	private PhonenumberSplitUtil phonenumbersplit;
	private Cache accountsCache;
	private String CLIENT_URL = "", TOKEN = "", TEXT_PATH = "", CALLBACKURI = "";
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

	
		logger = Logger.getLogger(this.getClass());

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonObject root2 = null, msisdnobject = null, creditrequestinitiate = null, creditrequest = null,
				hubtelobject = null, nextaction = null,responserequest = null;
		JsonArray hubtel2array =null;

		double amount = 0;

		int finalconvertedamount = 0;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", recipientcurrencycode = "",
				recipientcountrycode = "", recipientmobile = "", sendername = "", amountstring = "", remiturlss = "",
				responseobject = "", statuscodeinitiate = "", statuscode = "", statusdescription = "",
				referencenumberinitiate = "", referencenumber = "", token = "", debitreferenceno = "", networkname = "",
				mytimestamp = "", baseencode = "", real_pass = "", jsonResult = "", jsonData = "", TransactionType = "",
				phoneresults = "", mno = "", action = "", initiatetoken = "",merchantid="";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("debitcountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("debitcurrencycode").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			sendername = root.getAsJsonObject().get("debitname").getAsString();

			recipientmobile = root.getAsJsonObject().get("debitaccount").getAsString();

			debitreferenceno = root.getAsJsonObject().get("debitreferencenumber").getAsString();

			networkname = root.getAsJsonObject().get("networkname").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();
			
			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(recipientcurrencycode)
				|| StringUtils.isBlank(sendername) || StringUtils.isBlank(amountstring)
				|| StringUtils.isBlank(remiturlss) || StringUtils.isBlank(recipientcountrycode)) {

			expected.put("status_code", "00032");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

		}

		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("status_code", "00032");
			expected.put("status_description", "unauthorised user");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		token = PropertiesConfig.getConfigValue("HUB2AUTHKEY");
		merchantid = PropertiesConfig.getConfigValue("HUB2MERCHANTID");

		

		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################
		token = PropertiesConfig.getConfigValue("HUB2AUTHKEY");
		merchantid = PropertiesConfig.getConfigValue("HUB2MERCHANTID");

		

		// Assign mno field Value.
		phonenumbersplit = new PhonenumberSplitUtil();
		if (recipientcountrycode.equalsIgnoreCase("CI")) {// Côte d'Ivoire
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 5);

			if (phoneresults.equalsIgnoreCase("22507")) {

				mno = "orange";

			} else if (phoneresults.equalsIgnoreCase("22505")) {
				mno = "mtn";
			} else if (phoneresults.equalsIgnoreCase("22501")) {
				mno = "moov";

			} else {
				// return error unkmowm mobile prefix
			}

		}

		if (recipientcountrycode.equalsIgnoreCase("GN")) {// guinea
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 5);

			if (phoneresults.equalsIgnoreCase("22462") || phoneresults.equalsIgnoreCase("224610")
					|| phoneresults.equalsIgnoreCase("224611")) {
				mno = "orange";

			} else {
				// 22466
				mno = "mtn";
			}

		}

		if (recipientcountrycode.equalsIgnoreCase("BF")) {// bukina faso
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 5);

			if (phoneresults.equalsIgnoreCase("22606") || phoneresults.equalsIgnoreCase("22607")
					|| phoneresults.equalsIgnoreCase("22654") || phoneresults.equalsIgnoreCase("22655")
					|| phoneresults.equalsIgnoreCase("22657") || phoneresults.equalsIgnoreCase("22664")
					|| phoneresults.equalsIgnoreCase("22665") || phoneresults.equalsIgnoreCase("22666")
					|| phoneresults.equalsIgnoreCase("22667") || phoneresults.equalsIgnoreCase("22674")
					|| phoneresults.equalsIgnoreCase("22675") || phoneresults.equalsIgnoreCase("22676")
					|| phoneresults.equalsIgnoreCase("22677")) {
				mno = "orange";
			} else {
				// 22601, 22602, 22651, 22652, 22653, 22660, 22661, 22662, 22663, 22670, 22671,
				// 22672, 22673
				mno = "mobicash";
			}

		}

		if (recipientcountrycode.equalsIgnoreCase("BJ")) {// benin
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 5);

			if (phoneresults.equalsIgnoreCase("22997") || phoneresults.equalsIgnoreCase("22996")
					|| phoneresults.equalsIgnoreCase("22962") || phoneresults.equalsIgnoreCase("22966")
					|| phoneresults.equalsIgnoreCase("22996") || phoneresults.equalsIgnoreCase("22961")) {
				mno = "mtn";

			} else {
				// 22963, 22964, 22965
				mno = "moov";// flooz
			}

		}

		if (recipientcountrycode.equalsIgnoreCase("ML")) {// MALI
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 5);

			if (phoneresults.equalsIgnoreCase("22370") || phoneresults.equalsIgnoreCase("22371")
					|| phoneresults.equalsIgnoreCase("22372") || phoneresults.equalsIgnoreCase("22373")
					|| phoneresults.equalsIgnoreCase("22374") || phoneresults.equalsIgnoreCase("22375")
					|| phoneresults.equalsIgnoreCase("22376") || phoneresults.equalsIgnoreCase("22377")
					|| phoneresults.equalsIgnoreCase("22378") || phoneresults.equalsIgnoreCase("22379")
					|| phoneresults.equalsIgnoreCase("22380") || phoneresults.equalsIgnoreCase("22381")
					|| phoneresults.equalsIgnoreCase("22382") || phoneresults.equalsIgnoreCase("22383")
					|| phoneresults.equalsIgnoreCase("22384") || phoneresults.equalsIgnoreCase("22390")
					|| phoneresults.equalsIgnoreCase("22391") || phoneresults.equalsIgnoreCase("22392")
					|| phoneresults.equalsIgnoreCase("22393") || phoneresults.equalsIgnoreCase("22394")) {
				mno = "orange";

			} else {
				// 223 60 to 223 69, 223 85 to 223 89, 223 95 to 223 99
				mno = "mobicash";// flooz
			}

		}

		if (recipientcountrycode.equalsIgnoreCase("CM")) {// CAMEROON
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 6);

			if (phoneresults.equalsIgnoreCase("237670") || phoneresults.equalsIgnoreCase("237671")
					|| phoneresults.equalsIgnoreCase("237672") || phoneresults.equalsIgnoreCase("237673")
					|| phoneresults.equalsIgnoreCase("237674") || phoneresults.equalsIgnoreCase("237675")
					|| phoneresults.equalsIgnoreCase("237676") || phoneresults.equalsIgnoreCase("237677")
					|| phoneresults.equalsIgnoreCase("237678") || phoneresults.equalsIgnoreCase("237679")
					|| phoneresults.equalsIgnoreCase("237680") || phoneresults.equalsIgnoreCase("237681")
					|| phoneresults.equalsIgnoreCase("237650") || phoneresults.equalsIgnoreCase("237651")
					|| phoneresults.equalsIgnoreCase("237652") || phoneresults.equalsIgnoreCase("237653")
					|| phoneresults.equalsIgnoreCase("237654")) {
				mno = "mtn";

			} else {
//237 655, 237 656, 237 657, 237 658, 237 6590, 237 6591, 237 6592, 237 6593, 237 6594, 237 69
				mno = "orange";// flooz
			}

		}

		if (recipientcountrycode.equalsIgnoreCase("TG")) {// TOGO
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 5);

			if (phoneresults.equalsIgnoreCase("22870") || phoneresults.equalsIgnoreCase("22890")
					|| phoneresults.equalsIgnoreCase("22891") || phoneresults.equalsIgnoreCase("22892")
					|| phoneresults.equalsIgnoreCase("22893")) {
				mno = "tmoney";
			} else {
				// 228 79, 228 96, 228 97, 228 98, 228 99
				mno = "moov";// flooz
			}

		}

		if (recipientcountrycode.equalsIgnoreCase("SN")) {// SENEGAL
			phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, 6);
			if (phoneresults.equalsIgnoreCase("221771") || phoneresults.equalsIgnoreCase("221772")
					|| phoneresults.equalsIgnoreCase("221773") || phoneresults.equalsIgnoreCase("221774")
					|| phoneresults.equalsIgnoreCase("221775") || phoneresults.equalsIgnoreCase("221776")
					|| phoneresults.equalsIgnoreCase("221777") || phoneresults.equalsIgnoreCase("221778")
					|| phoneresults.equalsIgnoreCase("221779") || phoneresults.equalsIgnoreCase("221781")
					|| phoneresults.equalsIgnoreCase("221782") || phoneresults.equalsIgnoreCase("221783")
					|| phoneresults.equalsIgnoreCase("221784") || phoneresults.equalsIgnoreCase("221785")
					|| phoneresults.equalsIgnoreCase("221786") || phoneresults.equalsIgnoreCase("221787")
					|| phoneresults.equalsIgnoreCase("221788") || phoneresults.equalsIgnoreCase("221789")) {
				mno = "orange";
			} else if (phoneresults.equalsIgnoreCase("221761") || phoneresults.equalsIgnoreCase("221762")
					|| phoneresults.equalsIgnoreCase("221763") || phoneresults.equalsIgnoreCase("221764")
					|| phoneresults.equalsIgnoreCase("221765") || phoneresults.equalsIgnoreCase("221766")
					|| phoneresults.equalsIgnoreCase("221767") || phoneresults.equalsIgnoreCase("221768")
					|| phoneresults.equalsIgnoreCase("221769")) {

				mno = "free";

			} else {
				// 221 701 to 221 709
				mno = "emoney";

			}

		}
		

		TOKEN = token;

	

		creditrequestinitiate = new JsonObject();

		creditrequestinitiate.addProperty("purchaseReference", transactioinid);
		creditrequestinitiate.addProperty("customerReference", debitreferenceno);
		//creditrequestinitiate.addProperty("amount", amountstring);
		creditrequestinitiate.addProperty("amount", Integer.valueOf(amountstring));
		creditrequestinitiate.addProperty("currency", recipientcurrencycode);

		/**
		 * if (vendorfields != null) { creditrequest.add("vendor_uniquefields",
		 * vendorfields); }
		 **/

		// assign the remit url from properties.config
		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");

		CLIENT_URL = remiturlss;

		jsonData = g.toJson(creditrequestinitiate);

		// postMinusThread = new PostMinusThread(CLIENT_URL, jsonData);
		postMinusThread = new PostWithIgnoreSSLHub2(CLIENT_URL, jsonData, TOKEN, merchantid);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {

			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			statuscodeinitiate = roots.getAsJsonObject().get("status").getAsString();

			//hubtelobject = roots.getAsJsonObject().get("payments").getAsJsonObject();

		} catch (Exception e) {
          //{"statusCode":400,"message":["amount must be a positive number","amount must be an integer number"],"error":"Bad Request"}
			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			responserequest = roots.getAsJsonObject();
			
			referencenumberinitiate = "investigateTransaction";
			statuscodeinitiate = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
			
			if (responserequest.has("statusCode")) {
				statuscode = "00029";
				statusdescription = responserequest.getAsJsonObject().get("error").getAsString();
				//referencenumber = responserequest.getAsJsonObject().get("requestId").getAsString();
			} else {
				referencenumber = "investigateTransaction";
				statuscode = "00032";
				statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
			}
			
			logger.error(".....................................................");
			logger.error("HUB2 DEBIT INTENT REQUEST FROM BRIDGE :" + jsonData + "\n");
			logger.error("HUB2 DEBIT INTENT ERROR RESPONSE:" + roots + "\n");
			logger.error(".....................................................");
			
			expected.put("am_referenceid", referencenumber);
			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);

			return jsonResult;
			
			
		}

		// System.out.println("What we are receiving from Requesting");
		logger.error(".....................................................");
		logger.error("HUB2 DEBIT INTENT REQUEST FROM BRIDGE :" + jsonData + "\n");
		logger.error("HUB2 DEBIT INTENT RESPONSE:" + roots + "\n");
		logger.error(".....................................................");

		// map ACCEPTED FOR PROCESSING
		//if (statuscodeinitiate.equalsIgnoreCase("successful")) {payment_required
		if (statuscodeinitiate.equalsIgnoreCase("payment_required")) {
			initiatetoken = roots.getAsJsonObject().get("token").getAsString();
			referencenumberinitiate = roots.getAsJsonObject().get("id").getAsString();

			statuscodeinitiate = "S001";
/**
			try {
				referencenumberinitiate = hubtelobject.getAsJsonObject().get("id").getAsString();
				nextaction = hubtelobject.getAsJsonObject().get("nextAction").getAsJsonObject();
				initiatetoken = hubtelobject.getAsJsonObject().get("token").getAsString();
				action = nextaction.getAsJsonObject().get("type").getAsString();
			} catch (Exception e) {
				// TODO: handle exception
				statuscodeinitiate = "00029";
				statusdescription = "REJECTED_TRANSACTION";
			}
			// check next action if otp or ussd reject
			if (action.equalsIgnoreCase("ussd")) {
				statuscodeinitiate = "S001";
			} else {

				statuscodeinitiate = "00029";
				statusdescription = "REJECTED_TRANSACTION";
			}**/
		} else {
			statuscodeinitiate = "00029";
			statusdescription = "REJECTED_TRANSACTION";
			// System.out.println("Testinh Hapa"+roots+"\n"+"The token "+TOKEN);
		}
		
		//return respons here

		String inprogress = "S001";

		if (statuscodeinitiate.equalsIgnoreCase(inprogress)) {

			CLIENT_URL = remiturlss + "/" + referencenumberinitiate + "/payments";
			// START PREPARATION
			creditrequest = new JsonObject();
			msisdnobject = new JsonObject();

			creditrequest.addProperty("token", initiatetoken);
			creditrequest.addProperty("paymentMethod", "mobile_money");
			creditrequest.addProperty("country", recipientcountrycode);
			creditrequest.addProperty("provider", mno);

			msisdnobject.addProperty("msisdn", recipientmobile);
			msisdnobject.addProperty("otp", "");
			creditrequest.add("mobileMoney", msisdnobject);
			creditrequest.addProperty("onCancelRedirectionUrl", "");
			creditrequest.addProperty("onFinishRedirectionUrl", "");

			jsonData = g.toJson(creditrequest);

			postMinusThread = new PostWithIgnoreSSLHub2(CLIENT_URL, jsonData, TOKEN, merchantid);

			try {
				// *******************************************************
				// capture the switch response.
				// *******************************************************
				responseobject = postMinusThread.doPost();

				// pass the returned json string
				roots = new JsonParser().parse(responseobject);

				statuscode = roots.getAsJsonObject().get("status").getAsString();

				/**
				 * {"id":"pi_sdNlohDIZFDjtEg3TH8FR","createdAt":"2022-05-06T19:07:29.687Z","updatedAt":"2022-05-06T19:07:32.490Z","merchantId":"965","purchaseReference":"35663538799836","customerReference":"5437764899","amount":10,"currency":"XOF","token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpbnRlbnRJZCI6InBpX3NkTmxvaERJWkZEanRFZzNUSDhGUiIsIm1lcmNoYW50SWQiOiI5NjUiLCJtb2RlIjoibGl2ZSIsImlhdCI6MTY1MTg2NDA0OX0.DHyAqcAJKC2WoXGRhlihAJeWjn1kyeCeKQebNfc2zvg","status":"processing",
"payments":[{"id":"pay_3h37PIJ7tGENbnHhF6Qys","intentId":"pi_sdNlohDIZFDjtEg3TH8FR","createdAt":"2022-05-06T19:07:31.636Z","updatedAt":"2022-05-06T19:07:32.490Z","amount":10,"currency":"XOF","status":"created","method":"mobile_money","country":"CI","provider":"orange","number":"0767475356","fees":[{"currency":"XOF","id":"fee_tuukPtaP4zrWGmfkxMSEA","label":"payments.payment_processor_fee","rate":3,"rateType":"percent","amount":1}]}],"mode":"live"}
				 */
				hubtel2array = roots.getAsJsonObject().get("payments").getAsJsonArray();
				
				for (int i=0; i<hubtel2array.size(); i++) {
					hubtelobject = hubtel2array.get(i).getAsJsonObject();
				    //String value = json.get("key").getAsString();
				}
				
				referencenumber = hubtelobject.getAsJsonObject().get("id").getAsString();

				//hubtelobject = hubtel2array.getAsJsonObject().get(0);
				
				//System.out.println("MSEMA UKWELI "+hubtelobject);

			} catch (Exception e) {
				responserequest = roots.getAsJsonObject();
				// TODO: handle exception
				//{"statusCode":404,"message":"Cannot POST /payment-intents//payments","error":"Not Found"}
				//{"statusCode":400,"message":["msisdn is invalid"],"error":"Bad Request"}
				
				
				if (responserequest.has("statusCode")) {
					statuscode = "00029";
					statusdescription = responserequest.getAsJsonObject().get("error").getAsString();
					//referencenumber = responserequest.getAsJsonObject().get("requestId").getAsString();
				} else {
					referencenumber = "investigateTransaction";
					statuscode = "00032";
					statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
				}
				
				
				logger.error(".....................................................");
				logger.error("HUB2 DEBIT PAYMENT REQUEST FROM BRIDGE :" + jsonData + "\n");
				logger.error("HUB2 DEBIT PAYMENT ERROR RESPONSE:" + roots + "\n");
				logger.error(".....................................................");
				
				expected.put("am_referenceid", referencenumber);
				expected.put("am_timestamp", username);
				expected.put("status_code", statuscode);
				expected.put("status_description", statusdescription);
				jsonResult = g.toJson(expected);

				return jsonResult;
				
				
			}
			
			logger.error(".....................................................");
			
			logger.error("HUB2 DEBIT PAYMENT REQUEST URL :" + CLIENT_URL + "\n");
			logger.error("HUB2 DEBIT PAYMENT REQUEST FROM BRIDGE :" + jsonData + "\n");
			logger.error("HUB2 DEBIT PAYMENT RESPONSE:" + roots + "\n");
			logger.error(".....................................................");

			if (statuscode.equalsIgnoreCase("failed")) {
				statuscode = "00029";
				//statusdescription = "REJECTED_TRANSACTION";

			} else {
				// "created" "pending" "successful"
				statuscode = "S001";
				statusdescription = "CREDIT_IN_PROGRESS";

			}

			expected.put("am_referenceid", referencenumber);
			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);

			return jsonResult;

		} else {
			expected.put("am_referenceid", referencenumberinitiate);
			expected.put("am_timestamp", username);
			expected.put("status_code", statuscodeinitiate);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

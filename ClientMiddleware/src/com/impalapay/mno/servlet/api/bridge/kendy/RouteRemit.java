package com.impalapay.mno.servlet.api.bridge.kendy;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
//import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLBeyonic;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLKendy;

public class RouteRemit extends HttpServlet {

	// private PostWithIgnoreSSLBeyonic postIgnoreSslBeyonic;
	private Cache accountsCache;
	private String CLIENT_URL1 = "", CLIENT_URL = "", CLIENT_URL2 = "";
	private PostWithIgnoreSSLKendy postIgnoreSsl;
	private PostWithIgnoreSSL postMinusThread;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null, roots3 = null;
		JsonObject root2 = null, creditrequest = null, beneficiaryAccount = null, transaction = null, auth = null,
				confirmtransaction = null;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", sourcecountrycode = "",
				recipientcurrencycode = "", recipientcountrycode = "", sourcemsisdn = "", recipientmobile = "",
				sendername = "", amountstring = "", remiturlss = "", responseobject1 = "", responseobject = "",
				responseobject2 = "", statuscode = "", statuscode1 = "", statusdescription = "", referencenumber = "",
				thirdreference = "";
		String accountType = "", accountID = "", salt = "", sessionurl = "", sessionkey = "", message = "",
				confirmationcode = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			sourcecountrycode = root.getAsJsonObject().get("sourcecountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("recipientcurrencycode").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("recipientcountrycode").getAsString();

			sendername = root.getAsJsonObject().get("Sender_Name").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();

			sourcemsisdn = root.getAsJsonObject().get("source_msisdn").getAsString();

			recipientmobile = root.getAsJsonObject().get("beneficiary_msisdn").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(sourcecountrycode)
				|| StringUtils.isBlank(recipientcurrencycode) || StringUtils.isBlank(sendername)
				|| StringUtils.isBlank(amountstring) || StringUtils.isBlank(remiturlss)
				|| StringUtils.isBlank(recipientcountrycode)) {

			expected.put("status_code", "server error");
			expected.put("status_description", "missing parameters");
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

		}
		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("status_code", "server error");
			expected.put("status_description", "unauthorised user");
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// username = apiusername;
		// password = apipassword;
		accountType = PropertiesConfig.getConfigValue("ACCOUNT_TYPE");
		;
		accountID = PropertiesConfig.getConfigValue("ACCOUNT_ID");
		salt = PropertiesConfig.getConfigValue("SALT");
		sessionurl = PropertiesConfig.getConfigValue("SESSION_URL");
		// ################################################################################
		// Request for session id
		// ################################################################################
		JsonObject sessionrequest = new JsonObject();

		sessionrequest.addProperty("username", apiusername);
		sessionrequest.addProperty("password", apipassword);
		sessionrequest.addProperty("accountType", accountType);
		sessionrequest.addProperty("accountID", accountID);
		sessionrequest.addProperty("salt", salt);

		String result2 = g.toJson(sessionrequest);

		// CLIENT_URL = remiturlss;
		CLIENT_URL1 = "https://localhost:8456/AirtelRemittanceImpalas/kendysession";
		postMinusThread = new PostWithIgnoreSSL(CLIENT_URL1, result2);

		try {
			// capture the switch respoinse.
			responseobject1 = postMinusThread.doPost();
			// pass the returned json string
			roots = new JsonParser().parse(responseobject1);
			sessionkey = roots.getAsJsonObject().get("status_code").getAsString();
			message = roots.getAsJsonObject().get("status_description").getAsString();
		} catch (Exception e) {

			// expected.put("command_status",
			// APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS);
			expected.put("command_status", "unable to fetch session" + "  " + responseobject1);
			String jsonResult = g.toJson(expected);

			return responseobject1;

		}

		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ###############################################################################

		beneficiaryAccount = new JsonObject();
		transaction = new JsonObject();
		auth = new JsonObject();

		String shaUser = sessionkey + apiusername;
		String shaPass = sessionkey + apipassword;

		// Hashing
		String usernames = DigestUtils.sha256Hex(shaUser);
		String passwords = DigestUtils.sha256Hex(shaPass);

		String newusername = usernames + salt;
		String newpassword = passwords + salt;

		auth.addProperty("user", newusername);
		auth.addProperty("pass", newpassword);
		auth.addProperty("accountType", Integer.parseInt(accountType));
		auth.addProperty("accountID", accountID);
		auth.addProperty("sessionKey", sessionkey);

		beneficiaryAccount.addProperty("accountNo", recipientmobile);
		beneficiaryAccount.addProperty("accountType", "");
		beneficiaryAccount.addProperty("organisation", "");
		beneficiaryAccount.addProperty("orgBranch", "");

		transaction.addProperty("reference", transactioinid);
		// transaction.addProperty("senderIDType", "");
		transaction.addProperty("senderIDType", "DL");
		// transaction.addProperty("senderIDDetails", "");
		transaction.addProperty("senderIDDetails", "1133444");
		transaction.addProperty("senderName", sendername);
		transaction.addProperty("senderPhone", sourcemsisdn);
		transaction.addProperty("senderEmail", "");
		// transaction.addProperty("senderAddress", "");
		transaction.addProperty("senderAddress", "1ST AVENUE");
		// transaction.addProperty("senderCity", "");
		transaction.addProperty("senderCity", "NAIROBI");
		transaction.addProperty("senderCountry", sourcecountrycode);
		transaction.addProperty("senderAmount", Integer.parseInt(amountstring));
		transaction.addProperty("senderCurrency", "KES");
		// transaction.addProperty("beneficiaryIDType", "NI");
		transaction.addProperty("beneficiaryIDType", "DL");
		transaction.addProperty("beneficiaryIDDetails", "2000000");
		transaction.addProperty("beneficiaryName", recipientmobile);
		transaction.addProperty("beneficiaryPhone", recipientmobile);
		transaction.addProperty("beneficiaryEmail", "");
		transaction.addProperty("beneficiaryAddress", "165");
		transaction.addProperty("beneficiaryCity", "");
		transaction.addProperty("beneficiaryCountry", recipientcountrycode);
		transaction.addProperty("beneficiaryAmount", Integer.parseInt(amountstring));
		transaction.addProperty("beneficiaryCurrency", recipientcurrencycode);
		transaction.addProperty("messageToBeneficiary", "");
		transaction.addProperty("transactionType", "M");
		transaction.addProperty("messageToPayingAgent", "");
		transaction.add("beneficiaryAccount", beneficiaryAccount);
		transaction.addProperty("transferReason", "PE");
		transaction.addProperty("exchangeRate", "");
		transaction.addProperty("messageToBeneficiary", "");
		transaction.addProperty("beneficiaryAdminDivName", "");
		transaction.addProperty("beneficiaryAdminDivCode", "");

		creditrequest = new JsonObject();

		creditrequest.add("transaction", transaction);
		creditrequest.add("auth", auth);

		String jsonData = g.toJson(creditrequest);

		// if (vendorfields != null) { creditrequest.add("vendor_uniquefields",
		// vendorfields); }

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================
		// assign the remit url from properties.config
		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");
		// CLIENT_URL = remiturlss;
		CLIENT_URL = "http://46.101.131.249:7140/MTSAPI/rest/test/v1/startTransaction";

		postIgnoreSsl = new PostWithIgnoreSSLKendy(CLIENT_URL, jsonData);

		// *******************************************************
		// capture the switch response.
		// *******************************************************
		responseobject = postIgnoreSsl.doPost();

		try {

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			statuscode = roots.getAsJsonObject().get("status").getAsString();

			// exctract a specific json element from the object(status_code)
			statusdescription = roots.getAsJsonObject().get("message").getAsString();

			// referencenumber = roots.getAsJsonObject().get("reference").getAsString();

			confirmationcode = roots.getAsJsonObject().get("confirmationCode").getAsString();

			roots3 = roots.getAsJsonObject().get("transaction").getAsJsonObject();

			referencenumber = roots3.getAsJsonObject().get("reference").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("command_status",
					APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS + responseobject);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (statuscode.equalsIgnoreCase("0")) {
			statuscode = "S001";
		} else {
			statuscode = "00029";
		}

		String success = "S001";

		if (statuscode.equalsIgnoreCase(success)) {

			// confirm Transaction.
			confirmtransaction = new JsonObject();
			confirmtransaction.add("auth", auth);
			confirmtransaction.addProperty("confirmationCode", confirmationcode);

			String jsonData1 = g.toJson(confirmtransaction);

			CLIENT_URL2 = "http://46.101.131.249:7140/MTSAPI/rest/test/v1/confirmTransaction";

			postIgnoreSsl = new PostWithIgnoreSSLKendy(CLIENT_URL2, jsonData1);

			responseobject2 = postIgnoreSsl.doPost();

			try {
				// exctract a specific json element from the object(status_code)
				statuscode1 = roots.getAsJsonObject().get("status").getAsString();

				// exctract a specific json element from the object(status_code)
				statusdescription = roots.getAsJsonObject().get("message").getAsString();

			} catch (Exception e) {
				expected.put("command_status",
						APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS + responseobject2 + jsonData1);
				String jsonResult = g.toJson(expected);

				return jsonData1;
			}

			// confirm transaction and return response.

			// expected.put("am_referenceid", thirdreference);
			// expected.put("am_timestamp", username);
			// expected.put("status_code", "S001");
			// expected.put("status_description", "CREDIT_IN_PROGRESS");
			// String jsonResult = g.toJson(expected);

			// return statuscode+responseobject2+"eugene"+"confirmation
			// reference"+responseobject+"this is the confirmationcode "+confirmationcode ;

			return "start transaction request to kendy " + jsonData + "\n" + " start transaction response from kendy "
					+ responseobject + "\n" + "confrirm transaction request to kendy " + jsonData1 + "\n"
					+ "confirm transaction response from kendy " + responseobject2;
		}
		/**
		 * // ===================================================================== //
		 * construct object to return to sender system //
		 * =====================================================================
		 * expected.put("am_referenceid", thirdreference); expected.put("am_timestamp",
		 * username); expected.put("status_code", statuscode);
		 * expected.put("status_description", statusdescription); String jsonResult =
		 * g.toJson(expected);
		 * 
		 * return jsonResult;
		 **/

		return confirmationcode;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}


        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            
                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-area-chart"></i> Home <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="../dashboard.jsp">Dashboard</a>
                                        </li>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-users"></i>Accounts<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="accountsIndex.jsp">Add Account</a>
                                        </li>
                                        <li><a href="addManagementAccount.jsp">Add Management</a>
                                        </li>
                                         <li><a href="addroutedefine.jsp">Account->Network</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-database"></i> Transactions <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="alltransaction.jsp">View All Transactions</a>
                                        </li>
                                         <li><a href="updatestatus.jsp">Adjust Transaction Status</a>
                                        </li>
                                    </ul>
                                </li>
                               <!-- <li><a><i class="fa fa-money"></i>Manage Balance<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="addFloat.jsp">Adjust Client Balance</a>
					<li><a href="floatHistory.jsp">Client Balance History</a>
                                    </ul>
                                </li>-->
								<li><a><i class="fa fa-wifi"></i>Manage Networks<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                       <li><a href="addCountry.jsp">Add Country(MNO)</a></li>
                                        </li>
                                       <li><a href="addmnoprefix.jsp">Define Prefix(MNO)</a></li>
                                        </li>
                                         <li><a href="addNetwork.jsp">Add Networks</a></li>
                                        </li>
					<li><a href="addBank.jsp">Add Bank</a></li>
                                        </li>
                                        <li><a href="addBill.jsp">Add BillPayment</a></li>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-exclamation-triangle"></i>Manage Security<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="addSessionIp.jsp">IP-WhiteList/Session-Url</a>
                                        </li>
                                         <li><a href="systemlog.jsp">SystemLog History</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-cloud-download"></i>Manage Receivables<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="../collection/addcollectionNetwork.jsp">Add CollectionNetwork</a>
                                        </li>
                                         <li><a href="../collection/collectiondefine.jsp">Account-> CollectionNetwork</a>
                                        </li>
  					<li><a href="../collection/addcollectionSubAccnt.jsp">CollectionNetwork_SubAccount</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    </div>


        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            
                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-area-chart"></i> Home <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="../dashboard.jsp">Dashboard</a>
                                        </li>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-users"></i>Accounts<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="../accounts/accountsIndex.jsp">Add Account</a>
                                        </li>
                                        <li><a href="../accounts/addManagementAccount.jsp">Add Management</a>
                                        </li>
                                         <li><a href="../accounts/addroutedefine.jsp">Account->Network</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-database"></i> Transactions <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="../accounts/alltransaction.jsp">View All Transactions</a>
                                        </li>
                                         <li><a href="../accounts/updatestatus.jsp">Adjust Transaction Status</a>
                                        </li>
                                    </ul>
                                </li>
                               <!-- <li><a><i class="fa fa-money"></i>Manage Balance<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="../accounts/addFloat.jsp">Adjust Client Balance</a>
					<li><a href="../accounts/floatHistory.jsp">Client Balance History</a>
                                    </ul>
                                </li>-->
								<li><a><i class="fa fa-wifi"></i>Manage Networks<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                       <li><a href="../accounts/addCountry.jsp">Add Country(MNO)</a></li>
                                        </li>
                                       <li><a href="../accounts/addmnoprefix.jsp">Define Prefix(MNO)</a></li>
                                        </li>
                                         <li><a href="../accounts/addNetwork.jsp">Add Networks</a></li>
                                        </li>
					<li><a href="../accounts/addBank.jsp">Add Bank</a></li>
                                        </li>
                                        <li><a href="../accounts/addBill.jsp">Add BillPayment</a></li>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-exclamation-triangle"></i>Manage Security<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="../accounts/addSessionIp.jsp">IP-WhiteList/Session-Url</a>
                                        </li>
                                         <li><a href="../accounts/systemlog.jsp">SystemLog History</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-cloud-download"></i>Manage Receivables<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="addcollectionNetwork.jsp">Add CollectionNetwork</a>
                                        </li>
                                         <li><a href="collectiondefine.jsp">Account-> CollectionNetwork</a>
                                        </li>
  					<li><a href="addcollectionSubAccnt.jsp">CollectionNetwork_SubAccount</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    </div>

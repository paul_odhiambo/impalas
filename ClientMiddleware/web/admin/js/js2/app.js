angular.module('impalahub', ['ngMessages'])
    .controller('ChartsController', function($scope) {
        //Refresh the charts on tab change
        function refreshTransPast30DataChart() {
            document.getElementById("trans-last-30-chart-area").remove();
            var canvas = document.createElement('canvas');
            canvas.setAttribute('id', 'trans-last-30-chart-area');
            canvas.setAttribute('height', '300');
            canvas.setAttribute('width', '300');
            document.getElementById("trans-last-30-chart-container").appendChild(canvas);
        }

        function refreshTransLast6DataChart() {
            document.getElementById("trans-last-6-chart-area").remove();
            var canvas = document.createElement('canvas');
            canvas.setAttribute('id', 'trans-last-6-chart-area');
            canvas.setAttribute('height', '300');
            canvas.setAttribute('width', '300');
            document.getElementById("trans-last-6-chart-container").appendChild(canvas);
        }

        function refreshEachMonthDataChart() {
            document.getElementById("trans-each-month-chart-area").remove();
            var canvas = document.createElement('canvas');
            canvas.setAttribute('id', 'trans-each-month-chart-area');
            canvas.setAttribute('height', '300');
            canvas.setAttribute('width', '450');
            document.getElementById("trans-each-month-chart-container").appendChild(canvas);
        }

        function refreshEachCurrencyDataChart() {
            document.getElementById("trans-currency-chart-area").remove();
            var canvas = document.createElement('canvas');
            canvas.setAttribute('id', 'trans-currency-chart-area');
            canvas.setAttribute('height', '300');
            canvas.setAttribute('width', '450');
            document.getElementById("trans-currency-chart-container").appendChild(canvas);
        }

        //Dumy data for charts
        var transLast30Data = [
            {
                value: 550,
                color: "#46BFBD",
                highlight: "#5AD3D1",
                label: "Sent Transactions"
            },
            {
                value: 330,
                color: "#FDB45C",
                highlight: "#FFC870",
                label: "Received Transactions"
            }
        ];
        var transLast6Data = [
            {
                value: 155550,
                color: "#46BFBD",
                highlight: "#5AD3D1",
                label: "Sent Transactions"
            },
            {
                value: 200000,
                color: "#FDB45C",
                highlight: "#FFC870",
                label: "Received Transactions"
            }
        ];
        var monthlyData = {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"],
            datasets: [
                {
                    label: "Sent Transactions",
                    fillColor: "rgba(14,45,150,0.2)",
                    strokeColor: "rgba(20,63,204,1)",
                    pointColor: "rgba(14,45,150,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [65006, 58754, 90474, 81554, 56455, 55643, 40465, 50056, 65000, 85698, 12544, 45444]
                },
                {
                    label: "Received Transactions",
                    fillColor: "rgba(19,168,36,0.2)",
                    strokeColor: "rgba(23,207,44,1)",
                    pointColor: "rgba(19,168,36,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: [22356, 48976, 45467, 19654, 96679, 27234, 10024, 54455, 75445, 88652, 95857, 14635]
                }
            ]
        };
        var currencyData = {
            labels: ["Dollar", "Euro", "Pound Sterling", "Yen", "Yuan", "CAD", "KES"],
            datasets: [
                {
                    label: "Sent Transactions",
                    fillColor: "rgba(14,45,150,0.2)",
                    strokeColor: "rgba(20,63,204,1)",
                    pointColor: "rgba(14,45,150,1)",
                    highlightFill: "rgba(14,45,150,0.75)",
                    highlightStroke: "rgba(20,63,204,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [4000, 2500, 850, 1500, 5900, 6542, 5500]
                },
                {
                    label: "Received Transactions",
                    fillColor: "rgba(19,168,36,0.2)",
                    strokeColor: "rgba(23,207,44,1)",
                    pointColor: "rgba(19,168,36,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: [5236, 4876, 4567, 1654, 3979, 2234, 3004]
                }
            ]
        };

        //Initial Trans Past 30 days polar chart
        angular.element(document).ready(function() {
            var ctx = document.getElementById("trans-last-30-chart-area").getContext("2d");
            window.last30PolarArea = new Chart(ctx).PolarArea(transLast30Data, {
                responsive: true
            });
            document.getElementById('trans-last-30-js-legend').innerHTML = window.last30PolarArea.generateLegend();
        });
        //Initial Trans Last 6 months polar chart
        angular.element(document).ready(function() {
            var ctx = document.getElementById("trans-last-6-chart-area").getContext("2d");
            window.last6PolarArea = new Chart(ctx).PolarArea(transLast6Data, {
                responsive: true
            });
            document.getElementById('trans-last-6-js-legend').innerHTML = window.last6PolarArea.generateLegend();
        });
        //Initial Each month radar chart
        angular.element(document).ready(function() {
            window.monthlyRadar = new Chart(document.getElementById("trans-each-month-chart-area").getContext("2d")).Radar(monthlyData, {
                responsive: true
            });
            document.getElementById('trans-each-month-js-legend').innerHTML = window.monthlyRadar.generateLegend();
        });
        //Initial Each currency radar chart
        angular.element(document).ready(function() {
            window.currencyRadar = new Chart(document.getElementById("trans-currency-chart-area").getContext("2d")).Radar(currencyData, {
                responsive: true
            });
            document.getElementById('trans-currency-js-legend').innerHTML = window.currencyRadar.generateLegend();
        });

        //Switch according to current tab
        $scope.trans_past_30 = function(chart) {
            switch (chart) {
            case 'polar':
                angular.element(document).ready(function() {
                    refreshTransPast30DataChart();

                    var polar = document.getElementById("trans-last-30-chart-area").getContext("2d");
                    window.last30PolarArea = new Chart(polar).PolarArea(transLast30Data, {
                        responsive: true
                    });
                    document.getElementById('trans-last-30-js-legend').innerHTML = window.last30PolarArea.generateLegend();
                });

                break;

            case 'donut':
                angular.element(document).ready(function() {
                    refreshTransPast30DataChart();

                    var donut = document.getElementById("trans-last-30-chart-area").getContext("2d");
                    window.last30DonutArea = new Chart(donut).Doughnut(transLast30Data, {
                        responsive: true
                    });
                    document.getElementById('trans-last-30-js-legend').innerHTML = window.last30DonutArea.generateLegend();
                });
                break;

            case 'pie':
                angular.element(document).ready(function() {
                    refreshTransPast30DataChart();

                    var pie = document.getElementById("trans-last-30-chart-area").getContext("2d");
                    window.last30PieArea = new Chart(pie).Pie(transLast30Data, {
                        responsive: true
                    });
                    document.getElementById('trans-last-30-js-legend').innerHTML = window.last30PieArea.generateLegend();
                });
                break;
            }
        };
        $scope.trans_past_6 = function(chart) {
            switch (chart) {
            case 'polar':
                angular.element(document).ready(function() {
                    refreshTransLast6DataChart();

                    var polar = document.getElementById("trans-last-6-chart-area").getContext("2d");
                    window.last6PolarArea = new Chart(polar).PolarArea(transLast6Data, {
                        responsive: true
                    });
                    document.getElementById('trans-last-6-js-legend').innerHTML = window.last6PolarArea.generateLegend();
                });

                break;

            case 'donut':
                angular.element(document).ready(function() {
                    refreshTransLast6DataChart();

                    var donut = document.getElementById("trans-last-6-chart-area").getContext("2d");
                    window.last6DonutArea = new Chart(donut).Doughnut(transLast6Data, {
                        responsive: true
                    });
                    document.getElementById('trans-last-6-js-legend').innerHTML = window.last6DonutArea.generateLegend();
                });
                break;

            case 'pie':
                angular.element(document).ready(function() {
                    refreshTransLast6DataChart();

                    var pie = document.getElementById("trans-last-6-chart-area").getContext("2d");
                    window.last6PieArea = new Chart(pie).Pie(transLast6Data, {
                        responsive: true
                    });
                    document.getElementById('trans-last-6-js-legend').innerHTML = window.last6PieArea.generateLegend();
                });
                break;
            }
        };
        $scope.trans_calendar_month = function(chart) {
            switch (chart) {
            case 'radar':
                angular.element(document).ready(function() {
                    refreshEachMonthDataChart();

                    window.monthlyRadar = new Chart(document.getElementById("trans-each-month-chart-area").getContext("2d")).Radar(monthlyData, {
                        responsive: true
                    });
                    document.getElementById('trans-each-month-js-legend').innerHTML = window.monthlyRadar.generateLegend();
                });

                break;

            case 'bar':
                angular.element(document).ready(function() {
                    refreshEachMonthDataChart();

                    var bar = document.getElementById("trans-each-month-chart-area").getContext("2d");
                    window.monthlyBar = new Chart(bar).Bar(monthlyData, {
                        responsive: true
                    });
                    document.getElementById('trans-each-month-js-legend').innerHTML = window.monthlyBar.generateLegend();
                });
                break;
            }
        };
        $scope.trans_currencies = function(chart) {
            switch (chart) {
            case 'radar':
                angular.element(document).ready(function() {
                    refreshEachCurrencyDataChart();

                    window.currencyRadar = new Chart(document.getElementById("trans-currency-chart-area").getContext("2d")).Radar(currencyData, {
                        responsive: true
                    });
                    document.getElementById('trans-currency-js-legend').innerHTML = window.currencyRadar.generateLegend();
                });

                break;

            case 'bar':
                angular.element(document).ready(function() {
                    refreshEachCurrencyDataChart();

                    var bar = document.getElementById("trans-currency-chart-area").getContext("2d");
                    window.currencyBar = new Chart(bar).Bar(currencyData, {
                        responsive: true
                    });
                    document.getElementById('trans-currency-js-legend').innerHTML = window.currencyBar.generateLegend();
                });
                break;
            }
        };
    })
    .controller('AccountController', function($scope) {
        $scope.accountTab = function(settings) {
            switch (settings) {
            case 'accountsettings':
                $scope.isAccountSettings = true;
                $scope.isPasswordSettings = false;
                break;
            case 'passwordsettings':
                $scope.isAccountSettings = false;
                $scope.isPasswordSettings = true;
                break;
            }
        }
    })
    .directive('pwCheck', [function() {
            return {
                require: 'ngModel',
                link: function(scope, elem, attrs, ctrl) {
                    var firstPassword = '#' + attrs.pwCheck;
                    elem.add(firstPassword).on('keyup', function() {
                        scope.$apply(function() {
                            var v = elem.val() === $(firstPassword).val();
                            ctrl.$setValidity('pwmatch', v);
                        });
                    });
                }
            }
        }
    ]);
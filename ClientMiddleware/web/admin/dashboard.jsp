<!DOCTYPE html>
<%-- 
  Copyright (c) impalapay Ltd., June 23, 2014
  
  @author eugene chimita, eugenechimita@impalapay.com
--%>
<%@page import="com.impalapay.airtel.beans.accountmgmt.Account"%>
<%@page import="com.impalapay.airtel.beans.geolocation.Country"%>
<%@page import="com.impalapay.airtel.beans.forex.Forex"%>




<%@page import="com.impalapay.airtel.accountmgmt.session.SessionStatistics"%>
<%@page import="com.impalapay.airtel.accountmgmt.admin.SessionConstants"%>


<%@page import="com.impalapay.airtel.cache.CacheVariables"%>

<%@page import="org.apache.commons.lang3.StringUtils" %>

<%@page import="org.apache.commons.lang3.StringUtils" %>

<%@page import="net.sf.ehcache.Cache" %>
<%@page import="net.sf.ehcache.CacheManager" %>
<%@page import="net.sf.ehcache.Element" %>
<%@page import="java.text.SimpleDateFormat" %>

<%@page import="java.util.Calendar" %>
<%@page import="java.util.LinkedList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>

<%
    // The following is for session management.    
    if (session == null) {
        response.sendRedirect("index.jsp");
    }

    String sessionKey = (String) session.getAttribute(SessionConstants.ADMIN_SESSION_KEY);

    if (StringUtils.isEmpty(sessionKey)) {
        response.sendRedirect("index.jsp");
    }

    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=logout");
    // End of session management code



    CacheManager mgr = CacheManager.getInstance();
    Cache statisticsCache = mgr.getCache(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS);
    Cache countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
    Cache usdforexCache = mgr.getCache(CacheVariables.CACHE_USDFOREX_BY_UUID);
     Cache gbpforexCache = mgr.getCache(CacheVariables.CACHE_GBPFOREX_BY_UUID);



    SessionStatistics statistics = new SessionStatistics();

    Element element;

    if ((element = statisticsCache.get(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS_KEY)) != null) {
        statistics = (SessionStatistics) element.getObjectValue();
    }

    // This HashMap contains the UUIDs of countries as keys and the country names as values
    HashMap<String, String> countryHash = new HashMap<String, String>();
    
     List<Forex> usdforex = new ArrayList<Forex>();

     List<Forex> gbpforex = new ArrayList<Forex>();

    int count = 0; // A generic counter

    List keys;
    Country country;

    keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countryHash.put(country.getUuid(), country.getName());
    }

    keys = usdforexCache.getKeys();
    for (Object key : keys) {
         if ((element = usdforexCache.get(key)) != null) {
            usdforex.add((Forex) element.getObjectValue());
        }
    }

     keys = gbpforexCache.getKeys();
    for (Object key : keys) {
         if ((element = gbpforexCache.get(key)) != null) {
            gbpforex.add((Forex) element.getObjectValue());
        }
    }
    
   
    Calendar calendar = Calendar.getInstance();
    
    final int DAYS_IN_MONTH = calendar.getActualMaximum(Calendar.DAY_OF_MONTH) + 1;
    final int DAY_OF_MONTH = calendar.get(Calendar.DAY_OF_MONTH);
    final int MONTH = calendar.get(Calendar.MONTH) + 1;
    final int YEAR = calendar.get(Calendar.YEAR);
    final int YEAR_COUNT = YEAR + 10;
%>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ImpalaPay | Dashboard</title>
    <link  rel="icon" type="image/png"  href="images/logo.jpg">

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet">
    <link href="css/floatexamples.css" rel="stylesheet" />
    <link rel="stylesheet" href="../dist/tablesaw.css">
  
  

    <link href="css/style.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/jquery.jqplot.min.css" />
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/js2/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="js/js2/jquery.jqplot.min.js"></script>
    <script type="text/javascript" src="js/js2/jqplot.barRenderer.min.js"></script>
    <script type="text/javascript" src="js/js2/jqplot.pieRenderer.min.js"></script>
    <script type="text/javascript" src="js/js2/jqplot.categoryAxisRenderer.min.js"></script>
    <script type="text/javascript" src="js/js2/jqplot.pointLabels.min.js"></script>
    <script type="text/javascript" src="js/js2/jqplot.meterGaugeRenderer.min.js"></script>
    <script type="text/javascript" src="js/js2/jqplot.donutRenderer.min.js"></script>
    <script type="text/javascript" src="js/js2/jqplot.dateAxisRenderer.min.js"></script>
    <script type="text/javascript" src="js/js2/plugins/jqplot.categoryAxisRenderer.min.js"></script>
    <script type="text/javascript" src="js/js2/plugins/jqplot.ohlcRenderer.min.js"></script>
    <script type="text/javascript" src="js/js2/plugins/jqplot.highlighter.min.js"></script>
    <script type="text/javascript" src="js/js2/plugins/jqplot.cursor.min.js"></script>
    <!--<script type="text/javascript" src="js/js2/jquery-1.7.2.min.js"></script>-->
   
     <!--<script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>-->
  

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">


                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/img.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                           <h2><%=sessionKey%></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <jsp:include page="admin_menu.jsp" />
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                   <!-- <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>-->
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/img.png" alt=""><%= sessionKey%>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <form name="logoutForm" method="post" action="logout">
                                            <p>
                                            <!--<input type="submit" class="btn btn-primary" name="logout" id="logout" value="Logout">-->
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-sign-out pull-right"></i> Log Out</button>
                                           
                                            </p>
                                        </form>
                                     
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
            <div class="right_col" role="main">

                                    <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Dashboard</h3>
                         </div>  
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_content">

                                    <div class="row">

                                            <div class="col-lg-6 col-xs-6">
                                                <!-- small box -->
                                                <div class="small-box">
                                                    <div class="inner" id="chart1">
                                                    </div>
                                                </div>
                                            </div><!-- ./col -->
                                            
                                            <div class="col-lg-6 col-xs-6">
                                                <!-- small box -->
                                                <div class="small-box">
                                                    <div class="inner" id="chart2">
                                                    </div>
                                                </div>
                                            </div><!-- ./col -->
                                    </div>
                                </div>
                            </div>
                        </div>
                           <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <div class="row">
                                    <div class="col-lg-6 col-xs-6">
                                                <!-- small box -->
                                                <div class="small-box">
                                                    <div class="inner" id="chart3">  
                                                    </div>
                                                </div>
                                            </div><!-- ./col -->
                                            
                                            <div class="col-lg-6 col-xs-6">
                                                <!-- small box -->
                                                <div class="small-box">
                                                    <div class="inner">
                                                    <img src="admintransactionBar" alt="Transactions Bar Chart" align="middle" />
                                                    </div>
                                                </div>
                                            </div><!-- ./col -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- footer content -->
                <footer>
                    <div class="">
                        <p>Copyright@ImpalaPay 2014-2015</p>
                            <a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy
                            Policy</a> | ImpalaPay Ltd <%= Calendar.getInstance().get(Calendar.YEAR)%>. All rights reserved.
                            <!--<img id="madeInKenya" alt="Made in Kenya" src="#" width="100" height="100" />-->
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>

                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>

    </div>

    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>

    <script src="js/custom.js"></script>
    <script>

var urls = 'networkransactionCountPie';
            //alert(urls);

$.getJSON(urls, function(data) {
        var items1 = new Array();
        var j=0;
        for ( var i in data ) {
            var items = new Array();
            items.push(i,Number(data[i]));
            items1[j++] = items;
        }

        var plot1 = jQuery.jqplot('chart2', eval([items1]), {
                    title: 'Percentage Breakdown By-Network',
                    seriesDefaults:{
                        // Make this a pie chart.
                        renderer:jQuery.jqplot.PieRenderer,
                        rendererOptions:{
                            // Put data labels on the pie slices.
                            // By default, labels show the percentage of the slice.
                            //dataLabels:'value',
                            fill: true,
                            showDataLabels: true,
                            // Add a margin to seperate the slices.
                            sliceMargin: 2,
                            // stroke the slices with a little thicker line.
                            //lineWidth: 5,
                            //showDataLabels:true
                        }
                    },
                    //setting the slices color
                    //seriesColors: ["#FFA500","#7BE319","#FF0000"],
                    highlighter: {
                        show: true
                    },

                    legend:{ show:true, location:'e' }
                }
        );

    });


var urls = 'networkransactionCountPie';
            //alert(urls);

$.getJSON(urls, function(data) {
        var items1 = new Array();
        var j=0;
        for ( var i in data ) {
            var items = new Array();
            items.push(i,Number(data[i]));
            items1[j++] = items;
        }

        var plot1 = jQuery.jqplot('chart1', eval([items1]), {
                    title: 'Total Transactions Count By-Network',
                    seriesDefaults:{
                        // Make this a pie chart.
                        renderer:jQuery.jqplot.DonutRenderer,
                        rendererOptions:{
                            // Put data labels on the pie slices.
                            // By default, labels show the percentage of the slice.
                            dataLabels:'value',
                            fill: true,
                            showDataLabels: true,
                            // Add a margin to seperate the slices.
                            sliceMargin: 3,
                            startAngle: -90,
                            // stroke the slices with a little thicker line.
                            //lineWidth: 5,
                            //showDataLabels:true

                        }
                    },
                    //setting the slices color
                    //seriesColors: ["#FFA500","#7BE319","#FF0000"],
                    highlighter: {
                        show: true
                    },

                    legend:{ show:true, location:'e' }
                }
        );

    });

var urls = 'accountransactionCountPie';
            //alert(urls);

$.getJSON(urls, function(data) {
        var items1 = new Array();
        var j=0;
        for ( var i in data ) {
            var items = new Array();
            items.push(i,Number(data[i]));
            items1[j++] = items;
        }

        var plot1 = jQuery.jqplot('chart3', eval([items1]), {
                    title: 'Total Transaction Count By-Account',
                    seriesDefaults:{
                        // Make this a pie chart.
                        renderer:jQuery.jqplot.DonutRenderer,
                        rendererOptions:{
                            // Put data labels on the pie slices.
                            // By default, labels show the percentage of the slice.
                            dataLabels:'value',
                            fill: true,
                            showDataLabels: true,
                            // Add a margin to seperate the slices.
                            sliceMargin: 3,
                            startAngle: -90,
                            // stroke the slices with a little thicker line.
                            //lineWidth: 5,
                            //showDataLabels:true
                        }
                    },
                    //setting the slices color
                    //seriesColors: ["#FFA500","#7BE319","#FF0000"],
                    highlighter: {
                        show: true
                    },

                    legend:{ show:true, location:'e' }
                }
        );

    });



</script>



    
</body>
</html>

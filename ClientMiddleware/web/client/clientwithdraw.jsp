<!DOCTYPE html>
<%-- 
  Copyright (c) impalapay Ltd., June 23, 2014
  
  @author eugene chimita, eugenechimita@impalapay.com
--%>
<%@ page import="com.impalapay.collection.beans.balance.CollectionBalanceHistory"%>
<%@ page import="com.impalapay.collection.accountmgmt.pagination.collectionbalance.CollectedBalancePage"%>
<%@ page import="com.impalapay.collection.accountmgmt.pagination.collectionbalance.CollectedBalancePaginator"%>
<%@ page import="com.impalapay.airtel.beans.accountmgmt.balance.AccountPurchaseByCountry"%>
<%@ page import="com.impalapay.airtel.beans.geolocation.Country" %>
<%@ page import="com.impalapay.airtel.beans.accountmgmt.Account" %>
<%@ page import="com.impalapay.airtel.accountmgmt.session.SessionStatistics"%>
<%@ page import="com.impalapay.airtel.accountmgmt.session.SessionConstants"%>
<%@ page import="com.impalapay.airtel.persistence.accountmgmt.AccountStatusDAO" %>
<%@ page import="com.impalapay.airtel.cache.CacheVariables"%>


<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collections"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Iterator"%>
<%@page import="java.util.Enumeration"%>
<%@ page import="net.sf.ehcache.Cache" %>
<%@ page import="net.sf.ehcache.CacheManager" %>
<%@ page import="net.sf.ehcache.Element"%>

<%
 // The following is for session management.    
                        if (session == null) {
                            response.sendRedirect("../client");
                        }

                        String sessionUsername = (String) session.getAttribute(SessionConstants.ACCOUNT_SIGN_IN_KEY);

                        if (StringUtils.isEmpty(sessionUsername)) {
                            response.sendRedirect("index.jsp");
                        }

                        session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
                        response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=logout");
                        // End of session management code

                        CacheManager mgr = CacheManager.getInstance();
                        Cache accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

                        Cache statisticsCache = mgr.getCache(CacheVariables.CACHE_STATISTICS_BY_USERNAME);
                        
                        Cache countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
                        
                        Cache statusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);

     
     // This HashMap contains the UUIDs of countries as keys and the country names as values
    HashMap<String, String> countryHash = new HashMap<String, String>();
    HashMap<String, String> countrycurrencyHash = new HashMap<String, String>();
    HashMap<String, String> accountHash = new HashMap<String, String>();
    HashMap<String, String> currencyHash = new HashMap<String, String>();
    
    List keys;

    Element element;
    Country country;
    Account account;

    List<Account> accountList = new ArrayList<Account>();
    List<CollectionBalanceHistory> collectionbalanceList;

    keys = accountsCache.getKeysWithExpiryCheck();
    for (Object key : keys) {
        if ((element = accountsCache.get(key)) != null) {
            accountList.add((Account) element.getObjectValue());
        }
    }

    keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countryHash.put(country.getUuid(), country.getName());
    }

    keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countrycurrencyHash.put(country.getUuid(), country.getCurrency());
    }
    
     keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        currencyHash.put(country.getUuid(), country.getCurrencycode());
    }
    
    keys = accountsCache.getKeys();
    for (Object key : keys) {
        element = accountsCache.get(key);
        account = (Account) element.getObjectValue();
        accountHash.put(account.getUuid(), account.getFirstName());
    }



    Enumeration enumeration = request.getParameterNames();
    String collectionbalanceuuid = "", parameterName = "";
    if (enumeration.hasMoreElements()) {
        parameterName = (String) enumeration.nextElement();

        if (StringUtils.equalsIgnoreCase(parameterName, "page") || parameterName == null) {
            parameterName = (String) session.getAttribute("parameterName");

        } else {
            session.setAttribute("parameterName", parameterName);

        }

        //Search by unique id
        if (StringUtils.equalsIgnoreCase(parameterName, "collectionbalanceuuid")) {

            collectionbalanceuuid = request.getParameter("collectionbalanceuuid");
            //if pagination buttons(next,previous,first and last) are pressed
            //parameter value becomes null hence need to store it in a session
            if (collectionbalanceuuid == null) {
                collectionbalanceuuid = (String) session.getAttribute("collectionbalanceuuid");
            } else {
                session.setAttribute("collectionbalanceuuid", collectionbalanceuuid);

            }
        }
    }


%>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Collection Balance| Withdraw</title>
    <link  rel="icon" type="image/png"  href="images/logo.jpg">

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet">
    <link href="css/floatexamples.css" rel="stylesheet" />

    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

	<div class="container body">


		<div class="main_container">

			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">


					<div class="clearfix"></div>

					<!-- menu prile quick info -->
					<div class="profile">
						<div class="profile_pic">
							<img src="images/img.png" alt="..."
								class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<span>Welcome,</span>
							<h2><%=sessionUsername%></h2>
						</div>
					</div>
					<!-- /menu prile quick info -->

					<br />

					<!-- sidebar menu -->
					 <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            
                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-area-chart"></i> Home <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="index.jsp">Dashboard</a>
                                        </li>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-gears"></i>Account Management<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="accountmanage.jsp">Account Settings</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-credit-card"></i>Track Balance<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="accountbalance.jsp">View Float</a>
                                        </li>
                                           <li><a href="accountbalancehistory.jsp">Float History</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-database"></i> Transactions <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="viewtrans.jsp">View Transactions</a>
                                        </li>
                                        <li><a href="search.jsp">Search Transactions</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-share"></i> Collection <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="collectionbalance.jsp">View Collection Float</a>
                                        </li>
                                        <li><a href="collectionbalancehistory.jsp">Collection FloatHistory </a>
                                        <li><a href="viewprocessedcollections.jsp">Collection Transactions </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    </div>
					<!-- /sidebar menu -->

					<!-- /menu footer buttons -->
					<!-- <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>-->
					<!-- /menu footer buttons -->
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">

				<div class="nav_menu">
					<nav class="" role="navigation">
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>

						<ul class="nav navbar-nav navbar-right">
							<li class=""><a href="javascript:;"
								class="user-profile dropdown-toggle" data-toggle="dropdown"
								aria-expanded="false"> <img src="images/img.png" alt=""><%= sessionUsername%>
									<span class=" fa fa-angle-down"></span>
							</a>
								<ul
									class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
									<li>
										<form name="logoutForm" method="post" action="logout">
											<p>
												<!--<input type="submit" class="btn btn-primary" name="logout" id="logout" value="Logout">-->
												<button type="submit" class="btn btn-primary">
													<i class="fa fa-sign-out pull-right"></i> Log Out
												</button>

											</p>
										</form>

									</li>
								</ul></li>
						</ul>
					</nav>
				</div>

			</div>
			<!-- /top navigation -->


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Enter Withdarwal Details</h3>
						</div>
					</div>



					<div class="row">
						<div class="col-md-12">
							<div class="x_panel">

								<div class="x_content">

									<div class="row">
                                    <%
                    String addErrStr = (String) session.getAttribute(SessionConstants.CLIENT_WITHDRAWALREQUEST_ERROR_KEY);
                    String addSuccessStr = (String) session.getAttribute(SessionConstants.CLIENT_WITHDRAWALREQUEST_SUCCESS_KEY);
                    HashMap<String, String> paramHash = (HashMap<String, String>) session.getAttribute(
                            SessionConstants.CLIENT_WITHDRAWALREQUEST_PARAMETERS);

                    if (paramHash == null) {
                        paramHash = new HashMap<String, String>();
                    }

                    if (StringUtils.isNotEmpty(addErrStr)) {
                        out.println("<p class=\"alert alert-error\">");
                        out.println("Form error: " + addErrStr);
                        out.println("</p>");
                        session.setAttribute(SessionConstants.CLIENT_WITHDRAWALREQUEST_ERROR_KEY, null);
                    }

                    if (StringUtils.isNotEmpty(addSuccessStr)) {
                        out.println("<p class=\"alert alert-success\">");
                        out.println(addSuccessStr);
                        session.setAttribute(SessionConstants.CLIENT_WITHDRAWALREQUEST_SUCCESS_KEY, null);
                    }
                %>



										<div class="col-lg-7 col-xs-7">
											<!-- small box -->
											<div class="small-box">
												<div class="inner">
													<div class="x_content"></div>
													<form id="userSecurityForm" class="form-horizontal"
														action="clientwithdrawcash" method="post">
														<!--<div class="container">-->
														<!--<div class="row">-->
														<!--<div class="span8">-->
														<div class="alert alert-block alert-info">
															<p>Withdrawal Details</p>
														</div>
														<fieldset>
															
													 <input type="hidden" name="username"
																<%
            out.println("value=\"" + StringUtils.trimToEmpty(sessionUsername) + "\"");
                                    %>>
                                     <input type="hidden" name="collectionbalanceuuid"
                                                                <%
            out.println("value=\"" + StringUtils.trimToEmpty(collectionbalanceuuid) + "\"");
                                    %>>


                                    
															
															<div class="form-group">
																<label class="control-label col-md-3 col-sm-3 col-xs-12">Preffered Withdrawal
																	Currency </label>
																<div class="col-md-7 col-sm-7 col-xs-12">
																	<select name="terminatecurrency" id="input"
																		class="form-control col-md-3 col-xs-12">
																		<option value="">select Currency from dropdown</option>
																		<%
                                                                            Iterator a = countrycurrencyHash.keySet().iterator();

                                                                            while (a.hasNext()) {

                                                                                String uuid = (String) a.next();

                                                                                String name = (String) countrycurrencyHash.get(uuid);
                                                                                    %>

																		<option value="<%=uuid%>"><%=name%></option>

																		<%
                                                                                        }
                                                                                    %>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<label class="control-label col-md-3 col-sm-3 col-xs-12"
																	for="amount">Amount in Current Currency<span></span>
																</label>
																<div class="col-md-5 col-sm-6 col-sm-12">
																	<input type="text" id="amount" name="amount"
																		required="required"
																		class="form-control col-md-7 col-xs-12">
																</div>
															</div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                                    for="amount">Banking/Extra information<span></span>
                                                                </label>
                                                                <div class="col-md-5 col-sm-6 col-sm-12">
                                                                        <textarea class="form-control col-md-7 col-xs-12" rows="5" id="comment" name="extrainfo"></textarea>
                                                                </div>
                                                            </div>


                                                              
														</fieldset>
														<footer id="submit-actions" class="form-actions">
															<button id="submit-button" type="submit"
																class="btn btn-primary" name="action" value="CONFIRM">Add</button>
														</footer>
														<!--</div>-->
														<!--</div>-->
														<!--</div>-->
													</form>

												</div>
											</div>
										</div>
										<div class="col-lg-5 col-xs-6">
											<div class="alert alert-block alert-info">
												<p>Below are some Basic instruction</p>
												1.Select the Currency You would like to Receive Your withdrawal in.</br> 2. Enter the Amount you would wish to Withdraw (Note this will be in the current Collected currency) </br> 3. Enter Details that will help the bank in Transfering the money to you this can include but not limited to:(Bank name,Swiftcode,Branchcode,Account number...e.t.c)
											</div>

										</div>
									</div>
									<!-- ./col -->

								</div>

							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!-- footer content -->
				<footer>
					<div class="">
						<p>Copyright@ImpalaPay 2014-2015</p>
						<a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy
							Policy</a> | ImpalaPay Ltd
						<%= Calendar.getInstance().get(Calendar.YEAR)%>. All rights
						reserved.
						<!--<img id="madeInKenya" alt="Made in Kenya" src="#" width="100" height="100" />-->
						</p>
					</div>
					<div class="clearfix"></div>
				</footer>

				<!-- /footer content -->
			</div>
			<!-- /page content -->
		</div>

	</div>

	<script src="js/bootstrap.min.js"></script>

	<!-- chart js -->
	<script src="js/chartjs/chart.min.js"></script>
	<!-- bootstrap progress js -->
	<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
	<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script src="js/icheck/icheck.min.js"></script>

	<script src="js/custom.js"></script>

</body>

</html>

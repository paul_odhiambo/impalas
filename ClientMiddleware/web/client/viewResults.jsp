<!DOCTYPE html>
<%-- 
  Copyright (c) impalapay Ltd., June 23, 2014
  
  @author eugene chimita, eugenechimita@impalapay.com
--%>
<%@page import="com.impalapay.airtel.accountmgmt.pagination.TransactionPage"%>
<%@page import="com.impalapay.airtel.accountmgmt.pagination.SearchUuidPaginator"%>
<%@page import="com.impalapay.airtel.accountmgmt.pagination.SearchRecipientMobilePaginator"%>



<%@page import="com.impalapay.airtel.beans.transaction.TransactionStatus"%>
<%@page import="com.impalapay.airtel.beans.transaction.Transaction"%>

<%@page import="com.impalapay.airtel.beans.accountmgmt.Account"%>
<%@page import="com.impalapay.airtel.beans.geolocation.Country"%>



<%@page import="com.impalapay.airtel.accountmgmt.session.SessionStatistics"%>
<%@page import="com.impalapay.airtel.accountmgmt.session.SessionConstants"%>



<%@page import="com.impalapay.airtel.persistence.util.CountUtils"%>

<%@page import="com.impalapay.airtel.cache.CacheVariables"%>

<%@page import="org.apache.commons.lang3.StringUtils" %>

<%@page import="net.sf.ehcache.Cache" %>
<%@page import="net.sf.ehcache.CacheManager" %>
<%@page import="net.sf.ehcache.Element"%>

<%@page import="java.net.URLEncoder"%>

<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.LinkedList"%>

<%
 // The following is for session management.    
                        if (session == null) {
                            response.sendRedirect("../client");
                        }

                        String sessionUsername = (String) session.getAttribute(SessionConstants.ACCOUNT_SIGN_IN_KEY);

                        if (StringUtils.isEmpty(sessionUsername)) {
                            response.sendRedirect("index.jsp");
                        }

                        session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
                        response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=logout");
                        // End of session management code

                        CacheManager mgr = CacheManager.getInstance();
                        Cache accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

                        Cache statisticsCache = mgr.getCache(CacheVariables.CACHE_STATISTICS_BY_USERNAME);
                        
                        Cache countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
                        
                        Cache statusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);


    //These HashMaps contains the UUIDs of Countries as keys and the country code of countries as values
        HashMap<String, String> countryHash = new HashMap<String, String>();
                        
      HashMap<String, String> statusHash = new HashMap<String, String>();

    Account account = new Account();
    CountUtils countUtils = CountUtils.getInstance();
    List keys;
    int count = 0;  // Generic counter

    int transactionCount = 0; // The current count of the Transactions  

    Element element;

    if ((element = accountsCache.get(sessionUsername)) != null) {
         account = (Account) element.getObjectValue();
       }
    
   //fetch from cache
    Country country;
    keys = countryCache.getKeys();
    for(Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countryHash.put(country.getUuid(), country.getName());          
      }
   
   TransactionStatus transactionStatus;
    keys = statusCache.getKeys();
    for(Object key : keys) {
        element = statusCache.get(key);
        transactionStatus = (TransactionStatus) element.getObjectValue();
        statusHash.put(transactionStatus.getUuid(), transactionStatus.getDescription());            
      }
      
	   TransactionPage transactionPage = new TransactionPage();
	   List<Transaction> transactionList = new LinkedList<Transaction>();

	    // returns an enumeration of all the parameter names
	    Enumeration enumeration = request.getParameterNames();
	    //String uuid = "", msisdn = "", parameterName = "";
	    
	    String uuid = "", phone = "", parameterName = "";

					   
					    if (enumeration.hasMoreElements()) {
						parameterName = (String) enumeration.nextElement();


						if (StringUtils.equalsIgnoreCase(parameterName, "page") || parameterName == null) {
						    parameterName = (String) session.getAttribute("parameterName");

						} else {
						    session.setAttribute("parameterName", parameterName);

						}


            uuid = request.getParameter("uuid");
              //uuid="466b7d564ded49beb85f00e03730a370";
	      count=9;
            //count = countUtils.getTransactionByUuidCount(account, uuid);

            SearchUuidPaginator paginator = new SearchUuidPaginator(sessionUsername, uuid);

            //if pagination buttons(next,previous,first and last) are pressed
            //parameter value becomes null hence need to store it in a session
				    if (uuid == null) {
					uuid = (String) session.getAttribute("uuid");
					paginator = new SearchUuidPaginator(sessionUsername, uuid);


				    } else {

					session.setAttribute("uuid", uuid);

				    }


					    if (count == 0) { // This user has no Transactions in his/her account
						transactionPage = new TransactionPage();
						transactionList = new ArrayList<Transaction>();
						transactionCount = 0;

					    } else {

						transactionPage = (TransactionPage) session.getAttribute("currentSearchPage");

						String pageStr = (String) request.getParameter("page");

						// We are to give the first page 
						if (transactionPage == null || pageStr == null
							|| StringUtils.equalsIgnoreCase(pageStr, "first")) {
						    transactionPage = paginator.getFirstPage();

						    // We are to give the last page
						} else if (StringUtils.equalsIgnoreCase(pageStr, "last")) {
						    transactionPage = paginator.getLastPage();

						    // We are to give the previous page
						} else if (StringUtils.equalsIgnoreCase(pageStr, "previous")) {
						    transactionPage = paginator.getPrevPage(transactionPage);

						    // We are to give the next page
						} else if (StringUtils.equalsIgnoreCase(pageStr, "next")) {
						    transactionPage = paginator.getNextPage(transactionPage);
						}

						session.setAttribute("currentSearchPage", transactionPage);

						transactionList = transactionPage.getContents();
						transactionCount = (transactionPage.getPageNum() - 1) * transactionPage.getPagesize() + 1;


					    }
}

       
%>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ImpalaPay |Search Results</title>
    <link  rel="icon" type="image/png"  href="images/logo.jpg">

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet">
    <link href="css/floatexamples.css" rel="stylesheet" />

    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">


                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/img.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><%=sessionUsername%></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            
                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-area-chart"></i> Home <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="index.jsp">Dashboard</a>
                                        </li>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-gears"></i>Account Management<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="accountmanage.jsp">Account Settings</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-credit-card"></i>Track Balance<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="accountbalance.jsp">View Float</a>
                                        </li>
				         <li><a href="accountbalancehistory.jsp">Float History</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-database"></i> Transactions <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="viewtrans.jsp">View Transactions</a>
                                        </li>
                                        <li><a href="search.jsp">Search Transactions</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-share"></i> Collection <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="collectionbalance.jsp">View Collection Float</a>
                                        </li>
                                        <li><a href="collectionbalancehistory.jsp">Collection FloatHistory </a>
                                        <li><a href="viewprocessedcollections.jsp">Collection Transactions </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                   <!-- <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>-->
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/img.png" alt=""><%= sessionUsername%>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <form name="logoutForm" method="post" action="logout">
                                            <p>
                                            <!--<input type="submit" class="btn btn-primary" name="logout" id="logout" value="Logout">-->
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-sign-out pull-right"></i> Log Out</button>
                                           
                                            </p>
                                        </form>
                                     
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>

            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="clearfix"></div>


                    <div class="row">



                        <div class="clearfix"></div>

                        <div class="span16">
                            <div class="x_panel">
                                <div class="row-fluid">
                                    <div class="pull-left">

                                                        </div>

                                                                <div class="x_content">
 <%
                                                            if (transactionList != null) {
                                                                for (Transaction codes : transactionList) {
                                                                %>
   <section class="content invoice">
                                        <!-- title row -->
                                        <div class="row">
                                            <div class="col-xs-12 invoice-header">
                                                <h1>
                                        <small class="pull-right">Server Time: <%=codes.getServerTime()%></small>
                                    </h1>
                                            </div>
                                            <!-- /.col -->
                                        </div>
<h4>
                                        <!-- info row -->
                                        <div class="row invoice-info">
                                            <div class="col-sm-4 invoice-col"> 
                                               <address>
                                                 
                                                 <b>From</b>
                                                 <br><br>
                                        <strong>SENDER NAME : </strong>
                                        <%=codes.getSenderName()%>
                                        <br><br>
                                        <strong>SOURCE COUNTRY : </strong>
                                        <%=codes.getSourceCountrycode()%>
                                       <br><br>
                                        <strong>CLIENT TIME : </strong>
                                       <%=codes.getClientTime()%>
                                      
                                    </address>
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4 invoice-col">
                                                
                                                <address>
                                                 <b>To</b>
                                                 <br><br>
                                        <strong>RECEIVER MOBILE : </strong>
                                        <%=codes.getRecipientMobile()%>
                                        <br><br>
                                        <strong>CURRENCY CODE : </strong>
                                        <%=codes.getCurrencyCode()%>
                                        <br><br>
                                        <strong>AMOUNT : </strong>
                                        <%=codes.getAmount()%>
                                        <br><br>
                                        <strong>RECIPIENT COUNTRY : </strong>
					<%=countryHash.get(codes.getRecipientCountryUuid())%>                                      
                                    </address>
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4 invoice-col">
                                            <address>
                                                <b>Details</b>
                                                 <br><br>
                                                <b>TRANSACTIN STATUS : </b>
                                                 <%=statusHash.get(codes.getTransactionStatusUuid())%>
                                                 <br><br>
                                                <b>REFERENCE NUMBER : </b>
                                                 <%=codes.getReferenceNumber()%>
                                                 <br><br>
                                                <b>RECEIVER TRANSACTINUUID : </b>
                                                 <%=codes.getReceivertransactionUuid()%>
                                                 <br><br>
                                                <b>SENDERTOKEN : </b>
                                                 <%=codes.getSenderToken()%>
                                                <address>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                      </h4>
                                        <!-- /.row -->

                                        <!-- Table row -->
                                        
                                        <!-- /.row -->

                                        
                                        <!-- /.row -->

                                        <!-- this row will not appear when printing -->
                                        <div class="row no-print">
                                            <div class="col-xs-12">
                                                <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                                                <a href="viewtrans.jsp" class="btn btn-success pull-right" role="button"><i class="fa fa-arrow-circle-left"></i>Back</a>
                                                
                                            </div>
                                        </div>
                                        </section>
  <%
                                                                    transactionCount++;
                                                                }
                                                                }

                                                            %>
                                                                 </div>

                                                                            
                                               
                                            
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p>Copyright@ImpalaPay 2014-2015</p>
                            <a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy
                            Policy</a> | ImpalaPay Ltd <%= Calendar.getInstance().get(Calendar.YEAR)%>. All rights reserved.
                            <!--<img id="madeInKenya" alt="Made in Kenya" src="#" width="100" height="100" />-->
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>

                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>

    </div>

    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>

    <script src="js/custom.js"></script>

</body>

</html>

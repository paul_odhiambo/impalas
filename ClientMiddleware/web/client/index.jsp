<!DOCTYPE html>
<%-- 
  Copyright (c) impalapay Ltd., June 23, 2014
  
  @author eugene chimita, eugenechimita@impalapay.com
--%>
<%@page import="com.impalapay.airtel.beans.accountmgmt.Account"%>



<%@page import="com.impalapay.airtel.accountmgmt.session.SessionStatistics"%>
<%@page import="com.impalapay.airtel.accountmgmt.session.SessionConstants"%>
<%@ page import="com.impalapay.airtel.accountmgmt.admin.pagination.network.NetworkPage"%>
<%@ page import="com.impalapay.airtel.accountmgmt.admin.pagination.network.NetworkPaginator"%>
<%@ page import="com.impalapay.beans.network.Network"%>
<%@ page import="com.impalapay.airtel.beans.geolocation.Country"%>

<%@ page import="com.impalapay.airtel.accountmgmt.admin.pagination.currentforex.CurrentforexPage"%>
<%@ page import="com.impalapay.airtel.accountmgmt.admin.pagination.currentforex.CurrentforexPaginator"%>
<%@ page import="com.impalapay.airtel.beans.forex.ForexEngine"%>

<%@page import="com.impalapay.airtel.cache.CacheVariables"%>

<%@page import="org.apache.commons.lang3.StringUtils" %>

<%@page import="net.sf.ehcache.Cache" %>
<%@page import="net.sf.ehcache.CacheManager" %>
<%@page import="net.sf.ehcache.Element"%>

<%@page import="java.text.DecimalFormat"%>

<%@page import="java.net.URLEncoder"%>
<%@page import="java.util.List" %>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList" %>

<%
    // The following is for session management.    
    if (session == null) {
        response.sendRedirect("../client");
    }

    String sessionUsername = (String) session.getAttribute(SessionConstants.ACCOUNT_SIGN_IN_KEY);

    if (StringUtils.isEmpty(sessionUsername)) {
        response.sendRedirect("index.jsp");
    }

    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=logout");
    // End of session management code

    CacheManager mgr = CacheManager.getInstance();
    Cache accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

    Cache statisticsCache = mgr.getCache(CacheVariables.CACHE_STATISTICS_BY_USERNAME);

    Account account = new Account();
    SessionStatistics statistics = new SessionStatistics();

    Element element;

    if ((element = accountsCache.get(sessionUsername)) != null) {
        account = (Account) element.getObjectValue();
    }

   if ((element = statisticsCache.get(sessionUsername)) != null) {
        statistics = (SessionStatistics) element.getObjectValue();
    }


    int count = 0; // A generic counter  
    List<Network> networkList;
    List<ForexEngine> currentforexList;
    List<Country> countryList = new ArrayList<Country>();
    HashMap<String, String> networkHash = new HashMap<String, String>();
    HashMap<String, String> countryHash = new HashMap<String, String>();
    
    Cache networkCache = mgr.getCache(CacheVariables.CACHE_NETWORK_BY_UUID);
    Cache forexrateCache = mgr.getCache(CacheVariables.CACHE_FOREX_BY_UUID);
    Cache countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
    Network network;
    Country country;

    List keys;
    
    keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countryHash.put(country.getUuid(), country.getName());
    }
    
    keys = networkCache.getKeys();
    for (Object key : keys) {
        element = networkCache.get(key);
        network = (Network) element.getObjectValue();
        networkHash.put(network.getUuid(), network.getNetworkname());
    }
    
    int incount = 0;  // Generic counter
    int incounts = 0;

    NetworkPaginator paginator = new NetworkPaginator();
    CurrentforexPaginator paginators = new CurrentforexPaginator(); 
    
    incount = statistics.getNetworkcountTotal();
    //incount = 1000;
    NetworkPage networkPage;
    int networkCount = 0; // The current count of the Accounts sessions

    if (incount == 0) { 	// This user has no Incoming USSD in the account
        networkPage = new NetworkPage();
        networkList = new ArrayList<Network>();
        networkCount = 0;

    } else {
        networkPage = (NetworkPage) session.getAttribute("currentNetworkPageClient");
        String referrer = request.getHeader("referer");
        String pageParam = (String) request.getParameter("page");

        // We are to give the first page
        if (networkPage == null
                || !StringUtils.endsWith(referrer, "index.jsp")
                || StringUtils.equalsIgnoreCase(pageParam, "first")) {
            networkPage = paginator.getFirstPage();

            // We are to give the last page
        } else if (StringUtils.equalsIgnoreCase(pageParam, "last")) {
            networkPage = paginator.getLastPage();

            // We are to give the previous page
        } else if (StringUtils.equalsIgnoreCase(pageParam, "previous")) {
            networkPage = paginator.getPrevPage(networkPage);

            // We are to give the next page 
        } else {
            networkPage = paginator.getNextPage(networkPage);
        }

        session.setAttribute("currentNetworkPageClient", networkPage);

        networkList = networkPage.getContents();

        networkCount = (networkPage.getPageNum() - 1) * networkPage.getPagesize() + 1;

        
    }


    incounts = statistics.getForexrateCountTotal();
    CurrentforexPage currentforexPage;
    int currentforexCount = 0; // The current count of the Accounts sessions

    if (incounts == 0) {    // This user has no Incoming USSD in the account
        currentforexPage = new CurrentforexPage();
        currentforexList = new ArrayList<ForexEngine>();
        currentforexCount = 0;

    } else {
        currentforexPage = (CurrentforexPage) session.getAttribute("currentforexratePage");
        String referrers = request.getHeader("referer");
        String pageParams = (String) request.getParameter("page");

        // We are to give the first page
        if (currentforexPage == null
                || !StringUtils.endsWith(referrers, "index.jsp")
                || StringUtils.equalsIgnoreCase(pageParams, "first")) {
            currentforexPage = paginators.getFirstPage();

            // We are to give the last page
        } else if (StringUtils.equalsIgnoreCase(pageParams, "last")) {
            currentforexPage = paginators.getLastPage();

            // We are to give the previous page
        } else if (StringUtils.equalsIgnoreCase(pageParams, "previous")) {
            currentforexPage = paginators.getPrevPage(currentforexPage);

            // We are to give the next page 
        } else {
            currentforexPage = paginators.getNextPage(currentforexPage);
        }

        session.setAttribute("currentforexratePage", currentforexPage);

        currentforexList = currentforexPage.getContents();

        currentforexCount = (currentforexPage.getPageNum() - 1) * currentforexPage.getPagesize() + 1;

        
    }

    
    
%>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ImpalaPay | Dashboard</title>
    <link  rel="icon" type="image/png"  href="images/logo.jpg">

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet">
    <link href="css/floatexamples.css" rel="stylesheet" />
    <link rel="stylesheet" href="../dist/tablesaw.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.jqplot.min.css" />
    <script src="js/jquery.min.js"></script>
    
     <!--<script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>-->
    <script type="text/javascript" src="js/jquery.jqplot.min.js"></script>
    <script type="text/javascript" src="js/jqplot.barRenderer.min.js"></script>
    <script type="text/javascript" src="js/jqplot.pieRenderer.min.js"></script>
    <script type="text/javascript" src="js/jqplot.categoryAxisRenderer.min.js"></script>
    <script type="text/javascript" src="js/jqplot.pointLabels.min.js"></script>
    <script type="text/javascript" src="js/jqplot.meterGaugeRenderer.min.js"></script>
    <script type="text/javascript" src="js/jqplot.donutRenderer.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">


                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/img.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><%=sessionUsername%></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            
                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-area-chart"></i> Home <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="index.jsp">Dashboard</a>
                                        </li>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-gears"></i>Account Management<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="accountmanage.jsp">Account Settings</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-credit-card"></i>Track Balance<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="accountbalance.jsp">View Float</a>
                                        </li>
                                        <li><a href="accountbalancehistory.jsp">Float History</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-database"></i> Transactions <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="viewtrans.jsp">View Transactions</a>
                                        </li>
                                        <li><a href="search.jsp">Search Transactions</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-share"></i> Collection <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="collectionbalance.jsp">View Collection Float</a>
                                        </li>
                                        <li><a href="collectionbalancehistory.jsp">Collection FloatHistory </a>
                                        <li><a href="viewprocessedcollections.jsp">Collection Transactions </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                   <!-- <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>-->
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/img.png" alt=""><%= sessionUsername%>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <form name="logoutForm" method="post" action="logout">
                                            <p>
                                            <!--<input type="submit" class="btn btn-primary" name="logout" id="logout" value="Logout">-->
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-sign-out pull-right"></i> Log Out</button>
                                           
                                            </p>
                                        </form>
                                     
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
            <div class="right_col" role="main">

                                    <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Dashboard</h3>
                         </div>  
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_content">

                                    <div class="row">

                                            <div class="col-lg-6 col-xs-6">
                                                <!-- small box -->
                                                <div class="small-box">
                                                    <div class="inner">

                                                     <img
                                                        src="transactionBar?<% out.print("username=" + URLEncoder.encode(sessionUsername, "UTF-8"));%>"
                                                        alt="Transactions Bar Chart" />
                                                       
                                                    </div>
                                                </div>
                                            </div><!-- ./col -->
                                            
                                            <div class="col-lg-6 col-xs-6">
                                                <!-- small box -->
                                                <div class="small-box">
                                                    <div class="inner">
                                                    <img
                                                        src="transactionPie?<% out.print("username=" + URLEncoder.encode(sessionUsername, "UTF-8"));%>"
                                                        alt="Transactions Pie Chart" />
                                                       
                                                    </div>
                                                </div>
                                            </div><!-- ./col -->
             
                                    </div>

                                </div>
                            </div>
                        </div>
                           <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_content">

                                    <div class="row">

                                            <div class="col-lg-6 col-xs-6">
                                                <!-- small box -->
                                                                                              <!-- small box -->
                                                <div class="small-box">
                                                    <div class="inner">
                                                    <h5>Active Networks</h5>
                                                    <div class="x_content">
                                                         <div align="right">
                                                        <div id="pagination">
                                                      <form name="pageForm" method="post" action="index.jsp">                                
					                    <%                                            if (!networkPage.isFirstPage()) {
					                    %>
					                    <input class="btn btn-info" type="submit" name="page" value="First" />
					                    <input class="btn btn-info" type="submit" name="page" value="Previous"/>
					 
					                    <%
					                        }
					                    %>
					                    <span class="pageInfo">Page 
					                        <span class="pagePosition currentPage"><%= networkPage.getPageNum()%></span> of 
					                        <span class="pagePosition"><%= networkPage.getTotalPage()%></span>
					                    </span>   
					                    <%
					                        if (!networkPage.isLastPage()) {
					                    %>
					                    <input class="btn btn-info" type="submit" name="page"value="Next"> 
					                    <input class="btn btn-info" type="submit"name="page" value="Last">
					                    <%
					                        }
					                    %>                                
					                </form>
                                                            
                                                        </div>
                                                        <table class="tablesaw" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                                        <!--<table id="example" class="table table-striped responsive-utilities jambo_table">-->
                                                        <!--<table class="table table-striped bootstrap-datatable datatable">-->
                                                        <thead>
                                                            <tr>
                                                               <!--<th></th>-->
                                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Network Name</th>
                                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Country</th>
                                                            <!--<th>Date Created</th>-->
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <%
                                                            if (networkList != null) {
                                                                for (Network code : networkList) {
                                                                %>

                                                                <tr class="btn-warning">
                                                                    <!--<td width="5%"><%=networkCount%></td>-->
                                                                    <td class="center"><%=code.getNetworkname()%></td>
                                                                    <td class="center"><%=countryHash.get(code.getCountryUuid())%></td>
                                                                  
                                                                    
                                                                   
                                                                </tr>
                                                                 <%
                                                                    networkCount++;
                                                                }
                                                                }

                                                            %>
                                                           
                                                        </tbody>
                                                        </table>
                                                     </div> 
                                                    </div>
                                                </div>
                                            </div><!-- ./col -->
                                            </div>
                                            <div class="col-lg-6 col-xs-6">
                                                <div class="small-box">
                                                    <div class="inner">
                                                        <h5>Current Rates</h5>
                                                        <div class="x_content">
                                                            <div align="right">
                                                                <div id="pagination">
                                                                    <form name="pageForm" method="post" action="index.jsp">                                
                                                                        <%                                            if (!currentforexPage.isFirstPage()) {
                                                                        %>
                                                                        <input class="btn btn-info" type="submit" name="page" value="First" />
                                                                        <input class="btn btn-info" type="submit" name="page" value="Previous"/>
                                                     
                                                                        <%
                                                                            }
                                                                        %>
                                                                        <span class="pageInfo">Page 
                                                                            <span class="pagePosition currentPage"><%= currentforexPage.getPageNum()%></span> of 
                                                                            <span class="pagePosition"><%= currentforexPage.getTotalPage()%></span>
                                                                        </span>   
                                                                        <%
                                                                            if (!currentforexPage.isLastPage()) {
                                                                        %>
                                                                        <input class="btn btn-info" type="submit" name="page"value="Next"> 
                                                                        <input class="btn btn-info" type="submit"name="page" value="Last">
                                                                        <%
                                                                            }
                                                                        %>                                
                                                                    </form> 
                                                                </div>  
                                                                 <table class="tablesaw" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                                                        <!--<table id="example" class="table table-striped responsive-utilities jambo_table">-->
                                                                        <!--<table class="table table-striped bootstrap-datatable datatable">-->
                                                                        <thead>
                                                                            <tr>
                                                                                <!--<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">&nbsp;</th>-->
                                                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">CurrencyPair</th>
                                                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Exchange Rate</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <%
                                                                            if (currentforexList != null) {
                                                                                for (ForexEngine codes : currentforexList) {
                                                                                %>

                                                                                <tr class="btn-primary">
                                                                                    <!--<td width="5%"><%=currentforexCount%></td>-->
                                                                                    <td class="center"><%=codes.getCurrencypair()%></td>
                                                                                    <td class="center"><%=codes.getSpreadrate()%></td>
                                                                                </tr>
                                                                                 <%
                                                                                    currentforexCount++;
                                                                                }
                                                                                }

                                                                            %>
                                                                           
                                                                        </tbody>
                                                                        </table>
                                                            </div> 
                                                        </div>
                                                    </div>  
                                                </div><!-- ./col -->
                                            </div>

                                </div>
                            </div>
                        </div>
                    </div>
            </div>
                <!-- footer content -->
                <footer>
                    <div class="">
                        <p>Copyright@ImpalaPay 2014-2015</p>
                            <a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy
                            Policy</a> | ImpalaPay Ltd <%= Calendar.getInstance().get(Calendar.YEAR)%>. All rights reserved.
                            <!--<img id="madeInKenya" alt="Made in Kenya" src="#" width="100" height="100" />-->
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>

                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>

    </div>

    



    <!-- chart js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
      <script src="js/chartjs/chart.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <script src="js/custom.js"></script>

    <script src="../dist/dependencies/jquery.js"></script>
    <script src="../dist/tablesaw.js"></script>
    <script src="../dist/tablesaw-init.js"></script>
    

</body>

</html>

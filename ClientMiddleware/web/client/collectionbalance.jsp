<!DOCTYPE html>
<%--
  Copyright (c) impalapay Ltd., June 23, 2014

  @author eugene chimita, eugenechimita@impalapay.com
--%>
<%@ page import="com.impalapay.collection.beans.balance.CollectionBalanceHistory"%>
<%@ page import="com.impalapay.collection.accountmgmt.pagination.collectionbalance.CollectedBalancePage"%>
<%@ page import="com.impalapay.collection.accountmgmt.pagination.collectionbalance.CollectedBalancePaginator"%>
<%@ page import="com.impalapay.collection.accountmgmt.pagination.cashwithdrawal.ClientWithdrawalPage"%>
<%@ page import="com.impalapay.collection.accountmgmt.pagination.cashwithdrawal.ClientWithdrawalPaginator"%>
<%@ page import="com.impalapay.collection.accountmgmt.pagination.cashwithdrawal.ClientWithdrawalHistPage"%>
<%@ page import="com.impalapay.collection.accountmgmt.pagination.cashwithdrawal.ClientWithdrawalHistPaginator"%>
<%@ page import="com.impalapay.collection.beans.balance.CashWithdrawal"%>
<%@ page import="com.impalapay.airtel.beans.transaction.TransactionStatus"%>
<%@ page import="com.impalapay.airtel.beans.geolocation.Country" %>
<%@ page import="com.impalapay.airtel.beans.accountmgmt.Account" %>
<%@ page import="com.impalapay.airtel.accountmgmt.session.SessionStatistics"%>
<%@ page import="com.impalapay.airtel.accountmgmt.session.SessionConstants"%>
<%@ page import="com.impalapay.airtel.persistence.accountmgmt.AccountStatusDAO" %>
<%@ page import="com.impalapay.airtel.cache.CacheVariables"%>


<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collections"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Iterator"%>

<%@ page import="net.sf.ehcache.Cache" %>
<%@ page import="net.sf.ehcache.CacheManager" %>
<%@ page import="net.sf.ehcache.Element"%>

<%
 // The following is for session management.
                        if (session == null) {
                            response.sendRedirect("../client");
                        }

                        String sessionUsername = (String) session.getAttribute(SessionConstants.ACCOUNT_SIGN_IN_KEY);

                        if (StringUtils.isEmpty(sessionUsername)) {
                            response.sendRedirect("index.jsp");
                        }

                        session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
                        response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=logout");
                        // End of session management code

                        CacheManager mgr = CacheManager.getInstance();
                        Cache accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

                        Cache statisticsCache = mgr.getCache(CacheVariables.CACHE_STATISTICS_BY_USERNAME);

                        Cache countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);

    Cache statusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);


     // This HashMap contains the UUIDs of countries as keys and the country names as values
    HashMap<String, String> countryHash = new HashMap<String, String>();
    HashMap<String, String> countrycurrencyHash = new HashMap<String, String>();
    HashMap<String, String> accountHash = new HashMap<String, String>();
    HashMap<String, String> currencyHash = new HashMap<String, String>();
    HashMap<String, String> statusHash = new HashMap<String, String>();


    List keys;

    Element element;
    Country country;
    Account account;

    List<Account> accountList = new ArrayList<Account>();
    List<CollectionBalanceHistory> collectionbalanceList;
    List<CashWithdrawal> clientwithdrawrequestList;
    List<CashWithdrawal> clientwithdrawalhirequestList;



    keys = accountsCache.getKeysWithExpiryCheck();
    for (Object key : keys) {
        if ((element = accountsCache.get(key)) != null) {
            accountList.add((Account) element.getObjectValue());
        }
    }

    keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countryHash.put(country.getUuid(), country.getName());
    }

    keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countrycurrencyHash.put(country.getUuid(), country.getCurrency());
    }

     keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        currencyHash.put(country.getUuid(), country.getCurrencycode());
    }

    keys = accountsCache.getKeys();
    for (Object key : keys) {
        element = accountsCache.get(key);
        account = (Account) element.getObjectValue();
        accountHash.put(account.getUuid(), account.getFirstName());
    }

    TransactionStatus transactionStatus;
    keys = statusCache.getKeys();
    for(Object key : keys) {
    element = statusCache.get(key);
    transactionStatus = (TransactionStatus) element.getObjectValue();
    statusHash.put(transactionStatus.getUuid(), transactionStatus.getDescription());
    }

    int count = 0;  // A generic counter
    int incount = 0;  // Generic counter
    int incounts = 0;
    int incountss = 0;


    SessionStatistics statistics = new SessionStatistics();

     if ((element = statisticsCache.get(sessionUsername)) != null) {
                            statistics = (SessionStatistics) element.getObjectValue();
                        }


    CollectedBalancePaginator paginator = new CollectedBalancePaginator(sessionUsername);
    ClientWithdrawalPaginator paginators = new ClientWithdrawalPaginator(sessionUsername);
    ClientWithdrawalHistPaginator paginatorss = new ClientWithdrawalHistPaginator(sessionUsername);




    incount = statistics.getCollectionbalanceCountTotal();//to be adjusted
    //incount =8;
    CollectedBalancePage collectionbalancePage;
    int collectionbalanceCount = 0; // The current count of the Accounts sessions

    if (incount == 0) { 	// This user has no Incoming USSD in the account
        collectionbalancePage = new CollectedBalancePage();
        collectionbalanceList = new ArrayList<CollectionBalanceHistory>();
        collectionbalanceCount = 0;

    } else {
        collectionbalancePage = (CollectedBalancePage) session.getAttribute("currentcollectionbalancePage");
        String referrer = request.getHeader("referer");
        String pageParam = (String) request.getParameter("page");

        // We are to give the first page
        if (collectionbalancePage == null
                || !StringUtils.endsWith(referrer, "collectionbalance.jsp")
                || StringUtils.equalsIgnoreCase(pageParam, "first")) {
            collectionbalancePage = paginator.getFirstPage();

            // We are to give the last page
        } else if (StringUtils.equalsIgnoreCase(pageParam, "last")) {
            collectionbalancePage = paginator.getLastPage();

            // We are to give the previous page
        } else if (StringUtils.equalsIgnoreCase(pageParam, "previous")) {
            collectionbalancePage = paginator.getPrevPage(collectionbalancePage);

            // We are to give the next page
        } else {
            collectionbalancePage = paginator.getNextPage(collectionbalancePage);
        }

        session.setAttribute("currentcollectionbalancePage", collectionbalancePage);

        collectionbalanceList = collectionbalancePage.getContents();

        collectionbalanceCount = (collectionbalancePage.getPageNum() - 1) * collectionbalancePage.getPagesize() + 1;


    }



    incounts = statistics.getClientwithdrawalCountTotal();
    //incounts =2;
    ClientWithdrawalPage clientwithdrawrequestPage;
    int clientwithdrawrequestCount = 0; // The current count of the Accounts sessions

    if (incounts == 0) {     // This user has no Incoming USSD in the account
        clientwithdrawrequestPage = new ClientWithdrawalPage();
        clientwithdrawrequestList = new ArrayList<CashWithdrawal>();
        clientwithdrawrequestCount = 0;

    } else {
        clientwithdrawrequestPage = (ClientWithdrawalPage) session.getAttribute("currentclientwithdrawalrequestPage");
        String referrers = request.getHeader("referer");
        String pageParams = (String) request.getParameter("page");

        // We are to give the first page
        if (clientwithdrawrequestPage == null
                || !StringUtils.endsWith(referrers, "collectionbalance.jsp")
                || StringUtils.equalsIgnoreCase(pageParams, "first")) {
            clientwithdrawrequestPage = paginators.getFirstPage();

            // We are to give the last page
        } else if (StringUtils.equalsIgnoreCase(pageParams, "last")) {
            clientwithdrawrequestPage = paginators.getLastPage();

            // We are to give the previous page
        } else if (StringUtils.equalsIgnoreCase(pageParams, "previous")) {
            clientwithdrawrequestPage = paginators.getPrevPage(clientwithdrawrequestPage);

            // We are to give the next page
        } else {
            clientwithdrawrequestPage = paginators.getNextPage(clientwithdrawrequestPage);
        }

        session.setAttribute("currentclientwithdrawalrequestPage", clientwithdrawrequestPage);

        clientwithdrawrequestList = clientwithdrawrequestPage.getContents();

        clientwithdrawrequestCount = (clientwithdrawrequestPage.getPageNum() - 1) * clientwithdrawrequestPage.getPagesize() + 1;


    }



    incountss = statistics.getWithdrawalhistoryCountTotal();
    //incountss =4;
    ClientWithdrawalHistPage clientwithdrawalhistrequestPage;
    int clientwithdrawalhistrequestCount = 0; // The current count of the Accounts sessions

    if (incountss == 0) {     // This user has no Incoming USSD in the account
        clientwithdrawalhistrequestPage = new ClientWithdrawalHistPage();
        clientwithdrawalhirequestList = new ArrayList<CashWithdrawal>();
        clientwithdrawalhistrequestCount = 0;

    } else {
        clientwithdrawalhistrequestPage = (ClientWithdrawalHistPage) session.getAttribute("withdrawalhistoryrequestPage");
        String referrerss = request.getHeader("referer");
        String pageParamss = (String) request.getParameter("page");

        // We are to give the first page
        if (clientwithdrawalhistrequestPage == null
                || !StringUtils.endsWith(referrerss, "collectionbalance.jsp")
                || StringUtils.equalsIgnoreCase(pageParamss, "first")) {
            clientwithdrawalhistrequestPage = paginatorss.getFirstPage();

            // We are to give the last page
        } else if (StringUtils.equalsIgnoreCase(pageParamss, "last")) {
            clientwithdrawalhistrequestPage = paginatorss.getLastPage();

            // We are to give the previous page
        } else if (StringUtils.equalsIgnoreCase(pageParamss, "previous")) {
            clientwithdrawalhistrequestPage = paginatorss.getPrevPage(clientwithdrawalhistrequestPage);

            // We are to give the next page
        } else {
            clientwithdrawalhistrequestPage = paginatorss.getNextPage(clientwithdrawalhistrequestPage);
        }

        session.setAttribute("withdrawalhistoryrequestPage", clientwithdrawalhistrequestPage);

        clientwithdrawalhirequestList = clientwithdrawalhistrequestPage.getContents();

        clientwithdrawalhistrequestCount = (clientwithdrawalhistrequestPage.getPageNum() - 1) * clientwithdrawalhistrequestPage.getPagesize() + 1;


    }



%>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Collection Balance| </title>
    <link  rel="icon" type="image/png"  href="images/logo.jpg">

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet">
    <link href="css/floatexamples.css" rel="stylesheet" />

    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">


                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/img.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><%=sessionUsername%></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">

                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-area-chart"></i> Home <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="index.jsp">Dashboard</a>
                                        </li>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-gears"></i>Account Management<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="accountmanage.jsp">Account Settings</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-credit-card"></i>Track Balance<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="accountbalance.jsp">View Float</a>
                                        </li>
                                           <li><a href="accountbalancehistory.jsp">Float History</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-database"></i> Transactions <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="viewtrans.jsp">View Transactions</a>
                                        </li>
                                        <li><a href="search.jsp">Search Transactions</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-share"></i> Collection <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="collectionbalance.jsp">View Collection Float</a>
                                        </li>
                                        <li><a href="collectionbalancehistory.jsp">Collection FloatHistory </a>
                                        <li><a href="viewprocessedcollections.jsp">Collection Transactions </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                   <!-- <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>-->
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/img.png" alt=""><%= sessionUsername%>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <form name="logoutForm" method="post" action="logout">
                                            <p>
                                            <!--<input type="submit" class="btn btn-primary" name="logout" id="logout" value="Logout">-->
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-sign-out pull-right"></i> Log Out</button>

                                            </p>
                                        </form>

                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>


            <!-- page content -->
            <div class="right_col" role="main">

                <br />
                <div class="">

                    <div class="row top_tiles">
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <!--<div class="tile-stats">


                                <h3><i class="fa fa-dollar"></i>Current Main Float/Balance</h3>

                            </div>-->
                        </div>


                    <div class="row">



                        <div class="clearfix"></div>
                     <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_content">

                                    <div class="row">

                                            <div class="col-lg-6 col-xs-6">
                                                <form name="pageForm" method="post" action="collectionbalance.jsp">
					                    <%                                            if (!collectionbalancePage.isFirstPage()) {
					                    %>
					                    <input class="btn btn-info" type="submit" name="page" value="First" />
					                    <input class="btn btn-info" type="submit" name="page" value="Previous"/>

					                    <%
					                        }
					                    %>
					                    <span class="pageInfo">Page
					                        <span class="pagePosition currentPage"><%= collectionbalancePage.getPageNum()%></span> of
					                        <span class="pagePosition"><%= collectionbalancePage.getTotalPage()%></span>
					                    </span>
					                    <%
					                        if (!collectionbalancePage.isLastPage()) {
					                    %>
					                    <input class="btn btn-info" type="submit" name="page"value="Next">
					                    <input class="btn btn-info" type="submit"name="page" value="Last">
					                    <%
					                        }
					                    %>
					                </form>
                                                <!-- small box -->
                                                <div class="small-box">
                                                    <div class="inner">
                                                     <i class="icon-eye-open icon-large"></i>
                                                        <div class="alert alert-block alert-success">
                                                                    <p>
                                                                      CURRENT COLLECTED AMOUNTs BY CURRENCY
                                                                    </p>
                                                                    </div>

                                                    <div class="x_content">
                                                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                                                        <!--<table class="table table-striped bootstrap-datatable datatable">-->
                                                            <thead>
                                                                <tr class="headings">
                                                                <th></th>
                                                                <!--<th>Account</th>-->
								                                <th>Currency</th>
                                                                <th>Balance</th>
                                                                <th>Withdraw</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                 <%
                                                            if (collectionbalanceList != null) {
                                                                for (CollectionBalanceHistory code : collectionbalanceList) {
                                                                %>

                                                                <tr>
                                                                    <td width="5%"><%=collectionbalanceCount%></td>
								                                    <td class="center"><%=currencyHash.get(code.getCountryuuid())%></td>
                                                                    <td class="center"><%=code.getBalance()%></td>
                                                                    <form method="POST" action="clientwithdraw.jsp">
                                                                    <input class="btn btn-info btn-xs" type="hidden" name="collectionbalanceuuid" <%
                                                                    out.println("value=\"" + StringUtils.trimToEmpty(code.getUuid()) + "\"");
                                                                        %>>
                                                                    <input type="hidden"  name="username" <%
                                                                    out.println("value=\"" + StringUtils.trimToEmpty(sessionUsername) + "\"");
                                                                    %>>
                                                                    <td class="center"><input class="btn btn-success btn-xs" type="submit"name="page" value="withdraw"></td>
                                                                    </form>
                                                                </tr>
                                                                 <%
                                                                    collectionbalanceCount++;
                                                                }
                                                                }

                                                            %>


                                                            </tbody>
                                                        </table>
                                                     </div>



                                                    </div>
                                                </div>
                                            </div><!-- ./col -->

                                            <div class="col-lg-6 col-xs-6">
                                              <form name="pageForm" method="post" action="collectionbalance.jsp">
                                        <%                                            if (!clientwithdrawrequestPage.isFirstPage()) {
                                        %>
                                        <input class="btn btn-info" type="submit" name="page" value="First" />
                                        <input class="btn btn-info" type="submit" name="page" value="Previous"/>

                                        <%
                                            }
                                        %>
                                        <span class="pageInfo">Page
                                            <span class="pagePosition currentPage"><%= clientwithdrawrequestPage.getPageNum()%></span> of
                                            <span class="pagePosition"><%= clientwithdrawrequestPage.getTotalPage()%></span>
                                        </span>
                                        <%
                                            if (!clientwithdrawrequestPage.isLastPage()) {
                                        %>
                                        <input class="btn btn-info" type="submit" name="page"value="Next">
                                        <input class="btn btn-info" type="submit"name="page" value="Last">
                                        <%
                                            }
                                        %>
                                    </form>
                                                <!-- small box -->
                                                <div class="small-box">
                                                    <div class="inner">
                                                     <i class="icon-eye-open icon-large"></i>
                                                        <div class="alert alert-block alert-success">
                                                                    <p>
                                                                      WITHDRAWALS AWAITING APPROVAL
                                                                    </p>
                                                                    </div>


                                                    <div class="x_content">
                                                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                                                        <!--<table class="table table-striped bootstrap-datatable datatable">-->
                                                            <thead>
                                                                <tr class="headings">
                                                                <th></th>
                                                                <th>Currency</th>
                                                                <th>Amount</th>
                                                                <th>withdrawalCurrency</th>
                                                                <th>Date</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                 <%
                                                            if (clientwithdrawrequestList != null) {
                                                                for (CashWithdrawal code : clientwithdrawrequestList) {
                                                                %>

                                                                <tr>
                                                                    <td width="5%"><%=clientwithdrawrequestCount%></td>
                                                                    <td class="center"><%=countrycurrencyHash.get(code.getCurrencyUuid())%></td>
                                                                    <td class="center"><%=code.getAmount()%></td>
                                                                    <td class="center"><%=countrycurrencyHash.get(code.getTocurrencyUuid())%></td>
                                                                    <td class="center"><%=code.getTransactionDate()%></td>
                                                                </tr>
                                                                 <%
                                                                    clientwithdrawrequestCount++;
                                                                }
                                                                }

                                                            %>
                                                            </tbody>
                                                        </table>
                                                     </div>
                                                    </div>
                                                </div>

                                            </div><!-- ./col -->

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                                        <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_content">

                                    <div class="row">
                                      <div align="right">
                    										<form id="exportwithdrawToExcel" name="exportwithdrawExcelForm" method="post"
                    											action="exportwithdrawExcel" target="_blank">
                    											<!--<input class="btn btn-primary" type="submit" name="exportExcel" value="Export All" class="icon-paper-clip icon-large"  />-->
                    											<button class="btn btn-primary pull-right" value="Export All"
                    												style="margin-right: 5px;" type="submit" name="exportwithdrawExcel">
                    												<i class="fa fa-file-archive-o"></i>Export All
                    											</button>
                    											<!--<input class="btn btn-primary" type="submit" name="exportExcel" value="Export Page" class="icon-paper-clip icon-large"  />-->
                    											<button class="btn btn-primary pull-right"
                    												value="Export Page" style="margin-right: 5px;" type="submit"
                    												name="exportwithdrawExcel">
                    												<i class="fa fa-language"></i>Export Page
                    											</button>

                    											</p>
                    										</form>
                    									</div>

                                            <!--<div class="col-lg-6 col-xs-6">-->
                                                <form name="pageForm" method="post" action="collectionbalance.jsp">
                                        <%                                            if (!clientwithdrawalhistrequestPage.isFirstPage()) {
                                        %>
                                        <input class="btn btn-info" type="submit" name="page" value="First" />
                                        <input class="btn btn-info" type="submit" name="page" value="Previous"/>

                                        <%
                                            }
                                        %>
                                        <span class="pageInfo">Page
                                            <span class="pagePosition currentPage"><%= clientwithdrawalhistrequestPage.getPageNum()%></span> of
                                            <span class="pagePosition"><%= clientwithdrawalhistrequestPage.getTotalPage()%></span>
                                        </span>
                                        <%
                                            if (!clientwithdrawalhistrequestPage.isLastPage()) {
                                        %>
                                        <input class="btn btn-info" type="submit" name="page"value="Next">
                                        <input class="btn btn-info" type="submit"name="page" value="Last">
                                        <%
                                            }
                                        %>
                                    </form>
                                                <!-- small box -->
                                                <div class="small-box">
                                                    <div class="inner">
                                                     <i class="icon-eye-open icon-large"></i>
                                                        <div class="alert alert-block alert-success">
                                                                    <p>
                                                                      WITHDRAWALS
                                                                    </p>
                                                                    </div>


                                                    <div class="x_content">
                                                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                                                        <!--<table class="table table-striped bootstrap-datatable datatable">-->
                                                            <thead>
                                                                <tr class="headings">
                                                                <th></th>
                                                                <th>Currency</th>
                                                                <th>Amount</th>
                                                                <th>withdrawalCurrency</th>
                                                                <th>Comission Estimate</th>
                                                                <th>Bank Exchangerate</th>
                                                                <th>Transfer Charge</th>
                                                                <th>TransactionStatus</th>
                                                                <th>Receivable Amount</th>
                                                                <th>Date</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                 <%
                                                            if (clientwithdrawalhirequestList != null) {
                                                                for (CashWithdrawal code : clientwithdrawalhirequestList) {
                                                                %>

                                                                <tr>
                                                                    <td width="5%"><%=clientwithdrawalhistrequestCount%></td>
                                                                    <td class="center"><%=countrycurrencyHash.get(code.getCurrencyUuid())%></td>
                                                                    <td class="center"><%=code.getAmount()%></td>
                                                                    <td class="center"><%=countrycurrencyHash.get(code.getTocurrencyUuid())%></td>
                                                                    <td class="center"><%=code.getComission()%></td>
                                                                    <td class="center"><%=code.getBankwithdrawexchangerate()%></td>
                                                                    <td class="center"><%=code.getTransfercharges()+" "+currencyHash.get(code.getTocurrencyUuid())%></td>
                                                                    <td class="center"><%=statusHash.get(code.getTransactionStatusUuid())%></td>
                                                                    <td class="center"><%=code.getReceivableamount()+" "+currencyHash.get(code.getTocurrencyUuid())%></td>
                                                                    <td class="center"><%=code.getTransactionDate()%></td>

                                                                </tr>
                                                                 <%
                                                                    clientwithdrawalhistrequestCount++;
                                                                }
                                                                }

                                                            %>
                                                            </tbody>
                                                        </table>
                                                     </div>
                                                    </div>
                                                </div>
                                            <!--</div>--><!-- ./col -->
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
            </div>


                </div>

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p>Copyright@ImpalaPay 2014-2015</p>
                            <a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy
                            Policy</a> | ImpalaPay Ltd <%= Calendar.getInstance().get(Calendar.YEAR)%>. All rights reserved.
                            <!--<img id="madeInKenya" alt="Made in Kenya" src="#" width="100" height="100" />-->
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>

                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>

    </div>

    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>

    <script src="js/custom.js"></script>

</body>

</html>

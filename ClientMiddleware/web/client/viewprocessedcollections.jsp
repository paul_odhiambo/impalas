<!DOCTYPE html>
<%--
  Copyright (c) impalapay Ltd., June 23, 2014

  @author eugene chimita, eugenechimita@impalapay.com
--%>
<%@ page
	import="com.impalapay.collection.accountmgmt.pagination.transaction.ProcessedCollectionPage"%>
<%@ page
	import="com.impalapay.collection.accountmgmt.pagination.transaction.ProcessedCollectionPaginator"%>
<%@ page
	import="com.impalapay.airtel.beans.transaction.TransactionStatus"%>
<%@ page import="com.impalapay.collection.beans.incoming.ProcessedCollection"%>
<%@ page import="com.impalapay.collection.beans.network.CollectionNetwork"%>
<%@ page import="com.impalapay.collection.beans.network.CollectionType"%>
<%@ page import="com.impalapay.airtel.beans.accountmgmt.Account"%>
<%@ page import="com.impalapay.airtel.beans.geolocation.Country"%>
<%@ page
	import="com.impalapay.airtel.accountmgmt.session.SessionStatistics"%>
<%@ page
	import="com.impalapay.airtel.accountmgmt.session.SessionConstants"%>

<%@ page import="com.impalapay.airtel.cache.CacheVariables"%>
<%@ page import="org.apache.commons.lang3.StringUtils"%>

<%@ page import="net.sf.ehcache.Cache"%>
<%@ page import="net.sf.ehcache.CacheManager"%>
<%@ page import="net.sf.ehcache.Element"%>
<%@ page import="java.text.DecimalFormat"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.HashMap"%>

<%
                     // The following is for session management.
                        if (session == null) {
                            response.sendRedirect("../client");
                        }

                        String sessionUsername = (String) session.getAttribute(SessionConstants.ACCOUNT_SIGN_IN_KEY);

                        if (StringUtils.isEmpty(sessionUsername)) {
                            response.sendRedirect("index.jsp");
                        }

                        session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
                        response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=logout");
                        // End of session management code

                        CacheManager mgr = CacheManager.getInstance();
                        Cache accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

                        Cache statisticsCache = mgr.getCache(CacheVariables.CACHE_STATISTICS_BY_USERNAME);

                        Cache countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);

                        Cache statusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);

                        Cache collectionnetworkCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_NETWORK);

                        Cache collectiontypeCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_CHANNEL);



                        //These HashMaps contains the UUIDs of Countries as keys and the country code of countries as values
                        HashMap<String, String> countryHash = new HashMap<String, String>();

                        HashMap<String, String> currencyHash = new HashMap<String, String>();


                        HashMap<String, String> statusHash = new HashMap<String, String>();

                       HashMap<String, String> collectionetworkHash = new HashMap<String, String>();

                       HashMap<String, String> collectiontypeHash = new HashMap<String, String>();




                        Element element;
                        int count = 0; //generic counter

                        Account account = new Account();
                        SessionStatistics statistics = new SessionStatistics();


                        if ((element = accountsCache.get(sessionUsername)) != null) {
                            account = (Account) element.getObjectValue();
                        }

                        List keys;
                        //fetch from cache
                        Country country;
                        keys = countryCache.getKeys();
                            for(Object key : keys) {
                            element = countryCache.get(key);
                            country = (Country) element.getObjectValue();
                            countryHash.put(country.getUuid(), country.getCountrycode());
                            }

                        keys = countryCache.getKeys();
                            for(Object key : keys) {
                            element = countryCache.get(key);
                            country = (Country) element.getObjectValue();
                            currencyHash.put(country.getUuid(), country.getCurrencycode());
                            }

                        TransactionStatus transactionStatus;
                        keys = statusCache.getKeys();
                            for(Object key : keys) {
                            element = statusCache.get(key);
                            transactionStatus = (TransactionStatus) element.getObjectValue();
                            statusHash.put(transactionStatus.getUuid(), transactionStatus.getDescription());
                            }

                         CollectionNetwork collectionnetwork;
                         keys = collectionnetworkCache.getKeys();
                            for(Object key : keys) {
                            element = collectionnetworkCache.get(key);
                            collectionnetwork = (CollectionNetwork) element.getObjectValue();
                            collectionetworkHash.put(collectionnetwork.getUuid(), collectionnetwork.getNetworkname());
                            }


                         CollectionType collectiontype;
                          keys = collectiontypeCache.getKeys();
                            for(Object key : keys) {
                            element = collectiontypeCache.get(key);
                            collectiontype = (CollectionType) element.getObjectValue();
                            collectiontypeHash.put(collectiontype.getUuid(), collectiontype.getType());
                            }




                        if ((element = statisticsCache.get(sessionUsername)) != null) {
                            statistics = (SessionStatistics) element.getObjectValue();
                        }

                        count = statistics.getCollectionprocessedtransCountTotal();



                        ProcessedCollectionPage processedcollectionPage;
                        List<ProcessedCollection> processedcollectionList;

                        int processedcollectionCount; // The current count of transactions
                        ProcessedCollectionPaginator paginator = new ProcessedCollectionPaginator(sessionUsername);

                        if (count == 0) { // This user has no transactions in his/her account
                            processedcollectionPage = new ProcessedCollectionPage();
                            processedcollectionList = new ArrayList<ProcessedCollection>();
                            processedcollectionCount = 0;

                        } else {
                            processedcollectionPage = (ProcessedCollectionPage) session.getAttribute("currentProcessedcollectionPage");
                            String referrer = request.getHeader("referer");
                            String pageParam = (String) request.getParameter("page");

                            // We are to give the first page
                            if (processedcollectionPage == null
                                    || !StringUtils.endsWith(referrer, "viewprocessedcollections.jsp")
                                    || StringUtils.equalsIgnoreCase(pageParam, "first")) {
                                processedcollectionPage = paginator.getFirstPage();

                                // We are to give the last page
                            } else if (StringUtils.equalsIgnoreCase(pageParam, "last")) {
                                processedcollectionPage = paginator.getLastPage();


                                // We are to give the previous page
                            } else if (StringUtils.equalsIgnoreCase(pageParam, "previous")) {
                                processedcollectionPage = paginator.getPrevPage(processedcollectionPage);

                                // We are to give the next page
                            } else {
                                processedcollectionPage= paginator.getNextPage(processedcollectionPage);
                            }

                            session.setAttribute("currentProcessedcollectionPage", processedcollectionPage);

                            processedcollectionList = processedcollectionPage.getContents();
                            processedcollectionCount = (processedcollectionPage.getPageNum() - 1) * processedcollectionPage.getPagesize() + 1;
                        }
                    %>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>ImpalaPay|View Transactions</title>
<link rel="icon" type="image/png" href="images/logo.jpg">

<!-- Bootstrap core CSS -->

<link href="css/bootstrap.min.css" rel="stylesheet">

<link href="fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">

<!-- Custom styling plus plugins -->
<link href="css/custom.css" rel="stylesheet">
<link href="css/icheck/flat/green.css" rel="stylesheet">
<link rel="stylesheet" href="../dist/tablesaw.css">

<script src="../dist/dependencies/jquery.js"></script>
<script src="../dist/tablesaw.js"></script>
<script src="../dist/tablesaw-init.js"></script>


<script src="js/jquery.min.js"></script>

<!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

	<div class="container body">


		<div class="main_container">

			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">


					<div class="clearfix"></div>

					<!-- menu prile quick info -->
					<div class="profile">
						<div class="profile_pic">
							<img src="images/img.png" alt="..."
								class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<span>Welcome,</span>
							<h2><%=sessionUsername%></h2>
						</div>
					</div>
					<!-- /menu prile quick info -->

					<br />

					<!-- sidebar menu -->
					<div id="sidebar-menu"
						class="main_menu_side hidden-print main_menu">

						<div class="menu_section">

							<ul class="nav side-menu">
								<li><a><i class="fa fa-area-chart"></i> Home <span
										class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu" style="display: none">
										<li><a href="index.jsp">Dashboard</a></li></li>
							</ul>
							</li>
							<li><a><i class="fa fa-gears"></i>Account Management<span
									class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">
									<li><a href="accountmanage.jsp">Account Settings</a></li>
								</ul></li>
							<li><a><i class="fa fa-credit-card"></i>Track Balance<span
									class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">
									<li><a href="accountbalance.jsp">View Float</a></li>
									<li><a href="accountbalancehistory.jsp">Float History</a>
									</li>
								</ul></li>
							<li><a><i class="fa fa-database"></i> Transactions <span
									class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">
									<li><a href="viewtrans.jsp">View Transactions</a></li>
									<li><a href="search.jsp">Search Transactions</a></li>
								</ul></li>
								<li><a><i class="fa fa-share"></i> Collection <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="collectionbalance.jsp">View Collection Float</a>
                                        </li>
                                        <li><a href="collectionbalancehistory.jsp">Collection FloatHistory </a>
                                        <li><a href="viewprocessedcollections.jsp">Collection Transactions </a>
                                        </li>
                                    </ul>
                                </li>
							</ul>
						</div>

					</div>
					<!-- /sidebar menu -->

					<!-- /menu footer buttons -->
					<!-- <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>-->
					<!-- /menu footer buttons -->
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">

				<div class="nav_menu">
					<nav class="" role="navigation">
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>

						<ul class="nav navbar-nav navbar-right">
							<li class=""><a href="javascript:;"
								class="user-profile dropdown-toggle" data-toggle="dropdown"
								aria-expanded="false"> <img src="images/img.png" alt=""><%= sessionUsername%>
									<span class=" fa fa-angle-down"></span>
							</a>
								<ul
									class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
									<li>
										<form name="logoutForm" method="post" action="logout">
											<p>
												<!--<input type="submit" class="btn btn-primary" name="logout" id="logout" value="Logout">-->
												<button type="submit" class="btn btn-primary">
													<i class="fa fa-sign-out pull-right"></i> Log Out
												</button>

											</p>
										</form>

									</li>
								</ul></li>
						</ul>
					</nav>
				</div>

			</div>

			<!-- /top navigation -->

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="clearfix"></div>


					<div class="row">



						<div class="clearfix"></div>

						<div class="span16">
							<div class="x_panel">
								<div class="row-fluid">
								
									<div align="right">
										<form id="exportcollectionToExcel" name="exportcollectionExcelForm" method="post"
											action="exportcollectionExcel" target="_blank">
											<!--<input class="btn btn-primary" type="submit" name="exportExcel" value="Export All" class="icon-paper-clip icon-large"  />-->
											<button class="btn btn-primary pull-right" value="Export All"
												style="margin-right: 5px;" type="submit" name="exportcollectionExcel">
												<i class="fa fa-file-archive-o"></i>Export All
											</button>
											<!--<input class="btn btn-primary" type="submit" name="exportExcel" value="Export Page" class="icon-paper-clip icon-large"  />-->
											<button class="btn btn-primary pull-right"
												value="Export Page" style="margin-right: 5px;" type="submit"
												name="exportcollectionExcel">
												<i class="fa fa-language"></i>Export Page
											</button>

											</p>
										</form>
									</div>
									<div align="right">
										<div id="pagination">
											<form name="pageForm" method="post" action="viewprocessedcollections.jsp">
												<%
                                if (!processedcollectionPage.isFirstPage()) {
                                                                %>
												<input class="btn btn-primary" type="submit" name="page"
													value="First" /> <input class="btn btn-primary"
													type="submit" name="page" value="Previous" />
												<%                                    }
                                                                %>
												<span class="pageInfo">Page <span
													class="pagePosition currentPage"><%= processedcollectionPage.getPageNum()%></span>
													of <span class="pagePosition"><%= processedcollectionPage.getTotalPage()%></span>
												</span>
												<%
                                if (!processedcollectionPage.isLastPage()) {
                                                                %>
												<input class="btn btn-primary" type="submit" name="page"
													value="Next"> <input class="btn btn-primary"
													type="submit" name="page" value="Last">
												<%
                                }
                                                                %>
											</form>
										</div>

										<div class="x_content">
											<!--<table id="example" class="table table-striped responsive-utilities jambo_table">-->
											<table class="tablesaw" data-tablesaw-mode="swipe"
												data-tablesaw-sortable data-tablesaw-sortable-switch
												data-tablesaw-minimap data-tablesaw-mode-switch>
												<!--<table class="table table-striped bootstrap-datatable datatable">-->
												<thead>
													<tr class="headings">
														<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="1">&nbsp;</th>
														<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="2">Network</th>
														<!--<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="3">Transaction_Type</th>-->
														<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="4">Debitor_Name</th>
														<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="5">Debit_Currency</th>
														<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="6">Debit_Country</th>
														<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="7">Amount</th>
														<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="8">Reference</th>
														<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="9">Status</th>
														<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="10">Debited_Account</th>
														<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="11">Originate_Uuid</th>
														<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="12">Receiver_Uuid</th>
														<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="13">Originate_Amount</th>
															<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="14">Originate_Currency</th>
														<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="15">Statusdescription</th>
														<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="16">Commission</th>
														<th scope="col" data-tablesaw-sortable-col
															data-tablesaw-priority="17">Servertime</th>
														<!--<th>View</th>-->
													</tr>
												</thead>

												<tbody>

													<%
                                                            if (processedcollectionList != null) {
                                                                for (ProcessedCollection codes : processedcollectionList) {
                                                                %>

													<tr class='even pointer'>
														<td><%=processedcollectionCount%></td>
														<td class="center"><%=collectionetworkHash.get(codes.getNetworkuuid())%></td>
														<!--<td class="center"><%=collectiontypeHash.get(codes.getTransactiontype())%></td>-->
														<td class="center"><%=codes.getDebitorname()%></td>
														<td class="center"><%=currencyHash.get(codes.getDebitcurrency())%></td>
														<td class="center"><%=countryHash.get(codes.getDebitcountry())%></td>
														<td class="center"><%=codes.getAmount()%></td>
														<td class="center"><%=codes.getAccountreference()%></td>
														<td class="center"><%=statusHash.get(codes.getTransactionstatusuuid())%></td>
														<td class="center"><%=codes.getDebitedaccount()%></td>
														<td class="center"><%=codes.getOriginatetransactionuuid()%></td>
														<td class="center"><%=codes.getReceivertransactionuuid()%></td>
														<td class="center"><%=codes.getOriginateamount()%></td>
														<td class="center"><%=codes.getOriginatecurrency()%></td>
														<td class="center"><%=codes.getEndpointstatusdescription()%></td>
														<td class="center"><%=codes.getCommission()%></td>
														<td class="center"><%=codes.getServertime()%></td>

														<!--<form method="POST" action="viewResults.jsp">
                                                                    <input class="btn btn-info btn-xs" type="hidden" name="uuid" <%
                                      				    out.println("value=\"" + StringUtils.trimToEmpty(codes.getUuid()) + "\"");
                                        		            %>>
                                                                    <td class="center"><input class="btn btn-success btn-xs" type="submit"name="page" 									    value="view"></td>
								    </form>-->
													</tr>
													<%
                                                                    processedcollectionCount++;
                                                                }
                                                                }

                                                            %>
												</tbody>

											</table>
										</div>




									</div>
								</div>
							</div>
						</div>

					</div>

					<!-- footer content -->
					<footer>
						<div class="">
							<p>Copyright@ImpalaPay 2014-2015</p>
							<a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy
								Policy</a> | ImpalaPay Ltd
							<%= Calendar.getInstance().get(Calendar.YEAR)%>. All rights
							reserved.
							<!--<img id="madeInKenya" alt="Made in Kenya" src="#" width="100" height="100" />-->
							</p>
						</div>
						<div class="clearfix"></div>
					</footer>

					<!-- /footer content -->

				</div>
				<!-- /page content -->
			</div>

		</div>

		<script src="js/bootstrap.min.js"></script>

		<!-- chart js -->
		<script src="js/chartjs/chart.min.js"></script>
		<!-- bootstrap progress js -->
		<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
		<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
		<!-- icheck -->
		<script src="js/icheck/icheck.min.js"></script>

		<script src="js/custom.js"></script>
</body>

</html>

<!DOCTYPE html>
<%-- 
  Copyright (c) impalapay Ltd., June 23, 2014
  
  @author eugene chimita, eugenechimita@impalapay.com
--%>
<%@page
    import="com.impalapay.airtel.accountmgmt.pagination.TransactionPage"%>
    <%@page
        import="com.impalapay.airtel.accountmgmt.pagination.TransactionPaginator"%>

        <%@page
            import="com.impalapay.airtel.beans.transaction.TransactionStatus"%>
            <%@page import="com.impalapay.airtel.beans.transaction.Transaction"%>

            <%@page import="com.impalapay.airtel.beans.accountmgmt.Account"%>

            <%@page import="com.impalapay.airtel.beans.geolocation.Country"%>

            <%@page
                import="com.impalapay.airtel.accountmgmt.session.SessionStatistics"%>
                <%@page
                    import="com.impalapay.airtel.accountmgmt.session.SessionConstants"%>

                    <%@page import="com.impalapay.airtel.cache.CacheVariables"%>

                    <%@page import="org.apache.commons.lang3.StringUtils"%>

                    <%@page import="net.sf.ehcache.Cache"%>
                    <%@page import="net.sf.ehcache.CacheManager"%>
                    <%@page import="net.sf.ehcache.Element"%>

                    <%@page import="java.text.DecimalFormat"%>

                    <%@page import="java.net.URLEncoder"%>

                    <%@page import="java.util.Map"%>
                    <%@page import="java.util.Iterator"%>
                    <%@page import="java.util.ArrayList"%>
                    <%@page import="java.util.Calendar"%>
                    <%@page import="java.util.List"%>
                    <%@page import="java.util.HashMap"%>

                    <%
                     // The following is for session management.    
                        if (session == null) {
                            response.sendRedirect("../client");
                        }

                        String sessionUsername = (String) session.getAttribute(SessionConstants.ACCOUNT_SIGN_IN_KEY);

                        if (StringUtils.isEmpty(sessionUsername)) {
                            response.sendRedirect("index.jsp");
                        }

                        session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
                        response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=logout");
                        // End of session management code

                        CacheManager mgr = CacheManager.getInstance();
                        Cache accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

                        Cache statisticsCache = mgr.getCache(CacheVariables.CACHE_STATISTICS_BY_USERNAME);
                        
            Cache countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
                        
                        Cache statusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);

                        //These HashMaps contains the UUIDs of Countries as keys and the country code of countries as values
                HashMap<String, String> countryHash = new HashMap<String, String>();
                        
                        HashMap<String, String> statusHash = new HashMap<String, String>();
            
            

                        Account account = new Account();
                        SessionStatistics statistics = new SessionStatistics();
    
                        int count = 0; //generic counter
                        Element element;

                        if ((element = accountsCache.get(sessionUsername)) != null) {
                            account = (Account) element.getObjectValue();
                        }
                        
            List keys;
            //fetch from cache
            Country country;
            keys = countryCache.getKeys();
                for(Object key : keys) {
                element = countryCache.get(key);
                country = (Country) element.getObjectValue();
                countryHash.put(country.getUuid(), country.getCountrycode());           
                }
                        
                        TransactionStatus transactionStatus;
            keys = statusCache.getKeys();
                for(Object key : keys) {
                element = statusCache.get(key);
                transactionStatus = (TransactionStatus) element.getObjectValue();
                statusHash.put(transactionStatus.getUuid(), transactionStatus.getDescription());            
                }
                        

                        if ((element = statisticsCache.get(sessionUsername)) != null) {
                            statistics = (SessionStatistics) element.getObjectValue();
                        }

   

                        count = statistics.getTransactionCountTotal();
                       //count = 2;

 
  
                        TransactionPage transactionPage;
                        List<Transaction> transactionList;

                        int transactionCount; // The current count of transactions
                        TransactionPaginator paginator = new TransactionPaginator(sessionUsername);

                        if (count == 0) { // This user has no transactions in his/her account
                            transactionPage = new TransactionPage();
                            transactionList = new ArrayList<Transaction>();
                            transactionCount = 0;

                        } else {
                            transactionPage = (TransactionPage) session.getAttribute("currentTransactionPage");
                            String referrer = request.getHeader("referer");
                            String pageParam = (String) request.getParameter("page");

                            // We are to give the first page
                            if (transactionPage == null
                                    || !StringUtils.endsWith(referrer, "viewtrans.jsp")
                                    || StringUtils.equalsIgnoreCase(pageParam, "first")) {
                                transactionPage = paginator.getFirstPage();

                                // We are to give the last page
                            } else if (StringUtils.equalsIgnoreCase(pageParam, "last")) {
                                transactionPage = paginator.getLastPage();


                                // We are to give the previous page
                            } else if (StringUtils.equalsIgnoreCase(pageParam, "previous")) {
                                transactionPage = paginator.getPrevPage(transactionPage);

                                // We are to give the next page 
                            } else {
                                transactionPage= paginator.getNextPage(transactionPage);
                            }

                            session.setAttribute("currentTransactionPage", transactionPage);

                            transactionList = transactionPage.getContents();
                            transactionCount = (transactionPage.getPageNum() - 1) * transactionPage.getPagesize() + 1;
                        }
                    %>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentallela Alela! | </title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">


    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentellela Alela!</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/img.jpg" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><%=sessionUsername%></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="index.html">Dashboard</a>
                                        </li>
                                        <li><a href="index2.html">Dashboard2</a>
                                        </li>
                                        <li><a href="index3.html">Dashboard3</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-edit"></i> Forms <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="form.html">General Form</a>
                                        </li>
                                        <li><a href="form_advanced.html">Advanced Components</a>
                                        </li>
                                        <li><a href="form_validation.html">Form Validation</a>
                                        </li>
                                        <li><a href="form_wizards.html">Form Wizard</a>
                                        </li>
                                        <li><a href="form_upload.html">Form Upload</a>
                                        </li>
                                        <li><a href="form_buttons.html">Form Buttons</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-desktop"></i> UI Elements <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="general_elements.html">General Elements</a>
                                        </li>
                                        <li><a href="media_gallery.html">Media Gallery</a>
                                        </li>
                                        <li><a href="typography.html">Typography</a>
                                        </li>
                                        <li><a href="icons.html">Icons</a>
                                        </li>
                                        <li><a href="glyphicons.html">Glyphicons</a>
                                        </li>
                                        <li><a href="widgets.html">Widgets</a>
                                        </li>
                                        <li><a href="invoice.html">Invoice</a>
                                        </li>
                                        <li><a href="inbox.html">Inbox</a>
                                        </li>
                                        <li><a href="calender.html">Calender</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-table"></i> Tables <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="alltrans.jsp">Tables</a>
                                        </li>
                                        <li><a href="tables_dynamic.html">Table Dynamic</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="chartjs.html">Chart JS</a>
                                        </li>
                                        <li><a href="chartjs2.html">Chart JS2</a>
                                        </li>
                                        <li><a href="morisjs.html">Moris JS</a>
                                        </li>
                                        <li><a href="echarts.html">ECharts </a>
                                        </li>
                                        <li><a href="other_charts.html">Other Charts </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="menu_section">
                            <h3>Live On</h3>
                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="e_commerce.html">E-commerce</a>
                                        </li>
                                        <li><a href="projects.html">Projects</a>
                                        </li>
                                        <li><a href="project_detail.html">Project Detail</a>
                                        </li>
                                        <li><a href="contacts.html">Contacts</a>
                                        </li>
                                        <li><a href="profile.html">Profile</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="page_404.html">404 Error</a>
                                        </li>
                                        <li><a href="page_500.html">500 Error</a>
                                        </li>
                                        <li><a href="plain_page.html">Plain Page</a>
                                        </li>
                                        <li><a href="login.html">Login Page</a>
                                        </li>
                                        <li><a href="pricing_tables.html">Pricing Tables</a>
                                        </li>

                                    </ul>
                                </li>
                                <li><a><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/img.jpg" alt="">John Doe
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li><a href="javascript:;">  Profile</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="badge bg-red pull-right">50%</span>
                                            <span>Settings</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">Help</a>
                                    </li>
                                    <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>

                            <li role="presentation" class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge bg-green">6</span>
                                </a>
                                <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                                    <li>
                                        <a>
                                            <span class="image">
                                        <img src="images/img.jpg" alt="Profile Image" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            <span class="image">
                                        <img src="images/img.jpg" alt="Profile Image" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            <span class="image">
                                        <img src="images/img.jpg" alt="Profile Image" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            <span class="image">
                                        <img src="images/img.jpg" alt="Profile Image" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <div class="text-center">
                                            <a>
                                                <strong>See All Alerts</strong>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>
                    Tables
                    <small>
                        Some examples to get you started
                    </small>
                </h3>
                        </div>

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>


                    <div class="row">



                        <div class="clearfix"></div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Table design <small>Custom design</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="x_content">

                                    <p>Add class <code>bulk_action</code> to table for bulk actions options on row select</p>

                                                                                <div class="box-content">
                                                <!--<table class="table table-bordered table-striped table-condensed">-->
                                                <table class="table table-striped bootstrap-datatable datatable">

                            <div class="alert alert-info">
                                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                        <strong><i class="icon-user icon-large"></i>&nbsp;Transactions Table</strong>
                                                    </div>

                                                    <div class="row-fluid">
                                                        <div class="pull-left">
<!--
                                                            <a href="" class="btn btn-primary">Refresh<i class="icon-refresh icon-large"></i>
                                                            </a>-->
                                                          <form id="exportToPDF" name="exportPDFForm" method="post" action="exportPDF" target="_blank">
                                                <p>
                                              <input class="btn btn-primary" type="submit" name="exportPDF" value="Export Summary To PDF" />
                                  </p>
                                                  </form>
                                                        </div>
                                                  <!--<div id="pagination" class="pull-left"></div>
                                                        <div align="right">
                                                             <form id="exportToPDF" name="exportPDFForm" method="post" action="exportPDF" target="_blank">
                                                <p>
                                              <input class="btn btn-primary" type="submit" name="exportPDF" value="Export Summary To PDF" />
                                  </p>
                                                  </form>
                                                           
                                                        </div>
                                                      </div>-->
                                                    <div class="row-fluid">
                                                     <div align="right">
                                                      <form id="exportToExcel" name="exportExcelForm" method="post" action="exportExcel" target="_blank" >
                               <input class="btn btn-primary" type="submit" name="exportExcel" value="Export All" class="icon-paper-clip icon-large"  />
                                                      <input class="btn btn-primary" type="submit" name="exportExcel" value="Export Page" class="icon-paper-clip icon-large"  />
                            
                                                </p>
                                                </form>
                                                        </div>
                                                      <div align="right">
                                                        <div id="pagination">
                                                            <form name="pageForm" method="post" action="viewtrans.jsp">
                                                                <%
                                if (!transactionPage.isFirstPage()) {
                                                                %>
                                                                <input class="btn btn-primary" type="submit" name="page"
                                                                       value="First" /> <input class="btn btn-primary" type="submit"
                                                                       name="page" value="Previous" />
                                                                <%                                    }
                                                                %>
                                                                <span class="pageInfo">Page <span
                                                                        class="pagePosition currentPage"><%= transactionPage.getPageNum()%></span>
                                                                    of <span class="pagePosition"><%= transactionPage.getTotalPage()%></span>
                                                                </span>
                                                                <%
                                if (!transactionPage.isLastPage()) {
                                                                %>
                                                                <input class="btn btn-primary" type="submit" name="page"
                                                                       value="Next"> <input class="btn btn-primary" type="submit"
                                                                       name="page" value="Last">
                                                                <%                                    
                                }
                                                                %>
                                                            </form>
                                                        </div>
                                                     </div>
                                                    </div>
                                                    <thead>
                                                        <tr class="alert alert-success">
                                                            <th>&nbsp;</th>
                                                            <th>transaction Uuid</th>
                                                            <!--<th>Status</th>-->
                                                            <th>Source country</th>
                                                            <th>Sender name</th>
                                                            <th>Recipient phone</th>
                                                            <th>Amount</th>
                                                            <th>Recipient Country</th>
                                                            <!--<th>Sender Token</th>-->
                                                            <th>Remit Status</th>
                                                            <!--<th>Server Time</th>-->
                                                            <th>Client Time</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <%
                            count = transactionList.size();


                            //out.println(count);
                            for (int j = 0; j < count; j++) {
                                out.println("<tr class='alert alert-info'>");
                                out.println("<td>" + transactionCount + "</td>");
                                out.println("<td>" + transactionList.get(j).getUuid() + "</td>");
                                //out.println("<td>" + statusHash.get(transactionList.get(j).getTransactionStatusUuid()) + "</td>");
                                out.println("<td>" + transactionList.get(j).getSourceCountrycode() + "</td>");
                                out.println("<td>" + transactionList.get(j).getSenderName() + "</td>");
                                out.println("<td>" + transactionList.get(j).getRecipientMobile() + "</td>");
                                out.println("<td>" + transactionList.get(j).getAmount() + "</td>");
                                out.println("<td>" + countryHash.get(transactionList.get(j).getRecipientCountryUuid()) + "</td>");
                                //out.println("<td>" + transactionList.get(j).getSenderToken() + "</td>");
                                //out.println("<td>" + transactionList.get(j).getClientTime() + "</td>");
                                out.println("<td>" + statusHash.get(transactionList.get(j).getTransactionStatusUuid()) + "</td>");
                                out.println("<td>" + transactionList.get(j).getServerTime() + "</td>");
                                out.println("</tr>");
                                transactionCount++;
                            }
                                                        %>





                                                    </tbody>
                                                </table>
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>

    <script src="js/custom.js"></script>

</body>

</html>
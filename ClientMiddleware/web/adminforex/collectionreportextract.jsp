<!DOCTYPE html>
<%--
  Copyright (c) impalapay Ltd., June 23, 2014

  @author eugene chimita, eugenechimita@impalapay.com
--%>
<%@ page import="com.impalapay.mno.accountmgmt.admin.pagination.floatbynetwork.MainfloatPerNetworkPage"%>
<%@ page import="com.impalapay.mno.accountmgmt.admin.pagination.floatbynetwork.MainfloatPerNetworkPaginator"%>
<%@ page import="com.impalapay.mno.beans.accountmgmt.balance.NetworkBalance"%>
<%@ page import="com.impalapay.mno.accountmgmt.admin.pagination.floatbynetworkhistory.MainfloatPerCountryHistPage"%>
<%@ page import="com.impalapay.mno.accountmgmt.admin.pagination.floatbynetworkhistory.MainfloatPerCountryHistPaginator"%>
<%@ page import="com.impalapay.mno.beans.accountmgmt.balance.NetworkPurchaseHistory"%>
<%@ page import="com.impalapay.collection.beans.network.CollectionNetwork"%>
<%@ page import="com.impalapay.airtel.beans.accountmgmt.Account" %>
<%@ page import="com.impalapay.airtel.beans.geolocation.Country" %>
<%@ page import="com.impalapay.airtel.beans.transaction.TransactionStatus"%>


<%@ page import="com.impalapay.airtel.beans.accountmgmt.AccountStatus" %>
<%@ page import="com.impalapay.airtel.accountmgmt.admin.SessionConstants" %>
<%@ page import="com.impalapay.airtel.accountmgmt.session.SessionStatistics"%>
<%@ page import="com.impalapay.airtel.persistence.accountmgmt.AccountStatusDAO" %>
<%@ page import="com.impalapay.airtel.cache.CacheVariables"%>

<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collections"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Iterator"%>

<%@ page import="net.sf.ehcache.Cache" %>
<%@ page import="net.sf.ehcache.CacheManager" %>
<%@ page import="net.sf.ehcache.Element"%>

<%
    // The following is for session management.
    if (session == null) {
        response.sendRedirect("../index.jsp");
    }

    String sessionKey = (String) session.getAttribute(SessionConstants.ADMIN_SESSION_KEY);

    if (StringUtils.isEmpty(sessionKey)) {
        response.sendRedirect("../index.jsp");
    }

    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=logout");
    // End of session management code




    CacheManager mgr = CacheManager.getInstance();
    Cache accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_UUID);
    Cache countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
    Cache networkCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_NETWORK);
    Cache transactionstatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
    Cache statusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);



    // This HashMap contains the UUIDs of countries as keys and the country names as values
    HashMap<String, String> countryHash = new HashMap<String, String>();
    HashMap<String, String> countrycurrencyHash = new HashMap<String, String>();
    HashMap<String, String> networkHash = new HashMap<String, String>();
    HashMap<String, String> networkcountryHash = new HashMap<String, String>();
    HashMap<String, String> accountHash = new HashMap<String, String>();
    HashMap<String, String> statusHash = new HashMap<String, String>();
    HashMap<String, String> transactionstatusHash = new HashMap<String, String>();



    Cache statisticsCache = mgr.getCache(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS);

    List<TransactionStatus> transactionstatusList = new ArrayList<TransactionStatus>();


    List keys;

    Element element;
    Country country;
    CollectionNetwork network;
    Account account;
    TransactionStatus transactionStatus;
    


    List<NetworkBalance> networkbalanceList;
    List<NetworkPurchaseHistory> networkpurchasehistoryList;

    keys = networkCache.getKeys();
    for (Object key : keys) {
        element = networkCache.get(key);
        network = (CollectionNetwork) element.getObjectValue();
        networkHash.put(network.getUuid(), network.getNetworkname());
    }

    keys = networkCache.getKeys();
    for (Object key : keys) {
        element = networkCache.get(key);
        network = (CollectionNetwork) element.getObjectValue();
        networkcountryHash.put(network.getUuid(), network.getCountryuuid());
    }

    keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countryHash.put(country.getUuid(), country.getName());
    }

    keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countrycurrencyHash.put(country.getUuid(), country.getCurrency());
    }

    keys = accountsCache.getKeys();
    for (Object key : keys) {
        element = accountsCache.get(key);
        account = (Account) element.getObjectValue();
        accountHash.put(account.getUuid(), account.getFirstName());
    }

    keys = transactionstatusCache.getKeysWithExpiryCheck();
    for (Object key : keys) {
        if ((element = transactionstatusCache.get(key)) != null) {
            transactionstatusList.add((TransactionStatus) element.getObjectValue());
        }
    }
    
    
    keys = statusCache.getKeys();
    for(Object key : keys) {
    element = statusCache.get(key);
    transactionStatus = (TransactionStatus) element.getObjectValue();
    statusHash.put(transactionStatus.getUuid(), transactionStatus.getDescription());            
    }  



    SessionStatistics statistics = new SessionStatistics();

     if ((element = statisticsCache.get(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS_KEY)) != null) {
        statistics = (SessionStatistics) element.getObjectValue();
    }


    Calendar calendar = Calendar.getInstance();
    
    final int DAYS_IN_MONTH = calendar.getActualMaximum(Calendar.DAY_OF_MONTH) + 4;
    final int DAY_OF_MONTH = calendar.get(Calendar.DAY_OF_MONTH);
    final int MONTH = calendar.get(Calendar.MONTH) + 1;
    final int YEAR = calendar.get(Calendar.YEAR) - 5;
    final int YEAR_COUNT = YEAR + 10;

  

%>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ImpalaPay | Collection Reports</title>
    <link  rel="icon" type="image/png"  href="images/logo.jpg">

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet">
    <link href="css/floatexamples.css" rel="stylesheet" />

    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">


                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/img.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><%=sessionKey%></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                                        <jsp:include page="sidemenu.jsp" />
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                   <!-- <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>-->
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/img.png" alt=""><%= sessionKey%>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <form name="logoutForm" method="post" action="logout">
                                            <p>
                                            <!--<input type="submit" class="btn btn-primary" name="logout" id="logout" value="Logout">-->
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-sign-out pull-right"></i> Log Out</button>

                                            </p>
                                        </form>

                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                                <div class="page-title">
                                    <div class="title_left">
                                        <h3>EXPORT SUMMARY</h3>
                                    </div>
                                </div>



                                        <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">

                                <div class="x_content">

                                    <div class="row">



                                            <div class="col-lg-8 col-xs-8">
                                                <!-- small box -->
                                                <div class="small-box">
                                                    <div class="inner">
                                                    <div class="x_content">
                                                    </div>
                                                         <form id="userSecurityForm" class="form-horizontal" action="exportExcelcollectionReport" method="post">
                                                        <!--<div class="container">-->
                                                            <!--<div class="row">-->
                                                                <!--<div class="span8">-->
                                                                    <div class="alert alert-block alert-info">
                                                                    <p>
                                                                       Export By:
                                                                    </p>
                                                                    </div>
                                                                   
                                                                    <fieldset>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Network
                                                                        </label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <select name="networkUuid" id="input" class="form-control col-md-3 col-xs-12" required>
                                                                               <option value="">select network from dropdown (optional)</option>
                                                                                   <%
                                                                            Iterator n = networkHash.keySet().iterator();

                                                                            while (n.hasNext()) {

                                                                                String uuid = (String) n.next();

                                                                                String name = (String) networkHash.get(uuid);
                                                                                    %>

                                                                                    <option value="<%=uuid%>" ><%=name%></option>

                                                                                    <%
                                                                                        }
                                                                                    %>
                                                                                </select>
                                                                        </div>
                                                                     </div>
                                                                     <div class="form-group">
                                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                                            Status </label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <select name="transactionstatus" id="input"
                                                                                class="form-control col-md-3 col-xs-12">
                                                                                <option value="">select status from dropdown</option>
                                                                                <%
                                                        for (TransactionStatus c : transactionstatusList) {
                                                          
                                                                out.println("<option value=\"" + c.getUuid() + "\">" + c.getDescription() + "</option>");
                                                           
                                                        }
                                                    %>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                     <div class="form-group">
                                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Account
                                                                        </label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <select name="accountUuid" id="input" class="form-control col-md-3 col-xs-12" required>
                                                                               <option value="">select account name from dropdown (optional)</option>
                                                                                 <%

																					 Iterator h = accountHash.keySet().iterator();

                                                                            while (h.hasNext()) {

                                                                                String uuid = (String) h.next();

                                                                                String name = (String) accountHash.get(uuid);

                                                                                 %>
                                                                                  <option value="<%=uuid%>" ><%=name%></option>

                                                                        <%
                                                                            }
                                                                        %>


                                                                                </select>
                                                                        </div>
                                                                     </div>

                                                                     <div class="form-group">
                                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Country
                                                                        </label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <select name="countryUuid" id="input" class="form-control col-md-3 col-xs-12" required>
                                                                               <option value="">select recipient country name from dropdown (optional)</option>
                                                                                   <%
                                                                            Iterator i = countryHash.keySet().iterator();

                                                                            while (i.hasNext()) {

                                                                                String uuid = (String) i.next();

                                                                                String name = (String) countryHash.get(uuid);
                                                                        %>

                                                                        <option value="<%=uuid%>" ><%=name%></option>

                                                                        <%
                                                                            }
                                                                        %>
                                                                                </select>
                                                                        </div>
                                                                     </div>
                                                                     <input type="hidden"  name="username"  					<%
                                           out.println("value=\"" + StringUtils.trimToEmpty(sessionKey) + "\"");
                                    %>>

                                                                        <div class="control-group ">

                                                                            <td><label class="control-label">Start Date (dd/mm/yy)&nbsp;</label></td>
                                                                            <div class="controls">
                                                                                <td><select name="addDay" id="input" class="span2">
                                                                                    <%
                                                                                        for (int j = 1; j < DAYS_IN_MONTH; j++) {
                                                                                            if (j == DAY_OF_MONTH) {
                                                                                                out.println("<option selected=\"selected\" value=\"" + j + "\">" + j + "</option>");
                                                                                            } else {
                                                                                                out.println("<option value=\"" + j + "\">" + j + "</option>");
                                                                                            }
                                                                                        }
                                                                                    %>
                                                                                 </select>-</td>
                                                                                <td> <select name="addMonth" id="input" class="span2">
                                                                                    <%
                                                                                        for (int j = 1; j < 13; j++) {
                                                                                            if (j == MONTH) {
                                                                                                out.println("<option selected=\"selected\" value=\"" + j + "\">" + j + "</option>");
                                                                                            } else {
                                                                                                out.println("<option value=\"" + j + "\">" + j + "</option>");
                                                                                            }
                                                                                        }
                                                                                     %>
                                                                                </select>-</td>
                                                                                <td> <select name="addYear" id="input" class="span2">
                                                                                    <%
                                                                                        for (int j = YEAR; j < YEAR_COUNT; j++) {
                                                                                            if (j == YEAR) {
                                                                                                out.println("<option selected=\"selected\" value=\"" + j + "\">" + j + "</option>");
                                                                                            } else {
                                                                                                out.println("<option value=\"" + j + "\">" + j + "</option>");
                                                                                            }
                                                                                        }
                                                                                    %>
                                                                                </select></td>
                                                                            </div>
                                                                        </div>

                                                                        <div class="control-group ">

                                                                            <td><label class="control-label">End Date (dd/mm/yy)&nbsp;</label></td>
                                                                            <div class="controls">
                                                                                <td><select name="addDay2" id="input" class="span2">
                                                                                    <%
                                                                                        for (int k = 1; k < DAYS_IN_MONTH; k++) {
                                                                                            if (k == DAY_OF_MONTH) {
                                                                                                out.println("<option selected=\"selected\" value=\"" + k + "\">" + k + "</option>");
                                                                                            } else {
                                                                                                out.println("<option value=\"" + k + "\">" + k + "</option>");
                                                                                            }
                                                                                        }
                                                                                    %>
                                                                                 </select>-</td>
                                                                                <td> <select name="addMonth2" id="input" class="span2">
                                                                                    <%
                                                                                        for (int k = 1; k < 13; k++) {
                                                                                            if (k == MONTH) {
                                                                                                out.println("<option selected=\"selected\" value=\"" + k + "\">" + k + "</option>");
                                                                                            } else {
                                                                                                out.println("<option value=\"" + k + "\">" + k + "</option>");
                                                                                            }
                                                                                        }
                                                                                     %>
                                                                                </select>-</td>
                                                                                <td> <select name="addYear2" id="input" class="span2">
                                                                                    <%
                                                                                        for (int k = YEAR; k < YEAR_COUNT; k++) {
                                                                                            if (k == YEAR) {
                                                                                                out.println("<option selected=\"selected\" value=\"" + k + "\">" + k + "</option>");
                                                                                            } else {
                                                                                                out.println("<option value=\"" + k + "\">" + k + "</option>");
                                                                                            }
                                                                                        }
                                                                                    %>
                                                                                </select></td>
                                                                            </div>
                                                                        </div>
                                                                    </fieldset>
                                                                    <footer id="submit-actions" class="form-actions">
                                                                        <button id="submit-button" type="submit" class="btn btn-primary" name="action" value="CONFIRM"><i class="fa fa-language"></i>Export</button>
                                                                    </footer>
                                                                <!--</div>-->
                                                            <!--</div>-->
                                                        <!--</div>-->
                                                    </form>

                                                     </div>
                                                    </div>
                                                </div>
                                            </div><!-- ./col -->

                                    </div>

                                </div>
                            </div>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                <!-- footer content -->
                <footer>
                    <div class="">
                        <p>Copyright@ImapalaPay 2014-2015</p>
                            <a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy
                            Policy</a> | ImapalaPay Ltd <%= Calendar.getInstance().get(Calendar.YEAR)%>. All rights reserved.
                            <!--<img id="madeInKenya" alt="Made in Kenya" src="#" width="100" height="100" />-->
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>

                <!-- /footer content -->
            </div>
            <!-- /page content -->
        </div>

    </div>

    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>

    <script src="js/custom.js"></script>

</body>

</html>

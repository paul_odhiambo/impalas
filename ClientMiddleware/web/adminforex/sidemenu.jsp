 <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                             <ul class="nav side-menu">
                                <li><a><i class="fa fa-area-chart"></i> Home <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="dashboard.jsp">Dashboard</a>
                                        </li>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-eur"></i>Forex<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="checkerforexrate.jsp">Checker Forex</a>
                                        </li>
                                        <li><a href="forexhistory.jsp">Forex Upload History</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-database"></i> Transactions <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="transactionforex.jsp">ForexTransactions</a>
                                        </li>
                                         <li><a href="alltransaction.jsp">AllTransactions</a>
                                        </li>
                                          <!--<li><a href="updatestatus.jsp">Adjust Transaction Status</a>
                                        </li>-->
                                         <li><a href="changestatus.jsp">Adjust Status</a>
                                          <li><a href="changestatushistory.jsp">Adjusted Transactions</a>
                                        <li><a href="commission.jsp">Commission Estimates</a>
                                        </li>
                                        <li><a href="reportextract.jsp">Disbursement Reports</a>
                                        </li>
                                        <li><a href="search.jsp">Search Transactions</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-university"></i>Balance Management<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                      <li><a href="checkerFloat.jsp">Checker Client Balance</a></li>
                                      <li><a href="addFloat.jsp">Adjust Client Balance</a></li>
                                        <li><a href="floatHistory.jsp">Client Balance History</a></li>
                                        <li><a href="addNetworkFloat.jsp">Adjust Network/Partner Balance</a></li>
					<li><a href="floatonHold.jsp">Holding Account Balance</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-cubes"></i>Withdrawals<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="clientwithdrawal.jsp">Withdrawal</a>
                                        </li>
                                        <li><a href="checkerwithdrawal.jsp">Checker Withdrawal</a>
                                        </li>
                                        <li><a href="inprogresswithdrawal.jsp">Inprogress Withdrawal</a>
                                        </li>
                                         <li><a href="withdrawalhistory.jsp">Withdrawal History</a>
                                        </li>




                                    </ul>
                                </li>
 <li><a><i class="fa fa-share"></i> Collection <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="collectionbalance.jsp">Collection Balance</a>
                                        </li>
                                        <li><a href="collectionbalanceonHold.jsp">Collection OnHold </a>
                                        <li><a href="allcollectiontransaction.jsp">Collection Transactions </a>
                                        <li><a href="collectionreportextract.jsp">Collection Reports</a>
                                        <li><a href="unresolvedtransaction.jsp">Collection Unresolved </a>
                                            <li><a href="changestatuscollection.jsp">Collection Adjust Status</a>
                                            </li>
                                            <li><a href="searchcollection.jsp">Search Collections</a>
                                            </li>

                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-refresh"></i>Refund<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="refund.jsp">Refund</a>
                                        </li>
                       
                                         <li><a href="refundhistory.jsp">Refund History</a>
                                        </li>




                                    </ul>
                                </li>

                            </ul>

                                </li>
                            </ul>
                        </div>
                    </div>

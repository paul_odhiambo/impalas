<!DOCTYPE html>
<%-- 
  Copyright (c) impalapay Ltd., June 23, 2014
  
  @author eugene chimita, eugenechimita@impalapay.com
--%>
<%@page import="com.impalapay.airtel.beans.accountmgmt.Account"%>
<%@page import="com.impalapay.airtel.accountmgmt.admin.pagination.commissions.CommissionPage"%>
<%@page import="com.impalapay.airtel.accountmgmt.admin.pagination.commissions.CommissionPaginator"%>
<%@page import="com.impalapay.airtel.beans.accountmgmt.AccountStatus" %>
<%@page import="com.impalapay.airtel.persistence.accountmgmt.AccountStatusDAO" %>
<%@page import="com.impalapay.beans.commission.CommisionEstimate"%>

<%@page import="com.impalapay.airtel.beans.geolocation.Country"%>
<%@page import="com.impalapay.airtel.accountmgmt.session.SessionStatistics"%>
<%@page import="com.impalapay.airtel.accountmgmt.admin.SessionConstants"%>

                    <%@page import="com.impalapay.airtel.cache.CacheVariables"%>

                    <%@page import="org.apache.commons.lang3.StringUtils"%>

                    <%@page import="net.sf.ehcache.Cache"%>
                    <%@page import="net.sf.ehcache.CacheManager"%>
                    <%@page import="net.sf.ehcache.Element"%>

                    <%@page import="java.text.DecimalFormat"%>

                    <%@page import="java.net.URLEncoder"%>

                    <%@page import="java.util.Map"%>
                    <%@page import="java.util.Iterator"%>
                    <%@page import="java.util.ArrayList"%>
                    <%@page import="java.util.Calendar"%>
                    <%@page import="java.util.List"%>
                    <%@page import="java.util.HashMap"%>

                    <%
                    // The following is for session management.    
                if (session == null) {
                response.sendRedirect("index.jsp");
                }

                String sessionKey = (String) session.getAttribute(SessionConstants.ADMIN_SESSION_KEY);
                            

                if (StringUtils.isEmpty(sessionKey)) {
                response.sendRedirect("index.jsp");
                }

                session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
                response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=logout");
                // End of session management code



                CacheManager mgr = CacheManager.getInstance();
                Cache statisticsCache = mgr.getCache(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS);
                Cache countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
                Cache accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);
                        
                //These HashMaps contains the UUIDs of Countries as keys and the country code of countries as values
                 AccountStatusDAO accountStatusDAO = AccountStatusDAO.getInstance();
                List<AccountStatus> accountStatusList = accountStatusDAO.getAllAccountStatus();
                HashMap<String, String> countryHash = new HashMap<String, String>();
                HashMap<String, String> accountHash = new HashMap<String, String>();
                        
                SessionStatistics statistics = new SessionStatistics();
    
                        int count = 0; //generic counter
                        Element element;

                       if ((element = statisticsCache.get(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS_KEY)) != null) {
                            statistics = (SessionStatistics) element.getObjectValue();
                            }
                        
            List keys;
            //fetch from cache
            Country country;
            keys = countryCache.getKeys();
                for(Object key : keys) {
                element = countryCache.get(key);
                country = (Country) element.getObjectValue();
                countryHash.put(country.getUuid(), country.getName());          
                }

            Account account; 
            keys = accountsCache.getKeys();
                for(Object key : keys) {
                element = accountsCache.get(key);
                account = (Account) element.getObjectValue();
                accountHash.put(account.getUuid(), account.getFirstName());         
                }         
            
           
                        count = statistics.getCommissionCountTotal();
                       //count = 9;

 
  
                        CommissionPage commissionPage;
                        List<CommisionEstimate> commissionList;

                        int commissionCount; // The current count of commission
                        CommissionPaginator paginator = new CommissionPaginator();

                        if (count == 0) { // if there is no forex history in the system.
                            commissionPage = new CommissionPage();
                            commissionList = new ArrayList<CommisionEstimate>();
                            commissionCount = 0;

                        } else {
                            commissionPage = (CommissionPage) session.getAttribute("currentcommissionPage");
                            String referrer = request.getHeader("referer");
                            String pageParam = (String) request.getParameter("page");

                            // We are to give the first page
                            if (commissionPage == null
                                    || !StringUtils.endsWith(referrer, "commission.jsp")
                                    || StringUtils.equalsIgnoreCase(pageParam, "first")) {
                                commissionPage = paginator.getFirstPage();

                                // We are to give the last page
                            } else if (StringUtils.equalsIgnoreCase(pageParam, "last")) {
                                commissionPage = paginator.getLastPage();


                                // We are to give the previous page
                            } else if (StringUtils.equalsIgnoreCase(pageParam, "previous")) {
                                commissionPage = paginator.getPrevPage(commissionPage);

                                // We are to give the next page 
                            } else {
                                commissionPage= paginator.getNextPage(commissionPage);
                            }

                            session.setAttribute("currentcommissionPage", commissionPage);

                            commissionList = commissionPage.getContents();
                            commissionCount = (commissionPage.getPageNum() - 1) * commissionPage.getPagesize() + 1;
                        }
                    %>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ImpalaPay|Forex Transactions</title>
    <link  rel="icon" type="image/png"  href="images/logo.jpg">

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">


    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">


                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/img.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><%=sessionKey%></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                                        <jsp:include page="sidemenu.jsp" />
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                   <!-- <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>-->
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/img.png" alt=""><%= sessionKey%>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <form name="logoutForm" method="post" action="logout">
                                            <p>
                                            <!--<input type="submit" class="btn btn-primary" name="logout" id="logout" value="Logout">-->
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-sign-out pull-right"></i> Log Out</button>
                                           
                                            </p>
                                        </form>
                                     
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->

            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="clearfix"></div>


                    <div class="row">
                    <h3>Commission Estimates</h3>



                        <div class="clearfix"></div>

                        <div class="span16">
                            <div class="x_panel">
                    
                                <div align="right">
                                                      <form id="exportToExcel" name="exportExcelForm" method="post" action="exportExcelCommission" target="_blank" >
                               <!--<input class="btn btn-primary" type="submit" name="exportExcel" value="Export All" class="icon-paper-clip icon-large"  />-->
                                                    <button class="btn btn-primary pull-right" value="Export All" style="margin-right: 5px;" type="submit" name="exportExcelCommission"><i class="fa fa-file-archive-o"></i>Export All</button>
                                                      <!--<input class="btn btn-primary" type="submit" name="exportExcel" value="Export Page" class="icon-paper-clip icon-large"  />-->
                                                       <button class="btn btn-primary pull-right" value="Export Page" style="margin-right: 5px;" type="submit" name="exportExcelCommission"><i class="fa fa-language"></i>Export Page</button>
                            
                                                </p>
                                                </form>
                                                        </div>
                                                      <div align="left">
                                                        <div id="pagination">
                                                            <form name="pageForm" method="post" action="transactionforex.jsp">
                                                                <%
                                if (!commissionPage.isFirstPage()) {
                                                                %>
                                                                <input class="btn btn-primary" type="submit" name="page"
                                                                       value="First" /> <input class="btn btn-primary" type="submit"
                                                                       name="page" value="Previous" />
                                                                <%                                    }
                                                                %>
                                                                <span class="pageInfo">Page <span
                                                                        class="pagePosition currentPage"><%= commissionPage.getPageNum()%></span>
                                                                    of <span class="pagePosition"><%= commissionPage.getTotalPage()%></span>
                                                                </span>
                                                                <%
                                if (!commissionPage.isLastPage()) {
                                                                %>
                                                                <input class="btn btn-primary" type="submit" name="page"
                                                                       value="Next"> <input class="btn btn-primary" type="submit"
                                                                       name="page" value="Last">
                                                                <%                                    
                                }
                                                                %>
                                                            </form>
                                                        </div>

                                                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                    <!--<table class="table table-striped bootstrap-datatable datatable">-->
                                        <thead>
                                            <tr class="headings">
                                                            <th>&nbsp;</th>
                                                            <!--<th>Transaction Uuid</th>-->
                                                            <th>Transactionuuid</th>
                                                            <th>Commissioncurrency</th>
                                                            <th>Commision</th>
                                                            <th>Receivercommission</th>
                                                            <th>Receiverfullamount</th>
                                                            <th>Status</th>
                                                            <th>Server Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           

						 <%
                                                            if (commissionList != null) {
                                                                for (CommisionEstimate codes : commissionList) {
                                                                %>

                                                                <tr class='even pointer'>
                                                                    <td><%=commissionCount%></td>
                                                                    <td class="center"><%=codes.getTransactionUuid()%></td>
                                                                    <td class="center"><%=codes.getCommissioncurrency()%></td>
                                                                    <td class="center"><%=codes.getCommision()%></td>
                                                                    <td class="center"><%=codes.getReceivercommission()%></td>
								                                    <td class="center"><%=codes.getReceiverfullamount()%></td>
                                                                    <td class="center"><%=codes.getStatusuuid()%></td> 
 								                                    <td class="center"><%=codes.getDateadded()%></td>               
                                                                </tr>
                                                                 <%
                                                                    commissionCount++;
                                                                }
                                                                }

                                                            %>
                                        </tbody>

                                    </table>
                                </div>

                                                                            
                                               
                                            
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p>Copyright@ImpalaPay 2014-2015</p>
                            <a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy
                            Policy</a> | ImpalaPay Ltd <%= Calendar.getInstance().get(Calendar.YEAR)%>. All rights reserved.
                            <!--<img id="madeInKenya" alt="Made in Kenya" src="#" width="100" height="100" />-->
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>

                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>

    </div>


    <script src="js/bootstrap.min.js"></script>
    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <script src="js/custom.js"></script>
    
<!-- Datatables -->
        <script src="js/datatables/js/jquery.dataTables.js"></script>
        <script src="js/datatables/tools/js/dataTables.tableTools.js"></script>
</body>

</html>

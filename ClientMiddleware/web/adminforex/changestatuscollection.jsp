<!DOCTYPE html>
<%-- 
  Copyright (c) impalapay Ltd., June 23, 2014
  
  @author eugene chimita, eugenechimita@impalapay.com
--%>
<%@ page import="com.impalapay.collection.accountmgmt.pagination.changestatus.CollectionUpdateStatusPage"%>
<%@ page import="com.impalapay.collection.accountmgmt.pagination.changestatus.CollectionUpdateStatusPaginator"%>
<%@ page import="com.impalapay.collection.beans.incoming.CollectionUpdateStatus"%>
<%@ page import="com.impalapay.airtel.beans.transaction.TransactionStatus"%>
<%@ page import="com.impalapay.airtel.beans.geolocation.Country"%>
<%@ page import="com.impalapay.airtel.beans.accountmgmt.Account" %>


<%@ page import="com.impalapay.airtel.beans.accountmgmt.AccountStatus"%>
<%@ page import="com.impalapay.airtel.accountmgmt.admin.SessionConstants"%>
<%@ page import="com.impalapay.airtel.accountmgmt.session.SessionStatistics"%>
<%@ page import="com.impalapay.airtel.cache.CacheVariables"%>

<%@ page import="org.apache.commons.lang3.StringUtils"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collections"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Iterator"%>

<%@ page import="net.sf.ehcache.Cache"%>
<%@ page import="net.sf.ehcache.CacheManager"%>
<%@ page import="net.sf.ehcache.Element"%>

<%
    // The following is for session management.    
    if (session == null) {
        response.sendRedirect("../index.jsp");
    }

    String sessionKey = (String) session.getAttribute(SessionConstants.ADMIN_SESSION_KEY);

    if (StringUtils.isEmpty(sessionKey)) {
        response.sendRedirect("../index.jsp");
    }

    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=logout");
    // End of session management code


    

    CacheManager mgr = CacheManager.getInstance();
    Cache accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_UUID);
    Cache countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
    Cache transactionstatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
    Cache statusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);

    // This HashMap contains the UUIDs of countries as keys and the country names as values
    HashMap<String, String> countryHash = new HashMap<String, String>();
    HashMap<String, String> countrycurrencyHash = new HashMap<String, String>();
    HashMap<String, String> transactionstatusHash = new HashMap<String, String>();
    HashMap<String, String> statusHash = new HashMap<String, String>();
	HashMap<String, String> accountHash = new HashMap<String, String>();


    List<TransactionStatus> transactionstatusList = new ArrayList<TransactionStatus>();
    
    Cache statisticsCache = mgr.getCache(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS);

    List keys;

    Element element;
    Country country;
    TransactionStatus transactionsatus;
	Account account;

    

    
    keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countryHash.put(country.getUuid(), country.getCurrencycode());
    }

    keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countrycurrencyHash.put(country.getUuid(), country.getCurrencycode());
    }
    
    keys = transactionstatusCache.getKeysWithExpiryCheck();
    for (Object key : keys) {
        if ((element = transactionstatusCache.get(key)) != null) {
            transactionstatusList.add((TransactionStatus) element.getObjectValue());
        }
    }
    
    TransactionStatus transactionStatus;
    keys = statusCache.getKeys();
    for(Object key : keys) {
    element = statusCache.get(key);
    transactionStatus = (TransactionStatus) element.getObjectValue();
    statusHash.put(transactionStatus.getUuid(), transactionStatus.getDescription());            
    }  
	
	keys = accountsCache.getKeys();
    for (Object key : keys) {
        element = accountsCache.get(key);
        account = (Account) element.getObjectValue();
        accountHash.put(account.getUuid(), account.getFirstName());
    }
    
    
    Calendar calendar = Calendar.getInstance();
    
    final int DAYS_IN_MONTH = calendar.getActualMaximum(Calendar.DAY_OF_MONTH) + 1;
    final int DAY_OF_MONTH = calendar.get(Calendar.DAY_OF_MONTH);
    final int MONTH = calendar.get(Calendar.MONTH) + 1;
    final int YEAR = calendar.get(Calendar.YEAR);
    final int YEAR_COUNT = YEAR + 10;
    
    int count = 0;  // A generic counter
 
    
    

    SessionStatistics statistics = new SessionStatistics();

     if ((element = statisticsCache.get(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS_KEY)) != null) {
        statistics = (SessionStatistics) element.getObjectValue();
    }

    count = statistics.getUpdatecollectionCountTotal();
    
	CollectionUpdateStatusPaginator paginator = new CollectionUpdateStatusPaginator();
    CollectionUpdateStatusPage transactionPage;
	List<CollectionUpdateStatus> transactionList;
    int transactionCount; // The current count of the Update transactions sessions

    if (count == 0) { 	// This user has no Incoming USSD in the account
        transactionPage = new CollectionUpdateStatusPage();
        transactionList = new ArrayList<CollectionUpdateStatus>();
		transactionCount = 0;

    } else {
        transactionPage = (CollectionUpdateStatusPage) session.getAttribute("changestatusupdatePage");
        String referrer = request.getHeader("referer");
        String pageParam = (String) request.getParameter("page");

        // We are to give the first page
        if (transactionPage == null
                || !StringUtils.endsWith(referrer, "changestatuscollection.jsp")
                || StringUtils.equalsIgnoreCase(pageParam, "first")) {
			transactionPage = paginator.getFirstPage();

            // We are to give the last page
        } else if (StringUtils.equalsIgnoreCase(pageParam, "last")) {
            transactionPage = paginator.getLastPage();

            // We are to give the previous page
        } else if (StringUtils.equalsIgnoreCase(pageParam, "previous")) {
            transactionPage = paginator.getPrevPage(transactionPage);

            // We are to give the next page 
        } else {
            transactionPage = paginator.getNextPage(transactionPage);
        }

        session.setAttribute("changestatusupdatePage", transactionPage);

        transactionList = transactionPage.getContents();

        transactionCount = (transactionPage.getPageNum() - 1) * transactionPage.getPagesize() + 1;


    }


%>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>ImpalaPay | Adjust Status</title>
<link rel="icon" type="image/png" href="images/logo.jpg">

<!-- Bootstrap core CSS -->

<link href="css/bootstrap.min.css" rel="stylesheet">

<link href="fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">

<!-- Custom styling plus plugins -->
<link href="css/custom.css" rel="stylesheet">
<link href="css/icheck/flat/green.css" rel="stylesheet">
 <link rel="stylesheet" href="../dist/tablesaw.css">

<script src="js/jquery.min.js"></script>
<script src="../dist/dependencies/jquery.js"></script>
<script src="../dist/tablesaw.js"></script>
<script src="../dist/tablesaw-init.js"></script>

<!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

	<div class="container body">


		<div class="main_container">

			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">


					<div class="clearfix"></div>

					<!-- menu prile quick info -->
					<div class="profile">
						<div class="profile_pic">
							<img src="images/img.png" alt="..."
								class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<span>Welcome,</span>
							<h2><%=sessionKey%></h2>
						</div>
					</div>
					<!-- /menu prile quick info -->

					<br />

					<!-- sidebar menu -->
					<jsp:include page="sidemenu.jsp" />
					<!-- /sidebar menu -->

					<!-- /menu footer buttons -->
					<!-- <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>-->
					<!-- /menu footer buttons -->
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav class="" role="navigation">
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>

						<ul class="nav navbar-nav navbar-right">
							<li class=""><a href="javascript:;"
								class="user-profile dropdown-toggle" data-toggle="dropdown"
								aria-expanded="false"> <img src="images/img.png" alt=""><%= sessionKey%>
									<span class=" fa fa-angle-down"></span>
							</a>
								<ul
									class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
									<li>
										<form name="logoutForm" method="post" action="logout">
											<p>
												<!--<input type="submit" class="btn btn-primary" name="logout" id="logout" value="Logout">-->
												<button type="submit" class="btn btn-primary">
													<i class="fa fa-sign-out pull-right"></i> Log Out
												</button>

											</p>
										</form>

									</li>
								</ul></li>
						</ul>
					</nav>
				</div>
			</div>
			<!-- /top navigation -->
			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Adjust TransactionStatus</h3>
						</div>
					</div>



					<div class="row">
						<div class="col-md-12">
							<div class="x_panel">

								<div class="x_content">

									<div class="row">



										<div class="col-lg-7 col-xs-7">
											<!-- small box -->
											<div class="small-box">
												<div class="inner">
													<div class="x_content"></div>
													<form id="userSecurityForm" class="form-horizontal"
														action="updatecollectionstatus" method="post">
														<!--<div class="container">-->
														<!--<div class="row">-->
														<!--<div class="span8">-->
														<div class="alert alert-block alert-info">
															<p>ADJUST TRANSACTION STATUS</p>
														</div>
														<%
                                                                        String addClientErr = (String) session.getAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY);
                                                                        String addClientSuccess = (String) session.getAttribute(
                                                                                SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_SUCCESS_KEY);

                                                                        HashMap<String, String> networkfloatparamHash = (HashMap<String, String>) session.getAttribute(
                                                                                SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_PARAMETERS);
                                                                        
                                                                        if (networkfloatparamHash == null) {
                                                                            networkfloatparamHash = new HashMap<String, String>();
                                                                        }

                                                                        if (StringUtils.isNotEmpty(addClientErr)) {
                                                                            out.println("<p class=\"alert alert-error\">");
                                                                            out.println("Form error: " + addClientErr);
                                                                            out.println("</p>");
                                                                            session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, null);
                                                                        }

                                                                        if (StringUtils.isNotEmpty(addClientSuccess)) {
                                                                            out.println("<p class=\"alert alert-success\">");
                                                                            out.println("You have successfully added a change status Request. "
                                                                                    + "Please relogin to system to get new listing of all the change staus Requests.");
                                                                            out.println("</p>");
                                                                            session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_SUCCESS_KEY, null);
                                                                        }
                                                                    %>
														<fieldset>
															<div class="form-group">
																<label class="control-label col-md-3 col-sm-3 col-xs-12"
																	for="referencenumber">Reference Number<span
																	class="required">*</span>
																</label>
																<div class="col-md-8 col-sm-6 col-sm-12">
																	<input type="text" id="referencenumber"
																		name="referencenumber"
																		class="form-control col-md-7 col-xs-12">
																</div>
															</div>
															<input type="hidden" name="username"
																<%
                                           out.println("value=\"" + StringUtils.trimToEmpty(sessionKey) + "\"");
                                    %>>
										
															<div class="form-group">
																<label class="control-label col-md-3 col-sm-3 col-xs-12">Originating
																	Currency </label>
																<div class="col-md-7 col-sm-7 col-xs-12">
																	<select name="originatecurrency" id="input"
																		class="form-control col-md-3 col-xs-12">
																		<option value="">select Currency from dropdown</option>
																		<%
                                                                            Iterator a = countrycurrencyHash.keySet().iterator();

                                                                            while (a.hasNext()) {

                                                                                String uuid = (String) a.next();

                                                                                String name = (String) countrycurrencyHash.get(uuid);
                                                                                    %>

																		<option value="<%=uuid%>"><%=name%></option>

																		<%
                                                                                        }
                                                                                    %>
																	</select>
																</div>
															</div>
										
															<div class="form-group">
																<label class="control-label col-md-3 col-sm-3 col-xs-12">Terminate
																	Currency </label>
																<div class="col-md-7 col-sm-7 col-xs-12">
																	<select name="recipientcurrency" id="input"
																		class="form-control col-md-3 col-xs-12">
																		<option value="">select Currency from dropdown</option>


																		<%
                                                                            Iterator n = countrycurrencyHash.keySet().iterator();

                                                                            while (n.hasNext()) {

                                                                                String uuid = (String) n.next();

                                                                                String name = (String) countrycurrencyHash.get(uuid);
                                                                                    %>

																		<option value="<%=uuid%>"><%=name%></option>

																		<%
                                                                                        }
                                                                                    %>
																	</select>
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-md-3 col-sm-3 col-xs-12">Current
																	TransactionStatus </label>
																<div class="col-md-7 col-sm-7 col-xs-12">
																	<select name="currenttransferstatus" id="input"
																		class="form-control col-md-3 col-xs-12">
																		<option value="">select Currentstatus from dropdown</option>
																		<%
                                                for (TransactionStatus b : transactionstatusList) {
                                                  
                                                        out.println("<option value=\"" + b.getUuid() + "\">" + b.getDescription() + "</option>");
                                           
                                                }
                                            %>
																	</select>
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-md-3 col-sm-3 col-xs-12">Update
																	TransactionStatus </label>
																<div class="col-md-7 col-sm-7 col-xs-12">
																	<select name="updatetransactionstatus" id="input"
																		class="form-control col-md-3 col-xs-12">
																		<option value="">select Updatestatus from dropdown</option>
																		<%
                                                for (TransactionStatus c : transactionstatusList) {
                                                  
                                                        out.println("<option value=\"" + c.getUuid() + "\">" + c.getDescription() + "</option>");
                                                   
                                                }
                                            %>
																	</select>
																</div>
															</div>




														</fieldset>
														<footer id="submit-actions" class="form-actions">
															<button id="submit-button" type="submit"
																class="btn btn-primary" name="action" value="CONFIRM">Add</button>
														</footer>
														<!--</div>-->
														<!--</div>-->
														<!--</div>-->
													</form>

												</div>
											</div>
										</div>
										<div class="col-lg-5 col-xs-6">
											<div class="alert alert-block alert-info">
												<p>Below are some Basic instruction</p>
												1.Please confrim the Referencenumber matches the one used by the IMTs when pushing Transfers.</br> 2 Confirm the Originate currency and terminate Currency Match the one on the Transaction forex table and Transaction Table </br> 3. Only a specific set of status are allowed to be changed and in cases where one tries to change a status thats not allowed the system will Reject the Change
											</div>

										</div>
									</div>
									<!-- ./col -->
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<h3>COLLECTION STATUS CHANGE REQUEST</h3>
						<div class="clearfix"></div>

						<div class="span16">
							<div class="x_panel">

								<div align="right">
										<form id="exportcollectionToExcel" name="exportcollectionExcelForm" method="post" action="#" target="_blank" >
										<!--<input class="btn btn-primary" type="submit" name="exportExcel" value="Export All" class="icon-paper-clip icon-large"  />-->
										<button class="btn btn-primary pull-right" value="Export All" style="margin-right: 5px;" type="submit" name="exportcollectionExcel"><i class="fa fa-file-archive-o"></i>Export All</button>
										<!--<input class="btn btn-primary" type="submit" name="exportExcel" value="Export Page" class="icon-paper-clip icon-large"  />-->
										<button class="btn btn-primary pull-right" value="Export Page" style="margin-right: 5px;" type="submit" name="exportcollectionExcel"><i class="fa fa-language"></i>Export Page</button>
										</p>
										</form>
								</div>
												<div align="left">
													<div id="pagination">
														<form name="pageForm" method="post" action="changestatuscollection.jsp">
							<%                                            if (!transactionPage.isFirstPage()) {
								%>
							<input class="btn btn-info" type="submit" name="page"
								value="First" /> <input class="btn btn-info" type="submit"
								name="page" value="Previous" />

							<%
									}
								%>
							<span class="pageInfo">Page <span
								class="pagePosition currentPage"><%= transactionPage.getPageNum()%></span>
								of <span class="pagePosition"><%= transactionPage.getTotalPage()%></span>
							</span>
							<%
									if (!transactionPage.isLastPage()) {
								%>
							<input class="btn btn-info" type="submit" name="page"
								value="Next"> <input class="btn btn-info"
								type="submit" name="page" value="Last">
							<%
									}
								%>
						</form>
													</div>
												</div>

													<div class="x_content">
														<table id="example" class="tablesaw" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-mode-switch>
														<!--<table class="table table-striped bootstrap-datatable datatable">-->
														 <!--<table class="tablesaw" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>-->
															<thead>
																<tr class="headings">
																  <th scope="col" data-tablesaw-sortable-col
																								  data-tablesaw-priority="1">&nbsp;</th>
																							  <th scope="col" data-tablesaw-sortable-col
																								  data-tablesaw-priority="2">Referencenumber</th>

																							  <th scope="col" data-tablesaw-sortable-col
																								  data-tablesaw-priority="4">originatecurrency</th>
																							  <th scope="col" data-tablesaw-sortable-col
																								  data-tablesaw-priority="5">originateamount</th>
																							  <th scope="col" data-tablesaw-sortable-col
																								  data-tablesaw-priority="6">debitcurrency</th>
																							  <th scope="col" data-tablesaw-sortable-col
																								  data-tablesaw-priority="7">amount</th>
																							  <th scope="col" data-tablesaw-sortable-col
																								  data-tablesaw-priority="8">account</th>
																							  <th scope="col" data-tablesaw-sortable-col
																								  data-tablesaw-priority="9">Current Status</th>
																							  <th scope="col" data-tablesaw-sortable-col
																								  data-tablesaw-priority="10">Update Status</th>
																							  <th scope="col" data-tablesaw-sortable-col
																								  data-tablesaw-priority="11">Reject</th>
																							  <th scope="col" data-tablesaw-sortable-col
																								  data-tablesaw-priority="12">Approve</th>
																							  

																</tr>

															</thead>
															<tbody>
																<%
																if (transactionList != null) {
																	for (CollectionUpdateStatus code : transactionList) {
																	%>
	
														<tr>
															<td width="5%"><%=transactionCount%></td>
															<td class="center"><%=code.getOriginatetransactionuuid()%></td>
															<td class="center"><%=countrycurrencyHash.get(code.getOriginatecurrency())%></td>
															<td class="center"><%=code.getOriginateamount()%></td>
															<td class="center"><%=countrycurrencyHash.get(code.getDebitcurrency())%></td>
															<td class="center"><%=code.getAmount()%></td>
															<td class="center"><%=accountHash.get(code.getCreditaccountuuid())%></td>
															<td class="center"><%=statusHash.get(code.getCurrentstatus())%></td>
															<td class="center"><%=statusHash.get(code.getUpdatestatus())%></td>	
															<form method="POST" action="deletecollectionupdate">
																<input class="btn btn-info btn-xs" type="hidden"
																	name="reject"
																	<%
																		out.println("value=\"" + StringUtils.trimToEmpty(code.getOriginatetransactionuuid()) + "\"");
																		 %>>
																<input type="hidden" name="username"
																	<%
																		out.println("value=\"" + StringUtils.trimToEmpty(sessionKey) + "\"");
																		%>>
																<td class="center"><input
																	class="btn btn-danger btn-xs" type="submit" name="page"
																	value="delete"></td>
															</form>
															<form method="POST" action="approvecollectionupdate">
																<input class="btn btn-info btn-xs" type="hidden"
																	name="approve"
																	<%
																		out.println("value=\"" + StringUtils.trimToEmpty(code.getOriginatetransactionuuid()) + "\"");
																			%>>
																<input type="hidden" name="username"
																	<%
																		out.println("value=\"" + StringUtils.trimToEmpty(sessionKey) + "\"");
																		%>>
																<td class="center"><input
																	class="btn btn-success btn-xs" type="submit" name="page"
																	value="approve"></td>
															</form>
														</tr>
														<%
														transactionCount++;
																	}
																	}
	
																%>
	
															</tbody>
														</table>
													</div>
							</div>
						</div>
					</div>					
				</div>
				                                <!-- footer content -->
												<footer>
													<div class="">
														<p>Copyright@ImpalaPay 2014-2015</p>
															<a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy
															Policy</a> | ImpalaPay Ltd <%= Calendar.getInstance().get(Calendar.YEAR)%>. All rights reserved.
															<!--<img id="madeInKenya" alt="Made in Kenya" src="#" width="100" height="100" />-->
														</p>
													</div>
													<div class="clearfix"></div>
												</footer>
												<!-- /footer content -->
			</div><!--Right column-->
			<!-- /page content -->
			
		
		
		</div>
	</div>

	<script src="js/bootstrap.min.js"></script>

	<!-- chart js -->
	<script src="js/chartjs/chart.min.js"></script>
	<!-- bootstrap progress js -->
	<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
	<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script src="js/icheck/icheck.min.js"></script>

	<script src="js/custom.js"></script>

</body>

</html>

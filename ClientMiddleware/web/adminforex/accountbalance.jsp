<!DOCTYPE html>
<%-- 
  Copyright (c) impalapay Ltd., June 23, 2014
  
  @author eugene chimita, eugenechimita@impalapay.com
--%>

<%@page import="com.impalapay.airtel.accountmgmt.pagination.SearchUuidPaginator"%>
<%@page import="com.impalapay.airtel.accountmgmt.pagination.SearchRecipientMobilePaginator"%>

<%@page import="com.impalapay.airtel.accountmgmt.pagination.extra.TopupPage"%>
<%@page import="com.impalapay.airtel.accountmgmt.pagination.extra.TopupPaginator"%>
<%@page import="com.impalapay.airtel.beans.topup.Topup"%>



<%@page import="com.impalapay.airtel.beans.transaction.TransactionStatus"%>
<%@page import="com.impalapay.airtel.beans.transaction.Transaction"%>

<%@page import="com.impalapay.airtel.beans.accountmgmt.Account"%>
<%@page import="com.impalapay.airtel.beans.geolocation.Country"%>

<%@page import="com.impalapay.airtel.beans.accountmgmt.balance.MasterAccountBalance"%>




<%@page import="com.impalapay.airtel.accountmgmt.session.SessionStatistics"%>
<%@page import="com.impalapay.airtel.accountmgmt.session.SessionConstants"%>

<%@page import="com.impalapay.airtel.persistence.accountmgmt.balance.AccountBalanceDAO"%>



<%@page import="com.impalapay.airtel.persistence.util.CountUtils"%>

<%@page import="com.impalapay.airtel.cache.CacheVariables"%>

<%@page import="org.apache.commons.lang3.StringUtils" %>

<%@page import="net.sf.ehcache.Cache" %>
<%@page import="net.sf.ehcache.CacheManager" %>
<%@page import="net.sf.ehcache.Element"%>

<%@page import="java.net.URLEncoder"%>

<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.LinkedList"%>

<%
 // The following is for session management.    
                        if (session == null) {
                            response.sendRedirect("../client");
                        }

                        String sessionUsername = (String) session.getAttribute(SessionConstants.ACCOUNT_SIGN_IN_KEY);

                        if (StringUtils.isEmpty(sessionUsername)) {
                            response.sendRedirect("index.jsp");
                        }

                        session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
                        response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=logout");
                        // End of session management code

                        CacheManager mgr = CacheManager.getInstance();
                        Cache accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

                        Cache statisticsCache = mgr.getCache(CacheVariables.CACHE_STATISTICS_BY_USERNAME);
                        
                        Cache countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
                        
                        Cache statusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);

     
     AccountBalanceDAO accountbalanceDAO = AccountBalanceDAO.getInstance();
     SessionStatistics statistics = new SessionStatistics();

     MasterAccountBalance balanceobject;

     Double balance = 0.0;
    //These HashMaps contains the UUIDs of Countries as keys and the country code of countries as values
    HashMap<String, String> countryHash = new HashMap<String, String>();
                        
    HashMap<String, String> statusHash = new HashMap<String, String>();

    Account account = new Account();
    CountUtils countUtils = CountUtils.getInstance();
    List keys;
    int count = 0;  // Generic counter

    int topupCount = 0; // The current count of the Transactions  

    Element element;

    if ((element = accountsCache.get(sessionUsername)) != null) {
         account = (Account) element.getObjectValue();
       }
    
    balanceobject = accountbalanceDAO.getMasterAccountBalance(account);
    
    if(balanceobject !=null){
     balance = balanceobject.getBalance();
    }
    
   //fetch from cache
    Country country;
    keys = countryCache.getKeys();
    for(Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countryHash.put(country.getUuid(), country.getName());          
      }
   
   TransactionStatus transactionStatus;
    keys = statusCache.getKeys();
    for(Object key : keys) {
        element = statusCache.get(key);
        transactionStatus = (TransactionStatus) element.getObjectValue();
        statusHash.put(transactionStatus.getUuid(), transactionStatus.getDescription());            
      }

    if ((element = statisticsCache.get(sessionUsername)) != null) {
         statistics = (SessionStatistics) element.getObjectValue();
        }

     count = statistics.getTopupCountTotal();

      
    TopupPage topupPage;
    List<Topup> topupList;

    

    TopupPaginator paginator = new TopupPaginator(sessionUsername);

            if (count == 0) { // This user has no transactions in his/her account
                topupPage = new TopupPage();
                topupList = new ArrayList<Topup>();
                topupCount = 0;

                } else {
                    topupPage = (TopupPage) session.getAttribute("currenttopupPage");
                    String referrer = request.getHeader("referer");
                    String pageParam = (String) request.getParameter("page");

                    // We are to give the first page
                if (topupPage == null
                    || !StringUtils.endsWith(referrer, "accountbalance.jsp")
                    || StringUtils.equalsIgnoreCase(pageParam, "first")) {
                    topupPage = paginator.getFirstPage();

                    // We are to give the last page
                } else if (StringUtils.equalsIgnoreCase(pageParam, "last")) {
                    topupPage = paginator.getLastPage();


                                // We are to give the previous page
                } else if (StringUtils.equalsIgnoreCase(pageParam, "previous")) {
                    topupPage = paginator.getPrevPage(topupPage);

                                // We are to give the next page 
                } else {
                    topupPage= paginator.getNextPage(topupPage);
                }

                session.setAttribute("currenttopupPage", topupPage);

                topupList = topupPage.getContents();
                topupCount = (topupPage.getPageNum() - 1) * topupPage.getPagesize() + 1;


    }




%>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Account Balance| </title>
    <link  rel="icon" type="image/png"  href="images/logo.jpg">

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet">
    <link href="css/floatexamples.css" rel="stylesheet" />

    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">


                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/img.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><%=sessionUsername%></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                                        <jsp:include page="sidemenu.jsp" />
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                   <!-- <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>-->
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/img.png" alt=""><%= sessionUsername%>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <form name="logoutForm" method="post" action="logout">
                                            <p>
                                            <!--<input type="submit" class="btn btn-primary" name="logout" id="logout" value="Logout">-->
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-sign-out pull-right"></i> Log Out</button>
                                           
                                            </p>
                                        </form>
                                     
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>


            <!-- page content -->
            <div class="right_col" role="main">

                <br />
                <div class="">

                    <div class="row top_tiles">
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                               
                                <div class="count"><%= balance%></div>
                                <h3><i class="fa fa-dollar"></i>Current Main Float/Balance</h3>
                                 
                            </div>
                        </div>
                        

                    <div class="row">



                        <div class="clearfix"></div>

                        <div class="span16">
                            <div class="x_panel">
                                <div class="row-fluid">
                                    <div class="pull-left">
                                        <!--<form id="exportToPDF" name="exportPDFForm" method="post" action="exportPDF" target="_blank">
                                        <p>
                                        <input class="btn btn-primary" type="submit" name="exportPDF" value="Export Summary To PDF" />
                                        </p>
                                        </form>-->
                                    </div>
                                        <div align="right">
                                                <!--<form id="exportToExcel" name="exportExcelForm" method="post" action="exportExcel" target="_blank" >
                                                <input class="btn btn-primary" type="submit" name="exportExcel" value="Export All" class="icon-paper-clip icon-large"  />
                                                      <input class="btn btn-primary" type="submit" name="exportExcel" value="Export Page" class="icon-paper-clip icon-large"  />
                            
                                                </p>
                                                </form>-->
                                        </div>
                                                    <div align="right">
                                                        <div id="pagination">
                                                            <form name="pageForm" method="post" action="accountbalance.jsp">
                                                                <%
                                                                if (!topupPage.isFirstPage()) {
                                                                %>
                                                                <input class="btn btn-primary" type="submit" name="page"
                                                                       value="First" /> <input class="btn btn-primary" type="submit"
                                                                       name="page" value="Previous" />
                                                                <%                                    }
                                                                %>
                                                                <span class="pageInfo">Page <span
                                                                        class="pagePosition currentPage"><%= topupPage.getPageNum()%></span>
                                                                    of <span class="pagePosition"><%= topupPage.getTotalPage()%></span>
                                                                </span>
                                                                <%
                                                                if (!topupPage.isLastPage()) {
                                                                %>
                                                                <input class="btn btn-primary" type="submit" name="page"
                                                                       value="Next"> <input class="btn btn-primary" type="submit"
                                                                       name="page" value="Last">
                                                                <%                                    
                                }
                                                                %>
                                                            </form>
                                                        </div>
                                                    </div>

                                     <div class="x_content">
                                            <table id="example" class="table table-striped responsive-utilities jambo_table">
                                            <!--<table class="table table-striped bootstrap-datatable datatable">-->
                                                
                                            <thead>
                                                <tr class="headings">
                                                    <th></th>
                                                    <th>Amount</th>
                                                    <th>TopUp Time</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <%
                                                count = topupList.size();
                                                //out.println(count);
                                                for (int j = 0; j < count; j++) {
                                                out.println("<tr class='even pointer'>");
                                                out.println("<td>" + topupCount + "</td>");
                                                out.println("<td>" + topupList.get(j).getAmount() + "</td>");
                                                //out.println("<td>" + statusHash.get(transactionList.get(j).getTransactionStatusUuid()) + "</td>");
                                                out.println("<td>" + topupList.get(j).getTopupTime() + "</td>");
                                                out.println("</tr>");
                                                topupCount++;
                                                }
                                                %>
                                            </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>

                </div>

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p>Copyright@ImpalaPay 2014-2015</p>
                            <a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy
                            Policy</a> | ImpalaPay Ltd <%= Calendar.getInstance().get(Calendar.YEAR)%>. All rights reserved.
                            <!--<img id="madeInKenya" alt="Made in Kenya" src="#" width="100" height="100" />-->
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>

                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>

    </div>

    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>

    <script src="js/custom.js"></script>

</body>

</html>

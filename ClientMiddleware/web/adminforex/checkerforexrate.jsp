<!DOCTYPE html>
<%-- 
  Copyright (c) impalapay Ltd., June 23, 2014
  
  @author eugene chimita, eugenechimita@impalapay.com
--%>
<%@page import="com.impalapay.airtel.accountmgmt.admin.pagination.checkerforex.CurrencyPage"%>
<%@page import="com.impalapay.airtel.accountmgmt.admin.pagination.checkerforex.CurrencyPaginator"%>
<%@page import="com.impalapay.airtel.beans.forex.ForexEngineHistory"%>
<%@page import="com.impalapay.airtel.beans.geolocation.Country"%>
<%@page import="com.impalapay.airtel.accountmgmt.session.SessionStatistics"%>
<%@page import="com.impalapay.airtel.accountmgmt.admin.SessionConstants"%>
<%@page import="com.impalapay.airtel.cache.CacheVariables"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="net.sf.ehcache.Cache"%>
<%@page import="net.sf.ehcache.CacheManager"%>
<%@page import="net.sf.ehcache.Element"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
                   
<%
    // The following is for session management.    
    if (session == null) {
    response.sendRedirect("index.jsp");
    }

    String sessionKey = (String) session.getAttribute(SessionConstants.ADMIN_SESSION_KEY);
                            

    if (StringUtils.isEmpty(sessionKey)) {
    response.sendRedirect("index.jsp");
    }

    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=logout");
    // End of session management code

    CacheManager mgr = CacheManager.getInstance();
    Cache statisticsCache = mgr.getCache(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS);
    Cache countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
                        
    Element element;
    List<ForexEngineHistory> forexrateList;
                        
    int count = 0;  // A generic counter
    SessionStatistics statistics = new SessionStatistics();

	if ((element = statisticsCache.get(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS_KEY)) != null) {
	statistics = (SessionStatistics) element.getObjectValue();
	}

    CurrencyPaginator paginator = new CurrencyPaginator();

    count = statistics.getCheckerforexrateCountTotal();
    CurrencyPage forexPage;
    int forexcheckerCount = 0; // The current count of the Accounts sessions

    if (count == 0) {    // This user has no Incoming USSD in the account
        forexPage = new CurrencyPage();
        forexrateList = new ArrayList<ForexEngineHistory>();
        forexcheckerCount = 0;

    } else {
        forexPage = (CurrencyPage) session.getAttribute("currentcheckerforexratePage");
        String referrers = request.getHeader("referer");
        String pageParams = (String) request.getParameter("page");

        // We are to give the first page
        if (forexPage == null
                || !StringUtils.endsWith(referrers, "checkerforexrate.jsp")
                || StringUtils.equalsIgnoreCase(pageParams, "first")) {
            forexPage = paginator.getFirstPage();

            // We are to give the last page
        } else if (StringUtils.equalsIgnoreCase(pageParams, "last")) {
            forexPage = paginator.getLastPage();

            // We are to give the previous page
        } else if (StringUtils.equalsIgnoreCase(pageParams, "previous")) {
            forexPage = paginator.getPrevPage(forexPage);

            // We are to give the next page 
        } else {
            forexPage = paginator.getNextPage(forexPage);
        }

        session.setAttribute("currentcheckerforexratePage", forexPage);

        forexrateList = forexPage.getContents();

        forexcheckerCount = (forexPage.getPageNum() - 1) * forexPage.getPagesize() + 1;

        
    }
%>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ImpalaPay|Checker Forex</title>
    <link  rel="icon" type="image/png"  href="images/logo.jpg">

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">


    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">


                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/img.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><%=sessionKey%></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                                        <jsp:include page="sidemenu.jsp" />
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/img.png" alt=""><%= sessionKey%>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <form name="logoutForm" method="post" action="logout">
                                            <p>
                                            <!--<input type="submit" class="btn btn-primary" name="logout" id="logout" value="Logout">-->
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-sign-out pull-right"></i> Log Out</button>
                                           
                                            </p>
                                        </form>
                                     
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->

            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="clearfix"></div>


                    <div class="row">
                     <%
                    String addErrStr = (String) session.getAttribute(SessionConstants.ADMIN_ADD_FOREX_ERROR_KEY);
                    String addSuccessStr = (String) session.getAttribute(SessionConstants.ADMIN_ADD_FOREX_SUCCESS_KEY);
                    HashMap<String, String> paramHash = (HashMap<String, String>) session.getAttribute(
                            SessionConstants.ADMIN_ADD_FOREX_PARAMETERS);

                    if (paramHash == null) {
                        paramHash = new HashMap<String, String>();
                    }

                    if (StringUtils.isNotEmpty(addErrStr)) {
                        out.println("<p class=\"alert alert-error\">");
                        out.println("Form error: " + addErrStr);
                        out.println("</p>");
                        session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_ERROR_KEY, null);
                    }

                    if (StringUtils.isNotEmpty(addSuccessStr)) {
                        out.println("<p class=\"alert alert-success\">");
                        out.println(addSuccessStr);
                        session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_SUCCESS_KEY, null);
                    }
                %>
                    <h3>Approve Forex</h3>



                        <div class="clearfix"></div>

                        <div class="span16">
                            <div class="x_panel">
                                
                                                      <div align="right">
                                                        <div id="pagination">
                                                            <form name="pageForm" method="post" action="checkerforexrate.jsp">
                                                                <%
                                if (!forexPage.isFirstPage()) {
                                                                %>
                                                                <input class="btn btn-primary" type="submit" name="page"
                                                                       value="First" /> <input class="btn btn-primary" type="submit"
                                                                       name="page" value="Previous" />
                                                                <%                                    }
                                                                %>
                                                                <span class="pageInfo">Page <span
                                                                        class="pagePosition currentPage"><%= forexPage.getPageNum()%></span>
                                                                    of <span class="pagePosition"><%= forexPage.getTotalPage()%></span>
                                                                </span>
                                                                <%
                                if (!forexPage.isLastPage()) {
                                                                %>
                                                                <input class="btn btn-primary" type="submit" name="page"
                                                                       value="Next"> <input class="btn btn-primary" type="submit"
                                                                       name="page" value="Last">
                                                                <%                                    
                                }
                                                                %>
                                                            </form>
                                                        </div>

                                                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                    <!--<table class="table table-striped bootstrap-datatable datatable">-->
                                        <thead>
                        
                                            <tr class="headings">
                                                            <th>&nbsp;</th>
                                                            <th>CurrencyPair</th>
                                                            <th>Market Rate</th>
                                                            <th>Spread Rate</th>
                                                            <th>Upload Date</th>
							                                <th>Reject</th>
							                                <th>Approve</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                            if (forexrateList != null) {
                                                                for (ForexEngineHistory codes : forexrateList) {
                                                                %>

                                                                <tr>
                                                                    <td width="5%"><%=forexcheckerCount%></td>
                                                                    <td class="center"><%=codes.getCurrencypair()%></td>
                                                                    <td class="center"><%=codes.getMarketrate()%></td>
                                 								    <td class="center"><%=codes.getSpreadrate()%></td>
                                								    <td class="center"><%=codes.getUploadDate()%></td>
								                                    <form method="POST" action="deletecheckerforex">
                                                                    <input class="btn btn-info btn-xs" type="hidden" name="reject" <%
                                      				                out.println("value=\"" + StringUtils.trimToEmpty(codes.getUuid()) + "\"");
                                        		                     %>>
								                                    <input type="hidden"  name="username" <%
                                                       			    out.println("value=\"" + StringUtils.trimToEmpty(sessionKey) + "\"");
                                                				    %>>
                                                                    <td class="center"><input class="btn btn-danger btn-xs" type="submit"name="page" 									    value="delete"></td>
								                                    </form>
                                                                    <form method="POST" action="approvecheckerforex">
                                                                    <input class="btn btn-info btn-xs" type="hidden" name="approve" <%
                                                  				    out.println("value=\"" + StringUtils.trimToEmpty(codes.getUuid()) + "\"");
                                                    		            %>>
                                                                    <input type="hidden"  name="username" <%
                                                       			    out.println("value=\"" + StringUtils.trimToEmpty(sessionKey) + "\"");
                                                				    %>>  
                                                                    <td class="center"><input class="btn btn-success btn-xs" type="submit"name="page" 									    value="approve"></td>
								                    </form>
                                                                </tr>
                                                                 <%
                                                                    forexcheckerCount++;
                                                                }
                                                                }

                                                            %>
                                        </tbody>

                                    </table>
                                </div>

                                                                            
                                               
                                            
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p>Copyright@ImpalaPay 2014-2015</p>
                            <a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy
                            Policy</a> | ImpalaPay Ltd <%= Calendar.getInstance().get(Calendar.YEAR)%>. All rights reserved.
                            <!--<img id="madeInKenya" alt="Made in Kenya" src="#" width="100" height="100" />-->
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>

                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>

    </div>

    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>

    <script src="js/custom.js"></script>

</body>

</html>

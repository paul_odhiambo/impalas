<!DOCTYPE html>
<%-- 
  Copyright (c) impalapay Ltd., June 23, 2014
  
  @author eugene chimita, eugenechimita@impalapay.com
--%>
<%@ page
    import="com.impalapay.airtel.accountmgmt.admin.pagination.transactionupdate.StatusUpdateHistoryPage"%>
<%@ page
    import="com.impalapay.airtel.accountmgmt.admin.pagination.transactionupdate.StatusUpdateHistoryPaginator"%>
<%@ page
    import="com.impalapay.airtel.beans.transaction.forexrate.TransactionUpdateStatus"%>
<%@ page
    import="com.impalapay.airtel.beans.transaction.TransactionStatus"%>

<!--<%@ page import="com.impalapay.mno.accountmgmt.admin.pagination.floatbynetworkhistory.MainfloatPerCountryHistPage"%>
<%@ page import="com.impalapay.mno.accountmgmt.admin.pagination.floatbynetworkhistory.MainfloatPerCountryHistPaginator"%>
<%@ page import="com.impalapay.mno.beans.accountmgmt.balance.NetworkPurchaseHistory"%>
<%@ page import="com.impalapay.beans.network.Network" %>-->
<%@ page import="com.impalapay.airtel.beans.geolocation.Country"%>

<%@ page import="com.impalapay.airtel.beans.accountmgmt.AccountStatus"%>
<%@ page
    import="com.impalapay.airtel.accountmgmt.admin.SessionConstants"%>
<%@ page
    import="com.impalapay.airtel.accountmgmt.session.SessionStatistics"%>
<%@ page import="com.impalapay.airtel.cache.CacheVariables"%>

<%@ page import="org.apache.commons.lang3.StringUtils"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collections"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Iterator"%>

<%@ page import="net.sf.ehcache.Cache"%>
<%@ page import="net.sf.ehcache.CacheManager"%>
<%@ page import="net.sf.ehcache.Element"%>

<%
    // The following is for session management.    
    if (session == null) {
        response.sendRedirect("../index.jsp");
    }

    String sessionKey = (String) session.getAttribute(SessionConstants.ADMIN_SESSION_KEY);

    if (StringUtils.isEmpty(sessionKey)) {
        response.sendRedirect("../index.jsp");
    }

    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=logout");
    // End of session management code


    

    CacheManager mgr = CacheManager.getInstance();
    Cache accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_UUID);
    Cache countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
    Cache transactionstatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
    Cache statusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);

    // This HashMap contains the UUIDs of countries as keys and the country names as values
    HashMap<String, String> countryHash = new HashMap<String, String>();
    HashMap<String, String> countrycurrencyHash = new HashMap<String, String>();
    HashMap<String, String> transactionstatusHash = new HashMap<String, String>();
    HashMap<String, String> statusHash = new HashMap<String, String>();

    List<TransactionStatus> transactionstatusList = new ArrayList<TransactionStatus>();
    
    Cache statisticsCache = mgr.getCache(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS);

    List keys;

    Element element;
    Country country;
    TransactionStatus transactionsatus;

    List<TransactionUpdateStatus> transactionupdatestatusList;
    //List<NetworkPurchaseHistory> networkpurchasehistoryList;

    
    keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countryHash.put(country.getUuid(), country.getCurrencycode());
    }

    keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countrycurrencyHash.put(country.getUuid(), country.getCurrencycode());
    }
    
    keys = transactionstatusCache.getKeysWithExpiryCheck();
    for (Object key : keys) {
        if ((element = transactionstatusCache.get(key)) != null) {
            transactionstatusList.add((TransactionStatus) element.getObjectValue());
        }
    }
    
    TransactionStatus transactionStatus;
    keys = statusCache.getKeys();
    for(Object key : keys) {
    element = statusCache.get(key);
    transactionStatus = (TransactionStatus) element.getObjectValue();
    statusHash.put(transactionStatus.getUuid(), transactionStatus.getDescription());            
    }      
    
    
    Calendar calendar = Calendar.getInstance();
    
    final int DAYS_IN_MONTH = calendar.getActualMaximum(Calendar.DAY_OF_MONTH) + 1;
    final int DAY_OF_MONTH = calendar.get(Calendar.DAY_OF_MONTH);
    final int MONTH = calendar.get(Calendar.MONTH) + 1;
    final int YEAR = calendar.get(Calendar.YEAR);
    final int YEAR_COUNT = YEAR + 10;
    
    int count = 0;  // A generic counter
    int incount = 0;  // Generic counter
    int incounts = 0;
    
    StatusUpdateHistoryPaginator paginator = new StatusUpdateHistoryPaginator();
    //MainfloatPerCountryHistPaginator paginators = new MainfloatPerCountryHistPaginator(); 

    SessionStatistics statistics = new SessionStatistics();

     if ((element = statisticsCache.get(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS_KEY)) != null) {
        statistics = (SessionStatistics) element.getObjectValue();
    }

    incount = statistics.getUpdatetransactionHistoryCountTotal();
   
    StatusUpdateHistoryPage transactionupdatestatusPage;
    int transactionupdatestatusCount = 0; // The current count of the Accounts sessions

    if (incount == 0) {     // This user has no Incoming USSD in the account
        transactionupdatestatusPage = new StatusUpdateHistoryPage();
        transactionupdatestatusList = new ArrayList<TransactionUpdateStatus>();
        transactionupdatestatusCount = 0;

    } else {
        transactionupdatestatusPage = (StatusUpdateHistoryPage) session.getAttribute("currentstatusupdatehistoryPage");
        String referrer = request.getHeader("referer");
        String pageParam = (String) request.getParameter("page");

        // We are to give the first page
        if (transactionupdatestatusPage == null
                || !StringUtils.endsWith(referrer, "changestatushistory.jsp")
                || StringUtils.equalsIgnoreCase(pageParam, "first")) {
            transactionupdatestatusPage = paginator.getFirstPage();

            // We are to give the last page
        } else if (StringUtils.equalsIgnoreCase(pageParam, "last")) {
            transactionupdatestatusPage = paginator.getLastPage();

            // We are to give the previous page
        } else if (StringUtils.equalsIgnoreCase(pageParam, "previous")) {
            transactionupdatestatusPage = paginator.getPrevPage(transactionupdatestatusPage);

            // We are to give the next page 
        } else {
            transactionupdatestatusPage = paginator.getNextPage(transactionupdatestatusPage);
        }

        session.setAttribute("currentstatusupdatehistoryPage", transactionupdatestatusPage);

        transactionupdatestatusList = transactionupdatestatusPage.getContents();

        transactionupdatestatusCount = (transactionupdatestatusPage.getPageNum() - 1) * transactionupdatestatusPage.getPagesize() + 1;

        
    }
                    %>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ImpalaPay| StatusUpdate History</title>
    <link  rel="icon" type="image/png"  href="images/logo.jpg">

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">


    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">


                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/img.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><%=sessionKey%></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                                       <jsp:include page="sidemenu.jsp" />
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                   <!-- <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>-->
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/img.png" alt=""><%= sessionKey%>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <form name="logoutForm" method="post" action="logout">
                                            <p>
                                            <!--<input type="submit" class="btn btn-primary" name="logout" id="logout" value="Logout">-->
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-sign-out pull-right"></i> Log Out</button>
                                           
                                            </p>
                                        </form>
                                     
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->

            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="clearfix"></div>


                    <div class="row">
                    <h3>Transaction Update History</h3>



                        <div class="clearfix"></div>

                        <div class="span16">
                            <div class="x_panel">
                                <!--<div class="row-fluid">
                                    <div class="pull-left">
                                        <form id="exportToPDF" name="exportPDFForm" method="post" action="exportPDF" target="_blank">
                                        <p>-->
                                        <!--<input class="btn btn-primary" class="fa fa-file-pdf-o" type="submit" name="exportPDF" value="Export Summary To PDF" />-->
                                        <!--<button class="btn btn-primary pull-right" style="margin-right: 5px;" type="submit" name="exportPDF"><i class="fa fa-file-pdf-o"></i> Export Summary PDF</button>
                                        </p>
                                        </form>
                                    </div>
                                <div align="right">
                                                      <form id="exportToExcel" name="exportExcelForm" method="post" action="exportExcel" target="_blank" >-->
                               <!--<input class="btn btn-primary" type="submit" name="exportExcel" value="Export All" class="icon-paper-clip icon-large"  />-->
                                                    <!--<button class="btn btn-primary pull-right" value="Export All" style="margin-right: 5px;" type="submit" name="exportExcel"><i class="fa fa-file-archive-o"></i>Export All</button>-->
                                                      <!--<input class="btn btn-primary" type="submit" name="exportExcel" value="Export Page" class="icon-paper-clip icon-large"  />-->
                                                       <!--<button class="btn btn-primary pull-right" value="Export Page" style="margin-right: 5px;" type="submit" name="exportExcel"><i class="fa fa-language"></i>Export Page</button>
                            
                                                </p>
                                                </form>
                                                        </div>-->
                                                      <div align="right">
                                                        <div id="pagination">
                                                            <form name="pageForm" method="post" action="changestatushistory.jsp">
                                    <%                                            if (!transactionupdatestatusPage.isFirstPage()) {
                                        %>
                                    <input class="btn btn-info" type="submit" name="page"
                                        value="First" /> <input class="btn btn-info" type="submit"
                                        name="page" value="Previous" />

                                    <%
                                            }
                                        %>
                                    <span class="pageInfo">Page <span
                                        class="pagePosition currentPage"><%= transactionupdatestatusPage.getPageNum()%></span>
                                        of <span class="pagePosition"><%= transactionupdatestatusPage.getTotalPage()%></span>
                                    </span>
                                    <%
                                            if (!transactionupdatestatusPage.isLastPage()) {
                                        %>
                                    <input class="btn btn-info" type="submit" name="page"
                                        value="Next"> <input class="btn btn-info"
                                        type="submit" name="page" value="Last">
                                    <%
                                            }
                                        %>
                                </form>
                                                        </div>

                                                                <div class="x_content">
                                   <table id="example"
                                                class="table table-striped responsive-utilities jambo_table">
                                                <!--<table class="table table-striped bootstrap-datatable datatable">-->
                                                <thead>
                                                    <tr class="headings">
                                                        <th></th>
                                                        <th>transactionUUid</th>
                                                        <th>sourcecurrency</th>
                                                        <th>OriginalAmount</th>
                                                        <th>recipientcurrency</th>
                                                        <th>ConvertedAmount
                                                        <th>Current Status</th>
                                                        <th>Update Status</th>
                                                        <th>Time/Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%
                                                            if (transactionupdatestatusList != null) {
                                                                for (TransactionUpdateStatus code : transactionupdatestatusList) {
                                                                %>

                                                    <tr>
                                                        <td width="5%"><%=transactionupdatestatusCount%></td>
                                                        <td class="center"><%=code.getTransactionUuid()%></td>
                                                        <td class="center"><%=countrycurrencyHash.get(code.getSourcecurrency())%></td>
                                                        <td class="center"><%=code.getLocalamount()%></td>
                                                        <td class="center"><%=countrycurrencyHash.get(code.getRecipientcurrency())%></td>
                                                        <td class="center"><%=code.getConvertedamount()%></td>
                                                        <td class="center"><%=statusHash.get(code.getCurrentstatus())%></td>
                                                        <td class="center"><%=statusHash.get(code.getUpdatestatus())%></td>
                                                        <td class="center"><%=code.getServerTime()%></td>

                                                    </tr>
                                                    <%
                                                                    transactionupdatestatusCount++;
                                                                }
                                                                }

                                                            %>


                                                </tbody>
                                            </table>
                                </div>

                                                                            
                                               
                                            
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p>Copyright@ImpalaPay 2014-2015</p>
                            <a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy
                            Policy</a> | ImpalaPay Ltd <%= Calendar.getInstance().get(Calendar.YEAR)%>. All rights reserved.
                            <!--<img id="madeInKenya" alt="Made in Kenya" src="#" width="100" height="100" />-->
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>

                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>

    </div>

    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>

    <script src="js/custom.js"></script>

</body>

</html>

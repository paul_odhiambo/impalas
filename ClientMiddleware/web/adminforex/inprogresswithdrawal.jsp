<!DOCTYPE html>
<%-- 
  Copyright (c) impalapay Ltd., June 23, 2014
  
  @author eugene chimita, eugenechimita@impalapay.com
--%>
<%@ page import="com.impalapay.collection.accountmgmt.admin.pagination.cashwithdrawal.InprogressWithdrawalPage"%>
<%@ page import="com.impalapay.collection.accountmgmt.admin.pagination.cashwithdrawal.InprogressWithdrawalPaginator"%>
<%@ page import="com.impalapay.collection.beans.balance.CashWithdrawal"%>
<%@ page import="com.impalapay.airtel.beans.transaction.TransactionStatus"%>
<%@ page import="com.impalapay.airtel.beans.geolocation.Country" %>
<%@ page import="com.impalapay.airtel.beans.accountmgmt.Account" %>
<%@ page import="com.impalapay.airtel.beans.accountmgmt.AccountStatus" %>
<%@ page import="com.impalapay.airtel.accountmgmt.admin.SessionConstants" %>
<%@ page import="com.impalapay.airtel.accountmgmt.session.SessionStatistics"%>
<%@ page import="com.impalapay.airtel.persistence.accountmgmt.AccountStatusDAO" %>
<%@ page import="com.impalapay.airtel.cache.CacheVariables"%>

<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collections"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Iterator"%>

<%@ page import="net.sf.ehcache.Cache" %>
<%@ page import="net.sf.ehcache.CacheManager" %>
<%@ page import="net.sf.ehcache.Element"%>

<%
    // The following is for session management.    
    if (session == null) {
        response.sendRedirect("../index.jsp");
    }

    String sessionKey = (String) session.getAttribute(SessionConstants.ADMIN_SESSION_KEY);

    if (StringUtils.isEmpty(sessionKey)) {
        response.sendRedirect("../index.jsp");
    }

    session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
    response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=logout");
    // End of session management code


    

    CacheManager mgr = CacheManager.getInstance();
    Cache accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_UUID);
    Cache countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
    Cache statusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);

    
    // This HashMap contains the UUIDs of countries as keys and the country names as values
    HashMap<String, String> countryHash = new HashMap<String, String>();
    HashMap<String, String> countrycurrencyHash = new HashMap<String, String>();
    HashMap<String, String> accountHash = new HashMap<String, String>();
    HashMap<String, String> currencyHash = new HashMap<String, String>();
    HashMap<String, String> statusHash = new HashMap<String, String>();


    Cache statisticsCache = mgr.getCache(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS);

    List keys;

    Element element;
    Country country;
    Account account;

    List<Account> accountList = new ArrayList<Account>();
    List<CashWithdrawal> clientwithdrawrequestList;
     

    keys = accountsCache.getKeysWithExpiryCheck();
    for (Object key : keys) {
        if ((element = accountsCache.get(key)) != null) {
            accountList.add((Account) element.getObjectValue());
        }
    }

    keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countryHash.put(country.getUuid(), country.getName());
    }

    keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        countrycurrencyHash.put(country.getUuid(), country.getCurrency());
    }
    
     keys = countryCache.getKeys();
    for (Object key : keys) {
        element = countryCache.get(key);
        country = (Country) element.getObjectValue();
        currencyHash.put(country.getUuid(), country.getCurrencycode());
    }
    
    keys = accountsCache.getKeys();
    for (Object key : keys) {
        element = accountsCache.get(key);
        account = (Account) element.getObjectValue();
        accountHash.put(account.getUuid(), account.getFirstName());
    }

    TransactionStatus transactionStatus;
    keys = statusCache.getKeys();
    for(Object key : keys) {
    element = statusCache.get(key);
    transactionStatus = (TransactionStatus) element.getObjectValue();
    statusHash.put(transactionStatus.getUuid(), transactionStatus.getDescription());            
    }    
    
    Calendar calendar = Calendar.getInstance();
    
    final int DAYS_IN_MONTH = calendar.getActualMaximum(Calendar.DAY_OF_MONTH) + 1;
    final int DAY_OF_MONTH = calendar.get(Calendar.DAY_OF_MONTH);
    final int MONTH = calendar.get(Calendar.MONTH) + 1;
    final int YEAR = calendar.get(Calendar.YEAR);
    final int YEAR_COUNT = YEAR + 10;
    
    int count = 0;  // A generic counter
    int incount = 0;  // Generic counter
    int incounts = 0;
    
    InprogressWithdrawalPaginator paginator = new InprogressWithdrawalPaginator();

    SessionStatistics statistics = new SessionStatistics();

     if ((element = statisticsCache.get(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS_KEY)) != null) {
        statistics = (SessionStatistics) element.getObjectValue();
    }

    incount = statistics.getWithdrawalhistoryCountTotal();
    InprogressWithdrawalPage clientwithdrawrequestPage;
    int clientwithdrawrequestCount = 0; // The current count of the Accounts sessions

    if (incount == 0) { 	// This user has no Incoming USSD in the account
        clientwithdrawrequestPage = new InprogressWithdrawalPage();
        clientwithdrawrequestList = new ArrayList<CashWithdrawal>();
        clientwithdrawrequestCount = 0;

    } else {
        clientwithdrawrequestPage = (InprogressWithdrawalPage) session.getAttribute("inprogresswithdrawalrequestPage");
        String referrer = request.getHeader("referer");
        String pageParam = (String) request.getParameter("page");

        // We are to give the first page
        if (clientwithdrawrequestPage == null
                || !StringUtils.endsWith(referrer, "inprogresswithdrawal.jsp")
                || StringUtils.equalsIgnoreCase(pageParam, "first")) {
            clientwithdrawrequestPage = paginator.getFirstPage();

            // We are to give the last page
        } else if (StringUtils.equalsIgnoreCase(pageParam, "last")) {
            clientwithdrawrequestPage = paginator.getLastPage();

            // We are to give the previous page
        } else if (StringUtils.equalsIgnoreCase(pageParam, "previous")) {
            clientwithdrawrequestPage = paginator.getPrevPage(clientwithdrawrequestPage);

            // We are to give the next page 
        } else {
            clientwithdrawrequestPage = paginator.getNextPage(clientwithdrawrequestPage);
        }

        session.setAttribute("inprogresswithdrawalrequestPage", clientwithdrawrequestPage);

        clientwithdrawrequestList = clientwithdrawrequestPage.getContents();

        clientwithdrawrequestCount = (clientwithdrawrequestPage.getPageNum() - 1) * clientwithdrawrequestPage.getPagesize() + 1;

        
    }
%>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ImpalaPay | Inprogress Withdrawals</title>
    <link  rel="icon" type="image/png"  href="images/logo.jpg">

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet">
    <link href="css/floatexamples.css" rel="stylesheet" />

    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">


                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/img.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><%=sessionKey%></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                                        <jsp:include page="sidemenu.jsp" />
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                   <!-- <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>-->
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/img.png" alt=""><%= sessionKey%>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <form name="logoutForm" method="post" action="logout">
                                            <p>
                                            <!--<input type="submit" class="btn btn-primary" name="logout" id="logout" value="Logout">-->
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-sign-out pull-right"></i> Log Out</button>
                                           
                                            </p>
                                        </form>
                                     
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                                <div class="page-title">
                                    <div class="title_left">
                                        <h3>Finalize Withdrawal</h3>
                                    </div>  
                                </div>
                    


               
                                                                <div class="clearfix"></div>
                                            <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_content">

                                    <div class="row">

                                            <!--<div class="col-lg-6 col-xs-6">-->
                                                <form name="pageForm" method="post" action="inprogresswithdrawal.jsp">                                
					                    <%                                            if (!clientwithdrawrequestPage.isFirstPage()) {
					                    %>
					                    <input class="btn btn-info" type="submit" name="page" value="First" />
					                    <input class="btn btn-info" type="submit" name="page" value="Previous"/>
					 
					                    <%
					                        }
					                    %>
					                    <span class="pageInfo">Page 
					                        <span class="pagePosition currentPage"><%= clientwithdrawrequestPage.getPageNum()%></span> of 
					                        <span class="pagePosition"><%= clientwithdrawrequestPage.getTotalPage()%></span>
					                    </span>   
					                    <%
					                        if (!clientwithdrawrequestPage.isLastPage()) {
					                    %>
					                    <input class="btn btn-info" type="submit" name="page"value="Next"> 
					                    <input class="btn btn-info" type="submit"name="page" value="Last">
					                    <%
					                        }
					                    %>                                
					                </form>
                                                <!-- small box -->
                                                <div class="small-box">
                                                    <div class="inner">
                                                     <i class="icon-eye-open icon-large"></i>
                                                        <div class="alert alert-block alert-success">
                                                                    <p>
                                                                      FINALISE  WITHDRAWAL
                                                                    </p>
                                                                    </div>
                                                                    <%
                                                                        String addClientErr = (String) session.getAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY);
                                                                        String addClientSuccess = (String) session.getAttribute(
                                                                                SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_SUCCESS_KEY);

                                                                        HashMap<String, String> networkfloatparamHash = (HashMap<String, String>) session.getAttribute(
                                                                                SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_PARAMETERS);
                                                                        
                                                                        if (networkfloatparamHash == null) {
                                                                            networkfloatparamHash = new HashMap<String, String>();
                                                                        }

                                                                        if (StringUtils.isNotEmpty(addClientErr)) {
                                                                            out.println("<p class=\"alert alert-error\">");
                                                                            out.println("Form error: " + addClientErr);
                                                                            out.println("</p>");
                                                                            session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, null);
                                                                        }

                                                                        if (StringUtils.isNotEmpty(addClientSuccess)) {
                                                                            out.println("<p class=\"alert alert-success\">");
                                                                            out.println("You have successfully Rejected a withdrawal approval Request. "
                                                                                    + "Please relogin to system to get new listing of all the pending approval Requests.");
                                                                            out.println("</p>");
                                                                            session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_SUCCESS_KEY, null);
                                                                        }
                                                                    %>
                                                 
                                                    <div class="x_content">
                                                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                                                        <!--<table class="table table-striped bootstrap-datatable datatable">-->
                                                            <thead>
                                                                <tr class="headings">
                                                                <th></th>
                                                                <th>Account</th>
                                                                <th>Currency</th>
                                                                <th>Amount</th>
                                                                <th>withdrawalCurrency</th> 
                                                                <th>Comission Estimate</th> 
                                                                <th>Date</th>
                                                                <th>TransactionStatus</th>
                                                                <th>Receivable Amount</th>                         
                                                                <th>Failed</th>
                                                                <th>Success</th>                         
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                 <%
                                                            if (clientwithdrawrequestList != null) {
                                                                for (CashWithdrawal code : clientwithdrawrequestList) {
                                                                %>

                                                                <tr>
                                                                    <td width="5%"><%=clientwithdrawrequestCount%></td>
                                                                    <td class="center"><%=accountHash.get(code.getAccountuuid())%></td>
                                                                    <td class="center"><%=countrycurrencyHash.get(code.getCurrencyUuid())%></td>
                                                                    <td class="center"><%=code.getAmount()%></td>
                                                                    <td class="center"><%=countrycurrencyHash.get(code.getTocurrencyUuid())%></td>
                                                                    <td class="center"><%=code.getComission()%></td>
                                                                    <td class="center"><%=code.getTransactionDate()%></td>
                                                                    <td class="center"><%=statusHash.get(code.getTransactionStatusUuid())%></td>
                                                                    <td class="center"><%=code.getReceivableamount()+" "+currencyHash.get(code.getTocurrencyUuid())%></td>

 								                                    <form method="POST" action="failedwithdrawcash">
                                                                    <input class="btn btn-info btn-xs" type="hidden" name="reject" <%
                                      				                 out.println("value=\"" + StringUtils.trimToEmpty(code.getUuid()) + "\"");
                                        		                     %>>
                                                                     <input type="hidden"  name="username" <%
                                           			                 out.println("value=\"" + StringUtils.trimToEmpty(sessionKey) + "\"");
                                    				                 %>>
                                                                    <td class="center"><input class="btn btn-danger btn-xs" type="submit"name="page" value="Failed"></td>
								                                    </form>
                                                                    <form method="POST" action="successwithdrawcash">
                                                                      <input class="btn btn-info btn-xs" type="hidden" name="approve" <%
                                      				                  out.println("value=\"" + StringUtils.trimToEmpty(code.getUuid()) + "\"");
                                        		                      %>>
								    <input type="hidden"  name="username" <%
                                           			                 out.println("value=\"" + StringUtils.trimToEmpty(sessionKey) + "\"");
                                    				                 %>>
                                                                    <td class="center"><input class="btn btn-success btn-xs" type="submit"name="page" 									    value="successfull"></td>
								                                    </form>
                                                                </tr>
                                                                 <%
                                                                    clientwithdrawrequestCount++;
                                                                }
                                                                }

                                                            %>  
                                                            </tbody>
                                                        </table>
                                                     </div>
                                                    </div>
                                                </div>
                                            <!--</div>--><!-- ./col -->
                                    </div>

            
                                </div>
                            </div>
                        </div>
                    </div>
            

    
                    </div>
              
                <!-- footer content -->
                <footer>
                    <div class="">
                        <p>Copyright@ImpalaPay 2014-2015</p>
                            <a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy
                            Policy</a> | ImpalaPay Ltd <%= Calendar.getInstance().get(Calendar.YEAR)%>. All rights reserved.
                            <!--<img id="madeInKenya" alt="Made in Kenya" src="#" width="100" height="100" />-->
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>

                <!-- /footer content -->
            </div>
            <!-- /page content -->
        </div>

    </div>

    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>

    <script src="js/custom.js"></script>

</body>

</html>

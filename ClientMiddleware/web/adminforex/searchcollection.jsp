<!DOCTYPE html>
<%--
  Copyright (c) Shujaa Solutions Ltd., Jan 30, 2013

  @author Anthony Maganga, anthonym@shujaa.co.ke
--%>
<%@ page import="com.impalapay.airtel.accountmgmt.session.SessionStatistics"%>
<%@ page import="com.impalapay.airtel.accountmgmt.admin.SessionConstants"%>


<%@ page import="org.apache.commons.lang3.StringUtils" %>

<%@ page import="org.apache.log4j.Logger" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.List" %>

<%@ page import="java.io.UnsupportedEncodingException" %>


<%
// The following is for session management.
if (session == null) {
 response.sendRedirect("index.jsp");
}

String sessionKey = (String) session.getAttribute(SessionConstants.ADMIN_SESSION_KEY);

if (StringUtils.isEmpty(sessionKey)) {
 response.sendRedirect("index.jsp");
}

session.setMaxInactiveInterval(SessionConstants.SESSION_TIMEOUT);
response.setHeader("Refresh", SessionConstants.SESSION_TIMEOUT + "; url=logout");
    // End of session management code

    Logger logger = Logger.getLogger(this.getClass());


%>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>ImpalaPay | Search Collection</title>
  <link  rel="icon" type="image/png"  href="images/logo.jpg">

  <!-- Bootstrap core CSS -->

  <link href="css/bootstrap.min.css" rel="stylesheet">

  <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="css/custom.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
  <link href="css/icheck/flat/green.css" rel="stylesheet">
  <link href="css/floatexamples.css" rel="stylesheet" />

  <script src="js/jquery.min.js"></script>

  <!--[if lt IE 9]>
      <script src="../assets/js/ie8-responsive-file-warning.js"></script>
      <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">


                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/img.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><%=sessionKey%></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <jsp:include page="sidemenu.jsp" />

                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                   <!-- <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>-->
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/img.png" alt=""><%= sessionKey%>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <form name="logoutForm" method="post" action="logout">
                                            <p>
                                            <!--<input type="submit" class="btn btn-primary" name="logout" id="logout" value="Logout">-->
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-sign-out pull-right"></i> Log Out</button>

                                            </p>
                                        </form>

                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>  <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">

                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Search Collection Transaction</h3>
                        </div>
                    </div>


                    <div class="">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">


                                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                        <!-- SMS Search navigation -->
                                        <!-- Declare the CSS style of a selected link. -->
                                        <%! String selectedLinkStyle = "class=\"tab-pane active\"";%>
                                        <!-- Declare the CSS style of an unselected link. -->
                                        <%! String unSelectedLinkStyle = "class=\"tab-pane\"";%>

                                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">




                                           <!-- Allow each link to forward to this page and also set a parameter in the request object. -->
                            <li <%= (StringUtils.equals(request.getParameter("searchType"), "msisdn")
                                || request.getParameter("searchType") == null) ? selectedLinkStyle : unSelectedLinkStyle%>
                                ><a href="searchcollection.jsp?searchType=msisdn">Search by Receiver Phone-Number</a></li>


                            <li <%= (StringUtils.equals(request.getParameter("searchType"), "uniqueCode"))
                                ? selectedLinkStyle : unSelectedLinkStyle%>
                                ><a href="searchcollection.jsp?searchType=uniqueCode">Search by Transaction Uuid</a></li>
                        </ul>
                                        </ul>





                            <%   if (StringUtils.equals(request.getParameter("searchType"), "uniqueCode")) {
                            %>

                            <!--<form id="searchByUniqueCodeForm"  name="searchByUniqueCodeForm" method="post" action="searchResults.jsp">
                                <p><span>1</span>&nbsp;&nbsp;&nbsp;Enter Transaction Uuid:
                                    <input id="uniqueCodeField" class="searchTextField" type="text" name="uuid"
                                           onFocus="checkInput('uniqueCodeField', 'uniqueCodeFieldAlert')"
                                           onKeyDown="checkInput('uniqueCodeField', 'uniqueCodeFieldAlert')"
                                           onKeyUp="checkInput('uniqueCodeField', 'uniqueCodeFieldAlert')" />
                                    <span id="uniqueCodeFieldAlert"></span></p>
                                <p><span>2</span>&nbsp;&nbsp;&nbsp;Submit your search: <input class="btn btn-info" type="submit" value="Search" /></p>
                            </form>-->
                                <div class="alert alert-info alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">close</span>
                                    </button>
                                    <strong>Enter Transaction UUID!</strong> Press search Button.
                                </div>


                         <div class="col-md-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <br />
                                    <form class="form-horizontal form-label-left" id="searchByUniqueCodeForm"  name="searchByUniqueCodeForm" method="post" action="searchcollectionResults.jsp">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Transaction Uuid:</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <p></p><input type="text" class="form-control" name="uuid"
                                               onFocus="checkInput('uniqueCodeField', 'uniqueCodeFieldAlert')"
                                               onKeyDown="checkInput('uniqueCodeField', 'uniqueCodeFieldAlert')"
                                               onKeyUp="checkInput('uniqueCodeField', 'uniqueCodeFieldAlert')">
                                                <span id="uniqueCodeFieldAlert"></span></p>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-info">Search</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>


                            <%  } else {
                            %>

                            <!--<form id="searchByMsisdnForm" class="airtimeSearchForm" name="searchByMsisdnForm" method="post" action="searchResults.jsp">
                                <p><span>1</span>&nbsp;&nbsp;&nbsp;Enter Phone Number:
                                    <input id="msisdnField" class="searchTextField" type="text" name="phone"
                                           onFocus="checkInput('msisdnField', 'msisdnFieldAlert')"
                                           onKeyDown="checkInput('msisdnField', 'msisdnFieldAlert')"
                                           onKeyUp="checkInput('msisdnField', 'msisdnFieldAlert')" />
                                    <span id="msisdnFieldAlert"></span></p>
                                <p><span>2</span>&nbsp;&nbsp;&nbsp;Submit your search: <input class="btn btn-info" type="submit" value="Search" /></p>
                            </form>-->
           		<div class="alert alert-info alert-dismissible fade in" role="alert">
               	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">close</span>
                  </button>
                  <strong>Enter Phone Number!</strong> Press search Button.
               </div>

                         <div class="col-md-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <br />
                                    <form class="form-horizontal form-label-left" id="searchByMsisdnForm"  name="searchByMsisdnForm" method="post" action="searchcollectionResults.jsp">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Phone Number:</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <p></p><input id="msisdnField" type="text" class="form-control" name="phone"
                                               onFocus="checkInput('uniqueCodeField', 'uniqueCodeFieldAlert')"
                                               onKeyDown="checkInput('uniqueCodeField', 'uniqueCodeFieldAlert')"
                                               onKeyUp="checkInput('uniqueCodeField', 'uniqueCodeFieldAlert')">
                                                <span id="uniqueCodeFieldAlert"></span></p>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-info">Search</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>



                            <%    }
                            %>




                    </div>
                </div>
            </div>
           </div>
            </div>
            <!-- footer content -->
                <footer>
                  <div class="">
                      <p>Copyright@ImpalaPay 2014-2015</p>
                          <a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy
                          Policy</a> | ImapalaPay Ltd <%= Calendar.getInstance().get(Calendar.YEAR)%>. All rights reserved.
                          <!--<img id="madeInKenya" alt="Made in Kenya" src="#" width="100" height="100" />-->
                      </p>
                  </div>
                    <div class="clearfix"></div>
                </footer>

                <!-- /footer content -->
			</div>

      </div>
    </div>
</div>
<script src="js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="js/icheck/icheck.min.js"></script>

<script src="js/custom.js"></script>
</body>

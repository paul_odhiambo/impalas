package com.impalapay.bank.servlet.api.bridge.centurygroup;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
import net.sf.ehcache.CacheManager;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryStatus extends HttpServlet {

	private String CLIENT_URL = "";
	private PostWithIgnoreSSL postwithignoreSSL;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, switchresponse = null;
		String responseobject = "";

		// These represent parameters received over the network
		String username = "", sessionid = "", referencenumber = "", receiveruuid = "", receiverquery = "",
				switchcode = "", statusdescription = "", statusdescription2 = "";

		// Hold values from properties config.
		String masteragentcode = "", languagecode = "", agentcode = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			sessionid = root.getAsJsonObject().get("password").getAsString();
			referencenumber = root.getAsJsonObject().get("transactionid").getAsString();
			receiveruuid = root.getAsJsonObject().get("receivertransactionid").getAsString();
			receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		masteragentcode = PropertiesConfig.getConfigValue("MASTER_AGENT_CODE");

		agentcode = PropertiesConfig.getConfigValue("AGENTCODE");

		languagecode = PropertiesConfig.getConfigValue("LANG_CODE");

		JsonObject queryrequest = new JsonObject();

		queryrequest.addProperty("UserName", username);
		queryrequest.addProperty("Password", sessionid);
		queryrequest.addProperty("PayoutReferenceID", receiveruuid);
		queryrequest.addProperty("MasterAgentCode", masteragentcode);
		queryrequest.addProperty("AgentCode", agentcode);
		queryrequest.addProperty("LangCode", languagecode);

		String results2 = g.toJson(queryrequest);

		CLIENT_URL = receiverquery;

		postwithignoreSSL = new PostWithIgnoreSSL(CLIENT_URL, results2);

		// capture the switch respoinse.
		responseobject = postwithignoreSSL.doPost();

		try {
			// pass the returned json string
			JsonElement roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			switchresponse = roots.getAsJsonObject().get("Transaction").getAsJsonObject();

			statusdescription = switchresponse.getAsJsonObject().get("TransactionStatus").getAsString();

			statusdescription2 = roots.getAsJsonObject().get("Code").getAsString();

		} catch (Exception e) {
			// instantiate the JSon
			expected.put("status_code", "0032");
			expected.put("status_description", "INVALID_RESPONSE_FROM_WALLETsssss");
			String jsonResult = g.toJson(expected);

			return jsonResult + "  " + responseobject.toString();

		}
		// start mapping all the error codes
		// map ACCEPTED FOR PROCESSING
		if (statusdescription2.equalsIgnoreCase("200")) {
			switchcode = "00029";
			statusdescription = "Invalid_Referencenumber";
		}

		if (statusdescription.equalsIgnoreCase("CON")) {// confirmed
			switchcode = "S001";
			statusdescription = "CREDIT_IN_PROGRESS";
		}
		if (statusdescription.equalsIgnoreCase("PAI")) {// paid
			switchcode = "S000";
			statusdescription = "SUCCESS";
		}
		if (statusdescription.equalsIgnoreCase("REJ")) {// rejected
			switchcode = "00029";
			statusdescription = "Rejected_Transaction";
		}
		if (statusdescription.equalsIgnoreCase("ATH")) {// inprogress
			switchcode = "S001";
			statusdescription = "CREDIT_IN_PROGRESS";
		}
		if (statusdescription.equalsIgnoreCase("CAN")) {// cancelled
			switchcode = "00029";
			statusdescription = "Cancelled_Transaction";
		}

		Date now = new Date();

		String stringdate = String.valueOf(now);

		String paid = "S000";
		String inprogress = "S001";
		String fail = "00029";

		if (switchcode.equalsIgnoreCase(paid)) {

			expected.put("am_timestamp", stringdate);
			expected.put("status_code", switchcode);
			expected.put("status_description", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (switchcode.equalsIgnoreCase(fail)) {
			expected.put("am_timestamp", stringdate);
			expected.put("status_code", switchcode);
			expected.put("status_description", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (switchcode.equalsIgnoreCase(inprogress)) {
			expected.put("am_timestamp", stringdate);
			expected.put("status_code", switchcode);
			expected.put("status_description", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("am_timestamp", stringdate);
		expected.put("status_code", switchcode);
		expected.put("status_description", statusdescription);
		String jsonResult = g.toJson(expected);

		return jsonResult;

		// return responseobject;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

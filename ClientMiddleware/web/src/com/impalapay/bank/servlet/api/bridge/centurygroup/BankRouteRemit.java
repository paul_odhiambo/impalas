package com.impalapay.bank.servlet.api.bridge.centurygroup;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.NameSplitUtil;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;

public class BankRouteRemit extends HttpServlet {

	private PostWithIgnoreSSL postwithignoreSSL;
	private Cache accountsCache;
	private String CLIENT_URL = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		// joined json string
		Account account = null;

		String join = "";
		JsonElement root = null, roots = null;
		JsonObject root2 = null, creditrequest = null;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", sourcecountrycode = "",
				recipientcurrencycode = "", recipientcountrycode = "", sourcemsisdn = "", recipientmobile = "",
				sendername = "", amountstring = "", remiturlss = "", responseobject = "", statuscode = "",
				statusdescription = "", referencenumber = "", senderaddress = "", sendercity = "", accountnumber = "",
				bankcode = "", recipientname = "", branchcode = "", iban = "";

		String senderfirstname = "", senderlastname = "", recipientfirstname = "", recipientlastname = "",
				responsedate = "";

		// Hold values from properties config.
		String destinationagentcode = "", productcode = "", masteragentcode = "", languagecode = "", fee = "", tax = "",
				agentcode = "", rate = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			sourcecountrycode = root.getAsJsonObject().get("sourcecountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("recipientcurrencycode").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("recipientcountrycode").getAsString();

			sendername = root.getAsJsonObject().get("Sender_Name").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();

			sourcemsisdn = root.getAsJsonObject().get("source_msisdn").getAsString();

			recipientmobile = root.getAsJsonObject().get("beneficiary_msisdn").getAsString();

			senderaddress = root.getAsJsonObject().get("sender_address").getAsString();

			sendercity = root.getAsJsonObject().get("sender_city").getAsString();

			bankcode = root.getAsJsonObject().get("bank_code").getAsString();

			branchcode = root.getAsJsonObject().get("branch_code").getAsString();

			iban = root.getAsJsonObject().get("iban").getAsString();

			accountnumber = root.getAsJsonObject().get("account_number").getAsString();

			recipientname = root.getAsJsonObject().get("recipient_name").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(sourcecountrycode)
				|| StringUtils.isBlank(recipientcurrencycode) || StringUtils.isBlank(sendername)
				|| StringUtils.isBlank(amountstring) || StringUtils.isBlank(remiturlss)
				|| StringUtils.isBlank(senderaddress) || StringUtils.isBlank(recipientmobile)
				|| StringUtils.isBlank(sourcemsisdn) || StringUtils.isBlank(sendercity) || StringUtils.isBlank(bankcode)
				|| StringUtils.isBlank(accountnumber) || StringUtils.isBlank(recipientcountrycode)
				|| StringUtils.isBlank(recipientname)) {

			expected.put("status_code", "server error");
			expected.put("status_description", "missing parameters");
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

		}

		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("status_code", "server error");
			expected.put("status_description", "unauthorised user");
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// ##################################################
		// split and assign firstname last name
		// ##################################################

		NameSplitUtil sendersplit = new NameSplitUtil(sendername);
		senderfirstname = sendersplit.getFirstName();
		senderlastname = sendersplit.getLastName();

		NameSplitUtil receiversplit = new NameSplitUtil(recipientname);
		recipientfirstname = receiversplit.getFirstName();
		recipientlastname = receiversplit.getLastName();

		productcode = PropertiesConfig.getConfigValue("PRODUCTCODE");

		masteragentcode = PropertiesConfig.getConfigValue("MASTER_AGENT_CODE");

		languagecode = PropertiesConfig.getConfigValue("LANG_CODE");

		fee = PropertiesConfig.getConfigValue("FEE");

		tax = PropertiesConfig.getConfigValue("TAX");

		agentcode = PropertiesConfig.getConfigValue("AGENTCODE");

		rate = PropertiesConfig.getConfigValue("EXCHANGE_RATE");

		destinationagentcode = "CCBZHE";

		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################

		creditrequest = new JsonObject();

		// server time
		Date now = new Date();

		// creditrequest.addProperty("OriginCountryCode", "ZAF");
		creditrequest.addProperty("OriginCountryCode", sourcecountrycode);
		creditrequest.addProperty("OriginCurrencyCode", "USD");
		creditrequest.addProperty("DestinationCountryCode", "CHN");
		creditrequest.addProperty("DestinationCurrencyCode", "USD");// used to
		creditrequest.addProperty("DestinationAgentCode", destinationagentcode);// CCBZHE
		creditrequest.addProperty("PayoutReferenceID", "");
		creditrequest.addProperty("ProductCode", productcode);
		creditrequest.addProperty("LocalSendDateTime", String.valueOf(now));
		creditrequest.addProperty("SendingAmount", "0.0");// put
		creditrequest.addProperty("Fee", fee);
		creditrequest.addProperty("Tax", tax);
		creditrequest.addProperty("Discount", "0.0");
		creditrequest.addProperty("ExchangeRate", rate);
		creditrequest.addProperty("PayingAmount", amountstring);// cny layout
		creditrequest.addProperty("SenderFirstName", senderfirstname);
		creditrequest.addProperty("SenderLastName", senderlastname);
		creditrequest.addProperty("SenderCountryCode", "ZAF");
		creditrequest.addProperty("BeneficiaryFirstName", recipientfirstname);
		creditrequest.addProperty("BeneficiaryLastName", recipientlastname);
		creditrequest.addProperty("BeneficiaryMobileNumber", recipientmobile);
		creditrequest.addProperty("BeneficiaryCountryCode", "CHN");
		creditrequest.addProperty("AccountNumber", accountnumber);
		creditrequest.addProperty("UserName", apiusername);
		creditrequest.addProperty("Password", apipassword);
		creditrequest.addProperty("MasterAgentCode", masteragentcode);
		creditrequest.addProperty("AgentCode", agentcode);
		creditrequest.addProperty("LangCode", languagecode);

		/**
		 * if (vendorfields != null) { creditrequest.add("vendor_uniquefields",
		 * vendorfields); }
		 **/

		// assign the remit url from properties.config
		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");
		CLIENT_URL = remiturlss;

		String jsonData = g.toJson(creditrequest);

		postwithignoreSSL = new PostWithIgnoreSSL(CLIENT_URL, jsonData);

		// *******************************************************
		// capture the switch response.
		// *******************************************************
		responseobject = postwithignoreSSL.doPost();

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			statuscode = roots.getAsJsonObject().get("Code").getAsString();

			statusdescription = roots.getAsJsonObject().get("Message").getAsString();

			referencenumber = roots.getAsJsonObject().get("PayoutReferenceID").getAsString();

			responsedate = roots.getAsJsonObject().get("ResponseDate").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("command_status", APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult + " " + responseobject.toString();
		}

		// start mapping all the error codes
		// map ACCEPTED FOR PROCESSING

		if (statuscode.equalsIgnoreCase("0")) {
			statuscode = "S001";
		}

		// map AUTHENTICATION_FAILED
		if (statuscode.equalsIgnoreCase("900")) {
			statuscode = "01029";
		}

		// map AUTHENTICATION_FAILED
		if (statuscode.equalsIgnoreCase("901")) {
			statuscode = "01029";
		}

		if (statuscode.equalsIgnoreCase("999")) {
			statuscode = "00032";

		}

		if (statuscode.equalsIgnoreCase("201") || statuscode.equalsIgnoreCase("202")
				|| statuscode.equalsIgnoreCase("203") || statuscode.equalsIgnoreCase("204")
				|| statuscode.equalsIgnoreCase("205") || statuscode.equalsIgnoreCase("206")
				|| statuscode.equalsIgnoreCase("207") || statuscode.equalsIgnoreCase("208")
				|| statuscode.equalsIgnoreCase("209") || statuscode.equalsIgnoreCase("203")
				|| statuscode.equalsIgnoreCase("210") || statuscode.equalsIgnoreCase("211")
				|| statuscode.equalsIgnoreCase("212") || statuscode.equalsIgnoreCase("213")
				|| statuscode.equalsIgnoreCase("214") || statuscode.equalsIgnoreCase("215")
				|| statuscode.equalsIgnoreCase("216") || statuscode.equalsIgnoreCase("217")
				|| statuscode.equalsIgnoreCase("902")) {
			statuscode = "00029";
		}

		// return String.valueOf(transactionid);

		String success = "S001";

		if (statuscode.equalsIgnoreCase(success)) {

			expected.put("am_referenceid", referencenumber);
			expected.put("am_timestamp", responsedate);
			expected.put("status_code", statuscode);
			expected.put("status_description", "CREDIT_IN_PROGRESS");
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}
		expected.put("am_referenceid", referencenumber);
		expected.put("am_timestamp", responsedate);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		String jsonResult = g.toJson(expected);

		return jsonResult + "   " + String.valueOf(creditrequest);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.airtel.tests.gson;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

public class JsonStringObject {

	public static void main(String[] args) {
		String username = "eugene";
		String exchangerate = "500";
		String date = "okay";
		String ok = "okeyyy";

		Map<String, String> user3 = new HashMap<>();
		user3.put("api_username", "demo");
		user3.put("session_id", "7021637a32da4a90bad789f0e66da7db");
		user3.put("source_country_code", "UK");
		user3.put("sendername", "Willis luck");

		Gson g = new Gson();

		String jsonData3 = g.toJson(user3);

		// String invalid ="eugene = chimita";
		String json = "{\n" + " \"api_username\": \"" + username + "\",\n" + " \"amount\": \"" + exchangerate + "\",\n"
				+ " \"client_datetime\": \"" + date + "\",\n" + " \"command_status\": " + jsonData3 + "\n" + "}";
		try {
			JsonElement root = new JsonParser().parse(json);

			String usernames = root.getAsJsonObject().get("amount").getAsString();

			JsonElement command = root.getAsJsonObject().get("command_status");

			// Convert JSON string back to Map.
			Type type = new TypeToken<Map<String, String>>() {
			}.getType();
			Map<String, String> map = g.fromJson(command, type);
			for (String key : map.keySet()) {
				// System.out.println("map.get = " + map.get(key));
			}

			// To get all the entries from a map:
			for (Map.Entry<String, String> entry : map.entrySet()) {
				String key = entry.getKey();
				String tab = entry.getValue();

				System.out.println(key + "=" + tab);
			}

			String apiusername = command.getAsJsonObject().get("source_country_code").getAsString();

			// System.out.println(command);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println("invalid json formart");
		}

	}
}
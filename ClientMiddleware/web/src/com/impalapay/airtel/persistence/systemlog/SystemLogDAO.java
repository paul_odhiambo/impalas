package com.impalapay.airtel.persistence.systemlog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.airtel.persistence.GenericDAO;

public class SystemLogDAO extends GenericDAO implements MonitorSystemLog {

	private static SystemLogDAO systemlogDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	public static SystemLogDAO getInstance() {

		if (systemlogDAO == null) {
			systemlogDAO = new SystemLogDAO();
		}

		return systemlogDAO;
	}

	protected SystemLogDAO() {
		super();
	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public SystemLogDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

	}

	@Override
	public SystemLog getsystemlog(String uuid) {
		SystemLog s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM systemlog WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, SystemLog.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting systemlog with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public boolean putsystemlog(SystemLog clientip) {
		boolean success = true;
		Date date;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("INSERT INTO systemlog(uuid,username," + "action) VALUES (?, ?, ?);");

			pstmt.setString(1, clientip.getUuid());
			pstmt.setString(2, clientip.getUsername());
			pstmt.setString(3, clientip.getAction());

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + clientip);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<SystemLog> getAllSystemLogs() {
		List<SystemLog> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM systemlog ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, SystemLog.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all systemlogs.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<SystemLog> getAllSystemLog(int fromIndex, int toIndex) {
		List<SystemLog> list = new ArrayList<>();
		SystemLog s;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM systemlog ORDER BY creationdate DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, SystemLog.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all systemlog from index " + fromIndex + " to index "
					+ toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

}

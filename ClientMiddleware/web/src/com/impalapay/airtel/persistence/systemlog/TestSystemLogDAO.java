package com.impalapay.airtel.persistence.systemlog;

import static org.junit.Assert.*;

import java.util.List;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import com.impalapay.airtel.beans.clientipaddress.ClientIP;
import com.impalapay.airtel.beans.systemlog.SystemLog;

public class TestSystemLogDAO {

	final String DB_NAME = "airteldblive";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "airtellive";
	final String DB_PASSWD = "ThejKoyb34";
	final int DB_PORT = 5432;

	final String CLIENTIP_UUID = "hjisuyhdiushusfhu";
	final String Account_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String IPADDRESS_UUID = "7691160b2eb346a88d0100249dba8ae6";
	final String IP_ADDRESS = "192.168.10.7";
	final int IP_COUNT = 1;

	final String REPLACE_IP = "127.168.10.67";

	private SystemLogDAO storage;

	@Test
	// @Ignore
	public void testPutClientIp() {
		storage = new SystemLogDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		SystemLog clientip = new SystemLog();

		clientip.setUuid("452356525yyyd");
		clientip.setUsername("eugene");
		clientip.setAction("chnaged password from zero to zigi");

		assertTrue(storage.putsystemlog(clientip));

	}

}

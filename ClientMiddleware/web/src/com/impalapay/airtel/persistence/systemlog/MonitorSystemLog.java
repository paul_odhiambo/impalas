package com.impalapay.airtel.persistence.systemlog;

import java.util.List;

import com.impalapay.airtel.beans.systemlog.SystemLog;

public interface MonitorSystemLog {

	/**
	 * Retrieve the IP corresponding to the uuid.
	 * 
	 * @param uuid
	 * @return ClientIP
	 */
	public SystemLog getsystemlog(String uuid);

	/**
	 * 
	 * @param systemlog
	 * @return
	 */
	public boolean putsystemlog(SystemLog systemlog);

	/**
	 * Retrieves all clientips
	 * 
	 * @return List<ClientIp>
	 * 
	 */
	public List<SystemLog> getAllSystemLogs();

	public List<SystemLog> getAllSystemLog(int fromIndex, int toIndex);

}

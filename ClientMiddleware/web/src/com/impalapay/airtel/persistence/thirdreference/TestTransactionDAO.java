package com.impalapay.airtel.persistence.thirdreference;

import java.util.Date;
import java.util.List;

import com.impalapay.airtel.beans.thirdreference.ThirdPartyReference;
import com.impalapay.airtel.beans.transaction.Transaction;
import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class TestTransactionDAO {

	final String DB_NAME = "airteldblive";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "airtellive";
	final String DB_PASSWD = "ThejKoyb34";
	final int DB_PORT = 5432;

	final String TRANSACTION_UUID = "61797229-eb8b-4f84-bb15-a0410dc5d33b";
	final String TRANSACTION_REF = "fc47bd5c-7fdd-4def-92d7-f2b840aebe0f";
	final String TRANSACTION_REF2 = "fc47b-7fdd-4def-92d7-f2b840aebe0f";
	final String TRANSCONFIRM_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String TRANSACTIONSTATUS_UUID = "3b6edb35-654d-4049-b7ea-0f1db29c6e77";
	final int ID = 1;
	final String UUID = "c089e01983d744fab21ae34982f174e0";
	final String DEMO_ACCOUNT_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String NEW_ACCOUNT_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";

	final String NEWUUID = "c089e01983d744fab21ae34982f174e0";
	final String SENDER_TOKEN = "f889-811a-1c66c7655a7f";
	final String SENDERNAME = "eugene chimita";
	final String NEWSOURCE_COUNTRYCODE = "KE";
	final String NEW_RECIPIENTCODE = "d4a676822f4546a0bee789e83070f788";
	final String NEW_RECIPIENTMOBILE = "254-715-266-678";
	final String NEW_CURRENCYCODE = "USD";
	final Date TRANSACTION_DATE_NEW = new Date(new Long("1367597206000").longValue()); // Fri May 03 19:06:46 EAT
																						// 2013;
	final Date TRANSACTION_DATE_VALID = new Date(new Long("1360065927000").longValue()); // 2013-02-05 15:05:27 (Feb
	final String NEWDATES = "2013-02-05 15:05:27"; // 5th)
	final int AMOUNT = 500;
	final String NETWORK_UUID = "602c66b6-83a4-46f4-a109-17a3bcd8d70b";

	final int TRANSACTION_COUNT = 1;

	private ThirdReferenceDAO storage = new ThirdReferenceDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

	/**
	 * Test method for ThirdReferenceDAO#getReference(String)
	 */
	@Ignore
	@Test
	public void testGetReferenceByUuid() {

		List<ThirdPartyReference> reference = storage.getReference(UUID);

		// assertEquals(transaction, 3);

		for (ThirdPartyReference data : reference) {
			System.out.println(data);
		}
	}

	/**
	 * Test method for {@link TransactionDAO#addTransaction(Transaction)}
	 * 
	 **/
	@Ignore
	@Test
	public void testAddReference() {

		ThirdPartyReference saved = new ThirdPartyReference();

		// saved.setId(ID);
		saved.setUuid(NEWUUID);
		saved.setTransactionuuid(NEW_ACCOUNT_UUID);
		saved.setReferencenumber(SENDER_TOKEN);
		saved.setServerTime(new Date());

		assertTrue(storage.addReferences(saved));
	}

	@Test
	public void testgetTransactionReference() {

		ThirdPartyReference reference = storage.getTransactionReference(NEW_ACCOUNT_UUID);

		// assertEquals(transaction, 3);

		// for (ThirdPartyReference data : reference) {
		System.out.println(reference.getReferencenumber());
		// }
	}

}

package com.impalapay.airtel.persistence.clientipaddress;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.clientipaddress.ClientIP;

public interface AirtelClientIpaddress {

	/**
	 * Retrieve the IP corresponding to the uuid.
	 * 
	 * @param uuid
	 * @return ClientIP
	 */
	public ClientIP getIpaddress(String uuid);

	/**
	 * Retrieve the IP
	 * 
	 * @param uuid
	 * @return ClientIP
	 */
	public ClientIP getClientIpaddress(String uuid);

	/**
	 * Retrieve the IP corresponding to the account.
	 * 
	 * @param uuid
	 * @return ClientIp
	 * 
	 */

	public ClientIP getIpaddress(Account account);

	/**
	 * @param clientip
	 * @return whether the action was successful or not
	 */
	public boolean putClientIp(ClientIP clientip);

	/**
	 * 
	 * @param clientip
	 * @return
	 */
	public boolean DeleteClientIp(String clientip);

	/**
	 * Retrieves all clientips
	 * 
	 * @return List<ClientIp>
	 */
	public List<ClientIP> getAllClientIp();

	public boolean updateClientIp(String clientIpUuid, ClientIP newClientIp);

	public List<ClientIP> getAllIp(int fromIndex, int toIndex);

}

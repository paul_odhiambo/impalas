package com.impalapay.airtel.persistence.clientipaddress;

import static org.junit.Assert.*;

import java.util.List;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import com.impalapay.airtel.beans.clientipaddress.ClientIP;

public class TestClientIpaddressDAO {

	final String DB_NAME = "airteldblive";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "airtellive";
	final String DB_PASSWD = "ThejKoyb34";
	final int DB_PORT = 5432;

	final String CLIENTIP_UUID = "hjisuyhdiushusfhu";
	final String Account_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String IPADDRESS_UUID = "7691160b2eb346a88d0100249dba8ae6";
	final String IP_ADDRESS = "192.168.10.7";
	final int IP_COUNT = 1;

	final String REPLACE_IP = "127.168.10.67";

	private ClientIpaddressDAO storage;

	/**
	 * Test method for
	 * com.impalapay.airtel.persistence.clientipaddress.ClientIpaddressDAO#getIpaddress(java.lang.String).
	 */
	@Ignore
	@Test
	public void testClientIpString() {
		storage = new ClientIpaddressDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		ClientIP clientIp = storage.getIpaddress(IPADDRESS_UUID);
		assertEquals(clientIp.getIpAddress(), IP_ADDRESS);
		;

	}

	/**
	 * Test method for
	 * com.impalapay.airtel.persistence.clientipaddress.ClientIpaddressDAO#getAllClientIp()
	 */
	@Test
	@Ignore
	public void testGetAllClientIp() {
		storage = new ClientIpaddressDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		List<ClientIP> list = storage.getAllClientIp();
		assertEquals(list.size(), IP_COUNT);

	}

	@Test
	@Ignore
	public void testPutClientIp() {
		storage = new ClientIpaddressDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		ClientIP clientip = new ClientIP();

		clientip.setUuid("452356525yyyd");
		clientip.setIpAddress("192.168.10.6");
		clientip.setAccountUuid(Account_UUID);

		assertTrue(storage.putClientIp(clientip));

	}

	@Test
	@Ignore
	public void testUpdateClientIP() {
		storage = new ClientIpaddressDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		ClientIP clientip = new ClientIP();

		clientip.setUuid(CLIENTIP_UUID);
		clientip.setIpAddress(REPLACE_IP);
		clientip.setAccountUuid(Account_UUID);

		assertTrue(storage.updateClientIp(CLIENTIP_UUID, clientip));

	}

	@Test
	@Ignore
	public void testGetIpaddress() {
		storage = new ClientIpaddressDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		String ip = "127.0.0.1";

		System.out.println(storage.getClientIpaddress(ip));

	}

	@Test
	// @Ignore
	public void deleteClientIP() {
		storage = new ClientIpaddressDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		String ip = "198.23.56.78";

		assertTrue(storage.DeleteClientIp(ip));

	}

}

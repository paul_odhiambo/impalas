package com.impalapay.airtel.persistence.transaction.forex;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;

public class TestTransactionForexDAO {

	final String DB_NAME = "remittancedb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "remittance";
	final String DB_PASSWD = "gertedNen2";
	final int DB_PORT = 5432;

	final String TRANSACTIONFOREX_UUID = "fc47bd5c-7fdd-4def-92d7-f2b840aebe0f";
	final String TRANSACTION_UUID = "61797229-eb8b-4f84-bb15-a0410dc5d33b";
	final String ACCOUNT_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String COUNTRY_UUID = "ed0cd3cd0f5246ef83f90721f8d38105";
	final String COUNTRY_UUID2 = "91fc8aae-cb76-4c64-ac45-48448fb5673f";
	final String LOCALAMOUNT = "7521.66";
	final String ACCOUNT_TYPE = "USD";
	final String CONVERT_AMOUNT = "84.5130337079";
	final String IMPALARATE = "89";
	final String BASERATE = "90";
	final String RECEIVER_MSISDN = "444-499-3303";
	final String SERVERTIME = "2014-06-09 01:20:36";
	final int ID = 1;

	final String UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String DEMO_ACCOUNT_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String NEW_ACCOUNT_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";

	final String NEWUUID = "c089e01983d744fab21ae34982f174e0";
	final String SENDER_TOKEN = "f889-811a-1c66c7655a7f";
	final String SENDERNAME = "eugene chimita";
	final String NEWSOURCE_COUNTRYCODE = "KE";
	final String NEW_RECIPIENTCODE = "d4a676822f4546a0bee789e83070f788";
	final String NEW_RECIPIENTMOBILE = "254-715-266-678";
	final String NEW_CURRENCYCODE = "USD";
	final Date TRANSACTION_DATE_NEW = new Date(new Long("1367597206000").longValue()); // Fri May 03 19:06:46 EAT
																						// 2013;
	final Date TRANSACTION_DATE_VALID = new Date(new Long("1360065927000").longValue()); // 2013-02-05 15:05:27 (Feb
	final String NEWDATES = "2013-02-05 15:05:27"; // 5th)
	final int AMOUNT = 500;
	final String NETWORK_UUID = "602c66b6-83a4-46f4-a109-17a3bcd8d70b";

	final int TRANSACTION_COUNT = 1;

	private TransactionForexDAO storage = new TransactionForexDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

	/**
	 * Test method for TransactionForexDAO#TransactionForexDAO(String)
	 */
	@Ignore
	@Test
	public void testGetTransactionsForexByUuid() {

		List<TransactionForexrate> transactionforex = storage.getTransactionForexs(TRANSACTIONFOREX_UUID);

		// assertEquals(transaction, 3);

		for (TransactionForexrate data : transactionforex) {
			System.out.println(data);
		}
	}

	/**
	 * Test method for TransactionDAO#getAllTransactionForexs(java.lang.String, int,
	 * int)
	 */
	@Ignore
	@Test
	public void TestgetAllTransactions() {

		int fromIndex = 0;
		int toIndex = 30;

		// String uuid = "61797229-eb8b-4f84-bb15-a0410dc5d33b";

		int expectedSize = storage.getAllTransactionForexs(fromIndex, toIndex).size();
		int actualSize = 29;
		// System.out.print(expectedSize);
		assertEquals(expectedSize, actualSize);

	}

	/**
	 * Test method for
	 * {@link TransactionForexDAO#getTransactionForexs(Account, String, int, int)}
	 */
	@Test
	@Ignore
	public void testGetTransactionForexs() {

		int fromIndex = 0;
		int toIndex = 15;
		Account account = new Account();
		account.setUuid(ACCOUNT_UUID);

		int expectedSize = storage.getTransactionForexs(account, fromIndex, toIndex).size();
		int actualSize = 7;
		assertEquals(expectedSize, actualSize);
		// System.out.println(expectedSize);
	}

	/**
	 * Test method for {@link TransactionForexDAO#addTransaction(Transaction)}
	 * 
	 **/
	@Ignore
	@Test
	public void testAddTransactionForex() {
		Date veve = new Date();

		TransactionForexrate saved = new TransactionForexrate();

		// saved.setId(ID);
		saved.setUuid("hdihfiodhfdo");
		saved.setTransactionUuid("4548a02895b44eab9b0ab50426e9a97c");
		saved.setAccount("9756f889-811a-4a94-b13d-1c66c7655a7f");
		saved.setRecipientcountry(COUNTRY_UUID);
		saved.setLocalamount(455.09);
		saved.setAccounttype(ACCOUNT_TYPE);
		saved.setConvertedamount(500);
		saved.setImpalarate(98);
		saved.setBaserate(100);
		saved.setReceivermsisdn("25476879988");
		saved.setSurplus(56.7);
		saved.setServerTime(veve);

		assertTrue(storage.addTransactionForex(saved));
	}

	/**
	 * Test method for TransactionForexDAO#TransactionForexDAO(String)
	 */
	@Ignore
	@Test
	public void testGetTransactionsForexByCountryUuid() {

		Country country1 = new Country();
		country1.setUuid(COUNTRY_UUID2);

		List<TransactionForexrate> transactionforex = storage.getTransactionForexsCountry(country1);
		// assertEquals(transaction, 3);

		for (TransactionForexrate data : transactionforex) {
			System.out.println(data);
		}
	}

	/**
	 * Test method for TransactionForexDAO#getAllTransactionForexs( )}
	 */
	@Ignore
	@Test
	public void testGetAllTransactionForexDAO() {

		int count = 100;
		List<TransactionForexrate> transactions = storage.getAllTransactionForexs();

		assertEquals(transactions.size(), count);
	}

	@Ignore
	@Test
	public void testdeleteTransactionForexDAO() {

		String uuid = "husgdusgdusgdus";

		assertTrue(storage.deleteTransactionForex(uuid));

	}

	/**
	 * Test method for TransactionForexDAO#TransactionForexDAO(String)
	 */
	@Ignore
	@Test
	public void testGetTransactionsForexByTransactionUuid() {

		TransactionForexrate transactionforex = storage.getTransactionForexsUuid("972881ac4a0c49a6aaf6b8bbeaf68f2f");

		System.out.println(transactionforex.getAccounttype());

	}

	// @Ignore
	@Test
	public void TestgetTransactions() {

		int fromIndex = 0;
		int toIndex = 15;

		String uuid = "de4e0c9b5ced45b1b370f47a29739e2f";

		int expectedSize = storage.getAllTransactionForexs(uuid, fromIndex, toIndex).size();
		int actualSize = 29;
		System.out.print(expectedSize);
		// assertEquals(expectedSize, actualSize);

	}

}

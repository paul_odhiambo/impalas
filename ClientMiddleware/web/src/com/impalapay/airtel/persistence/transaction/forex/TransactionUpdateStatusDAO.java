package com.impalapay.airtel.persistence.transaction.forex;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.transaction.forexrate.TransactionUpdateStatus;
import com.impalapay.airtel.persistence.GenericDAO;

public class TransactionUpdateStatusDAO extends GenericDAO implements AirtelTransactionUpdateStatus {

	public static TransactionUpdateStatusDAO transactionforexDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	public static TransactionUpdateStatusDAO getinstance() {
		if (transactionforexDAO == null) {
			transactionforexDAO = new TransactionUpdateStatusDAO();
		}

		return transactionforexDAO;

	}

	public TransactionUpdateStatusDAO() {
		super();
	}

	/**
	 * 
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public TransactionUpdateStatusDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);
	}

	@Override
	public List<TransactionUpdateStatus> getAllTransactionUpdateStatus() {
		// TODO Auto-generated method stub
		List<TransactionUpdateStatus> list = new LinkedList<>();

		TransactionUpdateStatus transactionupdatestatus = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM transactionupdatestatus;");

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transactionupdatestatus = beanProcessor.toBean(rset, TransactionUpdateStatus.class);
				// transaction.setId(rset.getInt("id"));
				list.add(transactionupdatestatus);

			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all transactionupdatestatus for update");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<TransactionUpdateStatus> getAllTransactionUpdateStatus(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		List<TransactionUpdateStatus> list = new LinkedList<>();

		// Transaction transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM transactionupdatestatus ORDER BY  serverTime DESC LIMIT ? OFFSET ? ;");

			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				list = beanProcessor.toBeanList(rset, TransactionUpdateStatus.class);
				// transaction = b.toBean(rset, Transaction.class);
				// transaction.setId(rset.getInt("sessionid"));

				// list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting transactionupdatestatus with uuid  from " + fromIndex
					+ " to " + toIndex);
			logger.error(e.toString());

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public boolean addTransactionUpdateStatus(TransactionUpdateStatus transactionupdatestatus) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO transactionupdatestatus (uuid,transactionUuid,recipientcurrency, sourcecurrency , localamount, "
							+ "convertedamount, exchangerate, Currentstatus, Updatestatus, serverTime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, transactionupdatestatus.getUuid());
			pstmt.setString(2, transactionupdatestatus.getTransactionUuid());
			pstmt.setString(3, transactionupdatestatus.getRecipientcurrency());
			pstmt.setString(4, transactionupdatestatus.getSourcecurrency());
			pstmt.setDouble(5, transactionupdatestatus.getLocalamount());
			pstmt.setDouble(6, transactionupdatestatus.getConvertedamount());
			pstmt.setDouble(7, transactionupdatestatus.getImpalarate());
			pstmt.setString(8, transactionupdatestatus.getCurrentstatus());
			pstmt.setString(9, transactionupdatestatus.getUpdatestatus());
			pstmt.setTimestamp(10, new Timestamp(transactionupdatestatus.getServerTime().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + transactionupdatestatus);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;

	}

	@Override
	public boolean deleteTransactionUpdateStatus(String transactionid) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("DELETE FROM transactionupdatestatus WHERE transactionUuid=?;");

			pstmt.setString(1, transactionid);

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while deleting " + transactionid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public TransactionUpdateStatus getTransactionUpdateStatusUuid(String transactionuuid) {
		TransactionUpdateStatus transactionforexrate = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM transactionupdatestatus WHERE transactionUuid=?;");
			pstmt.setString(1, transactionuuid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transactionforexrate = beanProcessor.toBean(rset, TransactionUpdateStatus.class);

			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching transactionupdatestatus with transactionuuid" + transactionuuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return transactionforexrate;
	}

	@Override
	public boolean addTransactionUpdateStatusHistory(TransactionUpdateStatus transactionupdatetable) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO transactionupdatestatushistory (uuid,transactionUuid,recipientcurrency, sourcecurrency , localamount, "
							+ "convertedamount, exchangerate, Currentstatus, Updatestatus, serverTime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, transactionupdatetable.getUuid());
			pstmt.setString(2, transactionupdatetable.getTransactionUuid());
			pstmt.setString(3, transactionupdatetable.getRecipientcurrency());
			pstmt.setString(4, transactionupdatetable.getSourcecurrency());
			pstmt.setDouble(5, transactionupdatetable.getLocalamount());
			pstmt.setDouble(6, transactionupdatetable.getConvertedamount());
			pstmt.setDouble(7, transactionupdatetable.getImpalarate());
			pstmt.setString(8, transactionupdatetable.getCurrentstatus());
			pstmt.setString(9, transactionupdatetable.getUpdatestatus());
			pstmt.setTimestamp(10, new Timestamp(transactionupdatetable.getServerTime().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + transactionupdatetable);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;

	}

	@Override
	public List<TransactionUpdateStatus> getAllTransactionUpdateStatusHistory(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		List<TransactionUpdateStatus> list = new LinkedList<>();

		// Transaction transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM transactionupdatestatushistory ORDER BY  serverTime DESC LIMIT ? OFFSET ? ;");

			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				list = beanProcessor.toBeanList(rset, TransactionUpdateStatus.class);
				// transaction = b.toBean(rset, Transaction.class);
				// transaction.setId(rset.getInt("sessionid"));

				// list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting transactionupdatestatus with uuid  from " + fromIndex
					+ " to " + toIndex);
			logger.error(e.toString());

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public TransactionUpdateStatus getTransactionUpdateStatusUuidHistory(String transactionuuid) {
		TransactionUpdateStatus transactionforexrate = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM transactionupdatestatushistory WHERE transactionUuid=?;");
			pstmt.setString(1, transactionuuid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transactionforexrate = beanProcessor.toBean(rset, TransactionUpdateStatus.class);

			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching transactionupdatestatushistory with transactionuuid"
					+ transactionuuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return transactionforexrate;
	}

}

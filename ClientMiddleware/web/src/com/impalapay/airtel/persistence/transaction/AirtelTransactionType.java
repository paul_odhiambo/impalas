package com.impalapay.airtel.persistence.transaction;

import java.util.List;

import com.impalapay.airtel.beans.transaction.TransactiontransferType;

public interface AirtelTransactionType {

	/**
	 * Gets all {@link TransactiontransferType}s which have an UUID matching the
	 * argument. The list returned is not arranged in any particular order.
	 * 
	 * @param uuid
	 * @return TransactionReferences request(s)
	 */
	public List<TransactiontransferType> getTransactionType(String uuid);

	/**
	 * Gets all transaction requests
	 *
	 * @return List<{@link TransactionReferences}> all transaction requests
	 */
	public List<TransactiontransferType> getAllBankTransactionType();

	/**
	 * Gets all transactions requests between the specified fromIndex, inclusive,
	 * and toIndex, exclusive.
	 *
	 * @param fromIndex
	 * @param toIndex
	 * 
	 * @return List<{@link TransactionReferences}> all TransactionReferences
	 *         requests
	 */
	public List<TransactiontransferType> getAllTransactionType(int fromIndex, int toIndex);

	/**
	 *
	 * @param TransactionReferences
	 * @return ThirdPartyReference
	 */
	public boolean addTransactionType(TransactiontransferType bank);

}

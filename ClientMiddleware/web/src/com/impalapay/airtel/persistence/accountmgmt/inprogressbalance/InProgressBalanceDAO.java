package com.impalapay.airtel.persistence.accountmgmt.inprogressbalance;

import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressBalancebyCountryHold;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressBalancebyCountryHoldHist;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressMasterBalanceHold;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressMasterBalanceHoldHist;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.airtel.persistence.systemlog.SystemLogDAO;
import com.impalapay.mno.beans.accountmgmt.balance.AccountPurchaseByCountry;
import com.impalapay.mno.beans.accountmgmt.balance.MasterAccountFloatPurchase;
import com.impalapay.mno.persistence.accountmgmt.balance.AccountPurchaseDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

public class InProgressBalanceDAO extends GenericDAO implements InProgressBalance {

	public static InProgressBalanceDAO inprogressbalanceDAO;

	private Logger logger;
	private AccountPurchaseDAO accountPurchaseDAO;
	private SystemLogDAO systemlogDAO;

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return {@link TransactionDAO}
	 */
	public static InProgressBalanceDAO getInstance() {

		if (inprogressbalanceDAO == null) {
			inprogressbalanceDAO = new InProgressBalanceDAO();
		}

		return inprogressbalanceDAO;
	}

	/**
	 * 
	 */
	public InProgressBalanceDAO() {
		super();
		accountPurchaseDAO = AccountPurchaseDAO.getInstance();
		systemlogDAO = SystemLogDAO.getInstance();
		logger = Logger.getLogger(this.getClass());
	}

	/**
	 * 
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public InProgressBalanceDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);
		accountPurchaseDAO = new AccountPurchaseDAO(dbName, dbHost, dbUsername, dbPassword, dbPort);
		logger = Logger.getLogger(this.getClass());
	}

	@Override
	public boolean deleteMasterBalanceOnSuccess(String transactionid) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean putBalanceOnHoldAccount(InProgressMasterBalanceHoldHist masteraccount,
			InProgressBalancebyCountryHoldHist balancebycountry) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null, pstmt3 = null, pstmt4 = null, pstmt5 = null, pstmt6 = null;
		ResultSet rset = null, rset2 = null;

		try {
			conn = dbCredentials.getConnection();
			conn.setAutoCommit(false);

			// inser into master balance hold history table
			pstmt = conn.prepareStatement(
					"INSERT INTO clientmainbalanceHoldhistory(uuid,accountuuid,amount,currency,transactionuuid,refundedback,processed,topuptime) "
							+ "VALUES(?,?,?,?,?,?,?,?);");

			pstmt.setString(1, masteraccount.getUuid());
			pstmt.setString(2, masteraccount.getAccountUuid());
			pstmt.setDouble(3, masteraccount.getAmount());
			pstmt.setString(4, masteraccount.getCurrency());
			pstmt.setString(5, masteraccount.getTransactionuuid());
			pstmt.setBoolean(6, masteraccount.isRefundedback());
			pstmt.setBoolean(7, masteraccount.isProcessed());
			pstmt.setTimestamp(8, new Timestamp(masteraccount.getTopuptime().getTime()));
			pstmt.execute();

			// Credit the master float balance(clientbalance)
			pstmt2 = conn.prepareStatement("SELECT * FROM clientmainbalanceHoldhistory WHERE transactionuuid = ?;");

			pstmt2.setString(1, masteraccount.getTransactionuuid());
			rset = pstmt2.executeQuery();

			if (rset.next()) {

				// insert into client balance holding account.
				pstmt3 = conn.prepareStatement(
						"INSERT INTO clientmainbalanceHold(uuid, accountuuid,currency,transactionuuid,amount) "
								+ "VALUES(?,?,?,?,?);");

				pstmt3.setString(1, UUID.randomUUID().toString());
				pstmt3.setString(2, masteraccount.getAccountUuid());
				pstmt3.setString(3, masteraccount.getCurrency());
				pstmt3.setString(4, masteraccount.getTransactionuuid());
				pstmt3.setDouble(5, masteraccount.getAmount());
				pstmt3.execute();

			}

			// insert into balance by country hold history table
			pstmt4 = conn.prepareStatement(
					"INSERT INTO balancebycountryHoldhistory(uuid,accountuuid,countryuuid,amount,transactionuuid,refundedback,processed,topuptime) "
							+ "VALUES(?,?,?,?,?,?,?,?);");

			pstmt4.setString(1, balancebycountry.getUuid());
			pstmt4.setString(2, balancebycountry.getAccountUuid());
			pstmt4.setString(3, balancebycountry.getCountryuuid());
			pstmt4.setDouble(4, balancebycountry.getAmount());
			pstmt4.setString(5, balancebycountry.getTransactionuuid());
			pstmt4.setBoolean(6, balancebycountry.isRefundedback());
			pstmt4.setBoolean(7, balancebycountry.isProcessed());
			pstmt4.setTimestamp(8, new Timestamp(balancebycountry.getTopuptime().getTime()));
			pstmt4.execute();

			pstmt5 = conn.prepareStatement("SELECT * FROM balancebycountryHoldhistory WHERE transactionuuid = ?;");

			pstmt5.setString(1, balancebycountry.getTransactionuuid());
			rset2 = pstmt5.executeQuery();

			if (rset2.next()) {

				// insert into client balance holding account.
				pstmt6 = conn.prepareStatement(
						"INSERT INTO balancebycountryHold (uuid, countryuuid, accountuuid, transactionuuid, amount) "
								+ "VALUES(?,?,?,?,?);");

				pstmt6.setString(1, UUID.randomUUID().toString());
				pstmt6.setString(2, balancebycountry.getCountryuuid());
				pstmt6.setString(3, balancebycountry.getAccountUuid());
				pstmt6.setString(4, balancebycountry.getTransactionuuid());
				pstmt6.setDouble(5, balancebycountry.getAmount());
				pstmt6.execute();

			}

			conn.commit();

		} catch (SQLException e) {
			logger.error("SQLException exception while inserting: " + masteraccount + " and balancebycountry "
					+ balancebycountry);
			logger.error(ExceptionUtils.getStackTrace(e));

			System.out.println(e.getStackTrace());

			try {
				conn.rollback();
			} catch (SQLException ex) {
			}
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (rset2 != null) {
				try {
					rset2.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt3 != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt4 != null) {
				try {
					pstmt4.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt5 != null) {
				try {
					pstmt5.close();
				} catch (SQLException e) {
				}
			}
			if (pstmt6 != null) {
				try {
					pstmt6.close();
				} catch (SQLException e) {

				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public boolean removeBalanceHoldSuccess(String transactionuuid) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null, pstmt3 = null, pstmt4 = null, pstmt5 = null, pstmt6 = null;
		ResultSet rset = null, rset2 = null;

		try {
			conn = dbCredentials.getConnection();
			conn.setAutoCommit(false);

			pstmt = conn.prepareStatement("SELECT * FROM clientmainbalanceHoldhistory WHERE transactionuuid=?;");
			pstmt.setString(1, transactionuuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				// update the status to success/processed
				pstmt2 = conn.prepareStatement(
						"UPDATE clientmainbalanceHoldhistory SET refundedback=?,processed=? WHERE transactionuuid = ?;");
				pstmt2.setBoolean(1, false);
				pstmt2.setBoolean(2, true);
				pstmt2.setString(3, transactionuuid);
				pstmt2.executeUpdate();
			}

			// stage two update the clientbalancebycountry hold
			pstmt3 = conn.prepareStatement("SELECT * FROM balancebycountryholdhistory WHERE transactionuuid=?;");
			pstmt3.setString(1, transactionuuid);
			rset2 = pstmt3.executeQuery();

			if (rset2.next()) {
				// update the status to success/processed
				pstmt4 = conn.prepareStatement(
						"UPDATE balancebycountryholdhistory SET refundedback=?,processed=? WHERE transactionuuid = ?;");
				pstmt4.setBoolean(1, false);
				pstmt4.setBoolean(2, true);
				pstmt4.setString(3, transactionuuid);
				pstmt4.executeUpdate();
			}

			// Delete the purchase
			pstmt5 = conn.prepareStatement("DELETE FROM clientmainbalanceHold WHERE transactionuuid=?;");
			pstmt5.setString(1, transactionuuid);
			pstmt5.executeUpdate();

			pstmt6 = conn.prepareStatement("DELETE FROM balancebycountryHold WHERE transactionuuid=?;");
			pstmt6.setString(1, transactionuuid);
			pstmt6.executeUpdate();

			conn.commit();

		} catch (SQLException e) {
			logger.error(
					"SQLException exception while executing multiple update for master balance with transactionuuid: "
							+ transactionuuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;
			try {
				conn.rollback();
			} catch (SQLException ex) {
			}

		} finally {
			try {
				if (rset != null) {
					rset.close();
				}
				if (rset2 != null) {
					rset2.close();
				}
				if (pstmt != null) {
					pstmt.close();
				}
				if (pstmt2 != null) {
					pstmt2.close();
				}
				if (pstmt3 != null) {
					pstmt3.close();
				}
				if (pstmt4 != null) {
					pstmt4.close();
				}
				if (pstmt5 != null) {
					pstmt5.close();
				}
				if (pstmt6 != null) {
					pstmt6.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (SQLException e) {
			}
		}

		return success;

	}

	@Override
	public boolean removeBalanceHoldFail(String transactionuuid) {
		boolean success = true;
		double masteramount = 0, bycountryamount = 0;
		String accountuuid = "", mastercurrency = "", countryuuid = "";

		Connection conn = null;
		MasterAccountFloatPurchase p;
		SystemLog systemlogmaster, systemlogbycountry;
		AccountPurchaseByCountry bycountry;
		PreparedStatement pstmt = null, pstmt2 = null, pstmt3 = null, pstmt4 = null, pstmt5 = null, pstmt6 = null;
		ResultSet rset = null, rset2 = null;

		try {
			conn = dbCredentials.getConnection();
			conn.setAutoCommit(false);

			pstmt = conn.prepareStatement("SELECT * FROM clientmainbalanceHoldhistory WHERE transactionuuid=?;");
			pstmt.setString(1, transactionuuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {

				// assign appropriate values
				accountuuid = rset.getString("accountuuid");
				masteramount = rset.getDouble("amount");
				mastercurrency = rset.getString("currency");
				// update the status to success/processed
				pstmt2 = conn.prepareStatement(
						"UPDATE clientmainbalanceHoldhistory SET refundedback=?,processed=? WHERE transactionuuid = ?;");
				pstmt2.setBoolean(1, true);
				pstmt2.setBoolean(2, true);
				pstmt2.setString(3, transactionuuid);
				pstmt2.executeUpdate();
			}

			// stage two update the clientbalancebycountry hold
			pstmt3 = conn.prepareStatement("SELECT * FROM balancebycountryHold WHERE transactionuuid=?;");
			pstmt3.setString(1, transactionuuid);
			rset2 = pstmt3.executeQuery();

			if (rset2.next()) {
				bycountryamount = rset2.getDouble("amount");
				countryuuid = rset2.getString("countryuuid");
				// update the status to success/processed
				pstmt4 = conn.prepareStatement(
						"UPDATE balancebycountryholdhistory SET refundedback=?,processed=? WHERE transactionuuid = ?;");
				pstmt4.setBoolean(1, true);
				pstmt4.setBoolean(2, true);
				pstmt4.setString(3, transactionuuid);
				pstmt4.executeUpdate();
			}

			// Prepare object for Master Float.
			p = new MasterAccountFloatPurchase();
			p.setUuid(UUID.randomUUID().toString());
			p.setAccountUuid(accountuuid);
			p.setAmount(masteramount);
			p.setCurrency(mastercurrency);

			bycountry = new AccountPurchaseByCountry();
			bycountry.setAccountUuid(accountuuid);
			bycountry.setCountryUuid(countryuuid);
			bycountry.setAmount(bycountryamount);
			bycountry.setPurchaseDate(new Date());

			if (accountPurchaseDAO.putMasterFloat(p) && accountPurchaseDAO.putClientPurchaseByCountry2(bycountry)) {

				// update the systemlogs for master float.
				systemlogmaster = new SystemLog();
				systemlogbycountry = new SystemLog();
				systemlogmaster.setUsername("automatic-systemreversal");
				systemlogmaster.setUuid(StringUtils.remove(UUID.randomUUID().toString(), '-'));
				systemlogmaster
						.setAction("automatic-systemreversal" + " added and approved new masterbalance of currency "
								+ mastercurrency + " amount= " + masteramount + " for account " + accountuuid);

				// update system logs for float by country
				systemlogbycountry.setUsername("automatic-systemreversal");
				systemlogbycountry.setUuid(StringUtils.remove(UUID.randomUUID().toString(), '-'));
				systemlogbycountry.setAction("automatic-systemreversal" + "added and approved new balance of "
						+ bycountryamount + " on country " + countryuuid + " for account " + accountuuid);
				systemlogDAO.putsystemlog(systemlogmaster);
				systemlogDAO.putsystemlog(systemlogbycountry);

				// Delete the tables
				pstmt5 = conn.prepareStatement("DELETE FROM clientmainbalanceHold WHERE transactionuuid=?;");
				pstmt5.setString(1, transactionuuid);
				pstmt5.executeUpdate();

				pstmt6 = conn.prepareStatement("DELETE FROM balancebycountryhold WHERE transactionuuid=?;");
				pstmt6.setString(1, transactionuuid);
				pstmt6.executeUpdate();

				conn.commit();
			}

		} catch (SQLException e) {
			logger.error(
					"SQLException exception while executing multiple update for master balance with transactionuuid: "
							+ transactionuuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;
			try {
				conn.rollback();
			} catch (SQLException ex) {
			}

		} finally {
			try {
				if (rset != null) {
					rset.close();
				}
				if (rset2 != null) {
					rset2.close();
				}
				if (pstmt != null) {
					pstmt.close();
				}
				if (pstmt2 != null) {
					pstmt2.close();
				}
				if (pstmt3 != null) {
					pstmt3.close();
				}
				if (pstmt4 != null) {
					pstmt4.close();
				}
				if (pstmt5 != null) {
					pstmt5.close();
				}
				if (pstmt6 != null) {
					pstmt6.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (SQLException e) {
			}
		}

		return success;
	}

	@Override
	public List<InProgressBalancebyCountryHold> getAllBalancebyCountryHold(int fromIndex, int toIndex) {
		List<InProgressBalancebyCountryHold> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn
					.prepareStatement("SELECT * FROM balancebycountryhold ORDER BY accountuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, InProgressBalancebyCountryHold.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all balancebycountryhold from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<InProgressBalancebyCountryHoldHist> getAllBalancebyCountryHoldHist(int fromIndex, int toIndex) {
		List<InProgressBalancebyCountryHoldHist> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM balancebycountryholdhistory ORDER BY topuptime DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, InProgressBalancebyCountryHoldHist.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all balancebycountryholdhistory from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<InProgressMasterBalanceHold> getAllMasterBalanceHold(int fromIndex, int toIndex) {
		List<InProgressMasterBalanceHold> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM clientmainbalanceHold ORDER BY accountuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, InProgressMasterBalanceHold.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all clientmainbalanceHold from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<InProgressMasterBalanceHoldHist> getAllMasterBalanceHoldHist(int fromIndex, int toIndex) {
		List<InProgressMasterBalanceHoldHist> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM clientmainbalanceholdhistory ORDER BY topuptime DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, InProgressMasterBalanceHoldHist.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all clientmainbalanceholdhistory from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

}

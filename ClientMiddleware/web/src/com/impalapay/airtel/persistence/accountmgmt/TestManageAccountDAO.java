package com.impalapay.airtel.persistence.accountmgmt;

import java.util.List;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;

/**
 * Test our {@link com.impalapay.airtel.persistence.accountmgmt.AccountDAO}
 * <p>
 * Copyright (c) ImpalaPay LTD., June 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class TestManageAccountDAO {

	final String DB_NAME = "remittancedb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "remittance";
	final String DB_PASSWD = "gertedNen2";
	final int DB_PORT = 5432;

	final String ACCOUNT_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f",
			ACCOUNT_UUID2 = "81bf3079-4495-4bec-a50d-c91a7c512d78";

	final String ACCOUNT_STATUS_UUID = "acecb9fa-7e21-455d-8abb-c61a840cdbec",
			ACCOUNT_STATUS_UUID2 = "0b539b9f-8ad1-4c33-910a-642d70012def";

	final String ACCOUNT_NAME = "demo", ACCOUNT_NAME2 = "eugene", ACCOUNT_NAME3 = "James";

	final String ACCOUNT_USERNAME = "demo", ACCOUNT_USERNAME2 = "eugenechi", ACCOUNT_USERNAME3 = "jamesbond";

	final String ACCOUNT_LOGIN_PASSWD = "fe01ce2a7fbac8fafaed7c982a04e229", ACCOUNT_LOGIN_PASSWD2 = "euge",
			ACCOUNT_LOGIN_PASSWD3 = "golden";

	// final int ACCOUNT_COUNTRY_ID = 130, ACCOUNT_COUNTRY_ID2 = 130,
	// ACCOUNT_COUNTRY_ID3 = 131;

	// final int ACCOUNT_LANGUAGE_ID = 170, ACCOUNT_LANGUAGE_ID2 = 170,
	// ACCOUNT_LANGUAGE_ID3 = 171;

	final int ACCOUNT_COUNT = 5;

	private ManageAccountDAO storage;

	/**
	 * Test method for
	 * {@link com.impalapay.airtel.persistence.accountmgmt.AccountDAO#getAccount(int)}.
	 */
	@Ignore
	@Test
	public void testGetAccountUuid() {
		storage = new ManageAccountDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		ManagementAccount a = storage.getAccount(ACCOUNT_UUID);

		assertEquals(a.getUuid(), ACCOUNT_UUID);
		assertEquals(a.getAccountStatusUuid(), ACCOUNT_STATUS_UUID);
		assertEquals(a.getAccountName(), ACCOUNT_NAME);
		assertEquals(a.getUsername(), ACCOUNT_USERNAME);
		assertEquals(a.getLoginPasswd(), ACCOUNT_LOGIN_PASSWD);

	}

	/**
	 * Test method for
	 * {@link com.impalapay.airtel.persistence.accountmgmt.AccountDAO#getAccountName(java.lang.String)}.
	 */
	@Ignore
	@Test
	public void testGetAccountName() {
		storage = new ManageAccountDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		ManagementAccount a = storage.getAccountName(ACCOUNT_USERNAME);

		assertEquals(a.getUuid(), ACCOUNT_UUID);
		assertEquals(a.getAccountStatusUuid(), ACCOUNT_STATUS_UUID);
		assertEquals(a.getAccountName(), ACCOUNT_NAME);
		assertEquals(a.getUsername(), ACCOUNT_USERNAME);
		assertEquals(a.getLoginPasswd(), ACCOUNT_LOGIN_PASSWD);

	}

	/**
	 * Test method for
	 * {@link com.impalapay.airtel.persistence.accountmgmt.AccountDAO#getAllAccounts()}.
	 */
	@Ignore
	@Test
	public void testGetAllAccounts() {
		storage = new ManageAccountDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		List<ManagementAccount> list = storage.getAllAccounts();
		// assertEquals(list.size(), ACCOUNT_COUNT);

		System.out.println(list.size());
		System.out.println(list.get(2));
	}

	/**
	 * Test method for
	 * {@link com.impalapay.airtel.persistence.accountmgmt.AccountDAO#addAccount(com.impalapay.hub.beans.accountmgmt.Account)}.
	 */
	@Ignore
	@Test
	public void testAddAccount() {
		storage = new ManageAccountDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		ManagementAccount a = new ManagementAccount();
		a.setUuid(ACCOUNT_UUID2);
		a.setAccountStatusUuid(ACCOUNT_STATUS_UUID2);
		a.setAccountName(ACCOUNT_NAME2);
		a.setUsername(ACCOUNT_USERNAME2);
		a.setLoginPasswd(ACCOUNT_LOGIN_PASSWD2);
		a.setUpdateforex(true);
		a.setUpdatereversal(true);
		a.setUpdatebalance(true);
		a.setChecker(true);

		assertTrue(storage.addAccount(a));

	}

	/**
	 * Test method for
	 * {@link AccountDAO#updateAccount(java.lang.String, com.impalapay.hub.beans.accountmgmt.Account) }.
	 */
	// @Ignore
	@Test
	public void testUpdateAccount() {
		storage = new ManageAccountDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		ManagementAccount a = new ManagementAccount();
		a.setUuid(ACCOUNT_UUID2);
		a.setAccountStatusUuid(ACCOUNT_STATUS_UUID);

		a.setAccountName(ACCOUNT_NAME3);
		a.setUsername(ACCOUNT_USERNAME3);
		a.setLoginPasswd(ACCOUNT_LOGIN_PASSWD3);
		a.setUpdateforex(false);
		a.setUpdatebalance(false);
		a.setUpdatereversal(false);
		a.setChecker(true);

		assertTrue(storage.updateAccount(ACCOUNT_UUID2, a));
	}

}

package com.impalapay.airtel.persistence.forex;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.forex.ForexEngine;
import com.impalapay.airtel.beans.forex.ForexEngineHistory;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.persistence.GenericDAO;

public class ForexEngineDAO extends GenericDAO implements AirtelForexEngine {

	private static ForexEngineDAO usdforexDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	public static ForexEngineDAO getinstance() {

		if (usdforexDAO == null) {
			usdforexDAO = new ForexEngineDAO();
		}

		return usdforexDAO;
	}

	protected ForexEngineDAO() {
		super();

	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public ForexEngineDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);
	}

	@Override
	public ForexEngine getForex(String uuid) {
		ForexEngine s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forexrate WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, ForexEngine.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting forex with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	/**
	 * 
	 */
	@Override
	public ForexEngine getCurrencyPairForex(String currencypair) {
		ForexEngine s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forexrate WHERE currencypair = ?;");
			pstmt.setString(1, currencypair);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, ForexEngine.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting forexrate with currencypair '" + currencypair + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public boolean PutForex(ForexEngine forex) {
		boolean success = true;
		Date date;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO forexrate(uuid,currencypair," + "marketrate,spreadrate) VALUES (?, ?, ?, ?);");

			pstmt.setString(1, forex.getUuid());
			pstmt.setString(2, forex.getCurrencypair());
			pstmt.setDouble(3, forex.getMarketrate());
			pstmt.setDouble(4, forex.getSpreadrate());
			// pstmt.setTimestamp(5,new
			// Timestamp(forex.getUploadDate().getTime()));

			pstmt.execute();

			// introduced to take care of adding to forex history.
			pstmt2 = conn.prepareStatement("INSERT INTO forexratehistory(uuid,currencypair,"
					+ "marketrate,spreadrate,uploaddate) VALUES (?, ?, ?, ?, ?);");

			pstmt2.setString(1, UUID.randomUUID().toString());
			pstmt2.setString(2, forex.getCurrencypair());
			pstmt2.setDouble(3, forex.getMarketrate());
			pstmt2.setDouble(4, forex.getSpreadrate());
			pstmt2.setTimestamp(5, new Timestamp(System.currentTimeMillis()));

			pstmt2.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + forex);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}
			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<ForexEngine> getAllForex() {
		List<ForexEngine> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forexrate ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, ForexEngine.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all forexrates.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public boolean UpdateForex(String forexuuid, ForexEngine forex) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null, pstmt3 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forexrate WHERE uuid=?;");
			pstmt.setString(1, forexuuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				pstmt2 = conn.prepareStatement(
						"UPDATE forexrate SET currencypair=?," + "marketrate=?,spreadrate=? WHERE uuid=?;");

				pstmt2.setString(1, forex.getCurrencypair());
				pstmt2.setDouble(2, forex.getMarketrate());
				pstmt2.setDouble(3, forex.getSpreadrate());
				pstmt2.setString(4, forexuuid);

				pstmt2.executeUpdate();

				// introduced to take care of adding to forex history.
				pstmt3 = conn.prepareStatement("INSERT INTO forexratehistory(uuid,currencypair,"
						+ "marketrate,spreadrate,uploaddate) VALUES (?, ?, ?, ?, ?);");

				pstmt3.setString(1, UUID.randomUUID().toString());
				pstmt3.setString(2, forex.getCurrencypair());
				pstmt3.setDouble(3, forex.getMarketrate());
				pstmt3.setDouble(4, forex.getSpreadrate());
				pstmt3.setTimestamp(5, new Timestamp(System.currentTimeMillis()));

				pstmt3.execute();

			} else {
				// PutUsdForex(forex);
			}

		} catch (SQLException e) {
			logger.error(
					"SQLException when trying to update usdforex with uuid '" + forexuuid + "' with " + forex + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt3 != null) {
				try {
					pstmt3.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	/**
	 * @Override public boolean UpdateForex2(String countryuuid, ForexEngine forex)
	 *           { boolean success = true;
	 * 
	 *           Connection conn = null; PreparedStatement pstmt = null, pstmt2 =
	 *           null, pstmt3 = null; ResultSet rset = null;
	 * 
	 *           try { conn = dbCredentials.getConnection(); pstmt = conn
	 *           .prepareStatement("SELECT * FROM forexrate WHERE countryuuid=?;");
	 *           pstmt.setString(1, countryuuid);
	 * 
	 *           rset = pstmt.executeQuery();
	 * 
	 *           if (rset.next()) { pstmt2 = conn .prepareStatement("UPDATE
	 *           forexrate SET" + "baserate=?,impalarate=? WHERE countryuuid=?;");
	 * 
	 *           //pstmt2.setString(1, forex.getCountryUuid()); pstmt2.setDouble(1,
	 *           forex.getBaserate()); pstmt2.setDouble(2, forex.getImpalarate());
	 *           pstmt2.setString(3, forex.getCountryUuid());
	 * 
	 *           pstmt2.executeUpdate();
	 * 
	 *           //introduced to take care of adding to forex history. pstmt3 =
	 *           conn.prepareStatement("INSERT INTO forexhistory(uuid,countryuuid,"
	 *           + "baserate,impalarate,currencytype,uploaddate) VALUES (?, ?, ?, ?,
	 *           ?, ?);");
	 * 
	 *           pstmt3.setString(1,UUID.randomUUID().toString());
	 *           pstmt3.setString(2,forex.getCountryUuid()); pstmt3.setDouble(3,
	 *           forex.getBaserate()); pstmt3.setDouble(4, forex.getImpalarate());
	 *           pstmt3.setString(5, "USD"); pstmt3.setTimestamp(6, new
	 *           Timestamp(System.currentTimeMillis()));
	 * 
	 * 
	 *           pstmt3.execute();
	 * 
	 * 
	 *           } else { PutForex(forex); }
	 * 
	 *           } catch (SQLException e) { logger.error("SQLException when trying
	 *           to update usdforex with uuid '" + countryuuid + "' with " + forex +
	 *           "."); logger.error(ExceptionUtils.getStackTrace(e)); success =
	 *           false;
	 * 
	 *           } finally { if (rset != null) { try { rset.close(); } catch
	 *           (SQLException e) { } }
	 * 
	 *           if (pstmt != null) { try { pstmt.close(); } catch (SQLException e)
	 *           { } }
	 * 
	 *           if (pstmt2 != null) { try { pstmt2.close(); } catch (SQLException
	 *           e) { } }
	 * 
	 *           if (pstmt3 != null) { try { pstmt3.close(); } catch (SQLException
	 *           e) { } }
	 * 
	 *           if (conn != null) { try { conn.close(); } catch (SQLException e) {
	 *           } } }
	 * 
	 *           return success; }
	 **/

	@Override
	public boolean UpdateForex2(String countryuuid, ForexEngine forex) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<ForexEngine> getAllForex(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		List<ForexEngine> list = new LinkedList<>();

		// Transaction transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forexrate ORDER BY currencypair DESC LIMIT ? OFFSET ? ;");

			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();
			list = beanProcessor.toBeanList(rset, ForexEngine.class);
			/**
			 * while (rset.next()) { list = beanProcessor.toBeanList(rset,
			 * ForexEngine.class); // transaction = b.toBean(rset, Transaction.class); //
			 * transaction.setId(rset.getInt("sessionid"));
			 * 
			 * // list.add(transaction); }
			 **/

		} catch (SQLException e) {
			logger.error(
					"SQLException exception while getting forexrate with uuid  from " + fromIndex + " to " + toIndex);
			logger.error(e.toString());

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}
}

package com.impalapay.airtel.persistence.forex;

import java.util.List;

import com.impalapay.airtel.beans.forex.Forex;
import com.impalapay.airtel.beans.forex.ForexHistory;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.transaction.Transaction;

public interface AirtelForexHistory {

	/**
	 * Retrieve forex history corresponding to the UUID
	 * 
	 * @param uuid
	 * 
	 */

	public ForexHistory getForexHistory(String uuid);

	/**
	 * Retrieve forex history corresponding to the country UUID
	 * 
	 * @param Country
	 * 
	 */

	public ForexHistory getCountryForexHistory(Country country);

	/**
	 * Retrieve all forex history
	 * 
	 * @return<list>
	 */

	public List<ForexHistory> getAllForexHistory();

	public boolean PutForexHistory(ForexHistory forex);

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<ForexHistory> getAllForexHistory(int fromIndex, int toIndex);

}

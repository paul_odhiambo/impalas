package com.impalapay.airtel.persistence.forex;

import java.util.List;

import com.impalapay.airtel.beans.forex.ForexEngine;
import com.impalapay.airtel.beans.forex.ForexEngineHistory;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.beans.network.Network;

public interface AirtelForexEngineHistory {

	/**
	 * Retrieve forex history corresponding to the UUID
	 * 
	 * @param uuid
	 * 
	 */

	public ForexEngineHistory getForexHistory(String uuid);

	/**
	 * Retrieve forex history corresponding to the country UUID
	 * 
	 * @param Country
	 * 
	 */

	public ForexEngineHistory getCountryForexHistory(Country country);

	/**
	 * Retrieve all forex history
	 * 
	 * @return<list>
	 */

	public List<ForexEngineHistory> getAllForexHistory();

	public boolean PutForexHistory(ForexEngineHistory forex);

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<ForexEngineHistory> getAllForexHistory(int fromIndex, int toIndex);

	// checker forex
	/**
	 * 
	 * @param uuid
	 * @param forex
	 * @return
	 */
	public boolean UpdateCheckerForex(String uuid, ForexEngineHistory forex);

	/**
	 * 
	 * @return
	 */
	public List<ForexEngineHistory> getAllCheckerForexRate();

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public ForexEngineHistory getCheckerForexHistory(String uuid);

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public ForexEngineHistory getCheckerForexHistoryBYUUID(String uuid);

	/**
	 * 
	 * @param forex
	 * @return
	 */
	public boolean PutCheckerForexHistory(ForexEngineHistory forex);

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<ForexEngineHistory> getAllCheckerForexHistory(int fromIndex, int toIndex);

	public List<ForexEngine> getAllCheckerForex(int fromIndex, int toIndex);

	public boolean deleteCheckerForexRate(String uuid);

	// new addition.
	public List<ForexEngineHistory> getAllForexHistory(String currencypair, double marketrate);

}

package com.impalapay.airtel.persistence.forex;

import java.util.List;

import com.impalapay.airtel.beans.forex.ForexEngine;
import com.impalapay.airtel.beans.forex.ForexEngineHistory;
import com.impalapay.airtel.beans.geolocation.Country;

public interface AirtelForexEngine {

	/**
	 * Retrieve forex corresponding to the UUID
	 * 
	 * @param uuid
	 * 
	 */

	public ForexEngine getForex(String uuid);

	/**
	 * Retrieve forex corresponding to the country UUID
	 * 
	 * @param Country
	 * 
	 */

	public ForexEngine getCurrencyPairForex(String currencypair);

	/**
	 * @param forex
	 * @return whether the action was successful or not
	 */
	public boolean PutForex(ForexEngine forex);

	/**
	 * Retrieve forex from all countries
	 * 
	 * @return<list>
	 */

	public List<ForexEngine> getAllForex();

	public List<ForexEngine> getAllForex(int fromIndex, int toIndex);

	public boolean UpdateForex(String forexuuid, ForexEngine forex);

	public boolean UpdateForex2(String countryuuid, ForexEngine forex);

}

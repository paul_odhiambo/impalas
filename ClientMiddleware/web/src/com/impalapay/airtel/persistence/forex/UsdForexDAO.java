package com.impalapay.airtel.persistence.forex;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.forex.Forex;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.persistence.GenericDAO;

public class UsdForexDAO extends GenericDAO implements AirtelForex {

	private static UsdForexDAO usdforexDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	public static UsdForexDAO getinstance() {

		if (usdforexDAO == null) {
			usdforexDAO = new UsdForexDAO();
		}

		return usdforexDAO;
	}

	protected UsdForexDAO() {
		super();

	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public UsdForexDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);
	}

	@Override
	public Forex getUsdForex(String uuid) {
		Forex s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM usdforex WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, Forex.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting forex with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	/**
	 * 
	 */
	@Override
	public Forex getCountryUsdForex(Country country) {
		Forex s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM usdforex WHERE countryuuid = ?;");
			pstmt.setString(1, country.getUuid());

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, Forex.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting forex with countryuuid '" + country + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public boolean PutUsdForex(Forex forex) {
		boolean success = true;
		Date date;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO usdforex(uuid,countryuuid," + "baserate,impalarate) VALUES (?, ?, ?, ?);");

			pstmt.setString(1, forex.getUuid());
			pstmt.setString(2, forex.getCountryUuid());
			pstmt.setDouble(3, forex.getBaserate());
			pstmt.setDouble(4, forex.getImpalarate());
			// pstmt.setTimestamp(5,new Timestamp(forex.getUploadDate().getTime()));

			pstmt.execute();

			// introduced to take care of adding to forex history.
			pstmt2 = conn.prepareStatement("INSERT INTO forexhistory(uuid,countryuuid,"
					+ "baserate,impalarate,currencytype,uploaddate) VALUES (?, ?, ?, ?, ?, ?);");

			pstmt2.setString(1, UUID.randomUUID().toString());
			pstmt2.setString(2, forex.getCountryUuid());
			pstmt2.setDouble(3, forex.getBaserate());
			pstmt2.setDouble(4, forex.getImpalarate());
			pstmt2.setString(5, "USD");
			pstmt2.setTimestamp(6, new Timestamp(System.currentTimeMillis()));

			pstmt2.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + forex);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}
			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<Forex> getAllUsdForex() {
		List<Forex> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM usdforex ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, Forex.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all clientipaddress.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public boolean UpdateUsdForex(String forexuuid, Forex forex) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null, pstmt3 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM usdforex WHERE uuid=?;");
			pstmt.setString(1, forexuuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				pstmt2 = conn.prepareStatement(
						"UPDATE usdforex SET countryuuid=?," + "baserate=?,impalarate=? WHERE uuid=?;");

				pstmt2.setString(1, forex.getCountryUuid());
				pstmt2.setDouble(2, forex.getBaserate());
				pstmt2.setDouble(3, forex.getImpalarate());
				pstmt2.setString(4, forexuuid);

				pstmt2.executeUpdate();

				// introduced to take care of adding to forex history.
				pstmt3 = conn.prepareStatement("INSERT INTO forexhistory(uuid,countryuuid,"
						+ "baserate,impalarate,currencytype,uploaddate) VALUES (?, ?, ?, ?, ?, ?);");

				pstmt3.setString(1, UUID.randomUUID().toString());
				pstmt3.setString(2, forex.getCountryUuid());
				pstmt3.setDouble(3, forex.getBaserate());
				pstmt3.setDouble(4, forex.getImpalarate());
				pstmt3.setString(5, "USD");
				pstmt3.setTimestamp(6, new Timestamp(System.currentTimeMillis()));

				pstmt3.execute();

			} else {
				// PutUsdForex(forex);
			}

		} catch (SQLException e) {
			logger.error(
					"SQLException when trying to update usdforex with uuid '" + forexuuid + "' with " + forex + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt3 != null) {
				try {
					pstmt3.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public boolean UpdateUsdForex2(String countryuuid, Forex forex) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null, pstmt3 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM usdforex WHERE countryuuid=?;");
			pstmt.setString(1, countryuuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				pstmt2 = conn.prepareStatement("UPDATE usdforex SET" + "baserate=?,impalarate=? WHERE countryuuid=?;");

				// pstmt2.setString(1, forex.getCountryUuid());
				pstmt2.setDouble(1, forex.getBaserate());
				pstmt2.setDouble(2, forex.getImpalarate());
				pstmt2.setString(3, forex.getCountryUuid());

				pstmt2.executeUpdate();

				// introduced to take care of adding to forex history.
				pstmt3 = conn.prepareStatement("INSERT INTO forexhistory(uuid,countryuuid,"
						+ "baserate,impalarate,currencytype,uploaddate) VALUES (?, ?, ?, ?, ?, ?);");

				pstmt3.setString(1, UUID.randomUUID().toString());
				pstmt3.setString(2, forex.getCountryUuid());
				pstmt3.setDouble(3, forex.getBaserate());
				pstmt3.setDouble(4, forex.getImpalarate());
				pstmt3.setString(5, "USD");
				pstmt3.setTimestamp(6, new Timestamp(System.currentTimeMillis()));

				pstmt3.execute();

			} else {
				PutUsdForex(forex);
			}

		} catch (SQLException e) {
			logger.error(
					"SQLException when trying to update usdforex with uuid '" + countryuuid + "' with " + forex + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt3 != null) {
				try {
					pstmt3.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}
}

package com.impalapay.airtel.persistence.forex;

import java.util.List;

import com.impalapay.airtel.beans.forex.Forex;
import com.impalapay.airtel.beans.geolocation.Country;

public interface AirtelForex {

	/**
	 * Retrieve forex corresponding to the UUID
	 * 
	 * @param uuid
	 * 
	 */

	public Forex getUsdForex(String uuid);

	/**
	 * Retrieve forex corresponding to the country UUID
	 * 
	 * @param Country
	 * 
	 */

	public Forex getCountryUsdForex(Country country);

	/**
	 * @param forex
	 * @return whether the action was successful or not
	 */
	public boolean PutUsdForex(Forex forex);

	/**
	 * Retrieve forex from all countries
	 * 
	 * @return<list>
	 */

	public List<Forex> getAllUsdForex();

	public boolean UpdateUsdForex(String forexuuid, Forex forex);

	public boolean UpdateUsdForex2(String countryuuid, Forex forex);

}

package com.impalapay.airtel.persistence.forex;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.impalapay.airtel.beans.forex.ForexEngineHistory;
import com.impalapay.airtel.beans.geolocation.Country;

public class TestForexEngineHistoryDAO {

	final String DB_NAME = "remittancedb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "remittance";
	final String DB_PASSWD = "gertedNen2";
	final int DB_PORT = 5432;

	final String FOREXHISTORY_UUID = "266fdbf2-c27f-49f9-b7b6-df006ca447cb";
	final String COUNTRY_UUID = "d4a676822f4546a0bee789e83070f788";
	final String BASERATE = "122";
	final String IMPALARATE = "91";
	final int COUNT = 2;
	final Date USD_DATE_NEW = new Date(new Long("1367597206000").longValue()); // Fri
																				// May
																				// 03
																				// 19:06:46
																				// EAT
																				// 2013

	final String UPDATE_BASERATE = "100";

	final String UPDATE_IMPALARATE = "115";

	private ForexEngineHistoryDAO storage;

	/**
	 * Test method for
	 * com.impalapay.airtel.persistence.forex.ForexHistoryDAO#getForexHistory
	 * (java.lang.String).
	 */
	@Ignore
	@Test
	public void testUsdForexString() {
		storage = new ForexEngineHistoryDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		ForexEngineHistory forex = storage.getForexHistory(FOREXHISTORY_UUID);
		assertEquals(forex.getMarketrate(), BASERATE);

	}

	/**
	 * Test method for
	 * com.impalapay.airtel.persistence.forex.ForexHistoryDAO#getAllForexHistory
	 */
	@Test
	@Ignore
	public void testGetAllForexHistory() {
		storage = new ForexEngineHistoryDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		List<ForexEngineHistory> list = storage.getAllForexHistory();
		assertEquals(list.size(), COUNT);

	}

	@Test
	// @Ignore
	public void testGetAllForexHistoryLimit() {
		storage = new ForexEngineHistoryDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		List<ForexEngineHistory> list = storage.getAllCheckerForexHistory(0, 15);
		// assertEquals(list.size(), COUNT);
		System.out.println(list.size());

	}

	@Test
	@Ignore
	public void testgetCountryForexHistory() {
		storage = new ForexEngineHistoryDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		Country countrytest = new Country();

		countrytest.setUuid("977f6e8fceed43e0a3c1716750171442");

		ForexEngineHistory forex = storage.getCountryForexHistory(countrytest);

		System.out.println(forex.toString());
	}

	@Test
	// @Ignore
	public void testPutForexHistory() {
		storage = new ForexEngineHistoryDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		ForexEngineHistory forexhistory = new ForexEngineHistory();

		Date danew = new Date();

		forexhistory.setUuid("husdhuihidu53276522");
		forexhistory.setCurrencypair("USD/KES");
		;
		forexhistory.setMarketrate(30);
		forexhistory.setSpreadrate(40);
		forexhistory.setUploadDate(danew);

		assertTrue(storage.PutForexHistory(forexhistory));

	}

}

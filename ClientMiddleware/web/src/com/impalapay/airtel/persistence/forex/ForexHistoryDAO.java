package com.impalapay.airtel.persistence.forex;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.forex.Forex;
import com.impalapay.airtel.beans.forex.ForexHistory;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.persistence.GenericDAO;

public class ForexHistoryDAO extends GenericDAO implements AirtelForexHistory {

	private static ForexHistoryDAO forexhistoryDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	public static ForexHistoryDAO getinstance() {

		if (forexhistoryDAO == null) {
			forexhistoryDAO = new ForexHistoryDAO();
		}

		return forexhistoryDAO;
	}

	protected ForexHistoryDAO() {
		super();

	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public ForexHistoryDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);
	}

	@Override
	public ForexHistory getForexHistory(String uuid) {
		ForexHistory s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forexhistory WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, ForexHistory.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting forexhistory with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public ForexHistory getCountryForexHistory(Country country) {
		ForexHistory s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forexhistory WHERE countryuuid = ?;");
			pstmt.setString(1, country.getUuid());

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, ForexHistory.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting forexhistory with countryuuid '" + country + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public List<ForexHistory> getAllForexHistory() {
		List<ForexHistory> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forexhistory ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, ForexHistory.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all forexhistory.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;

	}

	@Override
	public boolean PutForexHistory(ForexHistory forex) {
		boolean success = true;
		Date date;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("INSERT INTO forexhistory(uuid,countryuuid,"
					+ "baserate,impalarate,currencytype,uploadDate) VALUES (?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, forex.getUuid());
			pstmt.setString(2, forex.getCountryUuid());
			pstmt.setDouble(3, forex.getBaserate());
			pstmt.setDouble(4, forex.getImpalarate());
			pstmt.setString(5, forex.getCurrencytype());
			pstmt.setTimestamp(6, new Timestamp(forex.getUploadDate().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + forex);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<ForexHistory> getAllForexHistory(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		List<ForexHistory> list = new LinkedList<>();

		// Transaction transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM forexhistory ORDER BY  uploadDate DESC LIMIT ? OFFSET ? ;");

			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				list = beanProcessor.toBeanList(rset, ForexHistory.class);
				// transaction = b.toBean(rset, Transaction.class);
				// transaction.setId(rset.getInt("sessionid"));

				// list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting forex history with uuid  from " + fromIndex + " to "
					+ toIndex);
			logger.error(e.toString());

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

}

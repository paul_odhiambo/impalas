package com.impalapay.airtel.persistence.forex;

import java.util.List;

import com.impalapay.airtel.beans.forex.Forex;
import com.impalapay.airtel.beans.geolocation.Country;

public interface AirtelForexGBP {

	/**
	 * Retrieve forex corresponding to the UUID
	 * 
	 * @param uuid
	 * 
	 */

	public Forex getGbpForex(String uuid);

	/**
	 * Retrieve forex corresponding to the country UUID
	 * 
	 * @param Country
	 * 
	 */

	public Forex getCountryGbpForex(Country country);

	/**
	 * @param forex
	 * @return whether the action was successful or not
	 */
	public boolean PutGbpForex(Forex forex);

	/**
	 * Retrieve forex from all countries
	 * 
	 * @return<list>
	 */

	public List<Forex> getAllGbpForex();

	public boolean UpdateGbpForex(String forexuuid, Forex forex);

	public boolean UpdateGbpForex2(String countryuuid, Forex forex);

}

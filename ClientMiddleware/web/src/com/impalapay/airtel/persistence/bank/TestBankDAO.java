package com.impalapay.airtel.persistence.bank;

import java.util.Date;
import java.util.List;

import com.impalapay.airtel.beans.bank.BankCodes;
import com.impalapay.airtel.beans.geolocation.CountryMsisdn;
import com.impalapay.airtel.beans.thirdreference.ThirdPartyReference;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.persistence.geolocation.CountryMsisdnDAO;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class TestBankDAO {
	/**
	 * final String DB_NAME = "airteldblive"; final String DB_HOST = "localhost";
	 * final String DB_USERNAME = "airtellive"; final String DB_PASSWD =
	 * "ThejKoyb34";
	 **/
	final String DB_NAME = "remittancedb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "remittance";
	final String DB_PASSWD = "gertedNen2";
	final int DB_PORT = 5432;

	final String TRANSACTION_UUID = "61797229-eb8b-4f84-bb15-a0410dc5d33b";
	final String TRANSACTION_REF = "fc47bd5c-7fdd-4def-92d7-f2b840aebe0f";
	final String TRANSACTION_REF2 = "fc47b-7fdd-4def-92d7-f2b840aebe0f";
	final String TRANSCONFIRM_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String TRANSACTIONSTATUS_UUID = "3b6edb35-654d-4049-b7ea-0f1db29c6e77";
	final int ID = 1;
	final String UUID = "c089e01983d744fab21ae34982f174e0";
	final String DEMO_ACCOUNT_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String NEW_ACCOUNT_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";

	final String NEWUUID = "c089e01983d744fab21ae34982f174e0";
	final String NEWUUID2 = "1983d744fab21ae34982f174e0";
	final String SENDER_TOKEN = "f889-811a-1c66c7655a7f";
	final String SENDERNAME = "eugene chimita";
	final String NEWSOURCE_COUNTRYCODE = "KE";
	final String NEW_RECIPIENTCODE = "d4a676822f4546a0bee789e83070f788";
	final String NEW_RECIPIENTMOBILE = "254-715-266-678";
	final String NEW_CURRENCYCODE = "USD";
	final Date TRANSACTION_DATE_NEW = new Date(new Long("1367597206000").longValue()); // Fri May 03 19:06:46 EAT
																						// 2013;
	final Date TRANSACTION_DATE_VALID = new Date(new Long("1360065927000").longValue()); // 2013-02-05 15:05:27 (Feb
	final String NEWDATES = "2013-02-05 15:05:27"; // 5th)
	final int AMOUNT = 500;
	final String NETWORK_UUID = "602c66b6-83a4-46f4-a109-17a3bcd8d70b";

	final int TRANSACTION_COUNT = 1;

	private BankCodesDAO storage = new BankCodesDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

	/**
	 * Test method for ThirdReferenceDAO#getReference(String)
	 */

	@Ignore
	@Test
	public void testGetbankTransactionByUuid() {

		List<BankCodes> reference = storage.getBankCodes(UUID);

		// assertEquals(transaction, 3);

		for (BankCodes data : reference) {
			System.out.println(data);
		}
	}

	/**
	 * Test method for {@link TransactionDAO#addTransaction(Transaction)}
	 * 
	 **/
	// @Ignore
	@Test
	public void testAddbankCodes() {

		BankCodes saved = new BankCodes();

		Date now = new Date();
		// saved.setId(ID);
		saved.setUuid(NEWUUID);
		saved.setBankname("KCB");
		// saved.setCountryuuid("d4a676822f4546a0bee789e83070f788");
		saved.setNetworkuuid("55c65e4e645a4db78356ac74ec0c8dce");
		// saved.setCountrycode("KE");
		saved.setBankcode("KCB456");
		saved.setBranchcode("TRESSWESS");
		saved.setIban("wewrwrw");
		saved.setDateadded(now);

		assertTrue(storage.addBankCodes(saved));
	}

	/**
	 * Test method for ThirdReferenceDAO#getReference(String)
	 */

	@Ignore
	@Test
	public void testGetbankByCode() {

		BankCodes wewe = new BankCodes();

		wewe.setBankcode("CCBZHE");

		BankCodes reference = storage.getBankCodes(wewe);

		// assertEquals(transaction, 3);

		System.out.println(reference.getBankname());

	}

	/**
	 * 
	 */
	@Ignore
	@Test
	public void testUpdatebankCode() {

		BankCodes wewe = new BankCodes();

		wewe.setUuid(NEWUUID);
		wewe.setBankname("China Trade Bank");
		// wewe.setCountrycode("CN");
		wewe.setBankcode("CCBZHE");

		assertTrue(storage.updateBankCodes(NEWUUID, wewe));

	}

}

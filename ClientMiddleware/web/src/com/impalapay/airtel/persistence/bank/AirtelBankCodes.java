package com.impalapay.airtel.persistence.bank;

import java.util.List;

import com.impalapay.airtel.beans.bank.BankCodes;

public interface AirtelBankCodes {

	/**
	 * Gets all {@link AirtelBankCodes}s which have an UUID matching the argument.
	 * The list returned is not arranged in any particular order.
	 * 
	 * @param uuid
	 * @return getBankCodes request(s)
	 */
	public List<BankCodes> getBankCodes(String uuid);

	/**
	 * Gets all transaction requests
	 *
	 * @return List<{@link BankCodess}> all transaction requests
	 */
	public List<BankCodes> getAllBankCodes();

	/**
	 * Gets all transactions requests between the specified fromIndex, inclusive,
	 * and toIndex, exclusive.
	 *
	 * @param fromIndex
	 * @param toIndex
	 * 
	 * @return List<{@link AirtelBankCodes}> all TransactionReferences requests
	 */
	public List<BankCodes> getAllBankCodes(int fromIndex, int toIndex);

	public BankCodes getBankCodes(BankCodes bankcodes);

	/**
	 *
	 * @param AirtelBillpaymentCodes
	 * @return BankCodes
	 */
	public boolean addBankCodes(BankCodes bank);

	/**
	 * 
	 * @param uuid
	 * @param bankcodes
	 * @return
	 */
	public boolean updateBankCodes(String uuid, BankCodes bankcodes);

}

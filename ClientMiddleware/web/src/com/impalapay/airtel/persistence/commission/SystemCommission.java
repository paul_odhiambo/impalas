package com.impalapay.airtel.persistence.commission;

import java.util.List;

import com.impalapay.beans.commission.CommisionEstimate;

public interface SystemCommission {

	/**
	 * Retrieve the IP corresponding to the uuid.
	 * 
	 * @param uuid
	 * @return ClientIP
	 */
	public CommisionEstimate getsystemlog(String uuid);

	/**
	 * 
	 * @param systemlog
	 * @return
	 */
	public boolean putCommission(CommisionEstimate systemlog);

	/**
	 * Retrieves all clientips
	 * 
	 * @return List<ClientIp>
	 * 
	 */
	public List<CommisionEstimate> getAllCommission();

	public List<CommisionEstimate> getAllCommission(int fromIndex, int toIndex);

}

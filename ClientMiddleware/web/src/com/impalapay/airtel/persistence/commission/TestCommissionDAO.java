package com.impalapay.airtel.persistence.commission;

import static org.junit.Assert.*;

import java.util.List;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import com.impalapay.airtel.beans.clientipaddress.ClientIP;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.beans.commission.CommisionEstimate;

public class TestCommissionDAO {

	final String DB_NAME = "remittancedb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "remittance";
	final String DB_PASSWD = "gertedNen2";
	final int DB_PORT = 5432;

	final String CLIENTIP_UUID = "hjisuyhdiushusfhu";
	final String Account_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String IPADDRESS_UUID = "7691160b2eb346a88d0100249dba8ae6";
	final String IP_ADDRESS = "192.168.10.7";
	final int IP_COUNT = 1;

	final String REPLACE_IP = "127.168.10.67";

	private CommissionDAO storage;

	@Test
	// @Ignore
	public void testPutClientIp() {
		storage = new CommissionDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		CommisionEstimate clientip = new CommisionEstimate();

		clientip.setUuid("452356525yyyd");
		clientip.setTransactionUuid("13be8eaac8c2449da7de1462ceb1e37f");
		clientip.setCommissioncurrency("KES");
		clientip.setCommision(10);
		clientip.setReceivercommission(20);
		clientip.setReceiverfullamount(120);
		clientip.setStatusuuid("acecb9fa-7e21-455d-8abb-c61a840cdbec");

		assertTrue(storage.putCommission(clientip));

	}

}

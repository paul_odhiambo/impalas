package com.impalapay.airtel.servlet.api.temporarysend;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.CacheManager;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostMinusThread;

public class TrueAfricaQuery extends HttpServlet {
	private PostMinusThread postMinusThread;

	private String statuscode = "00032";

	private String Statusdescription = "INTERNAL_SERVER_ERROR";

	private String SendCash = "";

	private Map<String, String> totrueafrica = new HashMap<>();

	private String CLIENT_URL = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(checkName(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkName(HttpServletRequest request) throws IOException {

		// joined json string
		String join = "";
		JsonElement root = null;

		String responseobject = "";

		// These represent parameters received over the network
		String username = "", password = "", transaction_id = "", remiturl = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ##############################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use disableHtmlEscaping().
		// ##############################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();switchcode
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			password = root.getAsJsonObject().get("password").getAsString();
			transaction_id = root.getAsJsonObject().get("transaction_id").getAsString();
			remiturl = root.getAsJsonObject().get("url").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(transaction_id)
				|| StringUtils.isBlank(remiturl)) {

			expected.put("am_referenceid", username);
			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", Statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// construct json object to be sent to true Africa
		totrueafrica.put("username", username);
		totrueafrica.put("password", password);
		totrueafrica.put("transaction_id", transaction_id);

		String jsonData = g.toJson(totrueafrica);

		// assign the remit url from properties.config
		CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL4");

		postMinusThread = new PostMinusThread(CLIENT_URL, jsonData);

		// capture the switch respoinse.
		responseobject = postMinusThread.doPost();

		// pass the returned json string
		JsonElement roots = new JsonParser().parse(responseobject);

		// exctract a specific json element from the object(status_code)
		String switchresponse = roots.getAsJsonObject().get("status_code").getAsString();

		// exctract a specific json element from the object(status_code)
		String statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

		// map MISSING FIELDS
		if (switchresponse.equalsIgnoreCase("301")) {
			switchresponse = "01029";
		}

		// map INVALID TRANSACTIONID
		if (switchresponse.equalsIgnoreCase("302")) {
			switchresponse = "01029";
		}

		// map SUCCESS TRANSACTION
		if (switchresponse.equalsIgnoreCase("200")) {
			switchresponse = "S000";
			statusdescription = "SUCCESS";
		}

		// map TRANSACTION PENDING
		if (switchresponse.equalsIgnoreCase("303")) {
			switchresponse = "S001";
			statusdescription = "CREDIT_IN_PROGRESS";
		}

		// map FAILED TRANSACTION
		if (switchresponse.equalsIgnoreCase("304")) {
			switchresponse = "00029";
			statusdescription = "FAILED_TRANSACTION";
		}

		String success = "S000";
		String fail = "00029";
		String inprogress = "S001";

		if (switchresponse.equalsIgnoreCase(success)) {

			expected.put("am_timestamp", username);
			expected.put("status_code", switchresponse);
			expected.put("status_description", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (switchresponse.equalsIgnoreCase(fail)) {
			expected.put("am_timestamp", username);
			expected.put("status_code", switchresponse);
			expected.put("status_description", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (switchresponse.equalsIgnoreCase(inprogress)) {
			expected.put("am_timestamp", username);
			expected.put("status_code", switchresponse);
			expected.put("status_description", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("am_timestamp", username);
		expected.put("status_code", switchresponse);
		expected.put("status_description", statusdescription);
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.airtel.servlet.api.status;

import java.util.HashMap;
import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.mno.persistence.accountmgmt.balance.AccountBalanceDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;
import com.impalapay.beans.network.Network;
import com.impalapay.beans.route.RouteDefine;
import com.impalapay.mno.persistence.accountmgmt.balance.NetworkBalanceDAO;
import com.impalapay.mno.persistence.network.NetworkDAO;
import com.impalapay.persistence.routing.RouteDAO;

/**
 * Responsible to dispatch a new Session Id to a client URL.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class TransactionDispatcher extends Thread {

	private Account account;
	private Country coutruuuid;
	private Network network, testnetwork;

	private Transaction transaction;
	private TransactionForexrate transactionforexrate;
	// private TransactionForexDAO transactionforexDAO;
	private AccountBalanceDAO accountbalanceDAO;
	private NetworkBalanceDAO networkbalanceDAO;

	private HashMap<String, String> routeaccountnetworkmap = new HashMap<>();
	private HashMap<String, Boolean> routenetworkuuidmap = new HashMap<>();

	private NetworkDAO networkDAO;
	private RouteDAO routeDAO;
	private double commission;
	private double sentbalance;
	private double percentage;
	private double fullbalance;
	private String value = "";

	/**
	 * 
	 */
	private TransactionDispatcher() {
	}

	/**
	 * @param account
	 */
	public TransactionDispatcher(Transaction transaction, TransactionForexrate transactionforexrate) {
		this.transaction = transaction;
		this.transactionforexrate = transactionforexrate;

		// transactionforexDAO = TransactionForexDAO.getinstance();
		accountbalanceDAO = AccountBalanceDAO.getInstance();
		networkbalanceDAO = NetworkBalanceDAO.getInstance();
		networkDAO = NetworkDAO.getInstance();
		routeDAO = RouteDAO.getInstance();
		commission = 0;
		sentbalance = 0;
		percentage = 0;
		fullbalance = 0;
	}

	/**
	 * 
	 */
	@Override
	public void run() {

		coutruuuid = new Country();
		coutruuuid.setUuid(transaction.getRecipientCountryUuid());
		testnetwork = networkDAO.getNetwork(transaction.getNetworkuuid());

		commission = testnetwork.getCommission();
		sentbalance = transaction.getAmount();

		if (testnetwork.isSupportcommissionpercentage()) {

			// percentage = (commission * 100) / sentbalance;
			percentage = (commission * sentbalance) / 100;
			fullbalance = percentage + sentbalance;
			value = "percent of";

		} else {
			fullbalance = sentbalance + commission;

		}

		network = new Network();
		network.setUuid(transaction.getNetworkuuid());

		account = new Account();
		account.setUuid(transaction.getAccountUuid());

		accountbalanceDAO.deductBalanceByCountry(account, coutruuuid, transaction.getAmount());
		// transactionforexDAO.addTransactionForex(transactionforexrate);

		try {
			networkbalanceDAO.deductBalance(network, fullbalance);

			System.out.println("the amount sent is " + transaction.getAmount() + " the commission charged is "
					+ commission + " " + value + " the total amount deducted is " + fullbalance);

		} catch (Exception e) {
			System.out.println("network balance is below limit");
		}
		// ################################################################
		// Fetch route set up details
		// #################################################################
		List<RouteDefine> routedefine = routeDAO.getAllRoute(account);

		for (RouteDefine routenetworkuuid : routedefine) {
			routeaccountnetworkmap.put(routenetworkuuid.getNetworkUuid(), routenetworkuuid.getUuid());
			routenetworkuuidmap.put(routenetworkuuid.getUuid(), routenetworkuuid.isSupportforex());

		}

		// check if the network is already configured in the routes table if not
		// return error
		String networkrouteuuid = routeaccountnetworkmap.get(transaction.getNetworkuuid());

		// check if route allows for forex conversion.
		boolean forexstatus = routenetworkuuidmap.get(networkrouteuuid);

		/**
		 * if (forexstatus) { accountbalanceDAO.deductBalance(account,
		 * transactionforexrate.getLocalamount(),transactionforexrate.getAccounttype());
		 * 
		 * System.out.println("successfully deducted the master balance"); }
		 **/
		if (accountbalanceDAO.deductBalance(account, transactionforexrate.getLocalamount(),
				transactionforexrate.getAccounttype())) {
			// Action if sucessfull
			// System.out.println("successfully deducted");

		} else {
			// System.out.println("error in deducting");
		}

	}
}

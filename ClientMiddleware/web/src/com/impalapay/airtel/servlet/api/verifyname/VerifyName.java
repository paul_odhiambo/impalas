package com.impalapay.airtel.servlet.api.verifyname;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.sessionlog.SessionLog;
import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.util.PhonenumberSplitUtil;
import com.impalapay.airtel.util.SecurityUtil;
import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.beans.network.Network;
import com.impalapay.beans.prefix.PrefixSplit;
import com.impalapay.beans.route.RouteDefine;
import com.impalapay.persistence.routing.RouteDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for verifying of subscriber details through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class VerifyName extends HttpServlet {

	private PostMinusThread postMinusThread;

	private Cache accountsCache, countryCache, networkCache, prefixCache;

	private SessionLogDAO sessionlogDAO;
	private RouteDAO routeDAO;

	private HashMap<String, String> countryHash = new HashMap<>();

	private HashMap<String, String> countryIp = new HashMap<>();

	private Map<String, String> accountcheckurl = new HashMap<>();

	private String CLIENT_URL = "";

	private String phoneresults = "", networkroute = "", networkrouteuuid = "";

	private HashMap<String, String> networkConfirmUrlmap = new HashMap<>();

	private HashMap<String, String> networkcountrymap = new HashMap<>();

	private HashMap<String, String> networkBridgeConfirmUrlmap = new HashMap<>();

	private HashMap<String, String> networkUsernamemap = new HashMap<>();

	private HashMap<String, String> networkPasswordmap = new HashMap<>();

	private HashMap<String, String> prefixnumbernetworkHashmap = new HashMap<>();

	private HashMap<String, Integer> mobilesplitlenght = new HashMap<>();

	private HashMap<String, String> routeaccountnetworkmap = new HashMap<>();

	private HashMap<String, Boolean> routenetworkuuidmap = new HashMap<>();

	private HashMap<String, String> networksupportaccountcheckmap = new HashMap<>();

	private HashMap<String, String> countryUuid = new HashMap<>();

	private int prefixlength = 0;

	private PhonenumberSplitUtil phonenumbersplit;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

		sessionlogDAO = SessionLogDAO.getInstance();
		routeDAO = RouteDAO.getInstance();

		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
		networkCache = mgr.getCache(CacheVariables.CACHE_NETWORK_BY_UUID);
		prefixCache = mgr.getCache(CacheVariables.CACHE_PREFIX_BY_UUID);
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkName(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkName(HttpServletRequest request) throws IOException {

		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;

		// These represent parameters received over the network
		String username = "", sessionid = "", countrycode = "", firstname = "", secondname = "", lastname = "",
				msisdn = "";

		//
		String receiverusername = "", receiverpassword = "", receivercheckurl = "", receiverbridgecheckurl = "",
				supportaccountcheck = "", responseobject = "", switchstatuscode = "", switchmessage = "",
				statusdescription = "", vendorunique = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ===========================================================================
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ============================================================================

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("api_username").getAsString();

			sessionid = root.getAsJsonObject().get("session_id").getAsString();

			countrycode = root.getAsJsonObject().get("country_code").getAsString();

			firstname = root.getAsJsonObject().get("first_name").getAsString();

			secondname = root.getAsJsonObject().get("second_name").getAsString();

			lastname = root.getAsJsonObject().get("last_name").getAsString();

			msisdn = root.getAsJsonObject().get("mobile_number").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(sessionid) || StringUtils.isBlank(countrycode)
				|| StringUtils.isBlank(firstname) || StringUtils.isBlank(lastname) || StringUtils.isBlank(msisdn)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// =====================================================================
		// Retrieve the account details then check against username and
		// sessionid
		// ======================================================================

		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		if (account == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}
		// test for invalid sessionid
		SessionLog sessionlog = sessionlogDAO.getValidSessionLog(account);

		// ################################################################
		// Guard against all invalid sessionid error
		// ################################################################

		if (sessionlog == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}

		String session = sessionlog.getSessionUuid();

		if (!StringUtils.equals(SecurityUtil.getMD5Hash(sessionid), session)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}

		List keys;

		// fetch from cache
		Country country;
		keys = countryCache.getKeys();

		// place country uuid and country code in a hashmap
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryHash.put(country.getCountrycode(), country.getCurrencycode());
		}

		// country and country verify beneficiary ip
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryIp.put(country.getCountrycode(), country.getCountryverifyip());
		}

		if (!countryHash.containsKey(countrycode)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_COUNTRYCODE);

			return g.toJson(expected);
		}

		// country uuid and country code
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryUuid.put(country.getUuid(), country.getCountrycode());
		}

		// Countrycode and mobile splitlegth
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			mobilesplitlenght.put(country.getCountrycode(), country.getMobilesplitlength());
		}

		Network network;
		// network and confirmurl
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkConfirmUrlmap.put(network.getUuid(), network.getAccountcheckip());
		}
		// network and username
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkUsernamemap.put(network.getUuid(), network.getUsername());
		}

		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkcountrymap.put(network.getUuid(), network.getCountryUuid());
		}

		// network and password
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkPasswordmap.put(network.getUuid(), network.getPassword());
		}

		// network and bridgeconfirmurl
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkBridgeConfirmUrlmap.put(network.getUuid(), network.getBridgeaccountcheckip());
		}

		// *************Prefix Cache****************//
		PrefixSplit prefixsplit;
		// prefixnumber and networkuuid
		keys = prefixCache.getKeys();
		for (Object key : keys) {
			element = prefixCache.get(key);
			prefixsplit = (PrefixSplit) element.getObjectValue();
			prefixnumbernetworkHashmap.put(prefixsplit.getPrefix(), prefixsplit.getNetworkUuid());
		}

		// network and support querying
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networksupportaccountcheckmap.put(network.getUuid(), String.valueOf(network.isSupportaccountcheck()));
		}

		// ################################################################
		// Fetch route set up details
		// #################################################################
		List<RouteDefine> routedefine = routeDAO.getAllRoute(account);

		for (RouteDefine routenetworkuuid : routedefine) {
			routeaccountnetworkmap.put(routenetworkuuid.getNetworkUuid(), routenetworkuuid.getUuid());

		}

		// select the split length
		prefixlength = mobilesplitlenght.get(countrycode);
		// split the received phone number
		phonenumbersplit = new PhonenumberSplitUtil();
		phoneresults = phonenumbersplit.PhonenumberSplitUtil(msisdn, prefixlength);

		// check if the phonenumber matches the ones listed on hashmap.
		if (!prefixnumbernetworkHashmap.containsKey(phoneresults)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_NO_MSISDN_NETWORK_MATCH);

			return g.toJson(expected);
		}

		// choose network which the number belongs to
		networkroute = prefixnumbernetworkHashmap.get(phoneresults);

		// check the url authentication details(username and password exist)
		if (!networkConfirmUrlmap.containsKey(networkroute) || !networkUsernamemap.containsKey(networkroute)
				|| !networkPasswordmap.containsKey(networkroute)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_RECEIVER_ENDPOINT_AUTHENTICATE_ERROR);

			return g.toJson(expected);
		}

		// check if the network is already configured in the routes table if not
		// return error
		networkrouteuuid = routeaccountnetworkmap.get(networkroute);
		if (StringUtils.isEmpty(networkrouteuuid)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_ROUTEDEFINE_ERROR);

			return g.toJson(expected);
		}

		if (!StringUtils.equalsIgnoreCase(countryUuid.get(networkcountrymap.get(networkroute)), countrycode)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_COUNTRY_NO_NETWORK_ERROR
					+ countryUuid.get(networkcountrymap.get(networkroute)));

			return g.toJson(expected);
		}

		// check if route allows for account check/confirmation.
		// First step check if network support quering of url.
		supportaccountcheck = networksupportaccountcheckmap.get(networkroute);
		String booleansupport = "true";

		if (!supportaccountcheck.equalsIgnoreCase(booleansupport)) {
			expected.put("command_status", "Route does not support account check status");
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		receiverusername = networkUsernamemap.get(networkroute);
		receiverpassword = networkPasswordmap.get(networkroute);
		receivercheckurl = networkConfirmUrlmap.get(networkroute);
		receiverbridgecheckurl = networkBridgeConfirmUrlmap.get(networkroute);

		// ====================================
		// to be remove on live
		// toairtel.put("api_username", username);
		// toairtel.put("session_id", "tyte5656");
		// =====================================
		accountcheckurl.put("first_name", firstname);
		accountcheckurl.put("second_name", secondname);
		accountcheckurl.put("last_name", lastname);
		accountcheckurl.put("username", receiverusername);
		accountcheckurl.put("password", receiverpassword);
		accountcheckurl.put("receiveraccount", msisdn);
		accountcheckurl.put("receivercheckurl", receivercheckurl);

		String jsonData = g.toJson(accountcheckurl);

		CLIENT_URL = receiverbridgecheckurl;

		postMinusThread = new PostMinusThread(CLIENT_URL, jsonData);

		// capture the switch response.
		responseobject = postMinusThread.doPost();

		// pass the returned json string
		roots = new JsonParser().parse(responseobject);

		try {

			switchstatuscode = roots.getAsJsonObject().get("status_code").getAsString();

			// switchmessage = roots.getAsJsonObject().get("status_message")
			// .getAsString();

			// extract a specific json element from the object(status_code)
			statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

		} catch (Exception e) {
			switchstatuscode = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_INTERNAL_ERROR;
		}

		JsonObject provitional2 = roots.getAsJsonObject();

		if (provitional2.has("vendor_uniquefields")) {

			vendorunique = roots.getAsJsonObject().get("vendor_uniquefields").getAsString();

			expected.put("vendor_uniquefields", vendorunique);

		}
		expected.put("status_code", switchstatuscode);
		expected.put("status_description", statusdescription);

		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

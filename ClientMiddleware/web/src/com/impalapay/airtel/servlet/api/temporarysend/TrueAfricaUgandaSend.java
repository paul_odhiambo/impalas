package com.impalapay.airtel.servlet.api.temporarysend;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostMinusThread;

public class TrueAfricaUgandaSend extends HttpServlet {
	private Cache accountsCache;

	private PostMinusThread postMinusThread;

	private String statuscode = "00032";

	private String Statusdescription = "INTERNAL_SERVER_ERROR";

	private String SendCash = "";

	private Map<String, String> totrueafrica = new HashMap<>();

	private String CLIENT_URL = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(checkName(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkName(HttpServletRequest request) throws IOException {

		// joined json string
		String join = "";
		JsonElement root = null;

		String responseobject = "";

		// These represent parameters received over the network
		String username = "", password = "", transaction_id = "", source_msisdn = "", beneficiary_msisdn = "",
				amount = "", sendername = "", remiturl = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use disableHtmlEscaping().
		// ###############################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			password = root.getAsJsonObject().get("password").getAsString();
			transaction_id = root.getAsJsonObject().get("transaction_id").getAsString();
			source_msisdn = root.getAsJsonObject().get("source_msisdn").getAsString();
			beneficiary_msisdn = root.getAsJsonObject().get("beneficiary_msisdn").getAsString();
			amount = root.getAsJsonObject().get("amount").getAsString();
			sendername = root.getAsJsonObject().get("Sender_Name").getAsString();
			remiturl = root.getAsJsonObject().get("url").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(transaction_id)
				|| StringUtils.isBlank(source_msisdn) || StringUtils.isBlank(beneficiary_msisdn)
				|| StringUtils.isBlank(amount) || StringUtils.isBlank(sendername) || StringUtils.isBlank(remiturl)) {

			expected.put("am_referenceid", username);
			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", Statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// construct json object to be sent to true Africa
		totrueafrica.put("username", username);
		totrueafrica.put("password", password);
		totrueafrica.put("transaction_id", transaction_id);
		totrueafrica.put("source_msisdn", source_msisdn);
		totrueafrica.put("beneficiary_msisdn", beneficiary_msisdn);
		totrueafrica.put("amount", amount);
		totrueafrica.put("Sender_Name", sendername);

		String jsonData = g.toJson(totrueafrica);

		// assign the remit url from properties.config
		CLIENT_URL = remiturl;

		postMinusThread = new PostMinusThread(CLIENT_URL, jsonData);

		// capture the switch respoinse.
		responseobject = postMinusThread.doPost();

		// pass the returned json string
		JsonElement roots = new JsonParser().parse(responseobject);

		// exctract a specific json element from the object(status_code)
		String switchresponse = roots.getAsJsonObject().get("status_code").getAsString();

		// exctract a specific json element from the object(status_code)
		String statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

		// map AUTHENTICATION_FAILED
		if (switchresponse.equalsIgnoreCase("101")) {
			switchresponse = "01029";
		}

		// map AUTHENTICATION_FAILED
		if (switchresponse.equalsIgnoreCase("102")) {
			switchresponse = "01029";
		}

		// map ACCEPTED FOR PROCESSING
		if (switchresponse.equalsIgnoreCase("200")) {
			switchresponse = "S001";
		}

		// map INVALID MOBILENUMBER
		if (switchresponse.equalsIgnoreCase("203")) {
			switchresponse = "00051";
		}

		// map ACCEPTED FOR INVALID AMOUNT
		if (switchresponse.equalsIgnoreCase("104")) {
			switchresponse = "00028";
		}

		String success = "S001";

		if (switchresponse.equalsIgnoreCase(success)) {

			String thirdreference = roots.getAsJsonObject().get("am_transactionid").getAsString();

			expected.put("am_referenceid", thirdreference);
			expected.put("am_timestamp", username);
			expected.put("status_code", "S001");
			expected.put("status_description", "CREDIT_IN_PROGRESS");
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}
		expected.put("am_referenceid", username);
		expected.put("am_timestamp", username);
		expected.put("status_code", switchresponse);
		expected.put("status_description", statusdescription);
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

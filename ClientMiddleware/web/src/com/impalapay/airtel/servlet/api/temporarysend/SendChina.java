package com.impalapay.airtel.servlet.api.temporarysend;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
//import com.impalapay.airtel.beans.geolocation.Country2;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;

public class SendChina extends HttpServlet {

	private Cache countryalphaCache;

	private PostWithIgnoreSSL postwithignoreSSL;

	private String statuscode = "00032";

	private Map<String, String> totrueafrica = new HashMap<>();
	private HashMap<String, String> countryCodeHash = new HashMap<>();

	private String CLIENT_URL = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);

		countryalphaCache = mgr.getCache(CacheVariables.CACHE_COUNTRYALPHA3_BY_UUID);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(checkName(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkName(HttpServletRequest request) throws IOException {

		// joined json string
		String join = "";
		JsonElement root = null;

		String responseobject = "";

		// These represent parameters received over the network
		String username = "", password = "", transaction_id = "", source_msisdn = "", beneficiary_msisdn = "",
				amount = "", sendername = "", remiturl = "", recipientcurrencycode = "", recipientcountrycode = "";

		// These are the addedparameters.
		String senderfirstname = "", senderlastname = "", senderaddress = "", sendercity = "", recipientfirstname = "",
				recipientlastname = "", accountnumber = "", transactionid = "", switchcode = "", responsedate = "",
				statusdescription = "";

		// Hold values from properties config.
		String destinationagentcode = "", productcode = "", masteragentcode = "", languagecode = "", fee = "", tax = "",
				agentcode = "", rate = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JpostMinusThreadSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use disableHtmlEscaping().
		// ###############################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			password = root.getAsJsonObject().get("password").getAsString();
			transaction_id = root.getAsJsonObject().get("transaction_id").getAsString();
			source_msisdn = root.getAsJsonObject().get("source_msisdn").getAsString();
			beneficiary_msisdn = root.getAsJsonObject().get("beneficiary_msisdn").getAsString();
			amount = root.getAsJsonObject().get("amount").getAsString();
			remiturl = root.getAsJsonObject().get("url").getAsString();
			senderfirstname = root.getAsJsonObject().get("sender_firstname").getAsString();
			senderlastname = root.getAsJsonObject().get("sender_lastname").getAsString();
			senderaddress = root.getAsJsonObject().get("sender_address").getAsString();
			sendercity = root.getAsJsonObject().get("sender_city").getAsString();
			recipientfirstname = root.getAsJsonObject().get("recipient_firstname").getAsString();
			recipientlastname = root.getAsJsonObject().get("recipient_lastname").getAsString();
			accountnumber = root.getAsJsonObject().get("account_number").getAsString();
			recipientcurrencycode = root.getAsJsonObject().get("recipient_currency_code").getAsString();
			recipientcountrycode = root.getAsJsonObject().get("recipient_country_code").getAsString();
			destinationagentcode = root.getAsJsonObject().get("bank_code").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils
				.isBlank(password) /**
									 * || StringUtils.isBlank(transaction_id) || StringUtils.isBlank(source_msisdn)
									 * || StringUtils.isBlank(beneficiary_msisdn) || StringUtils.isBlank(amount) ||
									 * StringUtils.isBlank(sendername) || StringUtils.isBlank(remiturl)
									 **/
		) {

			expected.put("am_referenceid", username);
			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		/**
		 * Element element; List keys;
		 * 
		 * //fetch from cache Country2 country; keys = countryalphaCache.getKeys();
		 * for(Object key : keys) { element = countryalphaCache.get(key); country =
		 * (Country2) element.getObjectValue();
		 * countryCodeHash.put(country.getCountrycode2(), country.getCountrycode3()); }
		 **/

		// construct json object to be sent to true Africa
		// fetch details from properties config file.
		// destinationagentcode =
		// PropertiesConfig.getConfigValue("DESTINATION_AGENT_CODE");

		productcode = PropertiesConfig.getConfigValue("PRODUCTCODE");

		masteragentcode = PropertiesConfig.getConfigValue("MASTER_AGENT_CODE");

		languagecode = PropertiesConfig.getConfigValue("LANG_CODE");

		fee = PropertiesConfig.getConfigValue("FEE");

		tax = PropertiesConfig.getConfigValue("TAX");

		agentcode = PropertiesConfig.getConfigValue("AGENTCODE");

		rate = PropertiesConfig.getConfigValue("EXCHANGE_RATE");

		// server time
		Date now = new Date();

		// https://restsendapitest.centurygroup.hk/

		totrueafrica.put("OriginCountryCode", "ZAF");
		totrueafrica.put("OriginCurrencyCode", "USD");
		totrueafrica.put("DestinationCountryCode", "CHN");
		totrueafrica.put("DestinationCurrencyCode", "USD");// used to
		totrueafrica.put("DestinationAgentCode", destinationagentcode);// CCBZHE
		totrueafrica.put("PayoutReferenceID", "");
		totrueafrica.put("ProductCode", productcode);
		totrueafrica.put("LocalSendDateTime", String.valueOf(now));
		totrueafrica.put("SendingAmount", "0.0");// put
		totrueafrica.put("Fee", fee);
		totrueafrica.put("Tax", tax);
		totrueafrica.put("Discount", "0.0");
		totrueafrica.put("ExchangeRate", rate);
		totrueafrica.put("PayingAmount", amount);// cny layout
		totrueafrica.put("SenderFirstName", senderfirstname);
		totrueafrica.put("SenderLastName", senderlastname);
		totrueafrica.put("SenderCountryCode", "ZAF");
		totrueafrica.put("BeneficiaryFirstName", recipientfirstname);
		totrueafrica.put("BeneficiaryLastName", recipientlastname);
		totrueafrica.put("BeneficiaryMobileNumber", beneficiary_msisdn);
		totrueafrica.put("BeneficiaryCountryCode", "CHN");
		totrueafrica.put("AccountNumber", accountnumber);
		totrueafrica.put("UserName", username);
		totrueafrica.put("Password", password);
		totrueafrica.put("MasterAgentCode", masteragentcode);
		totrueafrica.put("AgentCode", agentcode);
		totrueafrica.put("LangCode", languagecode);

		String jsonData = g.toJson(totrueafrica);

		// String remitsurl =
		// "https://restsendapitest.centurygroup.hk/SendAPI/CreateTransaction";

		// assign the remit url from properties.config
		CLIENT_URL = remiturl;

		postwithignoreSSL = new PostWithIgnoreSSL(CLIENT_URL, jsonData);

		// capture the switch respoinse.
		responseobject = postwithignoreSSL.doPost();

		try {
			// pass the returned json string
			JsonElement roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			switchcode = roots.getAsJsonObject().get("Code").getAsString();

			// exctract a specific json element from the object(status_code)
			transactionid = roots.getAsJsonObject().get("PayoutReferenceID").getAsString();

			responsedate = roots.getAsJsonObject().get("ResponseDate").getAsString();

			statusdescription = roots.getAsJsonObject().get("Message").getAsString();

		} catch (Exception e) {
			// instantiate the JSon
			expected.put("status_code", "0032");
			expected.put("status_description", "INVALID_RESPONSE_FROM_WALLET");
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}
		// start mapping all the error codes
		// map ACCEPTED FOR PROCESSING

		if (switchcode.equalsIgnoreCase("0")) {
			switchcode = "S001";
		}

		// map AUTHENTICATION_FAILED
		if (switchcode.equalsIgnoreCase("900")) {
			switchcode = "01029";
		}

		// map AUTHENTICATION_FAILED
		if (switchcode.equalsIgnoreCase("901")) {
			switchcode = "01029";
		}

		if (switchcode.equalsIgnoreCase("999")) {
			switchcode = "00032";

		}

		if (switchcode.equalsIgnoreCase("201") || switchcode.equalsIgnoreCase("202")
				|| switchcode.equalsIgnoreCase("203") || switchcode.equalsIgnoreCase("204")
				|| switchcode.equalsIgnoreCase("205") || switchcode.equalsIgnoreCase("206")
				|| switchcode.equalsIgnoreCase("207") || switchcode.equalsIgnoreCase("208")
				|| switchcode.equalsIgnoreCase("209") || switchcode.equalsIgnoreCase("203")
				|| switchcode.equalsIgnoreCase("210") || switchcode.equalsIgnoreCase("211")
				|| switchcode.equalsIgnoreCase("212") || switchcode.equalsIgnoreCase("213")
				|| switchcode.equalsIgnoreCase("214") || switchcode.equalsIgnoreCase("215")
				|| switchcode.equalsIgnoreCase("216") || switchcode.equalsIgnoreCase("217")
				|| switchcode.equalsIgnoreCase("902")) {
			switchcode = "00029";
		}

		// return String.valueOf(transactionid);

		String success = "S001";

		if (switchcode.equalsIgnoreCase(success)) {

			expected.put("am_referenceid", transactionid);
			expected.put("am_timestamp", responsedate);
			expected.put("status_code", switchcode);
			expected.put("status_description", "CREDIT_IN_PROGRESS");
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}
		expected.put("am_referenceid", transactionid);
		expected.put("am_timestamp", responsedate);
		expected.put("status_code", switchcode);
		expected.put("status_description", statusdescription);
		String jsonResult = g.toJson(expected);

		return jsonResult;

		// return responseobject.toString();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

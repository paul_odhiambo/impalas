package com.impalapay.airtel.servlet.api.status;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.CurrencyConvertUtil;
import com.impalapay.mno.beans.accountmgmt.balance.ClientAccountBalanceByCountry;
import com.impalapay.mno.beans.accountmgmt.balance.MasterAccountBalance;
import com.impalapay.mno.persistence.accountmgmt.balance.AccountBalanceDAO;
import com.impalapay.mno.servlet.api.remit.AsyncTransactionDispatcher;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.clientipaddress.ClientIP;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.persistence.transaction.TransactionDAO;
import com.impalapay.airtel.persistence.transaction.TransactionStatusDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class EmergencyQueryStatus extends HttpServlet {

	private Cache clientIpCache, transactionStatusCache;

	private TransactionDAO transactionDAO;

	private TransactionStatusDAO transactionstatusDAO;

	private AccountBalanceDAO accountbalanceDAO;

	private HashMap<String, String> clientipHash = new HashMap<>();

	private HashMap<String, String> clientipaccountHash = new HashMap<>();

	private HashMap<String, String> transactionStatusHash = new HashMap<>();

	private Map<String, Double> balancemap = new HashMap<>();

	private List<ClientAccountBalanceByCountry> clientBalances;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		clientIpCache = mgr.getCache(CacheVariables.CACHE_IPADDRESS_BY_UUID);
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
		accountbalanceDAO = AccountBalanceDAO.getInstance();
		transactionDAO = TransactionDAO.getInstance();
		transactionstatusDAO = TransactionStatusDAO.getInstance();
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		Transaction transactions = null;

		TransactionStatus transactionstatus = null, transactionstatustoupdate = null;

		// joined json string
		String join = "";
		JsonElement root = null;

		// These represent parameters received over the network
		String username = "", referencenumber = "", switchresponse = "", transactioinid = "", newstatus = "",
				statusdescription = "", inprogress = "", transtatusuuid = "", jsonResult = "", secretpassword = "",
				transactionref = "", updatetransactionstatus = "", unknownerror = "", originatecurrency = "";

		double impalaexchangecalculate = 0, convertedamountToWallet = 0, amount = 0, originateamount = 0,
				finalconvertedamount = 0, imtmasterbalance = 0, countryamount = 0;

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			secretpassword = root.getAsJsonObject().get("authentication").getAsString();
			originatecurrency = root.getAsJsonObject().get("originate_currency").getAsString();
			originateamount = root.getAsJsonObject().get("sent_amount").getAsDouble();
			impalaexchangecalculate = root.getAsJsonObject().get("applied_exchangerate").getAsDouble();
			referencenumber = root.getAsJsonObject().get("reference_number").getAsString();
			updatetransactionstatus = root.getAsJsonObject().get("transactionstatusuuid").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(referencenumber)) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details then check against username and
		// sessionid
		Element element;

		// ip address module
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}

		List keys;

		// fetch from cache
		ClientIP clientIP;
		keys = clientIpCache.getKeys();
		for (Object key : keys) {
			element = clientIpCache.get(key);
			clientIP = (ClientIP) element.getObjectValue();
			clientipHash.put(clientIP.getUuid(), clientIP.getIpAddress());
		}

		keys = clientIpCache.getKeys();
		for (Object key : keys) {
			element = clientIpCache.get(key);
			clientIP = (ClientIP) element.getObjectValue();
			clientipaccountHash.put(clientIP.getIpAddress(), clientIP.getAccountUuid());
		}

		TransactionStatus status;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status.getUuid(), status.getStatus());
		}

		if (!StringUtils.equals(secretpassword, PropertiesConfig.getConfigValue("STATUS_PASSWORD"))) {
			expected.put("command_status", "AUTHORIZATION_ERROR");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		if (!transactionStatusHash.containsKey(updatetransactionstatus)) {
			expected.put("command_status", "PROVIDED_TRANSACTIONSTATUSUUID_INVALID");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		// compare remote address with the one stored in propertiesconfig
		// if (!clientipHash.containsValue(ip)) {
		// expected.put("your_ip", ip);
		// expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_IPADDRESS);
		// String jsonResult = g.toJson(expected);

		// return jsonResult;

		// }

		transactions = transactionDAO.getTransactionstatus1(referencenumber);

		if (transactions == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_REFERENCENUMBER);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}
		transactionref = transactions.getReferenceNumber();
		transtatusuuid = transactions.getTransactionStatusUuid();
		amount = transactions.getAmount();
		transactionstatus = transactionstatusDAO.getTransactionStatus(transtatusuuid);
		transactionstatustoupdate = transactionstatusDAO.getTransactionStatus(updatetransactionstatus);
		newstatus = transactionstatus.getStatus();

		// set the transaction status in the database to inprogress.
		account = new Account();
		account.setUuid(transactions.getAccountUuid());

		// generate UUID then save transaction and sending to comviva wallet.
		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');
		// server time
		Date now = new Date();

		// Start building the transaction forex object.
		TransactionForexrate transactionratehistory = new TransactionForexrate();

		transactionratehistory.setUuid(transactioinid);
		transactionratehistory.setTransactionUuid(transactions.getUuid());
		transactionratehistory.setAccount(transactions.getAccountUuid());
		transactionratehistory.setRecipientcountry(transactions.getRecipientCountryUuid());
		transactionratehistory.setLocalamount(originateamount);
		transactionratehistory.setAccounttype(originatecurrency);
		transactionratehistory.setConvertedamount(transactions.getAmount());
		transactionratehistory.setImpalarate(impalaexchangecalculate);
		transactionratehistory.setBaserate(0);
		transactionratehistory.setReceivermsisdn(transactions.getRecipientMobile());
		transactionratehistory.setSurplus(0);
		transactionratehistory.setServerTime(now);

		unknownerror = "00032";
		inprogress = "S001";

		// if the update status is credit in progress please perform the following.
		if (transactionStatusHash.get(updatetransactionstatus).equalsIgnoreCase(inprogress)) {

			// ############################################################
			// Check master balance.
			// MasterAccountBalance masterbalance =
			// accountbalanceDAO.getMasterAccountBalance(account);
			// ###########################################################
			MasterAccountBalance masterbalance = accountbalanceDAO.getMasterAccountBalance(account, originatecurrency);

			try {
				imtmasterbalance = masterbalance.getBalance();
				// #######################################################
				// Introduce a check for Master Balance to prevent one from
				// sending what he/She doesnt have
				// #######################################################

				if (imtmasterbalance <= amount) {
					expected.put("command_status", APIConstants.COMMANDSTATUS_LESS_MASTERBALANCE);
					return g.toJson(expected);
				}

			} catch (Exception e) {

				expected.put("command_status", APIConstants.COMMANDSTATUS_NO_MASTERBALANCE);
				return g.toJson(expected);

			}

			convertedamountToWallet = CurrencyConvertUtil.multiplyForex(originateamount, impalaexchangecalculate);

			finalconvertedamount = CurrencyConvertUtil.doubleToInteger(convertedamountToWallet);

			if (amount != finalconvertedamount) {

				// There is a problem with forex
				expected.put("command_status", "FOREX_CALCULATION_ERROR_EXPECTEDCONVERSION_IS " + amount
						+ "AMOUNT_RETURNED NOW IS " + finalconvertedamount);
				jsonResult = g.toJson(expected);
				return jsonResult;

			}

			// Check for the Balance by country
			// ##################################################
			// for a float based system the below is needed
			// ##################################################

			// fetch the list containing balance by country with the respective
			// balances
			try {
				clientBalances = accountbalanceDAO.getClientBalanceByCountry(account);

			} catch (Exception e) {
				expected.put("command_status", APIConstants.COMMANDSTATUS_NO_BALANCE_COUNTRIES);
				return g.toJson(expected);
			}

			// convert the resultant list to a hashmap.
			balancemap = new LinkedHashMap<>();

			for (ClientAccountBalanceByCountry balance : clientBalances)
				balancemap.put(balance.getCountryUuid(), balance.getBalance());
			try {

				countryamount = balancemap.get(transactions.getRecipientCountryUuid());

			} catch (Exception e) {
				expected.put("command_status", APIConstants.COMMANDSTATUS_NO_BALANCE);
				return g.toJson(expected);

			}

			if (countryamount <= convertedamountToWallet) {
				expected.put("command_status", APIConstants.STATUS_CODE_INSUFFICIENT_BALANCE);
				return g.toJson(expected);
			}
		}

		if (newstatus.equalsIgnoreCase(unknownerror)) {

			// Update the transaction status to the new one.
			if (!transactionDAO.updateTransactionStatus(transactions.getUuid(), transactionstatustoupdate)) {
				expected.put("command_status", "UNABLE_TO_UPDATE_THE_STATUS_ON_THE_DATABASE");
				jsonResult = g.toJson(expected);
				return jsonResult;
			}

			// update transaction and deduct balance if the transaction is on credit in
			// progress
			transactions = transactionDAO.getTransactionstatus1(referencenumber);
			transtatusuuid = transactions.getTransactionStatusUuid();
			transactionstatus = transactionstatusDAO.getTransactionStatus(transtatusuuid);
			newstatus = transactionstatus.getStatus();

			if (newstatus.equalsIgnoreCase(inprogress)) {

				// *************************************************************
				// Save the Transaction ForexRate
				// *************************************************************
				new AsyncTransactionDispatcher(transactionratehistory).start();
				expected.put("transaction_id", transactioinid);
				expected.put("command_status", APIConstants.COMMANDSTATUS_OK);
				expected.put("status_code", switchresponse);
				expected.put("remit_status", "SUCCESSFUL_ASYNCOPERATION");
				jsonResult = g.toJson(expected);
				// return jsonResult;
			} else {
				expected.put("command_status", APIConstants.COMMANDSTATUS_OK);
				expected.put("status_code", switchresponse);
				expected.put("remit_status", "SUCCESSFUL_OPERATION");
				jsonResult = g.toJson(expected);
			}

		} else {
			// This operation is not allowed
			expected.put("transaction_id", transactioinid);
			expected.put("command_status", APIConstants.COMMANDSTATUS_OK);
			expected.put("status_code", switchresponse);
			expected.put("remit_status", "OPARATION_FAILED/NOT_ALLOWED");
			jsonResult = g.toJson(expected);
		}

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

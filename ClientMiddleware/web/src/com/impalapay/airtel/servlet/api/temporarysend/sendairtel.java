package com.impalapay.airtel.servlet.api.temporarysend;

import java.io.IOException;
import java.io.OutputStream;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;

public class sendairtel extends HttpServlet {
	private Cache accountsCache;

	private String statuscode = "00032";

	private String Statusdescription = "INTERNAL_SERVER_ERROR";

	private String SendCash = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkName(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkName(HttpServletRequest request) throws IOException {

		// joined json string
		String join = "";
		JsonElement root = null;

		// These represent parameters received over the network
		String username = "", password = "", transaction_id = "", source_msisdn = "", beneficiary_msisdn = "",
				amount = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// #################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use disableHtmlEscaping().
		// #################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			password = root.getAsJsonObject().get("password").getAsString();
			transaction_id = root.getAsJsonObject().get("transaction_id").getAsString();
			source_msisdn = root.getAsJsonObject().get("source_msisdn").getAsString();
			beneficiary_msisdn = root.getAsJsonObject().get("beneficiary_msisdn").getAsString();
			amount = root.getAsJsonObject().get("amount").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(transaction_id)
				|| StringUtils.isBlank(source_msisdn) || StringUtils.isBlank(beneficiary_msisdn)
				|| StringUtils.isBlank(amount)) {

			expected.put("am_referenceid", username);
			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", Statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details then check against username and
		// sessionid

		SoapCreation sendmoney_airtel = new SoapCreation();
		try {
			SendCash = sendmoney_airtel.QuerryOutputProcess(amount, source_msisdn, beneficiary_msisdn, transaction_id);

		} catch (KeyManagementException e) {
			expected.put("am_referenceid", username);
			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", Statusdescription + "exception1");
			String jsonResult = g.toJson(expected);

			return jsonResult;

		} catch (NoSuchAlgorithmException e) {

			expected.put("am_referenceid", username);
			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", Statusdescription + "exception2");
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}

		String success = "Success";

		if (SendCash.equalsIgnoreCase(success)) {

			expected.put("am_referenceid", username);
			expected.put("am_timestamp", username);
			expected.put("status_code", "S000");
			expected.put("status_description", "SUCCESS");
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}
		expected.put("am_referenceid", username);
		expected.put("am_timestamp", username);
		expected.put("status_code", statuscode);
		expected.put("status_description", "FAIL");
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

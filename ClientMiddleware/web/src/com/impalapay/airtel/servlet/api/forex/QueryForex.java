package com.impalapay.airtel.servlet.api.forex;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.forex.ForexEngine;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.sessionlog.SessionLog;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.util.SecurityUtil;

public class QueryForex extends HttpServlet {
	private Cache accountsCache, countryCache, forexCache;

	private SessionLogDAO sessionlogDAO;

	private HashMap<String, String> countryHash = new HashMap<>();

	private HashMap<String, String> countryCode = new HashMap<>();

	private HashMap<String, String> countryUuid = new HashMap<>();

	private HashMap<String, String> countrycodeUuid = new HashMap<>();

	private HashMap<String, String> forexmarketratemap = new HashMap<>();

	private HashMap<String, String> forexspreadratemap = new HashMap<>();

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);

		forexCache = mgr.getCache(CacheVariables.CACHE_FOREX_BY_UUID);

		sessionlogDAO = SessionLogDAO.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(checkForex(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkForex(HttpServletRequest request) throws IOException {
		Account account = null;

		double baseexchange = 0;

		// These represent parameters received over the network
		String username = "", sessionid = "", basecurrency = "", quotecurrency = "", currencypair = "", rates = "";
		String join = "";
		JsonElement root = null;
		JsonObject root2 = null, forexrates = null;

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ##################################################################
		// instantiate the JSon
		// ##################################################################

		Gson g = new GsonBuilder().setPrettyPrinting().serializeNulls()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		LinkedHashMap<String, String> expected = new LinkedHashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("api_username").getAsString();

			sessionid = root.getAsJsonObject().get("session_id").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(sessionid)) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		if (account == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// test for invalid sessionid
		SessionLog sessionlog = sessionlogDAO.getValidSessionLog(account);

		// ################################################################
		// Guard against all invalid sessionid error
		// ################################################################

		if (sessionlog == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		String session = sessionlog.getSessionUuid();
		if (!StringUtils.equals(SecurityUtil.getMD5Hash(sessionid), session)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}

		List keys;

		// fetch from cache
		Country country;
		keys = countryCache.getKeys();
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryHash.put(country.getCountrycode(), country.getCurrencycode());
		}

		// country and country uuid
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryCode.put(country.getCountrycode(), country.getUuid());
		}
		// country uuid and country code
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryUuid.put(country.getUuid(), country.getCountrycode());
		}

		// countrycode and currencycode
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countrycodeUuid.put(country.getUuid(), country.getCurrencycode());
		}

		// forex with curency pairs
		ForexEngine forexengine;
		keys = forexCache.getKeys();
		for (Object key : keys) {
			element = forexCache.get(key);
			forexengine = (ForexEngine) element.getObjectValue();
			forexmarketratemap.put(forexengine.getCurrencypair(), String.valueOf(forexengine.getMarketrate()));

		}

		keys = forexCache.getKeys();
		for (Object key : keys) {
			element = forexCache.get(key);
			forexengine = (ForexEngine) element.getObjectValue();
			forexspreadratemap.put(forexengine.getCurrencypair(), String.valueOf(forexengine.getSpreadrate()));

		}

		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

		// check if request contains from and to currencies.
		if (root2.has("from") && root2.has("to")) {

			basecurrency = root.getAsJsonObject().get("from").getAsString();
			quotecurrency = root.getAsJsonObject().get("to").getAsString();

			// check if both currency exist
			// checks for the provide currencyCode(invalid)
			if (!countryHash.containsValue(basecurrency) || !countryHash.containsValue(quotecurrency)) {
				expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_CURRENCYCODE);

				return g.toJson(expected);
			}

			currencypair = basecurrency + "/" + quotecurrency;

			rates = forexspreadratemap.get(currencypair);

			expected.put("api_username", username);
			expected.put("server_datetime", ft.format(dNow));
			expected.put("from", basecurrency);
			expected.put("to", quotecurrency);
			expected.put("rate", rates);
			expected.put(currencypair, rates);

			// expected.put("forex_explanation", "1" + basecurrency + " equatesTO " +
			// quotecurrency + " : " + rate);

			expected.put("command_status", APIConstants.COMMANDSTATUS_OK);
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}

		String json = g.toJson(forexspreadratemap);
		JsonParser parser = new JsonParser();
		JsonObject rateobject = parser.parse(json).getAsJsonObject();

		JsonObject forexresult = new JsonObject();
		forexresult.addProperty("api_username", username);
		forexresult.addProperty("server_datetime", ft.format(dNow));

		JsonArray forexarray = new JsonArray();

		forexarray.add(rateobject);
		/**
		 * //clean the forex array JsonObject resultrates=null;
		 * 
		 * for(int i =0; i<forexarray.size(); i++){ resultrates =
		 * forexarray.get(i).getAsJsonObject(); }
		 **/

		forexresult.add("currency_rates", forexarray);

		String json2 = g.toJson(forexresult);

		return json2;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

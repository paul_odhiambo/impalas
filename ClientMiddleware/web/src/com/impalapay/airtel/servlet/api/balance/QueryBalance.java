package com.impalapay.airtel.servlet.api.balance;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.mno.beans.accountmgmt.balance.ClientAccountBalanceByCountry;
import com.impalapay.mno.beans.accountmgmt.balance.MasterAccountBalance;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.sessionlog.SessionLog;
import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.util.SecurityUtil;
import com.impalapay.mno.persistence.accountmgmt.balance.AccountBalanceDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of balance through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryBalance extends HttpServlet {

	private Cache accountsCache, countryCache;

	private SessionLogDAO sessionlogDAO;

	private AccountBalanceDAO accountbalanceDAO;

	private HashMap<String, String> countryHash = new HashMap<>();

	private HashMap<String, String> mainfloatHash = new LinkedHashMap<>();

	private HashMap<String, String> balancemap = new LinkedHashMap<>();

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

		sessionlogDAO = SessionLogDAO.getInstance();

		accountbalanceDAO = AccountBalanceDAO.getInstance();

		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkBalance(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkBalance(HttpServletRequest request) throws IOException {
		Account account = null;
		// joined json string
		String join = "";
		JsonElement root = null;
		JsonObject mainrateobject = null, countryrateobject = null, balanceresult = null;
		JsonArray mainarray, countryarray;

		// These represent parameters received over the network
		String username = "", sessionid = "", jsoncountry = "", jsonmain = "", jsonResult = null;

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ##############################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ##############################################################################

		Gson g = new GsonBuilder().setPrettyPrinting().serializeNulls()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("api_username").getAsString();
			sessionid = root.getAsJsonObject().get("session_id").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(sessionid)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		if (account == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}
		// test for invalid sessionid
		SessionLog sessionlog = sessionlogDAO.getValidSessionLog(account);

		// ################################################################
		// Guard against all invalid sessionid error
		// ################################################################

		if (sessionlog == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		String session = sessionlog.getSessionUuid();

		if (!StringUtils.equals(SecurityUtil.getMD5Hash(sessionid), session)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		List keys;
		// fetch from cache
		Country country;
		keys = countryCache.getKeys();
		// place country uuid and country code in a hashmap
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryHash.put(country.getUuid(), country.getCountrycode());
		}

		// fetch the main float balance MasterAccountBalance
		List<MasterAccountBalance> accountbalance = accountbalanceDAO.getMasterAccountBalance(account);

		if (accountbalance == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_NO_MASTERBALANCE);
			return g.toJson(expected);
		}

		for (MasterAccountBalance mainbalances : accountbalance) {
			mainfloatHash.put(mainbalances.getCurrency(), String.valueOf(mainbalances.getBalance()));
		}

		// fetch balance by country
		List<ClientAccountBalanceByCountry> clientBalances = accountbalanceDAO.getClientBalanceByCountry(account);

		if (clientBalances == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_NO_BALANCE_COUNTRIES);
			return g.toJson(expected);
		}

		// convert the resultant list to a hashmap.
		for (ClientAccountBalanceByCountry balance : clientBalances) {
			balancemap.put(countryHash.get(balance.getCountryUuid()), String.valueOf(balance.getBalance()));
		}

		jsonmain = g.toJson(mainfloatHash);
		jsoncountry = g.toJson(balancemap);
		JsonParser parser = new JsonParser();
		mainrateobject = parser.parse(jsonmain).getAsJsonObject();
		countryrateobject = parser.parse(jsoncountry).getAsJsonObject();

		balanceresult = new JsonObject();
		balanceresult.addProperty("api_username", username);

		mainarray = new JsonArray();

		mainarray.add(mainrateobject);

		countryarray = new JsonArray();

		countryarray.add(countryrateobject);

		balanceresult.add("main_float", mainarray);
		balanceresult.add("country_floatdistribution", countryarray);

		jsonResult = g.toJson(balanceresult);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.airtel.servlet.api.status;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.sessionlog.SessionLog;
import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.util.SecurityUtil;
import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.beans.thirdreference.ThirdPartyReference;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.persistence.thirdreference.ThirdReferenceDAO;
import com.impalapay.airtel.persistence.transaction.TransactionDAO;
import com.impalapay.airtel.persistence.transaction.TransactionStatusDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class AsyncQueryStatus extends HttpServlet {

	private PostMinusThread postMinusThread;

	private Cache accountsCache, countryCache, transactionStatusCache;

	private SessionLogDAO sessionlogDAO;

	private TransactionDAO transactionDAO;

	private TransactionForexDAO transactionforexDAO;

	private TransactionStatusDAO transactionstatusDAO;

	private ThirdReferenceDAO thirdreferenceDAO;

	private HashMap<String, String> countryIp = new HashMap<>();

	private HashMap<String, String> countryUuidmap = new HashMap<>();

	private HashMap<String, String> countryUsername = new HashMap<>();

	private HashMap<String, String> countryPassword = new HashMap<>();

	private HashMap<String, String> transactionStatusHash = new HashMap<>();

	private HashMap<String, String> transactionStatusuuidHash = new HashMap<>();

	private Map<String, String> ToThirdPartySystem = new HashMap<>();

	private String CLIENT_URL = "";
	String recipientcountrycode = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

		sessionlogDAO = SessionLogDAO.getInstance();
		transactionDAO = TransactionDAO.getInstance();
		transactionstatusDAO = TransactionStatusDAO.getInstance();
		thirdreferenceDAO = ThirdReferenceDAO.getInstance();
		transactionforexDAO = TransactionForexDAO.getinstance();

		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		String responseobject = "", switchresponse = "", statusdescription = "";

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;

		// These represent parameters received over the network
		String username = "", sessionid = "", referencenumber = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ####################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("api_username").getAsString();
			sessionid = root.getAsJsonObject().get("session_id").getAsString();
			referencenumber = root.getAsJsonObject().get("reference_number").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(sessionid) || StringUtils.isBlank(referencenumber)) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details then check against username and sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		if (account == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// test for invalid sessionid
		SessionLog sessionlog = sessionlogDAO.getValidSessionLog(account);

		// ################################################################
		// Guard against all invalid sessionid error
		// ################################################################

		if (sessionlog == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		String session = sessionlog.getSessionUuid();

		if (!StringUtils.equals(SecurityUtil.getMD5Hash(sessionid), session)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}

		// At this point we check to see if there is no transaction with the
		// given reference number.
		List<Transaction> transaction = transactionDAO.getTransactionstatus(referencenumber);

		if (transaction.size() == 0) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_REFERENCENUMBER);
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}
		List keys;

		// fetch from cache
		Country country;
		keys = countryCache.getKeys();
		// country and country ip
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryIp.put(country.getCountrycode(), country.getCountryverifyip());
		}
		// countryuuid and countrycode
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryUuidmap.put(country.getUuid(), country.getCountrycode());
		}
		// country and username
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryUsername.put(country.getCountrycode(), country.getUsername());
		}

		// country and country password
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryPassword.put(country.getCountrycode(), country.getPassword());
		}

		// ======================================================
		// Populate with the mapping of Transaction Statuses.
		// The key is a UUID of the status
		// ======================================================

		TransactionStatus status1;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status1.getStatus(), status1.getUuid());
		}

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusuuidHash.put(status1.getUuid(), status1.getStatus());
		}

		Transaction transactions = transactionDAO.getTransactionstatus1(referencenumber);
		// String transactionref = transactions.getReferenceNumber();

		String thirdrefuuid = transactions.getUuid();
		String countryuuid = transactions.getRecipientCountryUuid();

		// fetch the third partyreference number
		ThirdPartyReference thirdpartyref = thirdreferenceDAO.getTransactionReference(thirdrefuuid);

		// =========================================================================================
		// Check if the transaction id exist in the third reference Table
		// =========================================================================================

		if (thirdpartyref == null) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_CANCELED_TRANSACTION);

			return g.toJson(expected);

		}

		String fetchrefuuid = thirdpartyref.getReferencenumber();

		String transtatusuuid = transactions.getTransactionStatusUuid();
		TransactionStatus transactionstatus = transactionstatusDAO.getTransactionStatus(transtatusuuid);
		String status = transactionstatus.getStatus();

		recipientcountrycode = countryUuidmap.get(countryuuid);

		String apipassword = countryPassword.get(recipientcountrycode);

		String apiusername = countryUsername.get(recipientcountrycode);

		// String accounttype = account.getAccounttype();

		if (StringUtils.isEmpty(apipassword) || StringUtils.isEmpty(apiusername)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_COUNTRYAUTH_ERROR);

			return g.toJson(expected);
		}

		// retrieve the countryip to be used as URL
		String countryrip = countryIp.get(recipientcountrycode);

		String inprogressstatus = "S001";

		// This means that everything is ok
		String transactionuuid = transactions.getUuid();
		double amount = transactions.getAmount();
		String finalamount = String.valueOf(amount);
		String sender = transactions.getSenderName();
		// String receivermobile = transactions.getRecipientMobile();

		// create json object to send to thirdparty system.
		ToThirdPartySystem.put("username", apiusername);
		ToThirdPartySystem.put("password", apipassword);
		ToThirdPartySystem.put("transaction_id", fetchrefuuid);
		ToThirdPartySystem.put("url", countryrip);

		String jsonData = g.toJson(ToThirdPartySystem);

		// Transaction forex

		TransactionForexrate transactionforexhistory = transactionforexDAO.getTransactionForexsUuid(thirdrefuuid);

		if (status.equalsIgnoreCase(inprogressstatus)) {

			CLIENT_URL = countryrip;

			postMinusThread = new PostMinusThread(CLIENT_URL, jsonData);

			try {
				responseobject = postMinusThread.doPost();

				roots = new JsonParser().parse(responseobject);

				switchresponse = roots.getAsJsonObject().get("status_code").getAsString();

				statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

				/**
				 * used to throw back commandstatus within servlets to avoid Errors
				 */
				if (responseobject.contains("command_status")) {
					return String.valueOf(responseobject);
				}

			} catch (Exception e) {
				expected.put("command_status", APIConstants.COMMANDSTATUS_INTERNAL_ERROR);

				return g.toJson(expected);

			}

			if (!transactionStatusHash.containsKey(switchresponse)) {
				switchresponse = "00032";
			}

			// set the status UUID
			String statusuuid = transactionStatusHash.get(switchresponse);

			String success = "S000";
			// String fail = "00126";
			String fail = "00029";
			String inprogress = "S001";

			if (switchresponse.equalsIgnoreCase(success)) {

				transactions.setTransactionStatusUuid(statusuuid);
				// to be returned : transactionDAO.updateTransactionStatus(thirdrefuuid,
				// transactions);

				// deduct balance from country
				new AsynDeductions(transactions, transactionforexhistory).start();
				expected.put("transaction_id", thirdrefuuid);
				expected.put("refrence_id", referencenumber);
				expected.put("command_status", APIConstants.COMMANDSTATUS_OK);
				expected.put("status_code", switchresponse);
				expected.put("remit_status", statusdescription);

				String jsonResult = g.toJson(expected);

				return jsonResult;
			}

			if (switchresponse.equalsIgnoreCase(inprogress)) {

				expected.put("transaction_id", thirdrefuuid);
				expected.put("refrence_id", referencenumber);
				expected.put("command_status", APIConstants.COMMANDSTATUS_OK);
				expected.put("status_code", switchresponse);
				expected.put("remit_status", statusdescription);

				String jsonResult = g.toJson(expected);

				return jsonResult;
			}

			if (switchresponse.equalsIgnoreCase(fail)) {

				if (!transactionforexDAO.deleteTransactionForex(thirdrefuuid)) {
					expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL);
					String jsonResult = g.toJson(expected);
					return jsonResult;
				}

				transactions.setTransactionStatusUuid(statusuuid);
				// to be returned : transactionDAO.updateTransactionStatus(thirdrefuuid,
				// transactions);

				expected.put("transaction_id", thirdrefuuid);
				expected.put("refrence_id", referencenumber);
				expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL);
				expected.put("remit_status", statusdescription);

				String jsonResult = g.toJson(expected);

				return jsonResult;
			} else {

				expected.put("transaction_id", thirdrefuuid);
				expected.put("refrence_id", referencenumber);
				expected.put("status_code", switchresponse);
				expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL);
				expected.put("remit_status", statusdescription);
				String jsonResult = g.toJson(expected);

				return jsonResult;
			}

		} // end of inprogress status
			// return if results have already been queried and updated.
		expected.put("transaction_id", transactionuuid);
		expected.put("transaction_amount", finalamount);
		expected.put("sender_name", sender);
		expected.put("api_username", username);
		expected.put("transaction_status", status);
		expected.put("command_status", APIConstants.COMMANDSTATUS_OK);

		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.airtel.servlet.api.temporarysend;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.CacheManager;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;

public class ChinaQuery extends HttpServlet {
	private PostWithIgnoreSSL postwithignoreSSL;

	private String statuscode = "00032";

	private String Statusdescription = "INTERNAL_SERVER_ERROR";

	private String SendCash = "";

	private Map<String, String> tochina = new HashMap<>();

	private String CLIENT_URL = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(checkName(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkName(HttpServletRequest request) throws IOException {

		// joined json string
		String join = "";
		JsonElement root = null, switchresponse = null;

		String responseobject = "";

		// These represent parameters received over the network
		String username = "", password = "", transaction_id = "", remiturl = "", switchcode = "",
				statusdescription = "";

		// Hold values from properties config.
		String masteragentcode = "", languagecode = "", agentcode = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ##############################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use disableHtmlEscaping().
		// ##############################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			password = root.getAsJsonObject().get("password").getAsString();
			transaction_id = root.getAsJsonObject().get("transaction_id").getAsString();
			remiturl = root.getAsJsonObject().get("url").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(transaction_id)
				|| StringUtils.isBlank(remiturl)) {

			expected.put("am_referenceid", username);
			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", Statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		masteragentcode = PropertiesConfig.getConfigValue("MASTER_AGENT_CODE");

		agentcode = PropertiesConfig.getConfigValue("AGENTCODE");

		languagecode = PropertiesConfig.getConfigValue("LANG_CODE");

		// construct json object to be sent to true Africa
		tochina.put("UserName", username);
		tochina.put("Password", password);
		tochina.put("PayoutReferenceID", transaction_id);
		tochina.put("MasterAgentCode", masteragentcode);
		tochina.put("AgentCode", agentcode);
		tochina.put("LangCode", languagecode);

		String jsonData = g.toJson(tochina);

		// assign the remit url from properties.config
		CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL6");
		// CLIENT_URL = remiturl;

		postwithignoreSSL = new PostWithIgnoreSSL(CLIENT_URL, jsonData);

		// capture the switch respoinse.
		responseobject = postwithignoreSSL.doPost();
		/**
		 * 
		 * }
		 **/
		try {
			// pass the returned json string
			JsonElement roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			switchresponse = roots.getAsJsonObject().get("Transaction").getAsJsonObject();

			statusdescription = switchresponse.getAsJsonObject().get("TransactionStatus").getAsString();

		} catch (Exception e) {
			// instantiate the JSon
			expected.put("status_code", "0032");
			expected.put("status_description", "INVALID_RESPONSE_FROM_WALLET");
			String jsonResult = g.toJson(expected);

			return jsonResult;

		}
		// start mapping all the error codes
		// map ACCEPTED FOR PROCESSING
		if (statusdescription.equalsIgnoreCase("CON")) {// confirmed
			switchcode = "S001";
			statusdescription = "CREDIT_IN_PROGRESS";
		}
		if (statusdescription.equalsIgnoreCase("PAI")) {// paid
			switchcode = "S000";
			statusdescription = "SUCCESS";
		}
		if (statusdescription.equalsIgnoreCase("REJ")) {// rejected
			switchcode = "00029";
			statusdescription = "Rejected_Transaction";
		}
		if (statusdescription.equalsIgnoreCase("ATH")) {// inprogress
			switchcode = "S001";
			statusdescription = "CREDIT_IN_PROGRESS";
		}
		if (statusdescription.equalsIgnoreCase("CAN")) {// cancelled
			switchcode = "00029";
			statusdescription = "Cancelled_Transaction";
		}

		Date now = new Date();

		String stringdate = String.valueOf(now);

		String paid = "S000";
		String inprogress = "S001";
		String fail = "00029";

		if (switchcode.equalsIgnoreCase(paid)) {

			expected.put("am_timestamp", stringdate);
			expected.put("status_code", switchcode);
			expected.put("status_description", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (switchcode.equalsIgnoreCase(fail)) {
			expected.put("am_timestamp", stringdate);
			expected.put("status_code", switchcode);
			expected.put("status_description", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (switchcode.equalsIgnoreCase(inprogress)) {
			expected.put("am_timestamp", stringdate);
			expected.put("status_code", switchcode);
			expected.put("status_description", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("am_timestamp", stringdate);
		expected.put("status_code", switchcode);
		expected.put("status_description", statusdescription);
		String jsonResult = g.toJson(expected);

		return jsonResult;

		// return statusdescription.toString();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

package com.impalapay.airtel.servlet.api.status;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.persistence.accountmgmt.balance.AccountBalanceDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;

/**
 * Responsible to deducting client balance.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class AsynDeductions extends Thread {
	private Account account;
	private Country coutruuuid;

	private Transaction transaction;
	private TransactionForexrate transactionforexrate;

	private AccountBalanceDAO accountbalanceDAO;

	/**
	 * 
	 */
	private AsynDeductions() {
	}

	/**
	 * @param account
	 */
	public AsynDeductions(Transaction transaction, TransactionForexrate transactionforexrate) {
		this.transaction = transaction;
		this.transactionforexrate = transactionforexrate;

		accountbalanceDAO = AccountBalanceDAO.getInstance();
	}

	/**
	 * 
	 */
	@Override
	public void run() {
		coutruuuid = new Country();
		coutruuuid.setUuid(transaction.getRecipientCountryUuid());

		account = new Account();
		account.setUuid(transaction.getAccountUuid());

		accountbalanceDAO.deductBalanceByCountry(account, coutruuuid, transaction.getAmount());
		accountbalanceDAO.deductBalance(account, transactionforexrate.getLocalamount());

	}

}

package com.impalapay.airtel.servlet.api.remit;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.thirdreference.ThirdPartyReference;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.persistence.accountmgmt.balance.AccountBalanceDAO;
import com.impalapay.airtel.persistence.thirdreference.ThirdReferenceDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;

/**
 * Responsible to dispatch a new Session Id to a client URL.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class AsyncTransactionDispatcher extends Thread {

	private Account account;
	private Country coutruuuid;

	private Transaction transaction;
	private TransactionForexrate transactionforexrate;
	private ThirdPartyReference thirdpartyreference;

	private TransactionForexDAO transactionforexDAO;

	private AccountBalanceDAO accountbalanceDAO;

	private ThirdReferenceDAO thirdreferenceDAO;

	/**
	 * 
	 */
	private AsyncTransactionDispatcher() {
	}

	/**
	 * @param account
	 */
	public AsyncTransactionDispatcher(Transaction transaction, TransactionForexrate transactionforexrate,
			ThirdPartyReference thirdpartyreference) {
		this.transaction = transaction;
		this.transactionforexrate = transactionforexrate;
		this.thirdpartyreference = thirdpartyreference;

		transactionforexDAO = TransactionForexDAO.getinstance();
		accountbalanceDAO = AccountBalanceDAO.getInstance();
		thirdreferenceDAO = ThirdReferenceDAO.getInstance();
	}

	/**
	 * 
	 */
	@Override
	public void run() {
		coutruuuid = new Country();
		coutruuuid.setUuid(transaction.getRecipientCountryUuid());

		account = new Account();
		account.setUuid(transaction.getAccountUuid());

		transactionforexDAO.addTransactionForex(transactionforexrate);
		thirdreferenceDAO.addReferences(thirdpartyreference);

	}
}

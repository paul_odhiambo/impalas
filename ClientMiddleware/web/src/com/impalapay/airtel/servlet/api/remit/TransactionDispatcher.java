package com.impalapay.airtel.servlet.api.remit;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.persistence.accountmgmt.balance.AccountBalanceDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;

/**
 * Responsible to dispatch a new Session Id to a client URL.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class TransactionDispatcher extends Thread {

	private Account account;
	private Country coutruuuid;

	private Transaction transaction;
	private TransactionForexrate transactionforexrate;

	private TransactionForexDAO transactionforexDAO;

	private AccountBalanceDAO accountbalanceDAO;

	/**
	 * 
	 */
	private TransactionDispatcher() {
	}

	/**
	 * @param account
	 */
	public TransactionDispatcher(Transaction transaction, TransactionForexrate transactionforexrate) {
		this.transaction = transaction;
		this.transactionforexrate = transactionforexrate;

		transactionforexDAO = TransactionForexDAO.getinstance();
		accountbalanceDAO = AccountBalanceDAO.getInstance();
	}

	/**
	 * 
	 */
	@Override
	public void run() {/**
						 * String sessionId = StringUtils .remove(UUID.randomUUID().toString(), '-');
						 * 
						 * SessionLog session = new SessionLog();
						 * session.setAccountUuid(account.getUuid()); session.setValid(true); // We
						 * persist the hashed version of the Session Id while the plain one // is sent
						 * to the client application
						 * session.setSessionUuid(SecurityUtil.getMD5Hash(sessionId));
						 **/
		coutruuuid = new Country();
		coutruuuid.setUuid(transaction.getRecipientCountryUuid());

		account = new Account();
		account.setUuid(transaction.getAccountUuid());

		accountbalanceDAO.deductBalanceByCountry(account, coutruuuid, transaction.getAmount());
		accountbalanceDAO.deductBalance(account, transactionforexrate.getLocalamount());
		transactionforexDAO.addTransactionForex(transactionforexrate);

	}
}

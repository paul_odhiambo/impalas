package com.impalapay.airtel.servlet.api.temporarysend;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLBeyonic;

public class BeyonicQuery extends HttpServlet {
	private PostWithIgnoreSSLBeyonic postIgnoreSslBeyonic;

	private String statuscode = "00032";

	private String Statusdescription = "INTERNAL_SERVER_ERROR";

	private Map<String, String> tobeyonicUG = new HashMap<>();

	private String CLIENT_URL = "";

	private String switchresponse = "", statusdescription = "";

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(checkName(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkName(HttpServletRequest request) throws IOException {

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;

		String responseobject = "";

		// These represent parameters received over the network
		String username = "", password = "", transaction_id = "", remiturl = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use disableHtmlEscaping().
		// ###############################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			password = root.getAsJsonObject().get("password").getAsString();
			transaction_id = root.getAsJsonObject().get("transaction_id").getAsString();
			remiturl = root.getAsJsonObject().get("url").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(transaction_id)
				|| StringUtils.isBlank(remiturl)) {

			expected.put("am_referenceid", username);
			expected.put("am_timestamp", username);
			expected.put("status_code", statuscode);
			expected.put("status_description", Statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// construct json object to be sent to true Africa
		tobeyonicUG.put("Payment", transaction_id);

		String jsonData = g.toJson(tobeyonicUG);

		String transactionidcheck = transaction_id;

		// assign the remit url from properties.config
		// CLIENT_URL = remiturl;
		CLIENT_URL = "https://app.beyonic.com/api/payments/" + transactionidcheck + "?format=json";

		postIgnoreSslBeyonic = new PostWithIgnoreSSLBeyonic(CLIENT_URL, jsonData);

		// capture the switch respoinse.
		responseobject = postIgnoreSslBeyonic.doGet();

		try {
			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			switchresponse = roots.getAsJsonObject().get("state").getAsString();
			statusdescription = roots.getAsJsonObject().get("state").getAsString();

		} catch (Exception e) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INTERNAL_ERROR);

			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (switchresponse.equalsIgnoreCase("processed") || switchresponse.equalsIgnoreCase("validated")) {
			switchresponse = "S000";
			statusdescription = "SUCCESS";
		}

		// map error
		if (switchresponse.equalsIgnoreCase("processed_with_errors") || switchresponse.equalsIgnoreCase("aborted")) {
			switchresponse = "00029";
		}

		// map ACCEPTED FOR INVALID AMOUNT
		if (switchresponse.equalsIgnoreCase("paused_for_admin_action") || switchresponse.equalsIgnoreCase("queued")
				|| switchresponse.equalsIgnoreCase("paused") || switchresponse.equalsIgnoreCase("processing")
				|| switchresponse.equalsIgnoreCase("scheduled")
				|| switchresponse.equalsIgnoreCase("pending_confirmation") || switchresponse.equalsIgnoreCase("new")) {
			switchresponse = "S001";
			statusdescription = "CREDIT_IN_PROGRESS";
		}

		if (switchresponse.equalsIgnoreCase("rejected") || switchresponse.equalsIgnoreCase("cancelled")) {
			switchresponse = "00029";
			statusdescription = roots.getAsJsonObject().get("last_error").getAsString();
		}

		String success = "S000";
		// String fail = "00126";
		// String inprogress = "S001";
		// rejected and canceled should be included.

		if (switchresponse.equalsIgnoreCase(success)) {

			expected.put("am_timestamp", username);
			expected.put("status_code", switchresponse);
			expected.put("status_description", statusdescription);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("am_timestamp", username);
		expected.put("status_code", switchresponse);
		expected.put("status_description", statusdescription);
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

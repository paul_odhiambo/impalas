package com.impalapay.airtel.servlet.admin.accounts;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.beans.route.RouteDefine;
import com.impalapay.persistence.routing.RouteDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add an msisdn to a specific country.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 11, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class AddAccountToRoute extends HttpServlet {

	final String ERROR_NO_NETWORK = "Please provide a value for the Network ";
	final String ERROR_NO_ACCOUNT = "Please provide a value for the account .";
	final String ERROR_NO_FOREX = "Please state if forex will be allowed or not.";
	final String ERROR_NO_SETTLEMENT = "Please state if settlement will be pre or post-settlement.";
	final String ERROR_NO_FIXEDCOMMISSION = "Please state if Fixed Commission is supported or not .";
	final String ERROR_NO_COMMISSION = "Please provide a value for the Commission.";
	final String ERROR_MINIMUM_BALANCE = "Please provide a Valid minimum balance.";
	final String ERROR_UNABLE_ADD = "Unable to add the account to route";
	// These represent form parameters
	private String networkuuid, accountuuid, supportforex, presettlement, fixedcommission, commission, minimumbalance;
	private String addDay, addMonth, addYear;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> PrefixparamHash;

	private Cache routedefineCache;
	private RouteDAO routeDAO;

	private Logger logger;

	private String alluuid = "";

	private String transactioinid = "";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		routedefineCache = mgr.getCache(CacheVariables.CACHE_ROUTEDEFINED_BY_UUID);

		routeDAO = RouteDAO.getInstance();

		logger = Logger.getLogger(this.getClass());
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_PARAMETERS, PrefixparamHash);

		if (StringUtils.isBlank(networkuuid)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_ERROR_KEY, ERROR_NO_NETWORK);

		} else if (StringUtils.isBlank(accountuuid)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_ERROR_KEY, ERROR_NO_ACCOUNT);

		} else if (StringUtils.isBlank(fixedcommission)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_ERROR_KEY, ERROR_NO_FIXEDCOMMISSION);

		} else if (StringUtils.isBlank(presettlement)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_ERROR_KEY, ERROR_NO_SETTLEMENT);

		} else if (StringUtils.isBlank(commission)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_ERROR_KEY, ERROR_NO_COMMISSION);

		} else if (StringUtils.isBlank(minimumbalance)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_ERROR_KEY, ERROR_MINIMUM_BALANCE);

		} else if (!addRouteDefine()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_ERROR_KEY, ERROR_UNABLE_ADD);

		} else {
			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_SUCCESS_KEY, "s");

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_ADD_ROUTEDEFINE_ERROR_KEY, null);

		}

		response.sendRedirect("addroutedefine.jsp");

		// purchasesCache.put(new
		// Element(CacheVariables.CACHE_PURCHASEPERCOUNTRY_KEY,
		// accountPurchaseDAO.getAllClientPurchasesByCountry()));
	}

	/**
	 * Add amount added to each country float.
	 * 
	 * @return boolean indicating if addition has been added or not.
	 */
	private boolean addRouteDefine() {

		Account account = new Account();

		account.setUuid(accountuuid);

		RouteDefine routedefine = routeDAO.getRoutes(networkuuid, account);

		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		if (routedefine != null) {

			alluuid = routedefine.getUuid();
		} else {
			alluuid = transactioinid;
		}

		RouteDefine routing = new RouteDefine();

		routing.setUuid(alluuid);
		routing.setAccountUuid(accountuuid);
		routing.setNetworkUuid(networkuuid);
		routing.setSupportforex(Boolean.parseBoolean(supportforex));
		routing.setFixedcommission(Boolean.parseBoolean(fixedcommission));
		routing.setPresettlement(Boolean.parseBoolean(presettlement));
		routing.setCommission(Double.parseDouble(commission));
		routing.setMinimumbalance(Double.parseDouble(minimumbalance));
		Calendar c = Calendar.getInstance();
		c.set(NumberUtils.toInt(addYear), NumberUtils.toInt(addMonth) - 1, NumberUtils.toInt(addDay));
		routing.setCreationDate(c.getTime());

		boolean response = routeDAO.updateRoute(alluuid, routing);

		routedefineCache.put(new Element(routing.getUuid(), routing));

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		accountuuid = StringUtils.trimToEmpty(request.getParameter("accountuuid"));
		networkuuid = StringUtils.trimToEmpty(request.getParameter("networkuuid"));
		supportforex = StringUtils.trimToEmpty(request.getParameter("optionforex"));
		fixedcommission = StringUtils.trimToEmpty(request.getParameter("optionfixedcommision"));
		presettlement = StringUtils.trimToEmpty(request.getParameter("optionsettlement"));
		commission = StringUtils.trimToEmpty(request.getParameter("commission"));
		minimumbalance = StringUtils.trimToEmpty(request.getParameter("minimumbalance"));
		addDay = StringUtils.trimToEmpty(request.getParameter("addDay"));
		addMonth = StringUtils.trimToEmpty(request.getParameter("addMonth"));
		addYear = StringUtils.trimToEmpty(request.getParameter("addYear"));

	}

	/**
	 * Place some of the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		PrefixparamHash = new HashMap<>();
		PrefixparamHash.put("networkuuid", networkuuid);
		PrefixparamHash.put("accountuuid", accountuuid);
		PrefixparamHash.put("supportforex", supportforex);
		PrefixparamHash.put("fixedcommission", fixedcommission);
		PrefixparamHash.put("presettlement", presettlement);
		PrefixparamHash.put("commission", commission);
		PrefixparamHash.put("minimumbalance", minimumbalance);

	}

}

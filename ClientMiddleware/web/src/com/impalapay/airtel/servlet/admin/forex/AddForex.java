package com.impalapay.airtel.servlet.admin.forex;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;
import com.impalapay.airtel.beans.forex.ForexEngineHistory;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.airtel.persistence.accountmgmt.ManageAccountDAO;
import com.impalapay.airtel.persistence.forex.ForexEngineHistoryDAO;
import com.impalapay.airtel.persistence.systemlog.SystemLogDAO;
import com.impalapay.airtel.servlet.util.PropertiesConfig;

import net.sf.ehcache.CacheManager;
import org.apache.commons.lang3.StringUtils;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add/update forex.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Nov 24, 2014
 *
 * @author <a href="mailto:mike@impalapay.com">Michael Wakahe</a>
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 *
 */
public class AddForex extends HttpServlet {

	final String ERROR_NO_QUOTECURRENCY = "Please slect the Quotecurrency type(e.g USD,GBP).";
	final String ERROR_NO_BASECURRENCY = "Please slect a Basecurrency from Drop-down list(e.g KES,UGX).";
	final String ERROR_NO_IMPALARATE = "Please provide Impala's Rate.";
	final String ERROR_NO_BASERATE = "Please provide the Base Rate.";
	final String ERROR_UNABLE_ADD = "Could not add forex to checker forex rate";
	final String ERROR_NOT_ALLOWED = "You dont have access rights to perform this Action,Please Contact System Administrator";
	private String basecurrency, impalarate, baserate, quotecurrency, username, createdpair;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;

	private SystemLogDAO systemlogDAO;
	private ManageAccountDAO managementaccountDAO;
	private ForexEngineHistoryDAO forexengineDAO;

	private boolean response;

	private HttpSession session;
	private ForexEngineHistory forex, forexengine;

	private String transactioinid = "";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		forexengineDAO = ForexEngineHistoryDAO.getInstance();

		managementaccountDAO = ManageAccountDAO.getInstance();

		CacheManager.getInstance();
		systemlogDAO = SystemLogDAO.getInstance();

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_PARAMETERS, paramHash);

		// No First currency provided
		if (StringUtils.isBlank(quotecurrency)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_ERROR_KEY, ERROR_NO_QUOTECURRENCY);

			// No Base rate provided
		} else if (StringUtils.isBlank(baserate)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_ERROR_KEY, ERROR_NO_BASERATE);

			// No Impala Rate Provided provided
		} else if (StringUtils.isBlank(impalarate)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_ERROR_KEY, ERROR_NO_IMPALARATE);

		} else if (StringUtils.isBlank(basecurrency)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_ERROR_KEY, ERROR_NO_BASECURRENCY);

		} else if (!addforex()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_ERROR_KEY, ERROR_NOT_ALLOWED);

		} else {

			if (!addForexUpdate()) {
				session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_ERROR_KEY, ERROR_UNABLE_ADD);
			}

			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_SUCCESS_KEY, "s");

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_ERROR_KEY, null);

		}

		response.sendRedirect("dashboard.jsp");

	}

	/**
	 *
	 */
	private boolean addForexUpdate() {

		// Create the Currency Pair
		createdpair = basecurrency + "/" + quotecurrency;
		// Country country = new Country();
		response = false;
		// country.setUuid(countryUuid);

		// Forex forex = usdForexDAO.getCountryUsdForex(country);
		forex = forexengineDAO.getCheckerForexHistory(createdpair);

		forexengine = new ForexEngineHistory();

		SystemLog systemlog = new SystemLog();
		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');
		systemlog.setUsername(username);
		systemlog.setUuid(transactioinid);
		systemlog.setAction(username + " adjusted the checkercurrencypair " + basecurrency + "/" + quotecurrency
				+ " to spreadrate= " + impalarate + " the baserate= " + baserate);

		if (forex != null) {

			forexengine.setUuid(forex.getUuid());
			forexengine.setCurrencypair(createdpair);
			forexengine.setMarketrate(Double.parseDouble(baserate));
			forexengine.setSpreadrate(Double.parseDouble(impalarate));

			// systemlogDAO.putsystemlog(systemlog);
			response = forexengineDAO.UpdateCheckerForex(forex.getUuid(), forexengine);
			if (response) {
				systemlogDAO.putsystemlog(systemlog);
			}

		} else {
			forexengine.setUuid(StringUtils.remove(UUID.randomUUID().toString(), '-'));
			forexengine.setCurrencypair(createdpair);
			forexengine.setMarketrate(Double.parseDouble(baserate));
			forexengine.setSpreadrate(Double.parseDouble(impalarate));

			// systemlogDAO.putsystemlog(systemlog);
			response = forexengineDAO.PutCheckerForexHistory(forexengine);
			if (response) {
				systemlogDAO.putsystemlog(systemlog);
			}
		}

		return response;
	}

	public boolean addforex() {

		if (StringUtils.equals(username, PropertiesConfig.getConfigValue("ADMIN_USERNAME"))) {
			response = true;
		} else {
			ManagementAccount status = managementaccountDAO.getAccountName(username);

			response = status.isUpdateforex();
		}

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		basecurrency = StringUtils.trimToEmpty(request.getParameter("basecurrency"));
		quotecurrency = StringUtils.trimToEmpty(request.getParameter("quotecurrency"));
		baserate = StringUtils.trimToEmpty(request.getParameter("baserate"));
		impalarate = StringUtils.trimToEmpty(request.getParameter("impalarate"));
		username = StringUtils.trimToEmpty(request.getParameter("username"));

	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		/**
		 * paramHash = new HashMap<>();
		 * 
		 * paramHash.put("currencypair", createdpair); paramHash.put("baserate",
		 * baserate); paramHash.put("impalarate", impalarate);
		 **/

	}

}

/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */

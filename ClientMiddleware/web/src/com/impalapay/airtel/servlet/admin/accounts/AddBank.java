package com.impalapay.airtel.servlet.admin.accounts;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.beans.bank.BankCodes;
import com.impalapay.airtel.persistence.bank.BankCodesDAO;
import com.impalapay.beans.network.Network;
import com.impalapay.mno.persistence.network.NetworkDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add an msisdn to a specific country.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 11, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class AddBank extends HttpServlet {

	final String ERROR_NO_BANKNAME = "Please provide a value for the bankname";
	final String ERROR_NO_BANKCODE = "Please provide a value for the bankcode";
	final String ERROR_NO_COUNTRYCODE = "Please provide a value for the country";
	final String ERROR_NO_NETWORK = "Please provide a value for Bank Network";
	final String ERROR_NO_BRANCHCODE = "Please provide a value for Brachcode";
	final String ERROR_NO_IBAN = "Please provide a value for Iban";
	final String ERROR_INVALID_COUNTRYCODE = "Please provide a valid  countrycode.";
	final String ERROR_UNABLE_ADD = "Unable to add new bank";
	// These represent form parameters
	private String bankname, countryuuid, networkuuid, bankcode, branchcode, iban;
	private String addDay, addMonth, addYear;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> BankparamHash;

	private Cache bankCache;

	private BankCodesDAO bankcodesDAO;

	private NetworkDAO networkDAO;

	private String alluuid = "";

	private String transactioinid = "";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		bankCache = mgr.getCache(CacheVariables.CACHE_BANK_BY_UUID);

		bankcodesDAO = BankCodesDAO.getInstance();

		networkDAO = NetworkDAO.getInstance();

		Logger.getLogger(this.getClass());
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_ADD_BANK_PARAMETERS, BankparamHash);

		if (StringUtils.isBlank(bankname)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_BANK_ERROR_KEY, ERROR_NO_BANKNAME);

		} else if (StringUtils.isBlank(bankcode)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_BANK_ERROR_KEY, ERROR_NO_BANKCODE);
			/**
			 * } else if (StringUtils.isBlank(countryuuid)) {
			 * session.setAttribute(SessionConstants.ADMIN_ADD_BANK_ERROR_KEY,
			 * ERROR_NO_COUNTRYCODE);
			 **/

		} else if (StringUtils.isBlank(networkuuid)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_BANK_ERROR_KEY, ERROR_NO_NETWORK);

		} else if (StringUtils.isBlank(branchcode)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_BANK_ERROR_KEY, ERROR_NO_BRANCHCODE);

		} else if (StringUtils.isBlank(iban)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_BANK_ERROR_KEY, ERROR_NO_IBAN);

		} else if (!addNewBank()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_BANK_ERROR_KEY, ERROR_UNABLE_ADD);

		} else {
			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_ADD_BANK_SUCCESS_KEY, "s");

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_ADD_BANK_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_ADD_BANK_ERROR_KEY, null);

		}

		response.sendRedirect("addBank.jsp");

	}

	/**
	 * Add amount added to each country float.
	 * 
	 * @return boolean indicating if addition has been added or not.
	 */
	private boolean addNewBank() {

		BankCodes bankcodes = new BankCodes();

		bankcodes.setBankcode(bankcode);

		Network retrievecountry = networkDAO.getNetwork(networkuuid);

		countryuuid = retrievecountry.getCountryUuid();

		BankCodes existingbankcode = bankcodesDAO.getBankCodes(bankcodes);

		if (existingbankcode != null) {

			alluuid = existingbankcode.getUuid();
		} else {
			alluuid = transactioinid;
		}

		BankCodes p = new BankCodes();

		p.setBankcode(bankcode);
		p.setBankname(bankname);
		p.setCountryuuid(countryuuid);
		p.setNetworkuuid(networkuuid);
		p.setBranchcode(branchcode);
		p.setIban(iban);
		Calendar c = Calendar.getInstance();
		c.set(NumberUtils.toInt(addYear), NumberUtils.toInt(addMonth) - 1, NumberUtils.toInt(addDay));
		p.setDateadded(c.getTime());

		boolean response = bankcodesDAO.updateBankCodes(alluuid, p);

		bankCache.put(new Element(p.getUuid(), p));

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {

		bankname = StringUtils.trimToEmpty(request.getParameter("bankname"));
		// countryuuid =
		// StringUtils.trimToEmpty(request.getParameter("countryUuid"));
		bankcode = StringUtils.trimToEmpty(request.getParameter("bankcode"));
		networkuuid = StringUtils.trimToEmpty(request.getParameter("networkUuid"));
		branchcode = StringUtils.trimToEmpty(request.getParameter("branchcode"));
		iban = StringUtils.trimToEmpty(request.getParameter("iban"));
		addDay = StringUtils.trimToEmpty(request.getParameter("addDay"));
		addMonth = StringUtils.trimToEmpty(request.getParameter("addMonth"));
		addYear = StringUtils.trimToEmpty(request.getParameter("addYear"));

	}

	/**
	 * Place some of the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		BankparamHash = new HashMap<>();

		BankparamHash.put("bankname", bankname);
		BankparamHash.put("countryuuid", countryuuid);
		BankparamHash.put("bankcode", bankcode);
		BankparamHash.put("branchcode", branchcode);
		BankparamHash.put("iban", iban);
		BankparamHash.put("networkuuid", networkuuid);
	}

}

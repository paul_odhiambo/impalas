package com.impalapay.airtel.servlet.admin.funds;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;
import com.impalapay.mno.beans.accountmgmt.balance.MasterAccountFloatPurchase;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.airtel.persistence.accountmgmt.ManageAccountDAO;
import com.impalapay.mno.persistence.accountmgmt.balance.AccountPurchaseDAO;
import com.impalapay.airtel.persistence.systemlog.SystemLogDAO;
import com.impalapay.airtel.servlet.util.PropertiesConfig;

import net.sf.ehcache.CacheManager;
import org.apache.commons.lang3.StringUtils;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add bulk airtime to the master account.
 * <p>
 * Copyright (c) Shujaa Solutions Ltd., Oct 11, 2013
 *
 * @author <a href="mailto:anthonym@shujaa.co.ke">Antony Wafula</a>
 * @version %I%, %G%
 */
public class DeleteCheckMasterFloat extends HttpServlet {

	final String ERROR_NO_BALANCEUUID = "Please provide a value for the float uuid ";
	final String ERROR_NO_USERNAME = "Please provide a value for the username ";

	final String ERROR_UNABLE_DELETE = "Unable to delete checker float .";
	final String ERROR_NOT_ALLOWED = "You dont have access rights to perform this Action,Please Contact System Administrator";
	final String SUCCESS = "You have seccesfully deleted the checker masterbalance from the system";

	// These represent form parameters
	private String username, transactioinid, masterbalanceuuid;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;

	private AccountPurchaseDAO accountPurchaseDAO;
	private ManageAccountDAO managementaccountDAO;
	private SystemLogDAO systemlogDAO;
	MasterAccountFloatPurchase fetchfloat;
	private boolean response;

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		mgr.getCache(CacheVariables.CACHE_FLOATPURCHASEPERCOUNTRY_BY_ACCOUNTUUID);
		managementaccountDAO = ManageAccountDAO.getInstance();
		systemlogDAO = SystemLogDAO.getInstance();

		accountPurchaseDAO = AccountPurchaseDAO.getInstance();
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_ADD_MASTER_FLOAT_PARAMETERS, paramHash);

		if (StringUtils.isBlank(masterbalanceuuid)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_MASTER_FLOAT_ERROR_KEY, ERROR_NO_BALANCEUUID);
		} else if (StringUtils.isBlank(username)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_MASTER_FLOAT_ERROR_KEY, ERROR_NO_USERNAME);
		} else if (!approveBalance()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_MASTER_FLOAT_ERROR_KEY, ERROR_NOT_ALLOWED);
		} else if (!deleteFloat()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_MASTER_FLOAT_ERROR_KEY, ERROR_UNABLE_DELETE);

		} else {
			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_ADD_MASTER_FLOAT_SUCCESS_KEY, SUCCESS);

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_ADD_MASTER_FLOAT_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_ADD_MASTER_FLOAT_ERROR_KEY, null);

		}

		response.sendRedirect("checkerFloat.jsp");

	}

	/**
	 * Add airtime purchased in bulk
	 * 
	 * @return
	 *
	 */
	private boolean deleteFloat() {
		response = false;
		// retrieve transaction details from checker mastserbalance table
		fetchfloat = accountPurchaseDAO.getCheckerMasterFloat(masterbalanceuuid);

		if (fetchfloat != null) {

			SystemLog systemlog = new SystemLog();
			transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

			systemlog.setUsername(username);
			systemlog.setUuid(transactioinid);
			systemlog.setAction(username + " deleted new checkermasterbalance of currency " + fetchfloat.getCurrency()
					+ " amount= " + fetchfloat.getBalance() + " for account " + fetchfloat.getAccountUuid());

			response = accountPurchaseDAO.deleteCheckerMasterFloat(masterbalanceuuid);
			if (response) {
				systemlogDAO.putsystemlog(systemlog);
			}
		}

		return response;

	}

	public boolean approveBalance() {

		if (StringUtils.equals(username, PropertiesConfig.getConfigValue("ADMIN_USERNAME"))) {
			response = true;
		} else {
			ManagementAccount status = managementaccountDAO.getAccountName(username);

			response = status.isChecker();
		}

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		masterbalanceuuid = StringUtils.trimToEmpty(request.getParameter("reject"));
		username = StringUtils.trimToEmpty(request.getParameter("username"));
	}

	/**
	 * Place some of the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		paramHash = new HashMap<>();

	}
}

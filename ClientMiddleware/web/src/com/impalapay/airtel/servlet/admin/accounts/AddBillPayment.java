package com.impalapay.airtel.servlet.admin.accounts;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.beans.bank.BankCodes;
import com.impalapay.airtel.beans.billpayment.BillPaymentCodes;
import com.impalapay.airtel.persistence.bank.BankCodesDAO;
import com.impalapay.airtel.persistence.billpayment.BillpaymentCodesDAO;
import com.impalapay.beans.network.Network;
import com.impalapay.mno.persistence.network.NetworkDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add an msisdn to a specific country.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 11, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class AddBillPayment extends HttpServlet {

	final String ERROR_NO_MERCHANTNAME = "Please provide a value for the merchantname";
	final String ERROR_NO_MERCHANTCODE = "Please provide a value for the merchantcode";
	final String ERROR_NO_COUNTRYCODE = "Please provide a value for the country";
	final String ERROR_NO_NETWORK = "Please provide a value for Merchant Network";
	final String ERROR_NO_MERCHANTID = "Please provide a value for merchantId";
	final String ERROR_NO_MSISDN = "Please provide a value for Merchant MSISDN mapped to MerchantCode";
	final String ERROR_INVALID_COUNTRYCODE = "Please provide a valid  countrycode.";
	final String ERROR_UNABLE_ADD = "Unable to add new BillPayment";
	// These represent form parameters
	private String merchantname, countryuuid, networkuuid, merchantcode, merchantmsisdn, merchantid;
	private String addDay, addMonth, addYear;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> BillparamHash;

	private Cache billpaymentCache;

	private BillpaymentCodesDAO billpaymentcodesDAO;

	private NetworkDAO networkDAO;

	private String alluuid = "";

	private String transactioinid = "";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		billpaymentCache = mgr.getCache(CacheVariables.CACHE_BILLPAYMENTCODES_BY_UUID);

		billpaymentcodesDAO = BillpaymentCodesDAO.getInstance();

		networkDAO = NetworkDAO.getInstance();

		Logger.getLogger(this.getClass());
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_ADD_BILLPAYMENT_PARAMETERS, BillparamHash);

		if (StringUtils.isBlank(merchantname)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_BILLPAYMENT_ERROR_KEY, ERROR_NO_MERCHANTNAME);

		} else if (StringUtils.isBlank(merchantcode)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_BILLPAYMENT_ERROR_KEY, ERROR_NO_MERCHANTCODE);
			/**
			 * } else if (StringUtils.isBlank(countryuuid)) {
			 * session.setAttribute(SessionConstants.ADMIN_ADD_BANK_ERROR_KEY,
			 * ERROR_NO_COUNTRYCODE);
			 **/

		} else if (StringUtils.isBlank(networkuuid)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_BILLPAYMENT_ERROR_KEY, ERROR_NO_NETWORK);

		} else if (StringUtils.isBlank(merchantmsisdn)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_BILLPAYMENT_ERROR_KEY, ERROR_NO_MSISDN);

		} else if (StringUtils.isBlank(merchantid)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_BILLPAYMENT_ERROR_KEY, ERROR_NO_MERCHANTID);

		} else if (!addNewBank()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_BILLPAYMENT_ERROR_KEY, ERROR_UNABLE_ADD);

		} else {
			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_ADD_BILLPAYMENT_SUCCESS_KEY, "s");

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_ADD_BILLPAYMENT_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_ADD_BILLPAYMENT_ERROR_KEY, null);

		}

		response.sendRedirect("addBill.jsp");

	}

	/**
	 * Add amount added to each country float.
	 * 
	 * @return boolean indicating if addition has been added or not.
	 */
	private boolean addNewBank() {

		BillPaymentCodes billpaymentcodes = new BillPaymentCodes();

		billpaymentcodes.setMerchantcode(merchantcode);

		Network retrievecountry = networkDAO.getNetwork(networkuuid);

		countryuuid = retrievecountry.getCountryUuid();

		BillPaymentCodes existingbillpaymnetcode = billpaymentcodesDAO.getBillPaymentCodes(billpaymentcodes);

		if (existingbillpaymnetcode != null) {

			alluuid = existingbillpaymnetcode.getUuid();
		} else {
			alluuid = transactioinid;
		}

		BillPaymentCodes p = new BillPaymentCodes();

		p.setMerchantcode(merchantcode);
		p.setMerchantname(merchantname);
		p.setCountryuuid(countryuuid);
		p.setNetworkuuid(networkuuid);
		p.setMsisdn(merchantmsisdn);
		p.setMerchantid(merchantid);
		Calendar c = Calendar.getInstance();
		c.set(NumberUtils.toInt(addYear), NumberUtils.toInt(addMonth) - 1, NumberUtils.toInt(addDay));
		p.setDateadded(c.getTime());

		boolean response = billpaymentcodesDAO.updateBillPaymentCodes(alluuid, p);

		billpaymentCache.put(new Element(p.getUuid(), p));

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {

		merchantname = StringUtils.trimToEmpty(request.getParameter("merchantname"));
		// countryuuid =
		// StringUtils.trimToEmpty(request.getParameter("countryUuid"));
		merchantcode = StringUtils.trimToEmpty(request.getParameter("merchantcode"));
		networkuuid = StringUtils.trimToEmpty(request.getParameter("networkUuid"));
		merchantmsisdn = StringUtils.trimToEmpty(request.getParameter("merchantmsisdn"));
		merchantid = StringUtils.trimToEmpty(request.getParameter("merchantid"));
		addDay = StringUtils.trimToEmpty(request.getParameter("addDay"));
		addMonth = StringUtils.trimToEmpty(request.getParameter("addMonth"));
		addYear = StringUtils.trimToEmpty(request.getParameter("addYear"));

	}

	/**
	 * Place some of the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		BillparamHash = new HashMap<>();

		BillparamHash.put("merchantname", merchantname);
		BillparamHash.put("countryuuid", countryuuid);
		BillparamHash.put("merchantcode", merchantcode);
		BillparamHash.put("merchantmsisdn", merchantmsisdn);
		BillparamHash.put("merchantid", merchantid);
		BillparamHash.put("networkuuid", networkuuid);
	}

}

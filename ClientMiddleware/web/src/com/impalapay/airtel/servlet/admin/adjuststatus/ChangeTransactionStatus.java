package com.impalapay.airtel.servlet.admin.adjuststatus;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.airtel.beans.transaction.Transaction;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionForexrate;
import com.impalapay.airtel.beans.transaction.forexrate.TransactionUpdateStatus;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.accountmgmt.ManageAccountDAO;
import com.impalapay.airtel.persistence.forex.ForexEngineHistoryDAO;
import com.impalapay.airtel.persistence.systemlog.SystemLogDAO;
import com.impalapay.airtel.persistence.transaction.TransactionDAO;
import com.impalapay.airtel.persistence.transaction.TransactionStatusDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionUpdateStatusDAO;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.CurrencyConvertUtil;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add/update forex.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Nov 24, 2014
 *
 * @author <a href="mailto:mike@impalapay.com">Michael Wakahe</a>
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 *
 */

public class ChangeTransactionStatus extends HttpServlet {

	final String ERROR_NO_TRANSACTIONID = "Please provide the transaction Reference Number";
	final String ERROR_NO_CURRENTSTATUS = "Please provide the Current Status of the Transaction";
	final String ERROR_NO_UPDATESTATUS = "Please provide the Status to be Updated";
	final String ERROR_NO_QUOTECURRENCY = "Please select the Sending Currency type(e.g USD,GBP).";
	final String ERROR_NO_BASECURRENCY = "Please select the Recipient Currency from Drop-down list(e.g KES,UGX).";
	final String ERROR_NO_IMPALARATE = "Please provide Impala's Rate.";
	final String ERROR_UNABLE_ADD = "Could not add updatestatus to checker update status";
	final String ERROR_NOT_ALLOWED = "You dont have access rights to perform this Action,Please Contact System Administrator";
	private String basecurrency, currentstatus, updatestatus, quotecurrency, username, transactionuuid, jsonResult;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;
	private Map<String, String> expected = new HashMap<>();
	private HashMap<String, String> transactionStatusHash = new HashMap<>();
	private HashMap<String, String> countryCode = new HashMap<>();
	private HashMap<String, String> currencyCode = new HashMap<>();

	private SystemLogDAO systemlogDAO;
	private ManageAccountDAO managementaccountDAO;
	private ForexEngineHistoryDAO forexengineDAO;
	private Cache transactionStatusCache, countryCache;
	private TransactionDAO transactionDAO;
	private TransactionStatusDAO transactionstatusDAO;
	private TransactionForexDAO transactionforexDAO;
	private TransactionUpdateStatusDAO transactionUpdateStatusDAO;
	private boolean response;
	private JsonElement root2 = null;
	private HttpSession session;
	private String transactioinid = "", transtatusuuid = "", inprogress = "", success = "", unknownerror = "",
			failedtransaction = "", reversedtransaction = "", exchangerate = "", amount = "", amountstring = "",
			originalamount = "", transactionid = "", recipientcountry = "", currentstatuscode = "",
			updatestatuscode = "", currencypair = "";

	private String statusdescription, statuscode = "";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		CacheManager mgr = CacheManager.getInstance();
		forexengineDAO = ForexEngineHistoryDAO.getInstance();
		managementaccountDAO = ManageAccountDAO.getInstance();
		systemlogDAO = SystemLogDAO.getInstance();
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
		transactionDAO = TransactionDAO.getInstance();
		transactionstatusDAO = TransactionStatusDAO.getInstance();
		transactionforexDAO = TransactionForexDAO.getinstance();
		transactionUpdateStatusDAO = TransactionUpdateStatusDAO.getinstance();

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_PARAMETERS, paramHash);

		// No First currency provided
		if (StringUtils.isBlank(transactionuuid)) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, ERROR_NO_TRANSACTIONID);

			// No Base rate provided
		} else if (StringUtils.isBlank(currentstatus)) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, ERROR_NO_CURRENTSTATUS);
			// No Impala Rate Provided provided
		} else if (StringUtils.isBlank(updatestatus)) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, ERROR_NO_UPDATESTATUS);
		} else if (StringUtils.isBlank(quotecurrency)) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY,
					ERROR_NO_QUOTECURRENCY + quotecurrency);
		} else if (StringUtils.isBlank(basecurrency)) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, ERROR_NO_BASECURRENCY);

		} else if (!addreversal()) {
			session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, ERROR_NOT_ALLOWED);

		} else {

			String receivedresponse = UpdateTransactionStatus();

			root2 = new JsonParser().parse(receivedresponse);
			statuscode = root2.getAsJsonObject().get("status_code").getAsString();
			statusdescription = root2.getAsJsonObject().get("status_description").getAsString();

			success = "S000";

			if (statuscode.equalsIgnoreCase(success)) {

				// If we get this far then all parameter checks are ok.
				session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_SUCCESS_KEY, "Successfull");

				// Reduce our session data
				session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_PARAMETERS, null);
				session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, null);

			} else {
				session.setAttribute(SessionConstants.ADMIN_UPDATE_TRANSACTIONSTATUS_ERROR_KEY, statusdescription);
			}

		}

		// response.sendRedirect("addAccount.jsp");a
		response.sendRedirect("changestatus.jsp");
	}

	/**
	 *
	 */
	private String UpdateTransactionStatus() {
		Account account = null;
		Transaction transactions = null;
		TransactionForexrate transactionforex = null;
		TransactionUpdateStatus transactionforupdate = null;
		double amount = 0, convertedamountToWallet = 0, finalconvertedamount = 0, localamount = 0, convertedamount = 0,
				usedexchangerate = 0;
		TransactionStatus transactionstatus = null, transactionstatustoupdate = null;

		TransactionUpdateStatus confirmexist = null, confirmexisthistory = null;

		String newtransactionuuid = "", accounttype = "";

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();
		// Fetch the transaction
		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		List keys;

		TransactionStatus status;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status.getUuid(), status.getStatus());
		}

		// fetch from cache
		Country country;
		keys = countryCache.getKeys();
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryCode.put(country.getCurrencycode(), country.getUuid());
		}

		keys = countryCache.getKeys();
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			currencyCode.put(country.getUuid(), country.getCurrencycode());
		}

		if (!transactionStatusHash.containsKey(currentstatus) || !transactionStatusHash.containsKey(updatestatus)) {
			expected.put("status_code", "00032");
			expected.put("status_description", "The Provided Transaction Status does not Exist!");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		transactions = transactionDAO.getTransactionstatus1(transactionuuid);

		if (transactions == null) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALID_REFERENCENUMBER);
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		transtatusuuid = transactions.getTransactionStatusUuid();
		amount = transactions.getAmount();
		transactionstatus = transactionstatusDAO.getTransactionStatus(transtatusuuid);
		transactionstatustoupdate = transactionstatusDAO.getTransactionStatus(updatestatus);

		confirmexist = transactionUpdateStatusDAO.getTransactionUpdateStatusUuid(transactions.getUuid());

		confirmexisthistory = transactionUpdateStatusDAO.getTransactionUpdateStatusUuidHistory(transactions.getUuid());

		if (confirmexist != null) {
			expected.put("status_code", "00032");
			expected.put("status_description", "Status Update request already exist for this Transaction");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		if (confirmexisthistory != null) {
			expected.put("status_code", "00032");
			expected.put("status_description", "This Transaction has already been Updated and fully Approved Before");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		// Check if the current status matches.
		if (!transtatusuuid.equalsIgnoreCase(currentstatus)) {
			expected.put("status_code", "00032");
			expected.put("status_description",
					"Your current Status does not match the one in the Database.PLease consult the system administrator");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		currentstatuscode = transactionStatusHash.get(transtatusuuid);
		updatestatuscode = transactionStatusHash.get(updatestatus);

		// set the transaction status in the database to inprogress.
		account = new Account();
		account.setUuid(transactions.getAccountUuid());

		// find the below from the above details
		unknownerror = "00032";
		inprogress = "S001";
		success = "S000";

		// Update transaction status allowed
		failedtransaction = "00029";
		reversedtransaction = "R000";

		// if transaction is inprogress make sure the update transaction is only failed
		// transaction
		if (currentstatuscode.equalsIgnoreCase(inprogress)) {

			if (!updatestatuscode.equalsIgnoreCase(failedtransaction)) {
				expected.put("status_code", "00032");
				expected.put("status_description",
						"Your suggested updatestatus change is not allowed for transactions with a current status of CREDIT_INPROGRESS");
				jsonResult = g.toJson(expected);
				return jsonResult;
			}

			// for transaction that have unknown error
		} else if (currentstatuscode.equalsIgnoreCase(unknownerror)) {

			if (!updatestatuscode.equalsIgnoreCase(failedtransaction)
					&& !updatestatuscode.equalsIgnoreCase(inprogress)) {
				expected.put("status_code", "00032");
				expected.put("status_description",
						"Your suggested updatestatus change is not allowed for transactions with a current status of UNKNOWN_ERROR");
				jsonResult = g.toJson(expected);
				return jsonResult;
			}
		} else if (currentstatuscode.equalsIgnoreCase(success)) {

			// means the transaction is on successs
			if (!transactionStatusHash.get(updatestatus).equalsIgnoreCase(failedtransaction)
					&& !updatestatuscode.equalsIgnoreCase(reversedtransaction)) {
				expected.put("status_code", "00032");
				expected.put("status_description",
						"Your suggested updatestatus change is not allowed for transactions with a current status of SUCCESS");
				jsonResult = g.toJson(expected);
				return jsonResult;
			}

		} else {

			expected.put("status_code", "00032");
			expected.put("status_description",
					"Current Status of this Transaction Does not allow Status Change.Contact System administrator for more Help "
							+ transactionStatusHash.get(transtatusuuid));
			jsonResult = g.toJson(expected);
			return jsonResult;

		}

		// search for certain elements and populate database.
		// check if the transaction is inprogress or success
		if (currentstatuscode.equalsIgnoreCase(inprogress) || currentstatuscode.equalsIgnoreCase(success)) {

			// Pull from the forextransaction database a transactionforex object
			transactionforex = transactionforexDAO.getTransactionForexsUuid(transactions.getUuid());

			if (transactionforex == null) {
				expected.put("status_code", "00032");
				expected.put("status_description",
						"Transaction not available in forextransaction table.Please contact system administrator");
				jsonResult = g.toJson(expected);
				return jsonResult;
			}

			transactionid = transactionforex.getTransactionUuid();
			accounttype = countryCode.get(transactionforex.getAccounttype());
			localamount = transactionforex.getLocalamount();
			recipientcountry = transactionforex.getRecipientcountry();//
			convertedamount = transactionforex.getConvertedamount();
			usedexchangerate = transactionforex.getImpalarate();

		} else {
			currencypair = currencyCode.get(basecurrency) + "/" + currencyCode.get(quotecurrency);
			// Loop through all the available forexrates history to see if what has been
			// inputed exist
			List forexhistorycheck = forexengineDAO.getAllForexHistory(currencypair, Double.parseDouble(exchangerate));

			int size = forexhistorycheck.size();

			if (size == 0) {
				expected.put("status_code", "00032");
				expected.put("status_description", "No records Exist for the CurrencyPair " + currencypair
						+ " With a Rate of " + exchangerate + "In ForexHistory Records");
				jsonResult = g.toJson(expected);
				return jsonResult;

			}

			// Reverse compute
			convertedamountToWallet = CurrencyConvertUtil.multiplyForex(Double.parseDouble(originalamount),
					Double.parseDouble(exchangerate));
			finalconvertedamount = CurrencyConvertUtil.doubleToInteger(convertedamountToWallet);
			// convert amount from double to string
			// amountstring = String.valueOf(finalconvertedamount);

			// if (amountstring != String.valueOf(transactions.getAmount())) {
			if (finalconvertedamount != transactions.getAmount()) {
				expected.put("status_code", "00032");
				expected.put("status_description", "Forex Calculation Error!!. Expected Conversion is "
						+ transactions.getAmount() + " But the amount bbeing returned now is " + finalconvertedamount);
				jsonResult = g.toJson(expected);
				return jsonResult;

			}

			transactionid = transactions.getUuid();
			accounttype = basecurrency;
			localamount = Double.parseDouble(originalamount);
			recipientcountry = transactions.getRecipientCountryUuid();//
			convertedamount = transactions.getAmount();
			usedexchangerate = Double.parseDouble(exchangerate);

		}

		newtransactionuuid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		transactionforupdate = new TransactionUpdateStatus();

		transactionforupdate.setUuid(newtransactionuuid);
		transactionforupdate.setTransactionUuid(transactionid);
		transactionforupdate.setSourcecurrency(accounttype);
		transactionforupdate.setLocalamount(localamount);
		transactionforupdate.setRecipientcurrency(recipientcountry);
		transactionforupdate.setConvertedamount(convertedamount);
		transactionforupdate.setImpalarate(usedexchangerate);
		transactionforupdate.setCurrentstatus(currentstatus);
		transactionforupdate.setUpdatestatus(updatestatus);

		SystemLog systemlog = new SystemLog();
		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');
		systemlog.setUsername(newtransactionuuid);
		systemlog.setUuid(transactioinid);
		systemlog.setAction(username + "made a request to adjust transaction with  " + transactionid
				+ "from this status " + currentstatus + " to " + updatestatus);

		// systemlogDAO.putsystemlog(systemlog);
		// response =
		// transactionUpdateStatusDAO.addTransactionUpdateStatus(transactionforupdate);
		if (!transactionUpdateStatusDAO.addTransactionUpdateStatus(transactionforupdate)) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_FAIL);
			jsonResult = g.toJson(expected);
			return jsonResult;
		} else {

			systemlogDAO.putsystemlog(systemlog);
		}
		expected.put("status_code", "S000");
		expected.put("status_description", APIConstants.COMMANDSTATUS_OK);
		jsonResult = g.toJson(expected);
		return jsonResult;
	}

	public boolean addreversal() {

		if (StringUtils.equals(username, PropertiesConfig.getConfigValue("ADMIN_USERNAME"))) {
			response = true;
		} else {
			ManagementAccount status = managementaccountDAO.getAccountName(username);

			// response = status.isUpdatereversal();
			response = status.isUpdatereversal();
			System.out.println("Username is " + username + " Can update reversal is " + status);
		}

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		transactionuuid = StringUtils.trimToEmpty(request.getParameter("referencenumber"));
		basecurrency = StringUtils.trimToEmpty(request.getParameter("originatecurrency"));
		quotecurrency = StringUtils.trimToEmpty(request.getParameter("recipientcurrency"));
		currentstatus = StringUtils.trimToEmpty(request.getParameter("currenttransferstatus"));
		updatestatus = StringUtils.trimToEmpty(request.getParameter("updatetransactionstatus"));
		username = StringUtils.trimToEmpty(request.getParameter("username"));
		exchangerate = StringUtils.trimToEmpty(request.getParameter("exchangerate"));
		originalamount = StringUtils.trimToEmpty(request.getParameter("amount"));

	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		/**
		 * paramHash = new HashMap<>();
		 * 
		 * paramHash.put("currencypair", createdpair); paramHash.put("baserate",
		 * baserate); paramHash.put("impalarate", impalarate);
		 **/

	}

}

/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */

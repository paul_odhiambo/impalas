package com.impalapay.airtel.servlet.admin.accounts;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.beans.network.Network;
import com.impalapay.mno.persistence.network.NetworkDAO;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add an msisdn to a specific country.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 11, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class AddNetwork extends HttpServlet {

	final String ERROR_NO_COUNTRYNAME = "Please provide the country name";
	final String ERROR_NO_ROUTEENDPOINTURL = "Please provide the remit URI";
	final String ERROR_NO_ROUTEBRIDGEENDPOINTURL = "Please provide the remit bridge URI";
	final String ERROR_NO_ROUTEACCOUNTURL = "Please provide the account check URI";
	final String ERROR_NO_ROUTEBRIDGEACCOUNTURL = "Please provide the account check Bridge URI";
	final String ERROR_NO_ROUTEREVERSEURL = "Please provide the reversal URI";
	final String ERROR_NO_ROUTEBRIDGEREVERSEURL = "Please provide the reversal Bridge URI";
	final String ERROR_NO_ROUTEQUERYURL = "Please provide the query  URI";
	final String ERROR_NO_ROUTEBRIDGEQUERYURL = "Please provide the query Bridge  URI";
	final String ERROR_NO_ROUTEFOREXURL = "Please provide the Forex URI";
	final String ERROR_NO_ROUTEBRIDGEFOREXURL = "Please provide the Forex Bridge  URI";
	final String ERROR_NO_ROUTEBALANCEURL = "Please provide the Balance/Float URI";
	final String ERROR_NO_ROUTEBRIDGEBALANCEURL = "Please provide the Balance/Float Bridge URI";
	final String ERROR_NO_ROUTEUSERNAME = "Please provide the route USERNAME";
	final String ERROR_NO_ROUTEPASSWORD = "please provide the route PASSWORD";
	final String ERROR_NO_ROUTENAME = "please provide the route/network NAME";
	final String ERROR_NO_ROUTEPARTNERNAME = "please provide the route/network PARTNERSNAME";
	final String ERROR_NO_ROUTECOMMISSION = "please provide the route/network COMMISSION";
	final String ERROR_NO_ROUTESTATUS = "please provide the route/network STATUS";
	final String ERROR_NO_UNABLETOUPDATEROUTE = "unable to update route";
	final String ERROR_ROUTENAME_EXISTS = "The Unique Name provided already exists in the system.";
	final String ERROR_ROUTEFOREX_STATUS = "Please state if forex will be allowed or not.";
	final String ERROR_ROUTEREVERSAL_STATUS = "Please state if reversals will be allowed or not.";
	final String ERROR_ROUTEACCOUNTCHECK_STATUS = "Please state if acccount check will be allowed or not.";
	final String ERROR_ROUTEQUERYCHECK_STATUS = "Please state if query check will be allowed or not.";
	final String ERROR_ROUTECOMMMISSION_STATUS = "Please state if route allows percentage commission or not.";
	final String ERROR_ROUTEBALANCECHECK_STATUS = "Please state if balance check will be allowed or not.";

	private String countryuuid, remitip, bridgeremitip, queryip, bridgequeryip, balanceip, bridgebalanceip, reversalip,
			bridgereversalip, forexip, bridgeforexip, accountcheckip, bridgeaccountcheckip, extraurl, username,
			password, networkname, partnername, supportforex, supportreversal, supportaccountcheck, supportquerycheck,
			supportbalancecheck, supportcommissionpercentage, networkstatusuuid, commission;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;

	private NetworkDAO networkDAO;
	private CacheManager cacheManager;
	private HttpSession session;

	private Cache networkCache;

	private String alluuid = "";

	private String transactioinid = "";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		networkDAO = NetworkDAO.getInstance();
		// cacheManager = CacheManager.getInstance();
		networkCache = mgr.getCache(CacheVariables.CACHE_NETWORK_BY_UUID);
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_PARAMETERS, paramHash);

		// No country provided
		if (StringUtils.isBlank(countryuuid)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_COUNTRYNAME);

			// No remit uri provided
		} else if (StringUtils.isBlank(remitip)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEENDPOINTURL);

			// No bridgeremit uri provided
		} else if (StringUtils.isBlank(bridgeremitip)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEBRIDGEENDPOINTURL);

			// No queryip provided
		} else if (StringUtils.isBlank(queryip)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEQUERYURL);

			// No bridgequeryip provided
		} else if (StringUtils.isBlank(bridgequeryip)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEBRIDGEQUERYURL);

			// No balanceip provided
		} else if (StringUtils.isBlank(balanceip)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEBALANCEURL);

			// No bridgebalanceip provided
		} else if (StringUtils.isBlank(bridgebalanceip)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEBRIDGEBALANCEURL);
			// No reversalip provided
		} else if (StringUtils.isBlank(reversalip)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEREVERSEURL);
			// No bridgereversalip provided
		} else if (StringUtils.isBlank(bridgereversalip)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEBRIDGEREVERSEURL);

			// No forexip provided
		} else if (StringUtils.isBlank(forexip)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEFOREXURL);

			// No bridgeforexip provided
		} else if (StringUtils.isBlank(bridgeforexip)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEBRIDGEFOREXURL);

			// No accountcheckip provided
		} else if (StringUtils.isBlank(accountcheckip)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEACCOUNTURL);
			// No bridgeaccountcheckip provided
		} else if (StringUtils.isBlank(bridgeaccountcheckip)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEBRIDGEACCOUNTURL);

			// No route username provided
		} else if (StringUtils.isBlank(username)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEUSERNAME);
			// No route password provided
		} else if (StringUtils.isBlank(password)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEPASSWORD);
			// No route networkname provided
		} else if (StringUtils.isBlank(networkname)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTENAME);
			// No route partnername provided
		} else if (StringUtils.isBlank(password)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEPARTNERNAME);
			// No route commission provided
		} else if (StringUtils.isBlank(commission)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTECOMMISSION);
			// No route commission provided
		} else if (StringUtils.isBlank(networkstatusuuid)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTESTATUS);

		} else if (!addNetwork()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_UNABLETOUPDATEROUTE);
		} else {

			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_SUCCESS_KEY, "s");

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, null);

			addNetwork();

		}

		response.sendRedirect("addNetwork.jsp");

	}

	/**
	 *
	 */
	private boolean addNetwork() {
		Network network2 = new Network();

		network2.setNetworkname(networkname);

		Network existingnetwork = networkDAO.getNetwork(network2);

		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		if (existingnetwork != null) {

			alluuid = existingnetwork.getUuid();
		} else {
			alluuid = transactioinid;
		}

		Network network = new Network();
		network.setUuid(alluuid);
		network.setCountryUuid(countryuuid);
		network.setRemitip(remitip);
		network.setBridgeremitip(bridgeremitip);
		network.setQueryip(queryip);
		network.setBridgequeryip(bridgequeryip);
		network.setBalanceip(balanceip);
		network.setBridgebalanceip(bridgebalanceip);
		network.setReversalip(reversalip);
		network.setBridgereversalip(bridgereversalip);
		network.setForexip(forexip);
		network.setBridgeforexip(bridgeforexip);
		network.setAccountcheckip(accountcheckip);
		network.setBridgeaccountcheckip(bridgeaccountcheckip);
		network.setExtraurl(extraurl);
		network.setUsername(username);
		network.setPassword(password);
		network.setNetworkname(networkname);
		network.setPartnername(partnername);
		network.setCommission(NumberUtils.toDouble(commission));
		network.setSupportforex(Boolean.parseBoolean(supportforex));
		network.setSupportreversal(Boolean.parseBoolean(supportreversal));
		network.setSupportaccountcheck(Boolean.parseBoolean(supportaccountcheck));
		network.setSupportbalancecheck(Boolean.parseBoolean(supportbalancecheck));
		network.setSupportquerycheck(Boolean.parseBoolean(supportquerycheck));
		network.setSupportcommissionpercentage(Boolean.parseBoolean(supportcommissionpercentage));
		network.setNetworkStatusUuid(networkstatusuuid);

		boolean response = networkDAO.updateNetwork(alluuid, network);

		networkCache.put(new Element(network.getUuid(), network));

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		supportcommissionpercentage = StringUtils.trimToEmpty(request.getParameter("optioncommissionpercentage"));
		supportbalancecheck = StringUtils.trimToEmpty(request.getParameter("optionbalancecheck"));
		supportquerycheck = StringUtils.trimToEmpty(request.getParameter("optionquerycheck"));
		supportaccountcheck = StringUtils.trimToEmpty(request.getParameter("optionaccountcheck"));
		supportreversal = StringUtils.trimToEmpty(request.getParameter("optionreversal"));
		supportforex = StringUtils.trimToEmpty(request.getParameter("optionforex"));
		networkstatusuuid = StringUtils.trimToEmpty(request.getParameter("networkstausuuid"));
		commission = StringUtils.trimToEmpty(request.getParameter("commission"));
		partnername = StringUtils.trimToEmpty(request.getParameter("partnername"));
		networkname = StringUtils.trimToEmpty(request.getParameter("networkname"));
		username = StringUtils.trimToEmpty(request.getParameter("username"));
		password = StringUtils.trimToEmpty(request.getParameter("password"));
		extraurl = StringUtils.trimToEmpty(request.getParameter("extraurl"));
		bridgeaccountcheckip = StringUtils.trimToEmpty(request.getParameter("bridgeaccountcheckip"));
		accountcheckip = StringUtils.trimToEmpty(request.getParameter("accountcheckip"));
		bridgeforexip = StringUtils.trimToEmpty(request.getParameter("bridgeforexip"));
		forexip = StringUtils.trimToEmpty(request.getParameter("forexip"));
		bridgereversalip = StringUtils.trimToEmpty(request.getParameter("bridgereversalip"));
		reversalip = StringUtils.trimToEmpty(request.getParameter("reversalip"));
		bridgebalanceip = StringUtils.trimToEmpty(request.getParameter("bridgebalanceip"));
		balanceip = StringUtils.trimToEmpty(request.getParameter("balanceip"));
		bridgequeryip = StringUtils.trimToEmpty(request.getParameter("bridgequeryip"));
		queryip = StringUtils.trimToEmpty(request.getParameter("queryip"));
		bridgeremitip = StringUtils.trimToEmpty(request.getParameter("bridgeremitip"));
		remitip = StringUtils.trimToEmpty(request.getParameter("remitip"));
		countryuuid = StringUtils.trimToEmpty(request.getParameter("countryuuid"));

	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		paramHash = new HashMap<>();

		paramHash.put("countryuuid", countryuuid);
		paramHash.put("remitip", remitip);
		paramHash.put("bridgeremitip", bridgeremitip);
		paramHash.put("queryip", queryip);
		paramHash.put("bridgequeryip", bridgequeryip);
		paramHash.put("balanceip", balanceip);
		paramHash.put("bridgebalanceip", bridgebalanceip);
		paramHash.put("reversalip", reversalip);
		paramHash.put("bridgereversalip", bridgereversalip);
		paramHash.put("forexip", forexip);
		paramHash.put("bridgeforexip", bridgeforexip);
		paramHash.put("accountcheckip", accountcheckip);
		paramHash.put("bridgeaccountcheckip", bridgeaccountcheckip);
		paramHash.put("extraurl", extraurl);
		paramHash.put("password", password);
		paramHash.put("username", username);
		paramHash.put("networkname", networkname);
		paramHash.put("partnername", partnername);
		paramHash.put("supportforex", supportforex);
		paramHash.put("supportreversal", supportreversal);
		paramHash.put("supportaccountcheck", supportaccountcheck);
		paramHash.put("supportquerycheck", supportquerycheck);
		paramHash.put("supportbalancecheck", supportbalancecheck);
		paramHash.put("supportcommissionpercentage", supportcommissionpercentage);
		paramHash.put("networkstatusuuid", networkstatusuuid);
		paramHash.put("commission", commission);
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		// response.sendRedirect("../index.jsp");
	}

}

package com.impalapay.airtel.servlet.admin.forex;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;
import com.impalapay.airtel.beans.forex.ForexEngine;
import com.impalapay.airtel.beans.forex.ForexEngineHistory;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.airtel.persistence.accountmgmt.ManageAccountDAO;
import com.impalapay.airtel.persistence.forex.ForexEngineHistoryDAO;
import com.impalapay.airtel.persistence.systemlog.SystemLogDAO;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import net.sf.ehcache.CacheManager;

import org.apache.commons.lang3.StringUtils;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add/update forex.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Nov 24, 2014
 *
 * @author <a href="mailto:mike@impalapay.com">Michael Wakahe</a>
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 *
 */
public class DeletecheckerForex extends HttpServlet {

	final String ERROR_NO_CHECKERFOREXUUID = "No checker forexrate UUID";
	final String ERROR_NO_USERNAME = "No username provided";
	final String ERROR_UNABLE_DELETE = "unable to delete checker forex .";
	final String ERROR_NOT_ALLOWED = "You dont have access rights to perform this Action,Please Contact System Administrator";
	final String SUCCESS = "You have successfully deleted the checker forex rate";
	private String forexuuid, createdpair, username;
	private double baserate = 0, impalarate = 0;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;

	private SystemLogDAO systemlogDAO;
	private ManageAccountDAO managementaccountDAO;
	private ForexEngineHistoryDAO forexenginehistoryDAO;
	private boolean response;
	private ForexEngineHistory checkerforexrate;

	private HttpSession session;

	private String transactioinid = "";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		// emailValidator = EmailValidator.getInstance();
		forexenginehistoryDAO = ForexEngineHistoryDAO.getInstance();
		managementaccountDAO = ManageAccountDAO.getInstance();

		CacheManager.getInstance();
		systemlogDAO = SystemLogDAO.getInstance();

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_PARAMETERS, paramHash);

		// No First currency provided
		if (StringUtils.isBlank(forexuuid)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_ERROR_KEY, ERROR_NO_CHECKERFOREXUUID);

			// No Base rate provided
		} else if (StringUtils.isBlank(username)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_ERROR_KEY, ERROR_NO_USERNAME);

		} else if (!addforex()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_ERROR_KEY, ERROR_NOT_ALLOWED);

		} else {

			if (!deletecheckerForex()) {
				session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_ERROR_KEY, ERROR_UNABLE_DELETE);
			}

			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_SUCCESS_KEY, SUCCESS);

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_ADD_FOREX_ERROR_KEY, null);

		}

		response.sendRedirect("checkerforexrate.jsp");

	}

	/**
	 *
	 */
	private boolean deletecheckerForex() {
		response = false;
		checkerforexrate = forexenginehistoryDAO.getCheckerForexHistoryBYUUID(forexuuid);

		if (checkerforexrate != null) {
			createdpair = checkerforexrate.getCurrencypair();
			baserate = checkerforexrate.getMarketrate();
			impalarate = checkerforexrate.getSpreadrate();

			SystemLog systemlog = new SystemLog();
			transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');
			systemlog.setUsername(username);
			systemlog.setUuid(transactioinid);
			systemlog.setAction(username + " deleted the checkerforexrate for currencypair " + createdpair
					+ " to spreadrate= " + impalarate + " the marketrate= " + baserate);

			response = forexenginehistoryDAO.deleteCheckerForexRate(forexuuid);
			if (response) {
				systemlogDAO.putsystemlog(systemlog);
			}

		}

		return response;
	}

	public boolean addforex() {

		if (StringUtils.equals(username, PropertiesConfig.getConfigValue("ADMIN_USERNAME"))) {
			response = true;
		} else {
			ManagementAccount status = managementaccountDAO.getAccountName(username);

			response = status.isChecker();
		}

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		forexuuid = StringUtils.trimToEmpty(request.getParameter("reject"));
		username = StringUtils.trimToEmpty(request.getParameter("username"));

	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		paramHash = new HashMap<>();

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		// response.sendRedirect("../index.jsp");
	}
}

/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */

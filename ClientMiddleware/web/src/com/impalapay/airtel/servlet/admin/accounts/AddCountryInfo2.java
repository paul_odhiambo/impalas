package com.impalapay.airtel.servlet.admin.accounts;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.geolocation.CountryDAO;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add/update forex.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Nov 24, 2014
 *
 * @author <a href="mailto:mike@impalapay.com">Michael Wakahe</a>
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 *
 */
public class AddCountryInfo2 extends HttpServlet {

	final String ERROR_NO_COUNTRYREMITIP = "Please provide the country remit IP.";
	final String ERROR_NO_COUNTRYBALANCEIP = "Please provide the country balance IP";
	final String ERROR_NO_COUNTRYVERIFYIP = "Please provide the country veriofy IP";
	final String ERROR_NO_COUNTRYUSERNAME = "Please provide the country USERNAME";
	final String ERROR_NO_COUNTRYPASSWORD = "please provide the country PASSWORD";
	final String ERROR_NO_UNABLETOUPDATECOUNTRY = "unable to update country";

	private String countryremitip, countryverifyip, countrybalanceip, username, password, countryUuid;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;

	private CountryDAO countryDAO;
	private CacheManager cacheManager;
	private HttpSession session;

	private Cache countryCache;

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		countryDAO = CountryDAO.getInstance();
		// cacheManager = CacheManager.getInstance();
		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_PARAMETERS, paramHash);

		// No country remitip provided
		if (StringUtils.isBlank(countryremitip)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_ERROR_KEY, ERROR_NO_COUNTRYREMITIP);

			// No country verifyip provided
		} else if (StringUtils.isBlank(countryverifyip)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_ERROR_KEY, ERROR_NO_COUNTRYVERIFYIP);

			// No country balanceip provided
		} else if (StringUtils.isBlank(countrybalanceip)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_ERROR_KEY, ERROR_NO_COUNTRYBALANCEIP);

			// No country username provided
		} else if (StringUtils.isBlank(username)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_ERROR_KEY, ERROR_NO_COUNTRYUSERNAME);
			// No country username provided
		} else if (StringUtils.isBlank(password)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_ERROR_KEY, ERROR_NO_COUNTRYPASSWORD);

		} else if (!addCountryUpdate()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_ERROR_KEY,
					ERROR_NO_UNABLETOUPDATECOUNTRY + countryUuid);

		} else {

			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_SUCCESS_KEY, "s");

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_ADD_COUNTRYINFO_ERROR_KEY, null);

		}

		response.sendRedirect("addCountry.jsp");

	}

	/**
	 *
	 */
	private boolean addCountryUpdate() {
		Country country = new Country();

		Country country2 = countryDAO.getCountry(countryUuid);

		country.setUuid(countryUuid);
		country.setCountryremitip(countryremitip);
		country.setCountryverifyip(countryverifyip);
		country.setCountrybalanceip(countrybalanceip);
		country.setUsername(username);
		country.setPassword(password);
		country.setAirtelnetwork(country2.getAirtelnetwork());
		country.setName(country2.getName());
		country.setCurrencycode(country2.getCurrencycode());
		country.setCountrycode(country2.getCountrycode());
		country.setCurrency(country2.getCurrency());

		boolean response = countryDAO.updateCountry(countryUuid, country);

		countryCache.put(new Element(country.getUuid(), country));

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		countrybalanceip = StringUtils.trimToEmpty(request.getParameter("countrybalanceip"));
		countryUuid = StringUtils.trimToEmpty(request.getParameter("countryUuid"));
		countryverifyip = StringUtils.trimToEmpty(request.getParameter("countryverifyip"));
		countryremitip = StringUtils.trimToEmpty(request.getParameter("countryremitip"));
		username = StringUtils.trimToEmpty(request.getParameter("username"));
		password = StringUtils.trimToEmpty(request.getParameter("password"));

	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		paramHash = new HashMap<>();

		paramHash.put("countrybalanceip", countrybalanceip);
		paramHash.put("countryverifyip", countryverifyip);
		paramHash.put("countryremitip", countryremitip);
		paramHash.put("username", username);
		paramHash.put("password", password);
		paramHash.put("countryUuid", countryUuid);

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		// response.sendRedirect("../index.jsp");
	}
}

/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */

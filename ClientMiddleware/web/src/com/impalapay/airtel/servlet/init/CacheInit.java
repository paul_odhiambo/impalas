package com.impalapay.airtel.servlet.init;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.Status;
import net.sf.ehcache.config.SizeOfPolicyConfiguration;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.DiskStoreConfiguration;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.StorableBean;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;
import com.impalapay.airtel.beans.bank.BankCodes;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.accountmgmt.AccountDAO;
import com.impalapay.airtel.persistence.accountmgmt.ManageAccountDAO;
import com.impalapay.airtel.persistence.bank.BankCodesDAO;
import com.impalapay.airtel.persistence.billpayment.BillpaymentCodesDAO;
import com.impalapay.airtel.persistence.clientipaddress.ClientIpaddressDAO;
import com.impalapay.airtel.persistence.forex.ForexEngineDAO;
import com.impalapay.airtel.persistence.forex.ForexEngineHistoryDAO;
import com.impalapay.airtel.persistence.forex.ForexHistoryDAO;
import com.impalapay.airtel.persistence.forex.GbpForexDAO;
import com.impalapay.airtel.persistence.forex.UsdForexDAO;
import com.impalapay.airtel.persistence.geolocation.CountryDAO;
//import com.impalapay.airtel.persistence.geolocation.CountryDAO2;
import com.impalapay.airtel.persistence.geolocation.CountryMsisdnDAO;
import com.impalapay.airtel.persistence.thirdreference.ThirdReferenceDAO;
import com.impalapay.airtel.persistence.transaction.TransactionDAO;
import com.impalapay.airtel.persistence.transaction.TransactionStatusDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionForexDAO;
import com.impalapay.airtel.persistence.transaction.forex.TransactionUpdateStatusDAO;
//import com.impalapay.airtel.persistence.topup.TopupStatusDAO;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.beans.route.RouteDefine;
import com.impalapay.collection.persistence.network.CollectionDefineDAOImpl;
import com.impalapay.collection.persistence.network.CollectionNetworkDAOImpl;
import com.impalapay.collection.persistence.network.CollectionTypeDAOimpl;
import com.impalapay.collection.persistence.networksubaccount.CollectionNetworkSubaccountDAOImpl;
import com.impalapay.mno.persistence.network.NetworkDAO;
import com.impalapay.mno.persistence.prefix.PrefixDAO;
import com.impalapay.persistence.routing.RouteDAO;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Brings certain frequently accessed variables into cache. <br />
 * Ehcache objects have to be serializable to allow for off-disk storage.
 * <p>
 * Copyright (c) ImpalaPay LTD., June 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @author <a href="mailto:mike@impalapay.com">Michael Wakahe</a>
 */
public class CacheInit extends HttpServlet {

	protected AccountDAO accountDAO;
	protected ManageAccountDAO manageaccountDAO;
	protected CountryDAO countryDAO;
	protected TransactionStatusDAO transactionStatusDAO;
	protected TransactionDAO transactionDAO;
	protected ThirdReferenceDAO thirdreferenceDAO;
	protected ClientIpaddressDAO clientipaddressDAO;
	protected ForexHistoryDAO forexhistoryDAO;
	protected TransactionForexDAO transactionforexDAO;
	protected TransactionUpdateStatusDAO transactionupdateforexDAO;
	protected CountryMsisdnDAO countrymsisdnDAO;

	// forex exchanges cache//
	protected UsdForexDAO usdforexDAO;
	protected GbpForexDAO gbpforexDAO;
	protected ForexEngineDAO forexengineDAO;

	// addition
	// protected CountryDAO2 countryDAO2;

	protected BankCodesDAO bankCodesDAO;
	protected BillpaymentCodesDAO billpayamnetCodesDAO;

	// switching and network
	protected PrefixDAO prefixDAO;
	protected NetworkDAO networkDAO;
	protected RouteDAO routeDAO;

	// checker forex.
	private CacheManager cacheManager;

	// Collection
	private CollectionNetworkDAOImpl collectionnetworkDAO;
	private CollectionNetworkSubaccountDAOImpl collectionetworksubaccountDAO;
	private CollectionDefineDAOImpl collectiondefineDAO;
	private CollectionTypeDAOimpl collectionchannelDAO;

	private Logger logger;

	private SizeOfPolicyConfiguration sizeOfPolicyConfiguration;

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		accountDAO = AccountDAO.getInstance();
		manageaccountDAO = ManageAccountDAO.getInstance();
		countryDAO = CountryDAO.getInstance();
		clientipaddressDAO = ClientIpaddressDAO.getInstance();
		transactionStatusDAO = TransactionStatusDAO.getInstance();
		transactionDAO = TransactionDAO.getInstance();
		thirdreferenceDAO = ThirdReferenceDAO.getInstance();
		transactionforexDAO = TransactionForexDAO.getinstance();
		countrymsisdnDAO = CountryMsisdnDAO.getInstance();
		// forex part
		forexhistoryDAO = ForexHistoryDAO.getinstance();
		usdforexDAO = UsdForexDAO.getinstance();
		gbpforexDAO = GbpForexDAO.getinstance();
		transactionupdateforexDAO = TransactionUpdateStatusDAO.getinstance();

		forexengineDAO = ForexEngineDAO.getinstance();
		// addition
		// countryDAO2 = CountryDAO2.getInstance();
		bankCodesDAO = BankCodesDAO.getInstance();
		billpayamnetCodesDAO = BillpaymentCodesDAO.getInstance();
		// switchingprefix and network
		prefixDAO = PrefixDAO.getInstance();
		networkDAO = NetworkDAO.getInstance();
		routeDAO = RouteDAO.getInstance();

		// collection Cache
		collectionnetworkDAO = CollectionNetworkDAOImpl.getInstance();
		collectionetworksubaccountDAO = CollectionNetworkSubaccountDAOImpl.getInstance();
		collectiondefineDAO = CollectionDefineDAOImpl.getInstance();
		collectionchannelDAO = CollectionTypeDAOimpl.getInstance();

		sizeOfPolicyConfiguration = new SizeOfPolicyConfiguration();
		sizeOfPolicyConfiguration.setMaxDepth(20000);// controlls the size of the Cache default is usullay set @1000
		sizeOfPolicyConfiguration.setMaxDepthExceededBehavior("abort");

		logger = Logger.getLogger(this.getClass());

		logger.info("Starting to initialize cache");
		initCache();
		logger.info("Have finished initializing cache");
	}

	/**
	 *
	 */
	protected void initCache() {
		DiskStoreConfiguration diskConfig = new DiskStoreConfiguration();
		diskConfig.setPath(System.getProperty("java.io.tmpdir") + File.separator + "ehcache" + File.separator
				+ "Remittancestaging");

		Configuration config = (new Configuration()).diskStore(diskConfig);
		config.setMaxBytesLocalHeap(Long.parseLong(PropertiesConfig.getConfigValue("MAX_BYTES_LOCAL_HEAP")));
		config.setMaxBytesLocalDisk(Long.parseLong(PropertiesConfig.getConfigValue("MAX_BYTES_LOCAL_DISK")));
		config.setUpdateCheck(false);

		cacheManager = CacheManager.create(config);

		initAccountsCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);
		initManagementAccountCache(CacheVariables.CACHE_MANAGEMENTACCOUNTS_BY_USERNAME);

		List<? extends StorableBean> objectList;

		objectList = accountDAO.getAllAccounts();
		initCacheByUuid(CacheVariables.CACHE_ACCOUNTS_BY_UUID, objectList);

		objectList = manageaccountDAO.getAllAccounts();
		initCacheByUuid(CacheVariables.CACHE_MANAGEMENTACCOUNTS_BY_UUID, objectList);

		objectList = countryDAO.getAllCountries();
		initCacheByUuid(CacheVariables.CACHE_COUNTRY_BY_UUID, objectList);

		// banks
		objectList = bankCodesDAO.getAllBankCodes();
		initCacheByUuid(CacheVariables.CACHE_BANK_BY_UUID, objectList);

		// billpaymentcodes
		objectList = billpayamnetCodesDAO.getAllBillPaymentCodes();
		initCacheByUuid(CacheVariables.CACHE_BILLPAYMENTCODES_BY_UUID, objectList);

		objectList = transactionStatusDAO.getAllTransactionStatus();
		initCacheByUuid(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID, objectList);

		objectList = clientipaddressDAO.getAllClientIp();
		initCacheByUuid(CacheVariables.CACHE_IPADDRESS_BY_UUID, objectList);

		// forex
		objectList = usdforexDAO.getAllUsdForex();
		initCacheByUuid(CacheVariables.CACHE_USDFOREX_BY_UUID, objectList);

		objectList = gbpforexDAO.getAllGbpForex();
		initCacheByUuid(CacheVariables.CACHE_GBPFOREX_BY_UUID, objectList);

		objectList = forexengineDAO.getAllForex();
		initCacheByUuid(CacheVariables.CACHE_FOREX_BY_UUID, objectList);

		// addition
		objectList = prefixDAO.getAllPrefixSplit();
		initCacheByUuid(CacheVariables.CACHE_PREFIX_BY_UUID, objectList);

		objectList = networkDAO.getAllNetwork();
		initCacheByUuid(CacheVariables.CACHE_NETWORK_BY_UUID, objectList);

		objectList = routeDAO.getAllRoutes();
		initCacheByUuid(CacheVariables.CACHE_ROUTEDEFINED_BY_UUID, objectList);

		objectList = transactionupdateforexDAO.getAllTransactionUpdateStatus();
		initCacheByUuid(CacheVariables.CACHE_ALLTRANSACTIONUPDATE_BY_UUID, objectList);
		/**
		 * objectList = countryDAO2.getAllCountries();
		 * initCacheByUuid(CacheVariables.CACHE_COUNTRYALPHA3_BY_UUID, objectList);
		 **/
		// Collection
		objectList = collectionnetworkDAO.getAllNetwork();
		initCacheByUuid(CacheVariables.CACHE_COLLECTION_NETWORK, objectList);

		objectList = collectionetworksubaccountDAO.getAllNetworkSubaccount();
		initCacheByUuid(CacheVariables.CACHE_COLLECTION_NETWORK_SUBACCOUNT, objectList);

		objectList = collectiondefineDAO.getAllCollectionRoute();
		initCacheByUuid(CacheVariables.CACHE_COLLECTION_DEFINE, objectList);

		objectList = collectionchannelDAO.getAllCollectionType();
		initCacheByUuid(CacheVariables.CACHE_COLLECTION_CHANNEL, objectList);

		initAccountsCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);
		initManagementAccountCache(CacheVariables.CACHE_MANAGEMENTACCOUNTS_BY_USERNAME);

		initGenericCache(CacheVariables.CACHE_STATISTICS_BY_USERNAME);
		initGenericCache(CacheVariables.CACHE_ALL_ACCOUNTS_STATISTICS);
		initGenericCache(CacheVariables.CACHE_MANAGEMENTACCOUNTS_STATISTICS_BY_USERNAME);

		initGenericCache(CacheVariables.CACHE_FLOATPURCHASEPERCOUNTRY_BY_ACCOUNTUUID);
		initGenericCache(CacheVariables.CACHE_BALANCEPERCOUNTRY_BY_ACCOUNTUUID);

		initGenericCache(CacheVariables.CACHE_MASTERBALANCE);
		initGenericCache(CacheVariables.CACHE_MASTERPURCHASE);
	}

	/**
	 *
	 * @param cacheName
	 */
	private void initAccountsCache(String cacheName) {

		if (!cacheManager.cacheExists(cacheName)) {
			CacheConfiguration cacheConfig = new CacheConfiguration().sizeOfPolicy(sizeOfPolicyConfiguration);
			cacheConfig.setCopyOnRead(false); // Whether the Cache should copy
												// elements it returns
			cacheConfig.setCopyOnWrite(false); // Whether the Cache should copy
												// elements it gets
			cacheConfig.setEternal(true); // Sets whether elements are eternal.
			cacheConfig.setName(cacheName); // Sets the name of the cache.

			Cache accountsCache = new Cache(cacheConfig);
			cacheManager.addCacheIfAbsent(accountsCache);
			if (accountsCache.getStatus() == Status.STATUS_UNINITIALISED) {
				accountsCache.initialise();
			}

			List<Account> allAccounts = accountDAO.getAllAccounts();

			if (StringUtils.equals(cacheName, CacheVariables.CACHE_ACCOUNTS_BY_USERNAME)) {
				for (Account a : allAccounts) {
					accountsCache.put(new Element(a.getUsername(), a)); // Username
																		// as
																		// the
																		// key
				}

			} else if (StringUtils.equals(cacheName, CacheVariables.CACHE_ACCOUNTS_BY_UUID)) {
				for (Account a : allAccounts) {
					accountsCache.put(new Element(a.getUuid(), a)); // Uuid as
																	// the key
				}

			}
		}
	}

	private void initManagementAccountCache(String cacheName) {

		if (!cacheManager.cacheExists(cacheName)) {
			CacheConfiguration cacheConfig = new CacheConfiguration().sizeOfPolicy(sizeOfPolicyConfiguration);
			cacheConfig.setCopyOnRead(false); // Whether the Cache should copy
												// elements it returns
			cacheConfig.setCopyOnWrite(false); // Whether the Cache should copy
												// elements it gets
			cacheConfig.setEternal(true); // Sets whether elements are eternal.
			cacheConfig.setName(cacheName); // Sets the name of the cache.

			Cache managementccountsCache = new Cache(cacheConfig);
			cacheManager.addCacheIfAbsent(managementccountsCache);
			if (managementccountsCache.getStatus() == Status.STATUS_UNINITIALISED) {
				managementccountsCache.initialise();
			}

			List<ManagementAccount> allAccounts = manageaccountDAO.getAllAccounts();

			if (StringUtils.equals(cacheName, CacheVariables.CACHE_MANAGEMENTACCOUNTS_BY_USERNAME)) {
				for (ManagementAccount a : allAccounts) {
					managementccountsCache.put(new Element(a.getUsername(), a)); // Username
																					// as
																					// the
																					// key
				}

			} else if (StringUtils.equals(cacheName, CacheVariables.CACHE_MANAGEMENTACCOUNTS_BY_UUID)) {
				for (ManagementAccount a : allAccounts) {
					managementccountsCache.put(new Element(a.getUuid(), a)); // Uuid
																				// as
																				// the
																				// key
				}

			}
		}
	}

	/**
	 *
	 * @param cacheName
	 * @param objList
	 */
	private void initCacheByUuid(String cacheName, List<? extends StorableBean> objList) {
		if (!cacheManager.cacheExists(cacheName)) {
			CacheConfiguration cacheConfig = new CacheConfiguration().sizeOfPolicy(sizeOfPolicyConfiguration);
			cacheConfig.setCopyOnRead(false); // Whether the Cache should copy
												// elements it returns
			cacheConfig.setCopyOnWrite(false); // Whether the Cache should copy
												// elements it gets
			cacheConfig.setEternal(true); // Sets whether elements are eternal.
			cacheConfig.setName(cacheName); // Sets the name of the cache.

			Cache cache = new Cache(cacheConfig);
			cacheManager.addCacheIfAbsent(cache);
			if (cache.getStatus() == Status.STATUS_UNINITIALISED) {
				cache.initialise();
			}

			for (StorableBean b : objList) {
				cache.put(new Element(b.getUuid(), b)); // UUID as the key
			}
		}
	}

	/**
	 *
	 * @param cacheName
	 */
	private void initGenericCache(String cacheName) {
		if (!cacheManager.cacheExists(cacheName)) {
			CacheConfiguration cacheConfig = new CacheConfiguration().sizeOfPolicy(sizeOfPolicyConfiguration);
			cacheConfig.setCopyOnRead(false); // Whether the Cache should copy
												// elements it returns
			cacheConfig.setCopyOnWrite(false); // Whether the Cache should copy
												// elements it gets
			cacheConfig.setEternal(true); // Sets whether elements are eternal.
			cacheConfig.setName(cacheName); // Sets the name of the cache.

			Cache cache = new Cache(cacheConfig);
			cacheManager.addCacheIfAbsent(cache);
			if (cache.getStatus() == Status.STATUS_UNINITIALISED) {
				cache.initialise();
			}
		}
	}

	/**
	 * @see javax.servlet.GenericServlet#destroy()
	 */
	@Override
	public void destroy() {
		super.destroy();

		CacheManager.getInstance().shutdown();
	}
}

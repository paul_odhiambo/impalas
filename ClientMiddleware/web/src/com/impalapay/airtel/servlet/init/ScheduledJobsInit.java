package com.impalapay.airtel.servlet.init;

import com.impalapay.airtel.scheduledjobs.sessionid.SessionMgmtLauncher;
import com.impalapay.airtel.scheduledjobs.temporary.AutomaticPollLauncher;
import com.impalapay.collection.scheduledjobs.performsettlement.collectionAutoNotifyPollLauncher;
import com.impalapay.collection.scheduledjobs.performsettlement.collectionAutoSettlePollLauncher;
import com.impalapay.collection.scheduledjobs.performsettlement.collectionAutomaticPollLauncher;
import com.impalapay.mno.servlet.api.bridge.centricgateway.CentricPollLauncher;
import com.impalapay.mno.servlet.api.bridge.mpesa.MpesaPollLauncher;
import com.impalapay.mno.servlet.api.bridge.ugandamart.UgmartPollLauncher;
import com.impalapay.mno.servlet.api.bridge.zeepay.ZeepayPollLauncher;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;

/**
 * Description of class.
 * <p>
 * Copyright (c) ImpalaPay LTD., June 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */

public class ScheduledJobsInit extends HttpServlet {

	private Logger logger;

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		logger = Logger.getLogger(this.getClass());

		logger.info("Starting to initialize scheduled jobs");

		// new SessionMgmtLauncher().start();

		// new AutomaticPollLauncher().start();

		// Collection launchers

		// new collectionAutomaticPollLauncher().start();

		// new collectionAutoSettlePollLauncher().start();

		// new collectionAutoNotifyPollLauncher().start();

		// new UgmartPollLauncher().start();

		// new MpesaPollLauncher().start();

		// new ZeepayPollLauncher().start();

		// new CentricPollLauncher().start();

		logger.info("Have finished initializing scheduled jobs");
	}

}

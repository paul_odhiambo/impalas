package com.impalapay.airtel.servlet.report.chart.pie;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jfree.data.general.DefaultPieDataset;

import com.impalapay.airtel.accountmgmt.session.SessionStatistics;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.cache.CacheVariables;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class javavava {

	public static void main(String[] args) {

		CacheManager mgr = CacheManager.getInstance();
		Cache statisticsCache = mgr.getCache(CacheVariables.CACHE_STATISTICS_BY_USERNAME);

		HashMap<String, Double> countHash = new HashMap<>();
		Iterator<String> keyIter;
		String key;
		Element element;
		SessionStatistics statistics = null;

		String username = "demo";

		if ((element = statisticsCache.get(username)) != null) {
			statistics = (SessionStatistics) element.getObjectValue();
		}

		Map<Country, Double> countryTransactionAmount = statistics.getCountryTransactionAmountSuccess();
		Iterator<Country> transactionIter = countryTransactionAmount.keySet().iterator();
		Country country;

		while (transactionIter.hasNext()) {
			country = transactionIter.next();
			countHash.put(country.getName(), countryTransactionAmount.get(country));
		}

		keyIter = countHash.keySet().iterator();

		// create a dataset...
		DefaultPieDataset dataset = new DefaultPieDataset();

		while (keyIter.hasNext()) {
			key = keyIter.next();
			dataset.setValue(key, countHash.get(key).intValue());
		}

		System.out.println(countHash);
	}

}

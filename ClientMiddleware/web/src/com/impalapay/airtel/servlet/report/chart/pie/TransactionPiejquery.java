package com.impalapay.airtel.servlet.report.chart.pie;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.impalapay.airtel.accountmgmt.session.SessionStatistics;
import com.impalapay.airtel.cache.CacheVariables;

import java.io.IOException;
import java.io.OutputStream;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.impalapay.airtel.beans.geolocation.Country;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

/**
 * Generates a Pie chart of
 * {@link ke.co.shujaa.airtimegw.server.beans.topup.Topup} that are ordered by
 * Network.
 * <p>
 * It is assumed that this class is only called when a user is logged in.
 * <p>
 * Copyright (c) Shujaa Solutions Ltd., Sep 24, 2013
 *
 * @author <a href="mailto:anthonym@shujaa.co.ke">Anthony Wafula</a>
 * @version %I%, %G%
 *
 */
public class TransactionPiejquery extends HttpServlet {

	final String CHART_TITLE = "Transaction percentage per Country";
	// final int CHART_WIDTH = 700;
	// final int CHART_HEIGHT = 700;
	final int CHART_WIDTH = 550;
	final int CHART_HEIGHT = 400;

	private Cache statisticsCache;

	private String username = "";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		statisticsCache = mgr.getCache(CacheVariables.CACHE_STATISTICS_BY_USERNAME);
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		username = request.getParameter("username");

		// response.setContentType("image/png");
		response.setContentType("text/plain;charset=UTF-8");
		// ChartUtilities.writeChartAsPNG(out, getChart(), CHART_WIDTH,
		// CHART_HEIGHT);
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(check(username).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * Creates a chart for incoming SMS information against all {@link Network}s
	 *
	 * @return chart
	 */
	private String check(String username) {
		Gson g = new GsonBuilder().disableHtmlEscaping().create();

		HashMap<String, Double> countHash = new HashMap<String, Double>();

		Element element;
		SessionStatistics statistics = null;

		if ((element = statisticsCache.get(username)) != null) {
			statistics = (SessionStatistics) element.getObjectValue();
		}

		Map<Country, Double> countryTransactionAmount = statistics.getCountryTransactionAmountSuccess();
		Iterator<Country> transactionIter = countryTransactionAmount.keySet().iterator();
		Country country;

		while (transactionIter.hasNext()) {
			country = transactionIter.next();
			countHash.put(country.getName(), countryTransactionAmount.get(country));
		}

		String jsonResult = g.toJson(countHash);

		return jsonResult;
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

}

/*
 ** Local Variables: mode: java c-basic-offset: 2 tab-width: 2 indent-tabs-mode:
 * nil End:
 **
 ** ex: set softtabstop=2 tabstop=2 expandtab:
 **
 */
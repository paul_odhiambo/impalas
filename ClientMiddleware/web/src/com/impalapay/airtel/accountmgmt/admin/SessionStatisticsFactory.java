package com.impalapay.airtel.accountmgmt.admin;

import com.impalapay.airtel.accountmgmt.session.*;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.transaction.TransactionStatusDAO;
import com.impalapay.beans.network.Network;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import java.util.ArrayList;
import java.util.List;

import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;

/**
 * Factory class to create transaction statistics from accounts. These will be
 * used during the sessions while a user is logged in.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Dec 20, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class SessionStatisticsFactory {
	private static TransactionStatusDAO transactionStatusDAO;
	private static CountUtils countUtils;
	private static List<Network> networkList;
	private static List<Country> countryList;
	private static List<Account> accountList;

	static {
		transactionStatusDAO = TransactionStatusDAO.getInstance();

		countUtils = CountUtils.getInstance();
		CacheManager mgr = CacheManager.getInstance();
		Cache networkCache = mgr.getCache(CacheVariables.CACHE_NETWORK_BY_UUID);
		Cache countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
		Cache accountCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_UUID);

		Element element;
		List keys;

		networkList = new ArrayList<>();
		Network network;
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkList.add(network);
		}

		countryList = new ArrayList<>();
		Country country;
		keys = countryCache.getKeys();
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryList.add(country);
		}

		accountList = new ArrayList<>();
		Account account;
		keys = accountCache.getKeys();
		for (Object key : keys) {
			element = accountCache.get(key);
			account = (Account) element.getObjectValue();
			accountList.add(account);
		}
	}

	/**
	 * Gets statistics relating to
	 * 
	 * all accounts
	 *
	 * @return SessionStatistics
	 */
	public static SessionStatistics getSessionStatistics() {
		SessionStatistics stats = new SessionStatistics();
		int count;
		double amount;
		TransactionStatus successTransactionStatus;

		stats.setTransactionCountTotal(countUtils.getAllTransactionCount());

		stats.setForexhistoryCountTotal(countUtils.getAllForexHistoryCount());

		stats.setTransactionforexCountTotal(countUtils.getAllTransactionForexrateCount());

		stats.setAlltopupCountTotal(countUtils.getAllTopupCount());

		stats.setAccountCountTotal(countUtils.getAllAccountCount());

		stats.setManagementAccountCountTotal(countUtils.getAllManageAccountCount());

		stats.setMnoprefixCountTotal(countUtils.getAllMnoPrefixCount());

		stats.setNetworkcountTotal(countUtils.getAllNetworkCount());

		stats.setRoutedefineCountTotal(countUtils.getAllRouteDefineCount());

		stats.setMainfloatCountTotal(countUtils.getAllMasterBalanceCount());

		stats.setMainfloatbycountryCountTotal(countUtils.getAllBalanceByCountryCount());

		stats.setClientipCountTotal(countUtils.getAllIpCount());

		stats.setClientsessionUrlCountTotal(countUtils.getAllClientUrlCount());

		stats.setCountryCountTotal(countUtils.getAllCountryCount());

		stats.setCountrymsisdnCountTotal(countUtils.getAllCountryMsisdnCount());

		stats.setMainfloatbycountryhistoryCountTotal(countUtils.getAllBalanceByCountryHistoryCount());

		stats.setMainfloathistoryCountTotal(countUtils.getAllMasterBalanceHistoryCount());

		stats.setBankCountTotal(countUtils.getAllBankCodeCount());

		stats.setMerchantCountTotal(countUtils.getAllBillPaymentCodeCount());

		stats.setSystemlogcountTotal(countUtils.getAllSystemLogCount());

		stats.setNetworkbalanceCountTotal(countUtils.getAllBalanceByNetworkCount());

		stats.setNetworkfloathistoryCountTotal(countUtils.getAllBalanceByNetworkHistoryCount());

		stats.setCheckermainfloatCountTotal(countUtils.getAllCheckerMasterBalanceCount());

		stats.setCheckerforexrateCountTotal(countUtils.getAllCheckerForexCount());

		stats.setForexratehistoryCountTotal(countUtils.getAllForexrateHistoryCount());

		stats.setClientmainbalancehistoryCountTotal(countUtils.getAllClientMainBalanceHistoryCount());

		stats.setCheckerbalancebycountryCountTotal(countUtils.getAllCheckerBalanceByCountryCount());

		stats.setCommissionCountTotal(countUtils.getAllCommissionCount());

		stats.setUpdatetransactionCountTotal(countUtils.getAllTransactionUpdateStatus());

		stats.setUpdatetransactionHistoryCountTotal(countUtils.getAllTransactionUpdateStatusHistory());

		// collectioncount
		stats.setCollectiondefineCountTotal(countUtils.getAllCollectionDefineCount());
		stats.setCollectionnetworkcountTotal(countUtils.getAllCollectionNetworkCount());
		stats.setCollectionsubaccountCountTotal(countUtils.getAllCollectionSubAccntCount());
		stats.setCollectionprocessedtransCountTotal(countUtils.getAllProcessedCollection());

		// withdrawal
		stats.setCollectionbalanceHistoryCountTotal(countUtils.getAllCollectionBalanceHistory());
		stats.setCollectionbalanceCountTotal(countUtils.getAllCollectionBalance());
		
		stats.setClientwithdrawalCountTotal(countUtils.getAllClientWithdrawal());
		stats.setCheckerwithdrawalCountTotal(countUtils.getAllCheckerWithdrawal());
		stats.setWithdrawalhistoryCountTotal(countUtils.getAllWithdrawalHistory());
		stats.setWithdrawalonHoldCountTotal(countUtils.getAllWithdrwalBalanceHold());
		stats.setWithdrawalonHoldHistCountTotal(countUtils.getAllWithdrwalBalanceHoldHistory());
		// Set up data for the pie charts
		for (Network network : networkList) {
			count = countUtils.getTransactionCount(network);
			if (count > 0) {
				stats.setNetworkTransactionCountTotal(network, count);
			}
		}

		// set data for pie charts accounts
		for (Account account : accountList) {
			count = countUtils.getTransactionCount(account);
			if (count > 0) {
				stats.setAccountTransactionCountTotal(account, count);
			}
		}

		// Set total value of transaction requests per country
		// that have been successful
		successTransactionStatus = transactionStatusDAO.getTransactionStatus(TransactionStatus.TRANSACTION_SUCCESS);
		// stats.setTopupCountTotal(countUtils.getTopupCount(account,
		// successTopupStatus));

		for (Country country : countryList) {
			count = countUtils.getTransactionCount(country, successTransactionStatus);
			amount = countUtils.getTransactionAmount(country, successTransactionStatus);
			if (count > 0) {
				stats.addCountryTransactionCountSuccess(country, count);

			}

			if (amount > 0) {
				stats.addCountryTransactionAmountSuccess(country, amount);

			}

		}

		/**
		 * TopupStatus acceptedTopupStatus,successTopupStatus; List <Network>
		 * networkList; int count,amount;
		 * 
		 * 
		 * //set total number of top up attempts or airtime requests acceptedTopupStatus
		 * = topupStatusDAO.getTopupStatus(TopupStatus.ACCEPTED_FOR_DELIVERY);
		 * stats.setTopupCountTotal(countUtils.getAllTopupCount( acceptedTopupStatus));
		 * 
		 * 
		 * networkList = networkDAO.getAllNetworks();
		 * 
		 * //set total number of top up attempts or airtime requests per network
		 * operator for (Network network : networkList) { count =
		 * countUtils.getAllTopupCount(network,acceptedTopupStatus); if (count > 0) {
		 * stats.addNetworkTopupCountTotal(network, count); }
		 * 
		 * }
		 * 
		 * //Set total value of top up attempts or airtime requests per network operator
		 * //that have been successful successTopupStatus =
		 * topupStatusDAO.getTopupStatus(TopupStatus.TOPUP_SUCCESS);
		 * //stats.setTopupCountTotal(countUtils.getAllTopupCount( successTopupStatus));
		 * 
		 * for (Network network : networkList) { count =
		 * countUtils.getAllTopupCount(network,successTopupStatus);
		 * amount=countUtils.getAllTopupAmount( network, successTopupStatus); if (count
		 * > 0) { stats.addNetworkTopupCountSuccess(network, count);
		 * 
		 * }
		 * 
		 * if (amount > 0) { stats.addNetworkTopupAmountSuccess(network, amount);
		 * 
		 * } } // Set up data for the bar charts DateMidnight dateMidnightStart =
		 * DateMidnight.now().minus(Hours.hours(24 * (TopupBarDay.DAY_COUNT - 1)));
		 * DateMidnight dateMidnightEnd = dateMidnightStart.plus(Hours.hours(24)); int
		 * numDays = 0; do { for (Network network : networkList) {
		 * 
		 * count = countUtils.getAllTopupAmount(network, acceptedTopupStatus, new
		 * Date(dateMidnightStart.getMillis()), new Date(dateMidnightEnd.getMillis()));
		 * 
		 * if (count > 0) { stats.addNetworkTopupAmountDay(new SimpleDateFormat( "MMM
		 * d").format(new Date(dateMidnightStart.getMillis())), network, count); } }
		 * 
		 * dateMidnightStart = dateMidnightStart.plus(Hours.hours(24)); dateMidnightEnd
		 * = dateMidnightEnd.plus(Hours.hours(24)); numDays++; } while (numDays <
		 * TopupBarDay.DAY_COUNT);
		 **/

		return stats;
	}

}

/*
 * * Local Variables: mode: java c-basic-offset: 2 tab-width: 2
 * indent-tabs-mode: nil End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */
package com.impalapay.airtel.accountmgmt.admin.pagination.balance;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.impalapay.airtel.beans.topup.Topup;

public class TestTopupPaginator {

	final String DB_NAME = "airteldblive";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "airtellive";
	final String DB_PASSWD = "ThejKoyb34";
	final int DB_PORT = 5432;

	/**
	 * Test method for getting firstpage
	 */
	@Ignore
	@Test
	public void testGetFirstPage() {
		TopupPaginator topupPaginator = new TopupPaginator(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		TopupPage firstPage = topupPaginator.getFirstPage();
		List<Topup> topupList = firstPage.getContents();
		assertEquals(topupList.size(), TopupPaginator.PAGESIZE);

		for (Topup s : topupList) {
			System.out.println(s);
		}
	}

	/**
	 * Test method for getting lastpage
	 */
	@Ignore
	@Test
	public void testGetLastPage() {
		TopupPaginator topupPaginator = new TopupPaginator(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		TopupPage firstPage = topupPaginator.getLastPage();
		List<Topup> topupList = firstPage.getContents();

		for (Topup s : topupList) {
			System.out.println(s);
		}
	}

	@Ignore
	@Test
	public void testGetNextPage() {
		TopupPaginator paginator = new TopupPaginator(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);
		int currentPageNum = 1;

		TopupPage page = new TopupPage(currentPageNum, 1, TopupPaginator.PAGESIZE, new ArrayList<Topup>());
		TopupPage nextPage = paginator.getNextPage(page);
		List<Topup> topupList = nextPage.getContents();

		assertEquals(topupList.size(), TopupPaginator.PAGESIZE);

		for (Topup s : topupList) {
			System.out.println(s);
		}
	}

}

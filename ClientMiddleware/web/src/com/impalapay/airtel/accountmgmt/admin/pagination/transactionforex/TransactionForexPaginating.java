package com.impalapay.airtel.accountmgmt.admin.pagination.transactionforex;

/**
 * Description of how to break down a {@link java.util.List} of
 * {@link com.impalapay.airtel.beans.transaction.Forexrate} into
 * {@link com.impalapay.airtel.accountmgmt.admin.pagination.ForexPage}
 * <p>
 * Copyright (c) ImpalaPay LTD., June 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public interface TransactionForexPaginating {

	/**
	 *
	 * @return TransactionForexPage
	 */
	public TransactionForexPage getFirstPage();

	/**
	 *
	 * @return TransactionForexPage
	 */
	public TransactionForexPage getLastPage();

	/**
	 *
	 * @param currentPage
	 * @return TransactionForexPage
	 */
	public TransactionForexPage getNextPage(TransactionForexPage currentPage);

	/**
	 *
	 * @param currentPage
	 * @return TransactionForexPage
	 */
	public TransactionForexPage getPrevPage(TransactionForexPage currentPage);
}

/*
 ** Local Variables: mode: java c-basic-offset: 2 tab-width: 2 indent-tabs-mode:
 * nil End:
 **
 ** ex: set softtabstop=2 tabstop=2 expandtab:
 **
 */
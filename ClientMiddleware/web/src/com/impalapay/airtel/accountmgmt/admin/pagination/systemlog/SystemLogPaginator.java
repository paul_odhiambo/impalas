/**
 * Copyright 2015 Tawi Commercial Services Ltd
 * 
 * Licensed under the Open Software License, Version 3.0  you may
 * not use this file except in compliance with the License. You may obtain a copy
 * of the License at:
 * http://opensource.org/licenses/OSL-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an AS IS BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 * 
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package com.impalapay.airtel.accountmgmt.admin.pagination.systemlog;

import java.util.List;
import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.airtel.persistence.systemlog.SystemLogDAO;

/**
 * Paginate an Inbox HTML view.
 * <p>
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 */
public class SystemLogPaginator {

	/**
	 *
	 */

	public static final int PAGESIZE = 15;
	private final SystemLogDAO systemlogDAO;
	private final CountUtils countUtils;

	/**
	 *
	 * @param accountuuid
	 */
	public SystemLogPaginator() {

		countUtils = CountUtils.getInstance();
		systemlogDAO = SystemLogDAO.getInstance();

	}

	/**
	 *
	 * @param email
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public SystemLogPaginator(String email, String dbName, String dbHost, String dbUsername, String dbPasswd,
			int dbPort) {

		// initialize the DAOs
		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);
		systemlogDAO = new SystemLogDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);
	}

	/**
	 *
	 * @return
	 */
	public SystemLogPage getFirstPage() {

		SystemLogPage page = new SystemLogPage();

		List<SystemLog> systemlogList = systemlogDAO.getAllSystemLog(0, PAGESIZE);

		page = new SystemLogPage(1, getTotalPage(), PAGESIZE, systemlogList);
		// result = new IncomingSMSPage (1, getTotalPage(), PAGESIZE, smsList);

		return page;
	}

	/**
	 * Provides the last page of the ip list report
	 *
	 * @return ip page
	 */
	public SystemLogPage getLastPage() {
		SystemLogPage page = new SystemLogPage();

		List<SystemLog> addressList = null;
		int addressCount, startIndex;
		int totalPage = getTotalPage();

		startIndex = (totalPage - 1) * PAGESIZE;
		addressCount = countUtils.getAllSystemLogCount();

		addressList = systemlogDAO.getAllSystemLog(startIndex, addressCount);

		page = new SystemLogPage(totalPage, totalPage, PAGESIZE, addressList);

		return page;
	}

	/**
	 * Moves you forward to the page of the Accounts that comes after the current
	 * page
	 *
	 * @param currentPage
	 * @return an ip page
	 */
	public SystemLogPage getNextPage(final SystemLogPage currentPage) {
		int totalPage = getTotalPage();

		SystemLogPage page = new SystemLogPage();

		List<SystemLog> addressList = systemlogDAO.getAllSystemLog(currentPage.getPageNum() * PAGESIZE,
				((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		page = new SystemLogPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, addressList);

		return page;
	}

	/**
	 * Moves you backward to the page of the Accounts that comes before the current
	 * page
	 *
	 * @param currentPage
	 * @return an ip page
	 */
	public SystemLogPage getPrevPage(final SystemLogPage currentPage) {
		int totalPage = getTotalPage();

		SystemLogPage page = new SystemLogPage();

		List<SystemLog> addressList = systemlogDAO.getAllSystemLog((currentPage.getPageNum() - 2) * PAGESIZE,
				((currentPage.getPageNum() - 1) * PAGESIZE));

		page = new SystemLogPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, addressList);

		return page;
	}

	/**
	 * Calculates the total number of pages that would be printed for the Account
	 * sessions that belong to the logged-in account
	 *
	 * @return an integer
	 */
	public int getTotalPage() {
		int totalSize = 0;

		// get the number of all sessions belonging to this email
		totalSize = countUtils.getAllSystemLogCount();

		// TODO: divide by the page size and add one to take care of remainders and what
		// else?
		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

package com.impalapay.airtel.accountmgmt.admin.pagination.routedefine;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import com.impalapay.beans.route.RouteDefine;

public class TestRouteDefinePaginator {

	final String DB_NAME = "airteldblive";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "airtellive";
	final String DB_PASSWD = "ThejKoyb34";
	final int DB_PORT = 5432;

	/**
	 * Test method for getting firstpage
	 */
	@Ignore
	@Test
	public void testGetFirstPage() {
		RouteDefinePaginator topupPaginator = new RouteDefinePaginator(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD,
				DB_PORT);

		RouteDefinePage firstPage = topupPaginator.getFirstPage();
		List<RouteDefine> topupList = firstPage.getContents();
		assertEquals(topupList.size(), RouteDefinePaginator.PAGESIZE);

		for (RouteDefine s : topupList) {
			System.out.println(s);
		}
	}

	/**
	 * Test method for getting lastpage
	 */
	@Ignore
	@Test
	public void testGetLastPage() {
		RouteDefinePaginator topupPaginator = new RouteDefinePaginator(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD,
				DB_PORT);

		RouteDefinePage firstPage = topupPaginator.getLastPage();
		List<RouteDefine> topupList = firstPage.getContents();

		for (RouteDefine s : topupList) {
			System.out.println(s);
		}
	}

	// @Ignore
	@Test
	public void testGetNextPage() {
		RouteDefinePaginator paginator = new RouteDefinePaginator(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);
		int currentPageNum = 1;

		RouteDefinePage page = new RouteDefinePage(currentPageNum, 1, RouteDefinePaginator.PAGESIZE,
				new ArrayList<RouteDefine>());
		RouteDefinePage nextPage = paginator.getNextPage(page);
		List<RouteDefine> topupList = nextPage.getContents();

		assertEquals(topupList.size(), RouteDefinePaginator.PAGESIZE);

		for (RouteDefine s : topupList) {
			System.out.println(s);
		}
	}

}

package com.impalapay.airtel.accountmgmt.admin.pagination.checkerforex;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.impalapay.airtel.beans.forex.ForexEngineHistory;

public class TestCurrencyPaginator {

	final String DB_NAME = "remittancedb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "remittance";
	final String DB_PASSWD = "gertedNen2";
	final int DB_PORT = 5432;

	/**
	 * Test method for getting firstpage
	 */
	// @Ignore
	@Test
	public void testGetFirstPage() {
		CurrencyPaginator topupPaginator = new CurrencyPaginator(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		CurrencyPage firstPage = topupPaginator.getFirstPage();
		List<ForexEngineHistory> topupList = firstPage.getContents();
		// assertEquals(topupList.size(), CurrencyPaginator.PAGESIZE);

		for (ForexEngineHistory s : topupList) {
			System.out.println(s);
		}
	}

}

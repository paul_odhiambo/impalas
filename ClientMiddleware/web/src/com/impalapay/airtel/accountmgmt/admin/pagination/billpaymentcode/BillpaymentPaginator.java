/**
 * Copyright 2015 Tawi Commercial Services Ltd
 * 
 * Licensed under the Open Software License, Version 3.0  you may
 * not use this file except in compliance with the License. You may obtain a copy
 * of the License at:
 * http://opensource.org/licenses/OSL-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an AS IS BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 * 
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package com.impalapay.airtel.accountmgmt.admin.pagination.billpaymentcode;

import java.util.List;
import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.airtel.beans.billpayment.BillPaymentCodes;
import com.impalapay.airtel.persistence.billpayment.BillpaymentCodesDAO;

/**
 * Paginate an Inbox HTML view.
 * <p>
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 */
public class BillpaymentPaginator {

	/**
	 *
	 */

	public static final int PAGESIZE = 13;
	private final BillpaymentCodesDAO billpaymentcodeDAO;
	private final CountUtils countUtils;

	/**
	 *
	 * @param accountuuid
	 */
	public BillpaymentPaginator() {

		countUtils = CountUtils.getInstance();
		billpaymentcodeDAO = BillpaymentCodesDAO.getInstance();

	}

	/**
	 *
	 * @param email
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public BillpaymentPaginator(String email, String dbName, String dbHost, String dbUsername, String dbPasswd,
			int dbPort) {

		// initialize the DAOs
		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);
		billpaymentcodeDAO = new BillpaymentCodesDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);
	}

	/**
	 *
	 * @return
	 */
	public BillpaymentPage getFirstPage() {

		BillpaymentPage page = new BillpaymentPage();

		List<BillPaymentCodes> addressList = billpaymentcodeDAO.getAllBillPaymentCodes(0, PAGESIZE);

		page = new BillpaymentPage(1, getTotalPage(), PAGESIZE, addressList);
		// result = new IncomingSMSPage (1, getTotalPage(), PAGESIZE, smsList);

		return page;
	}

	/**
	 * Provides the last page of the ip list report
	 *
	 * @return ip page
	 */
	public BillpaymentPage getLastPage() {
		BillpaymentPage page = new BillpaymentPage();

		List<BillPaymentCodes> addressList = null;
		int addressCount, startIndex;
		int totalPage = getTotalPage();

		startIndex = (totalPage - 1) * PAGESIZE;
		addressCount = countUtils.getAllBillPaymentCodeCount();

		addressList = billpaymentcodeDAO.getAllBillPaymentCodes(startIndex, addressCount);

		page = new BillpaymentPage(totalPage, totalPage, PAGESIZE, addressList);

		return page;
	}

	/**
	 * Moves you forward to the page of the Accounts that comes after the current
	 * page
	 *
	 * @param currentPage
	 * @return an ip page
	 */
	public BillpaymentPage getNextPage(final BillpaymentPage currentPage) {
		int totalPage = getTotalPage();

		BillpaymentPage page = new BillpaymentPage();

		List<BillPaymentCodes> addressList = billpaymentcodeDAO.getAllBillPaymentCodes(
				currentPage.getPageNum() * PAGESIZE, ((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		page = new BillpaymentPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, addressList);

		return page;
	}

	/**
	 * Moves you backward to the page of the Accounts that comes before the current
	 * page
	 *
	 * @param currentPage
	 * @return an ip page
	 */
	public BillpaymentPage getPrevPage(final BillpaymentPage currentPage) {
		int totalPage = getTotalPage();

		BillpaymentPage page = new BillpaymentPage();

		List<BillPaymentCodes> addressList = billpaymentcodeDAO.getAllBillPaymentCodes(
				(currentPage.getPageNum() - 2) * PAGESIZE, ((currentPage.getPageNum() - 1) * PAGESIZE));

		page = new BillpaymentPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, addressList);

		return page;
	}

	/**
	 * Calculates the total number of pages that would be printed for the Account
	 * sessions that belong to the logged-in account
	 *
	 * @return an integer
	 */
	public int getTotalPage() {
		int totalSize = 0;

		// get the number of all sessions belonging to this email
		totalSize = countUtils.getAllBillPaymentCodeCount();

		// TODO: divide by the page size and add one to take care of remainders
		// and what else?
		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

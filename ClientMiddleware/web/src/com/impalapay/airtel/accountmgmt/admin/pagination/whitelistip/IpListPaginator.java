/**
 * Copyright 2015 Tawi Commercial Services Ltd
 * 
 * Licensed under the Open Software License, Version 3.0  you may
 * not use this file except in compliance with the License. You may obtain a copy
 * of the License at:
 * http://opensource.org/licenses/OSL-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an AS IS BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 * 
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package com.impalapay.airtel.accountmgmt.admin.pagination.whitelistip;

import java.util.List;

import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.airtel.beans.clientipaddress.ClientIP;
import com.impalapay.airtel.persistence.clientipaddress.ClientIpaddressDAO;

/**
 * Paginate an Inbox HTML view.
 * <p>
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 */
public class IpListPaginator {

	/**
	 *
	 */

	public static final int PAGESIZE = 13;
	private final ClientIpaddressDAO addressDAO;
	private final CountUtils countUtils;

	/**
	 *
	 * @param accountuuid
	 */
	public IpListPaginator() {

		countUtils = CountUtils.getInstance();
		addressDAO = ClientIpaddressDAO.getInstance();

	}

	/**
	 *
	 * @param email
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public IpListPaginator(String email, String dbName, String dbHost, String dbUsername, String dbPasswd, int dbPort) {

		// initialize the DAOs
		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);
		addressDAO = new ClientIpaddressDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);
	}

	/**
	 *
	 * @return
	 */
	public IpListPage getFirstPage() {

		IpListPage page = new IpListPage();

		List<ClientIP> addressList = addressDAO.getAllIp(0, PAGESIZE);

		page = new IpListPage(1, getTotalPage(), PAGESIZE, addressList);
		// result = new IncomingSMSPage (1, getTotalPage(), PAGESIZE, smsList);

		return page;
	}

	/**
	 * Provides the last page of the ip list report
	 *
	 * @return ip page
	 */
	public IpListPage getLastPage() {
		IpListPage page = new IpListPage();

		List<ClientIP> addressList = null;
		int addressCount, startIndex;
		int totalPage = getTotalPage();

		startIndex = (totalPage - 1) * PAGESIZE;
		addressCount = countUtils.getAllIpCount();

		addressList = addressDAO.getAllIp(startIndex, addressCount);

		page = new IpListPage(totalPage, totalPage, PAGESIZE, addressList);

		return page;
	}

	/**
	 * Moves you forward to the page of the Accounts that comes after the current
	 * page
	 *
	 * @param currentPage
	 * @return an ip page
	 */
	public IpListPage getNextPage(final IpListPage currentPage) {
		int totalPage = getTotalPage();

		IpListPage page = new IpListPage();

		List<ClientIP> addressList = addressDAO.getAllIp(currentPage.getPageNum() * PAGESIZE,
				((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		page = new IpListPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, addressList);

		return page;
	}

	/**
	 * Moves you backward to the page of the Accounts that comes before the current
	 * page
	 *
	 * @param currentPage
	 * @return an ip page
	 */
	public IpListPage getPrevPage(final IpListPage currentPage) {
		int totalPage = getTotalPage();

		IpListPage page = new IpListPage();

		List<ClientIP> addressList = addressDAO.getAllIp((currentPage.getPageNum() - 2) * PAGESIZE,
				((currentPage.getPageNum() - 1) * PAGESIZE));

		page = new IpListPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, addressList);

		return page;
	}

	/**
	 * Calculates the total number of pages that would be printed for the Account
	 * sessions that belong to the logged-in account
	 *
	 * @return an integer
	 */
	public int getTotalPage() {
		int totalSize = 0;

		// get the number of all sessions belonging to this email
		totalSize = countUtils.getAllIpCount();

		// TODO: divide by the page size and add one to take care of remainders and what
		// else?
		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

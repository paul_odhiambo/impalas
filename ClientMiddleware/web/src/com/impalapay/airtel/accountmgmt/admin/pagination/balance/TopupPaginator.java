/**
 * Copyright 2015 Tawi Commercial Services Ltd
 * 
 * Licensed under the Open Software License, Version 3.0  you may
 * not use this file except in compliance with the License. You may obtain a copy
 * of the License at:
 * http://opensource.org/licenses/OSL-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an AS IS BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 * 
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package com.impalapay.airtel.accountmgmt.admin.pagination.balance;

import java.util.List;

import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.airtel.beans.topup.Topup;
import com.impalapay.airtel.persistence.topup.TopupDAO;

/**
 * Pagination of Sent HTML view.
 * <p>
 * 
 * @author <a href="mailto:michael@tawi.mobi">Michael Wakahe</a>
 */
public class TopupPaginator {

	public static final int PAGESIZE = 15; // The number of Transactions to display per page
	private CountUtils countUtils;
	private TopupDAO topupDAO;

	/**
	 *
	 * @param username
	 */
	public TopupPaginator() {

		// this.username = username;

		countUtils = CountUtils.getInstance();

		// accountDAO = AccountDAO.getInstance();

		// account = accountDAO.getAccountName(username);

		topupDAO = TopupDAO.getInstance();

	}

	/**
	 *
	 * @param username
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public TopupPaginator(String dbName, String dbHost, String dbUsername, String dbPasswd, int dbPort) {
		// this.username = username;

		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);

		topupDAO = new TopupDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);

		// accountDAO = new AccountDAO(dbName,
		// dbHost, dbUsername, dbPasswd, dbPort);

		// account = accountDAO.getAccountName(username);

	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getFirstPage()
	 */
	public TopupPage getFirstPage() {
		TopupPage result = new TopupPage();
		List<Topup> topupList;

		topupList = topupDAO.getAllTopups(0, PAGESIZE);

		result = new TopupPage(1, getTotalPage(), PAGESIZE, topupList);

		return result;
	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getLastPage()
	 */
	public TopupPage getLastPage() {

		TopupPage result = new TopupPage();
		List<Topup> topupList;
		int transactionCount, startIndex;
		int totalPage = getTotalPage();

		startIndex = (totalPage - 1) * PAGESIZE;
		transactionCount = countUtils.getAllTopupCount();
		topupList = topupDAO.getAllTopups(startIndex, transactionCount);

		result = new TopupPage(totalPage, totalPage, PAGESIZE, topupList);

		return result;

	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getNextPage(com.impalapay.airtel.accountmgmt.pagination.TransactionPage)
	 */
	public TopupPage getNextPage(final TopupPage currentPage) {
		int totalPage = getTotalPage();

		TopupPage result = new TopupPage();

		List<Topup> topupList = topupDAO.getAllTopups(currentPage.getPageNum() * PAGESIZE,
				((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		result = new TopupPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, topupList);

		return result;

	}

	/**
	 * @see com.impalapay.airtel.accountmgmt.pagination.TransactionPaginating#getPrevPage(com.impalapay.airtel.accountmgmt.pagination.TransactionPage)
	 */
	public TopupPage getPrevPage(final TopupPage currentPage) {
		int totalPage = getTotalPage();

		TopupPage result = new TopupPage();
		List<Topup> topupList = topupDAO.getAllTopups((currentPage.getPageNum() - 2) * PAGESIZE,
				((currentPage.getPageNum() - 1) * PAGESIZE));

		result = new TopupPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, topupList);

		return result;

	}

	/**
	 *
	 * @return int
	 */
	public int getTotalPage() {
		int totalSize = 0;

		totalSize = countUtils.getAllTopupCount();

		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

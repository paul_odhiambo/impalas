/**
 * Copyright 2015 Tawi Commercial Services Ltd
 * 
 * Licensed under the Open Software License, Version 3.0  you may
 * not use this file except in compliance with the License. You may obtain a copy
 * of the License at:
 * http://opensource.org/licenses/OSL-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an AS IS BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 * 
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package com.impalapay.airtel.accountmgmt.admin.pagination.bankcode;

import java.util.List;
import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.airtel.beans.bank.BankCodes;
import com.impalapay.airtel.persistence.bank.BankCodesDAO;

/**
 * Paginate an Inbox HTML view.
 * <p>
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 */
public class BankPaginator {

	/**
	 *
	 */

	public static final int PAGESIZE = 13;
	private final BankCodesDAO bankcodeDAO;
	private final CountUtils countUtils;

	/**
	 *
	 * @param accountuuid
	 */
	public BankPaginator() {

		countUtils = CountUtils.getInstance();
		bankcodeDAO = BankCodesDAO.getInstance();

	}

	/**
	 *
	 * @param email
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public BankPaginator(String email, String dbName, String dbHost, String dbUsername, String dbPasswd, int dbPort) {

		// initialize the DAOs
		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);
		bankcodeDAO = new BankCodesDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);
	}

	/**
	 *
	 * @return
	 */
	public BankPage getFirstPage() {

		BankPage page = new BankPage();

		List<BankCodes> addressList = bankcodeDAO.getAllBankCodes(0, PAGESIZE);

		page = new BankPage(1, getTotalPage(), PAGESIZE, addressList);
		// result = new IncomingSMSPage (1, getTotalPage(), PAGESIZE, smsList);

		return page;
	}

	/**
	 * Provides the last page of the ip list report
	 *
	 * @return ip page
	 */
	public BankPage getLastPage() {
		BankPage page = new BankPage();

		List<BankCodes> addressList = null;
		int addressCount, startIndex;
		int totalPage = getTotalPage();

		startIndex = (totalPage - 1) * PAGESIZE;
		addressCount = countUtils.getAllBankCodeCount();

		addressList = bankcodeDAO.getAllBankCodes(startIndex, addressCount);

		page = new BankPage(totalPage, totalPage, PAGESIZE, addressList);

		return page;
	}

	/**
	 * Moves you forward to the page of the Accounts that comes after the current
	 * page
	 *
	 * @param currentPage
	 * @return an ip page
	 */
	public BankPage getNextPage(final BankPage currentPage) {
		int totalPage = getTotalPage();

		BankPage page = new BankPage();

		List<BankCodes> addressList = bankcodeDAO.getAllBankCodes(currentPage.getPageNum() * PAGESIZE,
				((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		page = new BankPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, addressList);

		return page;
	}

	/**
	 * Moves you backward to the page of the Accounts that comes before the current
	 * page
	 *
	 * @param currentPage
	 * @return an ip page
	 */
	public BankPage getPrevPage(final BankPage currentPage) {
		int totalPage = getTotalPage();

		BankPage page = new BankPage();

		List<BankCodes> addressList = bankcodeDAO.getAllBankCodes((currentPage.getPageNum() - 2) * PAGESIZE,
				((currentPage.getPageNum() - 1) * PAGESIZE));

		page = new BankPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, addressList);

		return page;
	}

	/**
	 * Calculates the total number of pages that would be printed for the Account
	 * sessions that belong to the logged-in account
	 *
	 * @return an integer
	 */
	public int getTotalPage() {
		int totalSize = 0;

		// get the number of all sessions belonging to this email
		totalSize = countUtils.getAllBankCodeCount();

		// TODO: divide by the page size and add one to take care of remainders and what
		// else?
		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

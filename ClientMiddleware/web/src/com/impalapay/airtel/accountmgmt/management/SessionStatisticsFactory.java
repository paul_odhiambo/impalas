package com.impalapay.airtel.accountmgmt.management;

import com.impalapay.airtel.accountmgmt.session.SessionStatistics;
import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;
import com.impalapay.airtel.persistence.util.CountUtils;

/**
 * Creates statistics that are to be cached in the session of a user.
 * <p>
 * Copyright (c) ImpalaPay Ltd., July 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class SessionStatisticsFactory {

	private static CountUtils countUtils;

	static {
		countUtils = CountUtils.getInstance();

	}

	/**
	 * Some refactoring with reflection can be applied here.
	 * 
	 * @param account
	 * @return {@link SessionStatistics}
	 */
	public static SessionStatistics getSessionStatistics(ManagementAccount account) {
		SessionStatistics stats = new SessionStatistics();

		int count;
		double amount;

		stats.setManagementAccountCountTotal(10);

		return stats;

	}

}

/*
 ** Local Variables: mode: java c-basic-offset: 2 tab-width: 2 indent-tabs-mode:
 * nil End:
 **
 ** ex: set softtabstop=2 tabstop=2 expandtab:
 **
 */
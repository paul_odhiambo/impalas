package com.impalapay.airtel.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.math.NumberUtils;

public class TestDateUtil {

	public static void main(String[] args) throws ParseException {

		Calendar c = Calendar.getInstance();
		c.set(NumberUtils.toInt("1990"), NumberUtils.toInt("05") - 1, NumberUtils.toInt("9"));
		Date date = c.getTime();
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String inActiveDate = null;
		inActiveDate = format1.format(date);
		System.out.println(inActiveDate);

	}

}

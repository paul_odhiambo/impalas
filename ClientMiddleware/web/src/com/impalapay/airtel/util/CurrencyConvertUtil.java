package com.impalapay.airtel.util;

import java.math.BigDecimal;

import org.apache.log4j.Logger;

public class CurrencyConvertUtil {

	private static Logger logger = Logger.getLogger(CurrencyConvertUtil.class);

	public static double round2(double d, int decimalplaces) {

		BigDecimal bdTest = new BigDecimal(d);

		bdTest = bdTest.setScale(decimalplaces, BigDecimal.ROUND_UP);

		return bdTest.doubleValue();

	}

	public static double round(double d, int decimalplaces) {

		BigDecimal bdTest = new BigDecimal(d);

		bdTest = bdTest.setScale(decimalplaces, BigDecimal.ROUND_DOWN);

		return bdTest.doubleValue();

	}

	public static int doubleToInteger(double value) {

		Double d = new Double(value);
		int returnintvalue = d.intValue();

		return returnintvalue;
	}

	public static double multiplyForex(double a, double b) {

		Double value = a * b;

		return round2(value, 10);
	}

	public static double subtractDeficit(double originalamountafterconversion, int amountafterintegerconversion) {

		double subtracted = originalamountafterconversion - amountafterintegerconversion;

		double finaldefict = round(subtracted, 2);

		return finaldefict;
	}

	/***
	 * public static void main(String args[]){
	 * 
	 * System.out.println(subtractDeficit(100.9577878, 100)); }
	 **/

}
package com.impalapay.airtel.util;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Tests our {@link SecurityUtil}
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 13, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class TestSecurityUtil {

	final String ORIGINAL_STRING = "secret";
	// final String HASHED_STRING = "af606efbc2d0420ea16ede72cf89ba4c";

	/**
	 * Test method for
	 * {@link com.impalapay.airtel.util.SecurityUtil#getMD5Hash(java.lang.String)}.
	 */
	@Test
	public void testGetMD5Hash() {
		// assertEquals(SecurityUtil.getMD5Hash(ORIGINAL_STRING), HASHED_STRING);

		System.out.println(SecurityUtil.getMD5Hash("admin"));
	}

}

/*
 ** Local Variables: mode: java c-basic-offset: 2 tab-width: 2 indent-tabs-mode:
 * nil End:
 **
 ** ex: set softtabstop=2 tabstop=2 expandtab:
 **
 */
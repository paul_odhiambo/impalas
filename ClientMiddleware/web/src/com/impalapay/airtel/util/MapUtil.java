package com.impalapay.airtel.util;

import java.util.HashMap;

public class MapUtil {

	// used to get a key when given a value.
	public static Object getKeyFromValue(HashMap hm, Object value) {
		for (Object o : hm.keySet()) {
			if (hm.get(o).equals(value)) {
				return o;
			}
		}
		return null;
	}

}

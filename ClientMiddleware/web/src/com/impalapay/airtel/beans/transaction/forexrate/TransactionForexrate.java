package com.impalapay.airtel.beans.transaction.forexrate;

import java.util.Date;

import com.impalapay.airtel.beans.StorableBean;

/**
 * represents a transaction with forex rate quoted
 * <p>
 * Copyright (c) impalapay Ltd., April 16, 2015
 * 
 * @author <a href="mailto:eugenechimita@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */

public class TransactionForexrate extends StorableBean {

	private String transactionUuid;
	private String account;
	private String recipientcountry;
	private double localamount;
	private String accounttype;
	private double convertedamount;
	private double impalarate;
	private double baserate;
	private String receivermsisdn;
	private double surplus;
	private Date serverTime;

	public TransactionForexrate() {

		transactionUuid = "";
		account = "";
		recipientcountry = "";
		localamount = 0;
		accounttype = "";
		convertedamount = 0;
		impalarate = 0;
		baserate = 0;
		receivermsisdn = "";
		surplus = 0;
		serverTime = new Date();

	}

	public String getTransactionUuid() {
		return transactionUuid;
	}

	public void setTransactionUuid(String transactionUuid) {
		this.transactionUuid = transactionUuid;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getRecipientcountry() {
		return recipientcountry;
	}

	public void setRecipientcountry(String recipientcountry) {
		this.recipientcountry = recipientcountry;
	}

	public double getLocalamount() {
		return localamount;
	}

	public void setLocalamount(double localamount) {
		this.localamount = localamount;
	}

	public String getAccounttype() {
		return accounttype;
	}

	public void setAccounttype(String accounttype) {
		this.accounttype = accounttype;
	}

	public double getConvertedamount() {
		return convertedamount;
	}

	public void setConvertedamount(double convertedamount) {
		this.convertedamount = convertedamount;
	}

	public double getImpalarate() {
		return impalarate;
	}

	public void setImpalarate(double impalarate) {
		this.impalarate = impalarate;
	}

	public double getBaserate() {
		return baserate;
	}

	public void setBaserate(double baserate) {
		this.baserate = baserate;
	}

	public String getReceivermsisdn() {
		return receivermsisdn;
	}

	public void setReceivermsisdn(String receivermsisdn) {
		this.receivermsisdn = receivermsisdn;
	}

	public double getSurplus() {
		return surplus;
	}

	public void setSurplus(double surplus) {
		this.surplus = surplus;
	}

	public Date getServerTime() {
		return serverTime;
	}

	public void setServerTime(Date serverTime) {
		this.serverTime = serverTime;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TransactionForexrate [getUuid()=");
		builder.append(getUuid());
		builder.append(", transactionUuid=");
		builder.append(transactionUuid);
		builder.append(", account=");
		builder.append(account);
		builder.append(", recipientcountry=");
		builder.append(recipientcountry);
		builder.append(", localamount=");
		builder.append(localamount);
		builder.append(", accounttype=");
		builder.append(accounttype);
		builder.append(", convertedamount=");
		builder.append(convertedamount);
		builder.append(", impalarate=");
		builder.append(impalarate);
		builder.append(", baserate=");
		builder.append(baserate);
		builder.append(", receivermsisdn=");
		builder.append(receivermsisdn);
		builder.append(", surplus=");
		builder.append(surplus);
		builder.append(", serverTime=");
		builder.append(serverTime);
		builder.append("]");
		return builder.toString();
	}

}

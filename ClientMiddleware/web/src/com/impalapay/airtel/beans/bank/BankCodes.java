package com.impalapay.airtel.beans.bank;

import java.util.Date;

import com.impalapay.airtel.beans.StorableBean;

public class BankCodes extends StorableBean {

	private String bankname;
	private String countryuuid;
	private String networkuuid;
	private String bankcode;
	private String branchcode;
	private String iban;
	private Date dateadded;

	public BankCodes() {
		super();
		bankname = "";
		countryuuid = "";
		networkuuid = "";
		bankcode = "";
		branchcode = "";
		iban = "";
		dateadded = new Date();
	}

	public String getBankname() {
		return bankname;
	}

	public String getCountryuuid() {
		return countryuuid;
	}

	public String getNetworkuuid() {
		return networkuuid;
	}

	public String getBankcode() {
		return bankcode;
	}

	public String getBranchcode() {
		return branchcode;
	}

	public String getIban() {
		return iban;
	}

	public Date getDateadded() {
		return dateadded;
	}

	public void setBankname(String bankname) {
		this.bankname = bankname;
	}

	public void setCountryuuid(String countryuuid) {
		this.countryuuid = countryuuid;
	}

	public void setNetworkuuid(String networkuuid) {
		this.networkuuid = networkuuid;
	}

	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}

	public void setBranchcode(String branchcode) {
		this.branchcode = branchcode;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public void setDateadded(Date dateadded) {
		this.dateadded = dateadded;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BankCodes [getUuid()=");
		builder.append(getUuid());
		builder.append(", bankname=");
		builder.append(bankname);
		builder.append(", countryuuid=");
		builder.append(countryuuid);
		builder.append(", networkuuid=");
		builder.append(networkuuid);
		builder.append(", bankcode=");
		builder.append(bankcode);
		builder.append(", branchcode=");
		builder.append(branchcode);
		builder.append(", iban=");
		builder.append(iban);
		builder.append(", dateadded=");
		builder.append(dateadded);
		builder.append("]");
		return builder.toString();
	}

}

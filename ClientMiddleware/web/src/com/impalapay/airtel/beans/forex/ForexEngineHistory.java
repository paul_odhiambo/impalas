package com.impalapay.airtel.beans.forex;

import java.util.Date;

public class ForexEngineHistory extends ForexEngine {

	private Date uploadDate;

	/**
	 * 
	 */
	public ForexEngineHistory() {

		super();

		uploadDate = new Date();
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ForexEngineHistory [getUuid()=");
		builder.append(getUuid());
		builder.append(", getBasecurrency()=");
		builder.append(getBasecurrency());
		builder.append(", getQuotecurrency()=");
		builder.append(getQuotecurrency());
		builder.append(", getCurrencypair()=");
		builder.append(getCurrencypair());
		builder.append(", getMarketrate()=");
		builder.append(getMarketrate());
		builder.append(", getSpreadrate()=");
		builder.append(getSpreadrate());
		builder.append(", uploadDate=");
		builder.append(uploadDate);
		builder.append("]");
		return builder.toString();
	}

}

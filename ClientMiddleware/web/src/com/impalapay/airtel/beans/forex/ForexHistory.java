package com.impalapay.airtel.beans.forex;

import java.util.Date;

public class ForexHistory extends Forex {

	private String currencytype;

	private Date uploadDate;

	/**
	 * 
	 */
	public ForexHistory() {

		super();

		currencytype = "";
		uploadDate = new Date();
	}

	/**
	 * 
	 * @return
	 */
	public String getCurrencytype() {
		return currencytype;
	}

	/**
	 * 
	 * @return
	 */
	public Date getUploadDate() {
		return uploadDate;
	}

	/**
	 * 
	 * @param currencytype
	 */
	public void setCurrencytype(String currencytype) {
		this.currencytype = currencytype;
	}

	/**
	 * 
	 * @param uploadDate
	 */
	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ForexHistory [getUuid()=");
		builder.append(getUuid());
		builder.append(", getCountryUuid()=");
		builder.append(getCountryUuid());
		builder.append(", getBaserate()=");
		builder.append(getBaserate());
		builder.append(", getImpalarate()=");
		builder.append(getImpalarate());
		builder.append(", currencytype=");
		builder.append(currencytype);
		builder.append(", uploadDate=");
		builder.append(uploadDate);
		builder.append("]");
		return builder.toString();
	}
}

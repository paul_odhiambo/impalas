package com.impalapay.airtel.beans.accountmgmt.inprogressbalance;

public class InProgressMasterBalanceHoldHist extends InProgressBalanceHoldHistory {

	private String currency;

	public InProgressMasterBalanceHoldHist() {
		super();

		currency = "";

	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InProgressMasterBalanceHoldHist [getUuid()=");
		builder.append(getUuid());
		builder.append(", getTopuptime()=");
		builder.append(getTopuptime());
		builder.append(", isRefundedback()=");
		builder.append(isRefundedback());
		builder.append(", isProcessed()=");
		builder.append(isProcessed());
		builder.append(", getAccountUuid()=");
		builder.append(getAccountUuid());
		builder.append(", getTransactionuuid()=");
		builder.append(getTransactionuuid());
		builder.append(", getAmount()=");
		builder.append(getAmount());
		builder.append(", currency=");
		builder.append(currency);
		builder.append("]");
		return builder.toString();
	}

}

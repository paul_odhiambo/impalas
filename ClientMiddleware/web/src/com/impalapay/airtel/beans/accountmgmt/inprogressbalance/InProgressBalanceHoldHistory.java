package com.impalapay.airtel.beans.accountmgmt.inprogressbalance;

import java.util.Date;

/**
 * A generic balance of account
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class InProgressBalanceHoldHistory extends InProgressBalanceHold {

	private Date topuptime;
	private boolean refundedback;
	private boolean processed;

	/**
	 * 
	 */
	public InProgressBalanceHoldHistory() {
		super();

		topuptime = new Date();
		refundedback = false;
		processed = false;

	}

	public Date getTopuptime() {
		return topuptime;
	}

	public boolean isRefundedback() {
		return refundedback;
	}

	public boolean isProcessed() {
		return processed;
	}

	public void setProcessed(boolean processed) {
		this.processed = processed;
	}

	public void setTopuptime(Date topuptime) {
		this.topuptime = topuptime;
	}

	public void setRefundedback(boolean refundedback) {
		this.refundedback = refundedback;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InProgressBalanceHoldHistory [getUuid()=");
		builder.append(getUuid());
		builder.append(", getAccountUuid()=");
		builder.append(getAccountUuid());
		builder.append(", getTransactionuuid()=");
		builder.append(getTransactionuuid());
		builder.append(", getAmount()=");
		builder.append(getAmount());
		builder.append(", topuptime=");
		builder.append(topuptime);
		builder.append(", refundedback=");
		builder.append(refundedback);
		builder.append(", processed=");
		builder.append(processed);
		builder.append("]");
		return builder.toString();
	}

}

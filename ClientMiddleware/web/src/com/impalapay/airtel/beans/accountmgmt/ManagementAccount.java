package com.impalapay.airtel.beans.accountmgmt;

import com.impalapay.airtel.beans.StorableBean;

import java.util.Date;

/**
 * Represents an account holder who can send and receive SMS through the system.
 * <p>
 * Copyright (c) ImpalaPay LTD., June 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class ManagementAccount extends StorableBean {

	private String accountStatusUuid;
	private String accountName;
	private String username;
	private String loginPasswd;
	private boolean updateforex;
	private boolean updatebalance;
	private boolean updatereversal;
	private boolean checker;
	private Date creationDate;

	/**
	 *
	 */
	public ManagementAccount() {
		super();
		accountStatusUuid = "";
		accountName = "";
		username = "";
		loginPasswd = "";
		updateforex = false;
		updatebalance = false;
		updatereversal = false;
		checker = false;
		creationDate = new Date();
	}

	public String getAccountStatusUuid() {
		return accountStatusUuid;
	}

	public void setAccountStatusUuid(String accountStatusUuid) {
		this.accountStatusUuid = accountStatusUuid;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLoginPasswd() {
		return loginPasswd;
	}

	public void setLoginPasswd(String loginPasswd) {
		this.loginPasswd = loginPasswd;
	}

	public boolean isUpdateforex() {
		return updateforex;
	}

	public void setUpdateforex(boolean updateforex) {
		this.updateforex = updateforex;
	}

	public boolean isUpdatebalance() {
		return updatebalance;
	}

	public void setUpdatebalance(boolean updatebalance) {
		this.updatebalance = updatebalance;
	}

	public boolean isUpdatereversal() {
		return updatereversal;
	}

	public void setUpdatereversal(boolean updatereversal) {
		this.updatereversal = updatereversal;
	}

	public boolean isChecker() {
		return checker;
	}

	public void setChecker(boolean checker) {
		this.checker = checker;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ManagementAccount [getUuid()=");
		builder.append(getUuid());
		builder.append(", accountStatusUuid=");
		builder.append(accountStatusUuid);
		builder.append(", accountName=");
		builder.append(accountName);
		builder.append(", username=");
		builder.append(username);
		builder.append(", loginPasswd=");
		builder.append(loginPasswd);
		builder.append(", updateforex=");
		builder.append(updateforex);
		builder.append(", updatebalance=");
		builder.append(updatebalance);
		builder.append(", updatereversal=");
		builder.append(updatereversal);
		builder.append(", checker=");
		builder.append(checker);
		builder.append(", creationDate=");
		builder.append(creationDate);
		builder.append("]");
		return builder.toString();
	}

}

/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */
package com.impalapay.airtel.beans.accountmgmt.inprogressbalance;

import com.impalapay.airtel.beans.StorableBean;

/**
 * A generic balance of account
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class InProgressBalanceHold extends StorableBean {

	private String accountUuid;
	private String transactionuuid;
	private double amount;

	/**
	 * 
	 */
	public InProgressBalanceHold() {
		super();
		accountUuid = "";
		transactionuuid = "";
		amount = 0;

	}

	public String getAccountUuid() {
		return accountUuid;
	}

	public String getTransactionuuid() {
		return transactionuuid;
	}

	public double getAmount() {
		return amount;
	}

	public void setAccountUuid(String accountUuid) {
		this.accountUuid = accountUuid;
	}

	public void setTransactionuuid(String transactionuuid) {
		this.transactionuuid = transactionuuid;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}

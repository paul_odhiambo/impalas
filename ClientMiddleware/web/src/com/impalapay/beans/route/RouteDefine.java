package com.impalapay.beans.route;

import com.impalapay.airtel.beans.StorableBean;

import java.util.Date;

/**
 * Used to define conditions for various remit routes
 * <p>
 * Copyright (c) ImpalaPay LTD., Feb 14, 2015
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */

public class RouteDefine extends StorableBean {
	private String accountUuid;
	private String networkUuid;
	private boolean supportforex;
	private boolean fixedcommission;
	private boolean presettlement;
	private double commission;
	private double minimumbalance;
	private Date creationDate;

	public RouteDefine() {
		accountUuid = "";
		networkUuid = "";
		supportforex = false;
		fixedcommission = true;
		presettlement = true;
		commission = 0;
		minimumbalance = 0;
		creationDate = new Date();

	}

	public String getAccountUuid() {
		return accountUuid;
	}

	public void setAccountUuid(String accountUuid) {
		this.accountUuid = accountUuid;
	}

	public String getNetworkUuid() {
		return networkUuid;
	}

	public void setNetworkUuid(String networkUuid) {
		this.networkUuid = networkUuid;
	}

	public boolean isSupportforex() {
		return supportforex;
	}

	public void setSupportforex(boolean supportforex) {
		this.supportforex = supportforex;
	}

	public boolean isFixedcommission() {
		return fixedcommission;
	}

	public void setFixedcommission(boolean fixedcommission) {
		this.fixedcommission = fixedcommission;
	}

	public boolean isPresettlement() {
		return presettlement;
	}

	public void setPresettlement(boolean presettlement) {
		this.presettlement = presettlement;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public double getMinimumbalance() {
		return minimumbalance;
	}

	public void setMinimumbalance(double minimumbalance) {
		this.minimumbalance = minimumbalance;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RouteDefine [getUuid()=");
		builder.append(getUuid());
		builder.append(", accountUuid=");
		builder.append(accountUuid);
		builder.append(", networkUuid=");
		builder.append(networkUuid);
		builder.append(", supportforex=");
		builder.append(supportforex);
		builder.append(", fixedcommission=");
		builder.append(fixedcommission);
		builder.append(", presettlement=");
		builder.append(presettlement);
		builder.append(", commission=");
		builder.append(commission);
		builder.append(", minimumbalance=");
		builder.append(minimumbalance);
		builder.append(", creationDate=");
		builder.append(creationDate);
		builder.append("]");
		return builder.toString();
	}

}

package com.impalapay.beans.network;

import com.impalapay.airtel.beans.StorableBean;
import java.util.Date;

/**
 * Represents route networks can include mno or bank.
 * <p>
 * Copyright (c) ImpalaPay LTD., Feb 14, 2015
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */

public class Network extends StorableBean {

	private String countryUuid;

	private String remitip;

	private String bridgeremitip;

	private String queryip;

	private String bridgequeryip;

	private String balanceip;

	private String bridgebalanceip;

	private String reversalip;

	private String bridgereversalip;

	private String forexip;

	private String bridgeforexip;

	private String accountcheckip;

	private String bridgeaccountcheckip;

	private String extraurl;

	private String username;

	private String password;

	private String networkname;

	private String partnername;

	private String networkStatusUuid;

	private double commission;

	private boolean supportforex;

	private boolean supportquerycheck;

	private boolean supportbalancecheck;

	private boolean supportreversal;

	private boolean supportaccountcheck;

	private boolean supportcommissionpercentage;

	private Date dateadded;

	public Network() {
		super();

		countryUuid = "";

		remitip = "";

		bridgeremitip = "";

		queryip = "";

		bridgequeryip = "";

		balanceip = "";

		bridgebalanceip = "";

		reversalip = "";

		bridgereversalip = "";

		forexip = "";

		bridgeforexip = "";

		accountcheckip = "";

		bridgeforexip = "";

		extraurl = "";

		username = "";

		password = "";

		networkname = "";

		partnername = "";

		networkStatusUuid = "";

		commission = 0;

		supportcommissionpercentage = false;

		supportforex = false;

		supportreversal = false;

		supportaccountcheck = false;

		supportquerycheck = false;

		supportbalancecheck = false;

		dateadded = new Date();
	}

	public String getCountryUuid() {
		return countryUuid;
	}

	public String getRemitip() {
		return remitip;
	}

	public String getBridgeremitip() {
		return bridgeremitip;
	}

	public String getQueryip() {
		return queryip;
	}

	public String getBridgequeryip() {
		return bridgequeryip;
	}

	public String getBalanceip() {
		return balanceip;
	}

	public String getBridgebalanceip() {
		return bridgebalanceip;
	}

	public String getReversalip() {
		return reversalip;
	}

	public String getBridgereversalip() {
		return bridgereversalip;
	}

	public String getForexip() {
		return forexip;
	}

	public String getBridgeforexip() {
		return bridgeforexip;
	}

	public String getAccountcheckip() {
		return accountcheckip;
	}

	public String getBridgeaccountcheckip() {
		return bridgeaccountcheckip;
	}

	public String getExtraurl() {
		return extraurl;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getNetworkname() {
		return networkname;
	}

	public String getPartnername() {
		return partnername;
	}

	public String getNetworkStatusUuid() {
		return networkStatusUuid;
	}

	public double getCommission() {
		return commission;
	}

	public boolean isSupportforex() {
		return supportforex;
	}

	public boolean isSupportquerycheck() {
		return supportquerycheck;
	}

	public boolean isSupportbalancecheck() {
		return supportbalancecheck;
	}

	public boolean isSupportreversal() {
		return supportreversal;
	}

	public boolean isSupportaccountcheck() {
		return supportaccountcheck;
	}

	public boolean isSupportcommissionpercentage() {
		return supportcommissionpercentage;
	}

	public Date getDateadded() {
		return dateadded;
	}

	public void setCountryUuid(String countryUuid) {
		this.countryUuid = countryUuid;
	}

	public void setRemitip(String remitip) {
		this.remitip = remitip;
	}

	public void setBridgeremitip(String bridgeremitip) {
		this.bridgeremitip = bridgeremitip;
	}

	public void setQueryip(String queryip) {
		this.queryip = queryip;
	}

	public void setBridgequeryip(String bridgequeryip) {
		this.bridgequeryip = bridgequeryip;
	}

	public void setBalanceip(String balanceip) {
		this.balanceip = balanceip;
	}

	public void setBridgebalanceip(String bridgebalanceip) {
		this.bridgebalanceip = bridgebalanceip;
	}

	public void setReversalip(String reversalip) {
		this.reversalip = reversalip;
	}

	public void setBridgereversalip(String bridgereversalip) {
		this.bridgereversalip = bridgereversalip;
	}

	public void setForexip(String forexip) {
		this.forexip = forexip;
	}

	public void setBridgeforexip(String bridgeforexip) {
		this.bridgeforexip = bridgeforexip;
	}

	public void setAccountcheckip(String accountcheckip) {
		this.accountcheckip = accountcheckip;
	}

	public void setBridgeaccountcheckip(String bridgeaccountcheckip) {
		this.bridgeaccountcheckip = bridgeaccountcheckip;
	}

	public void setExtraurl(String extraurl) {
		this.extraurl = extraurl;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setNetworkname(String networkname) {
		this.networkname = networkname;
	}

	public void setPartnername(String partnername) {
		this.partnername = partnername;
	}

	public void setNetworkStatusUuid(String networkStatusUuid) {
		this.networkStatusUuid = networkStatusUuid;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public void setSupportforex(boolean supportforex) {
		this.supportforex = supportforex;
	}

	public void setSupportquerycheck(boolean supportquerycheck) {
		this.supportquerycheck = supportquerycheck;
	}

	public void setSupportbalancecheck(boolean supportbalancecheck) {
		this.supportbalancecheck = supportbalancecheck;
	}

	public void setSupportreversal(boolean supportreversal) {
		this.supportreversal = supportreversal;
	}

	public void setSupportaccountcheck(boolean supportaccountcheck) {
		this.supportaccountcheck = supportaccountcheck;
	}

	public void setSupportcommissionpercentage(boolean supportcommissionpercentage) {
		this.supportcommissionpercentage = supportcommissionpercentage;
	}

	public void setDateadded(Date dateadded) {
		this.dateadded = dateadded;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Network [getUuid()=");
		builder.append(getUuid());
		builder.append(", countryUuid=");
		builder.append(countryUuid);
		builder.append(", remitip=");
		builder.append(remitip);
		builder.append(", bridgeremitip=");
		builder.append(bridgeremitip);
		builder.append(", queryip=");
		builder.append(queryip);
		builder.append(", bridgequeryip=");
		builder.append(bridgequeryip);
		builder.append(", balanceip=");
		builder.append(balanceip);
		builder.append(", bridgebalanceip=");
		builder.append(bridgebalanceip);
		builder.append(", reversalip=");
		builder.append(reversalip);
		builder.append(", bridgereversalip=");
		builder.append(bridgereversalip);
		builder.append(", forexip=");
		builder.append(forexip);
		builder.append(", bridgeforexip=");
		builder.append(bridgeforexip);
		builder.append(", accountcheckip=");
		builder.append(accountcheckip);
		builder.append(", bridgeaccountcheckip=");
		builder.append(bridgeaccountcheckip);
		builder.append(", extraurl=");
		builder.append(extraurl);
		builder.append(", username=");
		builder.append(username);
		builder.append(", password=");
		builder.append(password);
		builder.append(", networkname=");
		builder.append(networkname);
		builder.append(", partnername=");
		builder.append(partnername);
		builder.append(", networkStatusUuid=");
		builder.append(networkStatusUuid);
		builder.append(", commission=");
		builder.append(commission);
		builder.append(", supportforex=");
		builder.append(supportforex);
		builder.append(", supportquerycheck=");
		builder.append(supportquerycheck);
		builder.append(", supportbalancecheck=");
		builder.append(supportbalancecheck);
		builder.append(", supportreversal=");
		builder.append(supportreversal);
		builder.append(", supportaccountcheck=");
		builder.append(supportaccountcheck);
		builder.append(", supportcommissionpercentage=");
		builder.append(supportcommissionpercentage);
		builder.append(", dateadded=");
		builder.append(dateadded);
		builder.append("]");
		return builder.toString();
	}

}

package com.impalapay.persistence.routing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.beans.route.RouteDefine;

public class TestRoute {

	final String DB_NAME = "airteldblive";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "airtellive";
	final String DB_PASSWD = "ThejKoyb34";
	/**
	 * final String DB_NAME = "impalahubdb"; final String DB_HOST = "localhost";
	 * final String DB_USERNAME = "impalahub"; final String DB_PASSWD =
	 * "shycsEtiv4";
	 **/
	final int DB_PORT = 5432;

	final String UUID = "10";
	final String ACCOUNT_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String ROUTE_TYPE = "e-wallet";
	final String ROUTE_NAME = "IDT-E-WALLET";
	final String USERNAME = "demo";
	final String PASSWORD = "demo";
	final String END_POINTURL = "http://localhost/testjson/";
	final String ACCOUNT_URL = "http://localhost/testjson2/";
	final String ROUTESTATUSUUID = "acecb9fa-7e21-455d-8abb-c61a840cdbec";

	final int ROUTE_COUNT = 1;

	final String REPLACE_IP = "127.168.10.67";

	private RouteDAO storage;

	@Test
	// @Ignore
	public void testPutRoute() {
		storage = new RouteDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		RouteDefine routing = new RouteDefine();

		routing.setUuid("452356525");
		routing.setAccountUuid(ACCOUNT_UUID);
		routing.setNetworkUuid("81bf3078-4495-4bec-a50d-c91a7c512d78");
		routing.setSupportforex(true);
		routing.setFixedcommission(true);
		routing.setCommission(10);
		routing.setMinimumbalance(3000);
		routing.setCreationDate(new Date());

		assertTrue(storage.putRoute(routing));

	}

	@Test
	@Ignore
	public void testGetCommissions() {
		storage = new RouteDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		RouteDefine testing = storage.getRoutes("81bf3078-4495-4bec-a50d-c91a7c512d78");

		System.out.println(testing.getCommission());

	}

	@Test
	@Ignore
	public void testGetAllcommissions() {
		storage = new RouteDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		List<RouteDefine> list = storage.getAllRoutes();
		assertEquals(list.size(), ROUTE_COUNT);

	}

	@Test
	@Ignore
	public void testGetAllcommissionsSize() {
		storage = new RouteDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		List<RouteDefine> list = storage.getAllRoute(0, 2);
		// assertEquals(list.size(), 7);

		System.out.println(list);

	}

	@Ignore
	@Test
	public void testGetRoute() {
		storage = new RouteDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);
		Account account = new Account();

		account.setUuid(ACCOUNT_UUID);

		RouteDefine list = storage.getRoutes("81bf3078-4495-4bec-a50d-c91a7c512d78", account);
		// assertEquals(list.size(), 7);

		System.out.println(list);

	}

	@Test
	@Ignore
	public void testGetRouteAccount() {
		storage = new RouteDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		Account account = new Account();

		account.setUuid(ACCOUNT_UUID);
		List<RouteDefine> list = storage.getAllRoute(account);
		// assertEquals(list.size(), 7);

		System.out.println(list);

	}

}

package com.impalapay.persistence.routing;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.beans.route.RouteDefine;

public class RouteDAO extends GenericDAO implements ImpalaRoute {
	private static RouteDAO routeDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	public static RouteDAO getInstance() {

		if (routeDAO == null) {
			routeDAO = new RouteDAO();
		}
		return routeDAO;
	}

	protected RouteDAO() {
		super();
	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public RouteDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

	}

	@Override
	public RouteDefine getRoute(String id) {
		RouteDefine s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM routedefine WHERE uuid = ?;");
			pstmt.setString(1, id);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, RouteDefine.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting routing with uuid '" + id + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public RouteDefine getRoutes(String networkuuid) {
		RouteDefine s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM routedefine WHERE networkUuid = ?;");
			pstmt.setString(1, networkuuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, RouteDefine.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting routing with networkUuid '" + networkuuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public boolean putRoute(RouteDefine rotedefine) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("INSERT INTO routedefine(uuid,accountuuid"
					+ ",networkuuid,supportforex,fixedcommission,presettlement,commission,minimumbalance,creationdate) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, rotedefine.getUuid());
			pstmt.setString(2, rotedefine.getAccountUuid());
			pstmt.setString(3, rotedefine.getNetworkUuid());
			pstmt.setBoolean(4, rotedefine.isSupportforex());
			pstmt.setBoolean(5, rotedefine.isFixedcommission());
			pstmt.setBoolean(6, rotedefine.isPresettlement());
			pstmt.setDouble(7, rotedefine.getCommission());
			pstmt.setDouble(8, rotedefine.getMinimumbalance());
			pstmt.setTimestamp(9, new Timestamp(rotedefine.getCreationDate().getTime()));
			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + rotedefine);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;

	}

	@Override
	public List<RouteDefine> getAllRoutes() {
		List<RouteDefine> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM routedefine ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, RouteDefine.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all routes.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<RouteDefine> getAllRoute(int fromIndex, int toIndex) {
		List<RouteDefine> list = new ArrayList<>();
		RouteDefine s;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM routedefine ORDER BY accountuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, RouteDefine.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all routing from index " + fromIndex + " to index "
					+ toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public RouteDefine getRoutes(String networkuuid, Account account) {
		RouteDefine s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM routedefine WHERE networkUuid = ? AND accountuuid=?;");
			pstmt.setString(1, networkuuid);
			pstmt.setString(2, account.getUuid());

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, RouteDefine.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting routing with networkUuid '" + networkuuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public List<RouteDefine> getAllRoute(Account account) {
		List<RouteDefine> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM routedefine WHERE accountuuid=? ORDER BY id ASC;");
			pstmt.setString(1, account.getUuid());

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, RouteDefine.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all routes.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public boolean updateRoute(String uuid, RouteDefine rotedefine) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM routedefine WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				pstmt2 = conn.prepareStatement(
						"UPDATE routedefine SET accountuuid=?,networkuuid=?,supportforex=?,fixedcommission=?,presettlement=?,"
								+ "commission=?,minimumbalance=?,creationdate=?" + "WHERE uuid=?;");

				pstmt2.setString(1, rotedefine.getAccountUuid());
				pstmt2.setString(2, rotedefine.getNetworkUuid());
				pstmt2.setBoolean(3, rotedefine.isSupportforex());
				pstmt2.setBoolean(4, rotedefine.isFixedcommission());
				pstmt2.setBoolean(5, rotedefine.isPresettlement());
				pstmt2.setDouble(6, rotedefine.getCommission());
				pstmt2.setDouble(7, rotedefine.getMinimumbalance());
				pstmt2.setTimestamp(8, new Timestamp(rotedefine.getCreationDate().getTime()));
				pstmt2.setString(9, uuid);

				pstmt2.executeUpdate();

			} else {
				success = putRoute(rotedefine);
			}

		} catch (SQLException e) {
			logger.error(
					"SQLException when trying to update routedefine with uuid '" + uuid + "' with " + rotedefine + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public boolean deleteaccountroute(String uuid) {
		boolean success = true;
		Date date;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("DELETE FROM routedefine WHERE uuid=?;");

			pstmt.setString(1, uuid);

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while Deleting " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

}

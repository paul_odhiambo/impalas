package com.impalapay.collection.beans.incoming;

import java.util.Date;

import com.impalapay.airtel.beans.StorableBean;

public class TempCollection extends StorableBean {

	private String networkuuid;

	private String creditaccountuuid;

	private String transactiontype;

	private String debitorname;

	private String collectiondefineuuid;

	private String debitcurrency;

	private String debitcountry;

	private double amount;

	private String accountreference;

	private String transactionstatusuuid;

	private String debitedaccount;

	private String originatetransactionuuid;

	private String receivertransactionuuid;

	private String vendorunique;

	private double originateamount;

	private String originatecurrency;

	private String processingstatus;

	private String endpointstatusdescription;

	private Date servertime;

	public TempCollection() {
		super();
		// TODO Auto-generated constructor stub
		networkuuid = "";
		creditaccountuuid = "";
		transactiontype = "";
		debitorname = "";
		collectiondefineuuid = "";
		debitcurrency = "";
		debitcountry = "";
		amount = 0;
		accountreference = "";
		transactionstatusuuid = "";
		debitedaccount = "";
		originatetransactionuuid = "";
		receivertransactionuuid = "";
		vendorunique = "";
		originateamount = 0;
		originatecurrency = "";
		processingstatus = "";
		endpointstatusdescription = "";

		servertime = new Date();

	}

	public String getNetworkuuid() {
		return networkuuid;
	}

	public void setNetworkuuid(String networkuuid) {
		this.networkuuid = networkuuid;
	}

	public String getCreditaccountuuid() {
		return creditaccountuuid;
	}

	public void setCreditaccountuuid(String creditaccountuuid) {
		this.creditaccountuuid = creditaccountuuid;
	}

	public String getTransactiontype() {
		return transactiontype;
	}

	public void setTransactiontype(String transactiontype) {
		this.transactiontype = transactiontype;
	}

	public String getDebitorname() {
		return debitorname;
	}

	public void setDebitorname(String debitorname) {
		this.debitorname = debitorname;
	}

	public String getCollectiondefineuuid() {
		return collectiondefineuuid;
	}

	public void setCollectiondefineuuid(String collectiondefineuuid) {
		this.collectiondefineuuid = collectiondefineuuid;
	}

	public String getDebitcurrency() {
		return debitcurrency;
	}

	public void setDebitcurrency(String debitcurrency) {
		this.debitcurrency = debitcurrency;
	}

	public String getDebitcountry() {
		return debitcountry;
	}

	public void setDebitcountry(String debitcountry) {
		this.debitcountry = debitcountry;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getAccountreference() {
		return accountreference;
	}

	public void setAccountreference(String accountreference) {
		this.accountreference = accountreference;
	}

	public String getTransactionstatusuuid() {
		return transactionstatusuuid;
	}

	public void setTransactionstatusuuid(String transactionstatusuuid) {
		this.transactionstatusuuid = transactionstatusuuid;
	}

	public String getDebitedaccount() {
		return debitedaccount;
	}

	public void setDebitedaccount(String debitedaccount) {
		this.debitedaccount = debitedaccount;
	}

	public String getOriginatetransactionuuid() {
		return originatetransactionuuid;
	}

	public void setOriginatetransactionuuid(String originatetransactionuuid) {
		this.originatetransactionuuid = originatetransactionuuid;
	}

	public String getReceivertransactionuuid() {
		return receivertransactionuuid;
	}

	public void setReceivertransactionuuid(String receivertransactionuuid) {
		this.receivertransactionuuid = receivertransactionuuid;
	}

	public String getVendorunique() {
		return vendorunique;
	}

	public void setVendorunique(String vendorunique) {
		this.vendorunique = vendorunique;
	}

	public double getOriginateamount() {
		return originateamount;
	}

	public void setOriginateamount(double originateamount) {
		this.originateamount = originateamount;
	}

	public String getOriginatecurrency() {
		return originatecurrency;
	}

	public void setOriginatecurrency(String originatecurrency) {
		this.originatecurrency = originatecurrency;
	}

	public String getProcessingstatus() {
		return processingstatus;
	}

	public void setProcessingstatus(String processingstatus) {
		this.processingstatus = processingstatus;
	}

	public String getEndpointstatusdescription() {
		return endpointstatusdescription;
	}

	public void setEndpointstatusdescription(String endpointstatusdescription) {
		this.endpointstatusdescription = endpointstatusdescription;
	}

	public Date getServertime() {
		return servertime;
	}

	public void setServertime(Date servertime) {
		this.servertime = servertime;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TempCollection [getUuid()=");
		builder.append(getUuid());
		builder.append(", networkuuid=");
		builder.append(networkuuid);
		builder.append(", creditaccountuuid=");
		builder.append(creditaccountuuid);
		builder.append(", transactiontype=");
		builder.append(transactiontype);
		builder.append(", debitorname=");
		builder.append(debitorname);
		builder.append(", collectiondefineuuid=");
		builder.append(collectiondefineuuid);
		builder.append(", debitcurrency=");
		builder.append(debitcurrency);
		builder.append(", debitcountry=");
		builder.append(debitcountry);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", accountreference=");
		builder.append(accountreference);
		builder.append(", transactionstatusuuid=");
		builder.append(transactionstatusuuid);
		builder.append(", debitedaccount=");
		builder.append(debitedaccount);
		builder.append(", originatetransactionuuid=");
		builder.append(originatetransactionuuid);
		builder.append(", receivertransactionuuid=");
		builder.append(receivertransactionuuid);
		builder.append(", vendorunique=");
		builder.append(vendorunique);
		builder.append(", originateamount=");
		builder.append(originateamount);
		builder.append(", originatecurrency=");
		builder.append(originatecurrency);
		builder.append(", processingstatus=");
		builder.append(processingstatus);
		builder.append(", endpointstatusdescription=");
		builder.append(endpointstatusdescription);
		builder.append(", servertime=");
		builder.append(servertime);
		builder.append("]");
		return builder.toString();
	}

}

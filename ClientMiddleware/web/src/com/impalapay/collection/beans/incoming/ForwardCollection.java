package com.impalapay.collection.beans.incoming;

import java.util.Date;

import com.impalapay.airtel.beans.StorableBean;

public class ForwardCollection extends StorableBean {

	private String creditaccountuuid;

	private String transactiontype;

	private String debitorname;

	private String debitcurrency;

	private double amount;

	private String accountreference;

	private String debitedaccount;

	private String originatetransactionuuid;

	private String forwarduri;

	private String transactionuuid;

	private Date collectiondate;

	private String networkname;

	private String processingstatus;

	private String endpointstatusdescription;

	public ForwardCollection() {
		super();
		// TODO Auto-generated constructor stub
		creditaccountuuid = "";
		transactionuuid = "";
		networkname = "";
		transactiontype = "";
		debitorname = "";
		debitcurrency = "";
		amount = 0;
		accountreference = "";
		debitedaccount = "";
		originatetransactionuuid = "";
		forwarduri = "";
		processingstatus = "";
		endpointstatusdescription = "";
		collectiondate = new Date();

	}

	public String getCreditaccountuuid() {
		return creditaccountuuid;
	}

	public void setCreditaccountuuid(String creditaccountuuid) {
		this.creditaccountuuid = creditaccountuuid;
	}

	public String getTransactiontype() {
		return transactiontype;
	}

	public void setTransactiontype(String transactiontype) {
		this.transactiontype = transactiontype;
	}

	public String getDebitorname() {
		return debitorname;
	}

	public void setDebitorname(String debitorname) {
		this.debitorname = debitorname;
	}

	public String getDebitcurrency() {
		return debitcurrency;
	}

	public void setDebitcurrency(String debitcurrency) {
		this.debitcurrency = debitcurrency;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getAccountreference() {
		return accountreference;
	}

	public void setAccountreference(String accountreference) {
		this.accountreference = accountreference;
	}

	public String getDebitedaccount() {
		return debitedaccount;
	}

	public void setDebitedaccount(String debitedaccount) {
		this.debitedaccount = debitedaccount;
	}

	public String getOriginatetransactionuuid() {
		return originatetransactionuuid;
	}

	public void setOriginatetransactionuuid(String originatetransactionuuid) {
		this.originatetransactionuuid = originatetransactionuuid;
	}

	public String getForwarduri() {
		return forwarduri;
	}

	public void setForwarduri(String forwarduri) {
		this.forwarduri = forwarduri;
	}

	public String getTransactionuuid() {
		return transactionuuid;
	}

	public void setTransactionuuid(String transactionuuid) {
		this.transactionuuid = transactionuuid;
	}

	public Date getCollectiondate() {
		return collectiondate;
	}

	public void setCollectiondate(Date collectiondate) {
		this.collectiondate = collectiondate;
	}

	public String getNetworkname() {
		return networkname;
	}

	public void setNetworkname(String networkname) {
		this.networkname = networkname;
	}

	public String getProcessingstatus() {
		return processingstatus;
	}

	public void setProcessingstatus(String processingstatus) {
		this.processingstatus = processingstatus;
	}

	public String getEndpointstatusdescription() {
		return endpointstatusdescription;
	}

	public void setEndpointstatusdescription(String endpointstatusdescription) {
		this.endpointstatusdescription = endpointstatusdescription;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ForwardCollection [getUuid()=");
		builder.append(getUuid());
		builder.append(", creditaccountuuid=");
		builder.append(creditaccountuuid);
		builder.append(", transactiontype=");
		builder.append(transactiontype);
		builder.append(", debitorname=");
		builder.append(debitorname);
		builder.append(", debitcurrency=");
		builder.append(debitcurrency);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", accountreference=");
		builder.append(accountreference);
		builder.append(", debitedaccount=");
		builder.append(debitedaccount);
		builder.append(", originatetransactionuuid=");
		builder.append(originatetransactionuuid);
		builder.append(", forwarduri=");
		builder.append(forwarduri);
		builder.append(", transactionuuid=");
		builder.append(transactionuuid);
		builder.append(", collectiondate=");
		builder.append(collectiondate);
		builder.append(", networkname=");
		builder.append(networkname);
		builder.append(", processingstatus=");
		builder.append(processingstatus);
		builder.append(", endpointstatusdescription=");
		builder.append(endpointstatusdescription);
		builder.append("]");
		return builder.toString();
	}

}

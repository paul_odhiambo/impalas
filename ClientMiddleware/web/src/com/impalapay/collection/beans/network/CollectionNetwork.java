package com.impalapay.collection.beans.network;

import com.impalapay.airtel.beans.StorableBean;
import java.util.Date;

/**
 * Represents route networks can include mno or bank.
 * <p>
 * Copyright (c) ImpalaPay LTD., Feb 14, 2015
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */

public class CollectionNetwork extends StorableBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1066386942978511399L;

	private String networkname;

	private String collectiontchannel;

	private String providername;

	private String addedbyuuid;

	private String queryingurl;

	private String bridgequeryurl;

	private String debiturl;

	private String bridgedebiturl;

	private String username;

	private String password;

	private String countryuuid;

	private String instruction;

	private String balanceurl;

	private String bridgebalanceurl;

	private String reversalurl;

	private String bridgereversalurl;

	private String forexurl;

	private String bridgeforexurl;

	private String accountcheckurl;

	private String bridgeaccountcheckurl;

	private String extraurl;

	private boolean supportreversal;
	private boolean supportaccountcheck;
	private boolean supportbalancecheck;
	private boolean supportlisteningurl;
	private boolean supportquerying;
	private boolean supportdebit;

	private Date dateadded;

	public CollectionNetwork() {
		super();

		networkname = "";

		collectiontchannel = "";

		providername = "";

		addedbyuuid = "";

		queryingurl = "";

		bridgequeryurl = "";

		debiturl = "";

		bridgedebiturl = "";

		username = "";

		password = "";

		countryuuid = "";

		instruction = "";

		balanceurl = "";

		bridgebalanceurl = "";

		reversalurl = "";

		bridgereversalurl = "";

		forexurl = "";

		bridgeforexurl = "";

		accountcheckurl = "";

		bridgeaccountcheckurl = "";

		extraurl = "";

		supportreversal = false;

		supportaccountcheck = false;

		supportbalancecheck = false;

		supportlisteningurl = false;

		supportquerying = false;

		supportdebit = false;

		dateadded = new Date();
	}

	public String getNetworkname() {
		return networkname;
	}

	public void setNetworkname(String networkname) {
		this.networkname = networkname;
	}

	public String getCollectiontchannel() {
		return collectiontchannel;
	}

	public void setCollectiontchannel(String collectiontchannel) {
		this.collectiontchannel = collectiontchannel;
	}

	public String getProvidername() {
		return providername;
	}

	public void setProvidername(String providername) {
		this.providername = providername;
	}

	public String getAddedbyuuid() {
		return addedbyuuid;
	}

	public void setAddedbyuuid(String addedbyuuid) {
		this.addedbyuuid = addedbyuuid;
	}

	public String getQueryingurl() {
		return queryingurl;
	}

	public void setQueryingurl(String queryingurl) {
		this.queryingurl = queryingurl;
	}

	public String getBridgequeryurl() {
		return bridgequeryurl;
	}

	public void setBridgequeryurl(String bridgequeryurl) {
		this.bridgequeryurl = bridgequeryurl;
	}

	public String getDebiturl() {
		return debiturl;
	}

	public void setDebiturl(String debiturl) {
		this.debiturl = debiturl;
	}

	public String getBridgedebiturl() {
		return bridgedebiturl;
	}

	public void setBridgedebiturl(String bridgedebiturl) {
		this.bridgedebiturl = bridgedebiturl;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCountryuuid() {
		return countryuuid;
	}

	public void setCountryuuid(String countryuuid) {
		this.countryuuid = countryuuid;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public String getBalanceurl() {
		return balanceurl;
	}

	public void setBalanceurl(String balanceurl) {
		this.balanceurl = balanceurl;
	}

	public String getBridgebalanceurl() {
		return bridgebalanceurl;
	}

	public void setBridgebalanceurl(String bridgebalanceurl) {
		this.bridgebalanceurl = bridgebalanceurl;
	}

	public String getReversalurl() {
		return reversalurl;
	}

	public void setReversalurl(String reversalurl) {
		this.reversalurl = reversalurl;
	}

	public String getBridgereversalurl() {
		return bridgereversalurl;
	}

	public void setBridgereversalurl(String bridgereversalurl) {
		this.bridgereversalurl = bridgereversalurl;
	}

	public String getForexurl() {
		return forexurl;
	}

	public void setForexurl(String forexurl) {
		this.forexurl = forexurl;
	}

	public String getBridgeforexurl() {
		return bridgeforexurl;
	}

	public void setBridgeforexurl(String bridgeforexurl) {
		this.bridgeforexurl = bridgeforexurl;
	}

	public String getAccountcheckurl() {
		return accountcheckurl;
	}

	public void setAccountcheckurl(String accountcheckurl) {
		this.accountcheckurl = accountcheckurl;
	}

	public String getBridgeaccountcheckurl() {
		return bridgeaccountcheckurl;
	}

	public void setBridgeaccountcheckurl(String bridgeaccountcheckurl) {
		this.bridgeaccountcheckurl = bridgeaccountcheckurl;
	}

	public String getExtraurl() {
		return extraurl;
	}

	public void setExtraurl(String extraurl) {
		this.extraurl = extraurl;
	}

	public boolean isSupportreversal() {
		return supportreversal;
	}

	public void setSupportreversal(boolean supportreversal) {
		this.supportreversal = supportreversal;
	}

	public boolean isSupportaccountcheck() {
		return supportaccountcheck;
	}

	public void setSupportaccountcheck(boolean supportaccountcheck) {
		this.supportaccountcheck = supportaccountcheck;
	}

	public boolean isSupportbalancecheck() {
		return supportbalancecheck;
	}

	public void setSupportbalancecheck(boolean supportbalancecheck) {
		this.supportbalancecheck = supportbalancecheck;
	}

	public boolean isSupportlisteningurl() {
		return supportlisteningurl;
	}

	public void setSupportlisteningurl(boolean supportlisteningurl) {
		this.supportlisteningurl = supportlisteningurl;
	}

	public boolean isSupportquerying() {
		return supportquerying;
	}

	public void setSupportquerying(boolean supportquerying) {
		this.supportquerying = supportquerying;
	}

	public boolean isSupportdebit() {
		return supportdebit;
	}

	public void setSupportdebit(boolean supportdebit) {
		this.supportdebit = supportdebit;
	}

	public Date getDateadded() {
		return dateadded;
	}

	public void setDateadded(Date dateadded) {
		this.dateadded = dateadded;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CollectionNetwork [getUuid()=");
		builder.append(getUuid());
		builder.append(", networkname=");
		builder.append(networkname);
		builder.append(", collectiontchannel=");
		builder.append(collectiontchannel);
		builder.append(", providername=");
		builder.append(providername);
		builder.append(", addedbyuuid=");
		builder.append(addedbyuuid);
		builder.append(", queryingurl=");
		builder.append(queryingurl);
		builder.append(", bridgequeryurl=");
		builder.append(bridgequeryurl);
		builder.append(", debiturl=");
		builder.append(debiturl);
		builder.append(", bridgedebiturl=");
		builder.append(bridgedebiturl);
		builder.append(", username=");
		builder.append(username);
		builder.append(", password=");
		builder.append(password);
		builder.append(", countryuuid=");
		builder.append(countryuuid);
		builder.append(", instruction=");
		builder.append(instruction);
		builder.append(", balanceurl=");
		builder.append(balanceurl);
		builder.append(", bridgebalanceurl=");
		builder.append(bridgebalanceurl);
		builder.append(", reversalurl=");
		builder.append(reversalurl);
		builder.append(", bridgereversalurl=");
		builder.append(bridgereversalurl);
		builder.append(", forexurl=");
		builder.append(forexurl);
		builder.append(", bridgeforexurl=");
		builder.append(bridgeforexurl);
		builder.append(", accountcheckurl=");
		builder.append(accountcheckurl);
		builder.append(", bridgeaccountcheckurl=");
		builder.append(bridgeaccountcheckurl);
		builder.append(", extraurl=");
		builder.append(extraurl);
		builder.append(", supportreversal=");
		builder.append(supportreversal);
		builder.append(", supportaccountcheck=");
		builder.append(supportaccountcheck);
		builder.append(", supportbalancecheck=");
		builder.append(supportbalancecheck);
		builder.append(", supportlisteningurl=");
		builder.append(supportlisteningurl);
		builder.append(", supportquerying=");
		builder.append(supportquerying);
		builder.append(", supportdebit=");
		builder.append(supportdebit);
		builder.append(", dateadded=");
		builder.append(dateadded);
		builder.append("]");
		return builder.toString();
	}

}

package com.impalapay.collection.beans.network;

import com.impalapay.airtel.beans.StorableBean;

public class CollectionType extends StorableBean {

	private String type;

	public CollectionType() {
		super();
		// TODO Auto-generated constructor stub
		type = "";
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CollectionType [getUuid()=");
		builder.append(getUuid());
		builder.append(", type=");
		builder.append(type);
		builder.append("]");
		return builder.toString();
	}

}

package com.impalapay.collection.beans.balance;

import com.impalapay.airtel.beans.StorableBean;

/**
 * A generic balance of account
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class CollectionBalance extends StorableBean {

	private String accountuuid;
	private String countryuuid;
	private double balance;

	/**
	 * 
	 */
	public CollectionBalance() {
		super();

		balance = 0;
		accountuuid = "";
		countryuuid = "";

	}

	public String getAccountuuid() {
		return accountuuid;
	}

	public void setAccountuuid(String accountuuid) {
		this.accountuuid = accountuuid;
	}

	public String getCountryuuid() {
		return countryuuid;
	}

	public void setCountryuuid(String countryuuid) {
		this.countryuuid = countryuuid;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CollectionBalance [getUuid()=");
		builder.append(getUuid());
		builder.append(", accountuuid=");
		builder.append(accountuuid);
		builder.append(", countryuuid=");
		builder.append(countryuuid);
		builder.append(", balance=");
		builder.append(balance);
		builder.append("]");
		return builder.toString();
	}
}

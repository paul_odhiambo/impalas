package com.impalapay.collection.beans.balance;

import java.util.Date;

import com.impalapay.airtel.beans.StorableBean;

public class CashWithdrawal extends StorableBean {

	private String accountuuid;
	private String currencyUuid;
	private String tocurrencyUuid;
	private double amount;
	private String extrainformation;
	private double systemexchangerate;
	private double comission;
	private String comissioncurrencyUuid;
	private String adminextrainformation;
	private String transactionStatusUuid;
	private double bankwithdrawexchangerate;
	private String authorisedchecker;
	private String authorisedmaker;
	private double transfercharges;
	private String transferchargecurrencyUuid;
	private double receivableamount;
	private Date transactionDate;

	public CashWithdrawal() {
		super();
		// TODO Auto-generated constructor stub
		accountuuid = "";
		currencyUuid = "";
		tocurrencyUuid = "";
		amount = 0;
		extrainformation = "";
		systemexchangerate = 0;
		comission = 0;
		comissioncurrencyUuid = "";
		adminextrainformation = "";
		transactionStatusUuid = "";
		bankwithdrawexchangerate = 0;
		authorisedchecker = "";
		authorisedmaker = "";
		transfercharges = 0;
		transferchargecurrencyUuid = "";
		receivableamount = 0;
		transactionDate = new Date();
	}

	public String getAccountuuid() {
		return accountuuid;
	}

	public void setAccountuuid(String accountuuid) {
		this.accountuuid = accountuuid;
	}

	public String getCurrencyUuid() {
		return currencyUuid;
	}

	public void setCurrencyUuid(String currencyUuid) {
		this.currencyUuid = currencyUuid;
	}

	public String getTocurrencyUuid() {
		return tocurrencyUuid;
	}

	public void setTocurrencyUuid(String tocurrencyUuid) {
		this.tocurrencyUuid = tocurrencyUuid;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getExtrainformation() {
		return extrainformation;
	}

	public void setExtrainformation(String extrainformation) {
		this.extrainformation = extrainformation;
	}

	public double getSystemexchangerate() {
		return systemexchangerate;
	}

	public void setSystemexchangerate(double systemexchangerate) {
		this.systemexchangerate = systemexchangerate;
	}

	public double getComission() {
		return comission;
	}

	public void setComission(double comission) {
		this.comission = comission;
	}

	public String getComissioncurrencyUuid() {
		return comissioncurrencyUuid;
	}

	public void setComissioncurrencyUuid(String comissioncurrencyUuid) {
		this.comissioncurrencyUuid = comissioncurrencyUuid;
	}

	public String getAdminextrainformation() {
		return adminextrainformation;
	}

	public void setAdminextrainformation(String adminextrainformation) {
		this.adminextrainformation = adminextrainformation;
	}

	public String getTransactionStatusUuid() {
		return transactionStatusUuid;
	}

	public void setTransactionStatusUuid(String transactionStatusUuid) {
		this.transactionStatusUuid = transactionStatusUuid;
	}

	public double getBankwithdrawexchangerate() {
		return bankwithdrawexchangerate;
	}

	public void setBankwithdrawexchangerate(double bankwithdrawexchangerate) {
		this.bankwithdrawexchangerate = bankwithdrawexchangerate;
	}

	public String getAuthorisedchecker() {
		return authorisedchecker;
	}

	public void setAuthorisedchecker(String authorisedchecker) {
		this.authorisedchecker = authorisedchecker;
	}

	public String getAuthorisedmaker() {
		return authorisedmaker;
	}

	public void setAuthorisedmaker(String authorisedmaker) {
		this.authorisedmaker = authorisedmaker;
	}

	public double getTransfercharges() {
		return transfercharges;
	}

	public void setTransfercharges(double transfercharges) {
		this.transfercharges = transfercharges;
	}

	public String getTransferchargecurrencyUuid() {
		return transferchargecurrencyUuid;
	}

	public void setTransferchargecurrencyUuid(String transferchargecurrencyUuid) {
		this.transferchargecurrencyUuid = transferchargecurrencyUuid;
	}

	public double getReceivableamount() {
		return receivableamount;
	}

	public void setReceivableamount(double receivableamount) {
		this.receivableamount = receivableamount;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CashWithdrawal [getUuid()=");
		builder.append(getUuid());
		builder.append(", accountuuid=");
		builder.append(accountuuid);
		builder.append(", currencyUuid=");
		builder.append(currencyUuid);
		builder.append(", tocurrencyUuid=");
		builder.append(tocurrencyUuid);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", extrainformation=");
		builder.append(extrainformation);
		builder.append(", systemexchangerate=");
		builder.append(systemexchangerate);
		builder.append(", comission=");
		builder.append(comission);
		builder.append(", comissioncurrencyUuid=");
		builder.append(comissioncurrencyUuid);
		builder.append(", adminextrainformation=");
		builder.append(adminextrainformation);
		builder.append(", transactionStatusUuid=");
		builder.append(transactionStatusUuid);
		builder.append(", bankwithdrawexchangerate=");
		builder.append(bankwithdrawexchangerate);
		builder.append(", authorisedchecker=");
		builder.append(authorisedchecker);
		builder.append(", authorisedmaker=");
		builder.append(authorisedmaker);
		builder.append(", transfercharges=");
		builder.append(transfercharges);
		builder.append(", transferchargecurrencyUuid=");
		builder.append(transferchargecurrencyUuid);
		builder.append(", receivableamount=");
		builder.append(receivableamount);
		builder.append(", transactionDate=");
		builder.append(transactionDate);
		builder.append("]");
		return builder.toString();
	}

}

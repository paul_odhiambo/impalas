package com.impalapay.collection.persistence.accountmgmt.inprogressbalance;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressBalancebyCountryHoldHist;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressMasterBalanceHoldHist;

/**
 * Tests the {@link AccountBalanceDAO}
 * <p>
 * Copyright (c) ImpalaPay LTD., Sep 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class TestInProgressBalanceDAO {

	final String DB_NAME = "remittancedb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "remittance";
	final String DB_PASSWD = "gertedNen2";
	final int DB_PORT = 5432;

	// Account holders' Uuids
	public static final String DEMO = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	public static final String KAKUZI = "48e249c2-856a-4269-820f-7b72c76b4957";
	public static final String BLUE_TRIANGLE = "81bf3078-4495-4bec-a50d-c91a7c512d78";
	public static final String SOFT_TOUCH = "7967107d-d61c-43dd-bc5b-aa12fd08497b";
	public static final String MOBISOKO = "91fc8aae-cb76-4c64-ac45-48448fb5673f";

	final String CLIENTBALANCE_UUID = "61a86ead-98a4-4bc6-b00f-3028e61abc69";
	final String CLIENTBALANCE_ACCOUNT_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final int CLIENTBALANCE_AMOUNT = 48_079_021;

	final int ALL_CLIENTS_BALANCE_COUNT = 150;
	final double AMOUNT = 10000;
	final int AMOUNT2 = 46271257;

	private InProgressBalanceDAO storage = new InProgressBalanceDAO(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

	// test add master floaty on hold table.
	@Ignore
	@Test
	public void testPutMasterPurchaseHold() {
		InProgressMasterBalanceHoldHist masterPurchase = new InProgressMasterBalanceHoldHist();

		InProgressBalancebyCountryHoldHist balancebycountry = new InProgressBalancebyCountryHoldHist();

		masterPurchase.setUuid(UUID.randomUUID().toString());
		masterPurchase.setAccountUuid("6840787bab314c319ce8d4f3b4e34c69");
		masterPurchase.setCurrency("KES");
		masterPurchase.setAmount(1000);
		masterPurchase.setTopuptime(new Date());
		masterPurchase.setTransactionuuid("3ba7d51a363c44c1b650222afbcdbc6d");
		masterPurchase.setRefundedback(false);
		masterPurchase.setProcessed(false);

		balancebycountry.setUuid(UUID.randomUUID().toString());
		balancebycountry.setAccountUuid("6840787bab314c319ce8d4f3b4e34c69");
		balancebycountry.setCountryuuid("d4a676822f4546a0bee789e83070f788");
		balancebycountry.setAmount(1000);
		balancebycountry.setTopuptime(new Date());
		balancebycountry.setTransactionuuid("3ba7d51a363c44c1b650222afbcdbc6d");
		balancebycountry.setRefundedback(false);
		balancebycountry.setProcessed(false);

		// assertTrue(storage.putBalanceOnHoldAccount(masterPurchase,balancebycountry));
	}

	@Ignore
	@Test
	public void testremoveMasterBalanceHoldSuccess() {
		String transactionuuid = "3ba7d51a363c44c1b650222afbcdbc6d";

		assertTrue(storage.removeBalanceHoldFail(transactionuuid));
	}

	/// @Ignore
	@Test
	public void testgetbalancebycountryhold() {

		int from = 0;

		int to = 4;

		// System.out.println(storage.getAllBalancebyCountryHold(from, to));
	}

}

package com.impalapay.collection.persistence.cashwithdraw;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.collection.beans.balance.CashWithdrawal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

public class CheckerWithdrawDAOImpl extends GenericDAO implements CheckerWithdrawDAO {

	public static CheckerWithdrawDAOImpl clientwithdrawDAOImpl;

	private Logger logger;

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return {@link TransactionDAO}
	 */
	public static CheckerWithdrawDAOImpl getInstance() {

		if (clientwithdrawDAOImpl == null) {
			clientwithdrawDAOImpl = new CheckerWithdrawDAOImpl();
		}

		return clientwithdrawDAOImpl;
	}

	/**
	 * 
	 */
	public CheckerWithdrawDAOImpl() {
		super();

		logger = Logger.getLogger(this.getClass());
	}

	/**
	 * 
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public CheckerWithdrawDAOImpl(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

		logger = Logger.getLogger(this.getClass());
	}

	@Override
	public boolean putCheckerWithdraw(CashWithdrawal collectionbalance) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO checker_withdrawal(uuid,accountUuid,currencyUuid,tocurrencyUuid,amount,extrainformation,systemexchangerate,comission,comissioncurrencyuuid,adminextrainformation,authorisedchecker,transactionDate) VALUES (?, ?,?,?,?,?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, collectionbalance.getUuid());
			pstmt.setString(2, collectionbalance.getAccountuuid());
			pstmt.setString(3, collectionbalance.getCurrencyUuid());
			pstmt.setString(4, collectionbalance.getTocurrencyUuid());
			pstmt.setDouble(5, collectionbalance.getAmount());
			pstmt.setString(6, collectionbalance.getExtrainformation());
			pstmt.setDouble(7, collectionbalance.getSystemexchangerate());
			pstmt.setDouble(8, collectionbalance.getComission());
			pstmt.setString(9, collectionbalance.getComissioncurrencyUuid());
			pstmt.setString(10, collectionbalance.getAdminextrainformation());
			pstmt.setString(11, collectionbalance.getAuthorisedchecker());
			pstmt.setTimestamp(12, new Timestamp(collectionbalance.getTransactionDate().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + collectionbalance);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

			System.out.println(ExceptionUtils.getStackTrace(e));

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<CashWithdrawal> getAllCheckerWithdraw(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		List<CashWithdrawal> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn
					.prepareStatement("SELECT * FROM checker_withdrawal ORDER BY accountuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CashWithdrawal.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all checker_withdrawal from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public boolean deleteCheckerWithdraw(String uuid) {
		boolean success = true;
		Date date;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("DELETE FROM checker_withdrawal WHERE uuid=?;");

			pstmt.setString(1, uuid);

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while Deleting " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public CashWithdrawal getCheckerWithdraw(String uuid) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		CashWithdrawal s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM checker_withdrawal WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, CashWithdrawal.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting checker_withdrawal with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

}

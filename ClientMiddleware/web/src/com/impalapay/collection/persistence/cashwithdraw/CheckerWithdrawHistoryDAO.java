package com.impalapay.collection.persistence.cashwithdraw;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.collection.beans.balance.CashWithdrawal;

/**
 * Abstraction for persistence of balance in an the master account.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public interface CheckerWithdrawHistoryDAO {

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public CashWithdrawal getWithdrawalHistoryTransaction(String uuid);

	/**
	 * 
	 * @param transactionUuid
	 * @param transactionstatus
	 * @return
	 */
	boolean updateTransactionStatus(String transactionUuid, TransactionStatus transactionstatus);

	/**
	 * 
	 * @param collectionbalance
	 * @return
	 */
	boolean putCheckerWithdrawHistory(CashWithdrawal collectionbalance);

	/***
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CashWithdrawal> getAllCheckerWithdrawHistory(int fromIndex, int toIndex);

	public List<CashWithdrawal> getAllCheckerWithdrawHistory(Account account, int fromIndex, int toIndex);

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CashWithdrawal> getAllInprogressWithdrawHistory(int fromIndex, int toIndex);

}

package com.impalapay.collection.persistence.cashwithdraw;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.collection.beans.balance.CashWithdrawal;

/**
 * Abstraction for persistence of balance in an the master account.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public interface CheckerWithdrawDAO {

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	boolean deleteCheckerWithdraw(String uuid);

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public CashWithdrawal getCheckerWithdraw(String uuid);

	/**
	 * 
	 * @param collectionbalance
	 * @return
	 */
	boolean putCheckerWithdraw(CashWithdrawal collectionbalance);

	/***
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CashWithdrawal> getAllCheckerWithdraw(int fromIndex, int toIndex);

}

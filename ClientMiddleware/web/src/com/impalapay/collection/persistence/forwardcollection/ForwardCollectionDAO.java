package com.impalapay.collection.persistence.forwardcollection;

import java.util.List;

import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.collection.beans.incoming.ForwardCollection;

public interface ForwardCollectionDAO {

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public ForwardCollection getForwardColletion(String uuid);

	/**
	 * 
	 * @param proceesedtransaction
	 * @return
	 */
	public boolean putForwardColletion(ForwardCollection proceesedtransaction);

	/**
	 * 
	 * @param tempincomingtransaction
	 * @return
	 */
	public boolean putForwardColletionHistory(ForwardCollection tempincomingtransaction);

	/**
	 * 
	 * @param uuid
	 * @param proceesedtransaction
	 * @return
	 */

	boolean updateForwardColletion(String uuid, ForwardCollection proceesedtransaction);

	/**
	 * 
	 * @param sendertransactionid
	 * @return
	 */
	public List<ForwardCollection> getAllForwardColletion(String sendertransactionid);

	/**
	 * 
	 * @return
	 */
	public List<ForwardCollection> getAllForwardColletion();

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<ForwardCollection> getAllForwardColletion(int fromIndex, int toIndex);

	/**
	 * 
	 * @param name
	 * @return
	 */
	public ForwardCollection getForwardColletionstatus(String transactionstatusuuid);

	/**
	 * 
	 * @param originatetransactionuuid
	 * @return
	 */
	public List<ForwardCollection> getTransactionReference(String originatetransactionuuid);

	/**
	 * 
	 * @param transactionStatus
	 * @param limit
	 * @return
	 */
	public List<ForwardCollection> getForwardColletionByStatusUuid(int limit);

	/**
	 * 
	 * @param temptransactionuuidid
	 * @return
	 */
	public boolean deleteForwardCollection(String temptransactionuuidid);

}

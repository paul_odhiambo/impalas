package com.impalapay.collection.persistence.tempincoming;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.collection.beans.incoming.TempCollection;

public interface TempIncomingDAO {

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public TempCollection getTempIncomingTrans(String uuid);

	/**
	 * 
	 * @param tempincomingtransaction
	 * @return
	 */
	public boolean putTempIncomingTrans(TempCollection tempincomingtransaction);

	/**
	 * 
	 * @param uuid
	 * @param tempincomingtransaction
	 * @return
	 */

	boolean updateTempIncomingTrans(String uuid, TempCollection tempincomingtransaction);

	/**
	 * 
	 * @param sendertransactionid
	 * @return
	 */
	public List<TempCollection> getAllTempIncomingTrans(String sendertransactionid);

	/**
	 * 
	 * @return
	 */
	public List<TempCollection> getAllTempIncomingTrans();

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<TempCollection> getAllTempIncomingTrans(int fromIndex, int toIndex);

	/**
	 * 
	 * @param name
	 * @return
	 */
	public TempCollection getTempIncomingTranstatus(String transactionstatusuuid);

	/**
	 * 
	 * @param originatetransactionuuid
	 * @return
	 */
	public List<TempCollection> getTransactionReference(String originatetransactionuuid);

	/**
	 * 
	 * @param transactionStatus
	 * @param limit
	 * @return
	 */
	public List<TempCollection> getTempIncomingTranstatusByStatusUuid(TransactionStatus transactionStatus, int limit);

	/**
	 * 
	 * @param transactionStatus
	 * @param limit
	 * @return
	 */
	public List<TempCollection> getTempIncomingTranstatusByStatusUuidAutoSettlement(TransactionStatus transactionStatus,
			int limit);

	/**
	 * 
	 * @param temptransactionuuidid
	 * @return
	 */
	public boolean deleteTempTransaction(String temptransactionuuidid);

	/**
	 * 
	 * @param transactionUuid
	 * @param transactionstatus
	 * @return
	 */
	public boolean updateTempIncomingTransactionStatus(String transactionUuid, TransactionStatus transactionstatus);

	/**
	 * 
	 * @param referencenumber
	 * @param account
	 * @return
	 */
	public TempCollection getTransactionstatus(String referencenumber, Account account);

	/**
	 * 
	 * @param referencenumber
	 * @param transactionstatus
	 * @return
	 */
	public TempCollection getTransactionstatus(String referencenumber, TransactionStatus transactionstatus);

}

package com.impalapay.collection.persistence.balance;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.collection.beans.balance.CollectionBalanceHistory;

/**
 * Abstraction for persistence of balance in an the master account.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public interface CollectionBalanceDAO {

	/**
	 * 
	 * @param collectionbalance
	 * @return
	 */
	public boolean deductCollectionBalance(CollectionBalanceHistory collectionbalance);

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public CollectionBalanceHistory getCollectionBalance(String uuid);

	/**
	 * 
	 * @param collectionbalance
	 * @return
	 */
	boolean putCollectionBalance(CollectionBalanceHistory collectionbalance);

	/**
	 * 
	 * @param account
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CollectionBalanceHistory> getAllClientCollectionBalance(Account account, int fromIndex, int toIndex);

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CollectionBalanceHistory> getAllClientCollectionBalance(int fromIndex, int toIndex);

	/**
	 * 
	 * @param account
	 * @param fromindex
	 * @param toindex
	 * @return
	 */
	public List<CollectionBalanceHistory> getAllClientCollectionBalanceHistory(Account account, int fromindex,
			int toindex);

	/**
	 * 
	 * @param fromindex
	 * @param toindex
	 * @return
	 */
	public List<CollectionBalanceHistory> getAllClientCollectionBalanceHistory(int fromindex, int toindex);

}

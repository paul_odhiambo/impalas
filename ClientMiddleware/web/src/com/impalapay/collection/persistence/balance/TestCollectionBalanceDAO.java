package com.impalapay.collection.persistence.balance;

import com.impalapay.collection.beans.balance.CollectionBalanceHistory;

import java.util.Date;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

/**
 * Tests the {@link AccountBalanceDAO}
 * <p>
 * Copyright (c) ImpalaPay LTD., Sep 14, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class TestCollectionBalanceDAO {

	final String DB_NAME = "collectiondb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "collection";
	final String DB_PASSWD = "mopwojAbr5";
	final int DB_PORT = 5432;

	// Account holders' Uuids
	public static final String DEMO = "9756f889-811a-4a94-b13d-1c66c7655a7f";

	final String CLIENTBALANCE_UUID = "61a86ead-98a4-4bc6-b00f-3028e61abc69";
	final String CLIENTBALANCE_ACCOUNT_UUID = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String Country_UUID2 = "5db5fa02790e4ee0a8d7a538b4df820a";

	final int CLIENTBALANCE_AMOUNT = 48_079_021;

	final int ALL_CLIENTS_BALANCE_COUNT = 150;
	final double AMOUNT = 10000;
	final int AMOUNT2 = 46271257;

	private CollectionBalanceDAOImpl storage = new CollectionBalanceDAOImpl(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD,
			DB_PORT);

	// @Ignore
	@Test
	public void testCollectionBalance() {

		CollectionBalanceHistory newbalance = new CollectionBalanceHistory();

		newbalance.setUuid(CLIENTBALANCE_UUID);
		newbalance.setAccountuuid(DEMO);
		newbalance.setCountryuuid(Country_UUID2);
		newbalance.setAmount(20);
		newbalance.setTransactionuuid("eef9cb5603ab4ad788a2e8d0c3e89b1d");
		newbalance.setTopupTime(new Date());

		assertTrue(storage.putCollectionBalance(newbalance));
	}

}

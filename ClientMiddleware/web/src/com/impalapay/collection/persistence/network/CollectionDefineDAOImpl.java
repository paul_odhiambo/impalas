package com.impalapay.collection.persistence.network;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.collection.beans.network.CollectionDefine;

public class CollectionDefineDAOImpl extends GenericDAO implements CollectionDefineDAO {

	private static CollectionDefineDAOImpl collectionDefineDAOImpl;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return CountryMsisdnDAO
	 */
	public static CollectionDefineDAOImpl getInstance() {
		if (collectionDefineDAOImpl == null) {
			collectionDefineDAOImpl = new CollectionDefineDAOImpl();
		}

		return collectionDefineDAOImpl;
	}

	protected CollectionDefineDAOImpl() {
		super();
	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public CollectionDefineDAOImpl(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

	}

	@Override
	public CollectionDefine getCollectionRoute(String uuid) {
		CollectionDefine s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collectiondefine WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, CollectionDefine.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting collectiondefine with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public boolean PutCollectionDefine(CollectionDefine collectiondefine) {
		boolean success = true;
		Date date;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn
					.prepareStatement("INSERT INTO collectiondefine(uuid,accountuuid,collectionnetworksubaccountuuid"
							+ ",networkuuid,supportforex,fixedcommission,presettlement,addedbyuuid,commission,referenceprefix,listeninguri,"
							+ "dateadded) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, collectiondefine.getUuid());
			pstmt.setString(2, collectiondefine.getAccountuuid());
			pstmt.setString(3, collectiondefine.getCollectionnetworksubaccountuuid());
			pstmt.setString(4, collectiondefine.getNetworkuuid());
			pstmt.setBoolean(5, collectiondefine.isSupportforex());
			pstmt.setBoolean(6, collectiondefine.isFixedcommission());
			pstmt.setBoolean(7, collectiondefine.isPresettlement());
			pstmt.setString(8, collectiondefine.getAddedbyuuid());
			pstmt.setDouble(9, collectiondefine.getCommission());
			pstmt.setString(10, collectiondefine.getReferenceprefix());
			pstmt.setString(11, collectiondefine.getListeninguri());
			pstmt.setTimestamp(12, new Timestamp(collectiondefine.getDateadded().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + collectiondefine);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public boolean UpdateCollectionDefine(String uuid, CollectionDefine collectiondefine) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collectiondefine WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				pstmt2 = conn.prepareStatement(
						"UPDATE collectiondefine SET accountuuid=?,collectionnetworksubaccountuuid=?,fixedcommission=?,presettlement=?,addedbyuuid=?,commission=?,referenceprefix=?,listeninguri=?,"
								+ "dateadded=? WHERE uuid=?;");

				pstmt2.setString(1, collectiondefine.getAccountuuid());
				pstmt2.setString(2, collectiondefine.getCollectionnetworksubaccountuuid());
				pstmt2.setBoolean(3, collectiondefine.isFixedcommission());
				pstmt2.setBoolean(4, collectiondefine.isPresettlement());
				pstmt2.setString(5, collectiondefine.getAddedbyuuid());
				pstmt2.setDouble(6, collectiondefine.getCommission());
				pstmt2.setString(7, collectiondefine.getReferenceprefix());
				pstmt2.setString(8, collectiondefine.getListeninguri());
				pstmt2.setTimestamp(9, new Timestamp(collectiondefine.getDateadded().getTime()));
				pstmt2.setString(10, collectiondefine.getUuid());

				pstmt2.executeUpdate();

			} else {
				success = PutCollectionDefine(collectiondefine);
			}

		} catch (SQLException e) {
			logger.error("SQLException when trying to update collectiondefine with uuid '" + uuid + "' with "
					+ collectiondefine + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<CollectionDefine> getAllCollectionRoute() {
		List<CollectionDefine> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collectiondefine ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CollectionDefine.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all collectiondefine.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<CollectionDefine> getAllCollectionRoute(int fromIndex, int toIndex) {
		List<CollectionDefine> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collectiondefine ORDER BY dateadded DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CollectionDefine.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collectiondefine from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<CollectionDefine> getAllCollectionRoute(String uuid, int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	@Override
	public CollectionDefine getCollectionRouteUuid(String networksubaccountuuid, String routeprefix) {
		CollectionDefine s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM collectiondefine WHERE collectionnetworksubaccountuuid = ? AND referenceprefix=?;");
			pstmt.setString(1, networksubaccountuuid);
			pstmt.setString(2, routeprefix);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, CollectionDefine.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting collectiondefine with referenceprefix '" + routeprefix + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public CollectionDefine getCollectionRouteUuid(String networksubaccountuuid) {
		CollectionDefine s = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collectiondefine WHERE collectionnetworksubaccountuuid = ?;");
			pstmt.setString(1, networksubaccountuuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, CollectionDefine.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting collectiondefine with collectionnetworksubaccountuuid '"
					+ networksubaccountuuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public List<CollectionDefine> getAllCollectionRoute(Account account) {
		List<CollectionDefine> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collectiondefine WHERE accountuuid=? ORDER BY id ASC;");
			pstmt.setString(1, account.getUuid());

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CollectionDefine.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting specific collectiondefine.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public boolean deleteCollectionDefine(String uuid) {
		boolean success = true;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("DELETE FROM collectiondefine WHERE uuid=?;");

			pstmt.setString(1, uuid);

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while Deleting " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}
}

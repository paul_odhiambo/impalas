package com.impalapay.collection.persistence.network;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.collection.beans.network.CollectionNetwork;

public class CollectionNetworkDAOImpl extends GenericDAO implements CollectionNetworkDAO {
	private static CollectionNetworkDAOImpl collectionNetworkDAOImpl;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return CountryMsisdnDAO
	 */
	public static CollectionNetworkDAOImpl getInstance() {
		if (collectionNetworkDAOImpl == null) {
			collectionNetworkDAOImpl = new CollectionNetworkDAOImpl();
		}

		return collectionNetworkDAOImpl;
	}

	protected CollectionNetworkDAOImpl() {
		super();
	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public CollectionNetworkDAOImpl(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

	}

	public CollectionNetwork getNetwork(String uuid) {
		CollectionNetwork s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collection_network WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, CollectionNetwork.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting collectionnetwork with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	public boolean putNetwork(CollectionNetwork network) {
		boolean success = true;
		Date date;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("INSERT INTO collection_network(uuid,networkname,collectiontchannel,"
					+ "addedbyuuid,queryingurl,bridgequeryurl,debiturl,bridgedebiturl,"
					+ "username,password,supportdebit,supportquerying,supportlisteningurl,"
					+ "instruction,countryuuid,dateadded,providername,balanceurl,bridgebalanceurl,reversalurl,bridgereversalurl,forexurl,"
					+ "bridgeforexurl,accountcheckurl,bridgeaccountcheckurl,extraurl,supportreversal,supportaccountcheck,supportbalancecheck) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, network.getUuid());
			pstmt.setString(2, network.getNetworkname());
			pstmt.setString(3, network.getCollectiontchannel());
			pstmt.setString(4, network.getAddedbyuuid());
			pstmt.setString(5, network.getQueryingurl());
			pstmt.setString(6, network.getBridgequeryurl());
			pstmt.setString(7, network.getDebiturl());
			pstmt.setString(8, network.getBridgedebiturl());
			pstmt.setString(9, network.getUsername());
			pstmt.setString(10, network.getPassword());
			pstmt.setBoolean(11, network.isSupportdebit());
			pstmt.setBoolean(12, network.isSupportquerying());
			pstmt.setBoolean(13, network.isSupportlisteningurl());
			pstmt.setString(14, network.getInstruction());
			pstmt.setString(15, network.getCountryuuid());
			pstmt.setTimestamp(16, new Timestamp(network.getDateadded().getTime()));
			pstmt.setString(17, network.getProvidername());
			pstmt.setString(18, network.getBalanceurl());
			pstmt.setString(19, network.getBridgebalanceurl());
			pstmt.setString(20, network.getReversalurl());
			pstmt.setString(21, network.getBridgereversalurl());
			pstmt.setString(22, network.getForexurl());
			pstmt.setString(23, network.getBridgeforexurl());
			pstmt.setString(24, network.getAccountcheckurl());
			pstmt.setString(25, network.getBridgeaccountcheckurl());
			pstmt.setString(26, network.getExtraurl());
			pstmt.setBoolean(27, network.isSupportreversal());
			pstmt.setBoolean(28, network.isSupportaccountcheck());
			pstmt.setBoolean(29, network.isSupportbalancecheck());

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + network);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	public boolean updateNetwork(String uuid, CollectionNetwork network) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collection_network WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				pstmt2 = conn.prepareStatement(
						"UPDATE collection_network SET networkname=?,providername=?,collectiontchannel=?,addedbyuuid=?,queryingurl=?,bridgequeryurl=?,debiturl=?,bridgedebiturl=?,"
								+ "username=?,password=?,supportdebit=?,supportquerying=?"
								+ ",supportlisteningurl=?,instruction=?,countryuuid=?,dateadded=?"
								+ ",balanceurl=?,bridgebalanceurl=?,reversalurl=?,bridgereversalurl=?,forexurl=?,bridgeforexurl=?,accountcheckurl=?,bridgeaccountcheckurl=?,extraurl=?,supportreversal=?,supportaccountcheck=?,supportbalancecheck=?"
								+ "WHERE uuid=?;");

				pstmt2.setString(1, network.getNetworkname());
				pstmt2.setString(2, network.getProvidername());
				pstmt2.setString(3, network.getCollectiontchannel());
				pstmt2.setString(4, network.getAddedbyuuid());
				pstmt2.setString(5, network.getQueryingurl());
				pstmt2.setString(6, network.getBridgequeryurl());
				pstmt2.setString(7, network.getDebiturl());
				pstmt2.setString(8, network.getBridgedebiturl());
				pstmt2.setString(9, network.getUsername());
				pstmt2.setString(10, network.getPassword());
				pstmt2.setBoolean(11, network.isSupportdebit());
				pstmt2.setBoolean(12, network.isSupportquerying());
				pstmt2.setBoolean(13, network.isSupportlisteningurl());
				pstmt2.setString(14, network.getInstruction());
				pstmt2.setString(15, network.getCountryuuid());
				pstmt2.setTimestamp(16, new Timestamp(network.getDateadded().getTime()));
				pstmt2.setString(17, network.getBalanceurl());
				pstmt2.setString(18, network.getBridgebalanceurl());
				pstmt2.setString(19, network.getReversalurl());
				pstmt2.setString(20, network.getBridgereversalurl());
				pstmt2.setString(21, network.getForexurl());
				pstmt2.setString(22, network.getBridgeforexurl());
				pstmt2.setString(23, network.getAccountcheckurl());
				pstmt2.setString(24, network.getBridgeaccountcheckurl());
				pstmt2.setString(25, network.getExtraurl());
				pstmt2.setBoolean(26, network.isSupportreversal());
				pstmt2.setBoolean(27, network.isSupportaccountcheck());
				pstmt2.setBoolean(28, network.isSupportbalancecheck());
				pstmt2.setString(29, network.getUuid());

				pstmt2.executeUpdate();

			} else {
				success = putNetwork(network);
			}

		} catch (SQLException e) {
			logger.error("SQLException when trying to update collection_network with uuid '" + uuid + "' with "
					+ network + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	public List<CollectionNetwork> getAllNetwork() {
		List<CollectionNetwork> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collection_network ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CollectionNetwork.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all collection_network.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	public List<CollectionNetwork> getAllNetwork(int fromIndex, int toIndex) {
		List<CollectionNetwork> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collection_network ORDER BY dateadded DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CollectionNetwork.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collection_network from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	public CollectionNetwork getNetwork(CollectionNetwork name) {
		CollectionNetwork s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM collection_network WHERE networkname = ?;");
			pstmt.setString(1, name.getNetworkname());

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, CollectionNetwork.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting collection_network with uuid '" + name.getUuid() + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	public List<CollectionNetwork> getAllNetwork(String uuid, int fromIndex, int toIndex) {
		List<CollectionNetwork> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM collection_network WHERE uuid=? ORDER BY dateadded DESC LIMIT ? OFFSET ?;");
			pstmt.setString(1, uuid);
			pstmt.setInt(2, toIndex - fromIndex);
			pstmt.setInt(3, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, CollectionNetwork.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all collection_network from index " + fromIndex
					+ " to index by uuid " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

}

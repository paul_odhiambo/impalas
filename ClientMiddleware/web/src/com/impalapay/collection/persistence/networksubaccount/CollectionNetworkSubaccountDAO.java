package com.impalapay.collection.persistence.networksubaccount;

import java.util.List;

import com.impalapay.collection.beans.network.CollectionNetworkSubaccount;

public interface CollectionNetworkSubaccountDAO {

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public CollectionNetworkSubaccount getCollectionNetworkSubAcct(String uuid);

	/**
	 * 
	 * @param collecnetworkuuid
	 * @param collectionnumber
	 * @return
	 */
	public CollectionNetworkSubaccount getCollectionNetworkSubAcct(String collecnetworkuuid, String collectionnumber);

	/**
	 * 
	 * @param networksubaccount
	 * @return
	 */
	public boolean putNetworkSubaccount(CollectionNetworkSubaccount networksubaccount);

	/**
	 * 
	 * @param uuid
	 * @param networksubaccount
	 * @return
	 */

	boolean updateNetworkSubaccount(String uuid, CollectionNetworkSubaccount networksubaccount);

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	boolean deleteNetworkSubaccount(String uuid);

	/**
	 * 
	 * @return
	 */
	public List<CollectionNetworkSubaccount> getAllNetworkSubaccount();

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<CollectionNetworkSubaccount> getAllNetworkSubaccount(int fromIndex, int toIndex);

	/**
	 * 
	 * @param name
	 * @return
	 */
	public CollectionNetworkSubaccount getNetworkSubaccount(CollectionNetworkSubaccount name);

}

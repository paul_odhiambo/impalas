package com.impalapay.collection.persistence.processedtransactions;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.collection.beans.incoming.ProcessedCollection;

public interface ProcessedTransactionDAO {

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public ProcessedCollection getProcessedTrans(String uuid);

	/**
	 * 
	 * @param proceesedtransaction
	 * @return
	 */
	public boolean putProcessedTrans(ProcessedCollection proceesedtransaction);

	/**
	 * 
	 * @param uuid
	 * @param proceesedtransaction
	 * @return
	 */

	boolean updateProcessedTrans(String uuid, ProcessedCollection proceesedtransaction);

	/**
	 * 
	 * @param sendertransactionid
	 * @return
	 */
	public List<ProcessedCollection> getAllProcessedTrans(String sendertransactionid);

	/**
	 * 
	 * @return
	 */
	public List<ProcessedCollection> getAllProcessedTrans();

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<ProcessedCollection> getAllProcessedTrans(int fromIndex, int toIndex);

	/**
	 * 
	 * @param name
	 * @return
	 */
	public ProcessedCollection getProcessedTranstatus(String transactionstatusuuid);

	/**
	 * 
	 * @param originatetransactionuuid
	 * @return
	 */
	public List<ProcessedCollection> getTransactionReference(String originatetransactionuuid);

	/**
	 * 
	 * @param transactionStatus
	 * @param limit
	 * @return
	 */
	public List<ProcessedCollection> getProcessedTranstatusByStatusUuid(TransactionStatus transactionStatus, int limit);

	/**
	 * 
	 * @param referencenumber
	 * @param account
	 * @return
	 */
	public List<ProcessedCollection> getTransactionstatus(String referencenumber, Account account);

	/**
	 * 
	 * @param referencenumber
	 * @param account
	 * @return
	 */
	public ProcessedCollection getTransactionstatus1(String referencenumber, Account account);

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<ProcessedCollection> getAllProcessedTrans(Account account, int fromIndex, int toIndex);

}

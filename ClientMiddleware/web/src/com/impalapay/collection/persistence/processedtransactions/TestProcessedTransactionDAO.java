package com.impalapay.collection.persistence.processedtransactions;

import static org.junit.Assert.*;

import org.junit.Test;

import com.impalapay.collection.beans.incoming.TempCollection;
import com.impalapay.collection.beans.network.CollectionNetworkSubaccount;

import org.junit.Ignore;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Tests the com.impalapay.airtel.persistence.country.CountryDAO
 * <p>
 * Copyright (c) impalapay Ltd., june 24, 2014
 * 
 * @author <a href="mailto:eugenechimita@impalapay.com">Eugene Chimita</a>
 * @author <a href="mailto:michael@impalapay.com">Michael Wakahe</a>
 * 
 */
public class TestProcessedTransactionDAO {

	final String DB_NAME = "collectiondb";
	final String DB_HOST = "localhost";
	final String DB_USERNAME = "collection";
	final String DB_PASSWD = "mopwojAbr5";
	final int DB_PORT = 5432;

	final String Balance_IP = "20";
	final String UUID = "81bf3078-4495-4bec-a50d-c91a7c512d78";

	final String Country_UUID = "d4a676822f4546a0bee789e83070f788";
	final String Remit_Ip = "1234646";
	final String Query_Ip = "1234646";
	final String Balance_Ip = "1234646";
	final String Reversal_Ip = "1234646";
	final String Forex_Ip = "1234646";
	final String Accountcheck_Ip = "1234646";
	final String extra_Url = "1234646";
	final String username = "njkljlk";
	final String password = "njkljlk";
	final String partnername = "njkljlk";
	final boolean supportforex = true;
	final boolean supportreversal = true;
	final boolean supportaccountcheck = true;

	final String Account_UUID2 = "9756f889-811a-4a94-b13d-1c66c7655a7f";
	final String UUID2 = "3ec83cb1-b030-44be-a8bc-0df73d0628bf";
	final String Country_MSISDN2 = "25473348678";
	final String Country_UUID2 = "5db5fa02790e4ee0a8d7a538b4df820a";

	final int Country_COUNT = 17;

	private ProcessedTransactionDAOImpl storage;

	/**
	 * Test method for
	 * com.impalapay.airtel.persistence.country.CountryDAO#getCountry(java.lang.
	 * String).
	 */
	@Ignore
	@Test
	public void testNetworkString() {
		storage = new ProcessedTransactionDAOImpl(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

	}

	// @Ignore
	@Test
	public void testPutTempTrans() {
		storage = new ProcessedTransactionDAOImpl(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);
		TempCollection tempincomingtransaction = new TempCollection();

		tempincomingtransaction.setUuid("12878eretrtet");
		tempincomingtransaction.setNetworkuuid("52365263526356");
		tempincomingtransaction.setCreditaccountuuid("9756f889-811a-4a94-b13d-1c66c7655a7f");
		tempincomingtransaction.setAmount(40);
		// tempincomingtransaction.setSendername("eugene chimita");
		// tempincomingtransaction.setCurrency("KES");
		// tempincomingtransaction.setOriginateaccount("0715290374");
		tempincomingtransaction.setOriginatetransactionuuid("12345556");
		tempincomingtransaction.setAccountreference("IPL1233443");
		tempincomingtransaction.setVendorunique("");
		tempincomingtransaction.setTransactionstatusuuid("5c9b8b0b-a035-4a07-bfd8-eccd4f039d53");
		// tempincomingtransaction.setCommission(20);
		// tempincomingtransaction.setPresettlement(true);
		tempincomingtransaction.setServertime(new Date());

		// assertTrue(storage.putTempIncomingTrans(tempincomingtransaction));

	}

	@Ignore
	@Test
	public void testUpdateNetwork() {
		storage = new ProcessedTransactionDAOImpl(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

		// assertTrue(storage.updateNetworkSubaccount("gfgfgt456656565", network));

	}

	@Ignore
	@Test
	public void testgetCollectionAccount() {
		storage = new ProcessedTransactionDAOImpl(DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWD, DB_PORT);

	}

}

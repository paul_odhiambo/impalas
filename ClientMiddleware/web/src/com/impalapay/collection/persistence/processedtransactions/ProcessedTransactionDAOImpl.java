package com.impalapay.collection.persistence.processedtransactions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.collection.beans.incoming.ProcessedCollection;

public class ProcessedTransactionDAOImpl extends GenericDAO implements ProcessedTransactionDAO {

	private static ProcessedTransactionDAOImpl processedDAOImpl;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return
	 */
	public static ProcessedTransactionDAOImpl getInstance() {
		if (processedDAOImpl == null) {
			processedDAOImpl = new ProcessedTransactionDAOImpl();
		}

		return processedDAOImpl;
	}

	protected ProcessedTransactionDAOImpl() {
		super();
	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public ProcessedTransactionDAOImpl(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

	}

	@Override
	public ProcessedCollection getProcessedTrans(String uuid) {
		ProcessedCollection s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM processedtransaction WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, ProcessedCollection.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting processedtransaction with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	@Override
	public boolean putProcessedTrans(ProcessedCollection tempincomingtransaction) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO processedtransaction(uuid,networkuuid,creditaccountuuid,transactiontype,debitorname,"
							+ "debitcurrency,debitcountry,amount,accountreference,transactionstatusuuid,debitedaccount,originatetransactionuuid,receivertransactionuuid,"
							+ "vendorunique,originateamount,originatecurrency,endpointstatusdescription,commission,serverTime) VALUES (?, ?, ?, ?, ?, ?, ? ,? ,? ,? ,? ,?, ?, ?, ?, ?, ?, ?, ?);");
			pstmt.setString(1, tempincomingtransaction.getUuid());
			pstmt.setString(2, tempincomingtransaction.getNetworkuuid());
			pstmt.setString(3, tempincomingtransaction.getCreditaccountuuid());
			pstmt.setString(4, tempincomingtransaction.getTransactiontype());
			pstmt.setString(5, tempincomingtransaction.getDebitorname());
			pstmt.setString(6, tempincomingtransaction.getDebitcurrency());
			pstmt.setString(7, tempincomingtransaction.getDebitcountry());
			pstmt.setDouble(8, tempincomingtransaction.getAmount());
			pstmt.setString(9, tempincomingtransaction.getAccountreference());
			pstmt.setString(10, tempincomingtransaction.getTransactionstatusuuid());
			pstmt.setString(11, tempincomingtransaction.getDebitedaccount());
			pstmt.setString(12, tempincomingtransaction.getOriginatetransactionuuid());
			pstmt.setString(13, tempincomingtransaction.getReceivertransactionuuid());
			pstmt.setString(14, tempincomingtransaction.getVendorunique());
			pstmt.setDouble(15, tempincomingtransaction.getOriginateamount());
			pstmt.setString(16, tempincomingtransaction.getOriginatecurrency());
			pstmt.setString(17, tempincomingtransaction.getEndpointstatusdescription());
			pstmt.setDouble(18, tempincomingtransaction.getCommission());
			pstmt.setTimestamp(19, new Timestamp(tempincomingtransaction.getServertime().getTime()));

			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + tempincomingtransaction);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

			System.out.println(ExceptionUtils.getStackTrace(e));

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public boolean updateProcessedTrans(String uuid, ProcessedCollection tempincomingtransaction) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM processedtransaction WHERE uuid=?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {

				// System.out.println("executing second stage");
				pstmt2 = conn.prepareStatement(
						"UPDATE processedtransaction SET collectionnetworkuuid=?,networksubaccountuuid=?,"
								+ "creditaccountuuid=?,amount=?,sendername=?,currency=?,originateaccount=?,originatetransactionuuid=?,"
								+ "accountreference=?,vendorunique=?,transactionstatusuuid=?,commission=?,presettlement=?,serverTime=?"
								+ "WHERE uuid=?;");

				// pstmt2.setString(1, tempincomingtransaction.getCollectionnetworkuuid());
				// pstmt2.setString(2, tempincomingtransaction.getNetworksubaccountuuid());
				pstmt2.setString(3, tempincomingtransaction.getCreditaccountuuid());
				pstmt2.setDouble(4, tempincomingtransaction.getAmount());
				// pstmt2.setString(5, tempincomingtransaction.getSendername());
				// pstmt2.setString(6, tempincomingtransaction.getCurrency());
				// pstmt2.setString(7, tempincomingtransaction.getOriginateaccount());
				pstmt2.setString(8, tempincomingtransaction.getOriginatetransactionuuid());
				pstmt2.setString(9, tempincomingtransaction.getAccountreference());
				pstmt2.setString(10, tempincomingtransaction.getVendorunique());
				pstmt2.setString(11, tempincomingtransaction.getTransactionstatusuuid());
				// pstmt2.setDouble(12, tempincomingtransaction.getCommission());
				// pstmt2.setBoolean(13, tempincomingtransaction.isPresettlement());
				pstmt2.setTimestamp(14, new Timestamp(tempincomingtransaction.getServertime().getTime()));

				pstmt2.executeUpdate();

			} else {
				success = putProcessedTrans(tempincomingtransaction);
			}

		} catch (SQLException e) {
			logger.error("SQLException when trying to update processedtransaction with uuid '" + uuid + "' with "
					+ tempincomingtransaction + ".");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<ProcessedCollection> getAllProcessedTrans() {
		List<ProcessedCollection> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM processedtransaction ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, ProcessedCollection.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all processedtransaction.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<ProcessedCollection> getAllProcessedTrans(int fromIndex, int toIndex) {
		List<ProcessedCollection> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn
					.prepareStatement("SELECT * FROM processedtransaction ORDER BY servertime DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, ProcessedCollection.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all processedtransaction from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public ProcessedCollection getProcessedTranstatus(String transactionstatusuuid) {
		ProcessedCollection s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM processedtransaction WHERE transactionstatusuuid=? ;");
			pstmt.setString(1, transactionstatusuuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, ProcessedCollection.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting processedtransaction with transactionstatusuuid '"
					+ transactionstatusuuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	/**
	 * 
	 */
	@Override
	public List<ProcessedCollection> getAllProcessedTrans(String sendertransactionid) {
		List<ProcessedCollection> list = new LinkedList<>();
		ProcessedCollection transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM processedtransaction WHERE originatetransactionuuid=?;");
			pstmt.setString(1, sendertransactionid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transaction = beanProcessor.toBean(rset, ProcessedCollection.class);

				list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching processedtransaction with originatetransactionuuid"
					+ sendertransactionid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<ProcessedCollection> getTransactionReference(String originatetransactionuuid) {
		List<ProcessedCollection> list = new LinkedList<>();
		ProcessedCollection transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM processedtransaction WHERE originatetransactionuuid=?;");
			pstmt.setString(1, originatetransactionuuid);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transaction = beanProcessor.toBean(rset, ProcessedCollection.class);

				list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching transaction with referenceNumber" + originatetransactionuuid);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<ProcessedCollection> getProcessedTranstatusByStatusUuid(TransactionStatus transactionStatus,
			int limit) {
		List<ProcessedCollection> list = new LinkedList<>();
		ProcessedCollection transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM processedtransaction WHERE transactionstatusuuid=? ORDER BY RANDOM() ASC LIMIT ?;");
			pstmt.setString(1, transactionStatus.getUuid());
			pstmt.setInt(2, limit);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transaction = beanProcessor.toBean(rset, ProcessedCollection.class);

				list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error(
					"SQL exception while fetching processedtransaction with transactionstatus" + transactionStatus);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<ProcessedCollection> getTransactionstatus(String referencenumber, Account account) {
		List<ProcessedCollection> list = new LinkedList<>();
		ProcessedCollection transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM processedtransaction WHERE originatetransactionuuid=? AND creditaccountuuid=?;");
			pstmt.setString(1, referencenumber);
			pstmt.setString(2, account.getUuid());

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transaction = beanProcessor.toBean(rset, ProcessedCollection.class);

				list.add(transaction);
			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching transaction with originatetransactionuuid" + referencenumber);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public ProcessedCollection getTransactionstatus1(String referencenumber, Account account) {
		ProcessedCollection transaction = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM processedtransaction WHERE originatetransactionuuid=? AND creditaccountuuid=?;");
			pstmt.setString(1, referencenumber);
			pstmt.setString(2, account.getUuid());

			rset = pstmt.executeQuery();

			while (rset.next()) {
				transaction = beanProcessor.toBean(rset, ProcessedCollection.class);
			}

		} catch (SQLException e) {
			logger.error("SQL exception while fetching transaction with originatetransactionuuid" + referencenumber);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return transaction;
	}

	@Override
	public List<ProcessedCollection> getAllProcessedTrans(Account account, int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		List<ProcessedCollection> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM processedtransaction WHERE creditaccountuuid=? ORDER BY debitcurrency DESC LIMIT ? OFFSET ?;");
			pstmt.setString(1, account.getUuid());
			pstmt.setInt(2, toIndex - fromIndex);
			pstmt.setInt(3, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, ProcessedCollection.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all processedtransaction from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

}

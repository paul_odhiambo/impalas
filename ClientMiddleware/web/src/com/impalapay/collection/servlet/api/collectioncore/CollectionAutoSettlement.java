package com.impalapay.collection.servlet.api.collectioncore;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.collection.beans.balance.CollectionBalanceHistory;
import com.impalapay.collection.beans.incoming.ProcessedCollection;
import com.impalapay.collection.beans.incoming.TempCollection;
import com.impalapay.collection.beans.network.CollectionDefine;
import com.impalapay.collection.beans.network.CollectionNetwork;
import com.impalapay.collection.persistence.balance.CollectionBalanceDAOImpl;
import com.impalapay.collection.persistence.network.CollectionDefineDAOImpl;
import com.impalapay.collection.persistence.processedtransactions.ProcessedTransactionDAOImpl;
import com.impalapay.collection.persistence.tempincoming.TempIncomingDAOImpl;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class CollectionAutoSettlement extends HttpServlet {

	private Cache networkCache, transactionStatusCache;

	private TempIncomingDAOImpl transactionDAO;

	private ProcessedTransactionDAOImpl proccesedtransactionDAO;

	private CollectionBalanceDAOImpl collectionDAO;

	private CollectionDefineDAOImpl collectiondefineDAO;

	private HashMap<String, String> transactionStatusHash = new HashMap<>();

	private HashMap<String, String> transactionStatusuuidHash = new HashMap<>();

	private HashMap<String, String> networkQueryUrlmap = new HashMap<>();

	private HashMap<String, String> networkBridgeQueryUrlmap = new HashMap<>();

	private HashMap<String, String> networksupportquerymap = new HashMap<>();

	private HashMap<String, String> networkUsernamemap = new HashMap<>();

	private HashMap<String, String> networkPasswordmap = new HashMap<>();

	private HashMap<String, String> routenetworkuuiddefineuuidmap = new HashMap<>();

	private HashMap<String, Boolean> routenefixedcommissionmap = new HashMap<>();

	private HashMap<String, Double> routecommissionmap = new HashMap<>();

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		transactionDAO = TempIncomingDAOImpl.getInstance();
		collectionDAO = CollectionBalanceDAOImpl.getInstance();
		proccesedtransactionDAO = ProcessedTransactionDAOImpl.getInstance();
		collectiondefineDAO = CollectionDefineDAOImpl.getInstance();

		networkCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_NETWORK);
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {

		// joined json string
		String join = "";
		JsonElement root = null;
		Account fetchcollectiondefine = null;

		double commission = 0, amount = 0, switchcommission = 0;

		boolean fixedcommission;

		// These represent parameters received over the network
		String transactionid = "", networkid = "", jsonResult = "", switchresponse = "";

		//
		String Successfulltransaction = "S000", fail = "00029";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ####################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			transactionid = root.getAsJsonObject().get("transaction_id").getAsString();

			networkid = root.getAsJsonObject().get("networkuuid").getAsString();

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(transactionid) || StringUtils.isBlank(networkid)) {

			expected.put("status_code", "00032");
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		Element element;

		List keys;

		// **************Network Cache****************//

		CollectionNetwork collectionnetwork;

		// collection name and collection network uuid

		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkQueryUrlmap.put(collectionnetwork.getUuid(), collectionnetwork.getQueryingurl());
		}

		// network and username
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkUsernamemap.put(collectionnetwork.getUuid(), collectionnetwork.getUsername());
		}

		// network and password
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkPasswordmap.put(collectionnetwork.getUuid(), collectionnetwork.getPassword());
		}

		// network and bridgeremiturl
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkBridgeQueryUrlmap.put(collectionnetwork.getUuid(), collectionnetwork.getBridgequeryurl());
		}

		// network and support querying
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networksupportquerymap.put(collectionnetwork.getUuid(),
					String.valueOf(collectionnetwork.isSupportquerying()));
		}

		// ======================================================
		// Populate with the mapping of Transaction Statuses.
		// The key is a UUID of the status
		// ======================================================

		TransactionStatus status1;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status1.getStatus(), status1.getUuid());
		}

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusuuidHash.put(status1.getUuid(), status1.getStatus());
		}

		// ################################################################
		// Empty the below current Hashmaps(helps in updating deleted items)
		// ################################################################
		routenetworkuuiddefineuuidmap.clear();
		routenefixedcommissionmap.clear();
		routecommissionmap.clear();

		TempCollection transactions = transactionDAO.getTempIncomingTrans(transactionid);
		// String transactionref = transactions.getReferenceNumber();
		if ((transactions == null)) {
			expected.put("status_code", "00032");
			expected.put("command_status", "We have a problem at autoquery under fetching transactions");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		fetchcollectiondefine = new Account();
		fetchcollectiondefine.setUuid(transactions.getCreditaccountuuid());

		// ################################################################
		// Fetch route set up details
		// #################################################################
		List<CollectionDefine> routedefine = collectiondefineDAO.getAllCollectionRoute(fetchcollectiondefine);

		for (CollectionDefine routenetworkuuid : routedefine) {
			routenetworkuuiddefineuuidmap.put(routenetworkuuid.getNetworkuuid(), routenetworkuuid.getUuid());
			routenefixedcommissionmap.put(routenetworkuuid.getUuid(), routenetworkuuid.isFixedcommission());
			routecommissionmap.put(routenetworkuuid.getUuid(), routenetworkuuid.getCommission());

		}

		try {
			// check if route allows fixed commission.
			fixedcommission = routenefixedcommissionmap.get(transactions.getCollectiondefineuuid());
			commission = routecommissionmap.get(transactions.getCollectiondefineuuid());

			if (fixedcommission) {
				switchcommission = commission;
			} else {
				switchcommission = (commission * transactions.getAmount()) / 100;
			}

		} catch (Exception e) {

			expected.put("status_code", "00032");
			expected.put("status_description", "ERROR_PROCESSING_COMMISSION");
			jsonResult = g.toJson(expected);

			System.out.println("Kacommission ndo haka " + commission);

			return jsonResult;

		}

		switchresponse = transactionStatusuuidHash.get(transactions.getTransactionstatusuuid());

		ProcessedCollection processedtransfer = new ProcessedCollection();

		// server time
		Date now = new Date();

		// processedtransfer.setUuid(transactions.getUuid());
		processedtransfer.setUuid(StringUtils.remove(UUID.randomUUID().toString(), '-'));
		processedtransfer.setNetworkuuid(transactions.getNetworkuuid());
		processedtransfer.setCreditaccountuuid(transactions.getCreditaccountuuid());
		processedtransfer.setTransactiontype(transactions.getTransactiontype());
		processedtransfer.setDebitorname(transactions.getDebitorname());
		processedtransfer.setCollectiondefineuuid(transactions.getCollectiondefineuuid());
		processedtransfer.setDebitcurrency(transactions.getDebitcurrency());
		processedtransfer.setDebitcountry(transactions.getDebitcountry());
		processedtransfer.setAmount(transactions.getAmount());
		processedtransfer.setAccountreference(transactions.getAccountreference());
		processedtransfer.setTransactionstatusuuid(transactions.getTransactionstatusuuid());
		processedtransfer.setOriginatetransactionuuid(transactions.getOriginatetransactionuuid());
		processedtransfer.setReceivertransactionuuid(transactions.getReceivertransactionuuid());
		processedtransfer.setDebitedaccount(transactions.getDebitedaccount());
		processedtransfer.setOriginateamount(transactions.getOriginateamount());
		processedtransfer.setOriginatecurrency(transactions.getOriginatecurrency());
		processedtransfer.setEndpointstatusdescription(transactions.getEndpointstatusdescription());
		processedtransfer.setCommission(switchcommission);
		processedtransfer.setServertime(now);

		if (switchresponse.equalsIgnoreCase(Successfulltransaction) || switchresponse.equalsIgnoreCase(fail)) {

			if (switchresponse.equals(Successfulltransaction)) {
				CollectionBalanceHistory newbalance = new CollectionBalanceHistory();

				newbalance.setUuid(transactions.getUuid());
				newbalance.setAccountuuid(transactions.getCreditaccountuuid());
				newbalance.setCountryuuid(transactions.getDebitcurrency());
				newbalance.setAmount(transactions.getAmount());
				newbalance.setTransactionuuid(transactions.getUuid());
				newbalance.setTopupTime(new Date());

				if (!collectionDAO.putCollectionBalance(newbalance)) {

					expected.put("status_code", "00032");
					expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL
							+ "Unable to Add on Collection Balance Table OR Delete from Temp transaction Table");
					jsonResult = g.toJson(expected);

					return jsonResult;

				}

			}

			if (!proccesedtransactionDAO.putProcessedTrans(processedtransfer)) {

				expected.put("status_code", "00032");
				expected.put("command_status",
						APIConstants.COMMANDSTATUS_FAIL + " unable to add to processed transactiondatabase");
				jsonResult = g.toJson(expected);

				return jsonResult;
			}

			if (!transactionDAO.deleteTempTransaction(transactionid)) {
				// Delete from temp transaction if it refuses do marke the transaction under
				// dispute.)
				expected.put("status_code", "00032");
				expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL
						+ "Unable to Add on Collection Balance Table OR Delete from Temp transaction Table");
				jsonResult = g.toJson(expected);
			} else {

				expected.put("status_code", "S000");
				expected.put("command_status", APIConstants.COMMANDSTATUS_OK);
				jsonResult = g.toJson(expected);
			}

			return jsonResult;

		}

		expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL);
		expected.put("remit_status", "Transaction Might still be inprogress");
		jsonResult = g.toJson(expected);

		return jsonResult;

	}// end of inprogress status

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

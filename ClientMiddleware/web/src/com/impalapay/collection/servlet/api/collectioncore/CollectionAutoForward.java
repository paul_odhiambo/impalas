package com.impalapay.collection.servlet.api.collectioncore;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.transaction.TransactionStatus;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.util.net.PostWithIgnoreSSAutoFwrd;
import com.impalapay.collection.beans.incoming.ForwardCollection;
import com.impalapay.collection.beans.network.CollectionNetwork;
import com.impalapay.collection.persistence.forwardcollection.ForwardCollectionDAOImpl;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class CollectionAutoForward extends HttpServlet {

	private Cache networkCache, transactionStatusCache, accountsCache, countryCache;

	private PostWithIgnoreSSAutoFwrd postMinusThread;

	private ForwardCollectionDAOImpl forwardcollectionDAO;

	private HashMap<String, String> countryCode = new HashMap<>();

	private HashMap<String, String> currencyCode = new HashMap<>();

	private HashMap<String, String> transactionStatusHash = new HashMap<>();

	private HashMap<String, String> transactionStatusuuidHash = new HashMap<>();

	private HashMap<String, String> networkQueryUrlmap = new HashMap<>();

	private HashMap<String, String> networkBridgeQueryUrlmap = new HashMap<>();

	private HashMap<String, String> networksupportquerymap = new HashMap<>();

	private HashMap<String, String> networkUsernamemap = new HashMap<>();

	private HashMap<String, String> networkPasswordmap = new HashMap<>();

	private HashMap<String, String> accountUsername = new HashMap<>();

	private HashMap<String, String> accountPassword = new HashMap<>();

	private String CLIENT_URL = "";

	private ForwardCollection forwardcollection;

	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		forwardcollectionDAO = ForwardCollectionDAOImpl.getInstance();
		networkCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_NETWORK);
		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);
		transactionStatusCache = mgr.getCache(CacheVariables.CACHE_TRANSACTIONSTATUS_BY_UUID);
		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_UUID);
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonObject queryrequest = null;

		// These represent parameters received over the network
		String transactionid = "", networkid = "", switchresponse = "", statusdescription = "";

		String receiverlisteningurl = "", jsonResult = "", responseobject = "", statusuuid = "", username = "",
				password = "", results2 = "";

		//
		String Successfulltransaction = "S000", fail = "00029";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ####################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			transactionid = root.getAsJsonObject().get("transaction_id").getAsString();

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(transactionid)) {

			expected.put("status_code", "00032");
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		Element element;

		List keys;

		// **************Network Cache****************//

		CollectionNetwork collectionnetwork;

		// collection name and collection network uuid

		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkQueryUrlmap.put(collectionnetwork.getUuid(), collectionnetwork.getQueryingurl());
		}

		// network and username
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkUsernamemap.put(collectionnetwork.getUuid(), collectionnetwork.getUsername());
		}

		// network and password
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkPasswordmap.put(collectionnetwork.getUuid(), collectionnetwork.getPassword());
		}

		// network and bridgeremiturl
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networkBridgeQueryUrlmap.put(collectionnetwork.getUuid(), collectionnetwork.getBridgequeryurl());
		}

		// network and support querying
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			collectionnetwork = (CollectionNetwork) element.getObjectValue();
			networksupportquerymap.put(collectionnetwork.getUuid(),
					String.valueOf(collectionnetwork.isSupportquerying()));
		}

		// ======================================================
		// Populate with the mapping of Transaction Statuses.
		// The key is a UUID of the status
		// ======================================================

		TransactionStatus status1;
		keys = transactionStatusCache.getKeys();

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusHash.put(status1.getStatus(), status1.getUuid());
		}

		for (Object key : keys) {
			element = transactionStatusCache.get(key);
			status1 = (TransactionStatus) element.getObjectValue();
			transactionStatusuuidHash.put(status1.getUuid(), status1.getStatus());
		}

		// **************Accounts Cache****************//
		Account accounts;
		keys = accountsCache.getKeys();

		for (Object key : keys) {
			element = accountsCache.get(key);
			accounts = (Account) element.getObjectValue();
			accountUsername.put(accounts.getUuid(), accounts.getApiUsername());
		}

		for (Object key : keys) {
			element = accountsCache.get(key);
			accounts = (Account) element.getObjectValue();
			accountPassword.put(accounts.getUuid(), accounts.getApiPasswd());
		}

		// fetch from cache
		Country country;
		keys = countryCache.getKeys();
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			currencyCode.put(country.getUuid(), country.getCurrencycode());
		}

		// country and country uuid
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryCode.put(country.getUuid(), country.getCountrycode());
		}

		ForwardCollection transactions = forwardcollectionDAO.getForwardColletion(transactionid);
		// String transactionref = transactions.getReferenceNumber();
		if ((transactions == null)) {
			expected.put("status_code", "00032");
			expected.put("command_status", "We have a problem fetching the above transaction");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		// #############################################################################################
		// construct a Mega-Json Object to route-transactions to internal
		// Servlet routing transactions.
		// #############################################################################################
		queryrequest = new JsonObject();

		queryrequest.addProperty("api_username", accountUsername.get(transactions.getCreditaccountuuid()));
		queryrequest.addProperty("reference_number", transactions.getAccountreference());
		queryrequest.addProperty("amount", transactions.getAmount());
		queryrequest.addProperty("source_account", transactions.getDebitedaccount());
		queryrequest.addProperty("source_country_code", countryCode.get(transactions.getDebitcurrency()));
		queryrequest.addProperty("source_currency_code", currencyCode.get(transactions.getDebitcurrency()));
		queryrequest.addProperty("source_transactionid", transactions.getOriginatetransactionuuid());
		queryrequest.addProperty("source_networkname", transactions.getNetworkname());
		queryrequest.addProperty("client_datetime", String.valueOf(transactions.getCollectiondate()));

		results2 = g.toJson(queryrequest);

		receiverlisteningurl = transactions.getForwarduri();

		CLIENT_URL = receiverlisteningurl;

		username = accountUsername.get(transactions.getCreditaccountuuid());
		password = accountPassword.get(transactions.getCreditaccountuuid());

		if (StringUtils.isNotEmpty(CLIENT_URL)) {

			postMinusThread = new PostWithIgnoreSSAutoFwrd(CLIENT_URL, results2, username, password);

			logger.error(".....................................................");
			logger.error("AUTOFORWARD REQUEST TO CLIENT ");
			logger.error(".....................................................");
			logger.error("AUTOFORWARD REQUEST TO CLIENT URI " + CLIENT_URL + "\n");
			logger.error("AUTOFORWARD REQUEST TO CLIENT OBJECT " + results2 + "\n");

			try {

				// capture the switch respoinse.
				responseobject = postMinusThread.executeresult();

				roots = new JsonParser().parse(responseobject);
				// ===============================================================
				// an object that will contain parameters for provisional response
				// ===============================================================
				// exctract a specific json element from the object(status_code)
				switchresponse = roots.getAsJsonObject().get("status_code").getAsString();

				// exctract a specific json element from the object(status_code)
				statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

				// reference number
				statusdescription = roots.getAsJsonObject().get("reference_number").getAsString();

			} catch (Exception e) {

				// ================================================
				// Missing fields in response from receiver system
				// ================================================
				// logger.equals("Forward transaction with uuid " + transactions.getUuid() + "
				// was complete failure investigate immediately");
				switchresponse = "00032";
				statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
				jsonResult = g.toJson(expected);
			}

			if (!transactionStatusHash.containsKey(switchresponse)) {
				switchresponse = "00032";
				statusdescription = "UNKNOWN_RESPONSE_CODE_FROM_RECEIVER";
			}

			if (switchresponse.equalsIgnoreCase(Successfulltransaction) || switchresponse.equalsIgnoreCase(fail)) {
				forwardcollection = new ForwardCollection();
				statusuuid = transactionStatusHash.get(switchresponse);

				forwardcollection.setUuid(transactions.getUuid());
				forwardcollection.setCreditaccountuuid(transactions.getCreditaccountuuid());
				forwardcollection.setNetworkname(transactions.getNetworkname());
				forwardcollection.setTransactiontype(transactions.getTransactiontype());
				forwardcollection.setDebitorname(transactions.getDebitorname());
				forwardcollection.setDebitcurrency(transactions.getDebitcurrency());
				forwardcollection.setAmount(transactions.getAmount());
				forwardcollection.setAccountreference(transactions.getAccountreference());
				forwardcollection.setDebitedaccount(transactions.getDebitedaccount());
				forwardcollection.setOriginatetransactionuuid(transactions.getOriginatetransactionuuid());
				forwardcollection.setForwarduri(transactions.getForwarduri());
				forwardcollection.setProcessingstatus(statusuuid);
				forwardcollection.setEndpointstatusdescription(statusdescription);
				forwardcollection.setCollectiondate(new Date());

				if (forwardcollectionDAO.putForwardColletionHistory(forwardcollection)) {
					// delete from forward table
					forwardcollectionDAO.deleteForwardCollection(transactions.getUuid());

					switchresponse = "S000";
					statusdescription = "SUCCESS";

				} else {
					switchresponse = "00032";
					statusdescription = "SERIOUS ERROR HAS OCCURED";

				}

				expected.put("status_code", switchresponse);
				expected.put("status_description", statusdescription);
				jsonResult = g.toJson(expected);

				return jsonResult;

			}

			// response to be returned if transaction fails

			// return "omwamiiiiiiiiiiiiii";

		}

		expected.put("command_status", APIConstants.COMMANDSTATUS_FAIL);
		expected.put("remit_status",
				"Recipient URI " + CLIENT_URL + " dint not Reply.Transaction forwarded back to INPROGRESS" + "\n"
						+ "The Recipient might have replied with the below " + roots);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}// end of inprogress status

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

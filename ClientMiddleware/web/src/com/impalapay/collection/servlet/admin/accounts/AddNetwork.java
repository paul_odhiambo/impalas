package com.impalapay.collection.servlet.admin.accounts;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.collection.beans.network.CollectionNetwork;
import com.impalapay.collection.persistence.network.CollectionNetworkDAOImpl;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add an msisdn to a specific country.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 11, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class AddNetwork extends HttpServlet {

	final String ERROR_NO_COUNTRYNAME = "Please provide the country name";
	final String ERROR_NO_ROUTEENDPOINTURL = "Please provide the debit URI";
	final String ERROR_NO_ROUTEBRIDGEENDPOINTURL = "Please provide the debit bridge URI";
	final String ERROR_NO_ROUTEACCOUNTURL = "Please provide the account check URI";
	final String ERROR_NO_ROUTEBRIDGEACCOUNTURL = "Please provide the account check Bridge URI";
	final String ERROR_NO_ROUTEREVERSEURL = "Please provide the reversal URI";
	final String ERROR_NO_ROUTEBRIDGEREVERSEURL = "Please provide the reversal Bridge URI";
	final String ERROR_NO_ROUTEQUERYURL = "Please provide the query  URI";
	final String ERROR_NO_ROUTEBRIDGEQUERYURL = "Please provide the query Bridge  URI";
	final String ERROR_NO_ROUTEFOREXURL = "Please provide the Forex URI";
	final String ERROR_NO_ROUTEBRIDGEFOREXURL = "Please provide the Forex Bridge  URI";
	final String ERROR_NO_ROUTEBALANCEURL = "Please provide the Balance/Float URI";
	final String ERROR_NO_ROUTEBRIDGEBALANCEURL = "Please provide the Balance/Float Bridge URI";
	final String ERROR_NO_ROUTEUSERNAME = "Please provide the route USERNAME";
	final String ERROR_NO_ROUTEPASSWORD = "please provide the route PASSWORD";
	final String ERROR_NO_ROUTENAME = "please provide the route/network NAME";
	final String ERROR_NO_ROUTEPARTNERNAME = "please provide the route/network PARTNERSNAME";
	final String ERROR_NO_INSTRUCTION = "please provide the route/network Usage Instructions";
	final String ERROR_NO_COLLECTIONCHANNEL = "please provide the route/network channel type";
	final String ERROR_NO_UNABLETOUPDATEROUTE = "unable to update route";
	final String ERROR_ROUTENAME_EXISTS = "The Unique Name provided already exists in the system.";
	final String ERROR_ROUTEFOREX_STATUS = "Please state if forex will be allowed or not.";
	final String ERROR_ROUTEREVERSAL_STATUS = "Please state if reversals will be allowed or not.";
	final String ERROR_ROUTEACCOUNTCHECK_STATUS = "Please state if acccount check will be allowed or not.";
	final String ERROR_ROUTEQUERYCHECK_STATUS = "Please state if query check will be allowed or not.";
	final String ERROR_ROUTECOMMMISSION_STATUS = "Please state if route allows percentage commission or not.";
	final String ERROR_ROUTEBALANCECHECK_STATUS = "Please state if balance check will be allowed or not.";

	private String countryuuid, debiturl, bridgedebiturl, queryingurl, bridgequeryurl, username, password, networkname,
			providername, supportforex, supportquerycheck, supportlisteningurl, supportdebit, addedbyuuid,
			collectiontchannel, instruction, balanceurl, bridgebalanceurl, reversalurl, bridgereversalurl, forexurl,
			bridgeforexurl, accountcheckurl, bridgeaccountcheckurl, extraurl, supportreversal, supportaccountcheck,
			supportbalancecheck;

	// queryinginterval = 0;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;

	private CollectionNetworkDAOImpl networkDAO;
	private CacheManager cacheManager;
	private HttpSession session;

	private Cache networkCache;

	private String alluuid = "";

	private String transactioinid = "";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		networkDAO = CollectionNetworkDAOImpl.getInstance();
		// cacheManager = CacheManager.getInstance();
		networkCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_NETWORK);
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_PARAMETERS, paramHash);

		// No country provided
		if (StringUtils.isBlank(countryuuid)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_COUNTRYNAME);

			// No remit uri provided
		} else if (StringUtils.isBlank(debiturl)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEENDPOINTURL);

			// No bridgeremit uri provided
		} else if (StringUtils.isBlank(bridgedebiturl)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEBRIDGEENDPOINTURL);

			// No queryip provided
		} else if (StringUtils.isBlank(queryingurl)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEQUERYURL);

			// No bridgequeryip provided
		} else if (StringUtils.isBlank(bridgequeryurl)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEBRIDGEQUERYURL);

			// No balanceip provided
		} else if (StringUtils.isBlank(balanceurl)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEBALANCEURL);

			// No bridgebalanceip provided
		} else if (StringUtils.isBlank(bridgebalanceurl)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEBRIDGEBALANCEURL);
			// No reversalip provided
		} else if (StringUtils.isBlank(reversalurl)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEREVERSEURL);
			// No bridgereversalip provided
		} else if (StringUtils.isBlank(bridgereversalurl)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEBRIDGEREVERSEURL);

			// No forexip provided
		} else if (StringUtils.isBlank(forexurl)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEFOREXURL);

			// No bridgeforexip provided
		} else if (StringUtils.isBlank(bridgeforexurl)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEBRIDGEFOREXURL);

			// No accountcheckip provided
		} else if (StringUtils.isBlank(accountcheckurl)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEACCOUNTURL);
			// No bridgeaccountcheckip provided
		} else if (StringUtils.isBlank(bridgeaccountcheckurl)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEBRIDGEACCOUNTURL);

			// No route username provided
		} else if (StringUtils.isBlank(username)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEUSERNAME);
			// No route password provided
		} else if (StringUtils.isBlank(password)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEPASSWORD);
			// No route networkname provided
		} else if (StringUtils.isBlank(networkname)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTENAME);
			// No route partnername provided
		} else if (StringUtils.isBlank(password)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_ROUTEPARTNERNAME);
			// No route commission provided
		} else if (StringUtils.isBlank(instruction)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_INSTRUCTION);
			// No route commission provided
		} else if (StringUtils.isBlank(collectiontchannel)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_COLLECTIONCHANNEL);

		} else if (!addNetwork()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, ERROR_NO_UNABLETOUPDATEROUTE);
		} else {

			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_SUCCESS_KEY, "s");

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_ADD_NETWORK_ERROR_KEY, null);

			addNetwork();

		}

		response.sendRedirect("addcollectionNetwork.jsp");

	}

	/**
	 *
	 */
	private boolean addNetwork() {
		CollectionNetwork network2 = new CollectionNetwork();

		network2.setNetworkname(networkname);

		CollectionNetwork existingnetwork = networkDAO.getNetwork(network2);

		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		if (existingnetwork != null) {

			alluuid = existingnetwork.getUuid();
		} else {
			alluuid = transactioinid;
		}

		CollectionNetwork network = new CollectionNetwork();
		network.setUuid(alluuid);
		network.setNetworkname(networkname);
		network.setCollectiontchannel(collectiontchannel);
		network.setAddedbyuuid(addedbyuuid);
		network.setQueryingurl(queryingurl);
		network.setBridgequeryurl(bridgequeryurl);
		network.setDebiturl(debiturl);
		network.setBridgedebiturl(bridgedebiturl);
		network.setUsername(username);
		network.setPassword(password);
		network.setSupportdebit(Boolean.parseBoolean(supportdebit));
		network.setSupportquerying(Boolean.parseBoolean(supportquerycheck));
		network.setSupportlisteningurl(Boolean.parseBoolean(supportlisteningurl));
		network.setInstruction(instruction);
		network.setCountryuuid(countryuuid);
		network.setProvidername(providername);
		network.setBalanceurl(balanceurl);
		network.setBridgebalanceurl(bridgebalanceurl);
		network.setReversalurl(reversalurl);
		network.setBridgebalanceurl(bridgebalanceurl);
		network.setForexurl(forexurl);
		network.setBridgeforexurl(bridgeforexurl);
		network.setExtraurl(extraurl);
		network.setSupportreversal(Boolean.parseBoolean(supportreversal));
		network.setSupportaccountcheck(Boolean.parseBoolean(supportaccountcheck));
		network.setSupportbalancecheck(Boolean.parseBoolean(supportbalancecheck));

		boolean response = networkDAO.updateNetwork(alluuid, network);

		networkCache.put(new Element(network.getUuid(), network));

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		supportlisteningurl = StringUtils.trimToEmpty(request.getParameter("optionlistenercheck"));
		supportdebit = StringUtils.trimToEmpty(request.getParameter("optiondebitcheck"));
		addedbyuuid = StringUtils.trimToEmpty(request.getParameter("username"));
		collectiontchannel = StringUtils.trimToEmpty(request.getParameter("channeluuid"));
		instruction = StringUtils.trimToEmpty(request.getParameter("instruction"));

		supportbalancecheck = StringUtils.trimToEmpty(request.getParameter("optionbalancecheck"));
		supportquerycheck = StringUtils.trimToEmpty(request.getParameter("optionquerycheck"));
		supportaccountcheck = StringUtils.trimToEmpty(request.getParameter("optionaccountcheck"));
		supportreversal = StringUtils.trimToEmpty(request.getParameter("optionreversal"));
		supportforex = StringUtils.trimToEmpty(request.getParameter("optionforex"));
		providername = StringUtils.trimToEmpty(request.getParameter("partnername"));
		networkname = StringUtils.trimToEmpty(request.getParameter("networkname"));
		username = StringUtils.trimToEmpty(request.getParameter("apiusername"));
		password = StringUtils.trimToEmpty(request.getParameter("password"));
		extraurl = StringUtils.trimToEmpty(request.getParameter("extraurl"));
		bridgeaccountcheckurl = StringUtils.trimToEmpty(request.getParameter("bridgeaccountcheckip"));
		accountcheckurl = StringUtils.trimToEmpty(request.getParameter("accountcheckip"));
		bridgeforexurl = StringUtils.trimToEmpty(request.getParameter("bridgeforexip"));
		forexurl = StringUtils.trimToEmpty(request.getParameter("forexip"));
		bridgereversalurl = StringUtils.trimToEmpty(request.getParameter("bridgereversalip"));
		reversalurl = StringUtils.trimToEmpty(request.getParameter("reversalip"));
		bridgebalanceurl = StringUtils.trimToEmpty(request.getParameter("bridgebalanceip"));
		balanceurl = StringUtils.trimToEmpty(request.getParameter("balanceip"));
		bridgequeryurl = StringUtils.trimToEmpty(request.getParameter("bridgequeryip"));
		queryingurl = StringUtils.trimToEmpty(request.getParameter("queryip"));
		bridgedebiturl = StringUtils.trimToEmpty(request.getParameter("bridgeremitip"));
		debiturl = StringUtils.trimToEmpty(request.getParameter("remitip"));
		countryuuid = StringUtils.trimToEmpty(request.getParameter("countryuuid"));

	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		paramHash = new HashMap<>();

		paramHash.put("countryuuid", countryuuid);
		paramHash.put("debiturl", debiturl);
		paramHash.put("bridgedebiturl", bridgedebiturl);
		paramHash.put("queryingurl", queryingurl);
		paramHash.put("bridgequeryurl", bridgequeryurl);
		paramHash.put("balanceurl", balanceurl);
		paramHash.put("bridgebalanceurl", bridgebalanceurl);
		paramHash.put("reversalurl", reversalurl);
		paramHash.put("bridgereversalurl", bridgereversalurl);
		paramHash.put("forexurl", forexurl);
		paramHash.put("bridgeforexurl", bridgeforexurl);
		paramHash.put("accountcheckurl", accountcheckurl);
		paramHash.put("bridgeaccountcheckurl", bridgeaccountcheckurl);
		paramHash.put("extraurl", extraurl);
		paramHash.put("password", password);
		paramHash.put("username", username);
		paramHash.put("networkname", networkname);
		paramHash.put("providername", providername);
		paramHash.put("supportforex", supportforex);
		paramHash.put("supportreversal", supportreversal);
		paramHash.put("supportaccountcheck", supportaccountcheck);
		paramHash.put("supportquerycheck", supportquerycheck);
		paramHash.put("supportbalancecheck", supportbalancecheck);

		paramHash.put("supportlisteningurl", supportlisteningurl);
		paramHash.put("supportdebit", supportdebit);
		paramHash.put("addedbyuuid", addedbyuuid);
		paramHash.put("collectiontchannel", collectiontchannel);
		paramHash.put("instruction", instruction);

		// paramHash.put("supportcommissionpercentage", supportcommissionpercentage);
		// paramHash.put("networkstatusuuid", networkstatusuuid);
		// paramHash.put("commission", commission);
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		// response.sendRedirect("../index.jsp");
	}

}

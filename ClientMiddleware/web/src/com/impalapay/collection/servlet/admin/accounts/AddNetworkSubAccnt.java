package com.impalapay.collection.servlet.admin.accounts;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.persistence.geolocation.CountryDAO;
import com.impalapay.collection.beans.network.CollectionNetworkSubaccount;
import com.impalapay.collection.persistence.networksubaccount.CollectionNetworkSubaccountDAOImpl;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to add an msisdn to a specific country.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 11, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class AddNetworkSubAccnt extends HttpServlet {

	final String ERROR_NO_NETWORK = "Please provide a value for the Collection Network ";
	final String ERROR_NO_COUNTRY = "Please provide a value for the Currency .";
	final String ERROR_NO_SHARED = "Please state if its a shared configuration connection(e.g Shared Paybill)";
	final String ERROR_INVALID_SPLIT = "Please provide a value for the Reference SplitSize";
	final String ERROR_NO_NUMBER = "Please provide a value for the Collection_Number";
	final String ERROR_UNABLE_ADD = "Unable to add Collection Sub Account";
	// These represent form parameters
	private String networkUuid, countryUuid, splitlength, collectionnumber, optionsharedconfiguration;
	private String addDay, addMonth, addYear;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> PrefixparamHash;

	private Cache collectionnetworksubaccntCache;
	private CountryDAO countryDAO;
	private CollectionNetworkSubaccountDAOImpl collectionnetworksubaccntDAO;

	private Logger logger;

	private String alluuid = "";

	private String transactioinid = "";

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		collectionnetworksubaccntCache = mgr.getCache(CacheVariables.CACHE_COLLECTION_NETWORK_SUBACCOUNT);
		countryDAO = CountryDAO.getInstance();
		collectionnetworksubaccntDAO = CollectionNetworkSubaccountDAOImpl.getInstance();
		logger = Logger.getLogger(this.getClass());
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		setClassParameters(request);

		initParamHash();
		session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_PARAMETERS, PrefixparamHash);

		if (StringUtils.isBlank(networkUuid)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_ERROR_KEY, ERROR_NO_NETWORK);

		} else if (StringUtils.isBlank(countryUuid)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_ERROR_KEY, ERROR_NO_COUNTRY);

		} else if (StringUtils.isBlank(optionsharedconfiguration)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_ERROR_KEY, ERROR_NO_SHARED);

		} else if (StringUtils.isBlank(splitlength)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_ERROR_KEY, ERROR_INVALID_SPLIT);
		} else if (StringUtils.isBlank(collectionnumber)) {
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_ERROR_KEY, ERROR_NO_NUMBER);

		} else if (!addSplitPrefix()) {
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_ERROR_KEY, ERROR_UNABLE_ADD);

		} else {
			// If we get this far then all parameter checks are ok.
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_SUCCESS_KEY, "s");

			// Reduce our session data
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_PARAMETERS, null);
			session.setAttribute(SessionConstants.ADMIN_ADD_PREFIX_ERROR_KEY, null);

		}

		response.sendRedirect("addcollectionSubAccnt.jsp");

		// purchasesCache.put(new
		// Element(CacheVariables.CACHE_PURCHASEPERCOUNTRY_KEY,
		// accountPurchaseDAO.getAllClientPurchasesByCountry()));
	}

	/**
	 * Add amount added to each country float.
	 * 
	 * @return boolean indicating if addition has been added or not.
	 */
	private boolean addSplitPrefix() {

		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');

		Country country = countryDAO.getCountry(countryUuid);

		countryUuid = country.getCurrencycode();

		country.setUuid(countryUuid);

		CollectionNetworkSubaccount collectonsubaccntpage = collectionnetworksubaccntDAO
				.getCollectionNetworkSubAcct(networkUuid, collectionnumber);

		if (collectonsubaccntpage != null) {

			alluuid = collectonsubaccntpage.getUuid();
		} else {
			alluuid = transactioinid;
		}

		CollectionNetworkSubaccount p = new CollectionNetworkSubaccount();

		p.setNetworkuuid(networkUuid);
		p.setCurrency(countryUuid);
		p.setCollectionnumber(collectionnumber);
		p.setSharedconfiguration(Boolean.parseBoolean(optionsharedconfiguration));
		p.setReferencesplitlength(Integer.parseInt(splitlength));
		Calendar c = Calendar.getInstance();
		c.set(NumberUtils.toInt(addYear), NumberUtils.toInt(addMonth) - 1, NumberUtils.toInt(addDay));
		p.setDateadded(c.getTime());

		System.out.print(p);

		boolean response = collectionnetworksubaccntDAO.updateNetworkSubaccount(alluuid, p);

		collectionnetworksubaccntCache.put(new Element(p.getUuid(), p));

		return response;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {
		networkUuid = StringUtils.trimToEmpty(request.getParameter("channeluuid"));
		countryUuid = StringUtils.trimToEmpty(request.getParameter("currencyuuid"));
		splitlength = StringUtils.trimToEmpty(request.getParameter("splitlength"));
		collectionnumber = StringUtils.trimToEmpty(request.getParameter("collectionnumber"));
		optionsharedconfiguration = StringUtils.trimToEmpty(request.getParameter("optionsharedconfiguration"));
		addDay = StringUtils.trimToEmpty(request.getParameter("addDay"));
		addMonth = StringUtils.trimToEmpty(request.getParameter("addMonth"));
		addYear = StringUtils.trimToEmpty(request.getParameter("addYear"));

	}

	/**
	 * Place some of the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		PrefixparamHash = new HashMap<>();
		PrefixparamHash.put("channeluuid", networkUuid);
		PrefixparamHash.put("countryUuid", countryUuid);
		PrefixparamHash.put("splitlength", splitlength);
		PrefixparamHash.put("collectionnumber", collectionnumber);
		PrefixparamHash.put("optionsharedconfiguration", optionsharedconfiguration);

	}

}

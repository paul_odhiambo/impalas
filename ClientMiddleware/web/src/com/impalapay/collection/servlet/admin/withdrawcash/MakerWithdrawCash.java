package com.impalapay.collection.servlet.admin.withdrawcash;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.accountmgmt.admin.SessionConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.accountmgmt.ManagementAccount;
import com.impalapay.airtel.beans.accountmgmt.inprogressbalance.InProgressMasterBalanceHoldHist;
import com.impalapay.airtel.beans.forex.ForexEngine;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.systemlog.SystemLog;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.systemlog.SystemLogDAO;
import com.impalapay.collection.beans.balance.CashWithdrawal;
import com.impalapay.collection.beans.balance.CollectionBalanceHistory;
import com.impalapay.collection.persistence.accountmgmt.inprogressbalance.InProgressBalanceDAO;
import com.impalapay.collection.persistence.balance.CollectionBalanceDAOImpl;
import com.impalapay.collection.persistence.cashwithdraw.CheckerWithdrawDAOImpl;
import com.impalapay.collection.persistence.cashwithdraw.CheckerWithdrawHistoryDAOImpl;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.lang3.StringUtils;

/**
 * Servlet that receives parameters from an administrative web form that are
 * used to create a new account.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Nov 24, 2014
 *
 * @author <a href="mailto:mike@impalapay.com">Michael Wakahe</a>
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 *
 */
public class MakerWithdrawCash extends HttpServlet {

	final String ERROR_NO_USERNAME = "Please provide a Username.";
	final String ERROR_NO_BALANCEUUID = "Please provide the checker withdrawal Unique indetifier.";
	final String ERROR_NO_BANKRATE = "Please provide the Final withdrawal rate provided by the Bank";
	final String ERROR_NO_TRANSFERCHARGE = "Please Provide the total transfer charges for this Transaction";
	final String ERROR_NO_WITHDRAWFAIL = "Unable to finalise Your withdrawal Request Please contact the ystem administrator or try after a few minutes";

	private String username, checkerwithdrawuuid, transfercharge, bankexchangerate;

	// This is used to store parameter names and values from the form.
	private HashMap<String, String> paramHash;
	private HashMap<String, Double> systemforexmap = new HashMap<>();
	private HashMap<String, String> countrycurrencycodemap = new HashMap<>();
	private SystemLogDAO systemlogDAO;
	private CheckerWithdrawDAOImpl checkerbalanceDAO;
	private CheckerWithdrawHistoryDAOImpl checkerwithdrawalhistoryDAO;
	private CollectionBalanceDAOImpl collectionbalanceDAO;
	private InProgressBalanceDAO inprogressbalancehold;
	private CashWithdrawal withdrawalrequest, checkerwithdrawalobject;
	private CollectionBalanceHistory deductbalance;
	private InProgressMasterBalanceHoldHist putcollectionbalanceonhold;
	private String transactioinid = "", inprogresstransactionuuid = "5c9b8b0b-a035-4a07-bfd8-eccd4f039d53",
			jsonResult = "", success = "S000", fail = "00032", statusdescription, statuscode = "", transactioinid2 = "";
	private CacheManager cacheManager;
	private Cache forexCache, countryCache, accountsCache;
	private ManagementAccount account = null;
	private HttpSession session;
	private JsonElement root2 = null;
	private Map<String, String> expected = new HashMap<>();
	private double originalwithdrawamount = 0, comissionamount = 0, bankexchangerates = 0, receivableamount = 0,
			convertedamount = 0, eligiblewithdrawalamount = 0, withdrawalcharges = 0;

	/**
	 *
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		checkerbalanceDAO = CheckerWithdrawDAOImpl.getInstance();
		checkerwithdrawalhistoryDAO = CheckerWithdrawHistoryDAOImpl.getInstance();
		systemlogDAO = SystemLogDAO.getInstance();
		collectionbalanceDAO = CollectionBalanceDAOImpl.getInstance();
		inprogressbalancehold = InProgressBalanceDAO.getInstance();
		cacheManager = CacheManager.getInstance();
		countryCache = cacheManager.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);

		forexCache = cacheManager.getCache(CacheVariables.CACHE_FOREX_BY_UUID);
		accountsCache = cacheManager.getCache(CacheVariables.CACHE_MANAGEMENTACCOUNTS_BY_USERNAME);

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);

		setClassParameters(request);

		initParamHash();

		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (ManagementAccount) element.getObjectValue();
		}
		session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_PARAMETERS, paramHash);

		// forex with curency pairs
		List keys;

		// fetch from cache
		Country country;
		keys = countryCache.getKeys();
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countrycurrencycodemap.put(country.getUuid(), country.getCurrencycode());
		}

		ForexEngine forexengine;
		keys = forexCache.getKeys();
		for (Object key : keys) {
			element = forexCache.get(key);
			forexengine = (ForexEngine) element.getObjectValue();
			systemforexmap.put(forexengine.getCurrencypair(), forexengine.getSpreadrate());

		}

		// No First Name provided
		if (StringUtils.isBlank(username)) {
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, ERROR_NO_USERNAME);

			// No Unique Name provided
		} else if (StringUtils.isBlank(checkerwithdrawuuid)) {
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, ERROR_NO_BALANCEUUID);

			// No website login password provided
		} else if (StringUtils.isBlank(transfercharge)) {
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, ERROR_NO_TRANSFERCHARGE);

		} else if (StringUtils.isBlank(bankexchangerate)) {
			session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, ERROR_NO_BANKRATE);

		} else {

			String receivedresponse = makerWithdrawCash();

			root2 = new JsonParser().parse(receivedresponse);
			statuscode = root2.getAsJsonObject().get("status_code").getAsString();
			statusdescription = root2.getAsJsonObject().get("status_description").getAsString();

			if (statuscode.equalsIgnoreCase(success)) {

				// If we get this far then all parameter checks are ok.
				session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_SUCCESS_KEY, statusdescription);

				// Reduce our session data
				session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_PARAMETERS, null);
				session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, null);

			} else {
				session.setAttribute(SessionConstants.ADMIN_CHECKERWITHDRAWALREQUEST_ERROR_KEY, statusdescription);
			}

		}

		// response.sendRedirect("addAccount.jsp");accountsIndex.jsp
		response.sendRedirect("checkerapprovewithdrawal.jsp");
	}

	/**
	 *
	 */
	private String makerWithdrawCash() {

		Gson g = new Gson();

		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');
		transactioinid2 = StringUtils.remove(UUID.randomUUID().toString(), '-');

		checkerwithdrawalobject = checkerbalanceDAO.getCheckerWithdraw(checkerwithdrawuuid);

		if (checkerwithdrawalobject == null) {

			expected.put("status_code", fail);
			expected.put("status_description",
					"System error:-unable to locate this specific checker withdrawal Request");
			jsonResult = g.toJson(expected);
			return jsonResult;

		}
		try {
			originalwithdrawamount = checkerwithdrawalobject.getAmount();
			comissionamount = checkerwithdrawalobject.getComission();
			bankexchangerates = Double.parseDouble(bankexchangerate);
			withdrawalcharges = Double.parseDouble(transfercharge);

			// start calculculations.

			// original amount - comission amount = eligible amount for withdrawal
			eligiblewithdrawalamount = originalwithdrawamount - comissionamount;
			// comission amount * bankexchangerate = converted amount for withdrawwal
			convertedamount = eligiblewithdrawalamount * bankexchangerates;
			// receivable amount = converted amount for withdrawal-transfer charges

			receivableamount = convertedamount - withdrawalcharges;
		} catch (Exception e) {
			// TODO: handle exception
			expected.put("status_code", fail);
			expected.put("status_description",
					"System error:- Arithmetic computational error.Please retry after a few minutes or contact the system administrator");
			jsonResult = g.toJson(expected);
			return jsonResult;

		}

		if (receivableamount <= 0) {

			expected.put("status_code", fail);
			expected.put("status_description", "This Transaction afer all deductions are done results to a Negative or "
					+ "Zero value.Please Reject this transaction" + "\n" + "Original withdrawal amount "
					+ checkerwithdrawalobject.getAmount()
					+ " After withdrawing commissions Eligible amount for withdrawal is " + eligiblewithdrawalamount
					+ " " + countrycurrencycodemap.get(checkerwithdrawalobject.getCurrencyUuid()) + "\n"
					+ "Now the amount for withdrwal calculated with the bank Rate is " + convertedamount
					+ " when bank charges are removed the receivable amoungt would be " + receivableamount + " "
					+ countrycurrencycodemap.get(checkerwithdrawalobject.getTocurrencyUuid()));
			jsonResult = g.toJson(expected);
			return jsonResult;
		}
		// uopdate transaction inprogess
		// deduct the balance from the table take the balance to holding table

		withdrawalrequest = new CashWithdrawal();
		deductbalance = new CollectionBalanceHistory();
		putcollectionbalanceonhold = new InProgressMasterBalanceHoldHist();

		putcollectionbalanceonhold.setUuid(transactioinid2);
		putcollectionbalanceonhold.setAccountUuid(checkerwithdrawalobject.getAccountuuid());
		putcollectionbalanceonhold.setAmount(checkerwithdrawalobject.getAmount());
		putcollectionbalanceonhold.setCurrency(checkerwithdrawalobject.getCurrencyUuid());
		putcollectionbalanceonhold.setTransactionuuid(transactioinid);
		putcollectionbalanceonhold.setRefundedback(false);
		putcollectionbalanceonhold.setProcessed(false);
		putcollectionbalanceonhold.setTopuptime(new Date());

		deductbalance.setAccountuuid(checkerwithdrawalobject.getAccountuuid());
		deductbalance.setCountryuuid(checkerwithdrawalobject.getCurrencyUuid());
		deductbalance.setAmount(checkerwithdrawalobject.getAmount());

		withdrawalrequest.setUuid(transactioinid);
		withdrawalrequest.setAccountuuid(checkerwithdrawalobject.getAccountuuid());
		withdrawalrequest.setCurrencyUuid(checkerwithdrawalobject.getCurrencyUuid());
		withdrawalrequest.setTocurrencyUuid(checkerwithdrawalobject.getTocurrencyUuid());
		withdrawalrequest.setExtrainformation(checkerwithdrawalobject.getExtrainformation());
		withdrawalrequest.setAmount(checkerwithdrawalobject.getAmount());
		withdrawalrequest.setComission(checkerwithdrawalobject.getComission());
		withdrawalrequest.setComissioncurrencyUuid(checkerwithdrawalobject.getCurrencyUuid());
		withdrawalrequest.setAdminextrainformation(checkerwithdrawalobject.getAdminextrainformation());
		withdrawalrequest.setSystemexchangerate(checkerwithdrawalobject.getSystemexchangerate());

		withdrawalrequest.setTransactionStatusUuid(inprogresstransactionuuid);
		withdrawalrequest.setBankwithdrawexchangerate(bankexchangerates);
		withdrawalrequest.setAuthorisedchecker(checkerwithdrawalobject.getAuthorisedchecker());
		withdrawalrequest.setAuthorisedmaker(account.getUuid());
		withdrawalrequest.setTransfercharges(withdrawalcharges);
		withdrawalrequest.setTransferchargecurrencyUuid(checkerwithdrawalobject.getTocurrencyUuid());
		withdrawalrequest.setReceivableamount(receivableamount);

		withdrawalrequest.setTransactionDate(new Date());

		// accountDAO.addAccount(a);

		SystemLog systemlog = new SystemLog();
		transactioinid = StringUtils.remove(UUID.randomUUID().toString(), '-');
		systemlog.setUsername(username);
		systemlog.setUuid(transactioinid);
		systemlog.setAction(username + " deleted the checker withdrawal request with transactionuuid" + transactioinid);

		// move balance to withdrawal holding account

		if (checkerwithdrawalhistoryDAO.putCheckerWithdrawHistory(withdrawalrequest)
				&& checkerbalanceDAO.deleteCheckerWithdraw(checkerwithdrawuuid)
				&& collectionbalanceDAO.deductCollectionBalance(deductbalance)
				&& inprogressbalancehold.putBalanceOnHoldAccount(putcollectionbalanceonhold)) {
			systemlogDAO.putsystemlog(systemlog);

			expected.put("status_code", success);
			expected.put("status_description", "The checker approved withdrawal was succesfully");
			jsonResult = g.toJson(expected);
		} else {
			expected.put("status_code", fail);
			expected.put("status_description",
					"System unbale to save this withdrawal transaction in the Database.Please contact the system administrator");
			jsonResult = g.toJson(expected);

		}

		return jsonResult;

	}

	/**
	 * Set the class variables that represent form parameters.
	 *
	 * @param request
	 */
	private void setClassParameters(HttpServletRequest request) {

		username = StringUtils.trimToEmpty(request.getParameter("username"));
		checkerwithdrawuuid = StringUtils.trimToEmpty(request.getParameter("checkerwithdrawuuid"));
		transfercharge = StringUtils.trimToEmpty(request.getParameter("transfercharge"));
		bankexchangerate = StringUtils.trimToEmpty(request.getParameter("bankexchangerate"));
	}

	/**
	 * Place all the received parameters in our class HashMap.
	 *
	 */
	private void initParamHash() {
		paramHash = new HashMap<>();

		paramHash.put("username", username);
		paramHash.put("checkerwithdrawuuid", checkerwithdrawuuid);
		paramHash.put("transfercharge", transfercharge);
		paramHash.put("bankexchangerate", bankexchangerate);

	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		// response.sendRedirect("../index.jsp");
	}
}

/*
 * * Local Variables:* mode: java* c-basic-offset: 2* tab-width: 2*
 * indent-tabs-mode: nil* End:** ex: set softtabstop=2 tabstop=2 expandtab:*
 */
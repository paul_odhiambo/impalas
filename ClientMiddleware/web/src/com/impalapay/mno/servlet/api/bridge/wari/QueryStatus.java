package com.impalapay.mno.servlet.api.bridge.wari;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLBeyonic;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLKendy;

import net.sf.ehcache.CacheManager;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryStatus extends HttpServlet {

	private String CLIENT_URL1 = "", CLIENT_URL = "", CLIENT_URL2 = "";
	private PostWithIgnoreSSLKendy postIgnoreSsl;
	private PostMinusThread postMinusThread;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonObject auth = null;
		String responseobject = "";

		// These represent parameters received over the network
		String username = "", sessionid = "", referencenumber = "", receiveruuid = "", receiverquery = "",
				switchresponse = "", statusdescription = "";
		String accountType = "", accountID = "", salt = "", sessionurl = "", sessionkey = "", message = "",
				confirmationcode = "", responseobject1 = "", remiturlss = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			sessionid = root.getAsJsonObject().get("password").getAsString();
			referencenumber = root.getAsJsonObject().get("transactionid").getAsString();
			receiveruuid = root.getAsJsonObject().get("receivertransactionid").getAsString();
			receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		accountType = PropertiesConfig.getConfigValue("ACCOUNT_TYPE");
		;
		accountID = PropertiesConfig.getConfigValue("ACCOUNT_ID");
		salt = PropertiesConfig.getConfigValue("SALT");
		sessionurl = PropertiesConfig.getConfigValue("SESSION_URL");
		// ################################################################################
		// Request for session id
		// ################################################################################
		JsonObject sessionrequest = new JsonObject();

		sessionrequest.addProperty("username", username);
		sessionrequest.addProperty("password", sessionid);
		sessionrequest.addProperty("accountType", Integer.parseInt(accountType));
		sessionrequest.addProperty("accountID", accountID);
		sessionrequest.addProperty("salt", salt);

		String result2 = g.toJson(sessionrequest);

		CLIENT_URL = remiturlss;
		// CLIENT_URL1 = "https://localhost:8456/AirtelRemittanceImpalas/kendysession";
		postMinusThread = new PostMinusThread(CLIENT_URL1, result2);
		// capture the switch respoinse.

		try {
			responseobject1 = postMinusThread.doPost();
			// pass the returned json string
			roots = new JsonParser().parse(responseobject1);
			sessionkey = roots.getAsJsonObject().get("status_code").getAsString();
			message = roots.getAsJsonObject().get("status_description").getAsString();
		} catch (Exception e) {

			// expected.put("command_status",
			// APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS);
			expected.put("command_status", "unable to fetch session" + "  " + responseobject1);
			String jsonResult = g.toJson(expected);

			return responseobject1;

		}

		auth = new JsonObject();

		String shaUser = sessionkey + username;
		String shaPass = sessionkey + sessionid;

		// Hashing
		String usernames = DigestUtils.sha256Hex(shaUser);
		String passwords = DigestUtils.sha256Hex(shaPass);

		String newusername = usernames + salt;
		String newpassword = passwords + salt;

		auth.addProperty("user", newusername);
		auth.addProperty("pass", newpassword);
		auth.addProperty("accountType", Integer.parseInt(accountType));
		auth.addProperty("accountID", accountID);
		auth.addProperty("sessionKey", sessionkey);

		JsonObject queryrequest = new JsonObject();

		queryrequest.addProperty("reference", referencenumber);
		queryrequest.add("auth", auth);

		String results2 = g.toJson(queryrequest);

		// CLIENT_URL = receiverquery;
		CLIENT_URL = "http://46.101.131.249:7140/MTSAPI/rest/test/v1/getTransactionStatus";

		postIgnoreSsl = new PostWithIgnoreSSLKendy(CLIENT_URL, results2);

		try {

			// capture the switch respoinse.
			responseobject = postIgnoreSsl.doPost();
			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			switchresponse = roots.getAsJsonObject().get("mainStatus").getAsString();
			statusdescription = roots.getAsJsonObject().get("payStatus").getAsString();
		} catch (Exception e) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INTERNAL_ERROR);

			String jsonResult = g.toJson(expected);

			return jsonResult + responseobject;
		}
		/**
		 * 
		 * if (switchresponse.equalsIgnoreCase("processed") ||
		 * switchresponse.equalsIgnoreCase("validated")) { switchresponse = "S000";
		 * statusdescription = "SUCCESS"; }
		 * 
		 * // map error if (switchresponse.equalsIgnoreCase("processed_with_errors") ||
		 * switchresponse.equalsIgnoreCase("aborted")) { switchresponse = "00029"; }
		 * 
		 * // map ACCEPTED FOR INVALID AMOUNT if
		 * (switchresponse.equalsIgnoreCase("paused_for_admin_action") ||
		 * switchresponse.equalsIgnoreCase("queued") ||
		 * switchresponse.equalsIgnoreCase("paused") ||
		 * switchresponse.equalsIgnoreCase("processing") ||
		 * switchresponse.equalsIgnoreCase("scheduled") ||
		 * switchresponse.equalsIgnoreCase("pending_confirmation") ||
		 * switchresponse.equalsIgnoreCase("new")) { switchresponse = "S001";
		 * statusdescription = "CREDIT_IN_PROGRESS"; }
		 * 
		 * if (switchresponse.equalsIgnoreCase("rejected") ||
		 * switchresponse.equalsIgnoreCase("cancelled")) { switchresponse = "00029";
		 * statusdescription = roots.getAsJsonObject().get("last_error").getAsString();
		 * }
		 * 
		 * String success = "S000"; // String fail = "00126"; // String inprogress =
		 * "S001"; // rejected and canceled should be included.
		 * 
		 * if (switchresponse.equalsIgnoreCase(success)) {
		 * 
		 * expected.put("am_timestamp", username); expected.put("status_code",
		 * switchresponse); expected.put("status_description", statusdescription);
		 * String jsonResult = g.toJson(expected);
		 * 
		 * return jsonResult; }
		 * 
		 * expected.put("am_timestamp", username); expected.put("status_code",
		 * switchresponse); expected.put("status_description", statusdescription);
		 * String jsonResult = g.toJson(expected);
		 * 
		 * return jsonResult;
		 **/

		return responseobject;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

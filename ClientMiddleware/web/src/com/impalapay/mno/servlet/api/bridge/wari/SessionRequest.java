package com.impalapay.mno.servlet.api.bridge.wari;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLKendy;

import net.sf.ehcache.CacheManager;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class SessionRequest extends HttpServlet {

	private String CLIENT_URL = "";
	private PostWithIgnoreSSL postignoreSSL;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		String responseobject = "";

		// These represent parameters received over the network
		String username = "", password = "", statuscode = "", statusmessage = "", url = "", source = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			password = root.getAsJsonObject().get("password").getAsString();
			url = root.getAsJsonObject().get("url").getAsString();
			source = root.getAsJsonObject().get("source").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		JsonObject sessionrequest = new JsonObject();

		sessionrequest.addProperty("username", username);
		sessionrequest.addProperty("password", password);
		sessionrequest.addProperty("lang", "english");
		sessionrequest.addProperty("source", source);

		String results2 = g.toJson(sessionrequest);

		CLIENT_URL = url;
		postignoreSSL = new PostWithIgnoreSSL(CLIENT_URL, results2);

		try {

			responseobject = postignoreSSL.doPost();

			roots = new JsonParser().parse(responseobject);

			statusmessage = roots.getAsJsonObject().get("statusMessage").getAsString();
			statuscode = roots.getAsJsonObject().get("statusCode").getAsString();

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INTERNAL_ERROR + "_AUTHENTICATION");
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// capture session and return proper response if successfull.
		if (statuscode.equalsIgnoreCase("000")) {
			statuscode = "S000";
			expected.put("status_code", statuscode);
			expected.put("session_key", statusmessage);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		} else {
			statuscode = "00029";
		}
		expected.put("status_code", statuscode);
		expected.put("status_description", statusmessage);
		String jsonResult = g.toJson(expected);
		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

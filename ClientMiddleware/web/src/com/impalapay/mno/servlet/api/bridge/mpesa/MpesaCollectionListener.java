package com.impalapay.mno.servlet.api.bridge.mpesa;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.CacheManager;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostMinusThread;

public class MpesaCollectionListener extends HttpServlet {

	private PostMinusThread postMinusThread;
	private String CLIENT_URL = "";
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		JsonObject root2 = null, creditrequest = null, responsetoreceiver = null;

		String transactiontype = "", transactioinid = "", thirdptransactioinid = "", transactiontime = "",
				amountstring = "", shortcode = "", referencenumber = "", invoicenumber = "", accountbalance = "",
				sourcemsisdn = "", firstname = "", middlename = "", lastname = "", remiturlss = "", responseobject = "",
				statuscode = "", statusdescription = "";

		/**
		 * { "TransactionType":"", "TransID":"LGR219G3EY", "TransTime":"20170727104247",
		 * "TransAmount":"10.00",
		 * 
		 * "BusinessShortCode":"600134", "BillRefNumber":"xyz", "InvoiceNumber":"",
		 * "OrgAccountBalance":"49197.00", "ThirdPartyTransID":"1234567890",
		 * "MSISDN":"254708374149", "FirstName":"John", "MiddleName":"", "LastName":"" }
		 **/
		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		logger.error(".....................................................");
		logger.error("MPESA LISTENER REQUEST :" + join);
		logger.error(".....................................................");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			transactiontype = root.getAsJsonObject().get("TransType").getAsString();

			transactioinid = root.getAsJsonObject().get("TransID").getAsString();

			transactiontime = root.getAsJsonObject().get("TransTime").getAsString();

			amountstring = root.getAsJsonObject().get("TransAmount").getAsString();

			shortcode = root.getAsJsonObject().get("BusinessShortCode").getAsString();

			referencenumber = root.getAsJsonObject().get("BillRefNumber").getAsString();

			invoicenumber = root.getAsJsonObject().get("InvoiceNumber").getAsString();

			accountbalance = root.getAsJsonObject().get("OrgAccountBalance").getAsString();

			thirdptransactioinid = root.getAsJsonObject().get("ThirdPartyTransID").getAsString();

			sourcemsisdn = root.getAsJsonObject().get("MSISDN").getAsString();

			firstname = root.getAsJsonObject().get("FirstName").getAsString();

			middlename = root.getAsJsonObject().get("MiddleName").getAsString();

			lastname = root.getAsJsonObject().get("LastName").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(transactioinid) || StringUtils.isBlank(amountstring) || StringUtils.isBlank(shortcode)
				|| StringUtils.isBlank(referencenumber) || StringUtils.isBlank(accountbalance)
				|| StringUtils.isBlank(sourcemsisdn) || StringUtils.isBlank(firstname)
				|| StringUtils.isBlank(lastname)) {

			expected.put("status_code", "server error");
			expected.put("status_description", "missing parameters");
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// ################################################################################
		// construct a Mega-Json Object to route-transactions to recipient
		// systems.
		// ################################################################################

		creditrequest = new JsonObject();

		creditrequest.addProperty("collection_number", shortcode);
		creditrequest.addProperty("reference_number", referencenumber);
		creditrequest.addProperty("debitor_account", sourcemsisdn);
		creditrequest.addProperty("debitor_currency_code", "KES");
		creditrequest.addProperty("debitor_transaction_id", transactioinid);
		creditrequest.addProperty("amount", amountstring);
		creditrequest.addProperty("debitor_name", firstname + lastname);
		creditrequest.addProperty("collection_datetime", transactiontime);

		// assign the remit url from properties.config
		CLIENT_URL = PropertiesConfig.getConfigValue("COLLECTION_BRIDGEURL");

		String jsonData = g.toJson(creditrequest);

		postMinusThread = new PostMinusThread(CLIENT_URL, jsonData);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {

			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// exctract a specific json element from the object(status_code)
			statuscode = roots.getAsJsonObject().get("status_code").getAsString();

			// exctract a specific json element from the object(status_code)
			statusdescription = roots.getAsJsonObject().get("status_description").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			referencenumber = "investigateTransaction";
			statuscode = "00032";
			statusdescription = APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS;
		}

		logger.error(".....................................................");
		logger.error("MPESA LISTENER REQUEST RESPONSE FROM BRIDGE ");
		logger.error(".....................................................");
		logger.error("RESPONSE " + roots + "\n");

		// =====================================================================
		// construct object to return to sender system
		// =====================================================================
		responsetoreceiver = new JsonObject();
		responsetoreceiver.addProperty("am_referenceid", transactioinid);
		responsetoreceiver.addProperty("status_code", statuscode);
		responsetoreceiver.addProperty("status_description", statusdescription);

		return responsetoreceiver.toString();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}
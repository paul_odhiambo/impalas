package com.impalapay.mno.servlet.api.bridge.zeepay;

import java.util.HashMap;
import com.google.gson.Gson;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostMinusThread;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This Quartz job is used to check for Session Ids in the SessionLog database
 * table for expiry. Session Ids are expired after a fixed duration of time.
 * <p>
 * Copyright (c) Tawi Commercial Services Ltd., Sep 23, 2014
 * 
 * @author <a href="mailto:michael@tawi.mobi">Michael Wakahe</a>
 * 
 */

public class ZeepayPoll implements Job {

	private PostMinusThread postMinusThread;

	private String CLIENT_URL = "";
	private String responseobject = "", receiverurl = "", username = "", password = "", clientid = "",
			clientsecret = "", zeegranttype = "";

	private HashMap<String, String> expected = new HashMap<>();

	/**
	 * Empty constructor for job initialization
	 * <p>
	 * Quartz requires a public empty constructor so that the scheduler can
	 * instantiate the class whenever it needs.
	 */
	public ZeepayPoll() {

	}

	/**
	 * <p>
	 * Called by the <code>{@link org.quartz.Scheduler}</code> when a
	 * <code>{@link org.quartz.Trigger}</code> fires that is associated with the
	 * <code>Job</code>.
	 * </p>
	 * 
	 * @throws JobExecutionException
	 *             if there is an exception while executing the job.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Gson g = new Gson();

		receiverurl = PropertiesConfig.getConfigValue("ZEEAUTHURL");
		username = PropertiesConfig.getConfigValue("ZEE_USERNAME");
		password = PropertiesConfig.getConfigValue("ZEE_PASSWORD");
		clientid = PropertiesConfig.getConfigValue("ZEECLIENTID");
		clientsecret = PropertiesConfig.getConfigValue("ZEECLIENTSECRET");
		zeegranttype = PropertiesConfig.getConfigValue("ZEEGRANTYPE");
		CLIENT_URL = PropertiesConfig.getConfigValue("ZEEPAYINTERNAL_AUTHURL");

		expected.put("username", username);
		expected.put("password", password);
		expected.put("receiverqueryurl", receiverurl);
		expected.put("clientsecret", clientsecret);
		expected.put("clientid", clientid);
		expected.put("granttype", zeegranttype);

		String jsonforxData = g.toJson(expected);

		try {
			postMinusThread = new PostMinusThread(CLIENT_URL, jsonforxData);

			// capture the switch respoinse.
			responseobject = postMinusThread.doPost();

			System.out.println(jsonforxData + " response " + responseobject + " ");
		} catch (Exception e) {
			System.out.println(jsonforxData + " Ugmart session Request error" + responseobject);
		}

		// Perform post request.
	}

}

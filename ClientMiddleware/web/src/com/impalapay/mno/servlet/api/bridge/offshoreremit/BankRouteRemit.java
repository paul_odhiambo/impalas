package com.impalapay.mno.servlet.api.bridge.offshoreremit;

import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.util.NameSplitUtil;
import com.impalapay.airtel.util.PhonenumberSplitUtil;
import com.impalapay.airtel.util.net.PostWithIgnoreSSL;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLWari;

public class BankRouteRemit extends HttpServlet {

	private Cache accountsCache;
	private String CLIENT_URL = "", TOKEN = "", ACCOUNTCODE = "", APPLLICATION = "", TEXT_PATH = "";
	private PostWithIgnoreSSLWari postIgnoreWari;
	private PostWithIgnoreSSL postMinusThread;
	private MessageDigest md;
	private OffshoreSecretUtil myEncryptor;
	private NameSplitUtil splitnames, splitreceivername;
	private PhonenumberSplitUtil splitreference;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		// joined json string
		Account account = null;

		String join = "";
		JsonElement root = null, roots = null;
		JsonObject root2 = null, creditrequest = null, vendorfields = null, transactionrequestobject = null,
				queryobject = null, beforeencryptobject = null;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", sourcecountrycode = "",
				recipientcurrencycode = "", recipientcountrycode = "", sourcemsisdn = "", recipientmobile = "",
				sendername = "", amountstring = "", remiturlss = "", responseobject = "", statuscode = "",
				statusdescription = "", referencenumber = "", senderaddress = "", sendercity = "", accountnumber = "",
				bankcode = "", recipientname = "", branchcode = "", iban = "", thirdreference = "", statuscode1 = "",
				service_type = "Bank", jsonData = "", token = "", purpose = "remittance", fileContent = "",
				responsedata = "";

		String meanstype = "", clientid = "", sessionkey = "", orderidmodified = "", decrypted = "", jsonResult = "";

		// vendor Unique parameters for Wari
		String recipientfirstname = "", recipientlastname = "", recipientemail = "", senderemail = "", toencrypt = "",
				jsonEncrypt = "", data = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			sourcecountrycode = root.getAsJsonObject().get("sourcecountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("recipientcurrencycode").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("recipientcountrycode").getAsString();

			sourcemsisdn = root.getAsJsonObject().get("source_msisdn").getAsString();

			recipientmobile = root.getAsJsonObject().get("beneficiary_msisdn").getAsString();

			sendername = root.getAsJsonObject().get("Sender_Name").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();

			senderaddress = root.getAsJsonObject().get("sender_address").getAsString();

			sendercity = root.getAsJsonObject().get("sender_city").getAsString();

			bankcode = root.getAsJsonObject().get("bank_code").getAsString();

			branchcode = root.getAsJsonObject().get("branch_code").getAsString();

			iban = root.getAsJsonObject().get("iban").getAsString();

			accountnumber = root.getAsJsonObject().get("account_number").getAsString();

			recipientname = root.getAsJsonObject().get("recipient_name").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(sourcecountrycode)
				|| StringUtils.isBlank(recipientcurrencycode) || StringUtils.isBlank(sendername)
				|| StringUtils.isBlank(amountstring) || StringUtils.isBlank(remiturlss)
				|| StringUtils.isBlank(senderaddress) || StringUtils.isBlank(recipientmobile)
				|| StringUtils.isBlank(sourcemsisdn) || StringUtils.isBlank(sendercity) || StringUtils.isBlank(bankcode)
				|| StringUtils.isBlank(accountnumber) || StringUtils.isBlank(recipientcountrycode)
				|| StringUtils.isBlank(recipientname)) {

			expected.put("status_code", "server error");
			expected.put("status_description", "missing parameters");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// Secure against strange servers making request(future upgrade should
		// lock on IP)
		if (account == null) {
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		splitnames = new NameSplitUtil(sendername);
		splitreceivername = new NameSplitUtil(recipientname);
		splitreference = new PhonenumberSplitUtil();
		orderidmodified = splitreference.PhonenumberSplitUtil(transactioinid, 16);
		toencrypt = "curyCode=" + recipientcurrencycode + "&merchant=" + apiusername + "&merchantBatchId="
				+ transactioinid + "&transTotalNum=" + 1 + apipassword;

		transactionrequestobject = new JsonObject();
		queryobject = new JsonObject();
		beforeencryptobject = new JsonObject();

		// md = MessageDigest.getInstance("MD5");
		md.update(toencrypt.getBytes());

		byte byteData[] = md.digest();

		// convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}

		// System.out.println("Digest(in hex format):: " + sb.toString());

		// convert the byte to hex format method 2
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			String hex = Integer.toHexString(0xff & byteData[i]);
			if (hex.length() == 1)
				hexString.append('0');
			hexString.append(hex);
		}

		fileContent = orderidmodified + "|" + amountstring + "|" + splitnames.getFirstName() + "|"
				+ splitnames.getLastName() + "|" + sourcemsisdn + "|" + sourcecountrycode + "|" + senderaddress + "|"
				+ splitreceivername.getFirstName() + "|" + splitreceivername.getLastName() + "|" + accountnumber + "|"
				+ recipientmobile + "|" + purpose;

		beforeencryptobject.addProperty("merchant", "merchantcode");
		beforeencryptobject.addProperty("merchantBatchId", transactioinid);
		beforeencryptobject.addProperty("curyCode", recipientcurrencycode);
		beforeencryptobject.addProperty("transTotalNum", 1);
		beforeencryptobject.addProperty("fileContent", fileContent);
		beforeencryptobject.addProperty("sign", hexString.toString());
		beforeencryptobject.addProperty("transTotalAmt", amountstring);// amount should be converted to iso

		jsonEncrypt = g.toJson(beforeencryptobject);

		try {
			myEncryptor = new OffshoreSecretUtil();
			data = myEncryptor.encrypt(jsonEncrypt);
		} catch (Exception e) {
			expected.put("command_status", "HASHING_ERROR_CONTACT_ADMIN");
			jsonResult = g.toJson(expected);
			return jsonResult;
		}

		transactionrequestobject.addProperty("merchant", "merchantcode");
		transactionrequestobject.addProperty("data", data);

		// assign the remit url from properties.config
		// CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL");
		CLIENT_URL = remiturlss;

		jsonData = g.toJson(transactionrequestobject);
		System.out.println(jsonData);

		// postMinusThread = new PostMinusThread(CLIENT_URL, jsonData);
		postMinusThread = new PostWithIgnoreSSL(CLIENT_URL, jsonData);

		// =============================================================================
		// if step one does not execute it means the response is synchronous
		// the try catch is used to guard against bad response from the receiver
		// system.
		// =============================================================================

		try {

			// *******************************************************
			// capture the switch response.
			// *******************************************************
			responseobject = postMinusThread.doPost();
			System.out.println(responseobject);
			roots = new JsonParser().parse(responseobject);

			statuscode = roots.getAsJsonObject().get("code").getAsString();

			statusdescription = roots.getAsJsonObject().get("message").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			referencenumber = "investigateTransaction";
			statuscode = "00032";
			statusdescription = "COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS";
		}

		// map ACCEPTED FOR PROCESSING

		if (statuscode.equalsIgnoreCase("0000")) {
			statuscode = "S000";
			System.out.println("Transaction Successful");
		} else {
			statuscode = "00029";
			statusdescription = "FAILED_TRANSACTION";
		}

		String success = "S000";

		if (statuscode.equalsIgnoreCase(success)) {

			expected.put("am_referenceid", orderidmodified);
			expected.put("am_timestamp", username);
			expected.put("status_code", "S000");
			expected.put("status_description", "SUCCESS");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}
		expected.put("am_referenceid", orderidmodified);// orderidmodified is the order id
		expected.put("am_timestamp", username);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

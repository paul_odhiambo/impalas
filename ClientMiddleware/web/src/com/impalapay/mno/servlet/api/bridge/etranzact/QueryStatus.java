package com.impalapay.mno.servlet.api.bridge.etranzact;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.util.net.EtranzactStatusRequest;
import net.sf.ehcache.CacheManager;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryStatus extends HttpServlet {

	private String CLIENT_URL = "";
	private EtranzactStatusRequest statusCheck;
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		String responseobject = "";
		String direction = "request", action = "TS", description = "Status check";

		// These represent parameters received over the network
		String username = "", sessionid = "", referencenumber = "", receiveruuid = "", receiverquery = "",
				statuscode = "", statusdescription = "", jsonResult = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		logger.error(".....................................................");
		logger.error("ETRANZACT QUERYSTATUS BRIDGE REQUEST :" + join);
		logger.error(".....................................................");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();
			sessionid = root.getAsJsonObject().get("password").getAsString();
			referencenumber = root.getAsJsonObject().get("transactionid").getAsString();
			receiveruuid = root.getAsJsonObject().get("receivertransactionid").getAsString();
			receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();

		} catch (Exception e) {
			expected.put("status_code", "00032");
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		try {

			statusCheck = new EtranzactStatusRequest();

			// capture the switch respoinse.
			responseobject = statusCheck.EtranzactStatus(direction, action, username, sessionid, "0", referencenumber,
					description, receiverquery);
			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			statuscode = roots.getAsJsonObject().get("status_code").getAsString();

			statusdescription = roots.getAsJsonObject().get("message").getAsString();

			referencenumber = roots.getAsJsonObject().get("reference").getAsString();

			logger.error(".....................................................");
			logger.error("ETRANZACT STATUSQUERY BRIDGE RESPONSE FROM ETRANZACT ");
			logger.error(".....................................................");
			logger.error("RESPONSE " + roots + "\n");

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			referencenumber = "investigateTransaction";
			statuscode = "00032";
			statusdescription = "COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS";

		}

		// map ACCEPTED FOR SUCCESS
		if (statuscode.equalsIgnoreCase("0")) {
			statuscode = "S000";
		}

		// map ACCEPTED FOR PROCESSING
		if (statuscode.equalsIgnoreCase("31") || statuscode.equalsIgnoreCase("32")
				|| statuscode.equalsIgnoreCase("-1")|| statuscode.equalsIgnoreCase("1000")) {
			statuscode = "S001";
		}

		if (statuscode.equalsIgnoreCase("99")) {
			statuscode = "00029";
		}

		String success = "S000", pending = "S001", fail = "00029";

		if (statuscode.equalsIgnoreCase(success)) {

			expected.put("am_referenceid", referencenumber);
			expected.put("status_code", "S000");
			expected.put("status_description", statusdescription);

			jsonResult = g.toJson(expected);

			return jsonResult;
		}
		if (statuscode.equalsIgnoreCase(pending)) {

			expected.put("am_referenceid", referencenumber);
			expected.put("status_code", "S001");
			expected.put("status_description", statusdescription);

			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (statuscode.equalsIgnoreCase(fail)) {

			expected.put("am_referenceid", referencenumber);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);

			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("status_code", "00032");
		expected.put("status_description", statusdescription);
		expected.put("am_referenceid", referencenumber);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

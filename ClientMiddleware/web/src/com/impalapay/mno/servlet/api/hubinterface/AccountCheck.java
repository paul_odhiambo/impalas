package com.impalapay.mno.servlet.api.hubinterface;

import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.sessionlog.SessionLog;
import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.util.PhonenumberSplitUtil;
import com.impalapay.beans.network.Network;
import com.impalapay.beans.prefix.PrefixSplit;
import com.impalapay.beans.route.RouteDefine;
import com.impalapay.airtel.beans.clientipaddress.ClientIP;
import com.impalapay.persistence.routing.RouteDAO;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Allows for sending through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * 
 */
public class AccountCheck extends HttpServlet {

	private PhonenumberSplitUtil phonenumbersplit;

	private Cache accountsCache, countryCache, clientIpCache;
	private Cache networkCache, prefixCache;

	private SessionLogDAO sessionlogDAO;
	private RouteDAO routeDAO;

	private HashMap<String, String> countryHash = new HashMap<>();

	private HashMap<String, String> countryCode = new HashMap<>();

	private HashMap<String, String> countryUuid = new HashMap<>();

	private HashMap<String, String> countryIp = new HashMap<>();

	private HashMap<String, String> networkCode = new HashMap<>();

	private HashMap<String, Integer> mobilesplitlenght = new HashMap<>();

	private HashMap<String, String> prefixnumbernetworkHashmap = new HashMap<>();

	private HashMap<String, String> networkRemitUrlmap = new HashMap<>();

	private HashMap<String, String> networkBridgeRemitUrlmap = new HashMap<>();

	private HashMap<String, String> networkUsernamemap = new HashMap<>();

	private HashMap<String, String> networkPasswordmap = new HashMap<>();

	private HashMap<String, String> networkPartnernamemap = new HashMap<>();

	private HashMap<String, String> networkcountrymap = new HashMap<>();

	private HashMap<String, String> networknamemap = new HashMap<>();

	private HashMap<String, String> routeaccountnetworkmap = new HashMap<>();

	private HashMap<String, Boolean> routenetworkuuidmap = new HashMap<>();

	private HashMap<String, Double> routeoperatingbalancemap = new HashMap<>();

	private HashMap<String, String> clientipHash = new HashMap<>();

	private String phoneresults = "";

	private String networkroute = "", networkrouteuuid = "";

	private int prefixlength = 0;

	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);

		networkCache = mgr.getCache(CacheVariables.CACHE_NETWORK_BY_UUID);

		prefixCache = mgr.getCache(CacheVariables.CACHE_PREFIX_BY_UUID);

		clientIpCache = mgr.getCache(CacheVariables.CACHE_IPADDRESS_BY_UUID);

		sessionlogDAO = SessionLogDAO.getInstance();

		routeDAO = RouteDAO.getInstance();

		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		OutputStream out = response.getOutputStream();
		// responseobject
		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(sendMoney(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return JSon response
	 * @throws IOException
	 */
	private String sendMoney(HttpServletRequest request) throws IOException {
		Account account = null;
		// joined json string
		String join = "";
		JsonElement root = null;
		JsonObject vendorfields = null, root2 = null;

		// These represent parameters received over the network
		String username = "", sessionid = "", recipientmobile = "", recipientcurrencycode = "",
				recipientcountrycode = "", referencenumber = "", jsonResult = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), " ");

		// ###########################################################
		// instantiate the JSon
		// ##########################################################

		Gson g = new GsonBuilder().setPrettyPrinting().serializeNulls()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		JsonArray responsearray = new JsonArray();
		JsonObject responseobject = new JsonObject();
		JsonObject senderdata = new JsonObject();
		JsonObject arrayobject = new JsonObject();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("username").getAsString();

			sessionid = root.getAsJsonObject().get("password").getAsString();

			referencenumber = root.getAsJsonObject().get("hubtransaction_id").getAsString();

			recipientmobile = root.getAsJsonObject().get("destination_uri").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata = responsearray.get(i).getAsJsonObject();
			}
			responseobject.add("errors", senderdata);
			jsonResult = g.toJson(responseobject);

			return jsonResult;
		}

		if (root2.has("vendor_uniquefields")) {

			// fetch the recipient currency and country code from vendor unique
			// fiels

			vendorfields = root.getAsJsonObject().get("vendor_uniquefields").getAsJsonObject();

			try {
				recipientcurrencycode = vendorfields.getAsJsonObject().get("recipient_currency_code").getAsString();

				recipientcountrycode = vendorfields.getAsJsonObject().get("recipient_country_code").getAsString();

			} catch (Exception e) {

			}

		} else {
			// return an erroe indicating lack of unique field
		}

		// ################################################################
		// Guard against all invalid sessionid error
		// ################################################################
		// ip address module
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}

		// ####################################################################
		// check for the presence of all required parameters
		// ####################################################################

		if (StringUtils.isBlank(username) || StringUtils.isBlank(sessionid) || StringUtils.isBlank(recipientmobile)
				|| StringUtils.isBlank(recipientcurrencycode) || StringUtils.isBlank(recipientcountrycode)
				|| StringUtils.isBlank(referencenumber)) {

			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description", APIConstants.COMMANDSTATUS_INVALIDEMPTY_PARAMETERS);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata = responsearray.get(i).getAsJsonObject();
			}
			responseobject.add("errors", senderdata);
			jsonResult = g.toJson(responseobject);

			return jsonResult;
		}

		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		if (account == null) {
			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata = responsearray.get(i).getAsJsonObject();
			}
			responseobject.add("errors", senderdata);
			jsonResult = g.toJson(responseobject);

			return jsonResult;

		}

		List keys;

		// fetch from cache
		Country country;
		keys = countryCache.getKeys();
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryHash.put(country.getCountrycode(), country.getCurrencycode());
		}

		// country and country uuid
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryCode.put(country.getCountrycode(), country.getUuid());
		}
		// country uuid and country code
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryUuid.put(country.getUuid(), country.getCountrycode());
		}

		// country and country ip
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryIp.put(country.getCountrycode(), country.getCountryremitip());
		}

		// Countrycode and mobile splitlegth
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			mobilesplitlenght.put(country.getCountrycode(), country.getMobilesplitlength());
		}

		// **************Network Cache****************//

		Network network;
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkCode.put(network.getCountryUuid(), network.getPartnername());
		}

		// network and remiturl
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkRemitUrlmap.put(network.getUuid(), network.getRemitip());
		}
		// network and username
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkUsernamemap.put(network.getUuid(), network.getUsername());
		}

		// network and username
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networknamemap.put(network.getUuid(), network.getNetworkname());
		}

		// network and password
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkPasswordmap.put(network.getUuid(), network.getPassword());
		}

		// network and password
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkPartnernamemap.put(network.getUuid(), network.getPartnername());
		}

		// network and bridgeremiturl
		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkBridgeRemitUrlmap.put(network.getUuid(), network.getBridgeremitip());
		}

		keys = networkCache.getKeys();
		for (Object key : keys) {
			element = networkCache.get(key);
			network = (Network) element.getObjectValue();
			networkcountrymap.put(network.getUuid(), network.getCountryUuid());
		}

		// *************Prefix Cache****************//
		PrefixSplit prefixsplit;

		// prefixnumber and networkuuid
		keys = prefixCache.getKeys();
		for (Object key : keys) {
			element = prefixCache.get(key);
			prefixsplit = (PrefixSplit) element.getObjectValue();
			prefixnumbernetworkHashmap.put(prefixsplit.getPrefix(), prefixsplit.getNetworkUuid());
		}

		// fetch from cache
		ClientIP clientIP;
		keys = clientIpCache.getKeys();
		for (Object key : keys) {
			element = clientIpCache.get(key);
			clientIP = (ClientIP) element.getObjectValue();
			clientipHash.put(clientIP.getUuid(), clientIP.getIpAddress());
		}
		// compare remote address with the one stored in propertiesconfig
		if (!clientipHash.containsValue(ip)) {

			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description", APIConstants.COMMANDSTATUS_INVALID_IPADDRESS + " : " + ip);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata = responsearray.get(i).getAsJsonObject();
			}
			responseobject.add("errors", senderdata);
			jsonResult = g.toJson(responseobject);

			return jsonResult;

		}

		// ################################################################
		// Fetch route set up details
		// #################################################################
		List<RouteDefine> routedefine = routeDAO.getAllRoute(account);

		for (RouteDefine routenetworkuuid : routedefine) {
			routeaccountnetworkmap.put(routenetworkuuid.getNetworkUuid(), routenetworkuuid.getUuid());
			routenetworkuuidmap.put(routenetworkuuid.getUuid(), routenetworkuuid.isSupportforex());
			routeoperatingbalancemap.put(routenetworkuuid.getUuid(), routenetworkuuid.getMinimumbalance());
		}

		// checks for the provide currencyCode(invalid)
		if (!countryHash.containsValue(recipientcurrencycode)) {

			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description", APIConstants.COMMANDSTATUS_INVALID_CURRENCYCODE);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata = responsearray.get(i).getAsJsonObject();
			}
			responseobject.add("errors", senderdata);
			jsonResult = g.toJson(responseobject);

			return jsonResult;

		}

		// checks for the provided countryCode(invalid)
		if (!countryHash.containsKey(recipientcountrycode)) {

			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description", APIConstants.COMMANDSTATUS_INVALID_COUNTRYCODE);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata = responsearray.get(i).getAsJsonObject();
			}
			responseobject.add("errors", senderdata);
			jsonResult = g.toJson(responseobject);

			return jsonResult;

		}

		// =========================================================
		// determines if the provided recipient currencyCode doesn't
		// correspond to the countryCode
		// =========================================================

		if (!StringUtils.equalsIgnoreCase(countryHash.get(recipientcountrycode), recipientcurrencycode)) {

			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description", APIConstants.COMMANDSTATUS_CURRENCY_COUNTRYMISMATCH);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata = responsearray.get(i).getAsJsonObject();
			}
			responseobject.add("errors", senderdata);
			jsonResult = g.toJson(responseobject);

			return jsonResult;
		}

		// retrieve from countrycode hashmap uuid representing the provided
		// country
		String countrycodetodb = countryCode.get(recipientcountrycode);

		// select prefix.
		// checks for the provided receiver country code is included in the
		// prefix hash(invalid)
		if (!mobilesplitlenght.containsKey(recipientcountrycode)) {

			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description", APIConstants.COMMANDSTATUS_INVALID_COUNTRYCODE);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata = responsearray.get(i).getAsJsonObject();
			}
			responseobject.add("errors", senderdata);
			jsonResult = g.toJson(responseobject);

			return jsonResult;
		}

		// select the split length
		prefixlength = mobilesplitlenght.get(recipientcountrycode);
		// split the received phone number
		phonenumbersplit = new PhonenumberSplitUtil();
		phoneresults = phonenumbersplit.PhonenumberSplitUtil(recipientmobile, prefixlength);

		// check if the phonenumber matches the ones listed on hashmap.
		if (!prefixnumbernetworkHashmap.containsKey(phoneresults)) {
			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description", APIConstants.COMMANDSTATUS_NO_MSISDN_NETWORK_MATCH);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata = responsearray.get(i).getAsJsonObject();
			}
			responseobject.add("errors", senderdata);
			jsonResult = g.toJson(responseobject);

			return jsonResult;
		}

		// choose network which the number belongs to
		networkroute = prefixnumbernetworkHashmap.get(phoneresults);

		// check if the network is already configured in the routes table if not
		// return error
		networkrouteuuid = routeaccountnetworkmap.get(networkroute);
		if (StringUtils.isEmpty(networkrouteuuid)) {
			arrayobject.addProperty("status_code", "00032");
			arrayobject.addProperty("status_description", APIConstants.COMMANDSTATUS_ROUTEDEFINE_ERROR);
			responsearray.add(arrayobject);

			for (int i = 0; i < responsearray.size(); i++) {
				senderdata = responsearray.get(i).getAsJsonObject();
			}
			responseobject.add("errors", senderdata);
			jsonResult = g.toJson(responseobject);

			return jsonResult;

		}

		expected.put("hubtransaction_id", referencenumber);
		expected.put("status_code", "S000");
		expected.put("status_description", "ACTIVE");
		expected.put("basecurrency", recipientcurrencycode);
		jsonResult = g.toJson(expected);
		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

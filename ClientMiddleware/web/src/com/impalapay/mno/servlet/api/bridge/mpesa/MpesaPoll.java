package com.impalapay.mno.servlet.api.bridge.mpesa;

import java.util.HashMap;
import com.google.gson.Gson;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostMinusThread;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This Quartz job is used to check for Session Ids in the SessionLog database
 * table for expiry. Session Ids are expired after a fixed duration of time.
 * <p>
 * Copyright (c) Tawi Commercial Services Ltd., Sep 23, 2014
 * 
 * @author <a href="mailto:michael@tawi.mobi">Michael Wakahe</a>
 * 
 */

public class MpesaPoll implements Job {

	private PostMinusThread postMinusThread;

	private String CLIENT_URL = "";
	private String responseobject = "", receiverurl = "", username = "", password = "";

	private HashMap<String, String> expected = new HashMap<>();

	/**
	 * Empty constructor for job initialization
	 * <p>
	 * Quartz requires a public empty constructor so that the scheduler can
	 * instantiate the class whenever it needs.
	 */
	public MpesaPoll() {

	}

	/**
	 * <p>
	 * Called by the <code>{@link org.quartz.Scheduler}</code> when a
	 * <code>{@link org.quartz.Trigger}</code> fires that is associated with the
	 * <code>Job</code>.
	 * </p>
	 * 
	 * @throws JobExecutionException
	 *             if there is an exception while executing the job.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Gson g = new Gson();

		receiverurl = PropertiesConfig.getConfigValue("MPESAAUTHURL");
		username = PropertiesConfig.getConfigValue("MPESA_USERNAME");
		password = PropertiesConfig.getConfigValue("MPESA_PASSWORD");

		CLIENT_URL = PropertiesConfig.getConfigValue("MPESAINTERNAL_AUTHURL");

		expected.put("username", username);
		expected.put("password", password);
		expected.put("receiverqueryurl", receiverurl);

		String jsonforxData = g.toJson(expected);

		try {

			postMinusThread = new PostMinusThread(CLIENT_URL, jsonforxData);

			// capture the switch respoinse.
			responseobject = postMinusThread.doPost();

			System.out.println("Mpesa Session request " + jsonforxData + "\n" + " Mpesa session response "
					+ responseobject + " " + "\n");
		} catch (Exception e) {
			System.out.println(jsonforxData + " Mpesa session Request error" + responseobject);
		}

		// Perform post request.
	}

}

package com.impalapay.mno.servlet.api.bridge.mpesa;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLMpesa;
import com.impalapay.airtel.beans.accountmgmt.Account;

import net.sf.ehcache.CacheManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Allows for querying of status through an HTTP API.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Sep 31, 2014
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 * 
 */
public class QueryStatusOnline extends HttpServlet {

	private String CLIENT_URL = "", TOKEN = "", TEXT_PATH = "";
	private PostWithIgnoreSSLMpesa postMinusThread;
	private SimpleDateFormat sdf;
	private Timestamp timestamp;
	private Logger logger;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();
		sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		timestamp = new Timestamp(System.currentTimeMillis());
		logger = Logger.getLogger(this.getClass());

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent

		out.write(checkStatus(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkStatus(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null;
		String responseobject = "";

		// These represent parameters received over the network
		String apiusername = "", apipassword = "", referencenumber = "", receiveruuid = "", receiverquery = "",
				statuscode = "", statusdescription = "", mpesatransactionid = "", token = "", debitreferenceno = "",
				networkname = "", mytimestamp = "", baseencode = "", real_pass = "", requestid = "", jsonResult = "",
				results2 = "";

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###################################################################
		// instantiate the JSon
		// ###################################################################

		Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();
			apipassword = root.getAsJsonObject().get("password").getAsString();
			referencenumber = root.getAsJsonObject().get("transactionid").getAsString();
			receiveruuid = root.getAsJsonObject().get("receivertransactionid").getAsString();
			receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();

		} catch (Exception e) {

			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		TEXT_PATH = PropertiesConfig.getConfigValue("MPESATEXT_PATH");

		try {

			File file = new File(TEXT_PATH);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			token = bufferedReader.readLine();

			System.out.println("The file Path " + TEXT_PATH + " The token " + token);
			// System.out.println(token);

		} catch (Exception e) {
			// TODO: handle exception
			expected.put("status_code", "00032");
			expected.put("status_description", "Authentication Token Failure");
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		CLIENT_URL = receiverquery;
		TOKEN = token;

		mytimestamp = sdf.format(timestamp);
		baseencode = apiusername + apipassword + mytimestamp;
		byte[] bytesEncoded = Base64.encodeBase64(baseencode.getBytes());
		real_pass = new String(bytesEncoded);

		JsonObject queryrequest = new JsonObject();

		queryrequest.addProperty("BusinessShortCode", apiusername);
		queryrequest.addProperty("Password", real_pass);
		queryrequest.addProperty("Timestamp", mytimestamp);
		queryrequest.addProperty("CheckoutRequestID", receiveruuid);

		results2 = g.toJson(queryrequest);

		postMinusThread = new PostWithIgnoreSSLMpesa(CLIENT_URL, results2, TOKEN);

		try {

			// capture the switch respoinse.
			responseobject = postMinusThread.doPost();

			// pass the returned json string
			roots = new JsonParser().parse(responseobject);

			// ===============================================================
			// an object that will contain parameters for provisional response
			// ===============================================================
			// exctract a specific json element from the object(status_code)
			statuscode = roots.getAsJsonObject().get("ResultCode").getAsString();

			// exctract a specific json element from the object(status_code)
			statusdescription = roots.getAsJsonObject().get("ResultDesc").getAsString();

			requestid = roots.getAsJsonObject().get("ResultDesc").getAsString();

			mpesatransactionid = roots.getAsJsonObject().get("CheckoutRequestID").getAsString();

		} catch (Exception e) {

			// ================================================
			// Missing fields in response from receiver system
			// ================================================
			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS);
			jsonResult = g.toJson(expected);

			System.out.print("The response returned from Mpesa Query online " + roots);

			return jsonResult;

		}

		logger.error(".....................................................");
		logger.error("MPESA QUERY REQUEST FROM BRIDGE :" + results2 + "\n");
		logger.error("MPESA QUERY RESPONSE:" + roots + "\n");
		logger.error(".....................................................");

		// map SUCCESS TRANSACTION
		if (statuscode.equalsIgnoreCase("0")) {
			statuscode = "S000";
			statusdescription = "SUCCESS";
		} else {
			statuscode = "00029";
			statusdescription = "FAILED_TRANSACTION";
		}

		String success = "S000";
		String fail = "00029";
		if (statuscode.equalsIgnoreCase(success)) {

			expected.put("am_timestamp", apiusername);
			expected.put("am_referenceid", mpesatransactionid);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		if (statuscode.equalsIgnoreCase(fail)) {
			expected.put("am_timestamp", apiusername);
			expected.put("am_referenceid", mpesatransactionid);
			expected.put("status_code", statuscode);
			expected.put("status_description", statusdescription);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		expected.put("am_timestamp", apiusername);
		expected.put("am_referenceid", mpesatransactionid);
		expected.put("status_code", statuscode);
		expected.put("status_description", statusdescription);
		jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

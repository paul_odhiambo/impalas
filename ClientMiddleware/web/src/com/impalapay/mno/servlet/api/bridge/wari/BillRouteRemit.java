package com.impalapay.mno.servlet.api.bridge.wari;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.servlet.util.PropertiesConfig;
import com.impalapay.airtel.util.net.PostMinusThread;
import com.impalapay.airtel.util.net.PostWithIgnoreSSLWari;;

public class BillRouteRemit extends HttpServlet {

	// private PostWithIgnoreSSLBeyonic postIgnoreSslBeyonic;
	private Cache accountsCache;
	private String CLIENT_URL1 = "", CLIENT_URL = "", CLIENT_URL2 = "";
	private PostWithIgnoreSSLWari postIgnoreWari;
	private PostMinusThread postMinusThread;

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(SendRequest(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String SendRequest(HttpServletRequest request) throws IOException {
		Account account = null;

		// joined json string
		String join = "";
		JsonElement root = null, roots = null, roots3 = null;
		JsonObject root2 = null, creditrequest = null, funds = null, sender = null, beneficiary = null,
				paymentMeans = null, receiptMeans = null, confirmtransaction = null, vendorfields = null,
				requestData = null;

		String apiusername = "", apipassword = "", username = "", transactioinid = "", sourcecountrycode = "",
				recipientcurrencycode = "", recipientcountrycode = "", sourcemsisdn = "", recipientmobile = "",
				sendername = "", amountstring = "", remiturlss = "", responseobject = "", statuscode = "",
				statusdescription = "", referencenumber = "", senderaddress = "", sendercity = "", accountnumber = "",
				responseobject1 = "", responseobject2 = "", statuscode1 = "", thirdreference = "", merchantcode = "",
				recipientname = "", receivedmerchantid = "", merchantmsisdn = "";

		String meanstype = "", walletpovider = "", clientid = "", merchantid = "", sessionkey = "", message = "",
				localsessionurl = "", remotesessionurl = "", confirmationcode = "", localconfirmurl = "",
				remoteconfirmurl = "";

		// vendor Unique parameters for Wari
		String recipientfirstname = "", recipientlastname = "", recipientemail = "", senderemail = "";

		// Get all parameters, the keys of the parameters are specified
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ###############################################################################################
		// instantiate the JSon
		// Note
		// The = sign is encoded to \u003d. Hence you need to use
		// disableHtmlEscaping().
		// ###############################################################################################

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		// Gson g = new Gson();
		Map<String, String> expected = new HashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			apiusername = root.getAsJsonObject().get("username").getAsString();

			apipassword = root.getAsJsonObject().get("password").getAsString();

			username = root.getAsJsonObject().get("sendingIMT").getAsString();

			transactioinid = root.getAsJsonObject().get("transaction_id").getAsString();

			sourcecountrycode = root.getAsJsonObject().get("sourcecountrycode").getAsString();

			recipientcurrencycode = root.getAsJsonObject().get("recipientcurrencycode").getAsString();

			recipientcountrycode = root.getAsJsonObject().get("recipientcountrycode").getAsString();

			sendername = root.getAsJsonObject().get("Sender_Name").getAsString();

			amountstring = root.getAsJsonObject().get("amount").getAsString();

			remiturlss = root.getAsJsonObject().get("url").getAsString();

			sourcemsisdn = root.getAsJsonObject().get("source_msisdn").getAsString();

			recipientmobile = root.getAsJsonObject().get("beneficiary_msisdn").getAsString();

			senderaddress = root.getAsJsonObject().get("sender_address").getAsString();

			sendercity = root.getAsJsonObject().get("sender_city").getAsString();

			merchantcode = root.getAsJsonObject().get("merchant_code").getAsString();

			receivedmerchantid = root.getAsJsonObject().get("merchant_id").getAsString();

			merchantmsisdn = root.getAsJsonObject().get("msisdn").getAsString();

			accountnumber = root.getAsJsonObject().get("account_number").getAsString();

			recipientname = root.getAsJsonObject().get("recipient_name").getAsString();

			root2 = root.getAsJsonObject();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(apiusername) || StringUtils.isBlank(apipassword) || StringUtils.isBlank(username)
				|| StringUtils.isBlank(transactioinid) || StringUtils.isBlank(sourcecountrycode)
				|| StringUtils.isBlank(recipientcurrencycode) || StringUtils.isBlank(sendername)
				|| StringUtils.isBlank(amountstring) || StringUtils.isBlank(remiturlss)
				|| StringUtils.isBlank(recipientcountrycode)) {

			expected.put("status_code", "00032");
			expected.put("status_description", APIConstants.COMMANDSTATUS_INVALIDEMPTY_PARAMETERS);
			String jsonResult = g.toJson(expected);

			return jsonResult;
		}
		/***
		 * 
		 * if (root2.has("vendor_uniquefields")) { // This route requires some extra
		 * parametsers vendorfields =
		 * root.getAsJsonObject().get("vendor_uniquefields").getAsJsonObject(); try {
		 * recipientfirstname =
		 * vendorfields.getAsJsonObject().get("recipient_firstname").getAsString();
		 * 
		 * recipientlastname =
		 * vendorfields.getAsJsonObject().get("recipient_lastname").getAsString();
		 * 
		 * recipientemail =
		 * vendorfields.getAsJsonObject().get("recipient_email").getAsString();
		 * 
		 * senderemail =
		 * vendorfields.getAsJsonObject().get("sender_email").getAsString();
		 * 
		 * } catch (Exception e) { expected.put("status_code", "00032");
		 * expected.put("status_description",
		 * APIConstants.COMMANDSTATUS_VENDORUNIQUE_PARAMETERS_ERROR); String jsonResult
		 * = g.toJson(expected); return jsonResult;
		 * 
		 * }
		 * 
		 * } else { // return missing vendor unique object expected.put("status_code",
		 * "00032"); expected.put("status_description",
		 * APIConstants.COMMANDSTATUS_VENDORUNIQUE_ERROR); String jsonResult =
		 * g.toJson(expected); return jsonResult; } // Retrieve the account details
		 * Element element; if ((element = accountsCache.get(username)) != null) {
		 * account = (Account) element.getObjectValue(); }
		 * 
		 * // Secure against strange servers making request(future upgrade should //
		 * lock on IP) if (account == null) { expected.put("status_code", "00032");
		 * expected.put("status_description",
		 * APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME); String jsonResult =
		 * g.toJson(expected);
		 * 
		 * return jsonResult; }
		 * 
		 * //
		 * ################################################################################
		 * // fetch credentials from property file. //
		 * ###############################################################################
		 * meanstype = PropertiesConfig.getConfigValue("MEANS_TYPE"); walletpovider =
		 * PropertiesConfig.getConfigValue("WALLET_PROVIDER"); clientid =
		 * PropertiesConfig.getConfigValue("CLIENT_ID"); merchantid =
		 * PropertiesConfig.getConfigValue("MERCHANT_ID"); localsessionurl =
		 * PropertiesConfig.getConfigValue("LOCAL_SESSIONURL"); remotesessionurl =
		 * PropertiesConfig.getConfigValue("REMOTE_SESSIONURL"); localconfirmurl =
		 * PropertiesConfig.getConfigValue("LOCAL_WARICONFIRMURL"); remoteconfirmurl =
		 * PropertiesConfig.getConfigValue("REMOTE_WARICONFIRMURL"); //
		 * ################################################################################
		 * // Request for session id //
		 * ################################################################################
		 * JsonObject sessionrequest = new JsonObject();
		 * 
		 * sessionrequest.addProperty("username", apiusername);
		 * sessionrequest.addProperty("password", apipassword);
		 * sessionrequest.addProperty("url", remotesessionurl);
		 * sessionrequest.addProperty("source", walletpovider);
		 * 
		 * String result2 = g.toJson(sessionrequest);
		 * 
		 * CLIENT_URL1 = localsessionurl; // CLIENT_URL1 =
		 * "http://localhost:8092/ImpalasRemittance/warisession"; postMinusThread = new
		 * PostMinusThread(CLIENT_URL1, result2);
		 * 
		 * try { // capture the switch respoinse. responseobject1 =
		 * postMinusThread.doPost(); // pass the returned json string roots = new
		 * JsonParser().parse(responseobject1); statuscode =
		 * roots.getAsJsonObject().get("status_code").getAsString(); sessionkey =
		 * roots.getAsJsonObject().get("session_key").getAsString(); } catch (Exception
		 * e) { message =
		 * roots.getAsJsonObject().get("status_description").getAsString();
		 * expected.put("status_code", statuscode); expected.put("status_description",
		 * message); String jsonResult = g.toJson(expected); return jsonResult; }
		 * 
		 * //
		 * ################################################################################
		 * // construct a Mega-Json Object to route-transactions to recipient //
		 * systems. //
		 * ###############################################################################
		 * 
		 * funds = new JsonObject(); sender = new JsonObject(); beneficiary = new
		 * JsonObject(); paymentMeans = new JsonObject(); receiptMeans = new
		 * JsonObject(); requestData = new JsonObject();
		 * 
		 * // ######################################## // Funds //
		 * ######################################### funds.addProperty("amount",
		 * amountstring); funds.addProperty("currency", recipientcurrencycode); //
		 * ######################################## // Sender //
		 * ######################################### sender.addProperty("countryCode",
		 * sourcecountrycode); sender.addProperty("address", "90100");
		 * sender.addProperty("firstName", sendername); sender.addProperty("lastName",
		 * sendername); sender.addProperty("phoneNumber", recipientmobile);
		 * sender.addProperty("email", senderemail); sender.addProperty("pieceType",
		 * "CN"); sender.addProperty("pieceCode", "12356985478");
		 * sender.addProperty("pieceDeliveryDate", "20061122");
		 * sender.addProperty("pieceValidityDate", "20161122");
		 * 
		 * // ######################################## // Beneficiary //
		 * #########################################
		 * beneficiary.addProperty("countryCode", recipientcountrycode);
		 * beneficiary.addProperty("address", ""); beneficiary.addProperty("firstName",
		 * recipientfirstname); beneficiary.addProperty("lastName", recipientlastname);
		 * beneficiary.addProperty("phoneNumber", recipientmobile);
		 * beneficiary.addProperty("email", recipientemail);
		 * beneficiary.addProperty("pieceType", "CIN");
		 * beneficiary.addProperty("pieceCode", "12356985478");
		 * beneficiary.addProperty("pieceDeliveryDate", "");
		 * beneficiary.addProperty("pieceValidityDate", "");
		 * 
		 * // ######################################## // PaymentMeans //
		 * #########################################
		 * paymentMeans.addProperty("meansType", meanstype);
		 * paymentMeans.addProperty("cardType", "");
		 * paymentMeans.addProperty("cardNumber", "");
		 * paymentMeans.addProperty("accountPovider", "");
		 * paymentMeans.addProperty("banqueCode", "");
		 * paymentMeans.addProperty("branchCode", "");
		 * paymentMeans.addProperty("ribKey", "");
		 * paymentMeans.addProperty("accountNumber", "");
		 * paymentMeans.addProperty("walletPovider", walletpovider);
		 * paymentMeans.addProperty("walletNumber", "");
		 * paymentMeans.addProperty("voucherProvider", "");
		 * paymentMeans.addProperty("voucherNumber", "");
		 * paymentMeans.addProperty("waripassProvider", "");
		 * paymentMeans.addProperty("wariPassNumber", "");
		 * paymentMeans.addProperty("tokenValue", "");
		 * 
		 * // ######################################## // RecipientMeans //
		 * #########################################
		 * receiptMeans.addProperty("meansType", "CASH");
		 * receiptMeans.addProperty("wariPassNumber", "");
		 * receiptMeans.addProperty("voucherProvider", "P66BAB000015018");
		 * receiptMeans.addProperty("walletPovider", "");
		 * receiptMeans.addProperty("cardType", ""); receiptMeans.addProperty("ribKey",
		 * ""); receiptMeans.addProperty("accountPovider", "");
		 * receiptMeans.addProperty("accountNumber", "");
		 * receiptMeans.addProperty("waripassProvider", "");
		 * receiptMeans.addProperty("branchCode", "");
		 * receiptMeans.addProperty("voucherNumber", "");
		 * receiptMeans.addProperty("banqueCode", "");
		 * receiptMeans.addProperty("cardNumber", "");
		 * receiptMeans.addProperty("walletNumber", "");
		 * 
		 * creditrequest = new JsonObject(); creditrequest.addProperty("requestID",
		 * transactioinid); creditrequest.add("funds", funds);
		 * creditrequest.add("sender", sender); creditrequest.add("beneficiary",
		 * beneficiary); creditrequest.add("paymentMeans", paymentMeans);
		 * creditrequest.add("receiptMeans", receiptMeans);
		 * creditrequest.addProperty("source", walletpovider);
		 * 
		 * String jsonData = g.toJson(creditrequest);
		 * 
		 * // CLIENT_URL = PropertiesConfig.getConfigValue("SERVER_REMITURL"); //
		 * CLIENT_URL = remiturlss; // CLIENT_URL = //
		 * "http://46.101.131.249:7140/MTSAPI/rest/test/v1/startTransaction"; CLIENT_URL
		 * = remiturlss;
		 * 
		 * postIgnoreWari = new PostWithIgnoreSSLWari(CLIENT_URL, jsonData, sessionkey,
		 * clientid);
		 * 
		 * // ******************************************************* // capture the
		 * switch response. // *******************************************************
		 * //
		 * {"funds":{"currency":"XOF","amount":100.0,"fees":50.0},"transactionID":"1964503","requestID":"3456789","statusMessage":"SUCCESS","statusCode":"000"}
		 * 
		 * try { responseobject = postIgnoreWari.doPost(); // pass the returned json
		 * string roots = new JsonParser().parse(responseobject);
		 * 
		 * // exctract a specific json element from the object(status_code)
		 * thirdreference = roots.getAsJsonObject().get("transactionID").getAsString();
		 * statuscode = roots.getAsJsonObject().get("statusCode").getAsString();
		 * statusdescription =
		 * roots.getAsJsonObject().get("statusMessage").getAsString();
		 * 
		 * } catch (Exception e) { // ================================================
		 * // Missing fields in response from receiver system //
		 * ================================================ expected.put("status_code",
		 * "00032"); expected.put("status_description",
		 * APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS +
		 * responseobject); String jsonResult = g.toJson(expected);
		 * 
		 * return jsonResult; }
		 * 
		 * if (statuscode.equalsIgnoreCase("000")) { statuscode = "S001"; } else {
		 * statuscode = "00029"; }
		 * 
		 * String success = "S001";
		 * 
		 * if (statuscode.equalsIgnoreCase(success)) {
		 * 
		 * // confirm Transaction. confirmtransaction = new JsonObject();
		 * 
		 * confirmtransaction.addProperty("requestID", transactioinid);
		 * confirmtransaction.addProperty("meansType", meanstype);
		 * confirmtransaction.addProperty("meansValue", meanstype);
		 * confirmtransaction.addProperty("transID", thirdreference);
		 * confirmtransaction.addProperty("currency", recipientcurrencycode);
		 * confirmtransaction.addProperty("amount", amountstring);
		 * confirmtransaction.addProperty("merchantID", merchantid);
		 * confirmtransaction.addProperty("confirmurl", remoteconfirmurl);
		 * confirmtransaction.addProperty("clieintID", clientid);
		 * confirmtransaction.addProperty("SessionID", sessionkey);
		 * 
		 * String jsonData1 = g.toJson(confirmtransaction);
		 * 
		 * CLIENT_URL2 = localconfirmurl;
		 * 
		 * postMinusThread = new PostMinusThread(CLIENT_URL2, jsonData1);
		 * 
		 * try { responseobject2 = postMinusThread.doPost(); // exctract a specific json
		 * element from the object(status_code) statuscode1 =
		 * roots.getAsJsonObject().get("status_code").getAsString();
		 * 
		 * // exctract a specific json element from the object(status_code)
		 * statusdescription =
		 * roots.getAsJsonObject().get("status_description").getAsString();
		 * 
		 * } catch (Exception e) { expected.put("status_code", "00032");
		 * expected.put("status_description",
		 * APIConstants.COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS + responseobject2
		 * + jsonData1); String jsonResult = g.toJson(expected); return jsonResult; }
		 * 
		 * } // return response. expected.put("am_referenceid", thirdreference);
		 * expected.put("am_timestamp", username); expected.put("status_code",
		 * statuscode1); expected.put("status_description", statusdescription); String
		 * jsonResult = g.toJson(expected);
		 * 
		 * return jsonResult;
		 **/

		expected.put("am_referenceid", "billpayment123");
		expected.put("am_timestamp", username);
		expected.put("status_code", "S000");
		expected.put("status_description", "BILLPAYMENT_SUCCESS");
		String jsonResult = g.toJson(expected);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

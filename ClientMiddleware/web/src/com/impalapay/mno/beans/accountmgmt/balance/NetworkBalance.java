package com.impalapay.mno.beans.accountmgmt.balance;

import com.impalapay.airtel.beans.StorableBean;

/**
 * A generic balance of account
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public class NetworkBalance extends StorableBean {

	private double balance;

	private String networkUuid;

	/**
	 * 
	 */
	public NetworkBalance() {
		super();

		balance = 0;
		networkUuid = "";

	}

	public double getBalance() {
		return balance;
	}

	public String getNetworkUuid() {
		return networkUuid;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void setNetworkUuid(String networkUuid) {
		this.networkUuid = networkUuid;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NetworkBalance [getUuid()=");
		builder.append(getUuid());
		builder.append(", balance=");
		builder.append(balance);
		builder.append(", networkUuid=");
		builder.append(networkUuid);
		builder.append("]");
		return builder.toString();
	}

}

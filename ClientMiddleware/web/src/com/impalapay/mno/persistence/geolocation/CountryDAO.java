package com.impalapay.mno.persistence.geolocation;

import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.persistence.GenericDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.impalapay.mno.persistence.geolocation.AirtelCountryDAO;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

/**
 * Persistence implementation for {@link Country}
 * <p>
 * copyright (c) impalapay ltd., June 24, 2014
 *
 * @author <a href="mailto:eugenechimita@impalapay.com">Eugene Chimita</a>
 */
public class CountryDAO extends GenericDAO implements AirtelCountryDAO {

	private static CountryDAO countryDAO;

	private Logger logger = Logger.getLogger(this.getClass());

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 *
	 * @return {@link CountryDAO}
	 */
	public static CountryDAO getInstance() {
		if (countryDAO == null) {
			countryDAO = new CountryDAO();
		}

		return countryDAO;
	}

	/**
	 *
	 */
	protected CountryDAO() {
		super();
	}

	/**
	 *
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public CountryDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);
	}

	/**
	 * 
	 * com.impalapay.airtel.persistence.geolocation.AirtelCountryDAO
	 */
	@Override
	public Country getCountry(String uuid) {
		Country s = null;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM country WHERE uuid = ?;");
			pstmt.setString(1, uuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				s = beanProcessor.toBean(rset, Country.class);
				// s.setId(rset.getInt("id"));
			}

		} catch (SQLException e) {
			logger.error("SQLException while getting country with uuid '" + uuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return s;
	}

	/**
	 *
	 * com.impalapay.airtel.persistence.geolocation.AirtelCountryDAO
	 */
	@Override
	public List<Country> getAllCountries() {
		List<Country> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM country ORDER BY id ASC;");

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, Country.class);

		} catch (SQLException e) {
			logger.error("SQLException while getting all Countries.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public boolean PutCountry(Country country) {
		boolean success = true;
		Date date;

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"INSERT INTO country(uuid,name,countrycode,currency,currencycode,mobilesplitlength) VALUES (?, ?, ?, ?, ?, ?);");

			pstmt.setString(1, country.getUuid());
			pstmt.setString(2, country.getName());
			pstmt.setString(3, country.getCountrycode());
			pstmt.setString(4, country.getCurrency());
			pstmt.setString(5, country.getCurrencycode());
			pstmt.setInt(6, country.getMobilesplitlength());
			pstmt.execute();

		} catch (SQLException e) {
			logger.error("SQLException exception while adding " + country);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public boolean updateCountry(String countryuuid, Country a) {
		boolean success = true;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM country WHERE uuid=?;");
			pstmt.setString(1, countryuuid);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				pstmt2 = conn.prepareStatement(
						"UPDATE country SET name=?,countrycode=?,currency=?,currencycode=?,mobilesplitlength=? WHERE uuid=?;");

				pstmt2.setString(1, a.getName());
				pstmt2.setString(2, a.getCountrycode());
				pstmt2.setString(3, a.getCurrency());
				pstmt2.setString(4, a.getCurrencycode());
				pstmt2.setInt(5, a.getMobilesplitlength());
				pstmt2.setString(6, a.getUuid());

				pstmt2.executeUpdate();

			} else {
				success = PutCountry(a);

			}

		} catch (SQLException e) {
			logger.error("SQLException when trying to update country with uuid '" + countryuuid + "'");
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public List<Country> getAllCountries(int fromIndex, int toIndex) {
		List<Country> list = new ArrayList<>();
		Country s;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement("SELECT * FROM country ORDER BY name DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, Country.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all country from index " + fromIndex + " to index "
					+ toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}
}

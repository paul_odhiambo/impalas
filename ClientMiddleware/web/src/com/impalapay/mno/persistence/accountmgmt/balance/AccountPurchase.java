package com.impalapay.mno.persistence.accountmgmt.balance;

import java.util.List;

import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.mno.beans.accountmgmt.balance.AccountPurchaseByCountry;
import com.impalapay.mno.beans.accountmgmt.balance.MasterAccountFloatPurchase;
import com.impalapay.airtel.beans.geolocation.Country;

public interface AccountPurchase {
	/**
	 * 
	 * @param uuid
	 * @return the {@link MasterAccountFloatPurchase} with a corresponding UUID
	 */
	public MasterAccountFloatPurchase getMasterFloat(String uuid);

	/**
	 * To be called when the client master account float is debited.
	 * 
	 * @param purchase
	 * @return whether or not the purchase action was successful
	 */
	public boolean putMasterFloat(MasterAccountFloatPurchase purchase);

	/**
	 * This method is to be used infrequently, for example when there is an
	 * accidental entry of a master Float.
	 * 
	 * @param uuid
	 * @return whether or not the delete action was successful
	 */
	public boolean deleteMasterFloat(String uuid);

	/**
	 * Gets all float update made on the master accounts
	 * 
	 * @return a list of purchases made by the master account.
	 */
	public List<MasterAccountFloatPurchase> getMasterFloat();

	public List<MasterAccountFloatPurchase> getAllMasterFloat(int fromIndex, int toIndex);

	public List<MasterAccountFloatPurchase> getAllMasterFloatHistory(int fromIndex, int toIndex);

	public List<MasterAccountFloatPurchase> getAllMasterFloat(Account account, int fromIndex, int toIndex);

	public List<MasterAccountFloatPurchase> getAllMasterFloatHistory(Account account, int fromIndex, int toIndex);

	/**
	 * Gets all float update made on the master accounts belonging to this account
	 * 
	 * @return a list of purchases made by the master account.
	 */
	public List<MasterAccountFloatPurchase> getMasterFloat(Account account);

	/**
	 * @param uuid
	 * @return
	 */
	public AccountPurchaseByCountry getByCountryPurchase(String uuid);

	/**
	 * To be called when a client redeem/recharge float on a country.
	 * 
	 * @param purchase
	 * @return whether or not the recharge action was successful
	 */
	public boolean putClientPurchaseByCountry(AccountPurchaseByCountry purchase);

	public boolean putClientPurchaseByCountry2(AccountPurchaseByCountry purchase);

	/**
	 * This method is to be used infrequently, for example when there is an
	 * accidental entry of a float in a country.
	 * 
	 * @param uuid
	 * @return whether or not the delete action was successful
	 */
	public boolean deleteClientPurchaseByCountry(String uuid);

	/**
	 * Gets a list of {@link AccountPurchaseByCountry}s belonging to this account
	 * holder.
	 * 
	 * @param account
	 * @return a list of recharge done by this client account.
	 */
	public List<AccountPurchaseByCountry> getClientPurchasesByCountry(Account account);

	/**
	 * Returns a list of all {@link AccountPurchaseByCountry}s.
	 * 
	 * @return a list of recharge done by all client accounts.
	 */
	public List<AccountPurchaseByCountry> getAllClientPurchasesByCountry();

	/**
	 * Gets a list of {@link AccountPurchaseByCountry}s belonging to this account
	 * holder.
	 * 
	 * @param account
	 * @return a list of recharge done by this client account.
	 */
	public List<AccountPurchaseByCountry> getClientPurchasesByCountry(Account account, Country couuntry);

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<AccountPurchaseByCountry> getAllClientPurchasesByCountry(int fromIndex, int toIndex);

	public List<AccountPurchaseByCountry> getAllClientPurchasesByCountryHistory(int fromIndex, int toIndex);

	// Checker table
	// fetch checkermasterfloat based on uuid
	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public MasterAccountFloatPurchase getCheckerMasterFloat(String uuid);

	// delete checker masterfloat
	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public boolean deleteCheckerMasterFloat(String uuid);

	/**
	 * 
	 * @param purchase
	 * @return
	 */
	public boolean putCheckerMasterFloat(MasterAccountFloatPurchase purchase);

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<MasterAccountFloatPurchase> getAllCheckerMasterFloat(int fromIndex, int toIndex);

	// checker balancebycountry.

	public AccountPurchaseByCountry getCheckerBalanaceBycountry(String uuid);

	/**
	 * 
	 * @param purchase
	 * @return
	 */
	public boolean putCheckerBalanceByCountry2(AccountPurchaseByCountry purchase);

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	public boolean deleteCheckerBalanceByCountry(String uuid);

	public List<AccountPurchaseByCountry> getAllCheckerBalanceByCountry(int fromIndex, int toIndex);

}

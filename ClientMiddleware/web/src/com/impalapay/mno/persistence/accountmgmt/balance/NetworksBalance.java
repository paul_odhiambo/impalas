package com.impalapay.mno.persistence.accountmgmt.balance;

import java.util.List;

import com.impalapay.beans.network.Network;
import com.impalapay.mno.beans.accountmgmt.balance.NetworkBalance;

/**
 * Abstraction for persistence of balance in an the master account.
 * <p>
 * Copyright (c) ImpalaPay Ltd., Oct 12, 2014
 *
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 * @version %I%, %G%
 */
public interface NetworksBalance {

	/**
	 * Checks whether a user has a certain amount of balance.
	 * 
	 * 
	 * @param account
	 * @param amount
	 * @return whether this network has that balance.
	 */
	public boolean hasBalance(Network account, double amount);

	/**
	 * Adds a certain amount of balance on an account on a particular country.
	 * 
	 * @param account
	 * @param country
	 * @param amount
	 * @return whether it was successful when attempting to add the balance to this
	 *         account
	 */
	public boolean addBalance(Network account, double amount);

	/**
	 * 
	 * @param account
	 * @param country
	 * @return balance belonging to this network
	 */
	public double getBalance(Network account);

	/**
	 * 
	 * @param network
	 * @return
	 */

	public NetworkBalance getNetworkBalance(Network network);

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<NetworkBalance> getAllNetworkBalance(int fromIndex, int toIndex);

	/**
	 * 
	 * @return a list of all master balances
	 */
	public List<NetworkBalance> getAllNetworkBalance();

	/**
	 * 
	 * @param account
	 * @return balances belonging to this account
	 */
	// public getClientBalance1(Account account);

	public boolean deductBalance(Network account, double amount);

}

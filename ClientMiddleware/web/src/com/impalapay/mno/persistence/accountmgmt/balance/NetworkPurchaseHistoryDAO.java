package com.impalapay.mno.persistence.accountmgmt.balance;

import java.util.List;
import java.util.UUID;

import com.impalapay.airtel.persistence.GenericDAO;
import com.impalapay.beans.network.Network;
import com.impalapay.mno.beans.accountmgmt.balance.NetworkPurchaseHistory;
import com.impalapay.mno.persistence.network.NetworkDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

public class NetworkPurchaseHistoryDAO extends GenericDAO implements NetworksPurchaseHistory {

	public static NetworkPurchaseHistoryDAO accountpurchaseDAO;

	private NetworkBalanceDAO networkBalanceDAO;
	private NetworkDAO networkDAO;

	private Logger logger;

	private BeanProcessor beanProcessor = new BeanProcessor();

	/**
	 * 
	 * @return {@link TransactionDAO}
	 */
	public static NetworkPurchaseHistoryDAO getInstance() {

		if (accountpurchaseDAO == null) {
			accountpurchaseDAO = new NetworkPurchaseHistoryDAO();
		}

		return accountpurchaseDAO;
	}

	/**
	 * 
	 */
	public NetworkPurchaseHistoryDAO() {
		super();

		networkBalanceDAO = NetworkBalanceDAO.getInstance();
		networkDAO = NetworkDAO.getInstance();
		logger = Logger.getLogger(this.getClass());
	}

	/**
	 * 
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPassword
	 * @param dbPort
	 */
	public NetworkPurchaseHistoryDAO(String dbName, String dbHost, String dbUsername, String dbPassword, int dbPort) {
		super(dbName, dbHost, dbUsername, dbPassword, dbPort);

		networkBalanceDAO = new NetworkBalanceDAO(dbName, dbHost, dbUsername, dbPassword, dbPort);
		logger = Logger.getLogger(this.getClass());
	}

	@Override
	public NetworkPurchaseHistory getByNetworkPurchaseHistory(String uuid) {
		NetworkPurchaseHistory p = new NetworkPurchaseHistory();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		BeanProcessor b = new BeanProcessor();
		try {

			conn = dbCredentials.getConnection();

			pstmt = conn.prepareStatement("SELECT * FROM networktopupbycountry WHERE uuid=?;");

			pstmt.setString(1, uuid);
			rset = pstmt.executeQuery();

			while (rset.next()) {
				p = b.toBean(rset, NetworkPurchaseHistory.class);

			}
		} catch (SQLException e) {
			logger.error(
					"SQLException exception while getting a network topup by country " + "with uuid '" + uuid + "'.");
			logger.error(ExceptionUtils.getStackTrace(e));
		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return p;

	}

	@Override
	public boolean putNetworkPurchaseHistory(NetworkPurchaseHistory purchase) {
		boolean success = true;

		int networkBalanceId = 0;
		double networkBalance = 0;

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null, pstmt3 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			conn.setAutoCommit(false);

			pstmt = conn.prepareStatement("INSERT INTO networktopupbycountry (uuid, networkuuid , "
					+ "balance,topuptime) VALUES (?, ?, ?, ?);");

			pstmt.setString(1, purchase.getUuid());
			pstmt.setString(2, purchase.getNetworkUuid());
			pstmt.setDouble(3, purchase.getBalance());
			pstmt.setTimestamp(4, new Timestamp(purchase.getTopuptime().getTime()));

			pstmt.execute();

			// Credit
			pstmt2 = conn.prepareStatement("SELECT * FROM networkbalancebycountry WHERE networkUuid = ?;");

			pstmt2.setString(1, purchase.getNetworkUuid());
			rset = pstmt2.executeQuery();

			if (rset.next()) {
				networkBalanceId = rset.getInt("balanceid");
				networkBalance = rset.getDouble("balance");
			}

			// if master balance already exists, credit the balance
			if (networkBalanceId > 0) {
				pstmt3 = conn.prepareStatement("UPDATE networkbalancebycountry SET balance=? WHERE balanceid=?;");
				pstmt3.setDouble(1, networkBalance + purchase.getBalance());
				pstmt3.setInt(2, networkBalanceId);
				pstmt3.executeUpdate();

			} else {
				pstmt3 = conn.prepareStatement(
						"INSERT INTO networkbalancebycountry(uuid,networkuuid, balance) " + "VALUES(?,?,?);");

				pstmt3.setString(1, UUID.randomUUID().toString());
				pstmt3.setString(2, purchase.getNetworkUuid());
				pstmt3.setDouble(3, purchase.getBalance());
				pstmt3.execute();
			}

			conn.commit();

		} catch (SQLException e) {
			logger.error("SQLException exception while putting: " + purchase);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;
			try {
				conn.rollback();
			} catch (SQLException ex) {
			}

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return success;
	}

	@Override
	public boolean deleteNetworkPurchaseHistory(String uuid) {
		boolean success = true;

		double purchaseAmount = 0;
		String networkUuid = "";

		Connection conn = null;
		PreparedStatement pstmt = null, pstmt2 = null;
		ResultSet rset = null;

		try {
			conn = dbCredentials.getConnection();
			conn.setAutoCommit(false);

			pstmt = conn.prepareStatement("SELECT * FROM networktopupbycountry WHERE uuid=?;");
			pstmt.setString(1, uuid);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				networkUuid = rset.getString("networkuuid");
				purchaseAmount = rset.getDouble("amount");
			}

			// Delete the top up by country
			pstmt2 = conn.prepareStatement("DELETE FROM networktopupbycountry WHERE uuid=?;");
			pstmt2.setString(1, uuid);
			pstmt2.executeUpdate();

			// Debit the balance by country
			if (networkBalanceDAO.deductBalance(networkDAO.getNetwork(networkUuid), purchaseAmount)) {
				conn.commit();
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while deleting networktopupbycountry by country with uuid: " + uuid);
			logger.error(ExceptionUtils.getStackTrace(e));
			success = false;
			try {
				conn.rollback();
			} catch (SQLException ex) {
			}

		} finally {
			try {
				if (rset != null) {
					rset.close();
				}
				if (pstmt != null) {
					pstmt.close();
				}
				if (pstmt2 != null) {
					pstmt2.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (SQLException e) {
			}
		}

		return success;
	}

	@Override
	public List<NetworkPurchaseHistory> getNetworkPurchaseHistory(Network network) {
		List<NetworkPurchaseHistory> list = new ArrayList<>();
		NetworkPurchaseHistory ap;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		BeanProcessor b = new BeanProcessor();

		try {
			conn = dbCredentials.getConnection();

			// Get all top up by country
			pstmt = conn.prepareStatement("SELECT * FROM networktopupbycountry WHERE networkUuid=?;");
			pstmt.setString(1, network.getUuid());
			rset = pstmt.executeQuery();

			while (rset.next()) {
				ap = b.toBean(rset, NetworkPurchaseHistory.class);

				list.add(ap);
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting network top up by country history belonging to account"
					+ network + ".");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<NetworkPurchaseHistory> getAllNetworkPurchaseHistory() {
		List<NetworkPurchaseHistory> list = new ArrayList<>();
		NetworkPurchaseHistory ap;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		BeanProcessor b = new BeanProcessor();

		try {
			conn = dbCredentials.getConnection();

			// Get all top up by country
			pstmt = conn.prepareStatement("SELECT * FROM networktopupbycountry;");
			rset = pstmt.executeQuery();

			while (rset.next()) {
				ap = b.toBean(rset, NetworkPurchaseHistory.class);

				list.add(ap);
			}

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all network top up by country history.");
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

	@Override
	public List<NetworkPurchaseHistory> getAllNetworkPurchaseHistory(int fromIndex, int toIndex) {
		List<NetworkPurchaseHistory> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		/*
		 * SELECT * FROM account ORDER BY incomingSMSId DESC LIMIT 15 OFFSET 5;
		 */

		try {
			conn = dbCredentials.getConnection();
			pstmt = conn.prepareStatement(
					"SELECT * FROM networktopupbycountry ORDER BY networkuuid DESC LIMIT ? OFFSET ?;");
			pstmt.setInt(1, toIndex - fromIndex);
			pstmt.setInt(2, fromIndex);

			rset = pstmt.executeQuery();

			list = beanProcessor.toBeanList(rset, NetworkPurchaseHistory.class);

		} catch (SQLException e) {
			logger.error("SQLException exception while getting all networktopupbycountry from index " + fromIndex
					+ " to index " + toIndex);
			logger.error(ExceptionUtils.getStackTrace(e));

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
				}
			}

			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return list;
	}

}

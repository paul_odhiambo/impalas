package com.impalapay.mno.persistence.prefix;

import java.util.List;

import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.beans.network.Network;
import com.impalapay.beans.prefix.PrefixSplit;

public interface ImpalaPayPrefixDAO {
	/**
	 * Retrieve the PrefixSplit corresponding to the uuid.
	 * 
	 * @param uuid
	 * @return PrefixSplit
	 */

	public PrefixSplit getPrefixSplit(String uuid);

	/**
	 * 
	 * @param prefix
	 * @return
	 */
	public PrefixSplit getprefixsplit(String prefix);

	/**
	 * @param PrefixSplit
	 * @return whether the action was successful or not
	 */
	public boolean putPrefixSplit(PrefixSplit prefix);

	/**
	 * 
	 * @param uuid
	 * @param PrefixSplit
	 * @return
	 */
	boolean updatePrefixSplit(String uuid, PrefixSplit prefix);

	/**
	 * 
	 * @return
	 */
	public List<PrefixSplit> getAllPrefixSplit();

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<PrefixSplit> getAllPrefixSplit(int fromIndex, int toIndex);

	/**
	 * 
	 * @param network
	 * @param country
	 * @param prefix
	 * @return
	 */
	public PrefixSplit getprefixsplit(Network network, Country country, String prefix);

	/**
	 * 
	 * @param uuid
	 * @return
	 */
	boolean deleteprefix(String uuid);

	/**
	 * 
	 * @param prefix
	 * @return
	 */
	public List<PrefixSplit> getPrefixSplits(String prefix, String countryuuid);

}

package com.impalapay.mno.persistence.network;

import java.util.List;

import com.impalapay.beans.network.Network;
import com.impalapay.beans.prefix.PrefixSplit;

public interface ImpalaPayNetworkDAO {
	/**
	 * Retrieve the Network corresponding to the uuid.
	 * 
	 * @param uuid
	 * @return Network
	 */

	public Network getNetwork(String uuid);

	/**
	 * @param Network
	 * @return whether the action was successful or not
	 */
	public boolean putNetwork(Network network);

	/**
	 * 
	 * @param uuid
	 * @param Network
	 * @return
	 */
	boolean updateNetwork(String uuid, Network network);

	/**
	 * 
	 * @return
	 */
	public List<Network> getAllNetwork();

	/**
	 * 
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 */
	public List<Network> getAllNetwork(int fromIndex, int toIndex);

	/**
	 * 
	 * @param name
	 * @return
	 */
	public Network getNetwork(Network name);

	public List<Network> getAllNetwork(String uuid, int fromIndex, int toIndex);

}

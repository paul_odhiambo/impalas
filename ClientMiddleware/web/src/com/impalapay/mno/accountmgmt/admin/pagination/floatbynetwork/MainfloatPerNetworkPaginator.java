/**
 * Copyright 2015 Tawi Commercial Services Ltd
 * 
 * Licensed under the Open Software License, Version 3.0  you may
 * not use this file except in compliance with the License. You may obtain a copy
 * of the License at:
 * http://opensource.org/licenses/OSL-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an AS IS BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 * 
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package com.impalapay.mno.accountmgmt.admin.pagination.floatbynetwork;

import java.util.List;

import org.apache.log4j.Logger;

import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.mno.beans.accountmgmt.balance.NetworkBalance;
import com.impalapay.mno.persistence.accountmgmt.balance.NetworkBalanceDAO;

/**
 * Paginate an Inbox HTML view.
 * <p>
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 */
public class MainfloatPerNetworkPaginator {

	/**
	 *
	 */
	// public static final int PAGESIZE = 15; // The number of Incoming SMS to
	// display per page
	public static final int PAGESIZE = 12;
	private final NetworkBalanceDAO networkbalanceDAO;
	private final CountUtils countUtils;
	private Logger logger = Logger.getLogger(this.getClass());

	/**
	 *
	 * @param accountuuid
	 */
	public MainfloatPerNetworkPaginator() {

		countUtils = CountUtils.getInstance();
		networkbalanceDAO = NetworkBalanceDAO.getInstance();

	}

	/**
	 *
	 * @param email
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public MainfloatPerNetworkPaginator(String email, String dbName, String dbHost, String dbUsername, String dbPasswd,
			int dbPort) {

		// initialize the DAOs
		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);
		networkbalanceDAO = new NetworkBalanceDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);
	}

	/**
	 *
	 * @return
	 */
	public MainfloatPerNetworkPage getFirstPage() {

		MainfloatPerNetworkPage page = new MainfloatPerNetworkPage();

		List<NetworkBalance> prefixList = networkbalanceDAO.getAllNetworkBalance(0, PAGESIZE);

		page = new MainfloatPerNetworkPage(1, getTotalPage(), PAGESIZE, prefixList);
		// result = new IncomingSMSPage (1, getTotalPage(), PAGESIZE, smsList);

		return page;
	}

	/**
	 * Provides the last page of the accounts report
	 *
	 * @return accounts page
	 */
	public MainfloatPerNetworkPage getLastPage() {
		MainfloatPerNetworkPage page = new MainfloatPerNetworkPage();

		List<NetworkBalance> prefixList = null;
		int prefixCount, startIndex;
		int totalPage = getTotalPage();

		startIndex = (totalPage - 1) * PAGESIZE;
		// check later
		prefixCount = countUtils.getAllBalanceByNetworkCount();

		prefixList = networkbalanceDAO.getAllNetworkBalance(startIndex, prefixCount);

		page = new MainfloatPerNetworkPage(totalPage, totalPage, PAGESIZE, prefixList);

		return page;
	}

	/**
	 * Moves you forward to the page of the Accounts that comes after the current
	 * page
	 *
	 * @param currentPage
	 * @return an Account page
	 */
	public MainfloatPerNetworkPage getNextPage(final MainfloatPerNetworkPage currentPage) {
		int totalPage = getTotalPage();

		MainfloatPerNetworkPage page = new MainfloatPerNetworkPage();

		List<NetworkBalance> prefixList = networkbalanceDAO.getAllNetworkBalance(currentPage.getPageNum() * PAGESIZE,
				((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		page = new MainfloatPerNetworkPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, prefixList);

		return page;
	}

	/**
	 * Moves you backward to the page of the Accounts that comes before the current
	 * page
	 *
	 * @param currentPage
	 * @return an Account page
	 */
	public MainfloatPerNetworkPage getPrevPage(final MainfloatPerNetworkPage currentPage) {
		int totalPage = getTotalPage();

		MainfloatPerNetworkPage page = new MainfloatPerNetworkPage();

		List<NetworkBalance> prefixList = networkbalanceDAO.getAllNetworkBalance(
				(currentPage.getPageNum() - 2) * PAGESIZE, ((currentPage.getPageNum() - 1) * PAGESIZE));

		page = new MainfloatPerNetworkPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, prefixList);

		return page;
	}

	/**
	 * Calculates the total number of pages that would be printed for the Account
	 * sessions that belong to the logged-in account
	 *
	 * @return an integer
	 */
	public int getTotalPage() {
		int totalSize = 0;

		// get the number of all sessions belonging to this email
		totalSize = countUtils.getAllBalanceByNetworkCount();

		// TODO: divide by the page size and add one to take care of remainders
		// and what else?
		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

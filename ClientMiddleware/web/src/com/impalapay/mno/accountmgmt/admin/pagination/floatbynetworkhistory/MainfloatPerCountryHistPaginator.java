/**
 * Copyright 2015 Tawi Commercial Services Ltd
 * 
 * Licensed under the Open Software License, Version 3.0  you may
 * not use this file except in compliance with the License. You may obtain a copy
 * of the License at:
 * http://opensource.org/licenses/OSL-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an AS IS BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 * 
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package com.impalapay.mno.accountmgmt.admin.pagination.floatbynetworkhistory;

import java.util.List;

import org.apache.log4j.Logger;

import com.impalapay.airtel.accountmgmt.admin.persistence.util.CountUtils;
import com.impalapay.mno.beans.accountmgmt.balance.NetworkPurchaseHistory;
import com.impalapay.mno.persistence.accountmgmt.balance.NetworkPurchaseHistoryDAO;

/**
 * Paginate an Inbox HTML view.
 * <p>
 * 
 * @author <a href="mailto:eugene@impalapay.com">Eugene Chimita</a>
 */
public class MainfloatPerCountryHistPaginator {

	/**
	 *
	 */
	// public static final int PAGESIZE = 15; // The number of Incoming SMS to
	// display per page
	public static final int PAGESIZE = 12;
	private final NetworkPurchaseHistoryDAO networkpurchaseDAO;
	private final CountUtils countUtils;
	private Logger logger = Logger.getLogger(this.getClass());

	/**
	 *
	 * @param accountuuid
	 */
	public MainfloatPerCountryHistPaginator() {

		countUtils = CountUtils.getInstance();
		networkpurchaseDAO = NetworkPurchaseHistoryDAO.getInstance();

	}

	/**
	 *
	 * @param email
	 * @param dbName
	 * @param dbHost
	 * @param dbUsername
	 * @param dbPasswd
	 * @param dbPort
	 */
	public MainfloatPerCountryHistPaginator(String email, String dbName, String dbHost, String dbUsername,
			String dbPasswd, int dbPort) {

		// initialize the DAOs
		countUtils = new CountUtils(dbName, dbHost, dbUsername, dbPasswd, dbPort);
		networkpurchaseDAO = new NetworkPurchaseHistoryDAO(dbName, dbHost, dbUsername, dbPasswd, dbPort);
	}

	/**
	 *
	 * @return
	 */
	public MainfloatPerCountryHistPage getFirstPage() {

		MainfloatPerCountryHistPage page = new MainfloatPerCountryHistPage();

		List<NetworkPurchaseHistory> prefixList = networkpurchaseDAO.getAllNetworkPurchaseHistory(0, PAGESIZE);

		page = new MainfloatPerCountryHistPage(1, getTotalPage(), PAGESIZE, prefixList);
		// result = new IncomingSMSPage (1, getTotalPage(), PAGESIZE, smsList);

		return page;
	}

	/**
	 * Provides the last page of the accounts report
	 *
	 * @return accounts page
	 */
	public MainfloatPerCountryHistPage getLastPage() {
		MainfloatPerCountryHistPage page = new MainfloatPerCountryHistPage();

		List<NetworkPurchaseHistory> prefixList = null;
		int prefixCount, startIndex;
		int totalPage = getTotalPage();

		startIndex = (totalPage - 1) * PAGESIZE;
		prefixCount = countUtils.getAllBalanceByNetworkHistoryCount();

		prefixList = networkpurchaseDAO.getAllNetworkPurchaseHistory(startIndex, prefixCount);

		page = new MainfloatPerCountryHistPage(totalPage, totalPage, PAGESIZE, prefixList);

		return page;
	}

	/**
	 * Moves you forward to the page of the Accounts that comes after the current
	 * page
	 *
	 * @param currentPage
	 * @return an Account page
	 */
	public MainfloatPerCountryHistPage getNextPage(final MainfloatPerCountryHistPage currentPage) {
		int totalPage = getTotalPage();

		MainfloatPerCountryHistPage page = new MainfloatPerCountryHistPage();

		List<NetworkPurchaseHistory> prefixList = networkpurchaseDAO.getAllNetworkPurchaseHistory(
				currentPage.getPageNum() * PAGESIZE, ((currentPage.getPageNum() * PAGESIZE) + PAGESIZE));

		page = new MainfloatPerCountryHistPage(currentPage.getPageNum() + 1, totalPage, PAGESIZE, prefixList);

		return page;
	}

	/**
	 * Moves you backward to the page of the Accounts that comes before the current
	 * page
	 *
	 * @param currentPage
	 * @return an Account page
	 */
	public MainfloatPerCountryHistPage getPrevPage(final MainfloatPerCountryHistPage currentPage) {
		int totalPage = getTotalPage();

		MainfloatPerCountryHistPage page = new MainfloatPerCountryHistPage();

		List<NetworkPurchaseHistory> prefixList = networkpurchaseDAO.getAllNetworkPurchaseHistory(
				(currentPage.getPageNum() - 2) * PAGESIZE, ((currentPage.getPageNum() - 1) * PAGESIZE));

		page = new MainfloatPerCountryHistPage(currentPage.getPageNum() - 1, totalPage, PAGESIZE, prefixList);

		return page;
	}

	/**
	 * Calculates the total number of pages that would be printed for the Account
	 * sessions that belong to the logged-in account
	 *
	 * @return an integer
	 */
	public int getTotalPage() {
		int totalSize = 0;

		// get the number of all sessions belonging to this email
		totalSize = countUtils.getAllBalanceByNetworkHistoryCount();

		// TODO: divide by the page size and add one to take care of remainders
		// and what else?
		return ((totalSize - 1) / PAGESIZE) + 1;
	}
}

package com.impalapay.billpayment.servlet.api.remit;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.impalapay.airtel.beans.accountmgmt.Account;
import com.impalapay.airtel.beans.billpayment.BillPaymentCodes;
import com.impalapay.airtel.beans.geolocation.Country;
import com.impalapay.airtel.beans.sessionlog.SessionLog;
import com.impalapay.airtel.cache.CacheVariables;
import com.impalapay.airtel.persistence.sessionlog.SessionLogDAO;
import com.impalapay.airtel.servlet.api.APIConstants;
import com.impalapay.airtel.util.SecurityUtil;

public class QueryBillPayment extends HttpServlet {
	private Cache accountsCache, billpaymentCache, countryCache;

	private SessionLogDAO sessionlogDAO;

	private HashMap<String, JsonObject> billpaymentHash = new HashMap<>();

	private HashMap<String, String> countryHash = new HashMap<>();

	/**
	 * 
	 * @param config
	 * @throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		CacheManager mgr = CacheManager.getInstance();

		accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);

		billpaymentCache = mgr.getCache(CacheVariables.CACHE_BILLPAYMENTCODES_BY_UUID);

		countryCache = mgr.getCache(CacheVariables.CACHE_COUNTRY_BY_UUID);

		sessionlogDAO = SessionLogDAO.getInstance();

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream out = response.getOutputStream();

		response.setContentType("text/plain;charset=UTF-8");
		response.setDateHeader("Expires", new Date().getTime()); // Expiration
																	// date
		response.setDateHeader("Date", new Date().getTime()); // Date and time
																// that the
																// message was
																// sent
		out.write(checkForex(request).getBytes());
		out.flush();
		out.close();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private String checkForex(HttpServletRequest request) throws IOException {
		Account account = null;
		BillPaymentCodes billpaymentcode = null;

		// These represent parameters received over the network
		String username = "", sessionid = "", jsonResult = "", jsonData = "";
		String join = "";
		JsonElement root = null;
		JsonObject bankelements = null, mainrateobject = null, billpaymentresult = null;
		JsonArray mainarray;

		// Get all parameters
		List<String> lines = IOUtils.readLines(request.getReader());

		// used to format/join incoming JSon string
		join = StringUtils.join(lines.toArray(), "");

		// ##################################################################
		// instantiate the JSon
		// ##################################################################

		Gson g = new GsonBuilder().setPrettyPrinting().serializeNulls()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		LinkedHashMap<String, String> expected = new LinkedHashMap<>();

		try {
			// parse the JSon string
			root = new JsonParser().parse(join);

			username = root.getAsJsonObject().get("api_username").getAsString();

			sessionid = root.getAsJsonObject().get("session_id").getAsString();

		} catch (Exception e) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// check for the presence of all required parameters
		if (StringUtils.isBlank(username) || StringUtils.isBlank(sessionid)) {

			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// Retrieve the account details then check against username and
		// sessionid
		Element element;
		if ((element = accountsCache.get(username)) != null) {
			account = (Account) element.getObjectValue();
		}

		// unknown username
		if (account == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_UNKNOWN_USERNAME);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		// test for invalid sessionid
		SessionLog sessionlog = sessionlogDAO.getValidSessionLog(account);

		// ################################################################
		// Guard against all invalid sessionid error
		// ################################################################

		if (sessionlog == null) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			jsonResult = g.toJson(expected);

			return jsonResult;
		}

		String session = sessionlog.getSessionUuid();
		if (!StringUtils.equals(SecurityUtil.getMD5Hash(sessionid), session)) {
			expected.put("command_status", APIConstants.COMMANDSTATUS_INVALID_SESSIONID);
			jsonResult = g.toJson(expected);

			return jsonResult;

		}

		List keys;
		Country country;
		keys = countryCache.getKeys();
		// place country uuid and country code in a hashmap
		for (Object key : keys) {
			element = countryCache.get(key);
			country = (Country) element.getObjectValue();
			countryHash.put(country.getUuid(), country.getName());
		}

		mainarray = new JsonArray();
		keys = billpaymentCache.getKeys();
		for (Object key : keys) {
			element = billpaymentCache.get(key);
			billpaymentcode = (BillPaymentCodes) element.getObjectValue();

			bankelements = new JsonObject();

			bankelements.addProperty("MerchnatName", billpaymentcode.getMerchantname());
			bankelements.addProperty("Merchantcode", billpaymentcode.getMerchantcode());
			bankelements.addProperty("Country", countryHash.get(billpaymentcode.getCountryuuid()));

			mainarray.add(bankelements);
			// billpaymentHash.put(billpaymentcode.getMerchantname(),
			// bankelements);
			// countryHash.put(country.getCountrycode(),
			// country.getCurrencycode());
		}

		// jsonData = g.toJson(billpaymentHash);
		// JsonParser parser = new JsonParser();
		// mainrateobject = parser.parse(jsonData).getAsJsonObject();
		// mainarray = new JsonArray();
		// mainarray.add(mainrateobject);

		billpaymentresult = new JsonObject();
		billpaymentresult.addProperty("api_username", username);
		billpaymentresult.add("Merchant_Numbers", mainarray);

		jsonResult = g.toJson(billpaymentresult);

		return jsonResult;

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 *             , IOException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}

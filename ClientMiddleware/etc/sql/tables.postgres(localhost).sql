-- Schema Name: remittancedb
-- Username: remittance
-- Password: gertedNen2 

-- This tables describe the database of the Airtel Gateway.

-- Make sure you have created a Postgres user with the above username, password
-- and appropriate permissions. For development environments, you can make the 
-- database user to be a superuser to allow for copying of external files. 
-- A postgres user with super user priviledges can be created with the following
-- command:
-- CREATE USER username WITH SUPERUSER PASSWORD 'password';
-- Then run the "dbSetup.sh" script in the bin folder of this project.
-- eugene

\c postgres

-- Then execute the following:
-- DROP DATABASE IF EXISTS remittancedb; 
-- To drop a database you can't be logged into it.
-- CREATE DATABASE remittancedb;

\c remittancedb

SET datestyle to ISO;


-- =========================
-- 1.Static Reference Data
-- =========================

-- ----------------
-- Table country
-- ----------------

CREATE TABLE country (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    name text UNIQUE NOT NULL,
    countrycode text UNIQUE NOT NULL,
    currency text NOT NULL,
    currencycode text NOT NULL,
    mobilesplitlength int NOT NULL
    -- airtelnetwork text UNIQUE NOT NULL,
    -- countryremitip text UNIQUE,
    -- countrybalanceip text UNIQUE,
    -- countryverifyip text UNIQUE,
    -- username text,
    -- password text
);

\COPY country(uuid,name,countrycode,currency,currencycode,mobilesplitlength) FROM '/tmp/Countries.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE country OWNER TO remittance;




-- ==================================
-- 2. Accounts and Session Management
-- ==================================

-- --------------------
-- Table accountStatus
-- --------------------
CREATE TABLE accountStatus (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    description text UNIQUE NOT NULL
);

\COPY accountStatus(uuid, description) FROM '/tmp/AccountStatus.csv' WITH DELIMITER AS '|'
ALTER TABLE accountStatus OWNER TO remittance;

-- --------------------
-- Table account
-- --------------------
CREATE TABLE account (
    accountId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    accountStatusUuid text ,
    firstName text NOT NULL,
    lastName text,
    username text NOT NULL,
    loginPasswd text NOT NULL,
    apiUsername text NOT NULL,
    apiPasswd text NOT NULL,
    email text NOT NULL,
    phone text,
    accounttype text,
    creationDate timestamp with time zone NOT NULL DEFAULT now(),
    UNIQUE (username, email)
);

\COPY account(uuid, accountStatusUuid, firstName, lastName, username, loginPasswd,apiUsername, apiPasswd, email, phone,accounttype, creationDate) FROM '/tmp/Accounts.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE account OWNER TO remittance;

-- --------------------
-- Table Management account
-- --------------------
CREATE TABLE managementaccount(
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    accountStatusUuid text,
    accountName text NOT NULL,
    username text NOT NULL,
    loginPasswd text NOT NULL,
    updateforex boolean,
    updatebalance boolean,
    updatereversal boolean,
    checker boolean,
    creationDate timestamp with time zone NOT NULL DEFAULT now()
);

\COPY managementaccount(uuid, accountStatusUuid, accountName, username, loginPasswd,updateforex,updatebalance,updatereversal,checker,creationDate) FROM '/tmp/Managementaccount.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE managementaccount OWNER TO remittance;

-- ----------------
-- Table sessionlog
-- ----------------
CREATE TABLE sessionlog (
   id SERIAL PRIMARY KEY,
   sessionUuid text UNIQUE NOT NULL, 
   accountUuid text REFERENCES account(uuid),
   creationTime timestamp with time zone NOT NULL DEFAULT now(),
   valid boolean
   );

\COPY sessionlog(sessionuuid,accountuuid,creationtime,valid) FROM '/tmp/Sessionlogs.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE sessionlog OWNER TO remittance;


-- ----------------
-- Table Client Urls
-- ----------------
CREATE TABLE clienturl (
   id SERIAL PRIMARY KEY,
   uuid text UNIQUE NOT NULL,
   accountUuid text REFERENCES account(uuid),
   url  text NOT NULL,
   dateActive timestamp with time zone NOT NULL DEFAULT now(),
   dateInactive timestamp with time zone,
   active boolean
);

\COPY clienturl(uuid, accountUuid, url, dateActive, dateInactive, active) FROM '/tmp/ClientUrls.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE clienturl OWNER TO remittance;

-- ==================================
-- 3 Forex Data
-- ==================================

-- --------------------
-- Table USDFOREX
-- --------------------

CREATE TABLE usdforex (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    countryUuid text UNIQUE  REFERENCES country(uuid),
    baserate double precision NOT NULL,
    impalarate double precision NOT NULL
   
   
);

\COPY usdforex(uuid, countryUuid, baserate, impalarate) FROM '/tmp/usdforex.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE usdforex OWNER TO remittance;

-- --------------------
-- Table GBPFOREX
-- --------------------

CREATE TABLE gbpforex (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    countryUuid text UNIQUE  REFERENCES country(uuid),
    baserate double precision NOT NULL,
    impalarate double precision NOT NULL
   
   
);

\COPY gbpforex(uuid, countryUuid, baserate, impalarate) FROM '/tmp/gbpforex.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE gbpforex OWNER TO remittance;

-- --------------------
-- Table FOREXRATE
-- --------------------

CREATE TABLE forexrate (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    currencypair text UNIQUE,
    marketrate double precision NOT NULL,
    spreadrate double precision NOT NULL
   
   
);

\COPY forexrate(uuid, currencypair, marketrate, spreadrate) FROM '/tmp/forexrate.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE forexrate OWNER TO remittance;

-- --------------------
-- Table CHECKERFOREXRATE
-- --------------------

CREATE TABLE checkerforexrate (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    currencypair text,
    marketrate double precision NOT NULL,
    spreadrate double precision NOT NULL,
    uploadDate timestamp with time zone NOT NULL DEFAULT now()
   
);

\COPY checkerforexrate(uuid, currencypair, marketrate, spreadrate, uploadDate) FROM '/tmp/forexratehistory.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE checkerforexrate OWNER TO remittance;

-- --------------------
-- Table FOREXRATE_HISTORY
-- --------------------

CREATE TABLE forexratehistory (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    currencypair text,
    marketrate double precision NOT NULL,
    spreadrate double precision NOT NULL,
    uploadDate timestamp with time zone NOT NULL DEFAULT now()
   
);

\COPY forexratehistory(uuid, currencypair, marketrate, spreadrate, uploadDate) FROM '/tmp/forexratehistory.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE forexratehistory OWNER TO remittance;

-- --------------------
-- Table FOREX_HISTORY
-- --------------------

CREATE TABLE forexhistory (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    countryUuid text  REFERENCES country(uuid),
    baserate double precision NOT NULL,
    impalarate double precision NOT NULL,
    currencytype text NOT NULL,
    uploadDate timestamp with time zone NOT NULL DEFAULT now()
   
);

\COPY usdforex(uuid, countryUuid, baserate, impalarate, uploadDate) FROM '/tmp/usdforex.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE forexhistory OWNER TO remittance;

-- ==================================
-- 4 Routing Management
-- ==================================
-- --------------------
-- Table NETWORK
-- --------------------
CREATE TABLE network (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    countryUuid text REFERENCES country(uuid),
    remitip text,
    bridgeremitip text,
    queryip text,
    bridgequeryip text,
    balanceip text,
    bridgebalanceip text,
    reversalip text,
    bridgereversalip text,
    forexip text,
    bridgeforexip text,
    accountcheckip text,
    bridgeaccountcheckip text,
    extraurl text,
    username text,
    password text,
    networkname text,
    partnername text,
    commission double precision,
    supportforex boolean,
    supportreversal boolean,
    supportaccountcheck boolean,
    supportquerycheck boolean,
    supportbalancecheck boolean,
    supportcommissionpercentage boolean,
    networkStatusUuid text REFERENCES accountStatus(uuid),
    dateadded timestamp with time zone NOT NUll
);
\COPY network(uuid, countryUuid, remitip, bridgeremitip, queryip, bridgequeryip, balanceip, bridgebalanceip, reversalip, bridgereversalip, forexip, bridgeforexip, accountcheckip, bridgeaccountcheckip, extraurl, username, password, networkname, partnername, commission, supportforex, supportreversal, supportaccountcheck,supportquerycheck,supportbalancecheck,supportcommissionpercentage,networkStatusUuid,dateadded) FROM '/tmp/network.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE network OWNER TO remittance;

-- ==================================
-- 5 Balanace/Float Management
-- ==================================

-- ------------------------
-- Table clientBalance
-- ------------------------
CREATE TABLE clientbalance (
    balanceId SERIAL PRIMARY KEY,
    uuid text NOT NULL,
    accountUuid text REFERENCES account(uuid) UNIQUE,
    balance double precision NOT NULL CHECK (balance >= 0)
);

\COPY clientbalance(uuid,accountUuid,balance) FROM '/tmp/ClientBalance.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE clientbalance OWNER TO remittance;

-- ---------------------
-- Table Topup
-- ---------------------
CREATE TABLE topup (
    topupId SERIAL PRIMARY KEY,
    uuid text NOT NULL,
    accountUuid text REFERENCES account(uuid),
    amount double precision,
    topupTime timestamp NOT NULL DEFAULT now()
);

\COPY topup(uuid, accountUuid,amount,topupTime) FROM '/tmp/Topups.csv'  WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE topup OWNER TO remittance;

-- ------------------------
-- Table CheckerBalanceByCountry
-- ------------------------

CREATE TABLE checkerbalancebycountry (
    balanceId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    countryUuid text  REFERENCES country(uuid),
    accountUuid text REFERENCES account(uuid),
    balance double precision NOT NULL CHECK (balance >= 0)
);

\COPY checkerbalancebycountry(uuid,countryuuid,accountUuid,balance) FROM '/tmp/BalancebyCountry.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE checkerbalancebycountry OWNER TO remittance; 
-- ------------------------
-- Table BalanceByCountry
-- ------------------------

CREATE TABLE balancebycountry (
    balanceId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    countryUuid text  REFERENCES country(uuid),
    accountUuid text REFERENCES account(uuid),
    balance double precision NOT NULL CHECK (balance >= 0)
);

\COPY balancebycountry(uuid,countryuuid,accountUuid,balance) FROM '/tmp/BalancebyCountry.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE balancebycountry OWNER TO remittance;

-- ---------------------
-- TopupByCountry
-- ---------------------
CREATE TABLE topupbycountry (
    topupId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    accountUuid text REFERENCES account(uuid) ,
    countryUuid text  REFERENCES country(uuid),
    amount double precision,
    topupTime timestamp NOT NULL DEFAULT now()
);

\COPY topupbycountry(uuid,accountuuid,countryuuid,amount,topupTime) FROM '/tmp/Topupsbycountry.csv'  WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE topupbycountry OWNER TO remittance;

-- ------------------------
-- Table networkBalanceByCountry
-- ------------------------

CREATE TABLE networkbalancebycountry (
    balanceId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    networkUuid text REFERENCES network(uuid) UNIQUE,
    balance double precision NOT NULL CHECK (balance >= 0)
);

\COPY networkbalancebycountry(uuid,networkUuid,balance) FROM '/tmp/NetworkBalancebyCountry.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE networkbalancebycountry OWNER TO remittance;

-- ---------------------
-- networkTopupByCountry
-- ---------------------
CREATE TABLE networktopupbycountry (
    topupId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    networkUuid text REFERENCES network(uuid),
    balance double precision,
    topupTime timestamp NOT NULL DEFAULT now()
);

\COPY networktopupbycountry(uuid,networkUuid,balance,topupTime) FROM '/tmp/NetworkTopupsbycountry.csv'  WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE networktopupbycountry OWNER TO remittance;

-- ------------------------
-- Table clientmainBalance
-- ------------------------
CREATE TABLE clientmainbalance (
    balanceId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    accountUuid text REFERENCES account(uuid),
    currency text,
    balance double precision NOT NULL CHECK (balance >= 0)
);

\COPY clientmainbalance(uuid,accountUuid,currency,balance) FROM '/tmp/ClientmainBalance.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE clientmainbalance OWNER TO remittance;
-- ------------------------
-- Table checkerclientmainBalance
-- ------------------------
CREATE TABLE checkerclientmainbalance (
    balanceId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    accountUuid text REFERENCES account(uuid),
    currency text,
    balance double precision NOT NULL CHECK (balance >= 0)
);

\COPY checkerclientmainbalance(uuid,accountUuid,currency,balance) FROM '/tmp/ClientmainBalance.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE checkerclientmainbalance OWNER TO remittance;

-- ---------------------
-- Table clientmainbalancehistory
-- ---------------------
CREATE TABLE clientmainbalancehistory (
    topupId SERIAL PRIMARY KEY,
    uuid text NOT NULL,
    accountUuid text REFERENCES account(uuid),
    amount double precision,
    currency text,
    topupTime timestamp NOT NULL DEFAULT now()
);

\COPY clientmainbalancehistory(uuid, accountUuid,amount,currency,topupTime) FROM '/tmp/Clientmainbalancehistory.csv'  WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE clientmainbalancehistory OWNER TO remittance;

-- --------------------
-- Table BANK
-- --------------------
CREATE TABLE bank (
    id SERIAL PRIMARY KEY,
    uuid text  UNIQUE NOT NULL, 
    bankname text NOT NULL,
    countryUuid text REFERENCES country(uuid),
    networkUuid text REFERENCES network(uuid), 
    bankcode text UNIQUE NOT NULL,
    branchcode text,
    iban text,
    dateadded timestamp with time zone NOT NULL DEFAULT now()
);

ALTER TABLE bank OWNER TO remittance;

-- --------------------
-- Table MERCHANT_CODES
-- --------------------
CREATE TABLE merchantbillcode (
    id SERIAL PRIMARY KEY,
    uuid text  UNIQUE NOT NULL, 
    merchantname text NOT NULL,
    countryUuid text REFERENCES country(uuid),
    networkUuid text REFERENCES network(uuid), 
    merchantcode text UNIQUE NOT NULL,
    merchantid text,
    msisdn text,
    dateadded timestamp with time zone NOT NULL DEFAULT now()
);

ALTER TABLE merchantbillcode OWNER TO remittance;

-- --------------------
-- Table PREFIX
-- --------------------
CREATE TABLE prefix (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    networkUuid text REFERENCES network(uuid), 
    countryUuid text REFERENCES country(uuid),
    Prefix text UNIQUE NOT NULL,
    wallettype text NOT NULL,
    --splitlength int NOT NULL,
    dateadded timestamp with time zone NOT NUll
);
\COPY prefix(uuid, networkUuid, countryUuid, Prefix, wallettype, dateadded) FROM '/tmp/prefix.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE prefix OWNER TO remittance;

-- ---------------------
-- msisdnByCountry
-- ---------------------
CREATE TABLE msisdnbycountry (
    msisdnId SERIAL PRIMARY KEY,
    uuid text NOT NULL,
    countryUuid text  REFERENCES country(uuid),
    networkUuid text REFERENCES network(uuid),
    accountUuid text REFERENCES account(uuid),
    msisdn text,
    creationDate timestamp with time zone NOT NULL DEFAULT now()
);

\COPY msisdnbycountry(uuid,countryuuid,accountUuid,msisdn,creationDate) FROM '/tmp/msisdnbycountry.csv'  WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE msisdnbycountry OWNER TO remittance;

-- -----------------------
-- RouteDefination
-- -----------------------
CREATE TABLE routedefine (
    id SERIAL PRIMARY KEY,
    uuid text NOT NULL,
    accountUuid text  REFERENCES account(uuid),
    networkUuid text REFERENCES network(uuid),
    supportforex boolean,
    fixedcommission boolean,
    presettlement boolean,
    commission double precision NOT NULL,
    minimumbalance double precision NOT NULL,
    creationDate timestamp with time zone NOT NULL DEFAULT now()
);

\COPY routedefine(uuid,accountUuid,networkUuid,supportforex,fixedcommission,commission,minimumbalance,creationDate) FROM '/tmp/routedefine.csv'  WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE msisdnbycountry OWNER TO remittance;

-- ==================================
-- 6. Audit Trail
-- ==================================

-- --------------------
-- Table logincount
-- --------------------
CREATE TABLE logincount (
    logincountId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    accountUuid text REFERENCES account(uuid),
    countlogin int 
);

\COPY logincount(uuid, accountUuid, countlogin) FROM '/tmp/logincount.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE logincount OWNER TO remittance;

-- -----------------------
--System Logs
-- -----------------------
CREATE TABLE systemlog(
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    username text,
    action text NOT NULL,
    creationDate timestamp with time zone NOT NULL DEFAULT now()
);

\COPY managementaccount(uuid, username, accountName, username,action,creationDate) FROM '/tmp/systemlog.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE systemlog OWNER TO remittance;


-- ==================================
-- 7. Simulation
-- ==================================

-- --------------------
-- Table Error_simulation
-- --------------------
CREATE TABLE errorsimulation (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    mobilenumber text UNIQUE NOT NULL,
    errorcode text UNIQUE NOT NULL,
    errorname text
   
);

\COPY errorsimulation(uuid, mobilenumber, errorcode,errorname) FROM '/tmp/errorsimulation.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE errorsimulation OWNER TO remittance;


-- ==================================
-- 8. Security
-- ==================================

-- --------------------
-- Table Client IP Address
-- --------------------

CREATE TABLE clientipaddress (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    accountUuid text REFERENCES account(uuid),
    ipAddress text
   
);

\COPY clientipaddress(uuid, accountUuid,ipAddress) FROM '/tmp/clientipaddress.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE clientipaddress OWNER TO remittance;

-- ==================================
-- 9. Transaction Management
-- ==================================
-- ------------------
-- Table transactionStatus
-- ------------------

CREATE TABLE transactionStatus (
    transactionStatusId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    status text UNIQUE,
    description text 
);

\COPY transactionstatus(uuid, status, description) FROM '/tmp/TransactionStatus.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE transactionStatus OWNER TO remittance;


-- --------------------
-- Table transaction
-- --------------------
CREATE TABLE transaction (
    id SERIAL PRIMARY KEY,
    --uuid text  NOT NULL,
    uuid text UNIQUE NOT NULL, --//proposed version  
    accountUuid text REFERENCES account(uuid),
    sourceCountryCode text  NOT NULL,
    senderName text NOT NULL,
    recipientMobile text NOT NULL,
    amount double precision NOT NULL,
    currencyCode text NOT NULL,
    recipientCountryUuid text  REFERENCES country(uuid),
    senderToken text NOT NULL,
    clientTime text NOT NULL,
    serverTime timestamp with time zone NOT NULL,
    transactionStatusUuid text  REFERENCES transactionStatus(uuid),
    referenceNumber text,
    receivertransactionUuid text NOT NULL,
    networkUuid text REFERENCES network(uuid)
);

\COPY transaction(uuid,accountUuid,sourcecountrycode,sendername,recipientmobile,amount,currencycode,recipientcountryuuid,sendertoken,clienttime,servertime,transactionstatusuuid,referencenumber,receivertransactionuuid,networkuuid) FROM '/tmp/Transactions.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE transaction OWNER TO remittance;

-- --------------------
-- Table TRANSACTION_FOREX
-- --------------------

CREATE TABLE transactionforex (
    id SERIAL PRIMARY KEY,
    uuid text  NOT NULL, 
    transactionUuid text REFERENCES transaction(uuid), --//proposed change.
    recipientCountry text,
    account text,
    localamount double precision NOT NULL,
    accounttype text NOT NULL,
    convertedamount double precision NOT NULL,
    impalarate text NOT NULL,
    baserate text NOT NULL,
    receivermsisdn text,
    surplus  double precision,
    serverTime timestamp with time zone NOT NULL
    
);

\COPY transactionforex(uuid,transactionuuid,recipientcountryUuid,accountuuid,localamount,accounttype,convertedamount,impalarate,baserate,receivermsisdn,servertime) FROM '/tmp/Transactionforex.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE transactionforex OWNER TO remittance;

-- --------------------
-- Table TRANSACTION_TYPE
-- --------------------

CREATE TABLE transactiontype (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    type text
);

\COPY transactiontype(uuid, type) FROM '/tmp/transactiontype.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE transactiontype OWNER TO remittance;

-- --------------------
-- Table TRANSACTION_TRANSFERTYPE
-- --------------------

CREATE TABLE transactiontransfertype (
    id SERIAL PRIMARY KEY,
    uuid text  NOT NULL, 
    transactionUuid text REFERENCES transaction(uuid),
    transactiontypeUuid text REFERENCES transactiontype(uuid),
    sourcecountrycode text, 
    senderfirstname text,
    senderlastname text,
    senderaddress text,
    sendercity text,
    recipientfirstname text,
    recipientlastname text, 
    recipientmobile text,
    recipientaccount text,
    serverTime timestamp with time zone NOT NUll
);

ALTER TABLE transactiontransfertype OWNER TO remittance;

-- --------------------
-- Table COMMISSION_ESTIMATES
-- --------------------

CREATE TABLE commissionestimate (
    id SERIAL PRIMARY KEY,
    uuid text  NOT NULL, 
    transactionUuid text REFERENCES transaction(uuid), --//proposed change.
    commissioncurrency text,
    commision double precision NOT NULL,
    receivercommission double precision NOT NULL,
    receiverfullamount double precision NOT NULL,
    statusuuid text,
    serverTime timestamp with time zone NOT NULL DEFAULT now()
    
);
\COPY transactionforex(uuid,transactionuuid,recipientcountryUuid,accountuuid,localamount,accounttype,convertedamount,impalarate,baserate,receivermsisdn,servertime) FROM '/tmp/Transactionforex.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE commissionestimate OWNER TO remittance;
-- -------------------------------------------------------------------
-- The below have been introduced to handle credit-inprogress challenge
-- -------------------------------------------------------------------
-- ------------------------
-- Table clientmainBalance
-- ------------------------
CREATE TABLE clientmainbalanceHold (
    balanceId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    accountUuid text REFERENCES account(uuid),
    currency text,
    transactionUuid text REFERENCES transaction(uuid) UNIQUE,
    amount double precision NOT NULL CHECK (amount >= 0)
);

\COPY clientmainbalance(uuid,accountUuid,currency,balance) FROM '/tmp/ClientmainBalance.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE clientmainbalanceHold OWNER TO remittance;

-- ---------------------
-- Table clientmainbalanceholdhistory
-- ---------------------
CREATE TABLE clientmainbalanceHoldhistory (
    topupId SERIAL PRIMARY KEY,
    uuid text NOT NULL,
    accountUuid text REFERENCES account(uuid),
    amount double precision,
    currency text,
    transactionUuid text REFERENCES transaction(uuid) UNIQUE,
    refundedback boolean,
    processed boolean,
    topupTime timestamp NOT NULL DEFAULT now()
);

\COPY clientmainbalancehistory(uuid, accountUuid,amount,currency,topupTime) FROM '/tmp/Clientmainbalancehistory.csv'  WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE clientmainbalanceHoldhistory OWNER TO remittance;

-- ------------------------
-- Table BalanceByCountryHold
-- ------------------------

CREATE TABLE balancebycountryHold (
    balanceId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    countryUuid text  REFERENCES country(uuid),
    accountUuid text REFERENCES account(uuid),
    transactionUuid text REFERENCES transaction(uuid),
    amount double precision NOT NULL CHECK (amount >= 0)
);

\COPY balancebycountry(uuid,countryuuid,accountUuid,balance) FROM '/tmp/BalancebyCountry.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE balancebycountryHold OWNER TO remittance;

-- ---------------------
-- TopupByCountryHold
-- ---------------------
CREATE TABLE balancebycountryHoldhistory (
    topupId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    accountUuid text REFERENCES account(uuid) ,
    countryUuid text  REFERENCES country(uuid),
    amount double precision,
    transactionUuid text REFERENCES transaction(uuid),
    refundedback boolean,
    processed boolean,
    topupTime timestamp NOT NULL DEFAULT now()
);

\COPY topupbycountry(uuid,accountuuid,countryuuid,amount,topupTime) FROM '/tmp/Topupsbycountry.csv'  WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE balancebycountryHoldhistory OWNER TO remittance;



-- --------------------
-- Table TRANSACTION_UPDATE
-- --------------------

CREATE TABLE transactionupdatestatus (
    id SERIAL PRIMARY KEY,
    uuid text  NOT NULL, 
    transactionUuid text REFERENCES transaction(uuid),
    recipientcurrency text REFERENCES country(uuid),
    sourcecurrency text REFERENCES country(uuid),
    localamount double precision NOT NULL,
    convertedamount double precision NOT NULL,
    exchangerate text NOT NULL,
    Currentstatus text NOT NULL,
    Updatestatus text,
    serverTime timestamp with time zone NOT NULL
    
);

ALTER TABLE transactionupdatestatus OWNER TO remittance;


-- --------------------
-- Table TRANSACTION_UPDATEHISTORY
-- --------------------
CREATE TABLE transactionupdatestatushistory (
    id SERIAL PRIMARY KEY,
    uuid text  NOT NULL, 
    transactionUuid text REFERENCES transaction(uuid),
    recipientcurrency text REFERENCES country(uuid),
    sourcecurrency text REFERENCES country(uuid),
    localamount double precision NOT NULL,
    convertedamount double precision NOT NULL,
    exchangerate text NOT NULL,
    Currentstatus text NOT NULL,
    Updatestatus text,
    serverTime timestamp with time zone NOT NULL
    
);

ALTER TABLE transactionupdatestatushistory OWNER TO remittance;


-- ---------------------
-- collection_network
-- ---------------------

-- --------------------
-- Table TRANSACTION_TYPE
-- --------------------

CREATE TABLE collectionchannel (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    type text
);

\COPY collectionchannel(uuid, type) FROM '/tmp/collectionchannel.csv' WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE collectionchannel OWNER TO remittance;


CREATE TABLE collection_network(
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    supportlisteningurl boolean,
    supportquerying boolean,
    supportdebit boolean,
    networkname text UNIQUE NOT NULL,
    providername text,
    collectiontchannel text REFERENCES collectionchannel(uuid),
    addedbyuuid text, 
    queryingurl text,
    bridgequeryurl text,
    debiturl text,
    bridgedebiturl text,
    username text,
    password text,
    instruction text,
    countryUuid text REFERENCES country(uuid),
    balanceurl text, 
    bridgebalanceurl text, 
    reversalurl text, 
    bridgereversalurl text, 
    forexurl text,
    bridgeforexurl text, 
    accountcheckurl text, 
    bridgeaccountcheckurl text, 
    extraurl text,
    supportreversal boolean, 
    supportaccountcheck boolean,
    supportbalancecheck boolean,
    dateadded timestamp with time zone NOT NUll
);

\COPY collection_network(uuid,supportlisteningurl,supportquerying,supportdebit,networkname,providername,collectiontchannel,addedbyuuid,queryingurl,bridgequeryurl,debiturl,bridgedebiturl,username,password,instruction,countryuuid,balanceurl,bridgebalanceurl,reversalurl,bridgereversalurl,forexurl,bridgeforexurl,accountcheckurl,bridgeaccountcheckurl,extraurl,supportreversal,supportaccountcheck,supportbalancecheck,dateadded) FROM '/tmp/collectionnetwork.csv'  WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE collection_network OWNER TO remittance;

-- ---------------------
-- collection_networksubaccount
-- ---------------------

CREATE TABLE collection_networksubaccount(
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    networkuuid text REFERENCES collection_network(uuid),
    sharedconfiguration boolean, --for paybills
    collectionnumber text UNIQUE NOT NULL, 
    currency text,
    referencesplitlength int,
    dateadded timestamp with time zone NOT NUll
);

\COPY collection_networksubaccount(uuid,networkuuid,sharedconfiguration,collectionnumber,currency,referencesplitlength,dateadded) FROM '/tmp/collectionnetworksubaccount.csv'  WITH DELIMITER AS '|' CSV HEADER

ALTER TABLE collection_networksubaccount OWNER TO remittance;

-- -----------------------
-- CollectionDefination
-- -----------------------
CREATE TABLE collectiondefine (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    accountUuid text  REFERENCES account(uuid),
    collectionnetworksubaccountUuid text,
    networkuuid text REFERENCES collection_network(uuid),
    supportforex boolean,
    fixedcommission boolean,
    presettlement boolean,
    addedbyuuid text, 
    commission double precision NOT NULL,
    referenceprefix text,
    listeninguri text,
    dateadded timestamp with time zone NOT NULL DEFAULT now()
);

\COPY collectiondefine(uuid,accountUuid,collectionnetworksubaccountUuid,networkuuid,supportforex,fixedcommission,presettlement,addedbyuuid,commission,referenceprefix,listeninguri,dateadded) FROM '/tmp/collectiondefine.csv'  WITH DELIMITER AS '|' CSV HEADER
ALTER TABLE collectiondefine OWNER TO remittance;


CREATE TABLE temp_incomingtransaction(
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    networkuuid text REFERENCES collection_network(uuid),
    creditaccountuuid text REFERENCES account(uuid),
    transactiontype text,
    debitorname text,
    collectiondefineuuid text REFERENCES collectiondefine(uuid),
    debitcurrency text REFERENCES country(uuid),
    debitcountry text REFERENCES country(uuid),
    amount double precision NOT NULL,
    accountreference text, 
    transactionstatusuuid text  REFERENCES transactionStatus(uuid),
    debitedaccount text,
    originatetransactionuuid text UNIQUE NOT NULL,
    receivertransactionuuid text,
    vendorunique text,
    originateamount double precision NOT NULL,
    originatecurrency text,
    processingstatus text  REFERENCES transactionStatus(uuid),
    endpointstatusdescription text,
    serverTime timestamp with time zone NOT NUll
);

ALTER TABLE temp_incomingtransaction OWNER TO remittance;	


-- ---------------------
-- processedtransaction
-- ---------------------

CREATE TABLE processedtransaction(
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    networkuuid text REFERENCES collection_network(uuid),
    creditaccountuuid text REFERENCES account(uuid),
    transactiontype text,
    debitorname text,
    collectiondefineuuid text REFERENCES collectiondefine(uuid),
    debitcurrency text REFERENCES country(uuid),
    debitcountry text REFERENCES country(uuid),
    amount double precision NOT NULL,
    accountreference text, 
    transactionstatusuuid text  REFERENCES transactionStatus(uuid),
    debitedaccount text,
    originatetransactionuuid text UNIQUE NOT NULL,
    receivertransactionuuid text,
    vendorunique text,
    originateamount double precision NOT NULL,
    originatecurrency text,
    endpointstatusdescription text,
    commission double precision,
    serverTime timestamp with time zone NOT NUll
);

ALTER TABLE processedtransaction OWNER TO remittance;


-- ---------------------
-- Forward Collection
-- ---------------------

CREATE TABLE forwardcollection(
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    creditaccountuuid text REFERENCES account(uuid),
    networkname text,
    transactiontype text,
    debitorname text,
    debitcurrency text REFERENCES country(uuid),
    amount double precision NOT NULL,
    accountreference text, 
    debitedaccount text,
    originatetransactionuuid text UNIQUE NOT NULL,
    forwarduri text,
    collectiondate timestamp with time zone NOT NUll
);

ALTER TABLE forwardcollection OWNER TO remittance;


-- ---------------------
-- ForwardCollectionHistory
-- ---------------------

CREATE TABLE forwardcollectionhistory(
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    creditaccountuuid text REFERENCES account(uuid),
    networkname text,
    transactiontype text,
    debitorname text,
    debitcurrency text REFERENCES country(uuid),
    amount double precision NOT NULL,
    accountreference text, 
    debitedaccount text,
    originatetransactionuuid text UNIQUE NOT NULL,
    forwarduri text,
    processingstatus text REFERENCES transactionStatus(uuid),
    endpointstatusdescription text,
    collectiondate timestamp with time zone NOT NUll
);

ALTER TABLE forwardcollectionhistory OWNER TO remittance;

-- ------------------------
-- Table collectionbalance
-- ------------------------

CREATE TABLE collectionbalance (
    balanceId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    accountUuid text REFERENCES account(uuid),
    countryUuid text  REFERENCES country(uuid),
    balance double precision NOT NULL CHECK (balance >= 0)
);

ALTER TABLE collectionbalance OWNER TO remittance;


-- ---------------------
-- CollectionByCountryHistory
-- ---------------------
CREATE TABLE collectionbalancehistory (
    balanceId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    accountUuid text REFERENCES account(uuid) ,
    countryUuid text  REFERENCES country(uuid),
    transactionuuid text,
    amount double precision,
    balance double precision,
    topupTime timestamp NOT NULL DEFAULT now()
);

ALTER TABLE collectionbalancehistory OWNER TO remittance;
-- ------------------------
-- Table collectionclosingbalance
-- -----------------------

-- ==================================
-- 11. Accounts Withdrawals
-- ==================================
-- ---------------------
-- Table client_withdrawal
-- ---------------------
CREATE TABLE client_withdrawal (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    accountUuid text REFERENCES account(uuid) ,
    currencyUuid text  REFERENCES country(uuid),
    tocurrencyUuid text  REFERENCES country(uuid),
    amount double precision,
    extrainformation text,
    transactionDate timestamp NOT NULL DEFAULT now()
);

ALTER TABLE client_withdrawal OWNER TO remittance;


-- ---------------------
-- Table checker_withdrawal
-- ---------------------

CREATE TABLE checker_withdrawal (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    accountUuid text REFERENCES account(uuid) ,
    currencyUuid text  REFERENCES country(uuid),
    tocurrencyUuid text  REFERENCES country(uuid),
    amount double precision,
    extrainformation text,
    systemexchangerate double precision NOT NULL,
    comission double precision,
    comissioncurrencyUuid text  REFERENCES country(uuid),
    adminextrainformation text,
    authorisedchecker text REFERENCES managementaccount(uuid),
    transactionDate timestamp NOT NULL DEFAULT now()
);

ALTER TABLE checker_withdrawal OWNER TO remittance;


-- ---------------------
-- Table withdrawal_history
-- ---------------------

CREATE TABLE withdrawal_history (
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    accountUuid text REFERENCES account(uuid),
    currencyUuid text  REFERENCES country(uuid),
    tocurrencyUuid text  REFERENCES country(uuid),
    amount double precision,
    extrainformation text,
    systemexchangerate double precision NOT NULL,
    comission double precision,
    comissioncurrencyUuid text  REFERENCES country(uuid),
    adminextrainformation text,
    transactionStatusUuid text  REFERENCES transactionStatus(uuid),
    bankwithdrawexchangerate double precision NOT NULL,
    authorisedchecker text REFERENCES managementaccount(uuid),
    authorisedmaker text REFERENCES managementaccount(uuid),
    transfercharges double precision,
    transferchargecurrencyUuid text REFERENCES country(uuid),
    receivableamount double precision,
    transactionDate timestamp NOT NULL DEFAULT now()
);

ALTER TABLE withdrawal_history OWNER TO remittance;


-- -------------------------------------------------------------------
-- The below have been introduced to handle credit-inprogress challenge
-- -------------------------------------------------------------------
-- ------------------------
-- Table withdrawalbalanceHold
-- ------------------------
CREATE TABLE withdrawalbalanceHold (
    balanceId SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL,
    accountUuid text REFERENCES account(uuid),
    currency text,
    transactionUuid text REFERENCES withdrawal_history(uuid) UNIQUE,
    amount double precision NOT NULL CHECK (amount >= 0)
);

ALTER TABLE withdrawalbalanceHold OWNER TO remittance;

-- ---------------------
-- Table clientmainbalanceholdhistory
-- ---------------------
CREATE TABLE withdrawalbalanceHoldhistory (
    topupId SERIAL PRIMARY KEY,
    uuid text NOT NULL,
    accountUuid text REFERENCES account(uuid),
    amount double precision,
    currency text,
    transactionUuid text REFERENCES withdrawal_history(uuid) UNIQUE,
    refundedback boolean,
    processed boolean,
    topupTime timestamp NOT NULL DEFAULT now()
);

ALTER TABLE withdrawalbalanceHoldhistory OWNER TO remittance;


CREATE TABLE temp_unresolvedtransaction(
    id SERIAL PRIMARY KEY,
    uuid text UNIQUE NOT NULL, 
    networkuuid text REFERENCES collection_network(uuid),
    creditaccountuuid text REFERENCES account(uuid),
    transactiontype text,
    debitorname text,
    collectiondefineuuid text REFERENCES collectiondefine(uuid),
    debitcurrency text REFERENCES country(uuid),
    debitcountry text REFERENCES country(uuid),
    amount double precision NOT NULL,
    accountreference text, 
    transactionstatusuuid text  REFERENCES transactionStatus(uuid),
    debitedaccount text,
    originatetransactionuuid text UNIQUE NOT NULL,
    receivertransactionuuid text,
    vendorunique text,
    originateamount double precision NOT NULL,
    originatecurrency text,
    processingstatus text  REFERENCES transactionStatus(uuid),
    endpointstatusdescription text,
    serverTime timestamp with time zone NOT NUll
);

ALTER TABLE temp_unresolvedtransaction OWNER TO remittance;	


















